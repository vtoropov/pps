#ifndef _UIXDRAWRENDERER_H_B10C71E6_F9B5_49d6_A9EE_82013C781AAB_INCLUDED
#define _UIXDRAWRENDERER_H_B10C71E6_F9B5_49d6_A9EE_82013C781AAB_INCLUDED
/*
	Created by Tech_dog (ebontrop@gmail.com) on 6-Feb-2015 at 00:32:11AM, GMT+3, Taganrog, Friday;
	This is UIX Draw library renderer class(es) declaration file.
	-----------------------------------------------------------------------------
	Adopted to version v15 on 16-Feb-2021 at 5:58:07.517 am, UTC+7, Novosibirsk, Tuesday;
*/
#include "UIX_CommonDrawDefs.h"
#include "UIX_GdiProvider.h"
#include "UIX_PngWrap.h"

namespace ex_ui { namespace draw { namespace renderers
{
	using ex_ui::draw::common::CPngBitmapPtr;
	using ex_ui::draw::CZBuffer;

	class CBackgroundRendererDefImpl:
		public ex_ui::draw::defs::IRenderer
	{
	protected:
		CPngBitmapPtr     m_bkgnd_cache;
	protected:
		::ATL::CWindow&   m_owner_ref;
		INT               m_x_shift;
		INT               m_y_shift;
	protected:
		 CBackgroundRendererDefImpl(::ATL::CWindow&  owner_ref);
		~CBackgroundRendererDefImpl(void);
	protected:
		virtual   HRESULT DrawParentBackground(const HWND hChild, const HDC hSurface, const RECT& rcDrawArea) override;
	public:
		HRESULT   GetImageSize(SIZE& __in_out_ref) const;
		HRESULT   SetVerticalShift(const INT);
	private:
		CBackgroundRendererDefImpl(const CBackgroundRendererDefImpl&);
		CBackgroundRendererDefImpl& operator= (const CBackgroundRendererDefImpl&);
	};

	class CBackgroundTileRenderer:
		public CBackgroundRendererDefImpl
	{
		typedef CBackgroundRendererDefImpl TBase;
	private:
		CPngBitmapPtr     m_tile_ptr;
	public:
		 CBackgroundTileRenderer(::ATL::CWindow&  owner_ref);
		 CBackgroundTileRenderer(const UINT nResId, ::ATL::CWindow&  owner_ref);
		~CBackgroundTileRenderer(void);
	private:
		virtual HRESULT   DrawBackground(const HDC hSurface, const RECT& rcDrawArea) override sealed;
	public:
		HRESULT           Draw(CZBuffer&, const RECT& rcDrawArea);
		HRESULT           InitializeFromFile(LPCTSTR pszFilePath);
	private:
		CBackgroundTileRenderer(const CBackgroundTileRenderer&);
		CBackgroundTileRenderer& operator= (const CBackgroundTileRenderer&);
	private:
		HRESULT   __create_cache(const RECT& rcDrawArea);
	};
}}}

#endif/*_UIXDRAWRENDERER_H_B10C71E6_F9B5_49d6_A9EE_82013C781AAB_INCLUDED*/