/*
	Created by Tech_dog (ebontrop@gmail.com) on 6-Feb-2015 at 2:03:53pm, GMT+3, Taganrog, Friday;
	This is UIX Draw library PNG wrapper common class(es) implementation file.
	-----------------------------------------------------------------------------
	Adopted to version v15 on 16-Feb-2021 at 6:07:51.557 am, UTC+7, Novosibirsk, Tuesday;
*/
#include "StdAfx.h"
#include "UIX_PngWrap.h"

using namespace ex_ui;
using namespace ex_ui::draw;
using namespace ex_ui::draw::common;

typedef Gdiplus::Bitmap TBitmap;
typedef CPngBitmap PBitmap;
/////////////////////////////////////////////////////////////////////////////

namespace ex_ui { namespace draw { namespace common { namespace details
{
	Gdiplus::Bitmap&   GetGdiPlusBitmapInvalidObject_Ref(void)
	{
		static Gdiplus::Bitmap bitmap(0, 0, PixelFormat32bppARGB);
		return bitmap;
	}

	CPngBitmap&        GetPngBitmapInvalidObject_Ref(void)
	{
		static CPngBitmap png;
		return png;
	}
}}}}

/////////////////////////////////////////////////////////////////////////////

CPngBitmap::CPngBitmap(void) : m_pBitmap(NULL)
{
}

CPngBitmap::CPngBitmap(const INT nWidth, const INT nHeight) : m_pBitmap(NULL)
{
	try
	{
		m_pBitmap = new Gdiplus::Bitmap(nWidth, nHeight, PixelFormat32bppARGB);
	}
	catch(::std::bad_alloc&)
	{
		ATLASSERT(0);
	}
}

CPngBitmap::CPngBitmap(LPCWSTR pszFile) : m_pBitmap(NULL)
{
	this->Load(pszFile);
}

CPngBitmap::~CPngBitmap(void)
{
	this->Empty();
}

/////////////////////////////////////////////////////////////////////////////

CPngBitmap::operator Gdiplus::Bitmap*(void) const
{
	return m_pBitmap;
}

/////////////////////////////////////////////////////////////////////////////

HRESULT  CPngBitmap::Attach(Gdiplus::Bitmap* pb)
{
	this->Empty();
	m_pBitmap = pb;
	return (IsValid() ? S_OK : S_FALSE);
}

HBITMAP  CPngBitmap::Clone(void) const
{
	HBITMAP hBitmap = NULL;
	HRESULT hr_ = this->GetHandle(hBitmap);
	return (hr_ == S_OK ? hBitmap : NULL);
}

TBitmap* CPngBitmap::Detach(void)
{
	Gdiplus::Bitmap* pBitmap = m_pBitmap;
	m_pBitmap = NULL;
	return pBitmap;
}

VOID     CPngBitmap::Empty(void)								
{
	if (m_pBitmap)
	{
		delete m_pBitmap;
		m_pBitmap = NULL;
	}
}

HRESULT  CPngBitmap::GetHandle(HBITMAP& ph) const
{
	if (!m_pBitmap)
		return OLE_E_BLANK;
	(*m_pBitmap).GetHBITMAP(0, &ph);
	return S_OK;
}

TBitmap* CPngBitmap::GetPtr(void) const
{
	return m_pBitmap;
}

const
TBitmap& CPngBitmap::GetRef(void) const
{
	return (NULL == m_pBitmap ? details::GetGdiPlusBitmapInvalidObject_Ref() : *m_pBitmap);
}

TBitmap& CPngBitmap::GetRef(void)
{
	return (NULL == m_pBitmap ? details::GetGdiPlusBitmapInvalidObject_Ref() : *m_pBitmap);
}

SIZE     CPngBitmap::GetSize(void) const
{
	SIZE sz_ = {0};
	if (this->IsValid())
	{
		sz_.cx = m_pBitmap->GetWidth();
		sz_.cy = m_pBitmap->GetHeight();
	}
	return sz_;
}

bool     CPngBitmap::IsValid(void) const
{
	return (m_pBitmap && m_pBitmap->GetLastStatus() == Gdiplus::Ok);
}

HRESULT  CPngBitmap::Load(LPCWSTR pszFile)
{
	this->Empty();
	m_pBitmap = Gdiplus::Bitmap::FromFile(pszFile);
	HRESULT hr_ = GdiplusStatusToHresult(m_pBitmap->GetLastStatus());
	return  hr_;
}

/////////////////////////////////////////////////////////////////////////////

CPngBitmapPtr::CPngBitmapPtr(const bool bCreateObject) : m_pBitmap(NULL), m_hResult(OLE_E_BLANK)
{
	if (bCreateObject)
		m_hResult = CPngBitmapPtr::CreateObject(m_pBitmap);
}

CPngBitmapPtr::~CPngBitmapPtr(void)
{
	CPngBitmapPtr::DestroyObject_Safe(m_pBitmap);
}

/////////////////////////////////////////////////////////////////////////////

HRESULT  CPngBitmapPtr::Attach(CPngBitmap* pBitmap)
{
	CPngBitmapPtr::DestroyObject_Safe(m_pBitmap);
	m_pBitmap = pBitmap;
	m_hResult = (m_pBitmap ? S_OK : OLE_E_BLANK);
	return m_hResult;
}

HRESULT  CPngBitmapPtr::Attach(Gdiplus::Bitmap* pBitmap)
{
	if (NULL == pBitmap)
		return E_INVALIDARG;
	CPngBitmapPtr::DestroyObject_Safe(m_pBitmap);
	m_hResult = CPngBitmapPtr::CreateObject(m_pBitmap);
	if (S_OK == m_hResult)
	{
		m_hResult = m_pBitmap->Attach(pBitmap);
	}
	return m_hResult;
}

HRESULT  CPngBitmapPtr::Create(const INT nWidth, const INT nHeight)
{
	CPngBitmapPtr::DestroyObject_Safe(m_pBitmap);
	return (m_hResult = CPngBitmapPtr::CreateObject(nWidth, nHeight, m_pBitmap));
}

HRESULT  CPngBitmapPtr::Create(const UINT resId, const HMODULE hResourceModule)
{
	TBitmap* pBitmap = NULL;
	HRESULT hr_ = CGdiPlusPngLoader::LoadResource(resId, hResourceModule, pBitmap);
	if (hr_ == S_OK)
		hr_ = this->Attach(pBitmap);
	return  hr_;
}

HRESULT  CPngBitmapPtr::Destroy(void)
{
	return CPngBitmapPtr::DestroyObject_Safe(m_pBitmap);
}

PBitmap* CPngBitmapPtr::Detach(void)
{
	CPngBitmap* pBitmap = m_pBitmap;
	m_pBitmap = NULL;
	m_hResult = OLE_E_BLANK;
	return pBitmap;
}

HRESULT  CPngBitmapPtr::GetLastResult(void) const
{
	return m_hResult;
}

PBitmap* CPngBitmapPtr::GetObject(void) const
{
	return m_pBitmap;
}

const
PBitmap& CPngBitmapPtr::GetObjectRef(void) const
{
	return (NULL != m_pBitmap ? *m_pBitmap : details::GetPngBitmapInvalidObject_Ref());
}

PBitmap& CPngBitmapPtr::GetObjectRef(void)
{
	return (NULL != m_pBitmap ? *m_pBitmap : details::GetPngBitmapInvalidObject_Ref());
}

bool     CPngBitmapPtr::IsValidObject(void) const
{
	return (m_pBitmap && m_pBitmap->IsValid());
}

/////////////////////////////////////////////////////////////////////////////

HRESULT  CPngBitmapPtr::CreateObject(CPngBitmap*& ptr_ref)
{
	if (ptr_ref)
		return HRESULT_FROM_WIN32(ERROR_OBJECT_ALREADY_EXISTS);
	try
	{
		ptr_ref = new CPngBitmap();
	}
	catch(::std::bad_alloc&) { ATLASSERT(0); return E_OUTOFMEMORY; }
	return S_OK;
}

HRESULT  CPngBitmapPtr::CreateObject(const INT nWidth, const INT nHeight, CPngBitmap*& ptr_ref)
{
	if (ptr_ref)
		return HRESULT_FROM_WIN32(ERROR_OBJECT_ALREADY_EXISTS);
	try
	{
		ptr_ref = new CPngBitmap(nWidth, nHeight);
	}
	catch(::std::bad_alloc&) { ATLASSERT(0); return E_OUTOFMEMORY; }
	return S_OK;
}

HRESULT  CPngBitmapPtr::DestroyObject_Safe(CPngBitmap*& ptr_ref)
{
	if (!ptr_ref)
		return S_FALSE;
	try
	{
		delete ptr_ref; ptr_ref = NULL;
	}
	catch(...){ ATLASSERT(0); return E_OUTOFMEMORY; }
	return S_OK;
}