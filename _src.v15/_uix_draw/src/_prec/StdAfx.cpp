/*
	Created by Tech_dog (ebontrop@gmail.com) on 5-Feb-2015 at 10:28:07pm, GMT+3, Taganrog, Thursday;
	This is UIX Draw library precompiled headers implementation file.
	-----------------------------------------------------------------------------
	Adopted to version v15 on 16-Feb-2021 at 5:43:32.162 am, UTC+7, Novosibirsk, Tuesday;
*/
#include "StdAfx.h"

#if (_ATL_VER < 0x0700)
#include <atlimpl.cpp>
#endif //(_ATL_VER < 0x0700)
