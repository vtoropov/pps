#ifndef __THEMESUPPORT_H__
#define __THEMESUPPORT_H__

#pragma once

#ifndef __ATLTHEME_H__
	#error ThemeSupport.h requires atltheme.h to be included first
#endif

#ifndef PACKVERSION
#	define PACKVERSION(major,minor) MAKELONG(minor,major)
#endif

static DWORD GetDllVersion(LPCTSTR lpszDllName)
{
	HINSTANCE hinstDll;
	DWORD dwVersion = 0;

	/* For security purposes, LoadLibrary should be provided with a 
	fully-qualified path to the DLL. The lpszDllName variable should be
	tested to ensure that it is a fully qualified path before it is used. */
	hinstDll = LoadLibrary(lpszDllName);

	if(hinstDll)
	{
		DLLGETVERSIONPROC pDllGetVersion;
		pDllGetVersion = (DLLGETVERSIONPROC)GetProcAddress(hinstDll, 
			"DllGetVersion");

		/* Because some DLLs might not implement this function, you
		must test for it explicitly. Depending on the particular 
		DLL, the lack of a DllGetVersion function can be a useful
		indicator of the version. */

		if(pDllGetVersion)
		{
			DLLVERSIONINFO dvi;
			HRESULT hr;

			ZeroMemory(&dvi, sizeof(dvi));
			dvi.cbSize = sizeof(dvi);

			hr = (*pDllGetVersion)(&dvi);

			if(SUCCEEDED(hr))
			{
				dwVersion = PACKVERSION(dvi.dwMajorVersion, dvi.dwMinorVersion);
			}
		}

		FreeLibrary(hinstDll);
	}
	return dwVersion;
}

static DWORD GetCommonCtrlDllVersion()
{
	static DWORD dwVersion = GetDllVersion(_T("comctl32.dll"));
	return dwVersion;
}

static BOOL _IsThemed()
{
	BOOL ret = FALSE;
//	OSVERSIONINFO ovi = {0};
//	ovi.dwOSVersionInfoSize = sizeof ovi;
//	GetVersionEx(&ovi);
//	if(ovi.dwMajorVersion==5 && ovi.dwMinorVersion==1)
	{
		//Windows XP detected
		typedef BOOL WINAPI ISAPPTHEMED();
		typedef BOOL WINAPI ISTHEMEACTIVE();
		ISAPPTHEMED* pISAPPTHEMED = NULL;
		ISTHEMEACTIVE* pISTHEMEACTIVE = NULL;
		HMODULE hMod = LoadLibrary(_T("uxtheme.dll"));
		if(hMod)
		{
			pISAPPTHEMED = reinterpret_cast<ISAPPTHEMED*>(
				GetProcAddress(hMod,"IsAppThemed"));
			pISTHEMEACTIVE = reinterpret_cast<ISTHEMEACTIVE*>(
				GetProcAddress(hMod,"IsThemeActive"));
			if(pISAPPTHEMED && pISTHEMEACTIVE)
			{
				if(pISAPPTHEMED() && pISTHEMEACTIVE())
				{
					typedef HRESULT CALLBACK DLLGETVERSION(DLLVERSIONINFO*);
					DLLGETVERSION* pDLLGETVERSION = NULL;

					HMODULE hModComCtl = LoadLibrary(_T("comctl32.dll"));
					if(hModComCtl)
					{
						pDLLGETVERSION = reinterpret_cast<DLLGETVERSION*>(
							GetProcAddress(hModComCtl,"DllGetVersion"));
						if(pDLLGETVERSION)
						{
							DLLVERSIONINFO dvi = {0};
							dvi.cbSize = sizeof dvi;
							if(pDLLGETVERSION(&dvi) == NOERROR )
							{
								ret = dvi.dwMajorVersion >= 6;
							}
						}
						FreeLibrary(hModComCtl);
					}
				}
			}
			FreeLibrary(hMod);
		}
	}
	return ret;
}

static BOOL IsThemeEnabled()
{
	//return CTheme::IsThemingSupported() && GetCommonCtrlDllVersion() >= PACKVERSION(6,00) && IsThemeActive();
	return CTheme::IsThemingSupported() && _IsThemed();
}

#endif