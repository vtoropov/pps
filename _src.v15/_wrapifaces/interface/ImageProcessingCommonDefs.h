#ifndef _SHAREDABSTRACTIMAGEPROCESSINGCOMMOBDEFS_H_4DF124CC_C51C_4219_B376_9DA68BF50DC1_INCLUDED
#define _SHAREDABSTRACTIMAGEPROCESSINGCOMMOBDEFS_H_4DF124CC_C51C_4219_B376_9DA68BF50DC1_INCLUDED
/*
	Created by Tech_dog (ebontrop@gmail.com) on 18-Mar-2014 at 10:45:22pm, GMT+4, Taganrog, Tuesday;
	This is Shared Abstract Image Processing Common Definitions file.
	-----------------------------------------------------------------------------
	Adopted to version v15 on 15-Feb-2021 at 5:05:53.586 am, UTC+7, Novosibirsk, Monday;
*/
#include "Shared_SystemError.h"
#include "Shared_RawData.h"

namespace shared { namespace recognition
{
	using shared::lite::data::T_RAW_DATA;
	using shared::lite::common::CSysError;

	typedef ::ATL::CComSafeArray<LONG>  T_RAW_USERS;
	typedef ::ATL::CComSafeArray<LONG>  T_RAW_UUIDS;

	HRESULT   ConvertDeviceResultToHresult(const INT);

	enum eProcessorType
	{
		eNone      = 0,
		eHibio     = 2,
	};

	enum eProcessorPropertyType
	{
		eUnknown          = 0,
		eDeviceIdentifier = 1,
	};

	interface IError
	{
		virtual INT               GetDeviceCode(void)        const = 0;
		virtual LPCWSTR           GetDescription(void)       const = 0;
		virtual HRESULT           GetHresult(void)           const = 0;
		virtual bool              IsFailure(void)            const = 0;
	};

	interface IInterruptProcess
	{
		virtual HRESULT           CanContinue(void) = 0;
	};

	interface IImageRawData
	{
		virtual HRESULT           Clear(void)                      = 0;
		virtual HRESULT           DetachTo(IImageRawData&)         = 0;   // moves data to object provided
		virtual INT_PTR           GetData(void)              const = 0;
		virtual const IError&     GetLastError_Ref(void)     const = 0;
		virtual eProcessorType    GetProcessorType(void)     const = 0;
		virtual const T_RAW_DATA& GetRawData_Ref(void)       const = 0;
		virtual T_RAW_DATA&       GetRawData_Ref(void)             = 0;
		virtual HRESULT           SetData(const INT_PTR  pData, const INT nSize) = 0;
		virtual HRESULT           SetRawData(const T_RAW_DATA&)    = 0;
	};

	class eImageProcessorModule
	{
	public:
		enum _enum {
			eNone           = 0,
			eProcessor      = 1,
			eInitializer    = 2,
			eMessageSystem  = 3,
			eEventModule    = 4,
			eImageStorage   = 6,
		};
	}; typedef eImageProcessorModule::_enum TImgProcModule;

	interface IInitializer;
	interface IMessageSystem;
	interface IImageProcessorEvents;
	interface IImageStorage;
	interface IImageProcessor;

	class CDeviceError : public IError, public CSysError
	{
	protected:
		INT               m_dev_code;                // device specific code
	public:
		 CDeviceError(void);
		 CDeviceError(LPCWSTR pError, const INT nDevCode = ERROR_SUCCESS, const HRESULT = OLE_E_BLANK);
		~CDeviceError(void);
	public: // IError
		virtual INT       GetDeviceCode(void)  const override;
		virtual LPCWSTR   GetDescription(void) const override;
		virtual HRESULT   GetHresult(void)     const override;
		virtual bool      IsFailure(void)      const override;
	public:
		void              SetDevCode(const INT);
	};

	class CFailedModule
	{
	private:
		TImgProcModule    m_module;
		CDeviceError      m_error;
	public:
		 CFailedModule(void);
		~CFailedModule(void);
	public:
		void              Clear(void);
		CONST
		CDeviceError&     Error(void)  const;
		CDeviceError&     Error(void)       ;
		TImgProcModule    Module(void) const;
		void              Module(const TImgProcModule);
		CStringW          Name(void)   const;
		void              Set(const IError&, const TImgProcModule);
		void              Set(const IImageProcessor&);
		void              Set(const IImageProcessorEvents&);
		void              Set(const IImageStorage&);
		void              Set(const IInitializer&);
		void              Set(const IMessageSystem&);
		CStringW          ToString(void)const;
		void              Update(const IImageProcessor&);
	};
}}

#endif/*_SHAREDABSTRACTIMAGEPROCESSINGCOMMOBDEFS_H_4DF124CC_C51C_4219_B376_9DA68BF50DC1_INCLUDED*/