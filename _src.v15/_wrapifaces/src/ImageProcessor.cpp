/*
	Created by Tech_dog (ebontrop@gmail.com) on 22-Mar-2014 at 9:51:11am, GMT+4, Moscow Region,
	Rail Road Train #43, Coatch #6, Place #1, Saturday;
	This is Shared Abstract Image Processor class implementation file.
	-----------------------------------------------------------------------------
	Adopted to version v15 on 15-Feb-2021 at 5:12:04.407 am, UTC+7, Novosibirsk, Monday;
*/
#include "StdAfx.h"
#include "ImageProcessor.h"
#include "Hit_ClientWrap.h"

using namespace shared;
using namespace shared::recognition;

/////////////////////////////////////////////////////////////////////////////

namespace shared { namespace recognition { namespace details
{
	static shared::recognition::eProcessorType& ImageProcessor_GetCurrentType_Ref(void)
	{
		static shared::recognition::eProcessorType proc_type;
		return proc_type;
	}
}}}

/////////////////////////////////////////////////////////////////////////////

namespace shared { namespace recognition
{
	INT GetResultCode(const eResultCodeType::_enum eCodeType)
	{
		INT nCode = (INT)(DWORD)0xffffffff;
		const shared::recognition::eProcessorType proc_type = details::ImageProcessor_GetCurrentType_Ref();
		switch (proc_type)
		{
		case eHibio:
			{
				nCode = shared::recognition::client::Hitachi::GetResultCode(eCodeType);
			} break;
		}
		return nCode;
	}

	INT GetCancelResultCode(void)
	{
		return GetResultCode(eResultCodeType::eCancel);
	}

	INT GetSuccessResultCode(void)
	{
		return GetResultCode(eResultCodeType::eSuccess);
	}

	INT GetTimeoutResultCode(void)
	{
		return GetResultCode(eResultCodeType::eTimeout);
	}

	HRESULT   CreateInitializer(const eProcessorType eType, IInitializer*& ptr_ref)
	{
		TRACE_FUNC();

		if (ptr_ref)
			return HRESULT_FROM_WIN32(ERROR_OBJECT_ALREADY_EXISTS);
		HRESULT hr__ = S_OK;
		switch (eType)
		{
		case eHibio:
			{
				TRACE_INFO(_T("Trying to create Hitachi initializer object..."));
				hr__ = shared::recognition::client::Hitachi::CreateInitializer(ptr_ref);
				if (S_OK == hr__)
				{
					TRACE_INFO(_T("The Hitachi initializer object has been created."));
				}
				else
				{
					TRACE_ERR__a1(_T("The Hitachi initializer object is not created: code=0x%x"), hr__);
				}
			} break;
		default:
			ATLASSERT(FALSE==TRUE);
			hr__ = DISP_E_TYPEMISMATCH;
			TRACE_ERROR(_T("The initializer object of the wrong type cannot be created"));
		}
		details::ImageProcessor_GetCurrentType_Ref() = eType;
		return  hr__;
	}

	HRESULT   DestroyInitializer_Safe(IInitializer*& ptr_ref)
	{
		TRACE_FUNC();

		if (!ptr_ref)
			return S_FALSE;
		HRESULT hr__ = S_OK;
		const shared::recognition::eProcessorType eProcType = (*ptr_ref).GetProcessorType();
		switch (eProcType)
		{
		case eHibio:
			{
				TRACE_INFO(_T("The Hitachi initializer object is being destroyed"));
				hr__ = shared::recognition::client::Hitachi::DestroyInitializer_Safe(ptr_ref);
			} break;
		default:
			hr__ = DISP_E_TYPEMISMATCH;
			TRACE_ERROR(_T("The initializer object is of the wrong type"));
		}
		return hr__;
	}

}}

/////////////////////////////////////////////////////////////////////////////

namespace shared { namespace recognition { namespace details
{
	class  CErrorStub:
		public shared::recognition::IError
	{
	public:
		CErrorStub(void)
		{
		}
		~CErrorStub(void)
		{
		}
	public:
		virtual INT      GetDeviceCode(void) const override sealed
		{
			return 0xffffffff;
		}
		virtual LPCWSTR  GetDescription(void) const override
		{
			static LPCWSTR pErrorDesc = _T("The image processor initializer object is not created");
			return pErrorDesc;
		}
		virtual HRESULT  GetHresult(void) const override sealed
		{
			return OLE_E_BLANK;
		}
		virtual bool     IsFailure(void) const override sealed
		{
			return true;
		}
	};

	class  CDeviceIdentifierEnumeratorStub:
		public IDeviceIdentifierEnumerator
	{
	private:
		CErrorStub  m_error_stub;
	public:
		CDeviceIdentifierEnumeratorStub(void)
		{
		}
		~CDeviceIdentifierEnumeratorStub(void)
		{
		}
	public:
		virtual LPCWSTR        GetBSPOf(const INT nIndex) const override sealed
		{
			nIndex;
			return NULL;
		}
		virtual INT            GetCount(void) const override sealed
		{
			return 0;
		}
		virtual LPCWSTR        GetDefault(void) const override sealed
		{
			return NULL;
		}
		virtual const IError&  GetLastError_Ref(void) const override sealed
		{
			return m_error_stub;
		}
		virtual HRESULT        Initialize(void) override sealed
		{
			return E_NOTIMPL;
		}
	private:
		CDeviceIdentifierEnumeratorStub(const CDeviceIdentifierEnumeratorStub&);
		CDeviceIdentifierEnumeratorStub& operator= (const CDeviceIdentifierEnumeratorStub&);
	};

	class  CInitializerStub:
		public shared::recognition::IInitializer
	{
	private:
		CDeviceIdentifierEnumeratorStub  m_ids_stub;
	public:
		CInitializerStub(void)
		{
		}
		~CInitializerStub(void)
		{
		}
	public:
		virtual const IDeviceIdentifierEnumerator&    GetDeviceIdentifierEnum_Ref(void) const override sealed
		{
			return m_ids_stub;
		}
		virtual INT_PTR        GetHandle(void) const override sealed
		{
			return NULL;
		}
		virtual const IError&  GetLastError_Ref(void) const override sealed
		{
			return m_ids_stub.GetLastError_Ref();
		}
		virtual eProcessorType GetProcessorType(void) const override sealed
		{
			return eNone;
		}
		virtual HRESULT        Initialize(void) override sealed
		{
			return E_NOTIMPL;
		}
		virtual bool           IsInitialized(void) const override sealed
		{
			return false;
		}
		virtual HRESULT        Terminate(void) override sealed
		{
			return E_NOTIMPL;
		}
	private:
		CInitializerStub(const CInitializerStub&);
		CInitializerStub& operator= (const CInitializerStub&);
	};

	static CInitializerStub&   GetInitializerInvalidObject_Ref(void)
	{
		static CInitializerStub  m_invalid_ref;
		return m_invalid_ref;
	}
}}}

namespace shared { namespace recognition { namespace details
{
	class  CErrorProcStub:
		public CErrorStub
	{
	public:
		virtual LPCWSTR  GetDescription(void) const override
		{
			static LPCWSTR pErrorDesc = _T("The image processor object is not created");
			return pErrorDesc;
		}
	};

	class  CImageProcessorStub:
		public shared::recognition::IImageProcessor
	{
	private:
		CErrorProcStub   m_error;
		_variant_t       m_empty_data;
	public:
		CImageProcessorStub(void)
		{
		}
	public:
		virtual HRESULT        Cancel(void)                            override sealed { return E_NOTIMPL;}
		virtual HRESULT        CreateEnrollmentTemplate(_variant_t&)   override sealed { return E_NOTIMPL;}
		virtual HRESULT        CreateVerificationTemplate(_variant_t&) override sealed { return E_NOTIMPL;}
		virtual HRESULT        Enroll(_variant_t&)                     override sealed { return E_NOTIMPL;}
		virtual const IError&  GetLastError_Ref(void)            const override sealed { return m_error;  }
		virtual eProcessorType GetProcessorType(void)            const override sealed { return eNone;    }
		virtual HRESULT        IsAlive(void)                     const override sealed { return E_NOTIMPL;}
		virtual HRESULT        VerifyMatch(const _variant_t&, const _variant_t&)override sealed { return E_NOTIMPL;}
	};

	static CImageProcessorStub& GetImageProcInvalidObject_Ref(void)
	{
		static CImageProcessorStub m_invalid_ref;
		return m_invalid_ref;
	}
}}}

/////////////////////////////////////////////////////////////////////////////

CInitializerPtr::CInitializerPtr(const eProcessorType eType):
	m_pInitializer(NULL),
	m_hResult(OLE_E_BLANK)
{
	TRACE_FUNC();
	m_hResult = shared::recognition::CreateInitializer(eType, m_pInitializer);
}

CInitializerPtr::~CInitializerPtr(void)
{
	TRACE_FUNC();
	if (m_pInitializer)
	{
		m_hResult = shared::recognition::DestroyInitializer_Safe(m_pInitializer);
	}
	m_hResult = OLE_E_BLANK;
}

/////////////////////////////////////////////////////////////////////////////

HRESULT             CInitializerPtr::GetLastResult(void) const
{
	return m_hResult;
}

const IInitializer& CInitializerPtr::GetObject_Ref(void) const
{
	return (NULL == m_pInitializer ? details::GetInitializerInvalidObject_Ref() : *m_pInitializer);
}

IInitializer&       CInitializerPtr::GetObject_Ref(void)
{
	return (NULL == m_pInitializer ? details::GetInitializerInvalidObject_Ref() : *m_pInitializer);
}

bool                CInitializerPtr::IsObjectValid(void) const
{
	return (S_OK == m_hResult && NULL !=m_pInitializer);
}

/////////////////////////////////////////////////////////////////////////////

namespace shared { namespace recognition
{
	HRESULT  CreateImageProcessor(const IInitializer& init_ref, IImageProcessor*& ptr_ref)
	{
		TRACE_FUNC();

		if (ptr_ref)
			return HRESULT_FROM_WIN32(ERROR_OBJECT_ALREADY_EXISTS);
		HRESULT hr__ = S_OK;
		switch (init_ref.GetProcessorType())
		{
		case eHibio:
			{
				hr__ = shared::recognition::client::Hitachi::CreateImageProcessor(init_ref, ptr_ref);
			} break;
		default:
			ATLASSERT(FALSE==TRUE);
			hr__ = DISP_E_TYPEMISMATCH;
		}
		return  hr__;
	}

	HRESULT  DestroyImageProcessor_Safe(IImageProcessor*& ptr_ref)
	{
		if (!ptr_ref)
			return S_FALSE;
		HRESULT hr__ = S_OK;
		const shared::recognition::eProcessorType eProcType = (*ptr_ref).GetProcessorType();
		switch (eProcType)
		{
		case eHibio:
			{
				hr__ = shared::recognition::client::Hitachi::DestroyImageProcessor_Safe(ptr_ref);
			} break;
		default:
			hr__ = DISP_E_TYPEMISMATCH;
			ATLASSERT(FALSE==TRUE);
		}
		return hr__;
	}
}}

/////////////////////////////////////////////////////////////////////////////

CImageProcessorPtr::CImageProcessorPtr(const IInitializer& init_ref):
	m_pImageProc(NULL),
	m_hResult(OLE_E_BLANK)
{
	m_hResult = shared::recognition::CreateImageProcessor(init_ref, m_pImageProc);
}

CImageProcessorPtr::~CImageProcessorPtr(void)
{
	if (m_pImageProc)
	{
		m_hResult = shared::recognition::DestroyImageProcessor_Safe(m_pImageProc);
	}
	m_hResult = OLE_E_BLANK;
}

/////////////////////////////////////////////////////////////////////////////

HRESULT                CImageProcessorPtr::GetLastResult(void) const
{
	return m_hResult;
}

const IImageProcessor& CImageProcessorPtr::GetObject_Ref(void) const
{
	return (NULL == m_pImageProc ? details::GetImageProcInvalidObject_Ref() : *m_pImageProc);
}

IImageProcessor&       CImageProcessorPtr::GetObject_Ref(void)
{
	return (NULL == m_pImageProc ? details::GetImageProcInvalidObject_Ref() : *m_pImageProc);
}

bool                   CImageProcessorPtr::IsObjectValid(void) const
{
	return false;
}