/*
	Created by Tech_dog (ebontrop@gmail.com) on 25-Mar-2014 at 4:25:12pm, GMT+4 Saint-Petersburg, Tuesday;
	This is Shared Lite Generic Data Provider class implementation file.
	-----------------------------------------------------------------------------
	Adopted to VS15 on 13-Feb-2021 at 1:21:19.572 pm, UTC+7, Novosibirsk, Saturday;
*/
#include "StdAfx.h"
#include "Shared_GenericDataProvider.h"

using namespace shared;
using namespace shared::lite;
using namespace shared::lite::data;

/////////////////////////////////////////////////////////////////////////////

namespace shared { namespace lite { namespace data { namespace details
{
	static const _variant_t& DataRecord_InvalidDataRef(void)
	{
		static _variant_t v_na;
		return v_na;
	}

	using shared::lite::data::CDataRecord;

	static const CDataRecord& DataSet_InvalidRecordRef(void)
	{
		static CDataRecord rec_;
		return rec_;
	}
}}}}

/////////////////////////////////////////////////////////////////////////////

CDataRecord::CDataRecord(void)
{
}

CDataRecord::~CDataRecord(void)
{
	Clear();
}

/////////////////////////////////////////////////////////////////////////////

HRESULT      CDataRecord::Clear(void)
{
	if (IsEmpty())
		return S_FALSE;
	else
	{
		m_data.clear();
		return S_OK;
	}
}

_variant_t   CDataRecord::Data(LPCWSTR pFieldName) const
{
	CStringW cs_name(pFieldName);
	TDataRow::const_iterator it__ = m_data.find(cs_name);
	if (it__ == m_data.end())
		return _variant_t();
	else
		return it__->second;
}

_variant_t*  CDataRecord::DataPtr(LPCWSTR pFieldName)
{
	CStringW cs_name(pFieldName);
	TDataRow::iterator it__ = m_data.find(cs_name);
	if (it__ == m_data.end())
		return NULL;
	else
		return &it__->second;
}

const _variant_t&  CDataRecord::DataRef(LPCWSTR pFieldName) const
{
	CStringW cs_name(pFieldName);
	TDataRow::const_iterator it__ = m_data.find(cs_name);
	if (it__ == m_data.end())
		return details::DataRecord_InvalidDataRef();
	else
		return it__->second;
}

HRESULT      CDataRecord::Empty(void)
{
	for (TDataRow::iterator it__ = m_data.begin(); it__!=m_data.end(); ++it__)
	{
		_variant_t& v_ref = it__->second;
		v_ref.Clear();
	}
	return S_OK;
}

HRESULT      CDataRecord::Insert(LPCWSTR pFieldName, const _variant_t& vData)
{
	CStringW cs_name(pFieldName);
	if (cs_name.IsEmpty())
	{
		return E_INVALIDARG;
	}
	TDataRow::iterator it__ = m_data.find(cs_name);
	if (it__ == m_data.end())
	{
		try
		{
			m_data.insert(::std::make_pair(cs_name, vData));
		}
		catch(::std::bad_alloc&) { ATLASSERT(FALSE); return E_OUTOFMEMORY; }
	}
	else
	{
		it__->second = vData;
	}
	return S_OK;
}

bool         CDataRecord::IsEmpty(void) const
{
	return m_data.empty();
}

/////////////////////////////////////////////////////////////////////////////

CDataSet::CDataSet(void) : m_current(m_records.end())
{
}

CDataSet::~CDataSet(void)
{
	this->Clear();
}

/////////////////////////////////////////////////////////////////////////////

HRESULT      CDataSet::Add(LPCWSTR pKey, CDataRecord*& _in_out_ptr)
{
	if (!pKey || ::lstrlenW(pKey) < 1)
		return E_INVALIDARG;
	m_current = m_records.end();
	CStringW cs_key(pKey);
	CDataRecord empty_record;
	try
	{
		m_records[cs_key] = empty_record;
	}
	catch(::std::bad_alloc&) { ATLASSERT(FALSE); return E_OUTOFMEMORY; }
	TOneKeySet::iterator it_ = m_records.find(cs_key);

	_in_out_ptr = (it_ == m_records.end() ? NULL : &it_->second);

	return S_OK;
}

HRESULT      CDataSet::Clear(void)
{
	if (m_records.empty())
		return S_FALSE;
	m_current = m_records.end();
	m_records.clear();
	return S_OK;
}

LONG         CDataSet::Count(void)const
{
	return (LONG)m_records.size();
}

const CDataRecord* CDataSet::First(void)const
{
	return (m_records.empty() ? NULL : &(m_current = m_records.begin())->second);
}

HRESULT      CDataSet::Insert(LPCWSTR pKey, const CDataRecord& rec_ref)
{
	if (!pKey || ::lstrlenW(pKey) < 1)
		return E_INVALIDARG;
	m_current = m_records.end();
	CStringW cs_key(pKey);
	try
	{
		m_records[cs_key] = rec_ref;
	}
	catch(::std::bad_alloc&) { ATLASSERT(FALSE); return E_OUTOFMEMORY; }
	return S_OK;
}

const CDataRecord* CDataSet::Next(void)const
{
	if (m_current == m_records.end())
		return NULL;
	m_current++;
	if (m_current == m_records.end())
	{
		return NULL;
	}
	else
		return &m_current->second;
}

HRESULT      CDataSet::Remove(LPCWSTR pKey)
{
	CStringW cs_key(pKey);
	if (cs_key.IsEmpty())
		return E_INVALIDARG;
	TOneKeySet::iterator it_ = m_records.find(cs_key);
	if (it_ == m_records.end())
		return S_FALSE;
	m_current = m_records.end();
	m_records.erase(it_);
	return S_OK;
}

const CDataRecord& CDataSet::Record(LPCWSTR pKey) const
{
	CStringW cs_key(pKey);
	TOneKeySet::const_iterator it_ = m_records.find(cs_key);
	return (it_ == m_records.end() ? details::DataSet_InvalidRecordRef() : it_->second);
}


CDataRecord*       CDataSet::Record(LPCWSTR pKey)
{
	CStringW cs_key(pKey);
	TOneKeySet::iterator it_ = m_records.find(cs_key);
	return (it_ == m_records.end() ? NULL : &it_->second);
}