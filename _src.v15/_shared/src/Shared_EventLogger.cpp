/*
	Created by Tech_dog (ebontrop@gmail.com) on 19-Mar-2014 at 8:12:09am, GMT+4, Taganrog, Wednesday;
	This is Shared Lite Event Logger class implementation file.
	-----------------------------------------------------------------------------
	Adopted to VS15 on 15-Feb-2021 at 4:19:57.014 am, UTC+7, Novosibirsk, Monday;
*/
#include "StdAfx.h"
#include "Shared_EventLogger.h"
#include "Shared_GenericAppObject.h"
#include "Shared_PersistentStorage.h"

using namespace shared;
using namespace shared::lite;
using namespace shared::lite::common;
using namespace shared::lite::log;
using namespace shared::lite::runnable;
using namespace shared::lite::persistent;

/////////////////////////////////////////////////////////////////////////////

extern CApplication& Global_GetAppObjectRef(void);

namespace shared { namespace lite { namespace log
{
	static LPCWSTR  EventLogger_ApplicationProfileName = _T("PlatinumClient.ini");

	const CEventLogger&  Logger(void)
	{
		static DWORD dwOptions   = eEventLoggerOption::eNone;
		static bool bInitialized = false;
		if (bInitialized == false)
		{
			bInitialized = true;
			const CApplication& the_app = Global_GetAppObjectRef();
			CStringW cs_file;
			the_app.GetPathFromAppFolder(_T(".\\"), cs_file);
			cs_file += EventLogger_ApplicationProfileName;
			CPrivateProfile profile_(cs_file);
			if (S_OK == profile_.GetLastResult())
			{
				const CPrivateProfileSection& section_ = profile_.SectionOf(_T("Logs"));
				if (section_.IsValid())
				{
					const CPrivateProfileItem& log_off = section_.ItemOf(_T("Tracking"));
					if (log_off.IsValid())
					{
						const bool b_log_off = (0 == log_off.Value().CompareNoCase(_T("OFF")));
						dwOptions |= (b_log_off ? eEventLoggerOption::eLoggingIsOff : 0);
					}
					const CPrivateProfileItem& use_one_file = section_.ItemOf(_T("UseSingleFile"));
					if (use_one_file.IsValid())
					{
						const bool b_use = (0 == use_one_file.Value().CompareNoCase(_T("true")));
						dwOptions |= (b_use ? eEventLoggerOption::eUseSingleFile : 0);
					}
					const CPrivateProfileItem& overwrite_file = section_.ItemOf(_T("OverwriteFile"));
					if (overwrite_file.IsValid())
					{
						const bool b_use = (0 == overwrite_file.Value().CompareNoCase(_T("true")));
						dwOptions |= (b_use ? eEventLoggerOption::eOverwriteFile : 0);
					}
					const CPrivateProfileItem& errors_only = section_.ItemOf(_T("ErrorsOnly"));
					if (errors_only.IsValid())
					{
						const bool b_errors = (0 == errors_only.Value().CompareNoCase(_T("true")));
						dwOptions |= (b_errors ? eEventLoggerOption::eLogErrorsOnly : 0);
					}
				}
			}
		}
		static CEventLogger logger(eEventPersistance::eFile, dwOptions);
		return logger;
	}
}}}

namespace shared { namespace lite { namespace log { namespace details
{
	using shared::lite::runnable::CGenericSyncObject;

	static CGenericSyncObject& EventLogger_SafeGardRef(void)
	{
		static CGenericSyncObject cs__;
		return cs__;
	}
}}}}

#define EVT_LOG_SAFE__LOCK() SAFE_LOCK(details::EventLogger_SafeGardRef());

namespace shared { namespace lite { namespace log { namespace details
{
	using shared::lite::log::eEventLoggerOption;

	static HRESULT  EventLogger_GetFileName(const DWORD dwOptions, CStringW& __in_out_ref)
	{
		static const INT nSize = _MAX_PATH + _MAX_DRIVE + _MAX_DIR;
		TCHAR path__[nSize] = {0};
		const BOOL ret__ = ::GetModuleFileName(NULL, path__, _countof(path__));
		if (FALSE == ret__)
			return HRESULT_FROM_WIN32(::GetLastError());
		TCHAR file_part__[_MAX_FNAME] = {0};
		_tsplitpath_s(path__, NULL, 0, NULL, 0, file_part__, _MAX_FNAME, NULL, 0);
		if (0 == (eEventLoggerOption::eUseSingleFile & dwOptions))
		{
			SYSTEMTIME st = {0};
			::GetLocalTime(&st);
			CStringW file__;
			file__.Format(_T("%s_%04d_%02d_%02d_%02d_%02d_%02d_%04d.log"), 
				file_part__, 
				st.wYear,
				st.wMonth,
				st.wDay,
				st.wHour,
				st.wMinute,
				st.wSecond,
				st.wMilliseconds);
			__in_out_ref = file__;
		}
		else
		{
			CStringW file__;
			file__.Format(_T("%s.log"), 
				file_part__);
			__in_out_ref = file__;
		}
		return S_OK;
	}
	static HRESULT  EventLogger_GetFolderName(CStringW& __in_out_ref)
	{
		static const INT nSize = _MAX_PATH + _MAX_DRIVE + _MAX_DIR;
		TCHAR path__[nSize] = {0};
		const BOOL ret__ = ::GetModuleFileName(NULL, path__, _countof(path__));
		if (FALSE == ret__)
			return HRESULT_FROM_WIN32(::GetLastError());
		CStringW folder__(path__);
		INT ptr__ = folder__.ReverseFind(_T('\\'));
		if (-1 == ptr__)
			return E_UNEXPECTED;
		folder__ = folder__.Left(ptr__ + 1);
		folder__+= _T("logs\\");
		if (!::PathFileExists(folder__))
		{
			if (NULL == ::CreateDirectory(folder__, NULL))
			{
				const DWORD dwError = ::GetLastError();
				if (ERROR_ALREADY_EXISTS != dwError)
					return HRESULT_FROM_WIN32(dwError);
			}
		}
		__in_out_ref = folder__;
		return S_OK;
	}

	class  EventLogger_Indentation
	{
		typedef ::std::map<DWORD, INT>  TIndentationMap;
		typedef ::std::map<DWORD, bool> TLockMap;
	private:
		TIndentationMap   m__indents;
		TLockMap          m__locks;
		DWORD             m__main_thread;
	public:
		EventLogger_Indentation(void): m__main_thread(::GetCurrentThreadId())
		{
			// it is assumed that the first call to the logger is done from the main thread, for example, from the main function
		}
		~EventLogger_Indentation(void)
		{
			m__indents.clear(); m__locks.clear();
		}
	public:
		HRESULT      Current(const INT nIndent)
		{
			EVT_LOG_SAFE__LOCK();
			TIndentationMap::iterator it__ = m__indents.find(::GetCurrentThreadId());
			if (it__ != m__indents.end())
			{
				it__->second = nIndent;
				return S_OK;
			}
			else if (!__create_indent_on_demand(nIndent))
				return E_OUTOFMEMORY;
			else
				return S_OK;
		}
		INT          Current(void)const
		{
			EVT_LOG_SAFE__LOCK();
			if (this->IsLocked())
				return 0;
			TIndentationMap::const_iterator it__ = m__indents.find(::GetCurrentThreadId());
			if (it__ != m__indents.end())
				return it__->second;
			else
				return 0;
		}
		INT          Decrease(void)
		{
			EVT_LOG_SAFE__LOCK();
			TIndentationMap::iterator it__ = m__indents.find(::GetCurrentThreadId());
			if (it__ != m__indents.end())
			{
				if (it__->second > 0) it__->second--;
				return it__->second;
			}
			else
				return 0;
		}
		bool         Increase(void)
		{
			EVT_LOG_SAFE__LOCK();
			TIndentationMap::iterator it__ = m__indents.find(::GetCurrentThreadId());
			if (it__ != m__indents.end())
			{
				it__->second++;
				return true;
			}
			else
				return __create_indent_on_demand(1);
		}
		bool         IsCtxAvailable(void)const
		{
			EVT_LOG_SAFE__LOCK();
			TIndentationMap::const_iterator it__ = m__indents.find(::GetCurrentThreadId());
			return (it__ != m__indents.end());
		}
		bool         IsMainThreadCtx(void)const
		{
			return (::GetCurrentThreadId() == m__main_thread);
		}
		bool         IsLocked(void)const
		{
			EVT_LOG_SAFE__LOCK();
			TLockMap::const_iterator it__ = m__locks.find(::GetCurrentThreadId());
			if (it__ != m__locks.end())
				return it__->second;
			else
				return false;
		}
		bool         Lock(void)    //disallows to use indentation
		{
			EVT_LOG_SAFE__LOCK();
			TLockMap::iterator it__ = m__locks.find(::GetCurrentThreadId());
			if (it__ != m__locks.end())
				return (it__->second = true);
			else
				return __create_lock_on_demand();
		}
		bool         Remove(void)
		{
			EVT_LOG_SAFE__LOCK();
			{
				TIndentationMap::iterator it__ = m__indents.find(::GetCurrentThreadId());
				if (it__!=m__indents.end())
					m__indents.erase(it__);
			}
			{
				TLockMap::iterator it__ = m__locks.find(::GetCurrentThreadId());
				if (it__!=m__locks.end())
					m__locks.erase(it__);
			}
			return true;
		}
		INT          Replicate(void)const // gets the current indent from main thread
		{
			EVT_LOG_SAFE__LOCK();
			TIndentationMap::const_iterator it__ = m__indents.find(m__main_thread);
			if (it__ != m__indents.end())
				return it__->second;
			else
				return 0;
		}
		bool         Unlock(void)  // allows to use indentation
		{
			EVT_LOG_SAFE__LOCK();
			TLockMap::iterator it__ = m__locks.find(::GetCurrentThreadId());
			if (it__ != m__locks.end())
			{
				it__->second = false;
				return true;
			}
			else
				return false; // there is no lock
		}
		LPCWSTR      ToString(void)const
		{
			EVT_LOG_SAFE__LOCK();
			const INT n__ident = this->Current();
			switch (n__ident)
			{
			case  0:  return _T("");
			case  1:  return _T("  ");
			case  2:  return _T("    ");
			case  3:  return _T("      ");
			case  4:  return _T("        ");
			case  5:  return _T("          ");
			case  6:  return _T("            ");
			case  7:  return _T("              ");
			case  8:  return _T("                ");
			case  9:  return _T("                  ");
			case 10:  return _T("                    ");
			case 11:  return _T("                      ");
			case 12:  return _T("                        ");
			case 13:  return _T("                          ");
			case 14:  return _T("                            ");
			case 15:  return _T("                              ");
			case 16:  return _T("                                ");
			case 17:  return _T("                                  ");
			case 18:  return _T("                                    ");
			}
			return _T("  ");
		}
	private:
		bool       __create_indent_on_demand(const INT nValue = 0)
		{
			try
			{
				m__indents.insert(::std::make_pair(::GetCurrentThreadId(), nValue));
			}
			catch (::std::bad_alloc&){ ATLASSERT(FALSE); return false; }
			return true;
		}
		bool       __create_lock_on_demand(void)
		{
			try
			{
				m__locks.insert(::std::make_pair(::GetCurrentThreadId(), true));
			}
			catch (::std::bad_alloc&){ ATLASSERT(FALSE); return false; }
			return true;
		}
	private:
		EventLogger_Indentation(const EventLogger_Indentation&);
		EventLogger_Indentation& operator= (const EventLogger_Indentation&);
	};

	static EventLogger_Indentation&   EventLogger_IndentationObject(void)
	{
		static EventLogger_Indentation obj__;
		return obj__;
	}
}}}}

/////////////////////////////////////////////////////////////////////////////

CEventLogger::CEventLogger(const eEventPersistance::_enum pers__, const DWORD dwOptions) : m_file(NULL), m_pers_state(pers__), m_dwOptions(dwOptions)
{
	m_log_error.Reset();
	if (0 != (eEventLoggerOption::eLoggingIsOff & m_dwOptions))
		return;
	CStringW log_file__;
	{
		m_log_error = details::EventLogger_GetFolderName(log_file__); ATLASSERT(!m_log_error);
		if (!m_log_error)
		{
			CStringW log_name__;
			m_log_error = details::EventLogger_GetFileName(m_dwOptions, log_name__); ATLASSERT(!m_log_error);
			if (!m_log_error)
			{
				log_file__ += log_name__;
			}
		}
	}
	if (!m_log_error)
	{
		const bool bOverwrite = (0 != (eEventLoggerOption::eOverwriteFile & m_dwOptions));
		errno_t err__ = _tfopen_s(&m_file, log_file__.GetString(), (bOverwrite ? _T("w") : _T("a"))); ATLASSERT(NULL != m_file);
		if (NULL == m_file || err__)
			m_log_error = HRESULT_FROM_WIN32(ERROR_ACCESS_DENIED);
	}
}

CEventLogger::~CEventLogger(void)
{
	if (NULL != m_file)
	{
		fclose(m_file); m_file = NULL;
	}
}

/////////////////////////////////////////////////////////////////////////////

const CSysError& CEventLogger::Error(VOID)const
{
	return m_log_error;
}

bool       CEventLogger::IsTurnedOn(void)const
{
	return (0 == (eEventLoggerOption::eLoggingIsOff & m_dwOptions));
}

HRESULT    CEventLogger::ToEvent(LPCWSTR pMessage, const eEventType::_enum type__) const
{
	SAFE_LOCK(m_sync_obj);
	if (NULL == pMessage)
		return E_INVALIDARG;

	shared::lite::common::CApplication the__app;

	HANDLE hEventSource = ::RegisterEventSource(NULL, the__app.GetName());
	if (NULL == hEventSource)
		return HRESULT_FROM_WIN32(::GetLastError());
	LPCWSTR lpszStrings[2] = {the__app.GetName(), pMessage};
	WORD evt_type__ = EVENTLOG_SUCCESS;
	switch (type__)
	{
	case eEventType::eError:   evt_type__ = EVENTLOG_ERROR_TYPE;       break;
	case eEventType::eInfo:    evt_type__ = EVENTLOG_INFORMATION_TYPE; break;
	case eEventType::eWarning: evt_type__ = EVENTLOG_WARNING_TYPE;     break;
	}
	const BOOL ret__ = ::ReportEvent(hEventSource, evt_type__, 0, 0, NULL, _countof(lpszStrings), 0, lpszStrings, NULL);
	::DeregisterEventSource(hEventSource);
	hEventSource = NULL;
	return (ret__ ? S_OK : HRESULT_FROM_WIN32(::GetLastError()));
}

HRESULT    CEventLogger::ToEvent(LPCWSTR pMessage, const DWORD dError) const
{
	HRESULT hr__ = ToEvent(pMessage, HRESULT_FROM_WIN32(dError));
	return  hr__;
}

HRESULT    CEventLogger::ToEvent(LPCWSTR pMessage, const HRESULT hError) const
{
	shared::lite::common::CSysError the__err(hError);

	CStringW csError = the__err.GetDescription();
	CStringW csMessage;

	if (csError.IsEmpty() || csError.Left(1) == _T("?"))
		csMessage.Format(_T("%s: 0x%x"), pMessage, hError);
	else
		csMessage.Format(_T("%s: %s"), pMessage, csError.GetString());
	HRESULT hr__ = ToEvent(csMessage.GetString(), eEventType::eError);
	return  hr__;
}

HRESULT    CEventLogger::ToFile (LPCWSTR pMessage, const eEventType::_enum type__) const
{
	SAFE_LOCK(m_sync_obj);
	if (NULL == m_file) return OLE_E_BLANK;
	if (0 != (eEventLoggerOption::eLogErrorsOnly & m_dwOptions) && eEventType::eError != type__)
		return S_OK;
	if (NULL == pMessage) return E_INVALIDARG;
	if (m_log_error) return m_log_error;
	SYSTEMTIME st__ = {0};
	::GetLocalTime(&st__);
	CStringW pattern__(_T("\n[%02d/%02d/%04d %02d:%02d:%02d.%04d] [%08d][%s] %s)"));
	switch (type__)
	{
	case eEventType::eError:   pattern__ = _T("\n[%02d/%02d/%04d %02d:%02d:%02d.%04d] [%08d][%s] [ERROR] %s"); break;
	case eEventType::eInfo:    pattern__ = _T("\n[%02d/%02d/%04d %02d:%02d:%02d.%04d] [%08d][%s] [INFO]  %s"); break;
	case eEventType::eWarning: pattern__ = _T("\n[%02d/%02d/%04d %02d:%02d:%02d.%04d] [%08d][%s] [WARN]  %s"); break;
	}
	CStringW cs__indent(details::EventLogger_IndentationObject().ToString());
	cs__indent += pMessage;
	_ftprintf(m_file,
			pattern__.GetString(),
			st__.wMonth,
			st__.wDay,
			st__.wYear,
			st__.wHour,
			st__.wMinute,
			st__.wSecond,
			st__.wMilliseconds,
			::GetCurrentThreadId(),
			(details::EventLogger_IndentationObject().IsMainThreadCtx() ? _T("s") : _T("a")),
			cs__indent.GetString());
	fflush(m_file);
	return S_OK;
}

HRESULT    CEventLogger::ToFile (LPCWSTR pMessage, const DWORD dError) const
{
	HRESULT hr__ = ToFile(pMessage, HRESULT_FROM_WIN32(dError));
	return  hr__;
}

HRESULT    CEventLogger::ToFile (LPCWSTR pMessage, const HRESULT hError) const
{
	shared::lite::common::CSysError the__err(hError);

	CStringW csError = the__err.GetDescription();
	CStringW csMessage;
	if (csError.IsEmpty() || csError.Left(1) == _T("?"))
		csMessage.Format(_T("%s: 0x%x"), pMessage, hError);
	else
		csMessage.Format(_T("%s: %s"), pMessage, csError.GetString());
	HRESULT hr__ = ToFile(csMessage.GetString(), eEventType::eError);
	return  hr__;
}

HRESULT    CEventLogger::ToLog  (LPCWSTR pMessage, const eEventType::_enum type__) const
{
	HRESULT hr__ = S_FALSE;
	if (0 != (eEventPersistance::eFile & m_pers_state))
		hr__ = ToFile(pMessage, type__);
	if (FAILED(hr__))
		return hr__;
	if (0 != (eEventPersistance::eJournal & m_pers_state))
		hr__ = ToEvent(pMessage, type__);
	return hr__;
}

HRESULT    CEventLogger::ToLog  (LPCWSTR pMessage, const DWORD dError) const
{
	HRESULT hr__ = S_FALSE;
	if (0 != (eEventPersistance::eFile & m_pers_state))
		hr__ = ToFile(pMessage, dError);
	if (FAILED(hr__))
		return hr__;
	if (0 != (eEventPersistance::eJournal & m_pers_state))
		hr__ = ToEvent(pMessage, dError);
	return hr__;
}

HRESULT    CEventLogger::ToLog  (LPCWSTR pMessage, const HRESULT hError) const
{
	HRESULT hr__ = S_FALSE;
	if (0 != (eEventPersistance::eFile & m_pers_state))
		hr__ = ToFile(pMessage, hError);
	if (FAILED(hr__))
		return hr__;
	if (0 != (eEventPersistance::eJournal & m_pers_state))
		hr__ = ToEvent(pMessage, hError);
	return hr__;
}

/////////////////////////////////////////////////////////////////////////////

CEventLog__function_auto_entry::CEventLog__function_auto_entry(const CEventLogger& log_ref, LPCWSTR func_name):
	m__log_ref(log_ref),
	m__func_name(func_name),
	m__removal_ctx(false)
{
	if (!log_ref.IsTurnedOn())
		return;
	details::EventLogger_Indentation& obj__ref = details::EventLogger_IndentationObject();
	if (!obj__ref.IsMainThreadCtx()) // makes the current indent copy for worker thread
	{
		if (!obj__ref.IsCtxAvailable())
		{
			const INT n__replica = obj__ref.Replicate();
			obj__ref.Current(n__replica);
		}
	}
	const INT curr_indent = obj__ref.Current(); obj__ref.Lock();
	CStringW cs__indent;
	{
		for (INT i__ = 0; i__ < curr_indent - 1; i__++)
			cs__indent += _T("--");
		cs__indent += _T("->");
		cs__indent += _T("(%d)[%s]");
	}
	{
		CStringW str__;
		str__.Format(cs__indent.GetString(), curr_indent, m__func_name.GetString());
		m__log_ref.ToFile(str__.GetString(), eEventType::eInfo);
	}
	obj__ref.Increase();
	obj__ref.Unlock();
}

CEventLog__function_auto_entry::~CEventLog__function_auto_entry(void)
{
	if (!m__log_ref.IsTurnedOn())
		return;
	details::EventLogger_Indentation& obj__ref = details::EventLogger_IndentationObject();
	const INT curr_indent = obj__ref.Decrease(); obj__ref.Lock();
	CStringW cs__indent;
	{
		cs__indent += _T("<-");
		for (INT i__ = 0; i__ < curr_indent - 1; i__++)
			cs__indent += _T("--");
		cs__indent += _T("(%d)[%s]");
	}
	{
		CStringW str__;
		str__.Format(cs__indent.GetString(), curr_indent, m__func_name.GetString());
		m__log_ref.ToFile(str__.GetString(), eEventType::eInfo);
	}
	obj__ref.Unlock();
	if (m__removal_ctx) obj__ref.Remove();
}