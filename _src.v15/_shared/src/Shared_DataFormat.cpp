/*
	Created by Tech_dog (ebontrop@gmail.com) on 20-Mar-2014 at 8:10:37pm, GMT+4, Taganrog, Thursday;
	This is Shared Lite DateTime Utility class implementation file.
	-----------------------------------------------------------------------------
	Adopted to VS15 on 15-Feb-2021 at 4:09:54.200 am, UTC+7, Novosibirsk, Monday;
*/
#include "StdAfx.h"
#include "Shared_DataFormat.h"
#include <atlcomtime.h>

using namespace shared::lite::format;
using namespace shared::lite::data;

/////////////////////////////////////////////////////////////////////////////

namespace shared { namespace lite { namespace format { namespace details
{
	static LPCWSTR DataFormat_DefaultDateFormat     = _T("%02d/%02d/%d");
	static LPCWSTR DataFormat_DefaultDateTimeFormat = _T("%02d/%02d/%d %02d:%02d:%02d");
	static LPCWSTR DataFormat_DefaultTimeFormat     = _T("%02d:%02d:%02d");
}}}}

/////////////////////////////////////////////////////////////////////////////

CDateTimeFormat::CDateTimeFormat(void)
{
}

CDateTimeFormat::~CDateTimeFormat(void)
{
}

/////////////////////////////////////////////////////////////////////////////

LPCWSTR    CDateTimeFormat::GetDefaultDateFormat(void)
{
	return details::DataFormat_DefaultDateFormat;
}

LPCWSTR    CDateTimeFormat::GetDefaultDateTimeFormat(void)
{
	return details::DataFormat_DefaultDateTimeFormat;
}

LPCWSTR    CDateTimeFormat::GetDefaultTimeFormat(void)
{
	return details::DataFormat_DefaultTimeFormat;
}

/////////////////////////////////////////////////////////////////////////////

CStringW   CDateTimeFormat::GetFormatString(const DWORD dFlags) const
{
	CStringW cs_format;
	const bool bHasDatePart = (0!=(eDataFormatType::eDateShort & dFlags));
	const bool bHasTimePart = (0!=(eDataFormatType::eTimeShort & dFlags));

	if (bHasDatePart)
	{
		TCHAR fixed_size_buffer[80] = {0};
		if (::GetLocaleInfo(LOCALE_USER_DEFAULT, LOCALE_SSHORTDATE, fixed_size_buffer, _countof(fixed_size_buffer)))
		{
			cs_format = fixed_size_buffer;
		}
	}

	if (bHasTimePart)
	{
		TCHAR fixed_size_buffer[80] = {0};
		if (::GetLocaleInfo(LOCALE_USER_DEFAULT, LOCALE_STIMEFORMAT, fixed_size_buffer, _countof(fixed_size_buffer)))
		{
			if (bHasDatePart)
				cs_format += _T(" ");
			cs_format += fixed_size_buffer;
		}
	}

	return cs_format;
}

LPCWSTR    CDateTimeFormat::ToDate(const SYSTEMTIME& sys__) const
{
	if (m_buffer.IsEmpty()==false)
		m_buffer.Empty();
	COleDateTime ole_date(sys__.wYear, sys__.wMonth, sys__.wDay, 0, 0, 0);
	if (COleDateTime::valid == ole_date.m_status)
	{
		_variant_t var_date(ole_date.m_dt, VT_DATE);
		const LCID lcid   = MAKELCID( MAKELANGID(LANG_NEUTRAL, SUBLANG_DEFAULT), SORT_DEFAULT );
		const HRESULT hr_ = ::VariantChangeTypeEx(&var_date, &var_date, lcid, 0, VT_BSTR);
		if (S_OK == hr_)
			m_buffer = var_date;
	}
	if (m_buffer.IsEmpty())
		m_buffer.Format(details::DataFormat_DefaultDateFormat, sys__.wDay, sys__.wMonth, sys__.wYear);
	return  m_buffer.GetString();
}

LPCWSTR    CDateTimeFormat::ToDate(const SYSTEMTIME& sys__, LPCWSTR pCustomPattern) const
{
	if (m_buffer.IsEmpty()==false)
		m_buffer.Empty();
	m_buffer = pCustomPattern;
	{
		CStringW cs_data;
		cs_data.Format(_T("%02d"), sys__.wDay);
		m_buffer.Replace(_T("DD"), cs_data.GetString());
	}
	{
		CStringW cs_data;
		cs_data.Format(_T("%02d"), sys__.wMonth);
		m_buffer.Replace(_T("MM"), cs_data.GetString());
	}
	{
		CStringW cs_data;
		cs_data.Format(_T("%04d"), sys__.wYear);
		m_buffer.Replace(_T("YYYY"), cs_data.GetString());
	}
	return m_buffer.GetString();
}

LPCWSTR    CDateTimeFormat::ToDateTime(const SYSTEMTIME& sys__) const
{
	if (m_buffer.IsEmpty()==false)
		m_buffer.Empty();
	m_buffer.Format(
			details::DataFormat_DefaultDateTimeFormat,
			sys__.wDay  ,
			sys__.wMonth,
			sys__.wYear ,
			sys__.wHour ,
			sys__.wMinute,
			sys__.wSecond
		);
	return  m_buffer.GetString();
}

LPCWSTR    CDateTimeFormat::ToDateTime(const SYSTEMTIME& dt_ref, const SYSTEMTIME& tm_ref) const
{
	if (m_buffer.IsEmpty()==false)
		m_buffer.Empty();
	m_buffer.Format(
			details::DataFormat_DefaultDateTimeFormat,
			dt_ref.wDay  ,
			dt_ref.wMonth,
			dt_ref.wYear ,
			tm_ref.wHour ,
			tm_ref.wMinute,
			tm_ref.wSecond
		);
	return  m_buffer.GetString();
}

LPCWSTR    CDateTimeFormat::ToTime(const SYSTEMTIME& sys__) const
{
	if (m_buffer.IsEmpty()==false)
		m_buffer.Empty();
	COleDateTime ole_date(sys__);
	if (COleDateTime::valid == ole_date.m_status)
	{
		_variant_t var_date(ole_date.m_dt, VT_DATE);
		const LCID lcid   = MAKELCID( MAKELANGID(LANG_NEUTRAL, SUBLANG_DEFAULT), SORT_DEFAULT );
		const HRESULT hr_ = ::VariantChangeTypeEx(&var_date, &var_date, lcid, 0, VT_BSTR);
		if (S_OK == hr_)
		{
			m_buffer = var_date;
			CStringW cs_format = this->GetFormatString(eDataFormatType::eTimeShort);
			m_buffer = m_buffer.Right(cs_format.GetLength());
			m_buffer.Trim();
		}
	}
	if (m_buffer.IsEmpty())
		m_buffer.Format(details::DataFormat_DefaultTimeFormat, sys__.wHour, sys__.wMinute, sys__.wSecond);
	return  m_buffer.GetString();
}

LPCWSTR    CDateTimeFormat::ToTime(const SYSTEMTIME& sys__, LPCWSTR pCustomPattern) const
{
	if (m_buffer.IsEmpty()==false)
		m_buffer.Empty();
	m_buffer = pCustomPattern;
	{
		CStringW cs_data;
		cs_data.Format(_T("%02d"), sys__.wHour);
		m_buffer.Replace(_T("hh"), cs_data.GetString());
	}
	{
		CStringW cs_data;
		cs_data.Format(_T("%02d"), sys__.wMinute);
		m_buffer.Replace(_T("mm"), cs_data.GetString());
	}
	{
		CStringW cs_data;
		cs_data.Format(_T("%02d"), sys__.wSecond);
		m_buffer.Replace(_T("ss"), cs_data.GetString());
	}
	return m_buffer.GetString();
}

/////////////////////////////////////////////////////////////////////////////

CTimestamp::CTimestamp(void) : m_value(0)
{
}

/////////////////////////////////////////////////////////////////////////////

VOID       CTimestamp::SetCurrentTime(void)
{
	::time(&m_value);
}

time_t     CTimestamp::Value(void)const
{
	return m_value;
}

VOID       CTimestamp::Value(const time_t _value)
{
	m_value = _value;
}

CStringW CTimestamp::ValueAsFormattedText(void)const
{
	if (0 == m_value)
		return CStringW(_T("0000-00-00 00:00:00"));

	TCHAR buf_[_MAX_PATH] = {0};

	::_tcsftime(
			buf_,
			_countof(buf_),
			_T("%Y-%m-%d %H:%M:%S"),
			::localtime(&m_value)
		);
	CStringW cs_stamp(buf_);
	return cs_stamp;
}

CStringW CTimestamp::ValueAsText(void)const
{
	CStringW cs_stamp;
	cs_stamp.Format(
			_T("%ld"),
			m_value
		);
	return cs_stamp;
}

HRESULT    CTimestamp::ValueAsText(LPCWSTR pszTimestamp, const bool bValidateData)
{
	CStringW cs_stamp(pszTimestamp);
	if (cs_stamp.GetLength() < 1)
	{
		m_value = 0;
		return S_OK;
	}

	if (bValidateData)
		for (INT i_ = 0; i_ < cs_stamp.GetLength(); i_++)
			if (!::_istdigit(cs_stamp.GetAt(i_)))
				return E_INVALIDARG;

	m_value = ::_tstoi64(cs_stamp);

	return S_OK;
}