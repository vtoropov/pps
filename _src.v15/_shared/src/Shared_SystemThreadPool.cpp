/*
	Created by Tech_dog (VToropov) on 11-Jan-2016 at 3:37:45pm, GMT+7, Phuket, Rawai, Monday;
	This is Shared Lite library system thread pool related class(s) implementation file.
*/
#include "StdAfx.h"
#include "Shared_SystemThreadPool.h"

using namespace shared::lite::sys_core;

/////////////////////////////////////////////////////////////////////////////

CThreadCrtData::CThreadCrtData(void) : m_bStopped(false), m_hStopEvent(NULL)
{
	m_hStopEvent = ::CreateEvent(NULL, TRUE, FALSE, NULL);
}

CThreadCrtData::~CThreadCrtData(void)
{
	if (m_hStopEvent)
	{
		::CloseHandle(m_hStopEvent);
		m_hStopEvent = NULL;
	}
}

/////////////////////////////////////////////////////////////////////////////

HANDLE& CThreadCrtData::EventObject(void)
{
	return m_hStopEvent;
}

bool    CThreadCrtData::IsStopped(void)const
{
	return m_bStopped;
}

VOID    CThreadCrtData::IsStopped(const bool _stop)
{
	m_bStopped = _stop;
}