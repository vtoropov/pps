/*
	Created by Tech_dog (ebontrop@gmail.com) at 19-Mar-2014 at 12:12:15pm, GMT+4, Taganrog, Wednesday;
	This is Shared Lite Persistent Storage class(es) implementation file.
	-----------------------------------------------------------------------------
	Adopted to version v15 on 13-Feb-2021 at 5:17:31.062 pm, UTC+7, Novosibirsk, Saturday;
*/
#include "StdAfx.h"
#include "Shared_PersistentStorage.h"
#include "Shared_RawData.h"

using namespace shared::lite::persistent;
using namespace shared::lite::persistent::factories;
using namespace shared::lite::data;

/////////////////////////////////////////////////////////////////////////////

namespace shared { namespace lite { namespace persistent { namespace details
{
	const LPCWSTR Profile__ROW_TOKEN       = _T("\n");
	const LPCWSTR Profile__END_TOKEN       = _T("\r");
	const LPCWSTR Profile__EMPTY_DATA      = _T("n/a");
	const LPCWSTR Profile__COMMENT_TOKEN   = _T(";");
	const LPCWSTR Profile__SEC_BEGIN_TOKEN = _T("[");
	const LPCWSTR Profile__SEC_END_TOKEN   = _T("]");
	const LPCWSTR Profile__PAIR_DEV_TOKEN  = _T("=");

	using shared::lite::persistent::CPrivateProfileItem;
	using shared::lite::persistent::CPrivateProfileSection;

	static CPrivateProfileItem&     Profile_ItemInvalidObject_Ref(void)
	{
		static CPrivateProfileItem pair__na;
		return pair__na;
	}

	static CPrivateProfileSection&  Profile_SectionInvalidObject_Ref(void)
	{
		static CPrivateProfileSection sec__na;
		return sec__na;
	}

	static HRESULT  Profile_LoadDataFrom(LPCWSTR pFile, CStringW& __in_out_ref)
	{
		::ATL::CAtlFile file__;
		HRESULT hr__ = file__.Create(pFile, FILE_READ_DATA, FILE_SHARE_READ, OPEN_EXISTING);
		if (S_OK != hr__)
			return  hr__;
		ULONGLONG size__ = 0;
		hr__ = file__.Seek(0, FILE_BEGIN);
		if (S_OK != hr__)
			return  hr__;
		hr__ = file__.GetSize(size__);
		if (S_OK != hr__)
			return  hr__;
		if (size__ & 0x0000ffff)
		{
			const INT n__sz__  = (INT)size__ + 1;
			shared::lite::data::CByteArray ptr__(n__sz__);
			hr__ = ptr__.GetLastResult();
			if (S_OK != hr__)
				return  hr__;
			hr__ = file__.Read((LPVOID)ptr__.GetData(), n__sz__ - 1);
			if (S_OK != hr__)
				return  hr__;
			__in_out_ref = (reinterpret_cast<char*>(ptr__.GetData()));
		}
		else
			hr__ = S_FALSE;
		return  hr__;
	}

	static bool  Profile_IsCommentLine(const CStringW& ln__ref)
	{
		return (ln__ref.GetLength() > 0 && ln__ref.Find(Profile__COMMENT_TOKEN) == 0);
	}

	static bool  Profile_IsSection(const CStringW& ln__ref, CStringW& sec__ref, bool& err__ref)
	{
		err__ref = false;
		if (ln__ref.GetLength() > 0 && ln__ref.Find(Profile__SEC_BEGIN_TOKEN) == 0)
		{
			const INT n__close_bracket = ln__ref.Find(Profile__SEC_END_TOKEN);
			if (n__close_bracket == -1)
				return false;
			sec__ref = ln__ref.Left(n__close_bracket);
			sec__ref.Replace(Profile__SEC_BEGIN_TOKEN, _T(""));
			return true;
		}
		else
			return false;
	}

	static bool  Profile_LineToItem(const CStringW& ln__ref, CPrivateProfileItem& pair__ref, LPCWSTR prefix__ = NULL)
	{
		const INT n__div__pos__ = ln__ref.Find(Profile__PAIR_DEV_TOKEN);
		if (n__div__pos__ < 0)
			return false;
		if (prefix__)
		{
			pair__ref.Name() = prefix__;
			pair__ref.Name()+= ln__ref.Left(n__div__pos__);
		}
		else
			pair__ref.Name() = ln__ref.Left(n__div__pos__);
		if (pair__ref.Name().IsEmpty() == false)
			pair__ref.Name().Trim();
		pair__ref.Value() = ln__ref.Right(ln__ref.GetLength() - n__div__pos__ - 1);
		if (pair__ref.Value().IsEmpty()== false)
			pair__ref.Value().Trim();
		const INT n__comment__pos__ = pair__ref.Value().Find(Profile__COMMENT_TOKEN);
		if (n__comment__pos__ > -1)
		{
			pair__ref.Comment() = pair__ref.Value().Right(pair__ref.Value().GetLength() - n__comment__pos__ - 1);
			pair__ref.Value() = pair__ref.Value().Left(n__comment__pos__);
			if (pair__ref.Value().IsEmpty()== false)
				pair__ref.Value().Trim();
		}
		return true;
	}
}}}}

/////////////////////////////////////////////////////////////////////////////

CPrivateProfileItem::CPrivateProfileItem(void)
{
}

CPrivateProfileItem::CPrivateProfileItem(LPCWSTR pName, LPCWSTR pValue, LPCWSTR pComment) : 
	m_name(pName),
	m_value(pValue),
	m_comment(pComment)
{
}

CPrivateProfileItem::~CPrivateProfileItem(void)
{
}

/////////////////////////////////////////////////////////////////////////////

const CStringW&     CPrivateProfileItem::Comment(void) const
{
	return m_comment;
}

CStringW&           CPrivateProfileItem::Comment(void)
{
	return m_comment;
}

bool                         CPrivateProfileItem::IsValid(void) const
{
	return !m_name.IsEmpty();
}

const CStringW&     CPrivateProfileItem::Name(void) const
{
	return m_name;
}

CStringW&           CPrivateProfileItem::Name(void)
{
	return m_name;
}

const CStringW&     CPrivateProfileItem::Value(void) const
{
	return m_value;
}

CStringW&           CPrivateProfileItem::Value(void)
{
	return m_value;
}

/////////////////////////////////////////////////////////////////////////////

CPrivateProfileItem::CPrivateProfileItem(const CPrivateProfileItem& rh__ref)
{
	this->m_comment = rh__ref.m_comment;
	this->m_name    = rh__ref.m_name;
	this->m_value   = rh__ref.m_value;
}

CPrivateProfileItem& CPrivateProfileItem::operator= (const CPrivateProfileItem& rh__ref)
{
	this->m_comment = rh__ref.m_comment;
	this->m_name    = rh__ref.m_name;
	this->m_value   = rh__ref.m_value;
	return (*this);
}

/////////////////////////////////////////////////////////////////////////////

CPrivateProfileSection::CPrivateProfileSection(void)
{
}

CPrivateProfileSection::CPrivateProfileSection(LPCWSTR pName) : m_name(pName)
{
}

CPrivateProfileSection::~CPrivateProfileSection(void)
{
	if (!m_index.empty()) m_index.clear();
	if (!m_items.empty()) m_items.clear();
}

/////////////////////////////////////////////////////////////////////////////

HRESULT                       CPrivateProfileSection::AddItem(LPCWSTR pszName)
{
	if (!pszName || !::lstrlenW(pszName))
		return E_INVALIDARG;

	const CPrivateProfileItem& item_ = this->ItemOf(pszName);
	if (item_.IsValid())
		return HRESULT_FROM_WIN32(ERROR_OBJECT_ALREADY_EXISTS);

	try
	{
		CPrivateProfileItem item_loc;
		item_loc.Name() = pszName;
		const INT nIndex = this->ItemCount();
		m_items.push_back(item_loc);
		m_index.insert(
			::std::make_pair(CAtlString(pszName), nIndex)
			);
	}
	catch(::std::bad_alloc&)
	{
		return E_OUTOFMEMORY;
	}
	HRESULT hr_ = S_OK;
	return  hr_;
}

bool                          CPrivateProfileSection::IsEmpty(void) const
{
	return m_items.empty();
}

bool                          CPrivateProfileSection::IsValid(void) const
{
	return !m_name.IsEmpty();
}

INT                           CPrivateProfileSection::ItemCount(void) const
{
	return (INT)m_items.size();
}

const CPrivateProfileItem&    CPrivateProfileSection::ItemOf(const INT nIndex) const
{
	if (0 > nIndex || nIndex > ItemCount() - 1)
		return details::Profile_ItemInvalidObject_Ref();
	else
		return m_items[nIndex];
}

CPrivateProfileItem&          CPrivateProfileSection::ItemOf(const INT nIndex)
{
	if (0 > nIndex || nIndex > ItemCount() - 1)
		return details::Profile_ItemInvalidObject_Ref();
	else
		return m_items[nIndex];
}

const CPrivateProfileItem&    CPrivateProfileSection::ItemOf(LPCWSTR pName) const
{
	TItemsIndex::const_iterator it__ = m_index.find(pName);
	if (it__ == m_index.end())
		return details::Profile_ItemInvalidObject_Ref();
	const INT nIndex = it__->second;
	return this->ItemOf(nIndex);
}

CPrivateProfileItem&          CPrivateProfileSection::ItemOf(LPCWSTR pName)
{
	TItemsIndex::const_iterator it__ = m_index.find(pName);
	if (it__ == m_index.end())
		return details::Profile_ItemInvalidObject_Ref();
	const INT nIndex = it__->second;
	return this->ItemOf(nIndex);
}

const CStringW&      CPrivateProfileSection::Name(void) const
{
	return m_name;
}

CStringW&            CPrivateProfileSection::Name(void)
{
	return m_name;
}

/////////////////////////////////////////////////////////////////////////////

CPrivateProfileSection::CPrivateProfileSection(const CPrivateProfileSection& rh__ref)
{
	this->m_index = rh__ref.m_index;
	this->m_name  = rh__ref.m_name;
	this->m_items = rh__ref.m_items;
}

CPrivateProfileSection& CPrivateProfileSection::operator= (const CPrivateProfileSection& rh__ref)
{
	this->m_index = rh__ref.m_index;
	this->m_name  = rh__ref.m_name;
	this->m_items = rh__ref.m_items;
	return (*this);
}

/////////////////////////////////////////////////////////////////////////////

CPrivateProfile::CPrivateProfile(void) : m_result(OLE_E_BLANK), m_store_mode(CPrivateProfile::SM_UseRegular)
{
}

CPrivateProfile::CPrivateProfile(LPCWSTR pFile) : m_result(OLE_E_BLANK)
{
	this->Create(pFile);
}

CPrivateProfile::~CPrivateProfile(void)
{
	if (!m_index.empty()) m_index.clear();
	if (!m_sections.empty()) m_sections.clear();
	m_result = OLE_E_BLANK;
}

/////////////////////////////////////////////////////////////////////////////

HRESULT                       CPrivateProfile::AddSection(LPCWSTR pszSection)
{
	if (!pszSection || !::lstrlenW(pszSection))
		return E_INVALIDARG;

	const CPrivateProfileSection& sec_ = this->SectionOf(pszSection);
	if (sec_.IsValid())
		return HRESULT_FROM_WIN32(ERROR_ALREADY_EXISTS);
	try
	{
		CPrivateProfileSection sec_new;
		sec_new.Name() = pszSection;

		const INT count_ = this->SectionCount();
		m_sections.push_back(sec_new);
		m_index.insert(
				::std::make_pair(CAtlString(pszSection), count_)
			);
	}
	catch(::std::bad_alloc&)
	{
		return E_OUTOFMEMORY;
	}
	HRESULT hr_ = S_OK;
	return  hr_;
}

HRESULT                       CPrivateProfile::Create(LPCWSTR pFile)
{
	if (!m_index.empty()) m_index.clear();
	if (!m_sections.empty()) m_sections.clear();
	m_result = OLE_E_BLANK;
	m_store_mode = CPrivateProfile::SM_UseRegular;
	m_store_path = pFile;

	CStringW buffer__;
	m_result = details::Profile_LoadDataFrom(pFile, buffer__);
	if (S_OK != m_result)
		return  m_result;
	try
	{
		bool err__ = false;
		INT line__ = NULL;
		CStringW line__token__;
		CStringW section__;
		CPrivateProfileSection sec__obj__;
		CPrivateProfileSectionFactory sec__fty__(sec__obj__);
		line__token__ = buffer__.Tokenize(details::Profile__ROW_TOKEN, line__);
		while (!line__token__.IsEmpty())
		{
			line__token__.Trim();
			if (!details::Profile_IsCommentLine(line__token__))
			{
				if (details::Profile_IsSection(line__token__, section__, err__) && !err__)
				{
					if (sec__obj__.IsValid()) // previous section is valid, we need to add the section to the collection and to re-create it
					{
						m_sections.push_back(sec__obj__);
						m_index.insert(::std::make_pair(sec__obj__.Name(), SectionCount() - 1));
					}
					sec__fty__.Clear();
					sec__obj__.Name() = section__.Trim();
				}
				else if (!err__ && sec__obj__.IsValid())
				{
					CPrivateProfileItem pair__obj__;
					if (details::Profile_LineToItem(line__token__, pair__obj__))
					{
						sec__obj__.m_items.push_back(pair__obj__);
						::std::pair<CStringW, INT> p__ =::std::make_pair(pair__obj__.Name(), sec__obj__.ItemCount() - 1);
						sec__obj__.m_index.insert(p__);
					}
				}
			}
			line__token__ = buffer__.Tokenize(details::Profile__ROW_TOKEN, line__);
		}
		if (sec__obj__.IsValid()) // inserts the last valid section
		{
			m_sections.push_back(sec__obj__);
			m_index.insert(::std::make_pair(sec__obj__.Name(), SectionCount() - 1));
		}
	}
	catch(::std::bad_alloc&)
	{
		ATLASSERT(FALSE);
		m_result = E_OUTOFMEMORY;
	}
	return m_result;
}

HRESULT                       CPrivateProfile::CreateGlobal(LPCWSTR pFile, LPCWSTR p__key__separator)
{
	if (!m_index.empty()) m_index.clear();
	if (!m_sections.empty()) m_sections.clear();
	m_result = OLE_E_BLANK;
	m_store_mode = CPrivateProfile::SM_UseGlobalIndex;

	CStringW buffer__;
	m_result = details::Profile_LoadDataFrom(pFile, buffer__);
	if (S_OK != m_result)
		return  m_result;
	CPrivateProfileSection sec__obj__(_T("GlobalIndex"));
	if (sec__obj__.IsValid())
	{
		m_sections.push_back(sec__obj__);
		m_index.insert(::std::make_pair(sec__obj__.Name(), SectionCount() - 1));
	}
	else
		return (m_result = E_OUTOFMEMORY);
	CPrivateProfileSection& sec__obj__ref = SectionOf(0);
	if (!sec__obj__ref.IsValid())
		return (m_result = E_OUTOFMEMORY);
	try
	{
		bool err__ = false;
		INT line__ = NULL;
		CStringW line__token__;
		CStringW prefix__;
		line__token__ = buffer__.Tokenize(details::Profile__ROW_TOKEN, line__);
		while (!line__token__.IsEmpty())
		{
			line__token__.Trim();
			if (!details::Profile_IsCommentLine(line__token__))
			{
				if (details::Profile_IsSection(line__token__, prefix__, err__) && !err__)
				{
					if (p__key__separator) prefix__ += p__key__separator;
				}
				else
				{
					CPrivateProfileItem pair__obj__;
					if (details::Profile_LineToItem(line__token__, pair__obj__, prefix__.GetString()))
					{
						sec__obj__ref.m_items.push_back(pair__obj__);
						::std::pair<CStringW, INT> p__ =::std::make_pair(pair__obj__.Name(), sec__obj__ref.ItemCount() - 1);
						sec__obj__ref.m_index.insert(p__);
					}
				}
			}
			line__token__ = buffer__.Tokenize(details::Profile__ROW_TOKEN, line__);
		}
	}
	catch(::std::bad_alloc&)
	{
		ATLASSERT(FALSE);
		m_result = E_OUTOFMEMORY;
	}
	return m_result;
}

bool                          CPrivateProfile::GetBoolValue(LPCWSTR pszSection, LPCWSTR pszItem, const bool bDefaultValue)const
{
	bool bResult = bDefaultValue;
	if (this->IsEmpty())
		return bResult;

	const CPrivateProfileSection& sec_ref = this->SectionOf(pszSection);
	if (sec_ref.IsValid())
	{
		const CPrivateProfileItem& item_ref = sec_ref.ItemOf(pszItem);
		if (item_ref.IsValid())
		{
			CStringW cs_flag = item_ref.Value();
			if (!cs_flag.IsEmpty())
			{
				const INT n_pos = cs_flag.CompareNoCase(_T("TRUE")); 
				bResult = (0 == n_pos);
			}
		}
	}
	return bResult;
}

DWORD                         CPrivateProfile::GetDwordValue(LPCWSTR pszSection, LPCWSTR pszItem, const DWORD dwDefaultValue)const
{
	DWORD dwValue = dwDefaultValue;
	CStringW cs_value = this->GetStringValue(pszSection, pszItem, NULL);
	if (!cs_value.IsEmpty())
		dwValue = static_cast<DWORD>(::_tstol(cs_value.GetString()));
	return dwValue;
}

HRESULT                       CPrivateProfile::GetLastResult(void) const
{
	return m_result;
}

CPrivateProfile::eStorageMode CPrivateProfile::GetStorageMode(void) const
{
	return m_store_mode;
}

CAtlString                    CPrivateProfile::GetStringValue(LPCWSTR pszSection, LPCWSTR pszItem, LPCWSTR pszDefaultValue)const
{
	CAtlString cs_result = pszDefaultValue;
	if (this->IsEmpty())
		return  cs_result;

	const CPrivateProfileSection& sec_ref = this->SectionOf(pszSection);
	if (sec_ref.IsValid())
	{
		const CPrivateProfileItem& item_ref = sec_ref.ItemOf(pszItem);
		if (item_ref.IsValid())
		{
			cs_result = item_ref.Value();
			if (!cs_result.IsEmpty())cs_result.Trim();
		}
	}
	return cs_result;
}

bool                          CPrivateProfile::IsEmpty(void) const
{
	return m_sections.empty();
}

bool                          CPrivateProfile::IsValid(void) const
{
	return (S_OK == this->GetLastResult());
}

INT                           CPrivateProfile::SectionCount(void) const
{
	return (INT)m_sections.size();
}

const CPrivateProfileSection& CPrivateProfile::SectionOf(const INT nIndex)const
{
	if (0 > nIndex || nIndex > SectionCount() - 1)
		return details::Profile_SectionInvalidObject_Ref();
	else
		return m_sections[nIndex];
}

CPrivateProfileSection&       CPrivateProfile::SectionOf(const INT nIndex)
{
	if (0 > nIndex || nIndex > SectionCount() - 1)
		return details::Profile_SectionInvalidObject_Ref();
	else
		return m_sections[nIndex];
}

const CPrivateProfileSection& CPrivateProfile::SectionOf(LPCWSTR pName)const
{
	TSectionIndex::const_iterator it__ = m_index.find(pName);
	if (it__ == m_index.end())
		return details::Profile_SectionInvalidObject_Ref();
	const INT nIndex = it__->second;
	return this->SectionOf(nIndex);
}

CPrivateProfileSection&       CPrivateProfile::SectionOf(LPCWSTR pName)
{
	TSectionIndex::const_iterator it__ = m_index.find(pName);
	if (it__ == m_index.end())
		return details::Profile_SectionInvalidObject_Ref();
	const INT nIndex = it__->second;
	return this->SectionOf(nIndex);
}

HRESULT                       CPrivateProfile::SetStringValue(LPCWSTR pszSection, LPCWSTR pszItem, LPCWSTR pszValue)
{
	if (!this->IsValid())
		return OLE_E_BLANK;

	this->AddSection(pszSection);

	CPrivateProfileSection& sec_ = this->SectionOf(pszSection);
	if (sec_.IsValid())
		return E_OUTOFMEMORY;

	sec_.AddItem(pszItem);

	CPrivateProfileItem& item_ = sec_.ItemOf(pszItem);
	if (!item_.IsValid())
		return E_OUTOFMEMORY;

	item_.Value() = pszValue;

	const BOOL bResult = ::WritePrivateProfileString(
										pszSection,
										pszItem,
										pszValue,
										m_store_path
									);

	HRESULT hr_ = (bResult ? S_OK : HRESULT_FROM_WIN32(::GetLastError()));
	return  hr_;
}

/////////////////////////////////////////////////////////////////////////////

CPrivateProfileSectionFactory::CPrivateProfileSectionFactory(CPrivateProfileSection& sec__ref) : m__sec__ref(sec__ref)
{
}

CPrivateProfileSectionFactory::~CPrivateProfileSectionFactory(void)
{
}

/////////////////////////////////////////////////////////////////////////////

HRESULT        CPrivateProfileSectionFactory::Clear(void)
{
	if (m__sec__ref.IsEmpty())
		return S_FALSE;
	if (!m__sec__ref.m_items.empty()) m__sec__ref.m_items.clear();
	if (!m__sec__ref.m_index.empty()) m__sec__ref.m_index.clear();
	m__sec__ref.m_name.Empty();
	return S_OK;
}

#include "Shared_GenericAppObject.h"
/////////////////////////////////////////////////////////////////////////////

namespace shared { namespace lite { namespace persistent { namespace details
{
	using shared::lite::common::CApplication;

	static CApplication&
	               RegistryStorage_AppObject(void)
	{
		static CApplication the_app;
		return the_app;
	}

	static VOID    RegistryStorage_FormatQryPath(LPCWSTR pFolder, CStringW& __in_out)
	{
		__in_out.Format(_T("Software\\%s\\%s"), RegistryStorage_AppObject().GetName(), pFolder);
	}

	static HRESULT RegistryStorage_CheckFolderName(LPCWSTR pFolder)
	{
		if (!pFolder)
			return E_INVALIDARG;  // cannot be nothing
		if (1 > ::lstrlenW(pFolder))
			return E_INVALIDARG;  // cannot be zero-length
		else
			return S_OK;
	}

	static HRESULT RegistryStorage_CheckPropertyName(LPCWSTR pProperty)
	{
		if (!pProperty)
			return E_INVALIDARG;  // cannot be nothing, but can be zero-length (default)
		else
			return S_OK;
	}
}}}}

/////////////////////////////////////////////////////////////////////////////

CRegistryStorage::CRegistryStorage(const HKEY hRoot):
	m_hRoot(hRoot)
{
}

CRegistryStorage::~CRegistryStorage(void)
{
}

/////////////////////////////////////////////////////////////////////////////

HRESULT      CRegistryStorage::Load(LPCWSTR pFolder, LPCWSTR pProperty, CStringW& __in_out_value)
{
	HRESULT hr__ = details::RegistryStorage_CheckFolderName(pFolder);
	if (S_OK != hr__)
		return  hr__;
	hr__ = details::RegistryStorage_CheckPropertyName(pProperty);
	if (S_OK != hr__)
		return  hr__;
	hr__ = details::RegistryStorage_AppObject().GetLastResult();
	if (S_OK != hr__)
		return  hr__;
	CStringW qry__;
	details::RegistryStorage_FormatQryPath(pFolder, qry__);
	::ATL::CRegKey  reg_key;
	LONG nLastResult = reg_key.Create(m_hRoot, qry__);
	if (ERROR_SUCCESS != nLastResult)
		return HRESULT_FROM_WIN32(nLastResult);
	static const ULONG n_buff_len = 4096; // should be enough for connection string value
	TCHAR local_buffer[n_buff_len] = {0};
	ULONG local_length = n_buff_len;
	nLastResult = reg_key.QueryStringValue(pProperty, local_buffer, &local_length);
	if (ERROR_SUCCESS != nLastResult)
	{
		if (ERROR_FILE_NOT_FOUND == nLastResult) // the registry can be clean
			return S_FALSE;
		else
			return HRESULT_FROM_WIN32(nLastResult);
	}
	__in_out_value = local_buffer;
	return  hr__;
}

HRESULT      CRegistryStorage::Load(LPCWSTR pFolder, LPCWSTR pProperty, LONG& __in_out_value)
{
	HRESULT hr__ = details::RegistryStorage_CheckFolderName(pFolder);
	if (S_OK != hr__)
		return  hr__;
	hr__ = details::RegistryStorage_CheckPropertyName(pProperty);
	if (S_OK != hr__)
		return  hr__;
	CStringW qry__;
	details::RegistryStorage_FormatQryPath(pFolder, qry__);
	::ATL::CRegKey  reg_key;
	LONG nLastResult = reg_key.Create(m_hRoot, qry__);
	if (ERROR_SUCCESS != nLastResult)
		return HRESULT_FROM_WIN32(nLastResult);
	DWORD dValue = 0;
	nLastResult = reg_key.QueryDWORDValue(pProperty, dValue);
	if (ERROR_SUCCESS != nLastResult)
	{
		if (ERROR_FILE_NOT_FOUND == nLastResult) // the registry can be clean
			return S_FALSE;
		else
			return HRESULT_FROM_WIN32(nLastResult);
	}
	__in_out_value = static_cast<LONG>(dValue);  // cannot be used to explicitly save signed values, but it is acceptable for now!
	return  hr__;
}

HRESULT      CRegistryStorage::Save(LPCWSTR pFolder, LPCWSTR pProperty, const CStringW& __in_value)
{
	HRESULT hr__ = details::RegistryStorage_CheckFolderName(pFolder);
	if (S_OK != hr__)
		return  hr__;
	hr__ = details::RegistryStorage_CheckPropertyName(pProperty);
	if (S_OK != hr__)
		return  hr__;
	CStringW qry__;
	details::RegistryStorage_FormatQryPath(pFolder, qry__);
	::ATL::CRegKey  reg_key;
	LONG nLastResult = reg_key.Create(m_hRoot, qry__);
	if (ERROR_SUCCESS != nLastResult)
		return HRESULT_FROM_WIN32(nLastResult);
	nLastResult = reg_key.SetStringValue(pProperty, __in_value.GetString());
	if (ERROR_SUCCESS != nLastResult)
		hr__ = HRESULT_FROM_WIN32(nLastResult);
	return  hr__;
}

HRESULT      CRegistryStorage::Save(LPCWSTR pFolder, LPCWSTR pProperty, const LONG __in_value)
{
	HRESULT hr__ = details::RegistryStorage_CheckFolderName(pFolder);
	if (S_OK != hr__)
		return  hr__;
	hr__ = details::RegistryStorage_CheckPropertyName(pProperty);
	if (S_OK != hr__)
		return  hr__;
	CStringW qry__;
	details::RegistryStorage_FormatQryPath(pFolder, qry__);
	::ATL::CRegKey  reg_key;
	LONG nLastResult = reg_key.Create(m_hRoot, qry__);
	if (ERROR_SUCCESS != nLastResult)
		return HRESULT_FROM_WIN32(nLastResult);
	const DWORD dValue = (DWORD)__in_value; // scary (implicit signed/unsigned value conversion), but we can cope with it for now!
	nLastResult = reg_key.SetDWORDValue(pProperty, dValue);
	if (ERROR_SUCCESS != nLastResult)
		hr__ = HRESULT_FROM_WIN32(nLastResult);
	return  hr__;
}

/////////////////////////////////////////////////////////////////////////////

namespace shared { namespace lite { namespace persistent { namespace details
{
	const LPCWSTR CsvFile__CSV_SEP_TOKEN  = _T("\t");
	const LPCWSTR CsvFile__CSV_ROW_TOKEN  = _T("\n");
	const LPCWSTR CsvFile__CSV_END_TOKEN  = _T("\r");
	const LPCWSTR CsvFile__CSV_EMPTY_DATA = _T("n/a");
	const LPCWSTR CsvFile__CSV_SEP_TOKEN2 = _T(";");
	const LPCWSTR CsvFile__CSV_SEP_TOKEN4 = _T(",");

	static const CCsvFile::TRow&  CsvFile_InvalidCsvRow_Ref(void)
	{
		static const CCsvFile::TRow row__;
		return row__;
	}
	static CCsvFile::TRow&        CsvFile_InvalidCsvRow_Ref2(void)
	{
		static CCsvFile::TRow row__;
		return row__;
	}

	// this function is based on assumption that ANSI text (english base character set) is represented
	// by UTF-16; that means the follower byte is always 0x0;
	static bool    CsvFile_IsUnicodeData(const CByteArray& _data, const DWORD _byte_to_check = 0x64)
	{
		const PBYTE pbData = _data.GetData();
		const DWORD dwSize = _data.GetSize();
		for ( DWORD i_ = 1; i_ < dwSize && i_ < _byte_to_check; i_ += 2)
		{
			if (0 == (pbData[i_ - 1] & 0x80) &&
				0 == (pbData[i_ - 0]))
				return true; // UTF-16 is detected
		}
		return false;
	}

	static LPCWSTR CsvFile_GetSeparator(const DWORD dwOptions)
	{
		return (eCsvFileOption::eUseSemicolon & dwOptions ? CsvFile__CSV_SEP_TOKEN2 :
				(eCsvFileOption::eUseCommaSeparator & dwOptions ? CsvFile__CSV_SEP_TOKEN4 : CsvFile__CSV_SEP_TOKEN));
	}
	static HRESULT CsvFile_WriteLine(::ATL::CAtlFile& file_, const ::std::vector<CStringW>& line_, LPCWSTR pFieldDivider)
	{
		HRESULT hr_ = S_FALSE;
		CStringW cs_buffer;
		const LONG nFields = (LONG)line_.size();
		for ( LONG i_ = 0; i_ < nFields; i_++)
		{
			cs_buffer += line_[i_].GetString();
			if (i_ < nFields - 1)
				cs_buffer += pFieldDivider;
			else
				cs_buffer += CsvFile__CSV_ROW_TOKEN;
		}
		if (!cs_buffer.IsEmpty())
		{
			const DWORD dwSize = sizeof(TCHAR) * cs_buffer.GetLength();
			hr_ = file_.Write((LPCVOID)cs_buffer.GetBuffer(), dwSize);
		}
		return hr_;
	}
}}}}

/////////////////////////////////////////////////////////////////////////////

CCsvFile::CCsvFile(const DWORD dwOptions):
	m_result(OLE_E_BLANK),
	m_has_header(false),
	m_dwOptions(dwOptions),
	m_src_size(0)
{
}

CCsvFile::~CCsvFile(void)
{
	Clear();
}

/////////////////////////////////////////////////////////////////////////////

HRESULT    CCsvFile::AddRow(const TRow& row_ref)
{
	try
	{
		m_table.push_back(row_ref);
	}
	catch(::std::bad_alloc&){ return E_OUTOFMEMORY;}
	return S_OK;
}

HRESULT    CCsvFile::AppendToFile(LPCWSTR pFile, const TRow& row_ref)
{
	m_src_path = pFile;

	::ATL::CAtlFile file__;
	HRESULT hr__ =  file__.Create(pFile, FILE_APPEND_DATA, FILE_SHARE_WRITE, OPEN_ALWAYS);
	if (S_OK != hr__)
		return  hr__;
	hr__ = file__.Seek(0, FILE_END);
	if (S_OK != hr__)
		return  hr__;
	LPCWSTR pFieldDivider = details::CsvFile_GetSeparator(m_dwOptions);
	hr__ = details::CsvFile_WriteLine(file__, row_ref, pFieldDivider);
	if (S_OK == hr__)
		hr__ = file__.Flush();
	return  hr__;
}

LPCWSTR    CCsvFile::Cell(const INT row__, const INT col__) const
{
	if (row__ < 0 || col__ < 0)
		return NULL;
	if (row__ > INT(m_table.size()) - 1)
		return NULL;
	const CCsvFile::TRow& row_ref = m_table[row__];
	if (col__ > (LONG)row_ref.size() - 1)
		return NULL;
	const CStringW& cell_ref = row_ref[col__];
	return  cell_ref.GetString();
}

HRESULT    CCsvFile::Clear(void)
{
	m_result = OLE_E_BLANK;
	if (!m_header.empty())
		 m_header.clear();
	m_has_header = false;
	if (!RowCount())
		return S_FALSE;
	for (TTable::iterator t_iter = m_table.begin(); t_iter != m_table.end(); ++t_iter)
	{
		CCsvFile::TRow&  row_ref = *t_iter;
		row_ref.clear();
	}
	m_table.clear();
	m_src_size = 0;
	m_src_path.Empty();

	return S_OK;
}

HRESULT    CCsvFile::Create(const CStringW& buffer_ref, const bool bHasHeader)
{
	Clear();
	if (true == buffer_ref.IsEmpty())
		return S_FALSE;
	m_has_header = bHasHeader;
	HRESULT hr__ = S_OK;
	LPCWSTR pFieldDivider = details::CsvFile_GetSeparator(m_dwOptions);
	try
	{
		INT row__ = NULL;
		CStringW row_token__;
		row_token__ = buffer_ref.Tokenize(details::CsvFile__CSV_ROW_TOKEN, row__);
		bool bHeaderIsInitialized = false;
		while (!row_token__.IsEmpty())
		{
			CCsvFile::TRow row_data_;
			INT col__ = NULL;
			CStringW col_token__;
			col_token__ = row_token__.Tokenize(pFieldDivider, col__);
			while (!col_token__.IsEmpty())
			{
				row_data_.push_back(col_token__);
				col_token__ = row_token__.Tokenize(pFieldDivider, col__);
			}
			if (m_has_header && !bHeaderIsInitialized)
			{
				bHeaderIsInitialized = true;
				if (m_header.empty() == false)
					m_header.clear();
				m_header = row_data_;
			}
			else
			{
				m_table.push_back(row_data_);
			}
			row_token__ = buffer_ref.Tokenize(details::CsvFile__CSV_ROW_TOKEN, row__);
		}
	}
	catch(::std::bad_alloc&)
	{
		ATLASSERT(FALSE);
		hr__ = E_OUTOFMEMORY;
	}
	m_result = hr__;
	return  hr__;
}

DOUBLE     CCsvFile::Double(const INT row__, const INT col__) const
{
	LPCWSTR pCell = Cell(row__, col__);
	if (pCell && ::lstrlenW(pCell))
		return ::_tstof(pCell);
	else
		return 0.0f;
}

LONG       CCsvFile::FieldCount(void) const
{
	return (LONG)m_header.size();
}

HRESULT    CCsvFile::GetLastResult(void) const
{
	return m_result;
}

bool       CCsvFile::HasHeader(void) const
{
	return m_has_header;
}

const CCsvFile::THeader&
           CCsvFile::Header(void) const
{
	return m_header;
}

HRESULT    CCsvFile::Header(const THeader& hdr_ref)
{
	if (!m_header.empty() || !m_table.empty())
		return HRESULT_FROM_WIN32(ERROR_ALREADY_INITIALIZED);
	m_header = hdr_ref;
	m_has_header = !m_header.empty();
	return (m_result = S_OK);
}

HRESULT    CCsvFile::Load(LPCWSTR pFile, const bool bHasHeader)
{
	m_src_path = pFile;

	::ATL::CAtlFile file__;
	HRESULT hr__ = file__.Create(pFile, FILE_READ_DATA, FILE_SHARE_READ, OPEN_EXISTING);
	if (S_OK != hr__)
		return  hr__;
	ULONGLONG size__ = 0;
	hr__ = file__.Seek(0, FILE_BEGIN);
	if (S_OK != hr__)
		return  hr__;
	hr__ = file__.GetSize(size__);
	if (S_OK != hr__)
		return  hr__;

	m_src_size = (size__ & 0xFFFFFFFF);
	if (m_src_size)
	{
		const INT n__sz__  = static_cast<INT>(m_src_size);
		shared::lite::data::CByteArray ptr__(n__sz__);
		hr__ = ptr__.GetLastResult();
		if (S_OK != hr__)
			return  hr__;
		hr__ = file__.Read((LPVOID)ptr__.GetData(), n__sz__);
		if (S_OK != hr__)
			return  hr__;

		const bool bIsUnicode = details::CsvFile_IsUnicodeData(ptr__);

		CAtlString cs_buffer;
		if (bIsUnicode)
			cs_buffer.Append(
				reinterpret_cast<TCHAR*>(ptr__.GetData()),
				n__sz__ / sizeof(TCHAR)
			);
		else
		{
			CAtlStringA cs_buffer_a(
					reinterpret_cast<char*>(ptr__.GetData()),
					n__sz__ / sizeof(char)
				);
			cs_buffer = cs_buffer_a;
		}
		cs_buffer.Replace(_T("\n\r"), _T(""));
		hr__ = this->Create(cs_buffer, bHasHeader);
	}
	else
		hr__ = S_FALSE;
	return hr__;
}

const CCsvFile::TRow&
           CCsvFile::Row(const INT row__) const
{
	if (row__ < 0 || row__ > (INT)m_table.size() - 1 || S_OK != m_result)
	{
		return details::CsvFile_InvalidCsvRow_Ref();
	}
	const CCsvFile::TRow& row_ref = m_table[row__];
	return row_ref;
}

LONG       CCsvFile::RowCount(void) const
{
	const LONG cnt__ = (LONG)m_table.size();
	return cnt__;
}

HRESULT    CCsvFile::Save(LPCWSTR pFile, const bool bHasHeader)
{
	m_src_path = pFile;

	::ATL::CAtlFile file__;
	HRESULT hr__ =  file__.Create(pFile, FILE_WRITE_DATA, FILE_WRITE_DATA, CREATE_ALWAYS);
	if (S_OK != hr__)
		return  hr__;
	hr__ = file__.Seek(0, FILE_BEGIN);
	if (S_OK != hr__)
		return  hr__;
	LPCWSTR pFieldDivider = details::CsvFile_GetSeparator(m_dwOptions);
	if (bHasHeader && m_has_header)
	{
		hr__ = details::CsvFile_WriteLine(file__, m_header, pFieldDivider);
		if (S_OK != hr__)
			return  hr__;
	}
	const LONG nRows = this->RowCount();
	for ( LONG i_ = 0; i_ < nRows; i_++)
	{
		const TRow& row_ref = m_table[i_];
		hr__ = details::CsvFile_WriteLine(file__, row_ref, pFieldDivider);
		if (S_OK != hr__)
			return  hr__;
	}
	hr__ = file__.Flush();
	return hr__;
}

LPCWSTR    CCsvFile::SourceFilePath(void)const
{
	return m_src_path;
}

DWORD      CCsvFile::SourceFileSize(void)const
{
	return m_src_size;
}

/////////////////////////////////////////////////////////////////////////////

HRESULT    CCsvFile::CreateFileFromHeader(LPCWSTR pFile, const THeader& header_ref, const DWORD dwOptions)
{
	::ATL::CAtlFile file__;
	HRESULT hr__ =  file__.Create(pFile, FILE_WRITE_DATA, FILE_WRITE_DATA, CREATE_ALWAYS);
	if (S_OK != hr__)
		return  hr__;
	hr__ = file__.Seek(0, FILE_BEGIN);
	if (S_OK != hr__)
		return  hr__;
	LPCWSTR pFieldDivider = details::CsvFile_GetSeparator(dwOptions);
	hr__ = details::CsvFile_WriteLine(file__, header_ref, pFieldDivider);
	return hr__;
}

bool       CCsvFile::IsFileExist(LPCWSTR pFile)
{
	::ATL::CAtlFile file__;
	HRESULT hr__ =  file__.Create(pFile, FILE_READ_DATA, FILE_WRITE_DATA, OPEN_EXISTING);
	return (hr__ == S_OK);
}