#ifndef _SHAREDLITEGENERICAPPLICATIONOBJECT_H_920767BA_8B5E_475b_98A4_CE06582BB19E_INCLUDED
#define _SHAREDLITEGENERICAPPLICATIONOBJECT_H_920767BA_8B5E_475b_98A4_CE06582BB19E_INCLUDED
/*
	Created by Tech_dog (ebontrop@gmail.com) on 19-Mar-2014 at 8:17:25am, GMT+4, Taganrog, Wednesday;
	This is Shared Lite Generic Application Object class declaration file.
	-----------------------------------------------------------------------------
	Adopted to version v15 on 13-Feb-2021 at 1:28:17.787 pm, UTC+7, Novosibirsk, Saturday;
*/
#include <map>
#pragma comment(lib, "version.lib")

namespace shared { namespace lite { namespace common
{
	class CApplication
	{
	public:
		class CProcess
		{
		private:
			HANDLE              m_mutex;
			CStringW            m_mutex_name;
			DWORD               m_proc_state;
			CONST CApplication& m_app_obj_ref;
		public:
			explicit CProcess(CONST CApplication&);
			~CProcess(VOID);
		public:
			bool                IsSingleton(void) const;
			HRESULT             RegisterSingleton(LPCWSTR pMutexName);
			HRESULT             UnregisterSingleton(VOID);
		};
		class CVersion
		{
		private:
			mutable HRESULT     m_hResult;     // last result
			LPVOID              m_pVerInfo;    // file version info pointer
		public:
			 CVersion(void);
			~CVersion(void);
		public:
			CStringW   CompanyName(void)        const;
			CStringW   CopyRight(void)          const;
			CStringW   FileDescription(void)    const;
			CStringW   FileType(void)           const;
			CStringW   FileVersion(void)        const;
			CStringW   FileVersionFixed(void)   const;
			CStringW   InternalName(void)       const;
			CStringW   OriginalFileName(void)   const;
			CStringW   Platform(void)           const;
			CStringW   ProductName(void)        const;
			CStringW   ProductVersion(void)     const;
			CStringW   ProductVersionFixed(void)const;
		};
		class CCommandLine
		{
			typedef ::std::map<CStringW, CStringW> TArguments;
		private:
			CStringW   m_module_full_path;
			TArguments          m_args;
		public:
			 CCommandLine(void);
			~CCommandLine(void);
		public:
			CStringW   Argument(LPCWSTR pName)const;
			INT                 Count(void)const;
			bool                Has(LPCWSTR pArgName)const;
			CStringW   ModuleFullPath(void)const;
		};
	private:
		HRESULT                 m_hResult;     // last result
		mutable CStringW        m_app_name;
		CVersion                m_version;
		CProcess                m_process;
		CCommandLine            m_cmd_line;
	public:
		 CApplication(void);
		~CApplication(void);
	public:
		const CCommandLine&     CommandLine  (void) const;
		HRESULT                 GetLastResult(void) const;
		LPCWSTR                 GetName(void) const;
		HRESULT                 GetPath(CStringW& __in_out_ref) const;
		HRESULT                 GetPathFromAppFolder(LPCWSTR pPattern, CStringW& __in_out_ref) const;
		bool                    Is64bit(VOID)const;
		const CProcess&         Process(VOID)const;
		CProcess&               Process(VOID);
		const CVersion&         Version(VOID)const;
	public:
		static bool             IsRelatedToAppFolder(LPCWSTR pPath);
	private:
		CApplication(const CApplication&);
		CApplication& operator= (const CApplication&);
	};

	class CApplicationIconLoader
	{
	private:
		HICON   m__small;
		HICON   m__big;
	public:
		 CApplicationIconLoader(const UINT nIconId); // it is assumed that resource identifier points to multiframe icon resource
		 CApplicationIconLoader(const UINT nSmallIconId, const UINT nBigIconId);
		~CApplicationIconLoader(void);
	public:
		HICON   DetachBigIcon(void);
		HICON   DetachSmallIcon(void);
		HICON   GetBigIcon(void) const;
		HICON   GetSmallIcon(void) const;
	private:
		CApplicationIconLoader(const CApplicationIconLoader&);
		CApplicationIconLoader& operator= (const CApplicationIconLoader&);
	};

	class CApplicationCursor
	{
	private:
		bool    m_resource_owner;
	public:
		CApplicationCursor(LPCWSTR lpstrCursor = IDC_WAIT);
		~CApplicationCursor(void);
	};
}}}

#endif/*_SHAREDLITEGENERICAPPLICATIONOBJECT_H_920767BA_8B5E_475b_98A4_CE06582BB19E_INCLUDED*/