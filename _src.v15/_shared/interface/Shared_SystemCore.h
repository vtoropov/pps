#ifndef _SHAREDLITESYSTEMCORE_H_5D2FFEE1_0785_4c17_ABF2_99598CF8D1FB_INCLUDED
#define _SHAREDLITESYSTEMCORE_H_5D2FFEE1_0785_4c17_ABF2_99598CF8D1FB_INCLUDED
/*
	Created by Tech_dog (VToropov) on 19-Mar-2014 at 9:47:36am, GMT+4, Taganrog, Wednesday;
	This is Shared Lite Operating System Core class(es) declaration file.
*/
#include "Shared_SystemError.h"
namespace shared { namespace lite { namespace sys_core
{
	using shared::lite::common::CSysError;

	class CComAutoInitializer
	{
	private:
		CSysError   m_error;
	public:
		CComAutoInitializer(const bool bMultiThreaded);
		~CComAutoInitializer(void);
	public:
		CONST CSysError& Error(VOID) CONST;
		bool             IsSuccess(VOID) CONST;
	private:
		CComAutoInitializer(const CComAutoInitializer&);
		CComAutoInitializer& operator= (const CComAutoInitializer&);
	};
}}}

#endif/*_SHAREDLITESYSTEMCORE_H_5D2FFEE1_0785_4c17_ABF2_99598CF8D1FB_INCLUDED*/