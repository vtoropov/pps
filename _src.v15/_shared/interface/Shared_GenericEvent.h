#ifndef _SHAREDLITEGENERICEVENT_H_5DDFC5F2_3B4D_4fce_AB36_1E1683F267B8_INCLUDED
#define _SHAREDLITEGENERICEVENT_H_5DDFC5F2_3B4D_4fce_AB36_1E1683F267B8_INCLUDED
/*
	Created by Tech_dog (ebontrop@gmail.com) on 19-Mar-2014 at 11:06:15am, GMT+4, Taganrog, Wednesday;
	This is Shared Lite Generic Event class declaration file.
	-----------------------------------------------------------------------------
	Adopted to VS15 on 13-Feb-2021 at 11:48:27.664 am, UTC+7, Novosibirsk, Saturday;
*/

namespace shared { namespace lite { namespace events
{
	interface IGenericEventNotify
	{
		virtual HRESULT     GenericEvent_OnNotify(const UINT eventId) PURE;
	};

	class CGenericEvent
	{
		enum {
			WM_INTERNAL_MSG = WM_USER + 1,
		};
	protected:
		class CMessageHandler:
			public ::ATL::CWindowImpl<CMessageHandler>
		{
		private:
			IGenericEventNotify&    m_sink_ref;
			const UINT              m_event_id;
		public:
			BEGIN_MSG_MAP(CMessageHandler)
				MESSAGE_HANDLER(CGenericEvent::WM_INTERNAL_MSG, OnGenericEventNotify)
			END_MSG_MAP()
		private:
			virtual LRESULT  OnGenericEventNotify(UINT, WPARAM, LPARAM, BOOL&);
		public:
			CMessageHandler(IGenericEventNotify&, const UINT eventId);
			virtual ~CMessageHandler(void);
		};
	protected:
		CMessageHandler      m_handler;
	public:
		CGenericEvent(IGenericEventNotify&, const UINT eventId);
		virtual ~CGenericEvent(void);
	public:
		virtual HRESULT      Create(void);
		virtual HRESULT      Destroy(void);
		virtual HWND         GetHandle_Safe(void) const;
	private:
		CGenericEvent(const CGenericEvent&);
		CGenericEvent& operator= (const CGenericEvent&);
	public:
		virtual HRESULT  Fire(const bool bAsynch = true);
		virtual HRESULT  Fire(const bool bAsynch, const UINT evtId);
	public:
		static  HRESULT  Fire(const HWND hHandler, const bool bAsynch = true);
		static  HRESULT  Fire(const HWND hHandler, const bool bAsynch, const UINT evtId);
	};
}}}

#endif/*_SHAREDLITEGENERICEVENT_H_5DDFC5F2_3B4D_4fce_AB36_1E1683F267B8_INCLUDED*/