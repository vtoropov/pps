#ifndef _SHAREDLITEGENERICSYNCOBJECT_H_9881D26A_CA3F_4b22_843A_AAB467B560FF_INCLUDED
#define _SHAREDLITEGENERICSYNCOBJECT_H_9881D26A_CA3F_4b22_843A_AAB467B560FF_INCLUDED
/*
	Created by Tech_dog (VToropov) on 19-Mar-2014 at 7:41:20am, GMT+4, Taganrog, Wednesday;
	This is Shared Lite Generic Synchronize Object class definition file.
*/

namespace shared { namespace lite { namespace runnable
{
	class CGenericSyncObject
	{
	protected:
		mutable CRITICAL_SECTION m_sec;
	public:
		CGenericSyncObject(void);
		virtual ~CGenericSyncObject(void);
	public:
		void Lock(void) const;
		void Unlock(void) const;
	private:
		CGenericSyncObject(const CGenericSyncObject&);
		CGenericSyncObject& operator= (const CGenericSyncObject&);
	};

	template<typename TLocker>
	class CGenericAutoLocker
	{
	private:
		TLocker&    m_locker_ref;
	public:
		CGenericAutoLocker(TLocker& locker_ref) : m_locker_ref(locker_ref)
		{
			m_locker_ref.Lock();
		}
		~CGenericAutoLocker(void)
		{
			m_locker_ref.Unlock();
		}
	private:
		CGenericAutoLocker(const CGenericAutoLocker&);
		CGenericAutoLocker& operator= (const CGenericAutoLocker&);
	};
}}}

typedef const shared::lite::runnable::CGenericAutoLocker<const shared::lite::runnable::CGenericSyncObject> TAutoLocker;

#define SAFE_LOCK(cs) TAutoLocker    tAutoLocker(cs);
#define SAFE_LOCK_THIS() TAutoLocker tAutoLocker(*this);

#endif/*_SHAREDLITEGENERICSYNCOBJECT_H_9881D26A_CA3F_4b22_843A_AAB467B560FF_INCLUDED*/