#ifndef _SHAREDLITEPERSISTENTSTORAGE_H_7E2C8E5E_F55B_40f8_9A14_AF78E1ABA691_INCLUDED
#define _SHAREDLITEPERSISTENTSTORAGE_H_7E2C8E5E_F55B_40f8_9A14_AF78E1ABA691_INCLUDED
/*
	Created by Tech_dog (ebontrop@gmail.com) on 19-Mar-2014 at 11:44:29am, GMT+4, Taganrog, Wednesday;
	This is Shared Lite Persistent Storage class(es) declaration file.
	-----------------------------------------------------------------------------
	Adopted to VS15 on 13-Feb-2021 at 1:21:19.482 pm, UTC+7, Novosibirsk, Saturday;
*/
namespace shared { namespace lite { namespace persistent { namespace factories
{
	class CPrivateProfileSectionFactory;
}}}}

namespace shared { namespace lite { namespace persistent
{
	using shared::lite::persistent::factories::CPrivateProfileSectionFactory;

	class CPrivateProfile;
	class CPrivateProfileItem
	{
	private:
		CStringW             m_name;
		CStringW             m_value;
		CStringW             m_comment;
	public:
		 CPrivateProfileItem(void);
		 CPrivateProfileItem(LPCWSTR pName, LPCWSTR pValue, LPCWSTR pComment);
		~CPrivateProfileItem(void);
	public:
		const CStringW&      Comment(void) const;
		CStringW&            Comment(void);
		bool                 IsValid(void) const;
		const CStringW&      Name(void) const;
		CStringW&            Name(void);
		const CStringW&      Value(void) const;
		CStringW&            Value(void);
	public:
		CPrivateProfileItem(const CPrivateProfileItem&);
		CPrivateProfileItem& operator= (const CPrivateProfileItem&);
	};

	class CPrivateProfileSection
	{
		friend class CPrivateProfileSectionFactory;
		friend class CPrivateProfile;
		typedef ::std::map<CStringW, INT>   TItemsIndex; // by pair name
		typedef ::std::vector<CPrivateProfileItem>   TItemsArray;
	private:
		TItemsIndex          m_index;
		TItemsArray          m_items;
		CStringW             m_name;
	public:
		 CPrivateProfileSection(void);
		 CPrivateProfileSection(LPCWSTR pName);
		~CPrivateProfileSection(void);
	public:
		HRESULT                       AddItem(LPCWSTR pszName);
		bool                          IsEmpty(void) const;
		bool                          IsValid(void) const;
		INT                           ItemCount(void) const;         // a number of elements in the section
		const CPrivateProfileItem&    ItemOf(const INT nIndex) const;
		CPrivateProfileItem&          ItemOf(const INT nIndex);
		const CPrivateProfileItem&    ItemOf(LPCWSTR pszName) const;
		CPrivateProfileItem&          ItemOf(LPCWSTR pszName);
		const CStringW&      Name(void) const;              // section name accessor [read only]
		CStringW&            Name(void);                    // section name accessor
	public:
		CPrivateProfileSection(const CPrivateProfileSection&);
		CPrivateProfileSection& operator= (const CPrivateProfileSection&);
	};

	class CPrivateProfile
	{
	public:
		enum eStorageMode
		{
			SM_UseRegular      = 0x0,    // default
			SM_UseGlobalIndex  = 0x1,    // all data are saved in one table, an access key is composed by section name + separator (optional) + item name
		};
	private:
		friend class CPrivateProfile;
		typedef ::std::map<CStringW, INT>      TSectionIndex; // by section name
		typedef ::std::vector<CPrivateProfileSection>   TSectionArray;
	private:
		TSectionIndex                 m_index;
		TSectionArray                 m_sections;
		HRESULT                       m_result;
		eStorageMode                  m_store_mode;
		CAtlString                    m_store_path;
	public:
		CPrivateProfile(void);
		CPrivateProfile(LPCWSTR pFile);
		~CPrivateProfile(void);
	public:
		HRESULT                       AddSection(LPCWSTR pszSection);
		HRESULT                       Create(LPCWSTR pszFile);
		HRESULT                       CreateGlobal  (LPCWSTR pszFile, LPCWSTR p__key__separator = NULL);
		bool                          GetBoolValue  (LPCWSTR pszSection, LPCWSTR pszItem, const bool bDefaultValue)const;
		DWORD                         GetDwordValue (LPCWSTR pszSection, LPCWSTR pszItem, const DWORD dwDefaultValue)const;
		HRESULT                       GetLastResult (void) const;
		eStorageMode                  GetStorageMode(void) const;
		CAtlString                    GetStringValue(LPCWSTR pszSection, LPCWSTR pszItem, LPCWSTR pszDefaultValue)const;
		bool                          IsEmpty(void) const;
		bool                          IsValid(void) const;
		INT                           SectionCount(void) const;         // a number of sections in the profile
		const CPrivateProfileSection& SectionOf(const INT nIndex)const;
		CPrivateProfileSection&       SectionOf(const INT nIndex);
		const CPrivateProfileSection& SectionOf(LPCWSTR pName)const;
		CPrivateProfileSection&       SectionOf(LPCWSTR pName);
		HRESULT                       SetStringValue(LPCWSTR pszSection, LPCWSTR pszItem, LPCWSTR pszValue);
	private:
		CPrivateProfile(const CPrivateProfile&);
		CPrivateProfile& operator= (const CPrivateProfile&);
	};

	namespace factories
	{
		using shared::lite::persistent::CPrivateProfileSection;

		class CPrivateProfileSectionFactory
		{
		private:
			CPrivateProfileSection&     m__sec__ref;
		public:
			CPrivateProfileSectionFactory(CPrivateProfileSection&);
			~CPrivateProfileSectionFactory(void);
		public:
			HRESULT        Clear(void);
		};
	}

	class CRegistryStorage
	{
	private:
		HKEY         m_hRoot;
	public:
		CRegistryStorage(const HKEY hRoot);
		~CRegistryStorage(void);
	public:
		HRESULT      Load(LPCWSTR pFolder, LPCWSTR pProperty, CStringW& __in_out_value);
		HRESULT      Load(LPCWSTR pFolder, LPCWSTR pProperty, LONG& __in_out_value);
		HRESULT      Save(LPCWSTR pFolder, LPCWSTR pProperty, const CStringW& __in_value);
		HRESULT      Save(LPCWSTR pFolder, LPCWSTR pProperty, const LONG __in_value);
	private:
		CRegistryStorage(const CRegistryStorage&);
		CRegistryStorage& operator= (const CRegistryStorage&);
	};

	class eCsvFileOption
	{
	public:
		enum _enum{
			eNone              = 0x0,
			eUseCommaSeparator = 0x1,
			eUseSemicolon      = 0x2,
		};
	};

	class CCsvFile
	{
	public:
		typedef ::std::vector<CStringW>  THeader;
		typedef ::std::vector<CStringW>  TRow;
	private:
		typedef ::std::vector<TRow>               TTable;
	private:
		const DWORD     m_dwOptions;
		mutable HRESULT m_result;
		THeader         m_header;
		TTable          m_table;
		bool            m_has_header;
		DWORD           m_src_size;        // source file size in bytes;
		CAtlString      m_src_path;
	public:
		CCsvFile(const DWORD dwOptions = eCsvFileOption::eNone);
		~CCsvFile(void);
	public:
		HRESULT         AddRow(const TRow&);
		HRESULT         AppendToFile(LPCWSTR pFile, const TRow&);         // opens the file specified and appends the row to the file
		LPCWSTR         Cell(const INT row__, const INT col__) const;
		HRESULT         Clear(void);
		HRESULT         Create(const CStringW& buffer_ref, const bool bHasHeader = false);
		DOUBLE          Double(const INT row__, const INT col__) const;
		LONG            FieldCount(void) const;
		HRESULT         GetLastResult(void) const;
		bool            HasHeader(void) const;
		const THeader&  Header(void) const;
		HRESULT         Header(const THeader&);
		HRESULT         Load(LPCWSTR pFile, const bool bHasHeader = false);
		const TRow&     Row(const INT row__) const;
		LONG            RowCount(void) const;
		HRESULT         Save(LPCWSTR pFile, const bool bHasHeader = false);
		LPCWSTR         SourceFilePath(void)const;
		DWORD           SourceFileSize(void)const;                        // gets source file size in bytes
	public:
		static HRESULT  CreateFileFromHeader(LPCWSTR lpszFilePath, const THeader&, const DWORD dwOptions);
		static bool     IsFileExist(LPCWSTR lpszFilePath);
	};
}}}

#endif/*_SHAREDLITEPERSISTENTSTORAGE_H_7E2C8E5E_F55B_40f8_9A14_AF78E1ABA691_INCLUDED*/