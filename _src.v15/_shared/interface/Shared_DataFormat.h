#ifndef _SHAREDLITEDATETIMEFORMAT_H_371B4EE5_2144_4248_BE81_2856CDBC8530_INCLUDED
#define _SHAREDLITEDATETIMEFORMAT_H_371B4EE5_2144_4248_BE81_2856CDBC8530_INCLUDED
/*
	Created by Tech_dog (ebontrop@gmail.com) on 20-Mar-2014 at 7:49:48pm, GMT+4, Taganrog, Thursday;
	This is Shared Lite DateTime Utility class declaration file.
	-----------------------------------------------------------------------------
	Adopted to VS15 on 15-Feb-2021 at 4:08:40.319 am, UTC+7, Novosibirsk, Monday;
*/

namespace shared { namespace lite { namespace format
{
	class eDataFormatType
	{
	public:
		enum _enum {
			eNone      = 0x0,
			eDateShort = 0x1,
			eTimeShort = 0x2,
		};
	};

	class CDateTimeFormat
	{
	private:
		mutable CStringW   m_buffer;
	public:
		 CDateTimeFormat(void);
		~CDateTimeFormat(void);
	public:
		CStringW   GetFormatString(const DWORD dFlags) const;
		LPCWSTR    ToDate(const SYSTEMTIME&) const;
		LPCWSTR    ToDate(const SYSTEMTIME&, LPCWSTR pCustomPattern) const;
		LPCWSTR    ToDateTime(const SYSTEMTIME&) const;
		LPCWSTR    ToDateTime(const SYSTEMTIME& dt_ref, const SYSTEMTIME& tm_ref) const;
		LPCWSTR    ToTime(const SYSTEMTIME&) const;
		LPCWSTR    ToTime(const SYSTEMTIME&, LPCWSTR pCustomPattern) const;
	public:
		static LPCWSTR    GetDefaultDateFormat(void);
		static LPCWSTR    GetDefaultDateTimeFormat(void);
		static LPCWSTR    GetDefaultTimeFormat(void);
	};
}
namespace data 
{
	class CTimestamp
	{
	private:
		time_t     m_value;
	public:
		CTimestamp(void);
	public:
		VOID       SetCurrentTime(void);
		time_t     Value(void)const;
		VOID       Value(const time_t);
		CStringW   ValueAsFormattedText(void)const;
		CStringW   ValueAsText(void)const;
		HRESULT    ValueAsText(LPCWSTR pszTimestamp, const bool bValidateData);
	};
}}}

#endif/*_SHAREDLITEDATETIMEFORMAT_H_371B4EE5_2144_4248_BE81_2856CDBC8530_INCLUDED*/