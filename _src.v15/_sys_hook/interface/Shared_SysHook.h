/*
	Created by Tech_dog(ebontrop@gmail.com) on 5-Mar-2015 at 12:54:34pm, GMT+3, Taganrog, Thursday;
	This is Shared System Hook Exported functions declaration file.
	-----------------------------------------------------------------------------
	Adopted to version v15 on 16-Feb-2021 at 5:24:33.306 am, UTC+7, Novosibirsk, Tuesday;
*/

#if defined(SHARED_SYSHOOK_EXPORTS)
	extern __declspec(dllexport) HRESULT InstallSysHook(const BOOL bUseBeep);
	extern __declspec(dllexport) BOOL    IsInstalled(VOID);
	extern __declspec(dllexport) HRESULT UninstallSysHook(VOID);
#else
	extern __declspec(dllimport) HRESULT InstallSysHook(const BOOL bUseBeep);
	extern __declspec(dllimport) BOOL    IsInstalled(VOID);
	extern __declspec(dllimport) HRESULT UninstallSysHook(VOID);
#endif