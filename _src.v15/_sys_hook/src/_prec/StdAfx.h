#ifndef _SHAREDSYSHOOKPRECOMPILEDHEADER_H_929EF5EC_E8A3_4b32_9DCB_84BFA1A632C5_INCLUDED
#define _SHAREDSYSHOOKPRECOMPILEDHEADER_H_929EF5EC_E8A3_4b32_9DCB_84BFA1A632C5_INCLUDED
/*
	Created by Tech_dog (ebontrop@gmail.com) on 5-Mar-2015 at 12:02:14pm, GMT+3, Taganrog, Thursday;
	This is Shared System Hook library precompiled headers' declaration file.
	-----------------------------------------------------------------------------
	Adopted to version v15 on 16-Feb-2021 at 5:26:03.206 am, UTC+7, Novosibirsk, Tuesday;
*/

#ifndef WINVER                 // Specifies that the minimum required platform is Windows XP SP2.
#define WINVER 0x0501          // Change this to the appropriate value to target other versions of Windows.
#endif

#ifndef _WIN32_WINNT           // Specifies that the minimum required platform is Windows XP SP2.
#define _WIN32_WINNT 0x0501    // Change this to the appropriate value to target other versions of Windows.
#endif

#ifndef _WIN32_WINDOWS         // Specifies that the minimum required platform is Windows XP SP2.
#define _WIN32_WINDOWS 0x0501  // Change this to the appropriate value to target Windows Me or later.
#endif

#ifndef _WIN32_IE              // Specifies that the minimum required platform is Internet Explorer 7.0.
#define _WIN32_IE 0x0700       // Change this to the appropriate value to target other versions of IE.
#endif

#define WIN32_LEAN_AND_MEAN    // Exclude rarely-used stuff from Windows headers

#include <windows.h>

#define _ATL_CSTRING_EXPLICIT_CONSTRUCTORS

#include <atlbase.h>
#include <atlstr.h>

#endif/*_SHAREDSYSHOOKPRECOMPILEDHEADER_H_929EF5EC_E8A3_4b32_9DCB_84BFA1A632C5_INCLUDED*/