/*
	Created by Tech_dog (ebontrop@gmail.com) on 5-Mar-2015 at 12:12:59pm, GMT+3, Taganrog, Thursday;
	This is Shared System Hook Exported functions implementation file.
	-----------------------------------------------------------------------------
	Adopted to version v15 on 16-Feb-2021 at 5:33:45.886 am, UTC+7, Novosibirsk, Tuesday;
*/
#include "StdAfx.h"
#include "Shared_SysHook.h"

/////////////////////////////////////////////////////////////////////////////

#pragma data_seg (".mydata")

HHOOK g_hHookKbdLL = NULL;
BOOL  g_bBeep = FALSE;

#pragma data_seg ()
#pragma comment(linker, "/SECTION:.mydata,RWS")

/////////////////////////////////////////////////////////////////////////////

LRESULT CALLBACK Shared_SysHookLL(int nCode, WPARAM wp, LPARAM lp)
{
	KBDLLHOOKSTRUCT* pkh = reinterpret_cast<KBDLLHOOKSTRUCT*>(lp);

	if (HC_ACTION == nCode)
	{
		const BOOL bCtrlKeyDown = ::GetAsyncKeyState(VK_CONTROL)>>((sizeof(SHORT) * 8) - 1);

		if ((pkh->vkCode == VK_ESCAPE && bCtrlKeyDown)              || // Ctrl + Esc
			(pkh->vkCode == VK_TAB    && pkh->flags & LLKHF_ALTDOWN)|| // Alt  + TAB
			(pkh->vkCode == VK_ESCAPE && pkh->flags & LLKHF_ALTDOWN)|| // Alt  + Esc
			(pkh->vkCode == VK_LWIN   || pkh->vkCode == VK_RWIN))
		{
			if (g_bBeep && (wp == WM_SYSKEYDOWN || wp == WM_KEYDOWN))
				MessageBeep(0);
			return TRUE;
		}
	}
	return ::CallNextHookEx(g_hHookKbdLL, nCode, wp, lp);
}

/////////////////////////////////////////////////////////////////////////////

HRESULT InstallSysHook(const BOOL bUseBeep)
{
	if (g_hHookKbdLL)
		return HRESULT_FROM_WIN32(ERROR_OBJECT_ALREADY_EXISTS);

	const HINSTANCE hInstance = ::ATL::_AtlBaseModule.GetModuleInstance();//::GetModuleHandle(NULL)

	g_hHookKbdLL = ::SetWindowsHookEx(WH_KEYBOARD_LL, Shared_SysHookLL, hInstance, 0);
	if (!g_hHookKbdLL)
		return HRESULT_FROM_WIN32(::GetLastError());
	else
	{
		g_bBeep = bUseBeep;
		return S_OK;
	}
}

BOOL    IsInstalled(VOID)
{
	return (NULL != g_hHookKbdLL);
}

HRESULT UninstallSysHook(VOID)
{
	if (!g_hHookKbdLL)
		return S_FALSE;

	::UnhookWindowsHookEx(g_hHookKbdLL);
	g_hHookKbdLL = NULL;
	g_bBeep = FALSE;

	return S_OK;
}