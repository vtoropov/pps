#ifndef _UIXCTRLCOMMONDEFINITIONS_H_AD2F5A63_32F0_40b2_935F_DFEBF2590E79_INCLUDED
#define _UIXCTRLCOMMONDEFINITIONS_H_AD2F5A63_32F0_40b2_935F_DFEBF2590E79_INCLUDED
/*
	Created by Tech_dog (ebontrop@gmail.com) on 9-Feb-2015 at 7:23:30pm, GMT+3, Taganrog, Monday;
	This is UIX Control common definition/class declaration file.
	-----------------------------------------------------------------------------
	Adopted to version v15 on 16-Feb-2021 at 6:27:12.612 am, UTC+7, Novosibirsk, Tuesday;
*/
#include "UIX_GdiProvider.h"

namespace ex_ui { namespace controls { namespace defs
{
	interface IControlNotify
	{
		virtual   HRESULT  IControlNotify_OnClick(const UINT ctrlId) PURE;
		virtual   HRESULT  IControlNotify_OnClick(const UINT ctrlId, const LONG_PTR nData) { ctrlId; nData;  return E_NOTIMPL;}
	};
}}}

#endif/*_UIXCTRLCOMMONDEFINITIONS_H_AD2F5A63_32F0_40b2_935F_DFEBF2590E79_INCLUDED*/