#ifndef _UIXCTRLCONTROLBASE_H_66B550AE_B93F_4fdd_8A5A_FB817B14DBF6_INCLUDED
#define _UIXCTRLCONTROLBASE_H_66B550AE_B93F_4fdd_8A5A_FB817B14DBF6_INCLUDED
/*
	Created by Tech_dog (ebontrop@gmail.com) on 8-Feb-2015 at 5:05:12pm, GMT+3, Taganrog, Sunday;
	This is UIX Control custom control base class(es) declaration file.
	-----------------------------------------------------------------------------
	Adopted to version v15 on 16-Feb-2021 at 6:29:01.848 am, UTC+7, Novosibirsk, Tuesday;
*/
#include "UIX_CommonCtrlDefs.h"
#include "UIX_GdiObject.h"
#include "UIX_GdiProvider.h"

namespace ex_ui { namespace controls
{
	using ex_ui::draw::defs::IRenderer;
	using ex_ui::draw::common::CColour;
	using ex_ui::controls::defs::IControlNotify;

	class CRendererDefaultImpl : public IRenderer
	{
	public:
		CRendererDefaultImpl(void);
		virtual ~CRendererDefaultImpl(void);
	private: // IRenderer
		virtual HRESULT   DrawParentBackground(const HWND hChild, const HDC hSurface, const RECT& rcDrawArea) override sealed;
	};

	class CControlCrt
	{
	protected:
		IControlNotify&   m_sink_ref;
		IRenderer&        m_renderer_ref;    // parent window renderer
		UINT              m_ctrlId;
		CColour           m_bk_clr;
	public:
		 CControlCrt(const UINT ctrlId, CControlCrt&);
		 CControlCrt(const UINT ctrlId, IRenderer& parent_rnd, IControlNotify&);
		~CControlCrt(void);
	public:
		const CColour&    BackColor(void)const;
		CColour&          BackColor(void);
		VOID              BackColor(const COLORREF, const BYTE _alpha);
		IControlNotify&   CtrlEventSink_Ref(void);
		UINT              CtrlId(void) const;
		IRenderer&        CtrlParentRenderer_Ref(void);
	};

	using ex_ui::draw::common::CImageCache;
	using ex_ui::draw::CZBuffer;

	class eControlState
	{
	public:
		enum _enum {
			eUndefined   = 0x00,
			eNormal      = 0x01,
			eDisabled    = 0x02,
			eSelected    = 0x04,
			eHovered     = 0x08,
			ePressed     = 0x10,
		};
	};

	class CControlBase
	{
	public:
		typedef struct tag_ControlState
		{
			DWORD  dwState;
			tag_ControlState(void) : dwState(eControlState::eNormal) {}
			bool IsDisabled(void) const { return (0 != (eControlState::eDisabled & dwState)); }
			bool IsHovered(void)  const { return (0 != (eControlState::eHovered  & dwState)); }
			bool IsPressed(void)  const { return (0 != (eControlState::ePressed  & dwState)); }
			void Reset(void)            { dwState = eControlState::eNormal; }
		} ControlState;
	protected:
		CImageCache         m_images;
		CControlCrt&        m_crt;
		ControlState        m_state;
		bool                m_bTracked;
		::ATL::CWindow&     m_ctrlWnd;
	protected:
		 CControlBase(::ATL::CWindow& ctrlWnd, CControlCrt&);
		~CControlBase(void);
	public:
		HRESULT             GetImageSize(SIZE&) const;
		HRESULT             GetImageSize(const DWORD dState, SIZE&) const;
		const ControlState& GetState(void) const;
		HRESULT             Refresh(const bool bAsynch);
		HRESULT             SetImage(const DWORD dwState, const UINT nResId, const HMODULE hResourceModule = NULL);
	public:
		virtual void        DrawBkgnd(CZBuffer&);
		virtual void        DrawImage(CZBuffer&);
		virtual void        DrawImage(CZBuffer&, const RECT& rcDrawArea);
		virtual void        DrawImage(CZBuffer&, const RECT& rcDrawArea, const DWORD dState);
		virtual void        GetDefaultSize(SIZE&) PURE;
		virtual HRESULT     RecalcWindowRect(RECT& _rc_out);
		virtual void        SetMouseTrack(void);
	private:
		CControlBase(const CControlBase&);
		CControlBase& operator= (const CControlBase&);
	};
}}

#endif/*_UIXCTRLCONTROLBASE_H_66B550AE_B93F_4fdd_8A5A_FB817B14DBF6_INCLUDED*/