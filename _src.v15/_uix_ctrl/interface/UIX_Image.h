#ifndef _UIXCTRLIMAGECONTROL_H_C9D587B2_CD6B_4c21_B458_E66E9D4EA274_INCLUDED
#define _UIXCTRLIMAGECONTROL_H_C9D587B2_CD6B_4c21_B458_E66E9D4EA274_INCLUDED
/*
	Created by Tech_dog (ebontrop@gmail.com) on 7-Feb-2015 at 6:04:58pm, GMT+3, Taganrog, Saturday;
	This is UIX library PNG image control class declaration file.
	-----------------------------------------------------------------------------
	Adopted to version v15 on 16-Feb-2021 at 6:50:07.938 am, UTC+7, Novosibirsk, Tuesday;
*/
#include "UIX_CommonCtrlDefs.h"
#include "UIX_PngWrap.h"
#include "UIX_GdiObject.h"

namespace ex_ui { namespace controls { namespace _impl
{
	class CImageShaperDefaultImpl;
	class CImageRendererDefaultImpl;
}}}

namespace ex_ui { namespace controls
{
	using ex_ui::controls::defs::IControlNotify;
	using ex_ui::draw::defs::IRenderer;
	using ex_ui::draw::common::CPngBitmapPtr;
	using ex_ui::draw::common::CColour;

	using ex_ui::controls::_impl::CImageRendererDefaultImpl;
	using ex_ui::controls::_impl::CImageShaperDefaultImpl;

	class eImageStyle
	{
	public:
		enum _e{
			eNone       = 0x0000,
			eBorder     = 0x0001,
			eStretch    = 0x0002,
			eOpaque     = 0x0004,
		};
	};

	class CImage
	{
	private:
		class CImageWnd : 
			public  ::ATL::CWindowImpl<CImageWnd>
		{
			friend class CImage;
			typedef ::ATL::CWindowImpl<CImageWnd> TWindow;
		private:
			CPngBitmapPtr    m_image_ptr;
			HRESULT          m_hResult;       // initialisation result
			DWORD            m_dwStyle;
			CImage&          m_ctrl_ref;
			IControlNotify*  m_pNotify;
			IRenderer*       m_pParentRenderer;
		private:
			CImageRendererDefaultImpl* m_pRenderer;
			CImageShaperDefaultImpl*   m_pShaper;
		public:
			 CImageWnd(CImage&);
			~CImageWnd(void);
		public:
			DECLARE_WND_CLASS(_T("ex_ui::controls::Image::Window"));

			BEGIN_MSG_MAP(CImageWnd)
				MESSAGE_HANDLER(WM_CREATE     ,   OnCreate    )
				MESSAGE_HANDLER(WM_DESTROY    ,   OnDestroy   )
				MESSAGE_HANDLER(WM_ERASEBKGND ,   OnErase     )
				MESSAGE_HANDLER(WM_LBUTTONDOWN,   OnLButtonDn )
			END_MSG_MAP()
		private:
			LRESULT OnCreate    (UINT, WPARAM, LPARAM, BOOL&);
			LRESULT OnDestroy   (UINT, WPARAM, LPARAM, BOOL&);
			LRESULT OnLButtonDn (UINT, WPARAM, LPARAM, BOOL&);
			LRESULT OnErase     (UINT, WPARAM, LPARAM, BOOL&);
		private:
			HRESULT  _UpdateLayout(void);
		};
	private:
		CImageWnd            m_wnd;
		UINT                 m_ctrlId;
		UINT                 m_resId;
		CColour              m_bk_clr;
	public:
		 CImage(IRenderer* pParentRenderer = NULL);
		~CImage(void);
	public:
		const CColour&       BackColor(void)const;
		VOID                 BackColor(const COLORREF, const BYTE _alpha);
		HRESULT              Clear(void);
		HRESULT              Create(const HWND hParent, const RECT& rcArea, const HWND hAfter = NULL, const UINT ctrlId = 0);
		HRESULT              Destroy(void);
		const CPngBitmapPtr& GetBitmapPtrRef(void)const;
		UINT                 GetIdentifier(void)const;
		IRenderer*           GetParentRendererPtr(void)const;
		IRenderer*           GetRendererPtr(void)const;
		UINT                 GetResourceId(void)const;
		HRESULT              GetSize(SIZE&) const;
		::ATL::CWindow&      GetWindow_Ref(void);
		HRESULT              Refresh(void);
		HRESULT              SetImage(const UINT nResId);
		HRESULT              SetParentNotifyPtr(IControlNotify*);
		HRESULT              SetParentRendererPtr(IRenderer*);
		DWORD                Style(void) const;
		HRESULT              Style(const DWORD);
		HRESULT              UpdateLayout(void);
	private:
		CImage(const CImage&);
		CImage& operator= (const CImage&);
	};
}}

#endif/*_UIXCTRLIMAGECONTROL_H_C9D587B2_CD6B_4c21_B458_E66E9D4EA274_INCLUDED*/