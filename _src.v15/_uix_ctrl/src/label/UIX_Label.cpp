/*
	Created by Tech_dog (ebontrop@gmail.com) on 17-Mar-2015 at 2:30:56pm, GMT+3, Taganrog, Tueday;
	This is UIX library label control class implementation file.
	-----------------------------------------------------------------------------
	Adopted to version v15 on 16-Feb-2021 at 6:43:00.410 am, UTC+7, Novosibirsk, Tuesday;
*/
#include "StdAfx.h"
#include "UIX_Label.h"
#include "UIX_LabelWnd.h"

using namespace ex_ui;
using namespace ex_ui::controls;
/////////////////////////////////////////////////////////////////////////////

CLabel::CLabel(const CControlCrt crt) : m_wnd_ptr(NULL), m_crt(crt)
{
	try { m_wnd_ptr = new CLabelWnd(m_crt); } catch(::std::bad_alloc&){ ATLASSERT(FALSE); }
}

CLabel::~CLabel(void)
{
	if (m_wnd_ptr)
	{
		try { delete m_wnd_ptr; m_wnd_ptr = NULL;} catch(...){ ATLASSERT(FALSE); }
	}
}

/////////////////////////////////////////////////////////////////////////////

HRESULT   CLabel::Create(const HWND hParent, const RECT& rcArea, LPCWSTR pszText)
{
	if (!::IsWindow(hParent))
		return OLE_E_INVALIDHWND;
	if (!m_wnd_ptr)
		return OLE_E_BLANK;

	RECT rc_ = rcArea;
	
	m_wnd_ptr->Create(hParent, rc_, pszText, WS_CHILD|WS_VISIBLE, 0, m_crt.CtrlId());
	if (FALSE == m_wnd_ptr->IsWindow())
		return HRESULT_FROM_WIN32(::GetLastError());
	return S_OK;
}

HRESULT   CLabel::Destroy(void)
{
	if (NULL == m_wnd_ptr)
		return OLE_E_BLANK;
	if (!(*m_wnd_ptr).IsWindow())
		return S_FALSE;
	(*m_wnd_ptr).SendMessage(WM_CLOSE);
	return S_OK;
}

HRESULT   CLabel::ForeColor(const COLORREF clrFore)
{
	if (NULL == m_wnd_ptr)
		return OLE_E_BLANK;
	HRESULT hr_ = m_wnd_ptr->ForeColor(clrFore);
	if (S_OK == hr_)
		m_wnd_ptr->Invalidate(TRUE);
	return hr_;
}

CWindow   CLabel::GetWindow(void) const
{
	::ATL::CWindow wnd;
	if (m_wnd_ptr) wnd = *m_wnd_ptr;
	return wnd;
}

HRESULT   CLabel::Text(LPCWSTR pText)
{
	if (!m_wnd_ptr || !m_wnd_ptr->IsWindow())
		return OLE_E_BLANK;
	const BOOL bResult = m_wnd_ptr->SetWindowText(pText);
	return (bResult ? S_OK : HRESULT_FROM_WIN32(::GetLastError()));
}