#ifndef _UIXCTRLLABELCONTROLWND_H_1A902F7E_258D_4e3c_9427_78CAA6B67D43_INCLUDED
#define _UIXCTRLLABELCONTROLWND_H_1A902F7E_258D_4e3c_9427_78CAA6B67D43_INCLUDED
/*
	Created by Tech_dog (ebontrop@gmail.com) on 16-Mar-2015 at 10:33:34pm, GMT+3, Taganrog, Monday;
	This is UIX library label control window class declaration file.
	-----------------------------------------------------------------------------
	Adopted to version v15 on 16-Feb-2021 at 6:39:50.528 am, UTC+7, Novosibirsk, Tuesday;
*/
#include "UIX_ControlBase.h"
#include "UIX_GdiProvider.h"

namespace ex_ui { namespace controls { namespace _impl {

	using namespace ex_ui::draw;

	class CLabelWnd:
		public  ::ATL::CWindowImpl<CLabelWnd>
	{
		typedef ::ATL::CWindowImpl<CLabelWnd> TWindow;
	private:
		CControlCrt&         m_crt;
		COLORREF             m_fore;
	public:
		 CLabelWnd(CControlCrt&);
		~CLabelWnd(void);
	public:
		BEGIN_MSG_MAP(CLabelWnd)
			MESSAGE_HANDLER(WM_ERASEBKGND , OnEraseBkgnd )
			MESSAGE_HANDLER(WM_PAINT      , OnPaint      )
			MESSAGE_HANDLER(WM_SETTEXT    , OnTextChange )
		END_MSG_MAP()
	private:
		LRESULT OnEraseBkgnd (UINT, WPARAM, LPARAM, BOOL&);
		LRESULT OnPaint      (UINT, WPARAM, LPARAM, BOOL&);
		LRESULT OnTextChange (UINT, WPARAM, LPARAM, BOOL&);
	public:
		HRESULT              ForeColor(const COLORREF);
	};
}}}

#endif/*_UIXCTRLLABELCONTROLWND_H_1A902F7E_258D_4e3c_9427_78CAA6B67D43_INCLUDED*/