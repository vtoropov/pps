/*
	Created by Tech_dog (ebontrop@gmail.com) on 8-Feb-2015 at 6:45:37pm, GMT+3, Taganrog, Sunday;
	This is UIX library custom button control class implementation file.
	-----------------------------------------------------------------------------
	Adopted to version v15 on 16-Feb-2021 at 6:48:02.698 am, UTC+7, Novosibirsk, Tuesday;
*/
#include "StdAfx.h"
#include "UIX_ButtonWnd.h"
#include "UIX_Button.h"

using namespace ex_ui;
using namespace ex_ui::controls;

/////////////////////////////////////////////////////////////////////////////

CButton::CButton(CControlCrt& crt_ref) : m_wnd_ptr(NULL), m_crt(crt_ref)
{
	try { m_wnd_ptr = new CButtonWnd(crt_ref); } catch(::std::bad_alloc&) { ATLASSERT(FALSE);}
}

CButton::CButton(CControlCrt& crt_ref, const UINT ctrlId) : m_wnd_ptr(NULL), m_crt(ctrlId, crt_ref)
{
	try { m_wnd_ptr = new CButtonWnd(m_crt); } catch(::std::bad_alloc&) { ATLASSERT(FALSE);}
}

CButton::CButton(const UINT ctrlId, IRenderer& parent_rnd, IControlNotify& snk_ref): m_wnd_ptr(NULL), m_crt(ctrlId, parent_rnd, snk_ref)
{
	try { m_wnd_ptr = new CButtonWnd(m_crt); } catch(::std::bad_alloc&) { ATLASSERT(FALSE);}
}

CButton::~CButton(void)
{
	if (NULL != m_wnd_ptr)
	{
		try { delete m_wnd_ptr; m_wnd_ptr = NULL; } catch(...){ ATLASSERT(FALSE); }
	}
}

/////////////////////////////////////////////////////////////////////////////

const
CColour& CButton::BackColor(void)const
{
	return m_crt.BackColor();
}

VOID     CButton::BackColor(const COLORREF clr, const BYTE _alpha)
{
	m_crt.BackColor().SetColorFromRGBA(clr, _alpha);
}

HRESULT  CButton::Create(const HWND hParent, const LPRECT lpRect, const bool bHidden)
{
	if (NULL == m_wnd_ptr)
		return OLE_E_BLANK;
	if ((*m_wnd_ptr).IsWindow())
		return HRESULT_FROM_WIN32(ERROR_OBJECT_ALREADY_EXISTS);
	RECT rc_ = {0};
	if (lpRect)
	{
		rc_ = *lpRect;
	}
	else
	{
		HRESULT hr_ = (*m_wnd_ptr).RecalcWindowRect(rc_);
		if (S_OK != hr_)
			return  hr_;
	}
	const DWORD dwStyle = WS_CHILD | (bHidden ? 0 : WS_VISIBLE) | WS_TABSTOP;
	(*m_wnd_ptr).Create(hParent, rc_, _T("oDesk::shared::controls::Button"), dwStyle);
	if (!(*m_wnd_ptr).IsWindow())
		return HRESULT_FROM_WIN32(::GetLastError());
	else
		return S_OK;
}

HRESULT  CButton::Destroy(void)
{
	if (NULL == m_wnd_ptr)
		return OLE_E_BLANK;
	if (!(*m_wnd_ptr).IsWindow())
		return S_FALSE;
	(*m_wnd_ptr).SendMessage(WM_CLOSE);
	return S_OK;
}

HRESULT  CButton::Enable (const bool bEnabled)
{
	if (NULL == m_wnd_ptr)
		return OLE_E_BLANK;
	if (!(*m_wnd_ptr).IsWindow())
		return OLE_E_BLANK;
	(*m_wnd_ptr).EnableWindow(bEnabled);
	return S_OK;
}

CWindow  CButton::GetWindow(void) const
{
	::ATL::CWindow wnd;
	if (m_wnd_ptr) wnd = *m_wnd_ptr;
	return wnd;
}

HRESULT  CButton::SetImage(const DWORD dState, const UINT nResId, const HINSTANCE hResourceModule)
{
	if (NULL == m_wnd_ptr) return OLE_E_BLANK;
	return (*m_wnd_ptr).SetImage(dState, nResId, hResourceModule);
}

HRESULT  CButton::Subclass(::ATL::CWindow& target)
{
	if (NULL == m_wnd_ptr) return OLE_E_BLANK;
	HWND hParent = target.GetParent();
	RECT rect_   = {0};
	
	target.GetWindowRect(&rect_);
	::MapWindowPoints(HWND_DESKTOP, hParent, (LPPOINT)&rect_, 0x2);
	
	(*m_wnd_ptr).RecalcWindowRect(rect_);
	const BOOL bDisabled = (FALSE == target.IsWindowEnabled());
	const DWORD   dStyle = WS_CHILD|WS_VISIBLE|WS_TABSTOP|(TRUE == bDisabled ? WS_DISABLED : 0);
	
	ATLVERIFY(target.DestroyWindow());
	const HRESULT res_ = ::IsWindow((*m_wnd_ptr).Create(hParent, &rect_, NULL, dStyle, 0, m_crt.CtrlId()))
									? S_OK
									: HRESULT_FROM_WIN32(::GetLastError());
	if (S_OK == res_)
	{
		if (FALSE != bDisabled)
		{
			(*m_wnd_ptr).SetState(eControlState::eDisabled);
		}
		(*m_wnd_ptr).Refresh(true);
	}
	return res_;
}