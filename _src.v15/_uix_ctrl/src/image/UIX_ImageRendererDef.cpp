/*
	Created by Tech_dog (ebontrop@gmail.com) on 7-Feb-2015 at 8:10:34pm, GMT+3, Taganrog, Saturday;
	This is UIX library image control default renderer class implementation file.
	-----------------------------------------------------------------------------
	Adopted to version v15 on 16-Feb-2021 at 6:56:44.973 am, UTC+7, Novosibirsk, Tuesday;
*/
#include "StdAfx.h"
#include "UIX_ImageRendererDef.h"
#include "UIX_PngWrap.h"

using namespace ex_ui;
using namespace ex_ui::draw::common;
using namespace ex_ui::controls;
using namespace ex_ui::controls::_impl;
using namespace ex_ui::draw::defs;

/////////////////////////////////////////////////////////////////////////////

CImageRendererDefaultImpl::CImageRendererDefaultImpl(CImage& ctrl_ref, CImageShaperDefaultImpl*& shaper_ptr_ref):
	m_ctrl_ref(ctrl_ref),
	m_shaper_ptr_ref(shaper_ptr_ref),
	TBaseRenderer(ctrl_ref.GetWindow_Ref())
{
	::SetRectEmpty(&m_rect);
}

CImageRendererDefaultImpl::~CImageRendererDefaultImpl(void)
{
}

/////////////////////////////////////////////////////////////////////////////

HRESULT   CImageRendererDefaultImpl::Clear(void)
{
	if (!TBaseRenderer::m_bkgnd_cache.IsValidObject())
		return S_FALSE;
	HRESULT hr__ = TBaseRenderer::m_bkgnd_cache.Destroy();
	return  hr__;
}

HRESULT   CImageRendererDefaultImpl::Draw(CZBuffer& dc_, const RECT& rcArea)
{
	HRESULT hr_ = S_OK;
	if (::IsRectEmpty(&rcArea)) return E_INVALIDARG;
	if (!::EqualRect(&rcArea, &m_rect) || true)
	{
		TBaseRenderer::m_bkgnd_cache.Destroy();
	}
	if (false == TBaseRenderer::m_bkgnd_cache.IsValidObject())
	{
		m_rect = rcArea;
		IRenderer* pParentRenderer = m_ctrl_ref.GetParentRendererPtr();

		dc_.DrawSolidRect(m_rect, m_ctrl_ref.BackColor());
		if (this->_IsTransparent() && NULL != pParentRenderer)
		{
			pParentRenderer->DrawParentBackground(m_ctrl_ref.GetWindow_Ref(), dc_, m_rect);
		}

		const CPngBitmapPtr& ptr_ref = m_ctrl_ref.GetBitmapPtrRef();
		if (NULL != m_shaper_ptr_ref && ptr_ref.IsValidObject())
		{
			Gdiplus::Bitmap* pBitmap = (*ptr_ref.GetObject()).GetPtr();

			RECT draw_ = m_shaper_ptr_ref->GetRectangle(CImageShaperDefaultImpl::eRT_Image);

			Gdiplus::Graphics gp_(dc_);
			gp_.DrawImage(pBitmap, draw_.left, draw_.top, __W(draw_), __H(draw_));
		}

		/*if (this->_HasBorder())
			dc_.Rectangle(&rc_, RGB(0x0f, 0x0f, 0x0f));*/

		HBITMAP hBitmap = NULL;
		hr_ = dc_.CopyTo(hBitmap);
		if (S_OK == hr_)
		{
			CGdiplusBitmapWrap wrap_(hBitmap);
			hr_ = TBaseRenderer::m_bkgnd_cache.Attach(wrap_.Detach());
			::DeleteObject((HGDIOBJ)hBitmap);
			hBitmap = NULL;
		}
	}
	if (!TBaseRenderer::m_bkgnd_cache.IsValidObject())
	{
		return TBaseRenderer::m_bkgnd_cache.GetLastResult();
	}
	else
	{
		const CPngBitmap* pPng = TBaseRenderer::m_bkgnd_cache.GetObject();
		if (NULL != pPng)
		{
			Gdiplus::Bitmap* pBitmap = pPng->GetPtr();
			if (pBitmap && m_shaper_ptr_ref)
			{
				RECT draw_ = m_shaper_ptr_ref->GetRectangle(CImageShaperDefaultImpl::eRT_Image);
				Gdiplus::Graphics gp_(dc_);
				gp_.DrawImage(pBitmap, draw_.left, draw_.top, __W(draw_), __H(draw_));
			}
		}
	}
	return hr_;
}

/////////////////////////////////////////////////////////////////////////////

bool     CImageRendererDefaultImpl::_IsTransparent(void) const
{
	return (0 == (m_ctrl_ref.Style() & eImageStyle::eOpaque));
}

bool     CImageRendererDefaultImpl::_HasBorder(void) const
{
	return (0 != (m_ctrl_ref.Style() & eImageStyle::eBorder));
}