/*
	Created by Tech_dog (ebontrop@gmail.com) on 5-Feb-2015 at 7:21:10pm, GMT+3, Taganrog, Monday;
	This is UIX Control library precompiled headers implementation file.
	-----------------------------------------------------------------------------
	Adopted to version v15 on 16-Feb-2021 at 6:25:36.998 am, UTC+7, Novosibirsk, Tuesday;
*/
#include "StdAfx.h"

#if (_ATL_VER < 0x0700)
#include <atlimpl.cpp>
#endif //(_ATL_VER < 0x0700)
