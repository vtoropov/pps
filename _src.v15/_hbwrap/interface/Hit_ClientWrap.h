#ifndef _SHAREDHITACHISDKCLIENTWRAPPER_H_AFCBA39F_3893_4385_AC78_A83DF826BEC3_INCLUDED
#define _SHAREDHITACHISDKCLIENTWRAPPER_H_AFCBA39F_3893_4385_AC78_A83DF826BEC3_INCLUDED
/*
	Created by Tech_dog (ebontrop@gmail.com) on 21-Mar-2014 at 1:00:55pm, GMT+4, Taganrog, Friday;
	This is Shared Recognition Hitachi BioSDK Client Wrapper class declaration file.
	-----------------------------------------------------------------------------
	Adopted to version v15 on 15-Feb-2021 at 5:12:04.407 am, UTC+7, Novosibirsk, Monday;
*/

#include "ImageProcessor.h"

namespace shared { namespace recognition { namespace client { namespace Hitachi
{
	using shared::recognition::eResultCodeType;
	using shared::recognition::IImageProcessor;
	using shared::recognition::IInitializer;
	using shared::recognition::eProcessorType;

	INT GetResultCode(const eResultCodeType::_enum);

	HRESULT   CreateInitializer(shared::recognition::IInitializer*&);
	HRESULT   DestroyInitializer_Safe(shared::recognition::IInitializer*&);

	HRESULT   CreateImageProcessor(const IInitializer&, shared::recognition::IImageProcessor*&);
	HRESULT   DestroyImageProcessor_Safe(shared::recognition::IImageProcessor*&);

	class CImpersonateParam
	{
	public:
		enum _enum {
			eBufferLen = 16,
			eSeqDefLen = 32,
		};
	};

	class CImpersonate
	{
	private:
		BYTE    m_UUID[CImpersonateParam::eBufferLen];
		mutable CStringW   m_buffer;
	public:
		 CImpersonate(void);
		 CImpersonate(const PBYTE pBuffer, const INT nSize);
		~CImpersonate(void);
	public:
		PBYTE const  GetUUID_Ptr(void)  const;
		HRESULT      StringToUUID(LPCWSTR)   ;
		LPCWSTR      UUIDToString(void) const;
	};
}}}}

#endif/*_SHAREDHITACHISDKCLIENTWRAPPER_H_AFCBA39F_3893_4385_AC78_A83DF826BEC3_INCLUDED*/