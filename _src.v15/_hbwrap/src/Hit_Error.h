#ifndef _SHAREDHITACHIBIOAPISDKERROR_H_0CC80C25_7420_491b_AB19_C88427BCAE43_INCLUDED
#define _SHAREDHITACHIBIOAPISDKERROR_H_0CC80C25_7420_491b_AB19_C88427BCAE43_INCLUDED
/*
	Created by Tech_dog (ebontrop@gmail.com) on 22-Mar-2014 at 11:31:07am, GMT+4, Moscow Region,
	Rail Road Train #43, Coatch #6, Place #1, Saturday;
	This is Shared Recognition Hitachi BioApi SDK Error object class declaration file.
	-----------------------------------------------------------------------------
	Adopted to version v15 on 15-Feb-2021 at 5:12:04.407 am, UTC+7, Novosibirsk, Monday;
*/
#include "ImageProcessor.h"

namespace shared { namespace recognition { namespace client { namespace Hitachi
{
	class CError:
		public shared::recognition::IError
	{
	private:
		INT               m_code;
		mutable CStringW  m_buffer;
		HRESULT           m_hResult;
	public:
		 CError(void);
		~CError(void);
	public:
#pragma warning(disable:4481)
		virtual INT       GetDeviceCode(void)        const override sealed;
		virtual LPCWSTR   GetDescription(void)       const override sealed;
		virtual HRESULT   GetHresult(void)           const override sealed;
		virtual bool      IsFailure(void)            const override sealed;
#pragma warning(default:4481)
	public:
		void              SetDeviceCode(const INT);
		void              SetHresult(const HRESULT);
	};
}}}}

#endif/*_SHAREDHITACHIBIOAPISDKERROR_H_0CC80C25_7420_491b_AB19_C88427BCAE43_INCLUDED*/