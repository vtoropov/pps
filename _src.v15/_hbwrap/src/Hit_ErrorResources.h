#ifndef _SHAREDHITACHIERRORRESOURCE_H_D849666A_0099_4dd7_845A_BCB39739408F_INCLUDED
#define _SHAREDHITACHIERRORRESOURCE_H_D849666A_0099_4dd7_845A_BCB39739408F_INCLUDED
/*
	Created by Tech_dog (ebontrop@gmail.com) on 22-Mar-2014 at 12:20:49pm, GMT+4, Moscow Region;
	Rail Road Train #43, Coatch #6, Place #1, Saturday;
	This is Shared Recognition Hitachi SDK Error Resource declaration file.
	-----------------------------------------------------------------------------
	Adopted to version v15 on 15-Feb-2021 at 6:27:56.885 am, UTC+7, Novosibirsk, Monday;
*/

namespace shared { namespace recognition { namespace client { namespace Hitachi { namespace details
{
	CStringW Error_GetString(const DWORD dResId);
}}}}}


////////////////////////////////////////////////////////////////////////////
//
//  Framework & BSP error codes
//
////////////////////////////////////////////////////////////////////////////

#define  IDS_ERROR_0x000101         0x000101
#define  IDS_ERROR_0x000102         0x000102
#define  IDS_ERROR_0x000103         0x000103
#define  IDS_ERROR_0x000104         0x000104
#define  IDS_ERROR_0x000105         0x000105
#define  IDS_ERROR_0x000106         0x000106
#define  IDS_ERROR_0x000108         0x000108
#define  IDS_ERROR_0x000109         0x000109
#define  IDS_ERROR_0x00010A         0x00010A
#define  IDS_ERROR_0x00010B         0x00010B
#define  IDS_ERROR_0x00010D         0x00010D
#define  IDS_ERROR_0x00010E         0x00010E
#define  IDS_ERROR_0x00010F         0x00010F
#define  IDS_ERROR_0x000112         0x000112
#define  IDS_ERROR_0x000113         0x000113
#define  IDS_ERROR_0x000115         0x000115
#define  IDS_ERROR_0x000116         0x000116
#define  IDS_ERROR_0x000117         0x000117
#define  IDS_ERROR_0x000118         0x000118
#define  IDS_ERROR_0x000119         0x000119
#define  IDS_ERROR_0x00011A         0x00011A
#define  IDS_ERROR_0x00011B         0x00011B
#define  IDS_ERROR_0x00011C         0x00011C
#define  IDS_ERROR_0x000202         0x000202
#define  IDS_ERROR_0x000203         0x000203
#define  IDS_ERROR_0x000205         0x000205
#define  IDS_ERROR_0x000206         0x000206
#define  IDS_ERROR_0x000501         0x000501

////////////////////////////////////////////////////////////////////////////
//
// Framework & BSP error codes (unsupported)
//
////////////////////////////////////////////////////////////////////////////

#define  IDS_ERROR_0x000107         0x000107
#define  IDS_ERROR_0x00010C         0x00010C
#define  IDS_ERROR_0x000110         0x000110
#define  IDS_ERROR_0x000111         0x000111
#define  IDS_ERROR_0x000114         0x000114
#define  IDS_ERROR_0x00011D         0x00011D
#define  IDS_ERROR_0x00011E         0x00011E
#define  IDS_ERROR_0x00011F         0x00011F
#define  IDS_ERROR_0x000201         0x000201
#define  IDS_ERROR_0x000204         0x000204
#define  IDS_ERROR_0x000300         0x000300
#define  IDS_ERROR_0x000301         0x000301
#define  IDS_ERROR_0x000302         0x000302
#define  IDS_ERROR_0x000303         0x000303
#define  IDS_ERROR_0x000304         0x000304
#define  IDS_ERROR_0x000305         0x000305
#define  IDS_ERROR_0x000306         0x000306
#define  IDS_ERROR_0x000307         0x000307
#define  IDS_ERROR_0x000308         0x000308
#define  IDS_ERROR_0x000309         0x000309
#define  IDS_ERROR_0x00030A         0x00030A
#define  IDS_ERROR_0x00030B         0x00030B
#define  IDS_ERROR_0x00030C         0x00030C
#define  IDS_ERROR_0x00030D         0x00030D
#define  IDS_ERROR_0x00030E         0x00030E
#define  IDS_ERROR_0x000400         0x000400
#define  IDS_ERROR_0x000401         0x000401
#define  IDS_ERROR_0x000402         0x000402
#define  IDS_ERROR_0x000403         0x000403
#define  IDS_ERROR_0x000404         0x000404
#define  IDS_ERROR_0x000405         0x000405
#define  IDS_ERROR_0x000406         0x000406
#define  IDS_ERROR_0x000407         0x000407
#define  IDS_ERROR_0x000408         0x000408
#define  IDS_ERROR_0x000409         0x000409
#define  IDS_ERROR_0x00040A         0x00040A
#define  IDS_ERROR_0x00040B         0x00040B
#define  IDS_ERROR_0x00040C         0x00040C

////////////////////////////////////////////////////////////////////////////
//
// Security Extension Error Codes
//
////////////////////////////////////////////////////////////////////////////

#define  IDS_ERROR_0x000801         0x000801
#define  IDS_ERROR_0x000802         0x000802
#define  IDS_ERROR_0x000803         0x000803
#define  IDS_ERROR_0x000804         0x000804
#define  IDS_ERROR_0x000805         0x000805
#define  IDS_ERROR_0x000806         0x000806
#define  IDS_ERROR_0x000807         0x000807
#define  IDS_ERROR_0x000808         0x000808
#define  IDS_ERROR_0x000809         0x000809
#define  IDS_ERROR_0x00080A         0x00080A
#define  IDS_ERROR_0x00080B         0x00080B
#define  IDS_ERROR_0x00080C         0x00080C
#define  IDS_ERROR_0x00080D         0x00080D
#define  IDS_ERROR_0x00080E         0x00080E
#define  IDS_ERROR_0x00080F         0x00080F
#define  IDS_ERROR_0x000810         0x000810
#define  IDS_ERROR_0x000811         0x000811
#define  IDS_ERROR_0x000812         0x000812
#define  IDS_ERROR_0x000813         0x000813
#define  IDS_ERROR_0x000814         0x000814
#define  IDS_ERROR_0x000815         0x000815
#define  IDS_ERROR_0x000816         0x000816
#define  IDS_ERROR_0x000817         0x000817
#define  IDS_ERROR_0x000818         0x000818
#define  IDS_ERROR_0x000819         0x000819
#define  IDS_ERROR_0x00081A         0x00081A
#define  IDS_ERROR_0x00081B         0x00081B
#define  IDS_ERROR_0x00081C         0x00081C
#define  IDS_ERROR_0x00081D         0x00081D
#define  IDS_ERROR_0x00081E         0x00081E

////////////////////////////////////////////////////////////////////////////
//
// Driver Error Codes
//
////////////////////////////////////////////////////////////////////////////

#define  IDS_ERROR_0x000010         0x000010
#define  IDS_ERROR_0x000011         0x000011
#define  IDS_ERROR_0x000012         0x000012
#define  IDS_ERROR_0x000013         0x000013
#define  IDS_ERROR_0x000014         0x000014
#define  IDS_ERROR_0x000015         0x000015
#define  IDS_ERROR_0x000016         0x000016
#define  IDS_ERROR_0x000017         0x000017
#define  IDS_ERROR_0x000018         0x000018
#define  IDS_ERROR_0x000019         0x000019
#define  IDS_ERROR_0x00001A         0x00001A
#define  IDS_ERROR_0x00001B         0x00001B
#define  IDS_ERROR_0x00001C         0x00001C
#define  IDS_ERROR_0x00001D         0x00001D
#define  IDS_ERROR_0x00001E         0x00001E
#define  IDS_ERROR_0x00001F         0x00001F
#define  IDS_ERROR_0x000021         0x000021
#define  IDS_ERROR_0x000022         0x000022
#define  IDS_ERROR_0x000023         0x000023
#define  IDS_ERROR_0x020001         0x020001

#endif/*_SHAREDHITACHIERRORRESOURCE_H_D849666A_0099_4dd7_845A_BCB39739408F_INCLUDED*/