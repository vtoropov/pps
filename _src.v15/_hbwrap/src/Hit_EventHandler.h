#ifndef _SHAREDHITACHISDKEVENTHANDLER_H_B924C3BD_3F57_4424_BCF2_9B8203638548_INCLUDED
#define _SHAREDHITACHISDKEVENTHANDLER_H_B924C3BD_3F57_4424_BCF2_9B8203638548_INCLUDED
/*
	Created by Tech_dog (ebontrop@gmail.com) on 22-Mar-2014 at 2:35:38pm, GMT+4, Moscow Region, 
	Rail Road Train #43, Coatch #6, Place #1, Saturday;
	This is Shared Recognition Hitachi SDK Wrapper Event Handler class declaration file.
	-----------------------------------------------------------------------------
	Adopted to version v15 on 15-Feb-2021 at 5:17:57.646 am, UTC+7, Novosibirsk, Monday;
*/
#include "Hit_Error.h"

namespace shared { namespace recognition { namespace client { namespace Hitachi
{
	enum UIStateEventType // this enumaration is created from BioAPISDKAPI_Reference.pdf, page 28th, BioAPI_GUI_STATE_CALLBACK
	{
		UIT_Process     = 0x0,
		UIT_Success     = 0x1,
		UIT_Failure     = 0x2,
		UIT_Cancelled   = 0x3,
		UIT_Timout      = 0x4,
		UIT_Detection   = 0x5,
	};
	
	interface IGenericCallback
	{
		virtual   HRESULT     OnDeviceStateChanged(const UIStateEventType) { return S_OK; }
	};

	class CEventHandler
	{
	private:
		IGenericCallback&     m_callback_ref;
		CError                m_error;
		bool                  m_state_callback_set;
	public:
		 CEventHandler(IGenericCallback&);
		~CEventHandler(void);
	public:
		const CError&  GetLastError_Ref(void) const;
		bool           IsStateCallbackSet(void) const;
		HRESULT        SetStateCallback(const INT device_handle);
	public:
		static BioAPI_RETURN BioAPI EventCallback(  __in_opt const BioAPI_UUID*,
													__in     BioAPI_UNIT_ID,
													__in_opt void* pNotifyCallbackCtx,
													__in     const BioAPI_UNIT_SCHEMA,
													__in     BioAPI_EVENT);
		static BioAPI_RETURN BioAPI StateCallback(  __in_opt void* pNotifyCallbackCtx,
													__in     BioAPI_GUI_STATE,
													__in_opt BioAPI_GUI_RESPONSE*,
													__in     BioAPI_GUI_MESSAGE,
													__in     BioAPI_GUI_PROGRESS,
													__in_opt const BioAPI_GUI_BITMAP*);
		static BioAPI_RETURN BioAPI StreamCallback( __in_opt void* pNotifyCallbackCtx,
													__in_opt const BioAPI_GUI_BITMAP*);
	};
}}}}

#endif/*_SHAREDHITACHISDKEVENTHANDLER_H_B924C3BD_3F57_4424_BCF2_9B8203638548_INCLUDED*/