/*
	Created by Tech_dog (ebontrop@gmail.com) on 22-Mar-2014 at 11:39:20am, GMT+4, Moscow Region
	Rail Road Train #43, Coatch #6, Place #1, Saturday;
	This is Shared Recognition Hitachi BioApi SDK Error object class implementation file.
	-----------------------------------------------------------------------------
	Adopted to version v15 on 15-Feb-2021 at 5:39:32.780 am, UTC+7, Novosibirsk, Monday;
*/
#include "StdAfx.h"
#include "Hit_Error.h"
#include "Shared_SystemError.h"
#include "Hit_ErrorResources.h"

using namespace shared;
using namespace shared::recognition;
using namespace shared::recognition::client;
using namespace shared::recognition::client::Hitachi;

/////////////////////////////////////////////////////////////////////////////
namespace shared { namespace recognition { namespace client { namespace Hitachi { namespace details
{
	class Error_SourceModule
	{
	public:
		enum _enum {
			eFramework   = 0x0,
			eBSP         = 0x1,
			eDriver      = 0x2,
			eSecureExt   = 0x8,
			eWin32       = 0xf,
		};
	};

	INT   Error_GetNumberFromCode(const DWORD dCode)
	{
		return (0x00ffffff & dCode);
	}

	Error_SourceModule::_enum  Error_GetSourceFromCode(const DWORD dCode)
	{
		Error_SourceModule::_enum e_src__ = Error_SourceModule::eWin32;
		const DWORD dSource  = ((dCode & 0xff000000) >> 24);
		switch (dSource)
		{
		case 0:      e_src__ = Error_SourceModule::eFramework;    break;
		case 1:      e_src__ = Error_SourceModule::eBSP;          break;
		case 2:      e_src__ = Error_SourceModule::eDriver;       break;
		case 8:      e_src__ = Error_SourceModule::eSecureExt;    break;
		}
		return e_src__;
	}
}}}}}
/////////////////////////////////////////////////////////////////////////////

CError::CError(void):
	m_code(BioAPI_OK),
	m_hResult(S_OK)
{
}

CError::~CError(void)
{
}

/////////////////////////////////////////////////////////////////////////////

INT       CError::GetDeviceCode(void) const
{
	return m_code;
}

LPCWSTR   CError::GetDescription(void) const
{
	details::Error_SourceModule::_enum e_src__ = details::Error_GetSourceFromCode(DWORD(m_code));
	const INT err_number__ = details::Error_GetNumberFromCode(DWORD(m_code));
	if (NULL == err_number__ && S_OK == m_hResult)
	{
		m_buffer = _T("HiBioAPI Framework status is OK");
		return m_buffer.GetString();
	}
	else if (S_OK != m_hResult)
	{
		e_src__ = details::Error_SourceModule::eWin32;
	}

	switch (e_src__)
	{
	case details::Error_SourceModule::eBSP:
	case details::Error_SourceModule::eFramework:
		{
			UINT res__ = (err_number__ == 0x000101 ? IDS_ERROR_0x000101:
							(err_number__ == 0x000102 ? IDS_ERROR_0x000102:
								(err_number__ == 0x000103 ? IDS_ERROR_0x000103:
									(err_number__ == 0x000104 ? IDS_ERROR_0x000104:
										(err_number__ == 0x000105 ? IDS_ERROR_0x000105:
											(err_number__ == 0x000106 ? IDS_ERROR_0x000106:
												(err_number__ == 0x000108 ? IDS_ERROR_0x000108:
													(err_number__ == 0x000109 ? IDS_ERROR_0x000109:
														(err_number__ == 0x00010A ? IDS_ERROR_0x00010A:
															(err_number__ == 0x00010B ? IDS_ERROR_0x00010B:
																(err_number__ == 0x00010D ? IDS_ERROR_0x00010D:
																	(err_number__ == 0x00010E ? IDS_ERROR_0x00010E:
																		(err_number__ == 0x00010F ? IDS_ERROR_0x00010F:
																			(err_number__ == 0x000112 ? IDS_ERROR_0x000112:
																				(err_number__ == 0x000113 ? IDS_ERROR_0x000113:
																					(err_number__ == 0x000115 ? IDS_ERROR_0x000115:
																						(err_number__ == 0x000116 ? IDS_ERROR_0x000116:
																							(err_number__ == 0x000117 ? IDS_ERROR_0x000117:
																								(err_number__ == 0x000118 ? IDS_ERROR_0x000118:
																									(err_number__ == 0x000119 ? IDS_ERROR_0x000119:
																										(err_number__ == 0x00011A ? IDS_ERROR_0x00011A:
																											(err_number__ == 0x00011B ? IDS_ERROR_0x00011B:
																												(err_number__ == 0x00011C ? IDS_ERROR_0x00011C:
																													(err_number__ == 0x000202 ? IDS_ERROR_0x000202:
																														(err_number__ == 0x000203 ? IDS_ERROR_0x000203:
																															(err_number__ == 0x000205 ? IDS_ERROR_0x000205:
																																(err_number__ == 0x000206 ? IDS_ERROR_0x000206:
																																	(err_number__ == 0x000501 ? IDS_ERROR_0x000501:
																																		0))))))))))))))))))))))))))));
			if (!res__) // checks for unsupported errors
			{
				res__ = (err_number__ == 0x000107 ? IDS_ERROR_0x000107:
							(err_number__ == 0x00010C ? IDS_ERROR_0x00010C:
								(err_number__ == 0x000110 ? IDS_ERROR_0x000110:
									(err_number__ == 0x000111 ? IDS_ERROR_0x000111:
										(err_number__ == 0x000114 ? IDS_ERROR_0x000114:
											(err_number__ == 0x00011D ? IDS_ERROR_0x00011D:
												(err_number__ == 0x00011E ? IDS_ERROR_0x00011E:
													(err_number__ == 0x00011F ? IDS_ERROR_0x00011F:
														(err_number__ == 0x000201 ? IDS_ERROR_0x000201:
															(err_number__ == 0x000204 ? IDS_ERROR_0x000204:
																(err_number__ == 0x000300 ? IDS_ERROR_0x000300:
																	(err_number__ == 0x000301 ? IDS_ERROR_0x000301:
																		(err_number__ == 0x000302 ? IDS_ERROR_0x000302:
																			(err_number__ == 0x000303 ? IDS_ERROR_0x000303:
																				(err_number__ == 0x000304 ? IDS_ERROR_0x000304:
																					(err_number__ == 0x000305 ? IDS_ERROR_0x000305:
																						(err_number__ == 0x000306 ? IDS_ERROR_0x000306:
																							(err_number__ == 0x000307 ? IDS_ERROR_0x000307:
																								(err_number__ == 0x000308 ? IDS_ERROR_0x000308:
																									(err_number__ == 0x000309 ? IDS_ERROR_0x000309:
																										(err_number__ == 0x00030A ? IDS_ERROR_0x00030A:
																											(err_number__ == 0x00030B ? IDS_ERROR_0x00030B:
																												(err_number__ == 0x00030C ? IDS_ERROR_0x00030C:
																													(err_number__ == 0x00030D ? IDS_ERROR_0x00030D:
																														(err_number__ == 0x00030E ? IDS_ERROR_0x00030E:
																															(err_number__ == 0x000400 ? IDS_ERROR_0x000400:
																																(err_number__ == 0x000401 ? IDS_ERROR_0x000401:
																																	(err_number__ == 0x000402 ? IDS_ERROR_0x000402:
																																		(err_number__ == 0x000403 ? IDS_ERROR_0x000403:
																																			(err_number__ == 0x000404 ? IDS_ERROR_0x000404:
																																				(err_number__ == 0x000405 ? IDS_ERROR_0x000405:
																																					(err_number__ == 0x000406 ? IDS_ERROR_0x000406:
																																						(err_number__ == 0x000407 ? IDS_ERROR_0x000407:
																																							(err_number__ == 0x000408 ? IDS_ERROR_0x000408:
																																								(err_number__ == 0x000409 ? IDS_ERROR_0x000409:
																																									(err_number__ == 0x00040A ? IDS_ERROR_0x00040A:
																																										(err_number__ == 0x00040B ? IDS_ERROR_0x00040B:
																																											(err_number__ == 0x00040C ? IDS_ERROR_0x00040C:
																																												(0)))))))))))))))))))))))))))))))))))))));
			}
			CStringW csError;
			if (!res__)
				csError = _T("Unknown Error");
			else
				csError = details::Error_GetString(res__);
			m_buffer.Format(_T("HiBioAPI Framework BSP Error: [code = 0x%x] %s"), err_number__, csError.GetString());
		} break;
	case details::Error_SourceModule::eDriver:
		{
			UINT res__ = (err_number__ == 0x000010 ? IDS_ERROR_0x000010 :
							(err_number__ == 0x000011 ? IDS_ERROR_0x000011:
								(err_number__ == 0x000012 ? IDS_ERROR_0x000012:
									(err_number__ == 0x000013 ? IDS_ERROR_0x000013:
										(err_number__ == 0x000014 ? IDS_ERROR_0x000014:
											(err_number__ == 0x000015 ? IDS_ERROR_0x000015:
												(err_number__ == 0x000016 ? IDS_ERROR_0x000016:
													(err_number__ == 0x000017 ? IDS_ERROR_0x000017:
														(err_number__ == 0x000018 ? IDS_ERROR_0x000018:
															(err_number__ == 0x000019 ? IDS_ERROR_0x000019:
																(err_number__ == 0x00001A ? IDS_ERROR_0x00001A:
																	(err_number__ == 0x00001B ? IDS_ERROR_0x00001B:
																		(err_number__ == 0x00001C ? IDS_ERROR_0x00001C:
																			(err_number__ == 0x00001D ? IDS_ERROR_0x00001D:
																				(err_number__ == 0x00001E ? IDS_ERROR_0x00001E:
																					(err_number__ == 0x00001F ? IDS_ERROR_0x00001F:
																						(err_number__ == 0x000021 ? IDS_ERROR_0x000021:
																							(err_number__ == 0x000022 ? IDS_ERROR_0x000022:
																								(err_number__ == 0x000023 ? IDS_ERROR_0x000023:
																									(err_number__ == 0x020001 ? IDS_ERROR_0x020001:
																										(0)))))))))))))))))))));
			CStringW csError;
			if (!res__)
				csError = _T("Unknown Error");
			else
				csError = details::Error_GetString(res__);
			m_buffer.Format(_T("HiBioAPI Driver Error: [code = 0x%x] %s"), err_number__, csError.GetString());
		} break;
	case details::Error_SourceModule::eSecureExt:
		{
			UINT res__ = (err_number__ == 0x000801 ? IDS_ERROR_0x000801:
							(err_number__ == 0x000802 ? IDS_ERROR_0x000802:
								(err_number__ == 0x000803 ? IDS_ERROR_0x000803:
									(err_number__ == 0x000804 ? IDS_ERROR_0x000804:
										(err_number__ == 0x000805 ? IDS_ERROR_0x000805:
											(err_number__ == 0x000806 ? IDS_ERROR_0x000806:
												(err_number__ == 0x000807 ? IDS_ERROR_0x000807:
													(err_number__ == 0x000808 ? IDS_ERROR_0x000808:
														(err_number__ == 0x000809 ? IDS_ERROR_0x000809:
															(err_number__ == 0x00080A ? IDS_ERROR_0x00080A:
																(err_number__ == 0x00080B ? IDS_ERROR_0x00080B:
																	(err_number__ == 0x00080C ? IDS_ERROR_0x00080C:
																		(err_number__ == 0x00080D ? IDS_ERROR_0x00080D:
																			(err_number__ == 0x00080E ? IDS_ERROR_0x00080E:
																				(err_number__ == 0x00080F ? IDS_ERROR_0x00080F:
																					(err_number__ == 0x000810 ? IDS_ERROR_0x000810:
																						(err_number__ == 0x000811 ? IDS_ERROR_0x000811:
																							(err_number__ == 0x000812 ? IDS_ERROR_0x000812:
																								(err_number__ == 0x000813 ? IDS_ERROR_0x000813:
																									(err_number__ == 0x000814 ? IDS_ERROR_0x000814:
																										(err_number__ == 0x000815 ? IDS_ERROR_0x000815:
																											(err_number__ == 0x000816 ? IDS_ERROR_0x000816:
																												(err_number__ == 0x000817 ? IDS_ERROR_0x000817:
																													(err_number__ == 0x000818 ? IDS_ERROR_0x000818:
																														(err_number__ == 0x000819 ? IDS_ERROR_0x000819:
																															(err_number__ == 0x00081A ? IDS_ERROR_0x00081A:
																																(err_number__ == 0x00081B ? IDS_ERROR_0x00081B:
																																	(err_number__ == 0x00081C ? IDS_ERROR_0x00081C:
																																		(err_number__ == 0x00081D ? IDS_ERROR_0x00081D:
																																			(err_number__ == 0x00081E ? IDS_ERROR_0x00081E:
																																				(0)))))))))))))))))))))))))))))));
			CStringW csError;
			if (!res__)
				csError = _T("Unknown Error");
			else
				csError = details::Error_GetString(res__);
			m_buffer.Format(_T("HiBioAPI Security Extension Error: [code = 0x%x] %s"), err_number__, csError.GetString());
		} break;
	default:; // win 32
		if (S_OK != m_hResult)
		{
			shared::lite::common::CSysError error(m_hResult);
			m_buffer.Format(_T("HiBioAPI Win32 Error: [code = 0x%x] %s"), m_hResult, error.GetDescription());
		}
		else
			m_buffer.Format(_T("HiBioAPI Win32 Error: [code = 0x%x]"), err_number__);
	}
	return m_buffer.GetString();
}

HRESULT   CError::GetHresult(void) const
{
	if (S_OK != m_hResult)
		return  m_hResult;
	HRESULT hr__ = (IsFailure() ? E_FAIL : S_OK);
	if (IsFailure())
	{
		const INT err_number__ = details::Error_GetNumberFromCode(DWORD(m_code));
		details::Error_SourceModule::_enum e_src__ = details::Error_GetSourceFromCode(DWORD(m_code));
		switch (e_src__)
		{
		case details::Error_SourceModule::eBSP:
		case details::Error_SourceModule::eFramework:
			{
				if (0x000118 == err_number__) hr__ = S_FALSE; // detects a user cancelled the operation (by pressing cancel button)
			} break;
		default:
			hr__ = E_FAIL; // simplified approach for now!
		}
	}
	return  hr__;
}

bool      CError::IsFailure(void) const
{
	return (m_code != BioAPI_OK);
}

/////////////////////////////////////////////////////////////////////////////

void      CError::SetDeviceCode(const INT nCode)
{
	m_code = nCode;
}

void      CError::SetHresult(const HRESULT hr__)
{
	m_hResult = hr__;
}