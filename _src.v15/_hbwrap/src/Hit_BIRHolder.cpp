/*
	Created by Tech_dog (ebontrop@gmail.com) on 27-Mar-2014 at 4:40:36am, GMT+4, Saint-Petersburg, Thursday;
	This is Shared Recognition Hitachi BioAPI BIR Holder class implementation file.
	-----------------------------------------------------------------------------
	Adopted to version v15 on 15-Feb-2021 at 6:38:23.030 am, UTC+7, Novosibirsk, Monday;
*/
#include "StdAfx.h"
#include "Hit_BIRHolder.h"

using namespace shared;
using namespace shared::recognition;
using namespace shared::recognition::client;
using namespace shared::recognition::client::Hitachi;

/////////////////////////////////////////////////////////////////////////////

CBIRHolder::CBIRHolder(void):
	m_hResult(OLE_E_BLANK),
	m_provider(NULL)
{
	::memset((void*)&m_bir, 0, sizeof(m_bir));
}

CBIRHolder::CBIRHolder(const BioAPI_BIR& bir_ref):
	m_hResult(S_OK),
	m_provider(NULL)
{
	::memset((void*)&m_bir, 0, sizeof(m_bir));
	this->Create(bir_ref);
}

CBIRHolder::CBIRHolder(const BioAPI_HANDLE provider_handle):
	m_hResult(S_OK),
	m_provider(provider_handle)
{
	::memset((void*)&m_bir, 0, sizeof(m_bir));
}

CBIRHolder::CBIRHolder(SAFEARRAY* raw_data):
	m_hResult(S_OK),
	m_provider(NULL)
{
	::memset((void*)&m_bir, 0, sizeof(m_bir));
	this->Create(raw_data);
}

CBIRHolder::~CBIRHolder(void)
{
	this->__clear();
}

/////////////////////////////////////////////////////////////////////////////

HRESULT           CBIRHolder::Clear(void)
{
	this->__clear();
	return S_OK;
}

HRESULT           CBIRHolder::Create(const _variant_t& varData)
{
	if (0 == (VT_ARRAY & varData.vt) || 0 == (VT_UI1 & varData.vt))
		return DISP_E_TYPEMISMATCH;
	if (!varData.parray)
		return E_INVALIDARG;
	HRESULT hr_ = this->Create(varData.parray);
	return  hr_;
}

HRESULT           CBIRHolder::Create(const BioAPI_BIR& bir_ref)
{
	this->__clear();
	m_hResult = S_OK;
	m_bir.Header = bir_ref.Header;
	m_bir.BiometricData.Length = bir_ref.BiometricData.Length;
	if (NULL != bir_ref.BiometricData.Length && NULL != bir_ref.BiometricData.Data)
	{
		try
		{
			m_bir.BiometricData.Data = new BYTE[bir_ref.BiometricData.Length];
			::memcpy((void*)m_bir.BiometricData.Data, (void*)bir_ref.BiometricData.Data, sizeof(BYTE) * bir_ref.BiometricData.Length);
		}
		catch (::std::bad_alloc&)
		{
			ATLASSERT(FALSE);
			m_hResult = E_OUTOFMEMORY;
			return m_hResult;
		}
	}
	m_bir.SecurityBlock.Length = bir_ref.SecurityBlock.Length;
	if (NULL != bir_ref.SecurityBlock.Length && NULL != bir_ref.SecurityBlock.Data)
	{
		try
		{
			m_bir.SecurityBlock.Data = new BYTE[bir_ref.SecurityBlock.Length];
			::memcpy((void*)m_bir.SecurityBlock.Data, (void*)bir_ref.SecurityBlock.Data, sizeof(BYTE) * bir_ref.SecurityBlock.Length);
		}
		catch (::std::bad_alloc&)
		{
			ATLASSERT(FALSE);
			m_hResult = E_OUTOFMEMORY;
			return m_hResult;
		}
	}
	return m_hResult;
}

HRESULT           CBIRHolder::Create(const INT_PTR ptr__)
{
	this->__clear();
	BioAPI_RETURN ret__ = Zw_BioAPI_GetBIRFromHandle(m_provider, (BioAPI_BIR_HANDLE)ptr__, &m_bir);
	m_error.SetDeviceCode(ret__);
	return m_error.GetHresult();
}

HRESULT           CBIRHolder::Create(LPCWSTR file_)
{
	if (NULL == file_ || NULL == ::_tcslen(file_))
		return E_INVALIDARG;
	__clear();
	HANDLE hFile = ::CreateFile(file_, GENERIC_READ, 0, NULL, OPEN_EXISTING, 0, NULL);
	if (INVALID_HANDLE_VALUE == hFile)
		return HRESULT_FROM_WIN32(::GetLastError());
	HRESULT hr__ = S_OK;
	DWORD dwReadSize = 0;
	do
	{
		if (NULL == ::ReadFile(hFile, &(m_bir.Header), sizeof(m_bir.Header), &dwReadSize, NULL))
		{
			hr__ = HRESULT_FROM_WIN32(::GetLastError()); break;
		}
		if (NULL == ::ReadFile(hFile, &(m_bir.BiometricData.Length), sizeof(m_bir.BiometricData.Length), &dwReadSize, NULL))
		{
			hr__ = HRESULT_FROM_WIN32(::GetLastError()); break;
		}
		else if (0 < m_bir.BiometricData.Length)
		{
			try
			{
				m_bir.BiometricData.Data = new BYTE[m_bir.BiometricData.Length];
				::memset((void*)m_bir.BiometricData.Data, 0, sizeof(BYTE)*m_bir.BiometricData.Length);
			}
			catch(::std::bad_alloc&)
			{
				hr__ = E_OUTOFMEMORY; break;
			}
			if (NULL == ::ReadFile(hFile, m_bir.BiometricData.Data, m_bir.BiometricData.Length , &dwReadSize, NULL))
			{
				hr__ = HRESULT_FROM_WIN32(::GetLastError()); break;
			}
		}
		if (NULL == ::ReadFile(hFile, &(m_bir.SecurityBlock.Length), sizeof(m_bir.SecurityBlock.Length), &dwReadSize, NULL))
		{
			hr__ = HRESULT_FROM_WIN32(::GetLastError()); break;
		}
		else if (0 < m_bir.SecurityBlock.Length)
		{
			try
			{
				m_bir.SecurityBlock.Data = new BYTE[m_bir.SecurityBlock.Length];
				::memset((void*)m_bir.SecurityBlock.Data, 0, sizeof(BYTE)*m_bir.SecurityBlock.Length);
			}
			catch(::std::bad_alloc&)
			{
				hr__ = E_OUTOFMEMORY; break;
			}
			if (NULL == ::ReadFile(hFile, m_bir.SecurityBlock.Data, m_bir.SecurityBlock.Length , &dwReadSize, NULL))
			{
				hr__ = HRESULT_FROM_WIN32(::GetLastError()); break;
			}
		}
	}
	while (false);
	::CloseHandle(hFile);
	hFile = NULL;
	return hr__;
}

HRESULT           CBIRHolder::Create(SAFEARRAY* raw_data)
{
	this->__clear();
	if (NULL == raw_data)
	{
		m_hResult = E_INVALIDARG;
		return m_hResult;
	}
	PBYTE pData = NULL;
	m_hResult = ::SafeArrayAccessData(raw_data, (void**) &pData);
	if (S_OK != m_hResult)
		return m_hResult;
	try
	{
		INT memory_block = sizeof(m_bir.Header);
		::memcpy((void*)&m_bir.Header, (void*)pData, memory_block * sizeof(BYTE));
		pData += memory_block;
		memory_block = sizeof(m_bir.BiometricData.Length);
		::memcpy((void*)&m_bir.BiometricData.Length, (void*)pData, memory_block * sizeof(BYTE));
		pData += memory_block;
		if (NULL != m_bir.BiometricData.Length)
		{
			memory_block = m_bir.BiometricData.Length;
			m_bir.BiometricData.Data = new BYTE[memory_block];
			::memcpy((void*)m_bir.BiometricData.Data, (void*)pData, memory_block * sizeof(BYTE));
			pData += memory_block;
		}
		memory_block = sizeof(m_bir.SecurityBlock.Length);
		::memcpy((void*)&m_bir.SecurityBlock.Length, (void*)pData, memory_block * sizeof(BYTE));
		pData += memory_block;
		if (NULL != m_bir.SecurityBlock.Length)
		{
			memory_block = m_bir.SecurityBlock.Length;
			m_bir.SecurityBlock.Data = new BYTE[memory_block];
			::memcpy((void*)m_bir.SecurityBlock.Data, (void*)pData, memory_block * sizeof(BYTE));
		}
	}
	catch(::std::bad_alloc&)
	{
		ATLASSERT(FALSE==TRUE);
		m_hResult = E_OUTOFMEMORY;
	}
	catch(...)
	{
		ATLASSERT(FALSE==TRUE);
		m_hResult = E_OUTOFMEMORY;
	}
	::SafeArrayUnaccessData(raw_data);
	return m_hResult;
}

const BioAPI_BIR& CBIRHolder::GetBir_Ref(void) const
{
	return m_bir;
}

BioAPI_BIR&       CBIRHolder::GetBir_Ref(void)
{
	return m_bir;
}

const CError&     CBIRHolder::GetError_Ref(void) const
{
	return m_error;
}

HRESULT           CBIRHolder::GetLastResult(void) const
{
	return m_hResult;
}

bool              CBIRHolder::IsFailure(void) const
{
	return (S_OK != m_hResult);
}

/////////////////////////////////////////////////////////////////////////////

void            CBIRHolder::__clear(void)
{
	if (NULL != m_bir.BiometricData.Data)
	{
		if (NULL != m_provider)
			Zw_BioAPI_Free(m_bir.BiometricData.Data);
		else
			delete [] m_bir.BiometricData.Data;
		m_bir.BiometricData.Data = NULL;
	}
	if (NULL != m_bir.SecurityBlock.Data)
	{
		if (NULL != m_provider)
			Zw_BioAPI_Free(m_bir.SecurityBlock.Data);
		else
			delete [] m_bir.SecurityBlock.Data;
		m_bir.SecurityBlock.Data = NULL;
	}
	::memset((void*)&m_bir, 0, sizeof(m_bir));
	m_hResult = OLE_E_BLANK;
}