/*
	Created by Tech_dog (ebontrop@gmail.com) on 22-Mar-2014 at 3:01:37pm, GMT+4, Moscow Region,
	Rail Road Train #43, Coatch #6, Place #1, Saturday;
	This is Shared Recognition Hitachi BioAPI SDK Initializer class implementation file.
	-----------------------------------------------------------------------------
	Adopted to version v15 on 15-Feb-2021 at 5:47:47.816 am, UTC+7, Novosibirsk, Monday;
*/
#include "StdAfx.h"
#include "Hit_Initializer.h"
#include "Hit_ClientWrap.h"

using namespace shared;
using namespace shared::recognition;
using namespace shared::recognition::client;
using namespace shared::recognition::client::Hitachi;

/////////////////////////////////////////////////////////////////////////////
namespace shared { namespace recognition { namespace client { namespace Hitachi { namespace details
{
	static const BioAPI_VERSION Initializer_ApiVersion = 0x20;
}}}}}
/////////////////////////////////////////////////////////////////////////////

CIdentifierEnum::CIdentifierEnum(void)
{
}

CIdentifierEnum::~CIdentifierEnum(void)
{
	m_ids.clear();
}

/////////////////////////////////////////////////////////////////////////////

LPCWSTR   CIdentifierEnum::GetBSPOf(const INT nIndex) const
{
	if (nIndex < 0 || nIndex > GetCount() - 1)
		return NULL;
	else
		return m_ids[nIndex].GetString();
}

INT       CIdentifierEnum::GetCount(void) const
{
	return (INT)m_ids.size();
}

LPCWSTR   CIdentifierEnum::GetDefault(void) const
{
	if (m_ids.empty())
		return NULL;
	return m_ids[GetCount() - 1].GetString();
}

const IError& CIdentifierEnum::GetLastError_Ref(void) const
{
	return m_error;
}

HRESULT   CIdentifierEnum::Initialize(void)
{
	BioAPI_BSP_SCHEMA* pSchemas = NULL;
	uint32_t  cnt__ = 0;
	if (!m_ids.empty())m_ids.clear();
	const BioAPI_RETURN res__ = Zw_BioAPI_EnumBSPs(&pSchemas, &cnt__);
	if (BioAPI_OK == res__)
	{
		for (uint32_t i__ = 0; i__ < cnt__; i__++)
		{
			CImpersonate  imp(pSchemas[i__].BSPUuid, CImpersonateParam::eBufferLen);
			CStringW  cs_bsp__(imp.UUIDToString());
			try
			{
				m_ids.push_back(cs_bsp__);
			}
			catch(::std::bad_alloc&)
			{
				ATLASSERT(FALSE==TRUE);
			}
			Zw_BioAPI_Free(pSchemas[i__].Path);
			Zw_BioAPI_Free(pSchemas[i__].BSPSupportedFormats) ;
		}
		Zw_BioAPI_Free(pSchemas);
	}
	pSchemas = NULL;
	cnt__ = 0;
	m_error.SetDeviceCode(res__);
	return m_error.GetHresult();
}

/////////////////////////////////////////////////////////////////////////////

CError&   CIdentifierEnum::GetLastError_Ref(void)
{
	return m_error;
}

/////////////////////////////////////////////////////////////////////////////

CInitializer::CInitializer(void):
	m_dev_handle(NULL),
	m_dev_result(0),
	m_error_ref(m_id_enum.GetLastError_Ref()),
	m_unit_schemas(NULL),
	m_unit_schemas_num(0),
	m_event_handler(*this)
{
	TRACE_FUNC();
	::memset((void*)m_flags, 0, sizeof(bool) * _countof(m_flags));
}

CInitializer::~CInitializer(void)
{
	TRACE_FUNC();
	Terminate();
}

/////////////////////////////////////////////////////////////////////////////

const IDeviceIdentifierEnumerator&  CInitializer::GetDeviceIdentifierEnum_Ref(void) const
{
	return m_id_enum;
}

INT_PTR         CInitializer::GetHandle(void) const
{
	return m_dev_handle;
}

const IError&   CInitializer::GetLastError_Ref(void) const
{
	return m_error_ref;
}

eProcessorType  CInitializer::GetProcessorType(void) const
{
	return shared::recognition::eHibio;
}

HRESULT         CInitializer::Initialize(void)
{
	TRACE_FUNC();
	m_dev_result = Zw_BioAPI_Terminate();
	m_dev_result = Zw_BioAPI_Init(details::Initializer_ApiVersion);
	m_error_ref.SetDeviceCode(m_dev_result);
	HRESULT hr__ = m_error_ref.GetHresult();
	if (S_OK != hr__)
	{
		CStringW cs_error;
		cs_error.Format(_T("The error occurred while initializing of BioAPI, code=0x%x, desc=%s"), hr__, m_error_ref.GetDescription());
		TRACE_ERROR(cs_error.GetString());
		return  hr__;
	}
	hr__ = m_id_enum.Initialize();
	if (S_OK != hr__)
	{
		TRACE_ERR__a1(_T("The error occurred while initializing device identifier enumerator, code=0x%x"), hr__);
		return  hr__;
	}
	m_flags[CInitializer::IPF__CORE_SUCCESS] = true;

	LPCWSTR pDefaultBSP = m_id_enum.GetDefault();
	CImpersonate imp__;
	imp__.StringToUUID(pDefaultBSP);
	m_dev_result = Zw_BioAPI_BSPLoad((BioAPI_UUID*)imp__.GetUUID_Ptr(), NULL/*(BioAPI_EventHandler)CEventHandler::EventCallback*/, NULL);
	m_error_ref.SetDeviceCode(m_dev_result);
	hr__ = m_error_ref.GetHresult();
	if (S_OK != hr__)
	{
		CStringW cs_error;
		cs_error.Format(_T("The error occurred while loading the device driver, code=0x%x, desc=%s"), hr__, m_error_ref.GetDescription());
		TRACE_ERROR(cs_error.GetString());
		return  hr__;
	}
	m_flags[CInitializer::IPF__BSP_LOAD_SUCCESS] = true;

	BioAPI_RETURN ret__ = Zw_BioAPI_QueryUnits((BioAPI_UUID*)imp__.GetUUID_Ptr(), &m_unit_schemas, &m_unit_schemas_num);
	m_error_ref.SetDeviceCode(ret__);
	hr__ = m_error_ref.GetHresult();
	if (S_OK != hr__)
	{
		CStringW cs_error;
		cs_error.Format(_T("The error occurred while initializing the device unit, code=0x%x, desc=%s"), hr__, m_error_ref.GetDescription());
		TRACE_ERROR(cs_error.GetString());
		return  hr__;
	}
	m_flags[CInitializer::IPF__UNIT_SCHEMA_SUCCESS] = true;

	BioAPI_HANDLE handle = NULL;
	if (NULL == m_unit_schemas_num)
	{
		m_dev_result = Zw_BioAPI_BSPAttach((BioAPI_UUID*)imp__.GetUUID_Ptr(), details::Initializer_ApiVersion, NULL, NULL, &handle);
	}
	else
	{
		BioAPI_UNIT_LIST_ELEMENT UnitList[1] = {0};
		UnitList[0].UnitCategory = m_unit_schemas[0].UnitCategory;
		UnitList[0].UnitId = m_unit_schemas[0].UnitId;
		m_dev_result = Zw_BioAPI_BSPAttach((BioAPI_UUID*)imp__.GetUUID_Ptr(), details::Initializer_ApiVersion, UnitList, 1, &handle);
	}
	m_error_ref.SetDeviceCode(m_dev_result);
	hr__ = m_error_ref.GetHresult();
	if (S_OK != hr__)
	{
		CStringW cs_error;
		cs_error.Format(_T("The error occurred while connecting to device unit, code=0x%x, desc=%s"), hr__, m_error_ref.GetDescription());
		TRACE_ERROR(cs_error.GetString());
		return  hr__;
	}
	m_flags[CInitializer::IPF__BSP_ATTACH_SUCCESS] = true; // my God! initialization completed successfully!!!
	m_dev_handle = (INT_PTR)handle; // TO DO: replace handle data type with appropriate one!
	TRACE_INFO(_T("The Hitachi device driver initialization is successful"));
	return  hr__;
}

bool            CInitializer::IsInitialized(void) const
{
	return (m_flags[CInitializer::IPF__CORE_SUCCESS]
			&& m_flags[CInitializer::IPF__BSP_LOAD_SUCCESS]
				&& m_flags[CInitializer::IPF__BSP_ATTACH_SUCCESS]
					&& m_flags[CInitializer::IPF__UNIT_SCHEMA_SUCCESS]);
}

HRESULT         CInitializer::Terminate(void)
{
	TRACE_FUNC();
	HRESULT hr__ = S_FALSE;
	if (m_flags[CInitializer::IPF__BSP_ATTACH_SUCCESS])
	{
		BioAPI_HANDLE handle = (BioAPI_HANDLE)m_dev_handle;
		m_dev_handle = NULL;
		m_dev_result = Zw_BioAPI_BSPDetach(handle);
		m_error_ref.SetDeviceCode(m_dev_result);
		hr__ = m_error_ref.GetHresult(); // for debug purposes!
		if (S_OK != hr__)
		{
			TRACE_WARN_a1(_T("The error occurred while detaching from device driver, code=0x%x"), hr__);
		}
	}
	if (m_flags[CInitializer::IPF__BSP_LOAD_SUCCESS])
	{
		LPCWSTR pDefaultBSP = m_id_enum.GetDefault();
		CImpersonate imp__;
		imp__.StringToUUID(pDefaultBSP);

		m_dev_result = Zw_BioAPI_BSPUnload((BioAPI_UUID*)imp__.GetUUID_Ptr(), NULL/*(BioAPI_EventHandler)CEventHandler::EventCallback*/, NULL);
		m_error_ref.SetDeviceCode(m_dev_result);
		hr__ = m_error_ref.GetHresult(); // for debug purposes!
		if (S_OK != hr__)
		{
			TRACE_WARN_a1(_T("The error occurred while unloading the device driver, code=0x%x"), hr__);
		}
	}
	if (m_flags[CInitializer::IPF__UNIT_SCHEMA_SUCCESS])
	{
		for (UINT i__ = 0; i__ < m_unit_schemas_num; i__++ )
		{
			Zw_BioAPI_Free( m_unit_schemas[i__].UnitProperty.Data);
		}
		Zw_BioAPI_Free(m_unit_schemas); m_unit_schemas = NULL;
		m_unit_schemas_num = 0;
	}
	if (m_flags[CInitializer::IPF__CORE_SUCCESS])
	{
		const BioAPI_RETURN ret__ = Zw_BioAPI_Terminate();
		m_error_ref.SetDeviceCode(ret__);
		hr__ = m_error_ref.GetHresult(); // for debug purposes!
		if (S_OK != hr__)
		{
			TRACE_WARN_a1(_T("The error occurred while terminating the BioAPI, code=0x%x"), hr__);
		}
	}
	::memset((void*)m_flags, 0, sizeof(bool) * _countof(m_flags));
	return  hr__;
}