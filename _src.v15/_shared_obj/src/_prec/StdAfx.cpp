/*
	Created by Tech_dog (ebontrop@gmail.com) on 9-Apr-2014 at 1:44:08pm, GMT+4, Saint-Petersburg, Wednesday;
	This is Platinum Payroll Systems Shared Objects library precompiled headers implementation file.
	-----------------------------------------------------------------------------
	Adopted to version v15 on 15-Feb-2021 at 9:59:28.853 pm, UTC+7, Novosibirsk, Monday;
*/
#include "StdAfx.h"

#if (_ATL_VER < 0x0700)
#include <atlimpl.cpp>
#endif //(_ATL_VER < 0x0700)
