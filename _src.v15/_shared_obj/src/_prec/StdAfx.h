#ifndef __PLATINUMSHAREDOBJECTSLIBRARYPRECOMPILEDHEADER_H_3A5DFE8D_4997_462c_A60A_77FC46572367_INCLUDED
#define __PLATINUMSHAREDOBJECTSLIBRARYPRECOMPILEDHEADER_H_3A5DFE8D_4997_462c_A60A_77FC46572367_INCLUDED
/*
	Created by Tech_dog (ebontrop@gmail.com) on 9-Apr-2014 at 1:37:57pm, GMT+4, Saint-Petersburg, Wednesday;
	This is Platinum Payroll Systems Shared Objects library precompiled headers definition file.
	-----------------------------------------------------------------------------
	Adopted to version v15 on 15-Feb-2021 at 9:44:42.244 pm, UTC+7, Novosibirsk, Monday;
*/

#define WIN32_LEAN_AND_MEAN

#ifndef  WINVER
#define  WINVER         0x0501  // this is for use Windows XP (or later) specific feature(s)
#endif   WINVER

#ifndef _WIN32_WINNT
#define _WIN32_WINNT    0x0501  // this is for use Windows XP (or later) specific feature(s)
#endif  _WIN32_WINNT

#ifndef _WIN32_IE
#define _WIN32_IE       0x0600  // this is for use IE 7 (or later) specific feature(s)
#endif  _WIN32_IE

#pragma warning(disable: 4481)  // nonstandard extension used: override specifier 'override'
#pragma warning(disable: 4996)  // security warning: function or variable may be unsafe

#include <atlbase.h>
#include <atlstr.h>
#include <atlsafe.h>
#include <comutil.h>
#include <vector>
#include <map>
#include <time.h>

#endif/*__PLATINUMSHAREDOBJECTSLIBRARYPRECOMPILEDHEADER_H_3A5DFE8D_4997_462c_A60A_77FC46572367_INCLUDED*/