/*
	Created by Tech_dog (ebontrop@gmail.com) on 27-Mar-2014 at 2:50:04am, GMT+4, Saint-Petersburg, Thursday;
	This is Platinum Client Track Time Data Provider class implementation file.
	-----------------------------------------------------------------------------
	Adopted to version v15 on 16-Feb-2021 at 4:46:31.048 am, UTC+7, Novosibirsk, Tuesday;
*/
#include "StdAfx.h"
#include "DataProvider_4_TimeTrack.h"

using namespace Platinum::client::data;

using namespace shared::lite::data;
using namespace shared::lite::common;
using namespace shared::lite::persistent;

extern CApplication&    Global_GetAppObjectRef(void);
/////////////////////////////////////////////////////////////////////////////

CTimeTrackDataSpec::CTimeTrackDataSpec(void)
{
	const INT nFields = CTimeTrackDataSpec::nFieldCount;
	for ( INT i_ = 0; i_ < nFields; i_++)
	{
		CStringW cs_field;
		switch (i_)
		{
		case CTimeTrackDataSpec::eCode:     cs_field = _T("[Emp_Code]"); break;
		case CTimeTrackDataSpec::eDate:     cs_field = _T("[Rec_Date]"); break;
		case CTimeTrackDataSpec::eTime:     cs_field = _T("[Rec_Time]"); break;
		case CTimeTrackDataSpec::eWorkArea: cs_field = _T("[Rec_WA]");   break;
		case CTimeTrackDataSpec::eFvId:     cs_field = _T("[Fv_Uid]");   break;
		default:
			continue;
		}
		try
		{
			TBase::m_fields.push_back(cs_field);
		} catch (::std::bad_alloc&)
		{
			break;
		}
	}

	m_range = ::std::make_pair(
				CTimeTrackDataSpec::nFieldCount - 1,
				CTimeTrackDataSpec::nFieldCount - 0
			);
}

/////////////////////////////////////////////////////////////////////////////

CTimeTrackDataProvider:: CTimeTrackDataProvider(void) : m_bDirty(false) { }
CTimeTrackDataProvider::~CTimeTrackDataProvider(void) { m_records.clear(); }

/////////////////////////////////////////////////////////////////////////////

namespace Platinum { namespace client { namespace data { namespace details
{
	static CTimeTrackRecord& TimeTrackProvider_InvalidRecordRef(void)
	{
		static CTimeTrackRecord record_(false);
		return record_;
	}

	static HRESULT    TimeTrackProvider_CreateHeader(CCsvFile::THeader& header_ref)
	{
		CTimeTrackDataSpec spec_;
		header_ref = spec_.CreateHeader();
		return S_OK;
	}

	static HRESULT    TimeTrackProvider_CreateRow(CCsvFile::TRow& row_ref, const CTimeTrackRecord& rec_ref, CBase64& base_64)
	{
		CStringW cs_encoded;
		const INT nFields = CTimeTrackDataSpec::nFieldCount;
		for ( INT i_ = 0; i_ < nFields; i_++)
		{
			try
			{
				CStringW cs_buffer;
				switch(i_)
				{
				case CTimeTrackDataSpec::eCode:     cs_buffer = rec_ref.Code();     break;
				case CTimeTrackDataSpec::eDate:     cs_buffer = rec_ref.Date();     break;
				case CTimeTrackDataSpec::eTime:     cs_buffer = rec_ref.Time();     break;
				case CTimeTrackDataSpec::eWorkArea: cs_buffer = rec_ref.WorkArea(); break;
				case CTimeTrackDataSpec::eFvId:     cs_buffer = rec_ref.FvTimestamp().ValueAsText(); break;
				default:
					return DISP_E_BADINDEX;
				}
				const HRESULT hr_ = base_64.Encode(cs_buffer, cs_encoded);
				if (S_OK == hr_)
					row_ref.push_back(cs_encoded);
				else
					return hr_;
			} catch (::std::bad_alloc&){ return E_OUTOFMEMORY; }
		}
		return S_OK;
	}

	static HRESULT    TimeTrackProvider_SaveEntry(LPCWSTR pszStorage, const CTimeTrackRecord& rec_ref)
	{
		HRESULT hr_ = S_OK;
		const bool bIsFileExist = CCsvFile::IsFileExist(pszStorage);
		if (!bIsFileExist)
		{
			CCsvFile::THeader header;
			hr_ = details::TimeTrackProvider_CreateHeader(header);
			if (S_OK != hr_)
				return  hr_;
			hr_ = CCsvFile::CreateFileFromHeader(pszStorage, header, eCsvFileOption::eUseCommaSeparator);
			if (S_OK != hr_)
				return  hr_;
		}
		CBase64 base_64;
		if (true)
		{
			CCsvFile::TRow row;
			hr_ = details::TimeTrackProvider_CreateRow(row, rec_ref, base_64);
			if (S_OK == hr_)
			{
				CCsvFile csv(eCsvFileOption::eUseCommaSeparator);
				hr_ = csv.AppendToFile(pszStorage, row);
			}
		}
		return  hr_;
	}

	static CStringW TimeTrackProvider_GetBackupFileName(void)
	{
		return CStringW(_T("employee_entries_backup.csv"));
	}

	static CStringW TimeTrackProvider_GetMainFileName(void)
	{
		return CStringW(_T("employee_entries.csv"));
	}
}}}}

/////////////////////////////////////////////////////////////////////////////

HRESULT   CTimeTrackDataProvider::Append(const CTimeTrackRecord& new_rec)
{
	try
	{
		m_records.push_back(new_rec);
	}
	catch(::std::bad_alloc&){ return E_OUTOFMEMORY; }
	m_bDirty = true;
	return S_OK;
}

HRESULT   CTimeTrackDataProvider::Initialize(const bool bSuppressAccessDeniedMessage)
{
	CApplicationCursor wait_cursor(IDC_APPSTARTING);

	CStringW cs_storage;
	HRESULT hr_ = Platinum::client::data::GetStorageFolder(cs_storage, bSuppressAccessDeniedMessage);
	if (S_OK != hr_)
		return  hr_;

	cs_storage+= _T("employee_entries.csv");
	if (!CCsvFile::IsFileExist(cs_storage))
		return S_OK; // can be clean machine

	CCsvFile csv(eCsvFileOption::eUseCommaSeparator);
	hr_ = csv.Load(cs_storage, true);
	if (S_OK != hr_)
	{
		DisplayOpenDataFileError(cs_storage, hr_);
		return  hr_;
	}

	CTimeTrackDataSpec spec_;
	hr_ = spec_.ValidateData(csv, true);
	if (FAILED(hr_))
		return hr_;

	CBase64 base_64;
	const INT nFields = csv.FieldCount();
	const INT nRows = csv.RowCount();

	for (INT i_ = 0; i_ < nRows; i_++)
	{
		const CCsvFile::TRow& row = csv.Row(i_);
		const INT nRowSize = (INT)row.size();
		CTimeTrackRecord record;
		CStringW cs_buffer;
		for (INT j_ = 0; j_ < nFields; j_++)
		{
			CStringW cs_decoded;
			if (nRowSize > j_)
			{
				cs_buffer = row[j_].GetString();
				hr_ = base_64.Decode(cs_buffer.GetString(), cs_decoded);
				if (S_OK != hr_)
					cs_decoded = _T("#invalid");
			}
			else
				cs_decoded = _T("#invalid");
			switch (j_)
			{
			case CTimeTrackDataSpec::eCode:     record.Code    (cs_decoded);     break;
			case CTimeTrackDataSpec::eDate:     record.Date    (cs_decoded);     break;
			case CTimeTrackDataSpec::eTime:     record.Time    (cs_decoded);     break;
			case CTimeTrackDataSpec::eWorkArea: record.WorkArea(cs_decoded);     break;
			case CTimeTrackDataSpec::eFvId:     record.FvTimestamp().ValueAsText(cs_decoded, false); break;
			}
		}
		hr_ = this->Append(record);
		if (S_OK != hr_)
			break;
	}
	return  hr_;
}

HRESULT   CTimeTrackDataProvider::Initialize(const bool bSuppressAccessDeniedMessage, const CEmployeeRecsCache& _cache)
{
	HRESULT hr_ = this->Initialize(bSuppressAccessDeniedMessage);
	if (S_OK == hr_)
	{
		for (INT i_ = 0; i_ < this->RecordCount(); i_++)
		{
			CTimeTrackRecord& rec_ = this->Record(i_);
			LPCWSTR pszCode = rec_.Code();

			const CEmployeeDataRecord& emp_ = _cache.ItemOf(pszCode);
			if (emp_.IsValid())
				rec_.Name(emp_.Name());
			else
				rec_.Name(_T("#n/a"));
		}
	}
	return hr_;
}

const
CTimeTrackRecord& CTimeTrackDataProvider::Record(const INT nIndex)const
{
	if (0 > nIndex || nIndex > this->RecordCount() - 1)
		return details::TimeTrackProvider_InvalidRecordRef();
	else
		return m_records[nIndex];
}

CTimeTrackRecord& CTimeTrackDataProvider::Record(const INT nIndex)
{
	if (0 > nIndex || nIndex > this->RecordCount() - 1)
		return details::TimeTrackProvider_InvalidRecordRef();
	else
		return m_records[nIndex];
}

INT       CTimeTrackDataProvider::RecordCount(void)const
{
	return (INT)m_records.size();
}

HRESULT   CTimeTrackDataProvider::Refresh(void)
{
	if (!m_records.empty()) m_records.clear();
	m_bDirty = false;
	HRESULT hr_ = this->Initialize(true);
	return  hr_;
}

HRESULT   CTimeTrackDataProvider::Refresh(const CEmployeeRecsCache& _cache)
{
	if (!m_records.empty()) m_records.clear();
	m_bDirty = false;
	HRESULT hr_ = this->Initialize(true, _cache);
	return  hr_;
}

HRESULT   CTimeTrackDataProvider::Save(void)
{
	if (!m_bDirty)
		return S_OK;

	CApplicationCursor wait_cursor;

	CStringW cs_storage;
	HRESULT hr_ = Platinum::client::data::GetStorageFolder(cs_storage);
	if (S_OK != hr_)
		return  hr_;
	cs_storage+= details::TimeTrackProvider_GetMainFileName();
	CCsvFile csv(eCsvFileOption::eUseCommaSeparator);
	{
		CCsvFile::THeader header;
		hr_ = details::TimeTrackProvider_CreateHeader(header);
		if (S_OK != hr_)
			return  hr_;
		hr_ = csv.Header(header);
		if (S_OK != hr_)
			return  hr_;
	}
	CBase64 base_64;
	{
		const INT nRecords = this->RecordCount();
		for ( INT i_ = 0; i_ < nRecords; i_++)
		{
			CCsvFile::TRow row;
			const CTimeTrackRecord& record = m_records[i_];
			hr_ = details::TimeTrackProvider_CreateRow(row, record, base_64);
			if (S_OK != hr_)
				break;
			hr_ = csv.AddRow(row);
			if (S_OK != hr_)
				break;
		}
	}
	if (S_OK == hr_)
		hr_ = csv.Save(cs_storage, true);
	
	// checks for backup option and if specified, duplicates the data to the alternative location
	CStringW cs_backup;
	hr_ = Platinum::client::data::GetBackupFolder(cs_backup);
	if (S_OK == hr_)
	{
		cs_backup += details::TimeTrackProvider_GetBackupFileName();
		hr_ = csv.Save(cs_backup, true);
	}

	return  hr_;
}

HRESULT   CTimeTrackDataProvider::SaveEntry(const CTimeTrackRecord& rec_ref)
{
	CApplicationCursor wait_cursor;

	CStringW cs_storage;
	HRESULT hr_ = Platinum::client::data::GetStorageFolder(cs_storage);
	if (S_OK != hr_)
		return  hr_;
	cs_storage+= details::TimeTrackProvider_GetMainFileName();

	hr_ = details::TimeTrackProvider_SaveEntry(cs_storage, rec_ref);
	
	// checks for backup option and if specified, duplicates the data to the alternative location
	CStringW cs_backup;
	hr_ = Platinum::client::data::GetBackupFolder(cs_backup);
	if (S_OK == hr_)
	{
		cs_backup += details::TimeTrackProvider_GetBackupFileName();
		hr_ = details::TimeTrackProvider_SaveEntry(cs_backup, rec_ref);
	}
	return  hr_;
}