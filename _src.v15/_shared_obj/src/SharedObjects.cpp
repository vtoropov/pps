/*
	Created by Tech_dog (ebontrop@gmail.com) on 22-Mar-2014 at 10:16:27pm, GMT+4, Moscow Region,
	Rail Road Train #43, Coatch #6, Place #1, Saturday;
	This is Platinum Client Application Shared Objects class implementation file.
	-----------------------------------------------------------------------------
	Adopted to version v15 on 16-Feb-2021 at 4:24:34.869 am, UTC+7, Novosibirsk, Tuesday;
*/
#include "StdAfx.h"
#include "SharedObjects.h"

using namespace Platinum;
using namespace Platinum::client;
using namespace Platinum::client::common;

using namespace shared::lite;
using namespace shared::recognition;

/////////////////////////////////////////////////////////////////////////////

namespace Platinum { namespace client { namespace common { namespace details
{
}}}}

/////////////////////////////////////////////////////////////////////////////

CSharedObjects::CSharedObjects(CCommonSettings& settings_ref):
	m_settings_ref(settings_ref),
	m_proc_type(eHibio),
	m_proc_init(m_proc_type),
	m_proc_obj (m_proc_init.GetObject_Ref()),
	m_hResult(OLE_E_BLANK)
{
	this->Initialize();
}

CSharedObjects::~CSharedObjects(void)
{
}

/////////////////////////////////////////////////////////////////////////////
const
CEmployeeRecsCache&  CSharedObjects::Cache(void)const
{
	return m_cache;
}

CEmployeeRecsCache&  CSharedObjects::Cache(void)
{
	return m_cache;
}

const IError&        CSharedObjects::Error(void) const
{
	return m_proc_obj.GetObject_Ref().GetLastError_Ref();
}

HRESULT              CSharedObjects::Initialize(void)
{
	if (this->IsInitialized())
		return HRESULT_FROM_WIN32(ERROR_ALREADY_INITIALIZED);

	m_hResult = m_proc_init.GetObject_Ref().Initialize();
	return m_hResult;
}

const
IInitializer&        CSharedObjects::Initializer(void) const
{
	return m_proc_init.GetObject_Ref();
}

IInitializer&        CSharedObjects::Initializer(void)
{
	return m_proc_init.GetObject_Ref();
}

bool      CSharedObjects::IsInitialized(void) const
{
	return (m_hResult == S_OK);
}

HRESULT   CSharedObjects::LastResult(void) const
{
	return m_hResult;
}

const
IImageProcessor&     CSharedObjects::Processor(void) const
{
	return m_proc_obj.GetObject_Ref();
}

IImageProcessor&     CSharedObjects::Processor(void)
{
	return m_proc_obj.GetObject_Ref();
}

const
CCommonSettings&     CSharedObjects::Settings(void) const
{
	return m_settings_ref;
}

CCommonSettings&     CSharedObjects::Settings(void)
{
	return m_settings_ref;
}