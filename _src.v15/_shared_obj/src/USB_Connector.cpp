/*
	Created by Tech_dog (ebontrop@gmail.com) on 19-May-2015, at 1:25:25pm, GMT+8, Phuket, Tuesday;
	This is PPS shared library USB notifications connector class implementation file.
	-----------------------------------------------------------------------------
	Adopted to version v15 on 15-Feb-2021 at 10:13:20.454 pm, UTC+7, Novosibirsk, Monday;
*/
#include "StdAfx.h"
#include "USB_Connector.h"

using namespace Platinum;
using namespace Platinum::client;
using namespace Platinum::client::usb;

/////////////////////////////////////////////////////////////////////////////

namespace Platinum { namespace client { namespace usb { namespace details
{
	HRESULT DeviceEnum_ObjectToEnum(IWbemClassObject* const pObject, eDeviceNotification::_e& eType)
	{
		if (!pObject)
			return E_INVALIDARG;
		_variant_t val_;
		HRESULT hr_ = pObject->Get(_T("__CLASS"), 0, &val_, NULL, NULL);
		if (S_OK != hr_)
			return  hr_;
		if (VT_BSTR != val_.vt || !val_.bstrVal)
			return DISP_E_TYPEMISMATCH;
		CStringW cs_class(val_.bstrVal);
		if (false){}
		else if (0 == cs_class.CompareNoCase(_T("__InstanceCreationEvent")))
			eType = eDeviceNotification::eArrival;
		else if (0 == cs_class.CompareNoCase(_T("__InstanceDeletionEvent")))
			eType = eDeviceNotification::eRemoval;
		else
			hr_ = S_FALSE;
		return  hr_;
	}

	HRESULT DeviceEnum_ObjectToTarget(IWbemClassObject* const pObject, ::ATL::CComPtr<IWbemClassObject>& obj_ref)
	{
		if (!pObject)
			return E_INVALIDARG;
		_variant_t val_;
		HRESULT hr_ = pObject->Get(_T("TargetInstance"), 0, &val_, NULL, NULL);
		if (S_OK != hr_)
			return  hr_;
		if (VT_UNKNOWN != val_.vt || !val_.punkVal)
			return DISP_E_TYPEMISMATCH;
		::ATL::CComQIPtr<IWbemClassObject> pTarget = val_.punkVal;
		if (!pTarget)
			return E_NOINTERFACE;
		else
		{
			obj_ref = pTarget;
			return hr_;
		}
	}

	HRESULT DeviceEnum_ObjectToTargetPath(IWbemClassObject* const pObject, _variant_t& v_path)
	{
		::ATL::CComPtr<IWbemClassObject> pTarget;
		HRESULT hr_ = DeviceEnum_ObjectToTarget(pObject, pTarget);
		if (S_OK != hr_)
			return  hr_;
		hr_= pTarget->Get(_T("__PATH"), 0, &v_path, NULL, NULL);
		return  hr_;
	}
}}}}

/////////////////////////////////////////////////////////////////////////////

CEventConnector::CNotificationHandler::CNotificationHandler(IDeviceNotification& notify_ref) : m_notify(notify_ref), m_lRef(0)
{
}

CEventConnector::CNotificationHandler::~CNotificationHandler(void)
{
}

/////////////////////////////////////////////////////////////////////////////

ULONG    CEventConnector::CNotificationHandler::AddRef(void)
{
	return ::InterlockedIncrement(&m_lRef);
}

ULONG    CEventConnector::CNotificationHandler::Release(void)
{
	LONG lRef = ::InterlockedDecrement(&m_lRef);
	if (!lRef)
		delete this;
	return lRef;
}

HRESULT  CEventConnector::CNotificationHandler::QueryInterface(REFIID riid, void** ppv)
{
	if (riid == IID_IUnknown || riid == IID_IWbemObjectSink)
	{
		*ppv = reinterpret_cast<IWbemObjectSink*>(this);
		this->AddRef();
		return WBEM_S_NO_ERROR;
	}
	else return E_NOINTERFACE;
}

/////////////////////////////////////////////////////////////////////////////

HRESULT  CEventConnector::CNotificationHandler::Indicate(
			/* [in] */ long lObjectCount,
			/* [size_is][in] */ __RPC__in_ecount_full(lObjectCount) IWbemClassObject** apObjArray)
{
	lObjectCount; apObjArray;
	if (apObjArray && lObjectCount)
	{
		for (long i_ = 0; i_ < lObjectCount; i_++)
		{
			IWbemClassObject* pObject = apObjArray[i_];
			if (!pObject)
				continue;
			eDeviceNotification::_e eType = eDeviceNotification::eUndefined;
			HRESULT hr_ = details::DeviceEnum_ObjectToEnum(pObject, eType);
			if (S_OK != hr_)
				continue;
			switch (eType)
			{
			case eDeviceNotification::eArrival:
				{
					_variant_t v_path;
					m_notify.DeviceNotification_OnChange(eDeviceNotification::eArrival, v_path);
				} break;
			case eDeviceNotification::eRemoval:
				{
					_variant_t v_path;
					hr_ = details::DeviceEnum_ObjectToTargetPath(pObject, v_path);
					m_notify.DeviceNotification_OnChange(eDeviceNotification::eRemoval, v_path);
				} break;
			}
		}
	}
	return WBEM_S_NO_ERROR;
}
	        
HRESULT  CEventConnector::CNotificationHandler::SetStatus(
				/* [in] */ long lFlags,
				/* [in] */ HRESULT hResult,
				/* [unique][in] */ __RPC__in_opt BSTR strParam,
				/* [unique][in] */ __RPC__in_opt IWbemClassObject* pObjParam)
{
	hResult; strParam; pObjParam;
	switch (lFlags)
	{
	case WBEM_STATUS_COMPLETE:
		{
		} break;
	}
	return WBEM_S_NO_ERROR;
}

/////////////////////////////////////////////////////////////////////////////

CEventConnector::CEventConnector(IDeviceNotification& notify_ref):
	m_notify (notify_ref),
	m_pHandler(NULL)
{
	m_error.Reset();
	::ATL::CComPtr<IWbemLocator> pLocator;
	m_error = ::CoCreateInstance(
				CLSID_WbemLocator,
				NULL,
				CLSCTX_INPROC_SERVER,
				IID_IWbemLocator, (LPVOID*) &pLocator);
	if (m_error) return;
	m_error = pLocator->ConnectServer(
				_bstr_t(L"ROOT\\CIMV2"),     // object path of WMI namespace
				NULL,                        // user name, NULL means current user
				NULL,                        // user password, NULL means current one
				NULL,                        // locale, NULL indicates current one
				0,                           // security flags
				0,                           // authority (e.g. Kerberos)
				NULL,                        // context object 
				&m_pService                  // pointer to IWbemServices proxy
				);
	if (m_error) return;
	m_error = ::CoSetProxyBlanket(
				m_pService,                  // indicates the proxy to set
				RPC_C_AUTHN_WINNT,           // RPC_C_AUTHN_xxx
				RPC_C_AUTHZ_NONE,            // RPC_C_AUTHZ_xxx
				NULL,                        // server principal name 
				RPC_C_AUTHN_LEVEL_CALL,      // RPC_C_AUTHN_LEVEL_xxx 
				RPC_C_IMP_LEVEL_IMPERSONATE, // RPC_C_IMP_LEVEL_xxx
				NULL,                        // client identity
				EOAC_NONE                    // proxy capabilities 
				);
	if (m_error) return;
	::ATL::CComPtr<IUnsecuredApartment> pUnsecure;
	m_error = ::CoCreateInstance(
				CLSID_UnsecuredApartment,
				NULL,
				CLSCTX_LOCAL_SERVER,
				IID_IUnsecuredApartment, 
				(void**)&pUnsecure);
	if (m_error) return;
	try { m_pHandler = new CNotificationHandler(m_notify); m_pHandler->AddRef(); }catch(::std::bad_alloc&){m_error = E_OUTOFMEMORY;}
	if (m_error) return;
	::ATL::CComPtr<IUnknown> pUnknown;
	m_error = pUnsecure->CreateObjectStub(m_pHandler, &pUnknown);
	if (m_error) return;
	m_error = pUnknown->QueryInterface(IID_IWbemObjectSink, (void**)&m_pObject);
	if (m_error) return;
	m_error = m_pService->ExecNotificationQueryAsync(
				_bstr_t("WQL"), 
				_bstr_t("SELECT * FROM __InstanceOperationEvent WITHIN 1 WHERE TargetInstance ISA 'Win32_PnPEntity'"), 
				WBEM_FLAG_SEND_STATUS, 
				NULL, 
				m_pObject);
}

CEventConnector::~CEventConnector(void)
{
	if (m_pService && m_pObject)
	{
		m_error = m_pService->CancelAsyncCall(m_pObject);
		m_error.Reset();
	}
	if (m_pHandler)
		m_pHandler->Release();
	m_pHandler = NULL;
}