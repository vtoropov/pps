/*
	Created by Tech_dog (ebontrop@gmail.com) on 24-May-2015 at 1:15:11pm, GMT+8, Phuket, Sunday;
	This is Platinum Client Library Employee Record Data Cache class implementation file.
	-----------------------------------------------------------------------------
	Adopted to version v15 on 16-Feb-2021 at 4:07:43.077 am, UTC+7, Novosibirsk, Tuesday;
*/
#include "StdAfx.h"
#include "EmployeeCache.h"

using namespace Platinum;
using namespace Platinum::client;
using namespace Platinum::client::data;

/////////////////////////////////////////////////////////////////////////////

namespace Platinum { namespace client { namespace data { namespace details
{
	using Platinum::client::data::CEmployeeDataRecord;

	CEmployeeDataRecord&  EmployeeRecsCache_InvalidData(void)
	{
		static CEmployeeDataRecord record(false);
		return record;
	}

	VOID                  EmployeeRecsCache_Reindex(const ::std::vector<CEmployeeDataRecord>& recs, ::std::map<CStringW, INT>& ndx_)
	{
		if (!ndx_.empty())ndx_.clear();

		const size_t t_count = recs.size();
		for ( size_t it_ = 0; it_ < t_count; it_++)
		{
			const CEmployeeDataRecord& rec_ref = recs[it_];
			ndx_[rec_ref.Code()] = static_cast<INT>(it_);
		}
	}
}}}}

/////////////////////////////////////////////////////////////////////////////

CEmployeeRecsCache:: CEmployeeRecsCache(void)
{
}

CEmployeeRecsCache::~CEmployeeRecsCache(void)
{
	this->Clear();
}

/////////////////////////////////////////////////////////////////////////////

HRESULT   CEmployeeRecsCache::Append(const CEmployeeDataRecord& rec_ref)
{
	CStringW cs_code(rec_ref.Code());
	TIndex::const_iterator it_ = m_index.find(cs_code);
	if (it_ == m_index.end())
	{
		try
		{
			const INT nIndex = (INT)m_cache.size();
			m_index[cs_code] = nIndex;
			m_cache.push_back(rec_ref);
		}
		catch(::std::bad_alloc&){ return E_OUTOFMEMORY; }
	}
	else
	{
		return HRESULT_FROM_WIN32(ERROR_OBJECT_ALREADY_EXISTS);
	}
	return S_OK;
}

HRESULT   CEmployeeRecsCache::Clear(void)
{
	if (!m_cache.empty())m_cache.clear();
	if (!m_index.empty())m_index.clear();
	return S_OK;
}

INT       CEmployeeRecsCache::Count(void)const
{
	return (INT)m_cache.size();
}

const
CEmployeeDataRecord&   CEmployeeRecsCache::Item  (const INT nIndex)const
{
	if (nIndex < 0 || nIndex > this->Count() -1 )
		return details::EmployeeRecsCache_InvalidData();
	else
		return m_cache[nIndex];
}

CEmployeeDataRecord&   CEmployeeRecsCache::Item  (const INT nIndex)
{
	if (nIndex < 0 || nIndex > this->Count() -1 )
		return details::EmployeeRecsCache_InvalidData();
	else
		return m_cache[nIndex];
}

const
CEmployeeDataRecord&   CEmployeeRecsCache::ItemOf(const CStringW& cs_code)const
{
	TIndex::const_iterator it_ = m_index.find(cs_code);
	if (it_ == m_index.end())
		return details::EmployeeRecsCache_InvalidData();
	const INT nIndex = it_->second;
	return this->Item(nIndex);
}

CEmployeeDataRecord&   CEmployeeRecsCache::ItemOf(const CStringW& cs_code)
{
	TIndex::iterator it_ = m_index.find(cs_code);
	if (it_ == m_index.end())
		return details::EmployeeRecsCache_InvalidData();
	const INT nIndex = it_->second;
	return this->Item(nIndex);
}

HRESULT   CEmployeeRecsCache::ItemOf(const CEmployeeDataRecord& rec_ref)
{
	CStringW cs_key(rec_ref.Code());
	TIndex::const_iterator it_ = m_index.find(cs_key);
	if (it_ == m_index.end())
	{
		return this->Append(rec_ref);
	}
	else
	{
		try
		{
			const INT nIndex = it_->second;
			if (-1 < nIndex && nIndex < this->Count())
				m_cache[nIndex] = rec_ref;
		}
		catch(::std::bad_alloc&){ return E_OUTOFMEMORY; }
	}
	return S_OK;
}

HRESULT   CEmployeeRecsCache::Remove(const CStringW& cs_code)
{
	TIndex::iterator it_ = m_index.find(cs_code);
	if (it_ == m_index.end())
		return TYPE_E_ELEMENTNOTFOUND;
	const INT nIndex = it_->second;
	try
	{
		m_cache.erase(m_cache.begin() + nIndex);
		details::EmployeeRecsCache_Reindex(m_cache, m_index);
	}
	catch(...){ return E_OUTOFMEMORY; }
	return S_OK;
}
