/*
	Created by Tech_dog (ebontrop@gmail.com) on 25-Mar-2014 at 6:08:13pm, GMT+4, Saint-Petersburg, Tuesday;
	This is Platinum Client Work Area Data Provider class implementation file.
	-----------------------------------------------------------------------------
	Adopted to version v15 on 16-Feb-2021 at 3:43:30.476 am, UTC+7, Novosibirsk, Tuesday;
*/
#include "StdAfx.h"
#include "DataProvider_4_WA.h"

using namespace Platinum;
using namespace Platinum::client;
using namespace Platinum::client::data;

using namespace shared::lite;
using namespace shared::lite::persistent;
using namespace shared::lite::data;

/////////////////////////////////////////////////////////////////////////////

CWorkAreaDataProvider::CWorkAreaDataProvider(const CCommonSettings& settings_ref):m_settings(settings_ref), m_bInitialized(false)
{
}

CWorkAreaDataProvider::~CWorkAreaDataProvider(void)
{
}

/////////////////////////////////////////////////////////////////////////////

HRESULT    CWorkAreaDataProvider::Initialize(void)
{
	if (this->IsInitialized())
		return S_OK;
	CStringW cs_profile = m_settings.Storage().WorkAreaProfilePath();
	CPrivateProfile profile;
	HRESULT hr_  =  profile.Create(cs_profile.GetString());
	if (S_OK == hr_)
	{
		CStringW cs_section = m_settings.Storage().WorkAreaProfileSection();
		const CPrivateProfileSection& sec_ref = profile.SectionOf(cs_section);
		CStringW cs_wa;
		const INT nCount = sec_ref.ItemCount();
		for ( INT i_ = 0; i_ < nCount; i_++)
		{
			const CPrivateProfileItem& item_ref = sec_ref.ItemOf(i_);
			if (!item_ref.IsValid())
				continue;
			cs_wa = item_ref.Value();
			CDataRecord record;
			hr_ = record.Insert(_T("WorkArea"), _variant_t(cs_wa.GetString()));
			if (S_OK != hr_)
				break;
			hr_ = m_ds.Insert(cs_wa.GetString(), record);
			if (S_OK != hr_)
				break;
		}
	}
	m_bInitialized = (S_OK == hr_);
	return  hr_;
}

bool       CWorkAreaDataProvider::IsInitialized(void)const
{
	return m_bInitialized;
}

const
CDataSet&  CWorkAreaDataProvider::DataSet(void)const
{
	return m_ds;
}