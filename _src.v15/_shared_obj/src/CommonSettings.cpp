/*
	Created by Tech_dog (ebontrop@gmail.com) on 22-Mar-2014 at 8:23:26pm, GMT+4, Moscow Region,
	Rail Road Train #43, Coatch #6, Place #1, Saturday;
	This is Platinum Client Application Common Settings class implementation file.
	-----------------------------------------------------------------------------
	Adopted to version v15 on 16-Feb-2021 at 3:08:05.067 am, UTC+7, Novosibirsk, Tuesday;
*/
#include "StdAfx.h"
#include "CommonSettings.h"

using namespace Platinum;
using namespace Platinum::client;
using namespace Platinum::client::common;

using namespace shared::lite::common;
using namespace shared::lite::persistent;

extern shared::lite::common::CApplication& Global_GetAppObjectRef(void);

/////////////////////////////////////////////////////////////////////////////

namespace Platinum { namespace client { namespace common { namespace details
{
	static LPCWSTR  SettingAttribute_DataStorageFolderDef    = _T(".\\storage\\");
	static LPCWSTR  SettingAttribute_DataStorageProperty     = _T("StorageFolder");
	static LPCWSTR  SettingAttribute_WorkAreaProfileName     = _T("PlatinumClient.ini");
	static LPCWSTR  SettingAttribute_WorkAreaProfileSection  = _T("WorkAreas");
	static LPCWSTR  SettingAttribute_ApplicationProfileName  = _T("PlatinumClient.ini");
	static LPCWSTR  SettingAttribute_DataStgProfileSection   = _T("Storage");
	static LPCWSTR  SettingAttribute_DataStgProfileItem      = _T("CsvFilePath");
	static LPCWSTR  SettingAttribute_EntriesBackupFolderItem = _T("EntriesBackupFolder");

	HRESULT   CommonSettings_GetProfileFolder(ATL::CStringW& cs_folder)
	{
		const CApplication& the_app = Global_GetAppObjectRef();
		HRESULT hr_ = the_app.GetPathFromAppFolder(_T(".\\cfg\\"), cs_folder);
		return  hr_;
	}
}}}}

/////////////////////////////////////////////////////////////////////////////

CCommonSettings::CDataStorage::CDataStorage(CPrivateProfile& prf_ref) : m_prf_ref(prf_ref)
{
}

CCommonSettings::CDataStorage::~CDataStorage(void)
{
}

/////////////////////////////////////////////////////////////////////////////

CStringW    CCommonSettings::CDataStorage::BackupFolder(void)const
{
	static CStringW cs_folder;
	static volatile bool bInitialized = false;
	if (bInitialized)
		return cs_folder;

	bInitialized = true;

	cs_folder = m_prf_ref.GetStringValue(
						details::SettingAttribute_DataStgProfileSection,
						details::SettingAttribute_EntriesBackupFolderItem,
						NULL
					);
	return cs_folder;
}

CStringW    CCommonSettings::CDataStorage::CustomFolder (void)const
{
	static CStringW cs_csv_path;
	static volatile bool bInitialized = false;
	if (bInitialized)
		return cs_csv_path;

	bInitialized = true;

	cs_csv_path = m_prf_ref.GetStringValue(
						details::SettingAttribute_DataStgProfileSection,
						details::SettingAttribute_DataStgProfileItem,
						this->DefaultFolder()
					);
	return cs_csv_path;
}

CStringW    CCommonSettings::CDataStorage::DefaultFolder(void)const
{
	static CStringW cs_folder;
	static volatile bool bInitialized = false;
	if (bInitialized != true)
	{
		bInitialized  = true;
		const CApplication& the_app = Global_GetAppObjectRef();
		the_app.GetPathFromAppFolder(details::SettingAttribute_DataStorageFolderDef, cs_folder);
	}
	return cs_folder;
}

bool          CCommonSettings::CDataStorage::IsCustomFolderUsed(void)const
{
	static bool bIsCustomFolderUsed = false;
	static volatile bool bInitialized = false;
	if (bInitialized != true)
	{
		bInitialized  = true;
		CStringW cs_folder = this->CustomFolder();
		bIsCustomFolderUsed  = !cs_folder.IsEmpty();
	}
	return bIsCustomFolderUsed;
}

CStringW    CCommonSettings::CDataStorage::WorkAreaProfilePath(void)const
{
	static CStringW cs_profile;
	static volatile bool bInitialized = false;
	if (bInitialized != true)
	{
		bInitialized  = true;
		HRESULT hr_ = details::CommonSettings_GetProfileFolder(cs_profile);
		if (S_OK == hr_)
			cs_profile += details::SettingAttribute_WorkAreaProfileName;
	}
	return cs_profile;
}

CStringW    CCommonSettings::CDataStorage::WorkAreaProfileSection(void)const
{
	static CStringW cs_section(details::SettingAttribute_WorkAreaProfileSection);
	return cs_section;
}

/////////////////////////////////////////////////////////////////////////////

CCommonSettings::CCommonSettings(void) : m_stg(m_profile)
{
	CStringW cs_profile;

	HRESULT hr_ = details::CommonSettings_GetProfileFolder(cs_profile);
	if (S_OK == hr_)
	{
		cs_profile += details::SettingAttribute_ApplicationProfileName;
		hr_  =  m_profile.Create(cs_profile);
	}
}

CCommonSettings::~CCommonSettings(void)
{
}

/////////////////////////////////////////////////////////////////////////////
const
TStorageSettings& CCommonSettings::Storage(void)const
{
	return m_stg;
}