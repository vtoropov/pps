/*
	Created by Tech_dog (ebontrop@gmail.com) on 3-Apr-2014 at 8:42:13am, GMT+4, Saint-Petersburg, Thursday;
	This is Platinum Client Employee Finger Vein Data class implementation file.
	-----------------------------------------------------------------------------
	Adopted to version v15 on 16-Feb-2021 at 3:51:24.549 am, UTC+7, Novosibirsk, Tuesday;
*/
#include "StdAfx.h"
#include "EmployeeFvData.h"

using namespace Platinum::client::data;
using namespace shared::lite::common;

extern CApplication&   Global_GetAppObjectRef(void);
/////////////////////////////////////////////////////////////////////////////

CEmployeeFvImage::CEmployeeFvImage(void)
{
}

/////////////////////////////////////////////////////////////////////////////

VOID          CEmployeeFvImage::Clear(void)
{
	m_timestamp.Value(0);
	m_fv_data.Clear();
}
const
_variant_t&   CEmployeeFvImage::Data(void)const
{
	return m_fv_data;
}

HRESULT       CEmployeeFvImage::Data(const _variant_t& _fv_data)
{
	const bool bEmptyIsAllowed = false;
	HRESULT hr_ = ValidateVeinData(_fv_data, bEmptyIsAllowed);
	if (!FAILED(hr_))
		m_fv_data = _fv_data;
	return  hr_;
}

bool          CEmployeeFvImage::IsValid(void)const
{
	const bool bEmptyIsAllowed = false;
	HRESULT hr_ = ValidateVeinData(m_fv_data, bEmptyIsAllowed);
	return !FAILED(hr_);
}
const
CTimestamp&   CEmployeeFvImage::Timestamp(void)const
{
	return m_timestamp;
}

CTimestamp&   CEmployeeFvImage::Timestamp(void)
{
	return m_timestamp;
}

/////////////////////////////////////////////////////////////////////////////

bool CEmployeeFvImage::operator!=(const CEmployeeFvImage& _obj) const
{
	return (0 != shared::lite::data::CompareSafeArrays(this->Data(), _obj.Data()));
}

bool CEmployeeFvImage::operator==(const CEmployeeFvImage& _obj) const
{
	return !(*this != _obj);
}

/////////////////////////////////////////////////////////////////////////////

namespace Platinum { namespace client { namespace data { namespace details
{
	const CEmployeeFvImage& EmployeeFvData_InvalidImageRef(void)
	{
		static CEmployeeFvImage var_;
		return var_;
	}
}}}}

/////////////////////////////////////////////////////////////////////////////

CEmployeeFvData::CEmployeeFvData(void):m_bChanged(false)
{
}

CEmployeeFvData::~CEmployeeFvData(void)
{
	this->Clear();
}

/////////////////////////////////////////////////////////////////////////////

HRESULT   CEmployeeFvData::Add(const _variant_t& _fv_data, LPCWSTR pszTimestamp)
{
	const bool bEmptyIsAllowed = false;
	HRESULT hr_ = ValidateVeinData(_fv_data, bEmptyIsAllowed);
	if (FAILED(hr_))
		return hr_;

	CEmployeeFvImage image_;
	if (pszTimestamp)
		image_.Timestamp().ValueAsText(pszTimestamp, false);
	else
		image_.Timestamp().SetCurrentTime();

	image_.Data(_fv_data);

	try
	{
		m_fv_images.push_back(image_);
	}
	catch(::std::bad_alloc&){ return E_OUTOFMEMORY; }
	return S_OK;
}

bool      CEmployeeFvData::Changed(void)const
{
	return m_bChanged;
}

void      CEmployeeFvData::Changed(const bool bSet)
{
	m_bChanged = bSet;
}

HRESULT   CEmployeeFvData::Clear(void)
{
	if (this->IsEmpty())
		return S_FALSE;
	try
	{
		m_fv_images.clear();
	}
	catch(...){ return E_OUTOFMEMORY; }
	return S_OK;
}

INT       CEmployeeFvData::Count(void)const
{
	return (INT)m_fv_images.size();
}

const CEmployeeFvImage&
          CEmployeeFvData::Image(const INT nIndex)const
{
	if (0 > nIndex || nIndex > this->Count() - 1)
		return details::EmployeeFvData_InvalidImageRef();
	else
		return m_fv_images[nIndex];
}

bool      CEmployeeFvData::IsEmpty(void)const
{
	return (m_fv_images.empty());
}
const
TVeinImages&   CEmployeeFvData::RawVeinData(void)const
{
	return m_fv_images;
}

TVeinImages&   CEmployeeFvData::RawVeinData(void)
{
	return m_fv_images;
}

HRESULT   CEmployeeFvData::Remove(const INT nIndex)
{
	if (0 > nIndex || nIndex > this->Count() - 1)
		return DISP_E_BADINDEX;
	try
	{
		m_fv_images.erase(m_fv_images.begin() + nIndex);
	}
	catch(...){ return E_OUTOFMEMORY; }
	return S_OK;
}

HRESULT   CEmployeeFvData::Update(const INT nIndex, const _variant_t& fv_data)
{
	if (0 > nIndex || nIndex > this->Count() - 1)
		return DISP_E_BADINDEX;
	HRESULT hr_ = m_fv_images[nIndex].Data(fv_data);
	return  hr_;
}

/////////////////////////////////////////////////////////////////////////////

bool CEmployeeFvData::operator!=(const CEmployeeFvData& _obj) const
{
	if (this->Count() != _obj.Count())
		return true;

	TVeinImages::const_iterator it_l = this->m_fv_images.begin();
	TVeinImages::const_iterator it_r = _obj.m_fv_images.begin();

	for (; it_l != this->m_fv_images.end() && it_r != _obj.m_fv_images.end();
				++it_l, ++it_r)
	{
		if (*it_l != *it_r)
			return true;
	}
	return false;
}

bool CEmployeeFvData::operator==(const CEmployeeFvData& _obj) const
{
	return !(*this != _obj);
}