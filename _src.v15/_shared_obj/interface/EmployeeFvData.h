#ifndef __PLATINUMCLIENTEMPLOYEEFINGERVEINDATA_9C49C534_F747_4973_BC21_F0E4FA48B7BB_INCLUDED
#define __PLATINUMCLIENTEMPLOYEEFINGERVEINDATA_9C49C534_F747_4973_BC21_F0E4FA48B7BB_INCLUDED
/*
	Created by Tech_dog (ebontrop@gmail.com) on 3-Apr-2014 at 8:37:27am, GMT+4, Saint-Petersburg, Thursday;
	This is Platinum Client Employee Finger Vein Data class declaration file.
	-----------------------------------------------------------------------------
	Adopted to version v15 on 16-Feb-2021 at 3:49:01.616 am, UTC+7, Novosibirsk, Tuesday;
*/
#include "Shared_DataFormat.h"
#include "Shared_RawData.h"
#include "Shared_GenericAppObject.h"
#include "DataProvider_CommonDefs.h"

namespace Platinum { namespace client { namespace data
{
	using shared::lite::data::CTimestamp;

	class CEmployeeFvImage
	{
	private:
		CTimestamp             m_timestamp;
		_variant_t             m_fv_data;
	public:
		CEmployeeFvImage(void);
	public:
		VOID                   Clear(void);
		const _variant_t&      Data (void)const;
		HRESULT                Data (const _variant_t&);
		bool                   IsValid(void)const;
		const
		CTimestamp&            Timestamp(void)const;
		CTimestamp&            Timestamp(void);
	public:
		bool operator!=(const CEmployeeFvImage&) const;
		bool operator==(const CEmployeeFvImage&) const;
	};

	typedef ::std::vector<CEmployeeFvImage> TVeinImages;

	class CEmployeeFvData
	{
	private:
		bool                   m_bChanged;
		TVeinImages            m_fv_images;
	public:
		 CEmployeeFvData(void);
		~CEmployeeFvData(void);
	public:
		HRESULT                Add(const _variant_t& fv_data, LPCWSTR pszTimestamp);// adds new finger vein (fv) image to the collection
		bool                   Changed(void)const;                                  // gets dirty state of the object
		void                   Changed(const bool);                                 // marks the object as changed/unchanged one
		HRESULT                Clear(void);                                         // clears the fv image collection, removes all fv images, if any
		INT                    Count(void)const;                                    // counts the fv images within the collection
		const CEmployeeFvImage&Image(const INT nIndex)const;                        // gets the fv image by the index provided, if the index is bad, the invalid fv image reference is returned
		bool                   IsEmpty(void)const;                                  // checks the emptiness of the collection
		const TVeinImages&     RawVeinData(void)const;                              // gets the raw data container reference (read-only)
		TVeinImages&           RawVeinData(void);                                   // gets the raw data container reference (read-write)
		HRESULT                Remove(const INT nIndex);                            // removes the fv image from the collection by the index provided
		HRESULT                Update(const INT nIndex, const _variant_t& fv_data); // updates the fv image in the collection by the index provided
	public:
		bool operator!=(const CEmployeeFvData&) const;
		bool operator==(const CEmployeeFvData&) const;
	};
}}}

#endif/*__PLATINUMCLIENTEMPLOYEEFINGERVEINDATA_9C49C534_F747_4973_BC21_F0E4FA48B7BB_INCLUDED*/