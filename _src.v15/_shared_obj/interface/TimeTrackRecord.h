#ifndef __PLATINUMCLIENTTIMETRACKDATARECORD_H_8AF0C8EA_1BFB_4d3a_8318_304F3F69DBD6_INCLUDED
#define __PLATINUMCLIENTTIMETRACKDATARECORD_H_8AF0C8EA_1BFB_4d3a_8318_304F3F69DBD6_INCLUDED
/*
	Created by Tech_dog (ebontrop@gmail.com) on 27-Mar-2014 at 2:30:28am, GMT+4, Saint-Petersburg, Thursday;
	This is Platinum Client Time Tracking Data Record class declaration file.
	-----------------------------------------------------------------------------
	Adopted to version v15 on 16-Feb-2021 at 4:11:59.117 am, UTC+7, Novosibirsk, Tuesday;
*/
#include "Shared_DataFormat.h"
#include "EmployeeDataRecord.h"

namespace Platinum { namespace client { namespace data
{
	using shared::lite::data::CTimestamp;

	class CTimeTrackRecord
	{
	private:
		bool        m_bValid;
		CStringW    m_code;
		CStringW    m_name;
		CStringW    m_date;
		CStringW    m_time;
		CStringW    m_area;
		CTimestamp  m_timestamp;   // finger vein identifier
	public:
		 CTimeTrackRecord(const bool bValid = true);
		~CTimeTrackRecord(void);
	public:
		LPCWSTR     Code(void)    const;
		HRESULT     Code(LPCWSTR)      ;
		LPCWSTR     Date(void)    const;
		HRESULT     Date(LPCWSTR)      ;
		const
		CTimestamp& FvTimestamp(void)const;
		CTimestamp& FvTimestamp(void)  ;
		bool        IsValid(void) const;
		LPCWSTR     Name(void)    const;
		HRESULT     Name(LPCWSTR)      ;
		LPCWSTR     Time(void)    const;
		HRESULT     Time(LPCWSTR)      ;
		LPCWSTR     WorkArea(void)const;
		HRESULT     WorkArea(LPCWSTR)  ;
	public:
		bool operator!=(const CTimeTrackRecord&) const;
		CTimeTrackRecord& operator= (const CEmployeeDataRecord&);
		CTimeTrackRecord& operator= (const CEmployeeDataRecordEx&);
	};

	class CTimeTrackRecord_ValidateRule
	{
	private:
		const   CTimeTrackRecord&   m_rec_ref;
		mutable CStringW m_buffer;
	public:
		 CTimeTrackRecord_ValidateRule(const CTimeTrackRecord&);
		~CTimeTrackRecord_ValidateRule(void);
	public:
		LPCWSTR    Details (void)const;
		HRESULT    Validate(void)const;
	};
}}}

#endif/*__PLATINUMCLIENTTIMETRACKDATARECORD_H_8AF0C8EA_1BFB_4d3a_8318_304F3F69DBD6_INCLUDED*/