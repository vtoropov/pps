#ifndef __PLATINUMCLIENTTIMETRACKDATAPROVIDER_H_1421F5FE_E94A_48a6_BBCA_63D959570238_INCLUDED
#define __PLATINUMCLIENTTIMETRACKDATAPROVIDER_H_1421F5FE_E94A_48a6_BBCA_63D959570238_INCLUDED
/*
	Created by Tech_dog (ebontrop@gmail.com) on 27-Mar-2014 at 2:44:42am, GMT+4, Saint-Petersburg, Thursday;
	This is Platinum Client Track Time Data Provider class declaration file.
	-----------------------------------------------------------------------------
	Adopted to version v15 on 16-Feb-2021 at 4:44:34.720 am, UTC+7, Novosibirsk, Tuesday;
*/
#include "Shared_PersistentStorage.h"
#include "Shared_RawData.h"
#include "Shared_GenericAppObject.h"

#include "EmployeeDataRecord.h"
#include "EmployeeCache.h"
#include "DataProvider_CommonDefs.h"
#include "TimeTrackRecord.h"

namespace Platinum { namespace client { namespace data
{
	using Platinum::client::data::CEmployeeDataRecord;
	using Platinum::client::data::CEmployeeDataRecordEx;

	class CTimeTrackDataSpec : public CRecordSpecBase
	{
		typedef CRecordSpecBase TBase;
	public:
		enum _enum{
			eCode     = 0,
			eDate     = 1,
			eTime     = 2,
			eWorkArea = 3,
			eFvId     = 4,  // an identifier of user's finger vein enrollment template that was used for recognition
		};
	public:
		CTimeTrackDataSpec(void);
	public:
		static const INT nFieldCount = CTimeTrackDataSpec::eFvId + 1;
	};

	class CTimeTrackDataProvider
	{
	public:
		typedef ::std::vector<CTimeTrackRecord> TRecords;
	private:
		bool              m_bDirty;
		TRecords          m_records;
	public:
		 CTimeTrackDataProvider(void);
		~CTimeTrackDataProvider(void);
	public:
		HRESULT           Initialize(const bool bSuppressAccessDeniedMessage);
		HRESULT           Initialize(const bool bSuppressAccessDeniedMessage, const CEmployeeRecsCache&);
		const
		CTimeTrackRecord& Record(const INT nIndex)const;
		CTimeTrackRecord& Record(const INT nIndex);
		INT               RecordCount(void)const;
		HRESULT           Refresh(void);
		HRESULT           Refresh(const CEmployeeRecsCache&);
		HRESULT           Save(void);
		HRESULT           SaveEntry(const CTimeTrackRecord&);   // appends a new entry record to the data file
	public:
		HRESULT           Append(const CTimeTrackRecord&);
	};
}}}

#endif/*__PLATINUMCLIENTTIMETRACKDATAPROVIDER_H_1421F5FE_E94A_48a6_BBCA_63D959570238_INCLUDED*/