#ifndef __PLATINUMCLIENTFINGERVEINIMAGEDATAPROVIDER_15BB822F_BCF2_4777_8BB5_82452F1A0889_INCLUDED
#define __PLATINUMCLIENTFINGERVEINIMAGEDATAPROVIDER_15BB822F_BCF2_4777_8BB5_82452F1A0889_INCLUDED
/*
	Created by Tech_dog (ebontrop@gmail.com) 3-Apr-2014 at 9:03:47am, GMT+4, Saint-Petersburg, Thursday;
	This is Platinum Client Finger Vein Image Data Provider class declaration file.
	-----------------------------------------------------------------------------
	Adopted to version v15 on 16-Feb-2021 at 4:31:26.437 am, UTC+7, Novosibirsk, Tuesday;
*/
#include "Shared_PersistentStorage.h"
#include "Shared_RawData.h"
#include "Shared_GenericAppObject.h"
#include "EmployeeFvData.h"
#include "SharedObjects.h"
#include "DataProvider_CommonDefs.h"

namespace Platinum { namespace client { namespace data
{
	class CEnrollFvDataSpec : public CRecordSpecBase
	{
		typedef CRecordSpecBase TBase;
	public:
		enum _enum{
			eCode      = 0,  // employee code
			eFvImage   = 1,  // finger vein data
			eFvUid     = 2,  // finger vein unique identifier (actually a timestamp, i.e. INT64)
		};
	public:
		CEnrollFvDataSpec(void);
	public:
		static const INT nFieldCount = CEnrollFvDataSpec::eFvUid + 1;
	};

	using Platinum::client::common::CSharedObjects;

	class CEnrollFvProvider
	{
	private:
		CSharedObjects&  m_objects;
	public:
		 CEnrollFvProvider(CSharedObjects&);
		~CEnrollFvProvider(void);
	public:
		HRESULT          Load(const bool bSuppressAccessDeniedMessage);
		HRESULT          Save(const bool bSuppressAccessDeniedMessage);
	};
}}}

#endif/*__PLATINUMCLIENTFINGERVEINIMAGEDATAPROVIDER_15BB822F_BCF2_4777_8BB5_82452F1A0889_INCLUDED*/