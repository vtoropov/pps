#ifndef __PLATINUMCLIENTENROLLMENTDATAPROVIDER_RA_H_9303109E_87B6_4f39_B675_FD1C2FAC9F80_INCLUDED
#define __PLATINUMCLIENTENROLLMENTDATAPROVIDER_RA_H_9303109E_87B6_4f39_B675_FD1C2FAC9F80_INCLUDED
/*
	Created by Tech_dog (ebontrop@gmail.com) on 9-Apr-2014 at 6:37:19pm, GMT+4, Saint-Petersburg, Wednesday;
	This is Platinum Client Enrollment Data Provider [Read Only Access] class declaration file.
	-----------------------------------------------------------------------------
	Adopted to version v15 on 16-Feb-2021 at 4:39:39.578 am, UTC+7, Novosibirsk, Tuesday;
*/
#include "Shared_PersistentStorage.h"
#include "Shared_GenericAppObject.h"
#include "Shared_RawData.h"

#include "EmployeeDataRecord.h"
#include "SharedObjects.h"
#include "DataProvider_CommonDefs.h"
#include "DataProvider_4_FvData.h"

namespace Platinum { namespace client { namespace data
{
	using Platinum::client::common::CSharedObjects;

	class CEnrollRecordSpec : public CRecordSpecBase
	{
		typedef CRecordSpecBase TBase;
	public:
		enum _enum{
			eCode      = 0,
			eEmployee  = 1,
			eWorkArea  = 2
		};
	public:
		CEnrollRecordSpec(void);
	public:
		static const INT nFieldCount = CEnrollRecordSpec::eWorkArea + 1;
	};

	class CEnrollDataProvider_RA
	{
	protected:
		CSharedObjects&  m_objects;
	public:
		 CEnrollDataProvider_RA(CSharedObjects&);
		~CEnrollDataProvider_RA(void);
	public:
		HRESULT          Initialize(void);
		INT              RecordCount(void)const;
	public:
		const CEmployeeDataRecord& Record(const CStringW& _emp_code) const;
	};
}}}

#endif/*__PLATINUMCLIENTENROLLMENTDATAPROVIDER_RA_H_9303109E_87B6_4f39_B675_FD1C2FAC9F80_INCLUDED*/