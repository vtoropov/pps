#ifndef __PLATINUMCLIENTDATAPROVIDERCOMMONDEFS_H_B967854D_ACAC_4554_9CF8_5DC74B9D29C7_INCLUDED
#define __PLATINUMCLIENTDATAPROVIDERCOMMONDEFS_H_B967854D_ACAC_4554_9CF8_5DC74B9D29C7_INCLUDED
/*
	Created by Tech_dog (ebontrop@gmail.com) on 3-Apr-2014 at 10:05:42am, GMT+4, Saint-Petersburg, Thursday;
	This is Platinum Client Data Provider Common Definitions declaration file.
	-----------------------------------------------------------------------------
	Adopted to version v15 on 16-Feb-2021 at 3:28:37.256 am, UTC+7, Novosibirsk, Tuesday;
*/
#include "atlapp.h"
#include "atluser.h"
#include "Shared_PersistentStorage.h"
#include "Shared_GenericAppObject.h"
#include "Shared_SystemError.h"

namespace Platinum { namespace client { namespace data {

	using namespace shared::lite::common;

	VOID       DisplayOpenDataFileError   (const CStringW& _file, const HRESULT _error);
	CStringW   GetAccessDeniedErrorMessage(LPCWSTR pFolder, const HRESULT hError);
	HRESULT    GetBackupFolder  (CStringW& cs_folder);
	HRESULT    GetStorageFolder (CStringW& cs_storage, const bool bSuppressErrMessage = false);
	HRESULT    IsDataFolderExist(LPCWSTR pFolderPath , const bool bWithCreateOption = true);
	HRESULT    ValidateVeinData (const _variant_t&   , const bool bEmptyIsAllowed = true);

	using shared::lite::persistent::CCsvFile;

	class CRecordSpecBase
	{
	protected:
		typedef ::std::vector<CStringW>    TFields;
		typedef ::std::pair<DWORD, DWORD>  TAcceptRange;
	protected:
		TFields           m_fields;
		TAcceptRange      m_range ;

	protected:
		CRecordSpecBase(void);

	public:
		CCsvFile::THeader CreateHeader(void)const;
		INT               FieldCount  (void)const;
		CStringW          FieldNameOf (const INT nIndex)const;
		INT               GetHeaderLen(void)const;
		HRESULT           ValidateData(const CCsvFile&, const bool bShowError);
	};
}}}

#endif/*__PLATINUMCLIENTDATAPROVIDERCOMMONDEFS_H_B967854D_ACAC_4554_9CF8_5DC74B9D29C7_INCLUDED*/