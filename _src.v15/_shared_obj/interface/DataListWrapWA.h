#ifndef __PLATINUMCLIENTWORKAREADATALISTWRAPPER_H_2185C1B7_2722_4ee9_8091_656201C259B1_INCLUDED
#define __PLATINUMCLIENTWORKAREADATALISTWRAPPER_H_2185C1B7_2722_4ee9_8091_656201C259B1_INCLUDED
/*
	Created by Tech_dog (ebontrop@gmail.com) on 25-Mar-2014 at 9:26:45pm, GMT+4, Saint-Petersburg, Tuesday;
	This is Platinum Client Work Area Data List Wrapper class declaration file.
	-----------------------------------------------------------------------------
	Adopted to version v15 on 16-Feb-2021 at 3:13:35.805 am, UTC+7, Novosibirsk, Tuesday;
*/
#include "Shared_GenericDataProvider.h"
#include "CommonSettings.h"
#include "DataListWrapBase.h"

namespace Platinum { namespace client { namespace data { namespace wrappers
{
	using Platinum::client::common::CCommonSettings;

	class CWorkAreaDataListWrap : public CDataListWrapBase { typedef CDataListWrapBase TBaseWrap;
	private:
		const CCommonSettings& m_settings;
	public:
		 CWorkAreaDataListWrap(const CCommonSettings&);
		~CWorkAreaDataListWrap(void);
	public:
		virtual HRESULT  SeedData(void) override sealed;
	};

	class CWorkAreaListBoxWrap
	{
		typedef CDataListWrapBase TBaseWrap;
	private:
		const CCommonSettings& m_settings;
		const CWindow&         m_list;
	public:
		 CWorkAreaListBoxWrap(const CWindow& lst_box, const CCommonSettings&);
		~CWorkAreaListBoxWrap(void);
	public:
		virtual HRESULT  SeedData(void);
	};
}}}}

#endif/*__PLATINUMCLIENTWORKAREADATALISTWRAPPER_H_2185C1B7_2722_4ee9_8091_656201C259B1_INCLUDED*/