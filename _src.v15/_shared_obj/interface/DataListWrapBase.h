#ifndef __PLATINUMCLIENTDATALISTCONTROLWRAPPERBASE_H_AA06F7E5_822C_4b1a_A58D_A1D5E5DEB72D_INCLUDED
#define __PLATINUMCLIENTDATALISTCONTROLWRAPPERBASE_H_AA06F7E5_822C_4b1a_A58D_A1D5E5DEB72D_INCLUDED
/*
	Created by Tech_dog (ebontrop@gmail.com) on 25-Mar-2014 at 1:15:43pm, GMT+4, Saint-Petersburg, Tuesday;
	This is Platinum Client Data List Control Wrapper Base class declaration file.
	-----------------------------------------------------------------------------
	Adopted to version v15 on 16-Feb-2021 at 3:13:35.805 am, UTC+7, Novosibirsk, Tuesday;
*/
#include "atlapp.h"
#include "atlctrls.h"
namespace Platinum { namespace client { namespace data { namespace wrappers {

	using ::WTL::CComboBox;

	class CDataListWrapBase
	{
	protected:
		CComboBox   m_list;
		UINT        m_ctrlId;
	public:
		 CDataListWrapBase(void);
		 CDataListWrapBase(const HWND ctrl);
		 CDataListWrapBase(::ATL::CWindow& parent_ref, const UINT ctrlId);
		~CDataListWrapBase(void);
	public:
		virtual HRESULT  SeedData(void);
	public:
		HRESULT     AppendItem (LPCWSTR pszItem, const LONG lData = 0);
		HRESULT     AttachTo   (::ATL::CWindow& parent_ref, const UINT ctrlId);
		HRESULT     Detach(void);
		HWND        GetCtrlHandle(void) const;
		bool        IsValid(void)const;
		HRESULT     SelectItem  (LPCWSTR);
		HRESULT     SelectItemOf(const LONG nItemIdentifier);
		HRESULT     SetFont  (const HFONT);
		HRESULT     SetHeight(const INT nHeight, const bool bDialogUnits = true);
	};
}}}}

#endif/*__PLATINUMCLIENTDATALISTCONTROLWRAPPERBASE_H_AA06F7E5_822C_4b1a_A58D_A1D5E5DEB72D_INCLUDED*/