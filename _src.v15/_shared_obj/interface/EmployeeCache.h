#ifndef _PLATINUMCLIENTEMPLOYEERECSCACHE_H_516EE1CF_84A0_48ff_9382_8CF92A23537A_INCLUDED
#define _PLATINUMCLIENTEMPLOYEERECSCACHE_H_516EE1CF_84A0_48ff_9382_8CF92A23537A_INCLUDED
/*
	Created by Tech_dog (ebontrop@gmail.com) on 24-May-2015 at 1:00:00pm, GMT+8, Phuket, Sunday;
	This is Platinum Client Library Employee Record Data Cache class declaration file.
	-----------------------------------------------------------------------------
	Adopted to version v15 on 16-Feb-2021 at 4:05:19.026 am, UTC+7, Novosibirsk, Tuesday;
*/
#include "EmployeeDataRecord.h"

namespace Platinum { namespace client { namespace data
{
	class CEmployeeRecsCache
	{
		typedef ::std::vector<CEmployeeDataRecord>  TRecords;
		typedef ::std::map<CStringW, INT>  TIndex;  // the key is employee code
	private:
		TRecords    m_cache;
		TIndex      m_index;
	public:
		 CEmployeeRecsCache(void);
		~CEmployeeRecsCache(void);
	public:
		HRESULT     Append(const CEmployeeDataRecord&);
		HRESULT     Clear (void);
		INT         Count (void)const;
		const
		CEmployeeDataRecord&   Item  (const INT nIndex)const;
		CEmployeeDataRecord&   Item  (const INT nIndex);
		const
		CEmployeeDataRecord&   ItemOf(const CStringW& cs_code)const;
		CEmployeeDataRecord&   ItemOf(const CStringW& cs_code);
		HRESULT     ItemOf(const CEmployeeDataRecord&);
		HRESULT     Remove(const CStringW& cs_code);
	private:
		CEmployeeRecsCache(const CEmployeeRecsCache&);
		CEmployeeRecsCache& operator= (const CEmployeeRecsCache&);
	};
}}}

#endif/*_PLATINUMCLIENTEMPLOYEERECSCACHE_H_516EE1CF_84A0_48ff_9382_8CF92A23537A_INCLUDED*/