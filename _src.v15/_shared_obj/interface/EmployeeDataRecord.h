#ifndef __PLATINUMCLIENTEMPLOYEEDATARECORD_H_62672490_9B7A_41e1_9673_E6DCAFFEE12E_INCLUDED
#define __PLATINUMCLIENTEMPLOYEEDATARECORD_H_62672490_9B7A_41e1_9673_E6DCAFFEE12E_INCLUDED
/*
	Created by Tech_dog (ebontrop@gmail.com) on 24-Mar-2014 at 5:54:16pm, GMT+4, Saint-Petersburg, Monday;
	This is Platinum Client Employee Data Record class declaration file.
	-----------------------------------------------------------------------------
	Adopted to version v15 on 16-Feb-2021 at 3:57:06.997 am, UTC+7, Novosibirsk, Tuesday;
*/
#include "DataProvider_CommonDefs.h"
#include "EmployeeFvData.h"

namespace Platinum { namespace client { namespace data
{
	class CEmployeeDataRecord
	{
	private:
		bool            m_bValid;
		CStringW        m_code;
		CStringW        m_name;
		CStringW        m_area;
		CEmployeeFvData m_fv_data;
	public:
		 CEmployeeDataRecord(const bool bValid = true);
		~CEmployeeDataRecord(void);
	public:
		HRESULT           Clear(void);
		LPCWSTR           Code (void) const;
		HRESULT           Code (LPCWSTR);
		const
		CEmployeeFvData&  FvData(void) const;
		CEmployeeFvData&  FvData(void);
		bool              IsValid(void)const;
		LPCWSTR           Name(void) const;
		HRESULT           Name(LPCWSTR);
		LPCWSTR           WorkArea(void)const;
		HRESULT           WorkArea(LPCWSTR);
	public:
		bool operator!=(const CEmployeeDataRecord&) const;
	};

	class CEmployeeDataRecord_ValidateRule
	{
	private:
		const CEmployeeDataRecord&   m_rec_ref;
		mutable ::ATL::CStringW    m_buffer;
	public:
		 CEmployeeDataRecord_ValidateRule(const CEmployeeDataRecord&);
		~CEmployeeDataRecord_ValidateRule(void);
	public:
		LPCWSTR    Details (void)const;
		HRESULT    Validate(void)const;
		HRESULT    ValidateStrict(void)const;
		HRESULT    ValidateVein(void)const;
	};

	class CEmployeeDataRecordEx :  // this extended version is used to save clocking data
		public  CEmployeeDataRecord
	{
		typedef CEmployeeDataRecord TBase;
	private:
		INT     m_selected; // selected vein image index that is used for clocking record
	public:
		CEmployeeDataRecordEx(void);
	public:
		INT     SelectedFvIndex(void)const;
		VOID    SelectedFvIndex(const INT);
	public:
		CEmployeeDataRecordEx& operator= (const CEmployeeDataRecord&);
	};
}}}

#endif/*__PLATINUMCLIENTEMPLOYEEDATARECORD_H_62672490_9B7A_41e1_9673_E6DCAFFEE12E_INCLUDED*/