;----------------------------------------------------------------------------
; Created by Tech_dog (VToropov) on 30-Apr-2014 at 9:29:40pm, GMT+4, Saint-Petersburg, Wednesday;
; This is Platinum Manager Application Project Setup for both architectures (x86 and x64).
;----------------------------------------------------------------------------

#if VER < 0x05040200
  #error Update your Inno Setup version
#endif

#define app_version            "1.0.0"
#define simple_app_version     "1.0.0"
#define copyright              "Copyright � 2014, Platinum Payroll Systems"
#define installer_build_date   GetDateTimeString('mmm, d yyyy', '', '')
#define quick_launch           "{userappdata}\Microsoft\Internet Explorer\Quick Launch\Platinum Manager"
#define bindir_x64             "E:\Prog_Man\_B2B_\oDesk\PP_Systems\Project\__bin__\x64\release"
#define bindir_x86             "E:\Prog_Man\_B2B_\oDesk\PP_Systems\Project\__bin__\x86\release"
#define setdir                 "E:\Prog_Man\_B2B_\oDesk\PP_Systems\Project.Installers\the_manager"

[Setup]
AppId                          ={{928C8AC5-DA20-4f58-86E3-8A769DCBE366}
AppName                        =Platinum Manager
AppVersion                     ={#app_version}
AppVerName                     =Platinum Manager {#app_version}
AppPublisher                   =Platinum Payroll Systems
AppCopyright                   ={#copyright}
AppPublisherURL                =http://platinumpayroll.co.nz/
AppSupportURL                  =http://platinumpayroll.co.nz/
AppUpdatesURL                  =http://platinumpayroll.co.nz/
AppContact                     =http://platinumpayroll.co.nz/
VersionInfoCompany             =Platinum Payroll Systems
VersionInfoCopyright           ={#copyright}
VersionInfoDescription         =Platinum Clocking {#app_version} Setup
VersionInfoProductName         =Platinum Clocking
VersionInfoProductVersion      ={#app_version}
VersionInfoTextVersion         ={#app_version}
VersionInfoVersion             ={#app_version}
DefaultDirName                 ={pf}\Platinum\Clocking
DefaultGroupName               =Platinum
LicenseFile                    =..\Platinum_Project_License.txt
InfoBeforeFile                 =Platinum_Manager_readme_before.rtf
OutputDir                      =.
OutputBaseFilename             =Platinum_Manager-Setup_v{#app_version}_release
; "ArchitecturesInstallIn64BitMode=x64" requests that the install be
; done in "64-bit mode" on x64, meaning it should use the native
; 64-bit Program Files directory and the 64-bit view of the registry.
; On all other architectures it will install in "32-bit mode".
;ArchitecturesInstallIn64BitMode=x64
; Note: We don't set ProcessorsAllowed because we want this
; installation to run on all architectures (including Itanium,
; since it's capable of running 32-bit code too).
Compression                    =lzma2/max
SolidCompression               =yes
MinVersion                     =0,5.1
UninstallDisplayName           =Platinum Manager {#app_version}
UninstallDisplayIcon           ={app}\PlatinumManager.exe
AppReadmeFile                  ={app}\Platinum_Manager_readme_before.rtf
WizardImageFile                =..\common_img\Platinum_Manager_LeftPane.bmp
WizardSmallImageFile           =..\common_img\Platinum_Manager_Top[32x32].bmp
SetupIconFile                  =..\Platinum_Manager_Project.ico
AllowNoIcons                   =yes
ShowTasksTreeLines             =yes
AlwaysShowDirOnReadyPage       =yes
AlwaysShowGroupOnReadyPage     =yes
PrivilegesRequired             =admin
DisableDirPage                 =no
DisableProgramGroupPage        =yes
AppMutex                       =Global\Platinum_Client

[Languages]
Name: en; MessagesFile: compiler:Default.isl
#include "PLATINUM_Manager_messages.iss"

[Messages]
BeveledLabel                   =Platinum Manager {#app_version} built on {#installer_build_date}
SetupAppTitle                  =Setup - Platinum Manager
SetupWindowTitle               =Setup - Platinum Manager

[Tasks]
Name: desktopicon;               Description: {cm:CreateDesktopIcon};       GroupDescription: {cm:AdditionalIcons}
Name: quicklaunchicon;           Description: {cm:CreateQuickLaunchIcon};   GroupDescription: {cm:AdditionalIcons};                                    Flags: unchecked; OnlyBelowVersion: 0,6.01

Name: delete_logs;               Description: {cm:tsk_delete_logs};         GroupDescription: {cm:tsk_client_reset};       Check: Common__LogsExist(); Flags: checkedonce unchecked

[Files]
;----------------------------------------------------------------------------
; visual c++ runtime related (side-by-side); Microsoft Installers;
;----------------------------------------------------------------------------
Source: {#setdir}\VC_2008_Redistributable_9.0.30729.6161_x64.exe; Flags: dontcopy deleteafterinstall;
Source: {#setdir}\VC_2008_Redistributable_9.0.30729.6161_x86.exe; Flags: dontcopy deleteafterinstall;
;----------------------------------------------------------------------------
; E-Opinion Walk Device Upload application files;
;----------------------------------------------------------------------------
Source: {#bindir_x64}\PlatinumManager_x64.exe;        DestDir: {app};            DestName:    "PlatinumManager.exe";       Check: IsWin64()
Source: {#bindir_x86}\PlatinumManager_x86.exe;        DestDir: {app};            DestName:    "PlatinumManager.exe";       Check: IsWin32()
Source: ..\Platinum_Project_License.txt;              DestDir: {app};            DestName:    "license.txt";                     Flags: ignoreversion
Source: Platinum_Manager_readme_after.rtf;            DestDir: {app};            DestName:    "readme.rtf";                      Flags: ignoreversion

[Icons]
Name: {group}\Platinum Manager;                       Filename: {app}\PlatinumManager.exe;           WorkingDir: {app};                         IconFilename: {app}\PlatinumManager.exe;  IconIndex: 0; Comment: Platinum Manager {#app_version}
Name: {group}\Uninstall Platinum Manager;             Filename: {uninstallexe};                      WorkingDir: {app};                         IconFilename: {app}\PlatinumManager.exe;  IconIndex: 1; Comment: {cm:UninstallProgram,Platinum Manager}
Name: {group}\Help and Support\ReadMe;                Filename: {app}\readme.rtf;                    WorkingDir: {app};                                                                                 Comment: Platinum Manager ReadMe
Name: {userdesktop}\Platinum Manager;                 Filename: {app}\PlatinumManager.exe;           WorkingDir: {app}; Tasks: desktopicon;     IconFilename: {app}\PlatinumManager.exe;  IconIndex: 0; Comment: Platinum Manager {#app_version}
Name: {#quick_launch};                                Filename: {app}\PlatinumManager.exe;           WorkingDir: {app}; Tasks: quicklaunchicon; IconFilename: {app}\PlatinumManager.exe;  IconIndex: 0; Comment: Platinum Manager {#app_version}

[Run]
Filename: {app}\PlatinumManager.exe;         Description: {cm:LaunchProgram,Platinum Manager};       WorkingDir: {app}; Flags: nowait postinstall skipifsilent runascurrentuser
Filename: http://platinumpayroll.co.nz/;     Description: {cm:run_visit_website};                                       Flags: nowait postinstall skipifsilent shellexec unchecked

[InstallDelete]
; During installation, delete old files in install folder
Name: {app}\license.txt;                              Type: files
; While we are at it, delete any shortcut which is not selected
Name: {userdesktop}\Platinum Manager.lnk;             Type: files; Check: not IsTaskSelected('desktopicon')     and Common__IsUpgrade()
Name: {#quick_launch}.lnk;                            Type: files; Check: not IsTaskSelected('quicklaunchicon') and Common__IsUpgrade(); OnlyBelowVersion: 0,6.01

[Code]
#include "..\common\common_custom_code.iss"
#include "..\common\common_services.iss"
#include "..\common\common_vc_run-time.iss"

///////////////////////////////////////////
//  Inno Setup functions and procedures  //
///////////////////////////////////////////
const
  installer_mutex_name = 'platinum_manager_setup_mutex';

function IsWin32(): Boolean;
begin
  Result := not IsWin64();
end;

function IsWin7(): Boolean;
begin
  Result := (GetWindowsVersion > $06000000);
end;

function InitializeSetup(): Boolean;
var
  merge__msi : String;
  merge__err : Integer;
begin
  // Create a mutex for the installer.
  // If it's already running display a message and stop the installation
  if CheckForMutexes(installer_mutex_name) and not WizardSilent() then begin
      Log('Custom Code: Installer is already running');
      SuppressibleMsgBox(CustomMessage('msg_client_setup_is_running'), mbError, MB_OK, MB_OK);
      Result := False;
  end
  else begin
    Result := True;
    Log('Custom Code: Creating installer`s mutex');
    CreateMutex(installer_mutex_name);

    if VC__CheckVersion_V9() then begin
      Result := True;
    end else begin
      if Is64BitInstallMode() then begin
        merge__msi := 'VC_2008_Redistributable_9.0.30729.6161_x64.exe';
      end else begin
        merge__msi := 'VC_2008_Redistributable_9.0.30729.6161_x86.exe';
      end;
      ExtractTemporaryFile(merge__msi);
      ShellExec('runas', ExpandConstant('{tmp}\' + merge__msi), '', '',  SW_SHOWNORMAL, ewWaitUntilTerminated, merge__err);
      Result := True;
    end;
  end;
end;

function InitializeUninstall(): Boolean;
begin
  if CheckForMutexes(installer_mutex_name) then begin
    SuppressibleMsgBox(CustomMessage('msg_client_setup_is_running'), mbError, MB_OK, MB_OK);
    Result := False;
  end
  else begin
    CreateMutex(installer_mutex_name);
    Result := True;
  end;
end;

function ShouldSkipPage(PageID: Integer): Boolean;
begin
  if Common__IsUpgrade() then begin
    // Hide the license page
    if (PageID = wpLicense) or (PageID = wpInfoBefore) then begin
       Result := True;
    end
    else begin
       Result := False;
    end;
  end;
end;

procedure CurStepChanged(CurStep: TSetupStep);
  var     iResultCode: Integer;
begin

  if CurStep = ssInstall then begin
  end;

  if CurStep = ssPostInstall then begin

    if IsTaskSelected('delete_logs') then begin
       Log('Custom Code: User selected the "delete_logs" task, calling RemoveLogs()');
       Common__RemoveLogs;
    end;

  end;
end;

procedure CurUninstallStepChanged(CurUninstallStep: TUninstallStep);
begin
  if CurUninstallStep = usUninstall then begin
    if (Common__LogsExist()) then begin
        if SuppressibleMsgBox(CustomMessage('msg_client_delete_logs'), mbConfirmation, MB_YESNO or MB_DEFBUTTON2, IDNO) = IDYES then begin
           Common__RemoveLogs();
        end;
    end;

    RemoveDir(ExpandConstant('{app}'));
  end;
end;