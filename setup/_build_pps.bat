

SET SOLUTION=..\_src\PlatinumPayrollSystemsLtd.sln
SET VS_IDE_EXE=C:\VS09\Common7\IDE\devenv.com
::
:: Compiling a solution in release mode for x86 platform
::

%VS_IDE_EXE% %SOLUTION% /Rebuild "Release|Win32"

::
:: Compiling a solution in release mode for x64 platform
::

%VS_IDE_EXE% %SOLUTION% /Rebuild "Release|x64"

::
:: Compile a setup package
::

SET SETUP_COMPILER="C:\Program Files (x86)\Inno Setup 5\Compil32.exe"
SET SETUP_PROJECT=Platinum_Project_Main.iss

CALL %SETUP_COMPILER% /CC %SETUP_PROJECT%