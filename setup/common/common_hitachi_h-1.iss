(*
;----------------------------------------------------------------------------
; Created by Tech_dog (VToropov) on 1-May-2014 at 1:00:02am, GMT+4, Saint-Petersburg, Thursday;
; This is Hitachi BioApi driver installation checking related script for Platinum project;
;----------------------------------------------------------------------------
*)

[Code]

function H1__ShowMessage() : Boolean;
var
  service : String;
  message : String;
begin
  Result  := False;
  service := 'Hitachi Biometric Service Provider for Finger Vein Module';
  message := '';
  message := message + 'You need to install ';
  message := message + service;
  message := message + ' to run Platinum applications.';
  if (IDOK = SuppressibleMsgBox(message, mbConfirmation, MB_OKCANCEL, IDOK)) then begin
    Result  := False; // a user agreed to install Hitachi device driver; returning false means the starting Hitachi's installer
  end else begin
    Result  := True;
  end;
  if (Result) then    // indicates that a user has pressed the cancel button
  begin
    message := '';
    message := message + 'You have chosen to not install ';
    message := message + service;
    message := message + ' . Platinum applications may not work properly. '; 
    message := message + 'You can install the service provider later.'
    SuppressibleMsgBox(message, mbError, MB_OK, IDOK);
  end;
end;

function H1__CheckInstallation() : Boolean;
var
  h1_desc : String;
begin
  Result := False;
  if RegQueryStringValue(HKLM, 'SOFTWARE\HITACHI\BioAPI\BSP\{e41a9357-67df-44d5-80e3-64935d21b642}', 'BSPDescription', h1_desc) then
  begin
    Result := True;
  end;
  if (not Result) then // asks a user about Hitachi drivers' installation
  begin
    Result := H1__ShowMessage();
  end;
end;