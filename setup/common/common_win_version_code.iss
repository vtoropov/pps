(*
;----------------------------------------------------------------------------
; Created by Tech_dog (VToropov) on 30-Apr-2014 at 6:47:47pm, GMT+4, Saint-Petersburg, Wednesday;
; This is MS Windows Version related code for the Platinum applications installer;
;----------------------------------------------------------------------------
*)

[Code]

//type
//  TWindowsVersion = record
//    Major           : Cardinal;  // Major version number
//    Minor           : Cardinal;  // Minor version number
//    Build           : Cardinal;  // Build number
//    ServicePackMajor: Cardinal;  // Major version number of service pack
//    ServicePackMinor: Cardinal;  // Minor version number of service pack
//    NTPlatform      : Boolean;   // True if an NT-based platform
//    ProductType     : Byte;      // Product type (see below)
//    SuiteMask       : Word;      // Product suites installed (see below)
//  end;

const
     WIN_VERSION_2000        = $05000000;
     WIN_VERSION_XP          = $05010000;
     WIN_VERSION_XP_64       = $05020000;
     WIN_VERSION_SVR_2003    = $05020000;
     WIN_VERSION_SVR_2003_R2 = $05020000;
     WIN_VERSION_VISTA       = $06000000;
     WIN_VERSION_SVR_2008    = $06000000;
     WIN_VERSION_7           = $06010000;
     WIN_VERSION_SVR_2008_R2 = $06010000;
     WIN_VERSION_8           = $06020000;
     WIN_VERSION_SVR_2012    = $06020000;
     WIN_VERSION_8_1         = $06030000;
     WIN_VERSION_SVR_2012_R2 = $06030000;

/////////////////////////////////////////////////////////////////////////////
//  Operating Platform Version related functions and procedures            //
/////////////////////////////////////////////////////////////////////////////

function Version__IsWindows2000() : Boolean;
var
    ver_info: TWindowsVersion;
begin
    GetWindowsVersionEx(ver_info);
    Result  := (ver_info.NTPlatform and (ver_info.Major = 5) and (ver_info.Minor = 0));
end;

function Version__IsWindowsXP() : Boolean;
var
    ver_info: TWindowsVersion;
begin
    GetWindowsVersionEx(ver_info);
    Result  := (ver_info.NTPlatform and (ver_info.Major = 5) and (ver_info.Minor = 1));
end;

function Version__IsWindowsXP64() : Boolean;
var
    ver_info: TWindowsVersion;
begin
    GetWindowsVersionEx(ver_info);
    Result  := (ver_info.NTPlatform and (ver_info.Major = 5) and (ver_info.Minor = 2) and (ver_info.ProductType = VER_NT_WORKSTATION));
end;

function Version__IsWindowsSvr2003() : Boolean;
var
    ver_info: TWindowsVersion;
begin
    GetWindowsVersionEx(ver_info);
    Result  := (ver_info.NTPlatform and (ver_info.Major = 5) and (ver_info.Minor = 2) and (ver_info.ProductType <> VER_NT_WORKSTATION));
end;

function Version__IsWindowsVista() : Boolean;
var
    ver_info: TWindowsVersion;
begin
    GetWindowsVersionEx(ver_info);
    Result  := (ver_info.NTPlatform and (ver_info.Major = 6) and (ver_info.Minor = 0) and (ver_info.ProductType = VER_NT_WORKSTATION));
end;

function Version__IsWindowsSvr2008() : Boolean;
var
    ver_info: TWindowsVersion;
begin
    GetWindowsVersionEx(ver_info);
    Result  := (ver_info.NTPlatform and (ver_info.Major = 6) and (ver_info.Minor = 0) and (ver_info.ProductType <> VER_NT_WORKSTATION));
end;

function Version__IsWindows7() : Boolean;
var
    ver_info: TWindowsVersion;
begin
    GetWindowsVersionEx(ver_info);
    Result  := (ver_info.NTPlatform and (ver_info.Major = 6) and (ver_info.Minor = 1) and (ver_info.ProductType = VER_NT_WORKSTATION));
end;

function Version__IsWindowsSvr2008R2() : Boolean;
var
    ver_info: TWindowsVersion;
begin
    GetWindowsVersionEx(ver_info);
    Result  := (ver_info.NTPlatform and (ver_info.Major = 6) and (ver_info.Minor = 1) and (ver_info.ProductType <> VER_NT_WORKSTATION));
end;

function Version__IsWindows8() : Boolean;
var
    ver_info: TWindowsVersion;
begin
    GetWindowsVersionEx(ver_info);
    Result  := (ver_info.NTPlatform and (ver_info.Major = 6) and (ver_info.Minor = 2) and (ver_info.ProductType = VER_NT_WORKSTATION));
end;

function Version__IsWindowsSvr2012() : Boolean;
var
    ver_info: TWindowsVersion;
begin
    GetWindowsVersionEx(ver_info);
    Result  := (ver_info.NTPlatform and (ver_info.Major = 6) and (ver_info.Minor = 2) and (ver_info.ProductType <> VER_NT_WORKSTATION));
end;

function Version__IsWindows8_1() : Boolean;
var
    ver_info: TWindowsVersion;
begin
    GetWindowsVersionEx(ver_info);
    Result  := (ver_info.NTPlatform and (ver_info.Major = 6) and (ver_info.Minor = 3) and (ver_info.ProductType = VER_NT_WORKSTATION));
end;

function Version__IsWindowsSvr2012R2() : Boolean;
var
    ver_info: TWindowsVersion;
begin
    GetWindowsVersionEx(ver_info);
    Result  := (ver_info.NTPlatform and (ver_info.Major = 6) and (ver_info.Minor = 3) and (ver_info.ProductType <> VER_NT_WORKSTATION));
end;

function Version__GetWindowsName() : string;
var
    s_name   : string;
    ver_info : TWindowsVersion;
begin
    GetWindowsVersionEx(ver_info);
    if not ver_info.NTPlatform then
    begin
        s_name := 'Not Windows platform';
    end
    else if ((ver_info.Major = 5) and (ver_info.Minor = 0)) then
    begin
        s_name := 'MS Windows 2000';
    end
    else if ((ver_info.Major = 5) and (ver_info.Minor = 1)) then
    begin
        s_name := 'MS Windows XP';
    end
    else if ((ver_info.Major = 5) and (ver_info.Minor = 2)  and (ver_info.ProductType = VER_NT_WORKSTATION)) then
    begin
        s_name := 'MS Windows XP';
    end
    else if ((ver_info.Major = 5) and (ver_info.Minor = 2)  and (ver_info.ProductType <> VER_NT_WORKSTATION)) then
    begin
        s_name := 'MS Windows Server 2003';
    end
    else if ((ver_info.Major = 6) and (ver_info.Minor = 0)  and (ver_info.ProductType = VER_NT_WORKSTATION)) then
    begin
        s_name := 'MS Windows Vista';
    end
    else if ((ver_info.Major = 6) and (ver_info.Minor = 0)  and (ver_info.ProductType <> VER_NT_WORKSTATION)) then
    begin
        s_name := 'MS Windows Server 2008';
    end
    else if ((ver_info.Major = 6) and (ver_info.Minor = 1)  and (ver_info.ProductType = VER_NT_WORKSTATION)) then
    begin
        s_name := 'MS Windows 7';
    end
    else if ((ver_info.Major = 6) and (ver_info.Minor = 1)  and (ver_info.ProductType <> VER_NT_WORKSTATION)) then
    begin
        s_name := 'MS Windows Server 2008 R2';
    end
    else if ((ver_info.Major = 6) and (ver_info.Minor = 2)  and (ver_info.ProductType = VER_NT_WORKSTATION)) then
    begin
        s_name := 'MS Windows 8';
    end
    else if ((ver_info.Major = 6) and (ver_info.Minor = 2)  and (ver_info.ProductType <> VER_NT_WORKSTATION)) then
    begin
        s_name := 'MS Windows Server 2012';
    end
    else if ((ver_info.Major = 6) and (ver_info.Minor = 3)  and (ver_info.ProductType = VER_NT_WORKSTATION)) then
    begin
        s_name := 'MS Windows 8.1';
    end
    else if ((ver_info.Major = 6) and (ver_info.Minor = 3)  and (ver_info.ProductType <> VER_NT_WORKSTATION)) then
    begin
        s_name := 'MS Windows Server 2012 R2';
    end
    else
    begin
        s_name := '{Unknown Windows Version}';
    end;
    Result := s_name;
end;

function Version__IsWwsSupported() : Boolean;
var
    ver_info : TWindowsVersion;
begin
    GetWindowsVersionEx(ver_info);
    Result := (ver_info.NTPlatform and (ver_info.Major >= 6) and (ver_info.Minor > 0));
end;