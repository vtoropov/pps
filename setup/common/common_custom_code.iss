(*
;-----------------------------------------------------------------------------
;   Created by Tech_dog (VToropov) on 30-Apr-2014 at 6:29:57pm, GMT+4, Saint-Petersburg, Wednesday;
;   This is Common Code of the Platinum Payroll Systems Applications Installation Program.
;-----------------------------------------------------------------------------
*)

[Code]
/////////////////////////////////////////////////////////////////////////////
//  Global variables and constants                                         //
/////////////////////////////////////////////////////////////////////////////

/////////////////////////////////////////////////////////////////////////////
//  Custom functions and procedures                                        //
/////////////////////////////////////////////////////////////////////////////

function Common__IniExist(bThoroughly: Boolean): Boolean;
var
  FindRec: TFindRec;
begin
  Result := False;
  if FindFirst(ExpandConstant('{app}\cfg\*.ini'), FindRec) then begin
    Log('Custom Code: Ini(s) exist');
    Result := True;
    FindClose(FindRec);
  end;
    
  if not Result then begin
    if bThoroughly then begin
      if DirExists(ExpandConstant('{app}\cfg')) then  Result := True;
    end;
  end;
end;

procedure Common__RemoveIni();
begin
//DeleteFile(ExpandConstant('{app}\cfg\PlatinumClient.ini'));
  DelTree(ExpandConstant('{app}\cfg\*.ini'), False, True, False);
  Log('Custom Code: INI file is deleted');
  RemoveDir(ExpandConstant('{app}\cfg\'));
end;

function Common__IsUpgrade(): Boolean;
var
  sPrevPath: String;
begin
  sPrevPath := WizardForm.PrevAppDir;
  Result := (sPrevPath <> '');
end;

function Common__LogsExist(): Boolean;
var
  FindRec: TFindRec;
begin
  Result := False;
  if FindFirst(ExpandConstant('{app}\logs\*.log'), FindRec) then begin
    Log('Custom Code: Logs exist');
    Result := True;
    FindClose(FindRec);
  end;
    
  if not Result then begin
    if DirExists(ExpandConstant('{app}\logs')) then  Result := True;
  end;
end;

procedure Common__RemoveLogs();
begin
  DelTree(ExpandConstant('{app}\logs\*.log'), False, True, False);
  RemoveDir(ExpandConstant('{app}\logs\'));
end;

function Common__StorageExist(): Boolean;
var
  FindRec: TFindRec;
begin
  Result := False;
  if FindFirst(ExpandConstant('{app}\storage\*.csv'), FindRec) then begin
    Log('Custom Code: storage exist');
    Result := True;
    FindClose(FindRec);
  end;

  if not Result then begin
    if DirExists(ExpandConstant('{app}\storage')) then  Result := True;
  end;

end;

procedure Common__RemoveStorage();
begin
  DelTree(ExpandConstant('{app}\storage\*.csv'), False, True, False);
  RemoveDir(ExpandConstant('{app}\storage\'));
end;
