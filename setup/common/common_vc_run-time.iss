(*
;----------------------------------------------------------------------------
; Created by Tech_dog (VToropov) on 30-Apr-2014 at 6:43:58pm, GMT+4, Saint-Petersburg, Wednesday;
; This is VC runtime installation checking related script for Platinum project;
;----------------------------------------------------------------------------
*)

[Code]

const VC__Runtime_Version_8   = '8.0.50727.4053';
const VC__Runtime_Version_9   = '9.0.30729.6161';
const VC__Runtime_Version_9_s = '30729.6161';

function VC__ShowMessage(version_req : String) : Boolean;
var
  message : String;
begin
  Result  := False;
  message := '';
  message := message + 'Oops, looks like your computer does not have Microsoft Visual C++ run-time version ';
  message := message +  version_req + '. ';
  message := message + 'You need to install it to run Platinum applications. ';
  message := message + 'It''s fast and won''t slow down your computer. ';
  message := message + 'Simply Click "Okay" or "Cancel" to skip installation.';
  if (IDOK = SuppressibleMsgBox(message, mbConfirmation, MB_OKCANCEL, IDOK)) then begin
    Result  := False; // a user agreed to install vc++
  end else begin
    Result  := True;
  end;
  if (Result) then    // indicates that a user has pressed the cancel button
  begin
    message := 'You have chosen to not install Microsoft Visual C++ run-time. Platinum applications may not work properly. You can install it later.'
    SuppressibleMsgBox(message, mbError, MB_OK, IDOK);
  end;
end;

function VC__CheckVersion_V8() : Boolean;
var
  sp_name : String;
begin
  Result := False;
  if RegQueryStringValue(HKLM, 'SOFTWARE\Microsoft\DevDiv\VC\Servicing\8.0\RED\1033', 'SPName', sp_name) then
  begin
    Log('Custom Code: VC 8.0 ATL run-time service pack name is' + sp_name);
    Result := ('RTM' = sp_name);
  end else begin
    Result := False;   // there is no appropriate registry entry
  end;
  if (not Result) then // asks a user about VC++ installation
  begin
    Result := VC__ShowMessage(VC__Runtime_Version_8);
  end;
end;

function VC__CheckVersion_V9() : Boolean;
var
  current_ver : String;
begin
  Result := False;
  if RegQueryStringValue(HKLM, 'SOFTWARE\Microsoft\DevDiv\VC\Servicing\9.0\RED\1033', 'Version', current_ver) then
  begin
    Log('Custom Code: VC run-time version installed is 9.0.' + current_ver);
    Result := (VC__Runtime_Version_9_s = current_ver);
  end else begin
    Result := False;   // there is no appropriate registry entry
  end;
  if (not Result) then // asks a user about VC++ installation
  begin
    Result := VC__ShowMessage(VC__Runtime_Version_9);
  end;
end;