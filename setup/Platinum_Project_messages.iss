;
; Created by Tech_dog (VToropov) on 1-May-2014 at 5:10:21pm, GMT+4, Saint-Petersburg, Thursday;
; This is custom messages for Platinum Clocking Suite Project installation program;
;

[CustomMessages]
; msg=Message, tsk=Task
; English
en.msg_main_setup_is_running       =Platinum Clocking Suite Setup is already running.
en.msg_main_service_not_available  =The service manager is not available.
en.msg_main_service_not_supported  =Only NT based systems support services.
en.msg_main_client_delete_logs     =Do you want to delete all Platinum Clocking Suite applications' log files?
en.msg_main_client_delete_storage  =Do you want to delete all Platinum Clocking Suite applications' storage files?
en.msg_main_client_delete_INI      =Do you want to delete Platinum Clocking Suite applications' INI file?
en.run_main_visit_website          =Visit Platinum Payroll Systems's Website
en.tsk_other                       =Other tasks:
en.tsk_client_reset                =Delete/Replace Platinum Clocking Suite files:
en.tsk_delete_logs                 =Logs
en.tsk_delete_storage              =Storage
en.tsk_delete_INI                  =INI file (Replace with default copy)
en.tsk_startup                     =Startup option:
en.tsk_CreateDesktopIcon           =Create desktop icons
