;-----------------------------------------------------------------------------
; Created by Tech_dog (VToropov) on 1-May-2014 at 4:27:27pm, GMT+4, Saint-Petersburg, Thursday;
; This is Platinum Application Main Project Setup for both architectures (x86 and x64).
;-----------------------------------------------------------------------------

#if VER < 0x05040200
  #error Update your Inno Setup version
#endif

#define app_version            "1.5.2.3"
#define simple_app_version     "1.5.2.3"
#define copyright              "Copyright � 2014-2016, Platinum Payroll Systems"
#define installer_build_date   GetDateTimeString('mmm, d yyyy', '', '')
#define quick_launch_clocking  "{userappdata}\Microsoft\Internet Explorer\Quick Launch\Platinum Clocking"
#define quick_launch_manager   "{userappdata}\Microsoft\Internet Explorer\Quick Launch\Platinum Manager"
#define bindir_x64             "E:\Prog_Man\_B2B_\oDesk\PP_Systems\Project\__bin__\x64\release"
#define bindir_x86             "E:\Prog_Man\_B2B_\oDesk\PP_Systems\Project\__bin__\x86\release"
#define setdir                 "E:\Prog_Man\_B2B_\oDesk\PP_Systems\Project.Installers"

[Setup]
AppId                          ={{F0DB2D6E-48B9-43b1-B0A9-5FCA9F146C94}
AppName                        =Platinum Clocking Suite
AppVersion                     ={#app_version}
AppVerName                     =Platinum Clocking Suite {#app_version}
AppPublisher                   =Platinum Payroll Systems Ltd.
AppCopyright                   ={#copyright}
AppPublisherURL                =http://platinumpayroll.co.nz/
AppSupportURL                  =http://platinumpayroll.co.nz/
AppUpdatesURL                  =http://platinumpayroll.co.nz/
AppContact                     =http://platinumpayroll.co.nz/
VersionInfoCompany             =Platinum Payroll Systems Ltd.
VersionInfoCopyright           ={#copyright}
VersionInfoDescription         =Platinum Clocking Suite {#app_version} Setup
VersionInfoProductName         =Platinum Clocking Suite
VersionInfoProductVersion      ={#app_version}
VersionInfoTextVersion         ={#app_version}
VersionInfoVersion             ={#app_version}
DefaultDirName                 ={src}
DefaultGroupName               =Platinum Clocking Suite
LicenseFile                    =Platinum_Project_License.txt
InfoBeforeFile                 =Platinum_Project_readme_before.rtf
OutputDir                      =.
OutputBaseFilename             =Platinum_Clocking_Suite-Setup_v{#app_version}

; "ArchitecturesInstallIn64BitMode=x64" requests that the install be
; done in "64-bit mode" on x64, meaning it should use the native
; 64-bit Program Files directory and the 64-bit view of the registry.
; On all other architectures it will install in "32-bit mode".
;ArchitecturesInstallIn64BitMode=x64
; Note: We don't set ProcessorsAllowed because we want this
; installation to run on all architectures (including Itanium,
; since it's capable of running 32-bit code too).

Compression                    =lzma2/max
SolidCompression               =yes
MinVersion                     =0,5.1
UninstallDisplayName           =Platinum Clocking Suite {#app_version}
UninstallDisplayIcon           ={app}\Platinum_Clocking_Suite.ico
AppReadmeFile                  ={app}\Platinum_Project_readme_before.rtf
WizardImageFile                =.\common_img\Platinum_Installer_LeftSide_Banner.bmp
WizardSmallImageFile           =compiler:WizModernSmallImage-IS.bmp
SetupIconFile                  =Platinum_Project_Clocking.ico
AllowNoIcons                   =yes
ShowTasksTreeLines             =yes
AlwaysShowDirOnReadyPage       =yes
AlwaysShowGroupOnReadyPage     =yes
PrivilegesRequired             =admin
DisableDirPage                 =no
DisableProgramGroupPage        =yes
AppMutex                       =Global\PayrollTimeClock,Global\Platinum_Client

[Languages]
Name: en; MessagesFile: compiler:Default.isl

#include "Platinum_Project_messages.iss"

[Messages]
BeveledLabel                   =Platinum Clocking Suite {#app_version} built on {#installer_build_date}
SetupAppTitle                  =Setup - Platinum Clocking Suite
SetupWindowTitle               =Setup - Platinum Clocking Suite

[Tasks]
Name: desktopicon;     Description: {cm:tsk_CreateDesktopIcon};       GroupDescription: {cm:AdditionalIcons}
Name: quicklaunchicon; Description: {cm:CreateQuickLaunchIcon};       GroupDescription: {cm:AdditionalIcons};  Flags: unchecked; OnlyBelowVersion: 0,6.01

Name: delete_logs;     Description: {cm:tsk_delete_logs};             GroupDescription: {cm:tsk_client_reset}; Flags: checkedonce unchecked; Check: Common__LogsExist()
Name: delete_storage;  Description: {cm:tsk_delete_storage};          GroupDescription: {cm:tsk_client_reset}; Flags: checkedonce unchecked; Check: Common__StorageExist()
Name: delete_INI;      Description: {cm:tsk_delete_INI};              GroupDescription: {cm:tsk_client_reset}; Flags: checkedonce unchecked; Check: Common__IniExist(false)

[Types]
Name: type_0;          Description: Platinum Clocking Suite Complete Installation
Name: type_1;          Description: Platinum Clocking Installation
Name: type_2;          Description: Platinum Manager Installation

[Components]
Name: component_0;     Description: Platinum Clocking; Types: type_0 type_1
Name: component_1;     Description: Platinum Manager;  Types: type_0 type_2

[Files]
;-----------------------------------------------------------------------------
; Hitachi finger vein scanner driver installers;
;-----------------------------------------------------------------------------

Source: {#setdir}\common\3dParty\HiBioAPI_setup_x64.exe;              Flags: dontcopy deleteafterinstall
Source: {#setdir}\common\3dParty\HiBioAPI_setup_x86.exe;              Flags: dontcopy deleteafterinstall

;-----------------------------------------------------------------------------
; visual c++ runtime related (side-by-side); Microsoft Installers;
;-----------------------------------------------------------------------------
;Source: {#setdir}\VC_2008_Redistributable_9.0.30729.6161_x64.exe;     Flags: dontcopy deleteafterinstall
;Source: {#setdir}\VC_2008_Redistributable_9.0.30729.6161_x86.exe;     Flags: dontcopy deleteafterinstall

;-----------------------------------------------------------------------------
; Platinum Clocking Suite registry files;
;-----------------------------------------------------------------------------
Source: {#setdir}\common\registry\PlatinumClocking.reg;               Flags: dontcopy deleteafterinstall
Source: {#setdir}\common\registry\PlatinumManager.reg;                Flags: dontcopy deleteafterinstall

;-----------------------------------------------------------------------------
; Platinum Clocking Suite applications' files;
;-----------------------------------------------------------------------------
Source: {#bindir_x64}\PlatinumClocking_x64.exe;      DestDir:  {app}; DestName: PlatinumClocking.exe;        Check: IsWin64();     Components: component_0
Source: {#bindir_x86}\PlatinumClocking_x86.exe;      DestDir:  {app}; DestName: PlatinumClocking.exe;        Check: IsWin32();     Components: component_0
Source: {#bindir_x64}\PlatinumManager_x64.exe;       DestDir:  {app}; DestName: PlatinumManager.exe;         Check: IsWin64();     Components: component_1
Source: {#bindir_x86}\PlatinumManager_x86.exe;       DestDir:  {app}; DestName: PlatinumManager.exe;         Check: IsWin32();     Components: component_1
Source: {#bindir_x64}\Shared_SysHook_x64.dll;        DestDir:  {app}; DestName: Shared_SysHook.dll;          Check: IsWin64();     Components: component_0
Source: {#bindir_x86}\Shared_SysHook_x86.dll;        DestDir:  {app}; DestName: Shared_SysHook.dll;          Check: IsWin32();     Components: component_0
Source: Platinum_Project_License.txt;                DestDir:  {app}; DestName: license.txt;                 Flags: ignoreversion
Source: Platinum_Project_readme_after.rtf;           DestDir:  {app}; DestName: readme.rtf;                  Flags: ignoreversion
Source: {#setdir}\common\profile\PlatinumClient.ini; DestDir:  {app}\cfg;    DestName: PlatinumClient.ini;          Check: CopyIni(false)
Source: Platinum_Project_Clocking.ico;               DestDir:  {app};        DestName: Platinum_Clocking_Suite.ico; Flags: ignoreversion
Source: {#setdir}\common\sounds\scan_failure.wav;    DestDir:  {app}\sounds; DestName: scan_failure.wav;     Flags: ignoreversion
Source: {#setdir}\common\sounds\scan_success.wav;    DestDir:  {app}\sounds; DestName: scan_success.wav;     Flags: ignoreversion

[Icons]
Name: {group}\Platinum Clocking;                     Filename: {app}\PlatinumClocking.exe; WorkingDir: {app}; IconFilename: {app}\PlatinumClocking.exe; IconIndex: 0; Comment: Platinum Clocking {#app_version}; Components: component_0
Name: {group}\Platinum Manager;                      Filename: {app}\PlatinumManager.exe;  WorkingDir: {app}; IconFilename: {app}\PlatinumManager.exe;  IconIndex: 0; Comment: Platinum Manager  {#app_version}; Components: component_1
Name: {group}\Uninstall Platinum Clocking Suite;     Filename: {uninstallexe};             WorkingDir: {app}; IconFilename: {app}\Platinum_Clocking_Suite.ico;        Comment: {cm:UninstallProgram,Platinum Clocking Suite}
Name: {group}\Help and Support\ReadMe;               Filename: {app}\readme.rtf;           WorkingDir: {app};                                                         Comment: Platinum Clocking ReadMe
Name: {userdesktop}\Platinum Clocking;               Filename: {app}\PlatinumClocking.exe; WorkingDir: {app}; IconFilename: {app}\PlatinumClocking.exe; IconIndex: 0; Comment: Platinum Clocking {#app_version}; Tasks: desktopicon;     Components: component_0
Name: {userdesktop}\Platinum Manager;                Filename: {app}\PlatinumManager.exe;  WorkingDir: {app}; IconFilename: {app}\PlatinumManager.exe;  IconIndex: 0; Comment: Platinum Manager  {#app_version}; Tasks: desktopicon;     Components: component_1
Name: {#quick_launch_clocking};                      Filename: {app}\PlatinumClocking.exe; WorkingDir: {app}; IconFilename: {app}\PlatinumClocking.exe; IconIndex: 0; Comment: Platinum Clocking {#app_version}; Tasks: quicklaunchicon; Components: component_0
Name: {#quick_launch_manager};                       Filename: {app}\PlatinumManager.exe;  WorkingDir: {app}; IconFilename: {app}\PlatinumManager.exe;  IconIndex: 0; Comment: Platinum Manager  {#app_version}; Tasks: quicklaunchicon; Components: component_1

; includes all required registry entries
#include "Platinum_Project_registry_entries.iss"

[Run]
;Filename: {app}\PlatinumClocking.exe;               Description: {cm:LaunchProgram,Platinum Clocking};      WorkingDir: {app}; Flags: nowait postinstall skipifsilent runascurrentuser
;Filename: {app}\PlatinumManager.exe;                Description: {cm:LaunchProgram,Platinum Manager};       WorkingDir: {app}; Flags: nowait postinstall skipifsilent runascurrentuser
Filename: http://platinumpayroll.co.nz/;             Description: {cm:run_main_visit_website}; Flags: nowait postinstall skipifsilent shellexec unchecked

[InstallDelete]
; During installation, delete old files in install folder
Name: {app}\license.txt; Type: files
; While we are at it, delete any shortcut which is not selected
Name: {userdesktop}\Platinum Clocking.lnk;           Type: files; Check: not IsTaskSelected('desktopicon')     and Common__IsUpgrade()
Name: {userdesktop}\Platinum Manager.lnk;            Type: files; Check: not IsTaskSelected('desktopicon')     and Common__IsUpgrade()
Name: {#quick_launch_clocking}.lnk;                  Type: files; Check: not IsTaskSelected('quicklaunchicon') and Common__IsUpgrade(); OnlyBelowVersion: 0,6.01
Name: {#quick_launch_manager}.lnk;                   Type: files; Check: not IsTaskSelected('quicklaunchicon') and Common__IsUpgrade(); OnlyBelowVersion: 0,6.01

[Code]
#include ".\common\common_custom_code.iss"
#include ".\common\common_services.iss"
//#include ".\common\common_vc_run-time.iss"
#include ".\common\common_hitachi_h-1.iss"

//////////////////////////////////////////////////////////////////////////////
//  Inno Setup functions and procedures                                     //
//////////////////////////////////////////////////////////////////////////////
const
  installer_mutex_name = 'platinum_clocking_setup_mutex';

function CopyIni(bCheckDir: Boolean): Boolean;
begin
  if not Common__IniExist(bCheckDir) then begin
    Result := True;
  end else begin
    Result := IsTaskSelected('delete_INI');
  end;
end;

function IsWin32(): Boolean;
begin
  Result := not IsWin64();
end;

function IsWin7(): Boolean;
begin
  Result := (GetWindowsVersion > $06000000);
end;

function InitializeSetup(): Boolean;
var
  merge__msi : String;
  merge__err : Integer;
begin
  // Create a mutex for the installer.
  // If it's already running display a message and stop the installation
  if CheckForMutexes(installer_mutex_name) and not WizardSilent() then begin
      Log('Custom Code: Installer is already running');
      SuppressibleMsgBox(CustomMessage('msg_main_setup_is_running'), mbError, MB_OK, MB_OK);
      Result := False;
  end
  else begin
    Result := True;
    Log('Custom Code: Creating installer`s mutex');
    CreateMutex(installer_mutex_name);

    if H1__CheckInstallation() then begin
      Result := True;
    end else begin
      if IsWin64() then begin
        merge__msi := 'HiBioAPI_setup_x64.exe';
      end else begin
        merge__msi := 'HiBioAPI_setup_x86.exe';
      end;
      ExtractTemporaryFile(merge__msi);
      ShellExec('runas', ExpandConstant('{tmp}\' + merge__msi), '', '',  SW_SHOWNORMAL, ewWaitUntilTerminated, merge__err);
      Result := True;
    end;

//    if VC__CheckVersion_V9() then begin
//      Result := True;
//    end else begin
//      if IsWin64() then begin
//        merge__msi := 'VC_2008_Redistributable_9.0.30729.6161_x64.exe';
//      end else begin
//        merge__msi := 'VC_2008_Redistributable_9.0.30729.6161_x86.exe';
//      end;
//      ExtractTemporaryFile(merge__msi);
//      ShellExec('runas', ExpandConstant('{tmp}\' + merge__msi), '', '',  SW_SHOWNORMAL, ewWaitUntilTerminated, merge__err);
//      Result := True;
//    end;
  end;
end;

function InitializeUninstall(): Boolean;
begin
  if CheckForMutexes(installer_mutex_name) then begin
    SuppressibleMsgBox(CustomMessage('msg_main_setup_is_running'), mbError, MB_OK, MB_OK);
    Result := False;
  end
  else begin
    CreateMutex(installer_mutex_name);
    Result := True;
  end;
end;

function ShouldSkipPage(PageID: Integer): Boolean;
begin
  if Common__IsUpgrade() then begin
    // Hide the license page
    if (PageID = wpLicense)
//  or (PageID = wpInfoBefore) 
    then begin
       Result := True;
    end
    else begin
       Result := False;
    end;
  end;
end;

procedure CurStepChanged(CurStep: TSetupStep);
  var
    iResultCode: Integer;
    reg_file   : String;
    cmd_line   : String;
    reg_error  : Integer;
begin
  if CurStep = ssInstall then begin
  //  if IsComponentSelected('compononent_0') then begin
  //    Log('Custom Code: merging PlatinumClocking.reg');
  //    reg_file := 'PlatinumClocking.reg';
  //    ExtractTemporaryFile(reg_file);
  //    cmd_line := '';
  //    cmd_line := cmd_line + 'reg import ';
  //    cmd_line := cmd_line + ExpandConstant('{tmp}\' + reg_file);
  //    Log('Running the command: ' + cmd_line);
  //    ShellExec('runas', cmd_line, '', '',  SW_SHOWNORMAL, ewWaitUntilTerminated, reg_error);
  //  end;
  //
  //  if IsComponentSelected('compononent_1') then begin
  //    Log('Custom Code: merging PlatinumManager.reg');
  //    reg_file := 'PlatinumManager.reg';
  //    ExtractTemporaryFile(reg_file);
  //    cmd_line := '';
  //    cmd_line := cmd_line + 'reg import ';
  //    cmd_line := cmd_line + ExpandConstant('{tmp}\' + reg_file);
  //    Log('Running the command: ' + cmd_line);
  //    ShellExec('runas', cmd_line, '', '',  SW_SHOWNORMAL, ewWaitUntilTerminated, reg_error);
  //  end;
  end;

  if CurStep = ssPostInstall then begin

    if IsTaskSelected('delete_logs') then begin
       Log('Custom Code: User selected the "delete_logs" task');
       Common__RemoveLogs;
    end;

    if IsTaskSelected('delete_storage') then begin
       Log('Custom Code: User selected the "delete_storage" task');
       Common__RemoveStorage;
    end;

//    if IsTaskSelected('delete_INI') then begin
//       Log('Custom Code: User selected the "delete_INI" task');
//       Common__RemoveINI;
//    end;

  end;
end;

procedure CurUninstallStepChanged(CurUninstallStep: TUninstallStep);
begin
  if CurUninstallStep = usUninstall then begin
    if (Common__LogsExist()) then begin
        if SuppressibleMsgBox(
                       CustomMessage('msg_main_client_delete_logs'),
                       mbConfirmation,
                       MB_YESNO or MB_DEFBUTTON2,
                       IDNO
                   ) = IDYES then begin
           Common__RemoveLogs();
        end;
    end;
    if (Common__StorageExist()) then begin
        if SuppressibleMsgBox(
                       CustomMessage('msg_main_client_delete_storage'),
                       mbConfirmation,
                       MB_YESNO or MB_DEFBUTTON2,
                       IDNO
                   ) = IDYES then begin
           Common__RemoveStorage();
        end;
    end;
 //   if (Common__IniExist(true)) then begin
 //       if SuppressibleMsgBox(CustomMessage('msg_main_client_delete_INI'), mbConfirmation, MB_YESNO or MB_DEFBUTTON2, IDNO) = IDYES then begin
 //           Common__RemoveINI();
 //       end;
 //   end;

    RemoveDir(ExpandConstant('{app}'));
  end;
end;