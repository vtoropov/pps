;
; Created by Tech_dog (VToropov) on 30-Apr-2014 at 7:02:33pm, GMT+4, Saint-Petersburg, Wednesday;
; This is custom messages for Platinum Clocking Application Project installation program;
;

[CustomMessages]
; msg=Message, tsk=Task
; English
en.msg_client_setup_is_running    =Platinum Clocking Application Setup is already running.
en.msg_service_not_available      =The service manager is not available.
en.msg_service_not_supported      =Only NT based systems support services.
en.msg_client_delete_logs         =Do you want to delete all Platinum Clocking application log files?
en.run_visit_website              =Visit Platinum Payroll Systems's Website
en.tsk_other                      =Other tasks:
en.tsk_client_reset               =Delete Platinum Clocking App's files:
en.tsk_delete_logs                =Logs
en.tsk_startup                    =Startup option:
