/*
	Created by Tech_dog (VToropov) on 9-Feb-2015 at 9:17:23pm, GMT+3, Taganrog, Monday;
	This is UIX Frame library panel base class implementation file.
*/
#include "StdAfx.h"
#include "UIX_PanelBase.h"
#include "UIX_GdiProvider.h"
#include "Shared_EventLogger.h"

using namespace ex_ui;
using namespace ex_ui::frames;
using namespace ex_ui::draw;

////////////////////////////////////////////////////////////////////////////

CMessageHandlerDefImpl::CMessageHandlerDefImpl(void)
{
}

CMessageHandlerDefImpl::~CMessageHandlerDefImpl(void)
{
}

////////////////////////////////////////////////////////////////////////////

LRESULT CMessageHandlerDefImpl::MessageHandler_OnMessage(UINT, WPARAM, LPARAM, BOOL& bHandled)
{
	bHandled = FALSE;
	return 0;
}

////////////////////////////////////////////////////////////////////////////

CPanelBase::CPanelBaseWnd::CPanelBaseWnd(const UINT nResId, IMessageHandler& snk_ref, CPanelBase& panel_ref):
	IDD(nResId),
	m_sink_ref(snk_ref),
	m_pane_ref(panel_ref),
	m_bkgnd_renderer(NULL),
	m_parent_renderer(NULL)
{
}

CPanelBase::CPanelBaseWnd::~CPanelBaseWnd(void)
{
}

////////////////////////////////////////////////////////////////////////////

LRESULT CPanelBase::CPanelBaseWnd::OnCreate  (UINT uMsg, WPARAM wParam, LPARAM lParam, BOOL& bHandled)
{
	uMsg; wParam; lParam; bHandled = FALSE;
	return 0;
}

LRESULT CPanelBase::CPanelBaseWnd::OnDestroy (UINT uMsg, WPARAM wParam, LPARAM lParam, BOOL& bHandled)
{
	uMsg; wParam; lParam; bHandled = FALSE;
	return 0;
}

LRESULT CPanelBase::CPanelBaseWnd::OnErase   (UINT uMsg, WPARAM wParam, LPARAM lParam, BOOL& bHandled)
{
	TRACE_FUNC();
	TRACE_INFO_a1(_T("page__id: %u"), this->IDD);

	uMsg; wParam; lParam; bHandled;
	RECT rc_ = {0};
	TWindow::GetClientRect(&rc_);

	::WTL::CDCHandle dc = (HDC)wParam;
	if (NULL != m_bkgnd_renderer)
	{
		const HRESULT hr_ = m_bkgnd_renderer->DrawBackground(dc, rc_);
		if (FAILED(hr_))
		{	TRACE_ERR__a1(_T("bkgnd_rnd_err: 0x%x"), hr_);}
#if (0)
		else
		{	TRACE_INFO(_T("bkgnd_rnd__ok"));}
#endif
	}
	else
	if (NULL != m_parent_renderer)
	{
		CZBuffer dc_(dc, rc_);
		dc_.FillSolidRect(&rc_, RGB(0xee, 0xee, 0xee));
		const HRESULT hr_ = (*m_parent_renderer).DrawParentBackground(TWindow::m_hWnd, (HDC)dc_, rc_);
		if (FAILED(hr_))
		{	TRACE_ERR__a1(_T("prn_bkgnd_rnd_err: 0x%x"), hr_);}
#if (0)
		else
		{	TRACE_INFO(_T("prn_bkgnd_rnd__ok"));}
#endif
	}
	else
	{
		TRACE_INFO(_T("no_rnd__set()"));
		dc.FillSolidRect(&rc_, RGB(0xff, 0xff, 0xff));
	}
	return 0;
}

LRESULT CPanelBase::CPanelBaseWnd::OnShowHide(UINT uMsg, WPARAM wParam, LPARAM lParam, BOOL& bHandled)
{
	uMsg; wParam; lParam; bHandled;
	if (NULL != lParam)
	{
		LPWINDOWPOS const pPos = reinterpret_cast<LPWINDOWPOS>(lParam);
		if (NULL != pPos) // in order to invoke overriding functions of the derived class
		{
			if (false){}
			else if (SWP_SHOWWINDOW & (*pPos).flags) m_pane_ref.Show();
			else if (SWP_HIDEWINDOW & (*pPos).flags) m_pane_ref.Hide();
		}
	}
	return 0;
}

////////////////////////////////////////////////////////////////////////////

CPanelBase::CPanelBase(const UINT nResId, IMessageHandler& snk_ref, const DWORD dwStyle, const bool bManaged):
	m_panel(nResId, snk_ref, *this),
	m_bManaged(bManaged),
	m_dwStyle(dwStyle)
{
}

CPanelBase::~CPanelBase(void)
{
}

////////////////////////////////////////////////////////////////////////////

HRESULT    CPanelBase::Create(const HWND hParent, const RECT& rcArea, const bool bVisible)
{
	if (!::IsWindow(hParent))
		return OLE_E_INVALIDHWND;

	if (m_panel.IsWindow())
		return HRESULT_FROM_WIN32(ERROR_ALREADY_EXISTS);

	m_panel.Create(hParent);

	if (!m_panel.IsWindow())
		return HRESULT_FROM_WIN32(::GetLastError());

	RECT rc_ = {0};
	m_panel.GetWindowRect(&rc_);
	::MapWindowPoints(HWND_DESKTOP, hParent, (LPPOINT)&rc_, 0x2);

	if (false){}
	else if (0 != (ePanelStyle::eAlignToRight & m_dwStyle))
	{
		const INT w_    = (rc_.right - rc_.left);
		const INT nLeft = rcArea.right - w_;
		const INT nTop  = rcArea.top;
		m_panel.SetWindowPos(NULL, nLeft, nTop, 0, 0, SWP_NOZORDER|SWP_NOACTIVATE|SWP_NOSIZE);
	}
	else if (0 != (ePanelStyle::eAlignToLeft & m_dwStyle))
	{
		const INT nLeft = rcArea.left;
		const INT nTop  = rcArea.top;
		m_panel.SetWindowPos(NULL, nLeft, nTop, 0, 0, SWP_NOZORDER|SWP_NOACTIVATE|SWP_NOSIZE);
	}
	else if (0 != (ePanelStyle::eUseOriginSize & m_dwStyle))
	{
		const INT nLeft = rcArea.left + ((rcArea.right - rcArea.left) - (rc_.right - rc_.left)) / 2;
		const INT nTop  = rcArea.top  + ((rcArea.bottom - rcArea.top) - (rc_.bottom - rc_.top)) / 2;
		m_panel.SetWindowPos(NULL, nLeft, nTop, 0, 0, SWP_NOZORDER|SWP_NOACTIVATE|SWP_NOSIZE);
	}
	else if (0 != (ePanelStyle::eAlignToCenter & m_dwStyle))
	{
		const INT nLeft = rcArea.left + ((rcArea.right - rcArea.left) - (rc_.right - rc_.left)) / 2;
		::OffsetRect(&rc_, nLeft, 0);
		rc_.top = rcArea.top;
		rc_.bottom = rcArea.bottom;
		m_panel.SetWindowPos(NULL, &rc_, SWP_NOZORDER|SWP_NOACTIVATE);
	}
	else
	{
		m_panel.MoveWindow(&rcArea);
	}
	if (bVisible)
		m_panel.ShowWindow(SW_SHOW);
	else
		m_panel.ShowWindow(SW_HIDE);
	return S_OK;
}

HRESULT    CPanelBase::Destroy(void)
{
	if (!m_panel.IsWindow())
		return S_FALSE;
	m_panel.DestroyWindow();
	if (NULL != m_panel.m_hWnd)
	{
		HRESULT hr_ = HRESULT_FROM_WIN32(::GetLastError());
		ATLASSERT(FALSE);
		return  hr_;
	}
	return S_OK;
}

const HWND CPanelBase::GetControl_Safe(const UINT nCtrlId) const
{
	if (!m_panel.IsWindow())
		return NULL;
	::ATL::CWindow ctrl_ = m_panel.GetDlgItem((INT)nCtrlId);
	return ctrl_;
}

const 
CWindow&   CPanelBase::GetWindow_Ref(void) const
{
	return m_panel;
}

CWindow&   CPanelBase::GetWindow_Ref(void)
{
	return m_panel;
}

bool       CPanelBase::HasStyle(const DWORD dwStyle) const
{
	return (0 != (dwStyle & m_dwStyle));
}

HRESULT    CPanelBase::Hide(void)
{
	if (!m_panel.IsWindowVisible())
		return S_FALSE;
	m_panel.ShowWindow(SW_HIDE);
	return S_OK;
}

bool       CPanelBase::IsManaged(void) const
{
	return m_bManaged;
}

bool       CPanelBase::IsValid(void)const
{
	return !!m_panel.IsWindow();
}

HRESULT    CPanelBase::Refresh(const bool bWithAsyncOpt, const bool bWithChildrenIncludeOpt)
{
	TRACE_FUNC();
	if (!m_panel.IsWindow() || !m_panel.IsWindowVisible())
	{
		TRACE_ERROR(_T("_wnd_not_exists_or_invisible"));
		return S_FALSE;
	}
	if (true == bWithAsyncOpt)
	{
		TRACE_INFO(_T("_ref_in_async_mode"));
		m_panel.Invalidate(TRUE);
	}
	else
	{
		if (true == bWithChildrenIncludeOpt)
		{
			TRACE_INFO(_T("_ref_in_sync_no_clip"));
			m_panel.RedrawWindow(0, 0, RDW_ERASE|RDW_INVALIDATE|RDW_ERASENOW|RDW_ALLCHILDREN);
		}
		else
		{
			TRACE_INFO(_T(""));
			m_panel.RedrawWindow(0, 0, RDW_ERASE|RDW_INVALIDATE|RDW_ERASENOW|RDW_NOCHILDREN);
		}
	}
	return S_OK;
}

HRESULT    CPanelBase::Show(void)
{
	if (m_panel.IsWindowVisible())
		return S_FALSE;
	m_panel.SetWindowPos(HWND_TOP, 0, 0, 0, 0, SWP_SHOWWINDOW|SWP_NOMOVE|SWP_NOSIZE);
	return S_OK;
}

HRESULT    CPanelBase::SetBkgndRenderer(IRenderer* const pRenderer)
{
	m_panel.m_bkgnd_renderer = pRenderer;
	return S_OK;
}

HRESULT    CPanelBase::SetParentRenderer(IRenderer* const pRenderer)
{
	m_panel.m_parent_renderer = pRenderer;
	return S_OK;
}

HRESULT    CPanelBase::UpdateLayout(LPRECT const pRect)
{
	if (pRect && !::IsRectEmpty(pRect) && this->IsValid())
	{
		RECT rc_ = *pRect;
		m_panel.SetWindowPos(NULL, &rc_, SWP_NOZORDER|SWP_NOACTIVATE);
	}
	return S_OK;
}