#ifndef _UIXFRAMEIMAGEHEADER_H_ADF4AA40_1A25_43f7_9ABF_B02BC5319F25_INCLUDED
#define _UIXFRAMEIMAGEHEADER_H_ADF4AA40_1A25_43f7_9ABF_B02BC5319F25_INCLUDED
/*
	Created by Tech_dog (VToropov) on 24-Mar-2014 at 3:17:13am, GMT+4, Saint-Petersburg, Monday;
	This is UIX Frame Library Image Header class declaration file.
*/
#include "UIX_GdiProvider.h"

namespace ex_ui { namespace frames
{
	class CImageHeader
	{
	private:
		class CImageHeaderWnd :
			public ::ATL::CWindowImpl<CImageHeaderWnd>
		{
			friend class CImageHeader;
		private:
			Gdiplus::Bitmap* m_pImage;        // header image pointer
			HRESULT          m_hResult;       // initialisation result
			mutable SIZE     m_szHeader;      // header size
		public:
			CImageHeaderWnd(const ATL::_U_STRINGorID RID);
			~CImageHeaderWnd(void);
		public:
			BEGIN_MSG_MAP(CImageHeaderWnd)
				MESSAGE_HANDLER(WM_CREATE    , OnCreate )
				MESSAGE_HANDLER(WM_DESTROY   , OnDestroy)
				MESSAGE_HANDLER(WM_ERASEBKGND, OnErase  )
				MESSAGE_HANDLER(WM_PAINT     , OnPaint  )
			END_MSG_MAP()
		private:
			LRESULT OnCreate (UINT, WPARAM, LPARAM, BOOL&);
			LRESULT OnDestroy(UINT, WPARAM, LPARAM, BOOL&);
			LRESULT OnErase  (UINT, WPARAM, LPARAM, BOOL&);
			LRESULT OnPaint  (UINT, WPARAM, LPARAM, BOOL&);
		};
	private:
		CImageHeaderWnd     m_wnd;
	public:
		CImageHeader(const ATL::_U_STRINGorID RID);
		~CImageHeader(void);
	public:
		HRESULT             Create(const HWND hParent);
		HRESULT             Destroy(void);
		const SIZE&         GetSize(void) const;
		::ATL::CWindow&     GetWindow_Ref(void);
	private:
		CImageHeader(const CImageHeader&);
		CImageHeader& operator= (const CImageHeader&);
	};
}}

#endif/*_UIXFRAMEIMAGEHEADER_H_ADF4AA40_1A25_43f7_9ABF_B02BC5319F25_INCLUDED*/