#ifndef _UIXFRAMEPANELBASE_H_3F309883_73D7_48a1_BE5C_B4E1354451E7_INCLUDED
#define _UIXFRAMEPANELBASE_H_3F309883_73D7_48a1_BE5C_B4E1354451E7_INCLUDED
/*
	Created by Tech_dog (VToropov) on 9-Feb-2015 at 8:29:23pm, GMT+3, Taganrog, Monday;
	This is UIX Frame library panel base class declaration file.
*/
#include "UIX_CommonDrawDefs.h"

namespace ex_ui { namespace frames
{
	interface IMessageHandler
	{
		virtual LRESULT MessageHandler_OnMessage(UINT uMsg, WPARAM wParam, LPARAM lParam, BOOL& bHandled) PURE;
	};

	class CMessageHandlerDefImpl:
		public IMessageHandler
	{
	public:
		CMessageHandlerDefImpl(void);
		~CMessageHandlerDefImpl(void);
	private:
		virtual LRESULT MessageHandler_OnMessage(UINT, WPARAM, LPARAM, BOOL&) override sealed;
	};

	class ePanelStyle
	{
	public:
		enum _e {
			eNone = 0x0,
			eUseOriginSize = 0x01,
			eAlignToCenter = 0x02,
			eAlignToLeft   = 0x04,
			eAlignToRight  = 0x08,
		};
	};

	using ex_ui::draw::defs::IRenderer;

	class CPanelBase
	{
	private:
		class CPanelBaseWnd:
			public  ::ATL::CDialogImpl<CPanelBaseWnd>
		{
			typedef ::ATL::CDialogImpl<CPanelBaseWnd> TWindow;
			friend class CPanelBase;
		protected:
			IRenderer*         m_bkgnd_renderer;
			IRenderer*         m_parent_renderer;
		private:
			IMessageHandler&   m_sink_ref;
			CPanelBase&        m_pane_ref;
		public:
			UINT IDD;
		public:
			BEGIN_MSG_MAP(CPanelBaseWnd)
				lResult = m_sink_ref.MessageHandler_OnMessage(uMsg, wParam, lParam, bHandled);
				if (bHandled)
					return TRUE;
				MESSAGE_HANDLER(WM_DESTROY          ,  OnDestroy )
				MESSAGE_HANDLER(WM_ERASEBKGND       ,  OnErase   )
				MESSAGE_HANDLER(WM_INITDIALOG       ,  OnCreate  )
				MESSAGE_HANDLER(WM_WINDOWPOSCHANGED ,  OnShowHide)
			END_MSG_MAP()
		private:
			virtual LRESULT OnCreate  (UINT uMsg, WPARAM wParam, LPARAM lParam, BOOL& bHandled);
			virtual LRESULT OnDestroy (UINT uMsg, WPARAM wParam, LPARAM lParam, BOOL& bHandled);
			virtual LRESULT OnErase   (UINT uMsg, WPARAM wParam, LPARAM lParam, BOOL& bHandled);
			virtual LRESULT OnShowHide(UINT uMsg, WPARAM wParam, LPARAM lParam, BOOL& bHandled);
		public:
			CPanelBaseWnd(const UINT nResId, IMessageHandler&, CPanelBase&);
			virtual ~CPanelBaseWnd(void);
		};
	private:
		CPanelBaseWnd          m_panel;
		bool                   m_bManaged;
		DWORD                  m_dwStyle;
	public:
		CPanelBase(const UINT nResId, IMessageHandler&, const DWORD dwStyle = ePanelStyle::eNone, const bool bManaged = false);
		virtual ~CPanelBase(void);
	public:
		virtual HRESULT        Create(const HWND hParent, const RECT& rcArea, const bool bVisible);
		virtual HRESULT        Destroy(void);
		virtual bool           HasStyle(const DWORD) const;
		virtual HRESULT        Hide(void);
		virtual bool           IsValid(void)const;
		virtual HRESULT        Refresh(const bool bWithAsyncOpt = true, const bool bWithChildrenIncludeOpt = false);
		virtual HRESULT        Show(void);
		virtual HRESULT        SetBkgndRenderer(IRenderer* const);
		virtual HRESULT        SetParentRenderer(IRenderer* const);
		virtual HRESULT        UpdateLayout(LPRECT const = NULL);
	public:
		const HWND             GetControl_Safe(const UINT nCtrlId) const;
		const ::ATL::CWindow&  GetWindow_Ref(void) const;
		::ATL::CWindow&        GetWindow_Ref(void);
		bool                   IsManaged(void) const;
	private:
		CPanelBase(const CPanelBase&);
		CPanelBase& operator= (const CPanelBase&);
	};
}}

#endif/*_UIXFRAMEPANELBASE_H_3F309883_73D7_48a1_BE5C_B4E1354451E7_INCLUDED*/