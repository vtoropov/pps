#ifndef _SHAREDABSTRACTIMAGEPROCESSOR_H_D3E623DB_839B_4cf0_80EE_21A58FF62812_INCLUDED
#define _SHAREDABSTRACTIMAGEPROCESSOR_H_D3E623DB_839B_4cf0_80EE_21A58FF62812_INCLUDED
/*
	Created by Tech_dog (VToropov) on 22-Mar-2014 at 7:52:24am, GMT+4, Rostov-on-Don Region,
	Rail Road Train #43, the Coatch #6, the place #1; Saturday;
	This is Shared Abstract Image Processor class declaration file.
*/
#include "ImageProcessingCommonDefs.h"

namespace shared { namespace recognition
{
	class eResultCodeType
	{
	public:
		enum _enum {
			eUndefined   = 0,
			eCancel      = 1,
			eTimeout     = 2,
			eSuccess     = 3,
		};
	};

	INT GetResultCode(const eResultCodeType::_enum);

	INT GetCancelResultCode(void);
	INT GetSuccessResultCode(void);    // returns a successful result code for a client
	INT GetTimeoutResultCode(void);

	interface IDeviceIdentifierEnumerator
	{
		virtual LPCTSTR                GetBSPOf(const INT nIndex)                              CONST PURE;
		virtual INT                    GetCount(void)                                          CONST PURE;
		virtual LPCTSTR                GetDefault(void)                                        CONST PURE;
		virtual const IError&          GetLastError_Ref(void)                                  CONST PURE;
		virtual HRESULT                Initialize(void)                                              PURE;
	};

	interface IInitializer
	{
		virtual const IDeviceIdentifierEnumerator&   GetDeviceIdentifierEnum_Ref(void)         CONST PURE;
		virtual INT_PTR                GetHandle(void)                                         CONST PURE;
		virtual const IError&          GetLastError_Ref(void)                                  CONST PURE;
		virtual eProcessorType         GetProcessorType(void)                                  CONST PURE;
		virtual HRESULT                Initialize(void)                                              PURE;
		virtual bool                   IsInitialized(void)                                     CONST PURE;
		virtual HRESULT                Terminate(void)                                               PURE;
	};

	HRESULT   CreateInitializer(const eProcessorType, IInitializer*&);
	HRESULT   DestroyInitializer_Safe(IInitializer*&);

	class CInitializerPtr
	{
	private:
		IInitializer*                  m_pInitializer;
		HRESULT                        m_hResult;
	public:
		CInitializerPtr(const eProcessorType);
		~CInitializerPtr(void);
	public:
		HRESULT                        GetLastResult(void) const;
		const IInitializer&            GetObject_Ref(void) const;  // returns object reference, if the object pointer is not initialized, returns a reference to not initialized object stub
		IInitializer&                  GetObject_Ref(void);        // returns object reference, if the object pointer is not initialized, returns a reference to not initialized object stub
		bool                           IsObjectValid(void) const;
	private:
		CInitializerPtr(const CInitializerPtr&);
		CInitializerPtr& operator= (const CInitializerPtr&);
	};

	enum ePersistenceType
	{
		eServerApplication = 0, // default
		eFileSystem        = 1,
	};

	enum ePropertyIdentifier
	{
		ePersistenceType   = 0x00010000,
		ePersistenceFolder = 0x00020000,
	};
	
	interface IImageProcessor
	{
		virtual HRESULT        Cancel(void)                                      PURE;
		virtual HRESULT        CreateEnrollmentTemplate(_variant_t& vTemplate)   PURE;
		virtual HRESULT        CreateVerificationTemplate(_variant_t& vTemplate) PURE;
		virtual HRESULT        Enroll(_variant_t& vTemplate)                     PURE; // takes an enroll template 3 times and returns the best one
		virtual const IError&  GetLastError_Ref(void)                      CONST PURE;
		virtual eProcessorType GetProcessorType(void)                      CONST PURE;
		virtual HRESULT        IsAlive(void)                               CONST PURE;
		virtual HRESULT        VerifyMatch(const _variant_t& verifyTempl,  const _variant_t& enrollTempl) = 0;
	};

	HRESULT  CreateImageProcessor(const IInitializer&, IImageProcessor*&);
	HRESULT  DestroyImageProcessor_Safe(IImageProcessor*&);

	class CImageProcessorPtr
	{
	private:
		IImageProcessor*               m_pImageProc;
		HRESULT                        m_hResult;
	public:
		CImageProcessorPtr(const IInitializer&);
		~CImageProcessorPtr(void);
	public:
		HRESULT                        GetLastResult(void) const;
		const IImageProcessor&         GetObject_Ref(void) const;  // returns object reference, if the object pointer is not initialized, returns a reference to not initialized object stub
		IImageProcessor&               GetObject_Ref(void);        // returns object reference, if the object pointer is not initialized, returns a reference to not initialized object stub
		bool                           IsObjectValid(void) const;
	private:
		CImageProcessorPtr(const CImageProcessorPtr&);
		CImageProcessorPtr& operator= (const CImageProcessorPtr&);
	};
}}

#endif/*_SHAREDABSTRACTIMAGEPROCESSOR_H_D3E623DB_839B_4cf0_80EE_21A58FF62812_INCLUDED*/