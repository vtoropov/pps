/*
	Created by Tech_dog (VToropov) on 22-Mar-2014 at 3:18:55am, GMT+4, Rostov-on-Don Region, Saturday;
	This is Shared Abstract Image Processing Common implementing file.
*/
#include "StdAfx.h"
#include "ImageProcessingCommonDefs.h"
#include "ImageProcessor.h"
#include "Shared_GenericSyncObject.h"

using namespace shared;
using namespace shared::recognition;

////////////////////////////////////////////////////////////////////////////

namespace shared { namespace recognition
{
	HRESULT   ConvertReturnResultToHresult(const INT nCode)
	{
		HRESULT hr__ = E_UNEXPECTED;
		switch (nCode)
		{
		case NULL:
		default:;
		}
		return  hr__;
	}
}}

////////////////////////////////////////////////////////////////////////////

CDeviceError::CDeviceError(void) : m_dev_code(ERROR_SUCCESS)
{
	CSysError::Reset();
}

CDeviceError::CDeviceError(LPCTSTR pError, const INT nDevCode, const HRESULT hr_): m_dev_code(nDevCode)
{
	CSysError::SetState(hr_, pError);
}

CDeviceError::~CDeviceError(void)
{
}

////////////////////////////////////////////////////////////////////////////

INT           CDeviceError::GetDeviceCode(void) const
{
	return m_dev_code;
}

LPCTSTR       CDeviceError::GetDescription(void) const
{
	return CSysError::GetDescription(); 
}

HRESULT       CDeviceError::GetHresult(void) const
{
	return CSysError::GetHresult();
}

bool          CDeviceError::IsFailure(void) const
{
	return (S_OK != this->GetHresult() || ERROR_SUCCESS != m_dev_code);
}

////////////////////////////////////////////////////////////////////////////

void          CDeviceError::SetDevCode(const INT nCode)
{
	m_dev_code = nCode;
}

////////////////////////////////////////////////////////////////////////////

using namespace shared;
using namespace shared::recognition;

////////////////////////////////////////////////////////////////////////////

namespace shared { namespace recognition { namespace details
{
	using shared::lite::runnable::CGenericSyncObject;

	const CGenericSyncObject& FailedModule_GetLockerRef(void)
	{
		static CGenericSyncObject crt__72ED5835_EE66_4a6d_BC42_E79C229DFFEC;
		return crt__72ED5835_EE66_4a6d_BC42_E79C229DFFEC;
	}
}}}

#define SAFE_LOCK_MODULE() SAFE_LOCK(details::FailedModule_GetLockerRef());
////////////////////////////////////////////////////////////////////////////

CFailedModule::CFailedModule(void)
{
	Clear();
}

CFailedModule::~CFailedModule(void)
{
}

////////////////////////////////////////////////////////////////////////////

void            CFailedModule::Clear(void)
{
	SAFE_LOCK_MODULE();
	m_module = eImageProcessorModule::eNone;
	m_error.Reset();
}

CONST CDeviceError&  CFailedModule::Error(void)const
{
	return m_error;
}

CDeviceError&        CFailedModule::Error(void)
{
	return m_error;
}

eImageProcessorModule::_enum  CFailedModule::Module(void) const
{
	SAFE_LOCK_MODULE();
	return m_module;
}

void            CFailedModule::Module(const eImageProcessorModule::_enum eModule)
{
	SAFE_LOCK_MODULE();
	m_module = eModule;
}

CAtlString      CFailedModule::Name(void)const
{
	SAFE_LOCK_MODULE();
	::ATL::CAtlString cs_name(_T("#n/a"));
	switch (m_module)
	{
	case eImageProcessorModule::eEventModule:    cs_name = _T("Event Module");          break;
	case eImageProcessorModule::eImageStorage:   cs_name = _T("Image Storage");         break;
	case eImageProcessorModule::eInitializer:    cs_name = _T("Processor Initializer"); break;
	case eImageProcessorModule::eMessageSystem:  cs_name = _T("Message System");        break;
	case eImageProcessorModule::eProcessor:      cs_name = _T("Image Processor");       break;
	}
	return cs_name;
}

void            CFailedModule::Set(const IError& err_ref, const eImageProcessorModule::_enum eModule)
{
	SAFE_LOCK_MODULE();
	this->Clear();
	m_module   = eModule;
	m_error.SetState(err_ref.GetHresult(), err_ref.GetDescription());
	m_error.SetDevCode(err_ref.GetDeviceCode());
}

void            CFailedModule::Set(const IImageProcessor& obj_ref)
{
	this->Clear();
	this->Set(obj_ref.GetLastError_Ref(), eImageProcessorModule::eProcessor);
}

void            CFailedModule::Set(const IInitializer& obj_ref)
{
	this->Clear();
	this->Set(obj_ref.GetLastError_Ref(), eImageProcessorModule::eInitializer);
}

CAtlString      CFailedModule::ToString(void)const
{
	::ATL::CAtlString cs_pattern;
	cs_pattern.Format(_T("module=%s, device code=%d, desc=%s, res=0x%x"),
						this->Name(),
						this->m_error.GetDeviceCode(),
						this->m_error.GetDescription(),
						this->m_error.GetHresult());
	return cs_pattern;
}

void            CFailedModule::Update(const IImageProcessor& obj_ref)
{
	switch (m_module)
	{
	case eImageProcessorModule::eProcessor:       this->Set(obj_ref);                 break;
	}
}