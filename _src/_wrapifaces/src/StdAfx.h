#ifndef _SHAREDIMAGEPROCESSINGLIB_H_95DF0CB6_122D_43cb_B50B_48BF7B15500E_INCLUDED
#define _SHAREDIMAGEPROCESSINGLIB_H_95DF0CB6_122D_43cb_B50B_48BF7B15500E_INCLUDED
/*
	Created by Tech_dog (VToropov) on 18-Mar-2014 at 10:21:43pm, GMT+4, Taganrog, Tuesday;
	This is Platinum Payroll Systems Image Processing Library Precompiled Header definition file.
*/

#define WIN32_LEAN_AND_MEAN

#ifndef  WINVER
#define  WINVER       0x0600 // this is for use Windows Vista (or later) specific feature(s)
#endif   WINVER

#ifndef _WIN32_WINNT
#define _WIN32_WINNT  0x0600 // this is for use Windows Vista (or later) specific feature(s)
#endif  _WIN32_WINNT

#ifndef _WIN32_IE
#define _WIN32_IE     0x0700 // this is for use IE 7 (or later) specific feature(s)
#endif  _WIN32_IE

#include <atlbase.h>
#include <atlwin.h>
#include <atlstr.h>
#include <comdef.h>
#include <new>
#include <map>
#include <set>
#include <vector>
#include <typeinfo>
#include <deque>

#include <atlsafe.h>
#include <atlcomtime.h>
#include <comutil.h>

#include "Shared_EventLogger.h"

#if defined(_DEBUG)
	#pragma comment(lib,"HitachiWrapLayer_V9D.lib")
#else
	#pragma comment(lib,"HitachiWrapLayer_V9.lib")
#endif

#pragma warning(disable: 4481) // nonstandard extension used: override specifier 'override'

#endif/*_SHAREDIMAGEPROCESSINGLIB_H_95DF0CB6_122D_43cb_B50B_48BF7B15500E_INCLUDED*/