/*
	Created by Tech_dog (VToropov) on 21-Mar-2014 at 9:21:58pm, GMT+4, Taganrog, Friday;
	This is Shared Recognition Hitachi BioSDK Client Wrapper class implementation file.
*/
#include "StdAfx.h"
#include "Hit_ClientWrap.h"
#include "Hit_Initializer.h"
#include "Hit_ImageProcessor.h"

using namespace shared::recognition;
using namespace shared::recognition::client;
using namespace shared::recognition::client::Hitachi;

////////////////////////////////////////////////////////////////////////////

namespace shared { namespace recognition { namespace client { namespace Hitachi
{
	using shared::recognition::IImageProcessor;
	using shared::recognition::eResultCodeType;

	INT GetResultCode(const eResultCodeType::_enum eType)
	{
		switch (eType)
		{
		case eResultCodeType::eCancel:      return (INT)BioAPI_CANCEL;
		case eResultCodeType::eTimeout:     return (INT)BioAPIERR_TIMEOUT_EXPIRED;
		case eResultCodeType::eSuccess:     return (INT)BioAPI_OK;
		}
		return (INT)(DWORD)0xffffffff;
	}

	HRESULT   CreateInitializer(IInitializer*& ptr_ref)
	{
		TRACE_FUNC();
		if (ptr_ref)
			return HRESULT_FROM_WIN32(ERROR_OBJECT_ALREADY_EXISTS);
		try
		{
			ptr_ref = new CInitializer();
		}
		catch(::std::bad_alloc&)
		{
			ATLASSERT(0);
			return E_OUTOFMEMORY;
		}
		return S_OK;
	}

	HRESULT   DestroyInitializer_Safe(shared::recognition::IInitializer*& ptr_ref)
	{
		TRACE_FUNC();
		if (!ptr_ref)
			return S_FALSE;
		CInitializer* pInitializer = NULL;
		try
		{
			pInitializer = dynamic_cast<CInitializer*>(ptr_ref);
			delete pInitializer; pInitializer = NULL; ptr_ref = NULL;
		}
		catch(::std::bad_cast&)
		{
			ATLASSERT(0);
			return DISP_E_TYPEMISMATCH;
		}
		catch (...)
		{
			ATLASSERT(0);
			return HRESULT_FROM_WIN32(ERROR_INVALID_ADDRESS);
		}
		return S_OK;
	}

	HRESULT   CreateImageProcessor(const IInitializer& init_ref, IImageProcessor*& ptr_ref)
	{
		TRACE_FUNC();
		if (ptr_ref)
		{
			ATLASSERT(0);
			return HRESULT_FROM_WIN32(ERROR_OBJECT_ALREADY_EXISTS);
		}
		try
		{
			ptr_ref = new CImageProcessor(init_ref);
		}
		catch(::std::bad_alloc&)
		{
			ATLASSERT(0);
			return E_OUTOFMEMORY;
		}
		return S_OK;
	}

	HRESULT   DestroyImageProcessor_Safe(IImageProcessor*& ptr_ref)
	{
		TRACE_FUNC();

		if (!ptr_ref)
			return S_FALSE;
		CImageProcessor*   pProcessor = NULL;
		try
		{
			pProcessor = dynamic_cast<CImageProcessor*>(ptr_ref);
			delete pProcessor; pProcessor = NULL; ptr_ref = NULL;
		}
		catch(::std::bad_cast&)
		{
			ATLASSERT(0);
			return DISP_E_TYPEMISMATCH;
		}
		catch(...)
		{
			ATLASSERT(0);
			return HRESULT_FROM_WIN32(ERROR_INVALID_ADDRESS);
		}
		return S_OK;
	}
}}}}

////////////////////////////////////////////////////////////////////////////

CImpersonate::CImpersonate(void)
{
	::memset((void*)m_UUID, 0, sizeof(BYTE) * CImpersonateParam::eBufferLen);
}

CImpersonate::CImpersonate(const PBYTE pBuffer, const INT nSize)
{
	if (pBuffer && CImpersonateParam::eBufferLen == nSize)
	{
		errno_t err = ::memcpy_s((void*)m_UUID, CImpersonateParam::eBufferLen, (void*)pBuffer, CImpersonateParam::eBufferLen);
		if (err)
		{
			ATLASSERT(FALSE==TRUE);
		}
	}
}

CImpersonate::~CImpersonate(void)
{
}

////////////////////////////////////////////////////////////////////////////

namespace shared { namespace recognition { namespace client { namespace Hitachi { namespace details
{
	static LPCTSTR Impersonate_UUID_Pattern = _T("{%02x%02x%02x%02x-%02x%02x-%02x%02x-%02x%02x-%02x%02x%02x%02x%02x%02x}");
}}}}}

////////////////////////////////////////////////////////////////////////////

PBYTE const  CImpersonate::GetUUID_Ptr(void) const
{
	return (PBYTE)&m_UUID[0];
}

HRESULT      CImpersonate::StringToUUID(LPCTSTR pUUID)
{
	if (!pUUID || ::_tcslen(pUUID) < 1)
		return E_INVALIDARG;
	::memset((void*)m_UUID, 0, sizeof(BYTE) * CImpersonateParam::eBufferLen);
	
	GUID guid = {0};
	::ATL::CComBSTR bsUUID(pUUID);

	HRESULT hr__ = ::IIDFromString(bsUUID, &guid);
	if (S_OK != hr__)
		return  hr__;

	m_UUID[0x0] = (BYTE)((DWORD(guid.Data1) & 0xff000000) >> 24);
	m_UUID[0x1] = (BYTE)((DWORD(guid.Data1) & 0x00ff0000) >> 16);
	m_UUID[0x2] = (BYTE)((DWORD(guid.Data1) & 0x0000ff00) >>  8);
	m_UUID[0x3] = (BYTE)((DWORD(guid.Data1) & 0x000000ff) >>  0);
	m_UUID[0x4] = (BYTE)((WORD(guid.Data2) & 0xff00) >> 8);
	m_UUID[0x5] = (BYTE)((WORD(guid.Data2) & 0x00ff) >> 0);
	m_UUID[0x6] = (BYTE)((WORD(guid.Data3) & 0xff00) >> 8);
	m_UUID[0x7] = (BYTE)((WORD(guid.Data3) & 0x00ff) >> 0);
	m_UUID[0x8] = (BYTE)guid.Data4[0];
	m_UUID[0x9] = (BYTE)guid.Data4[1];
	m_UUID[0xa] = (BYTE)guid.Data4[2];
	m_UUID[0xb] = (BYTE)guid.Data4[3];
	m_UUID[0xc] = (BYTE)guid.Data4[4];
	m_UUID[0xd] = (BYTE)guid.Data4[5];
	m_UUID[0xe] = (BYTE)guid.Data4[6];
	m_UUID[0xf] = (BYTE)guid.Data4[7];
	return S_OK;
}

LPCTSTR      CImpersonate::UUIDToString(void) const
{
	m_buffer.Format(details::Impersonate_UUID_Pattern, m_UUID[0x0],
											m_UUID[0x1],
											m_UUID[0x2],
											m_UUID[0x3],
											m_UUID[0x4],
											m_UUID[0x5],
											m_UUID[0x6],
											m_UUID[0x7],
											m_UUID[0x8],
											m_UUID[0x9],
											m_UUID[0xa],
											m_UUID[0xb],
											m_UUID[0xc],
											m_UUID[0xd],
											m_UUID[0xe],
											m_UUID[0xf]);
	return m_buffer.GetString();
}