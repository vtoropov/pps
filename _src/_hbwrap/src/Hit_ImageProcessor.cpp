/*
	Created by Tech_dog (VToropov) on 26-Mar-2014 at 7:44:39pm, GMT+4, Saint-Petersburg, Sunday;
	This is Shared Recognition Hitachi BioAPI SDK Image Processor class implementation file.
*/
#include "StdAfx.h"
#include "Hit_ImageProcessor.h"
#include "Hit_ImageRawData.h"
#include "Hit_BIRHolder.h"

using namespace shared;
using namespace shared::recognition;
using namespace shared::recognition::client;
using namespace shared::recognition::client::Hitachi;

////////////////////////////////////////////////////////////////////////////

CImageProcessor::CImageProcessor(const IInitializer& init_ref) : 
	m_init_ref(init_ref),
	m_cached(init_ref.GetProcessorType(), init_ref.GetHandle())
{
}

CImageProcessor::~CImageProcessor(void)
{
}

////////////////////////////////////////////////////////////////////////////

HRESULT        CImageProcessor::Cancel(void)
{
	TRACE_FUNC();
	BioAPI_HANDLE provider = (BioAPI_HANDLE)m_init_ref.GetHandle();
	BioAPI_RETURN ret__ = Zw_BioAPI_Cancel(provider);

	m_error.SetDeviceCode((INT)ret__);
	HRESULT hr_ = m_error.GetHresult();

	if (S_OK != hr_)
	{
		TRACE_ERR__a1(_T("The BioAPI_Capture() function returns the error code=0x%x"), hr_);
	}
	return hr_;
}

HRESULT        CImageProcessor::CreateEnrollmentTemplate(_variant_t& vTemplate)
{
	TRACE_FUNC();
	vTemplate;
	BioAPI_BIR_HANDLE pCaptured = NULL;
	BioAPI_HANDLE handle = (BioAPI_HANDLE)m_init_ref.GetHandle();
	BioAPI_RETURN ret__  = Zw_BioAPI_Capture(handle,
										BioAPI_PURPOSE_ENROLL,
										BioAPI_NO_SUBTYPE_AVAILABLE, 
										NULL,
										&pCaptured,
										-1,
										NULL);
	m_error.SetDeviceCode((INT)ret__);
	HRESULT hr__ = m_error.GetHresult();
	if (S_OK != hr__)
	{
		TRACE_ERR__a1(_T("The BioAPI_Capture() function returns the error code=0x%x"), hr__);
		return hr__;
	}
	BioAPI_BIR_HANDLE new_templ = NULL;
	BioAPI_INPUT_BIR input__ = {0};
	input__.Form = BioAPI_BIR_HANDLE_INPUT;
	input__.InputBIR.BIRinBSP = &pCaptured;
	
	ret__ = Zw_BioAPI_CreateTemplate(
				handle,
				&input__,
				NULL,
				NULL,
				&new_templ,
				NULL,
				NULL
			);
	m_error.SetDeviceCode((INT)ret__);
	hr__ = m_error.GetHresult();
	if (S_OK == hr__)
	{
		CImageRawData raw_data(m_init_ref.GetProcessorType(), m_init_ref.GetHandle());
		raw_data.SetData((INT_PTR)new_templ, 0);
		hr__ = raw_data.__ToVariant(vTemplate);
		if (S_OK != hr__)
		{
			TRACE_ERR__a1(_T("The error occurred while copying vein data to variant: code=0x%x"), hr__);
		}
	}
	else
	{
		TRACE_ERR__a1(_T("The BioAPI_CreateTemplate() function returns the error code=0x%x"), hr__);
	}
	Zw_BioAPI_FreeBIRHandle(handle, pCaptured);
	pCaptured = NULL;
	return hr__;
}

HRESULT        CImageProcessor::CreateVerificationTemplate(_variant_t& vTemplate)
{
	TRACE_FUNC();
	vTemplate;
	m_cached.Clear();
	BioAPI_BIR_HANDLE pCaptured = NULL;
	BioAPI_HANDLE handle = (BioAPI_HANDLE)m_init_ref.GetHandle();
	BioAPI_RETURN  ret__ = Zw_BioAPI_Capture(
								handle,
								BioAPI_PURPOSE_VERIFY,
								BioAPI_NO_SUBTYPE_AVAILABLE,
								NULL,
								&pCaptured,
								-1,
								NULL
							);
	m_error.SetDeviceCode((INT)ret__);
	HRESULT hr__ = m_error.GetHresult();
	if (S_OK != hr__)
	{
		TRACE_ERR__a1(_T("The BioAPI_Capture() function returns the error code=0x%x"), hr__);
		return  hr__;
	}
	BioAPI_INPUT_BIR input__ = {0};
	input__.Form = BioAPI_BIR_HANDLE_INPUT;
	input__.InputBIR.BIRinBSP = &pCaptured;
	BioAPI_BIR_HANDLE processed = NULL;

	ret__ = Zw_BioAPI_Process(handle, &input__, NULL, &processed);

	m_error.SetDeviceCode((INT)ret__);
	if (S_OK == m_error.GetHresult())
	{
		CImageRawData raw_data(m_init_ref.GetProcessorType(), m_init_ref.GetHandle());
		raw_data.SetData((INT_PTR)processed, 0);
		hr__ = raw_data.__ToVariant(vTemplate);
		if (S_OK != hr__)
		{
			TRACE_ERR__a1(_T("The error occurred while copying vein data to variant: code=0x%x"), hr__);
		}
	}
	else
	{
		TRACE_ERR__a1(_T("The BioAPI_Process() function returns the error code=0x%x"), hr__);
	}
	Zw_BioAPI_FreeBIRHandle(handle, pCaptured);
	pCaptured = NULL;
	return hr__;
}

HRESULT        CImageProcessor::Enroll(_variant_t& vTemplate)
{
	TRACE_FUNC();
	vTemplate;
	BioAPI_HANDLE handle = (BioAPI_HANDLE)m_init_ref.GetHandle();
	BioAPI_BIR_HANDLE new_templ = NULL;
	int32_t nTimeout = -1;

	BioAPI_RETURN ret_ = Zw_BioAPI_Enroll(
							handle,
							BioAPI_PURPOSE_ENROLL,
							BioAPI_NO_SUBTYPE_AVAILABLE,
							NULL,
							NULL,
							&new_templ,
							NULL,
							nTimeout,
							NULL,
							NULL
						);

	m_error.SetDeviceCode((INT)ret_);

	HRESULT hr_ = m_error.GetHresult();
	if (S_OK == m_error.GetHresult())
	{
		CImageRawData raw_data(m_init_ref.GetProcessorType(), m_init_ref.GetHandle());
		raw_data.SetData((INT_PTR)new_templ, 0);
		hr_ = raw_data.__ToVariant(vTemplate);
		if (S_OK != hr_)
		{
			TRACE_ERR__a1(_T("The error occurred while copying vein data to variant: code=0x%x"), hr_);
		}
	}
	else
	{
		TRACE_ERR__a1(_T("The BioAPI_Enroll() function returns the error code=0x%x"), hr_);
	}
	Zw_BioAPI_FreeBIRHandle(handle, new_templ);
	new_templ = NULL;
	return  hr_;
}

const IError&  CImageProcessor::GetLastError_Ref(void)const
{
	return m_error;
}

eProcessorType CImageProcessor::GetProcessorType(void)const
{
	return shared::recognition::eHibio;
}

HRESULT        CImageProcessor::IsAlive(void)const
{
	BioAPI_FRAMEWORK_SCHEMA schema_ = {0};
	const BioAPI_RETURN ret_ = Zw_BioAPI_GetFrameworkInfo(&schema_);
	m_error.SetDeviceCode(ret_);
	return m_error.GetHresult();
}

HRESULT        CImageProcessor::VerifyMatch(const _variant_t& verifyTempl,  const _variant_t& enrollTempl)
{
	verifyTempl;
	BioAPI_HANDLE provider_handle = (BioAPI_HANDLE)m_init_ref.GetHandle();
	CBIRHolder bir_proc/*(provider_handle)*/;
//	HRESULT hr__ = bir_proc.Create(m_cached.GetData());
	HRESULT hr__ = bir_proc.Create(verifyTempl);
	if (S_OK != hr__)
		return  hr__;
	BioAPI_INPUT_BIR ProcessedBIRInputData = {0};
	ProcessedBIRInputData.Form = BioAPI_FULLBIR_INPUT;
	ProcessedBIRInputData.InputBIR.BIR = &bir_proc.GetBir_Ref();

	CBIRHolder bir_enroll;
	hr__ = bir_enroll.Create(enrollTempl);
	if (S_OK != hr__)
		return  hr__;
	BioAPI_BIR& bir_ref = bir_enroll.GetBir_Ref();

	BioAPI_INPUT_BIR ReferenceTemplateInputData = {0};
	ReferenceTemplateInputData.Form = BioAPI_FULLBIR_INPUT;
	ReferenceTemplateInputData.InputBIR.BIR = &bir_ref;

	BioAPI_FMR FMRAchieved = {0};
	BioAPI_BOOL bResult = FALSE;
	BioAPI_RETURN ret__ = Zw_BioAPI_VerifyMatch(
							provider_handle,
							2072, // magic!
							&ProcessedBIRInputData,
							&ReferenceTemplateInputData,
							NULL,
							&bResult,
							&FMRAchieved,
							NULL
						); ret__;
	m_error.SetDeviceCode(ret__);
	hr__ = m_error.GetHresult();
	if (S_OK == hr__)
	{
		if (BioAPI_TRUE == bResult)
			hr__ = S_OK;
		else
			hr__ = S_FALSE;
	}
	return  hr__;
}