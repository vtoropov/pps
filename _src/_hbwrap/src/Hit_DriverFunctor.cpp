/*
	Created by Tech_dog(VToropov) on 13-Mar-2015 at 11:12:16pm, GMT+3, Taganrog, Friday;
	This is Hitachi Driver Dynamic Functor class implementation file.
*/
#include "StdAfx.h"
#include "Hit_DriverFunctor.h"

using namespace shared;
using namespace shared::recognition;
using namespace shared::recognition::client;
using namespace shared::recognition::client::Hitachi;

////////////////////////////////////////////////////////////////////////////

namespace shared { namespace recognition { namespace client { namespace Hitachi { namespace details
{
	typedef BioAPI_RETURN (BioAPI *PF_BioAPI_BSPAttach)(
			const BioAPI_UUID*,
			BioAPI_VERSION,
			const BioAPI_UNIT_LIST_ELEMENT*,
			uint32_t,
			BioAPI_HANDLE*
		);
	typedef BioAPI_RETURN (BioAPI *PF_BioAPI_BSPDetach)(BioAPI_HANDLE);
	typedef BioAPI_RETURN (BioAPI *PF_BioAPI_BSPLoad)(const BioAPI_UUID*, BioAPI_EventHandler, void*);
	typedef BioAPI_RETURN (BioAPI *PF_BioAPI_BSPUnload)(const BioAPI_UUID*, BioAPI_EventHandler, void*);
	typedef BioAPI_RETURN (BioAPI *PF_BioAPI_Capture)(
			BioAPI_HANDLE,
			BioAPI_BIR_PURPOSE,
			BioAPI_BIR_SUBTYPE,
			const BioAPI_BIR_BIOMETRIC_DATA_FORMAT*,
			BioAPI_BIR_HANDLE*,
			int32_t,
			BioAPI_BIR_HANDLE*
		);
	typedef BioAPI_RETURN (BioAPI *PF_BioAPI_Cancel)(BioAPI_HANDLE);
	typedef BioAPI_RETURN (BioAPI *PF_BioAPI_CreateTemplate)(
			BioAPI_HANDLE,
			const BioAPI_INPUT_BIR*,
			const BioAPI_INPUT_BIR*,
			const BioAPI_BIR_BIOMETRIC_DATA_FORMAT*,
			BioAPI_BIR_HANDLE*,
			const BioAPI_DATA*,
			BioAPI_UUID*
		);
	typedef BioAPI_RETURN (BioAPI *PF_BioAPI_Enroll)(
			BioAPI_HANDLE     ,
			BioAPI_BIR_PURPOSE,
			BioAPI_BIR_SUBTYPE,
			const BioAPI_BIR_BIOMETRIC_DATA_FORMAT*,
			const BioAPI_INPUT_BIR*,
			BioAPI_BIR_HANDLE*,
			const BioAPI_DATA*,
			int32_t Timeout   ,
			BioAPI_BIR_HANDLE*,
			BioAPI_UUID*
		);
	typedef BioAPI_RETURN (BioAPI *PF_BioAPI_Free)(void*);
	typedef BioAPI_RETURN (BioAPI *PF_BioAPI_FreeBIRHandle)(BioAPI_HANDLE, BioAPI_BIR_HANDLE);
	typedef BioAPI_RETURN (BioAPI *PF_BioAPI_EnumBSPs)(BioAPI_BSP_SCHEMA**, uint32_t*);
	typedef BioAPI_RETURN (BioAPI *PF_BioAPI_GetBIRFromHandle)(BioAPI_HANDLE, BioAPI_BIR_HANDLE, BioAPI_BIR*);
	typedef BioAPI_RETURN (BioAPI *PF_BioAPI_GetFrameworkInfo)(BioAPI_FRAMEWORK_SCHEMA*);
	typedef BioAPI_RETURN (BioAPI *PF_BioAPI_Init)(BioAPI_VERSION);
	typedef BioAPI_RETURN (BioAPI *PF_BioAPI_Process)(BioAPI_HANDLE, const BioAPI_INPUT_BIR*, const BioAPI_BIR_BIOMETRIC_DATA_FORMAT*, BioAPI_BIR_HANDLE*);
	typedef BioAPI_RETURN (BioAPI *PF_BioAPI_QueryUnits)(const BioAPI_UUID*, BioAPI_UNIT_SCHEMA**, uint32_t*);
	typedef BioAPI_RETURN (BioAPI *PF_BioAPI_Terminate)(void);
	typedef BioAPI_RETURN (BioAPI *PF_BioAPI_VerifyMatch)(
			BioAPI_HANDLE,
			BioAPI_FMR,
			const BioAPI_INPUT_BIR*,
			const BioAPI_INPUT_BIR*,
			BioAPI_BIR_HANDLE*,
			BioAPI_BOOL*,
			BioAPI_FMR*,
			BioAPI_DATA*
		);

	using shared::lite::common::CSysError;

	class CDriverFunctor
	{	
	private:
		HMODULE        m_driver;
		CSysError      m_error;
	public:
		CDriverFunctor(void);
		~CDriverFunctor(void);
	public:
		BioAPI_RETURN BioAPI_BSPAttach(
				const BioAPI_UUID*,
				BioAPI_VERSION,
				const BioAPI_UNIT_LIST_ELEMENT*,
				uint32_t,
				BioAPI_HANDLE*
			);
		BioAPI_RETURN BioAPI_BSPDetach(BioAPI_HANDLE);
		BioAPI_RETURN BioAPI_BSPLoad(const BioAPI_UUID*, BioAPI_EventHandler, void*);
		BioAPI_RETURN BioAPI_BSPUnload(const BioAPI_UUID*, BioAPI_EventHandler, void*);
		BioAPI_RETURN BioAPI_Cancel(BioAPI_HANDLE);
		BioAPI_RETURN BioAPI_Capture(
				BioAPI_HANDLE,
				BioAPI_BIR_PURPOSE,
				BioAPI_BIR_SUBTYPE,
				const BioAPI_BIR_BIOMETRIC_DATA_FORMAT*,
				BioAPI_BIR_HANDLE*,
				int32_t,
				BioAPI_BIR_HANDLE*
			);
		BioAPI_RETURN BioAPI_CreateTemplate(
				BioAPI_HANDLE,
				const BioAPI_INPUT_BIR*,
				const BioAPI_INPUT_BIR*,
				const BioAPI_BIR_BIOMETRIC_DATA_FORMAT*,
				BioAPI_BIR_HANDLE*,
				const BioAPI_DATA*,
				BioAPI_UUID*
			);
		BioAPI_RETURN BioAPI_Enroll(
				BioAPI_HANDLE,
				BioAPI_BIR_PURPOSE,
				BioAPI_BIR_SUBTYPE,
				const BioAPI_BIR_BIOMETRIC_DATA_FORMAT*,
				const BioAPI_INPUT_BIR*,
				BioAPI_BIR_HANDLE*,
				const BioAPI_DATA*,
				int32_t,
				BioAPI_BIR_HANDLE*,
				BioAPI_UUID*
			);
		BioAPI_RETURN BioAPI_EnumBSPs(BioAPI_BSP_SCHEMA**, uint32_t*);
		BioAPI_RETURN BioAPI_Free(void*);
		BioAPI_RETURN BioAPI_FreeBIRHandle(BioAPI_HANDLE, BioAPI_BIR_HANDLE);
		BioAPI_RETURN BioAPI_GetBIRFromHandle(BioAPI_HANDLE, BioAPI_BIR_HANDLE, BioAPI_BIR*);
		BioAPI_RETURN BioAPI_GetFrameworkInfo(BioAPI_FRAMEWORK_SCHEMA*);
		BioAPI_RETURN BioAPI_Init(BioAPI_VERSION);
		BioAPI_RETURN BioAPI_Process(BioAPI_HANDLE, const BioAPI_INPUT_BIR*, const BioAPI_BIR_BIOMETRIC_DATA_FORMAT*, BioAPI_BIR_HANDLE*);
		BioAPI_RETURN BioAPI_QueryUnits(const BioAPI_UUID*, BioAPI_UNIT_SCHEMA**, uint32_t*);
		BioAPI_RETURN BioAPI_Terminate(void);
		BioAPI_RETURN BioAPI_VerifyMatch(
				BioAPI_HANDLE,
				BioAPI_FMR,
				const BioAPI_INPUT_BIR*,
				const BioAPI_INPUT_BIR*,
				BioAPI_BIR_HANDLE*,
				BioAPI_BOOL*,
				BioAPI_FMR*,
				BioAPI_DATA*
			);
	public:
		TErrorRef      Error(void)const;
		HRESULT        Initialize(void);
		bool           IsInitialized(void)const;
		HRESULT        Terminate(void);
	private:
		CDriverFunctor(const CDriverFunctor&);
		CDriverFunctor& operator= (const CDriverFunctor&);
	};

	CDriverFunctor&  DriverFunctor_GetObject(void)
	{
		static CDriverFunctor func_;
		return func_;
	}
}}}}}

using namespace shared::recognition::client::Hitachi::details;
////////////////////////////////////////////////////////////////////////////

CDriverFunctor::CDriverFunctor(void) : m_driver(NULL)
{
}

CDriverFunctor::~CDriverFunctor(void)
{
	this->Terminate();
}

////////////////////////////////////////////////////////////////////////////

BioAPI_RETURN CDriverFunctor::BioAPI_BSPAttach(
				const BioAPI_UUID* a_0,
				BioAPI_VERSION     a_1,
				const BioAPI_UNIT_LIST_ELEMENT* a_2,
				uint32_t           a_3,
				BioAPI_HANDLE*     a_4
			)
{
	BioAPI_RETURN ret_ = BioAPIERR_FUNCTION_FAILED;

	details::PF_BioAPI_BSPAttach pf_ = reinterpret_cast<details::PF_BioAPI_BSPAttach>(::GetProcAddress(m_driver, "BioAPI_BSPAttach"));
	if (pf_)
		ret_ = pf_(a_0, a_1, a_2, a_3, a_4);
	return ret_;
}

BioAPI_RETURN CDriverFunctor::BioAPI_BSPDetach(BioAPI_HANDLE a_0)
{
	BioAPI_RETURN ret_ = BioAPIERR_FUNCTION_FAILED;

	details::PF_BioAPI_BSPDetach pf_ = reinterpret_cast<details::PF_BioAPI_BSPDetach>(::GetProcAddress(m_driver, "BioAPI_BSPDetach"));
	if (pf_)
		ret_ = pf_(a_0);
	return ret_;
}

BioAPI_RETURN CDriverFunctor::BioAPI_BSPLoad(const BioAPI_UUID* a_0, BioAPI_EventHandler a_1, void* a_2)
{
	BioAPI_RETURN ret_ = BioAPIERR_FUNCTION_FAILED;

	details::PF_BioAPI_BSPLoad pf_ = reinterpret_cast<details::PF_BioAPI_BSPLoad>(::GetProcAddress(m_driver, "BioAPI_BSPLoad"));
	if (pf_)
		ret_ = pf_(a_0, a_1, a_2);
	return ret_;
}

BioAPI_RETURN CDriverFunctor::BioAPI_BSPUnload(const BioAPI_UUID* a_0, BioAPI_EventHandler a_1, void* a_2)
{
	BioAPI_RETURN ret_ = BioAPIERR_FUNCTION_FAILED;

	details::PF_BioAPI_BSPUnload pf_ = reinterpret_cast<details::PF_BioAPI_BSPUnload>(::GetProcAddress(m_driver, "BioAPI_BSPUnload"));
	if (pf_)
		ret_ = pf_(a_0, a_1, a_2);
	return ret_;
}

BioAPI_RETURN CDriverFunctor::BioAPI_Cancel(BioAPI_HANDLE a_0)
{
	BioAPI_RETURN ret_ = BioAPIERR_FUNCTION_FAILED;

	details::PF_BioAPI_Cancel pf_ = reinterpret_cast<details::PF_BioAPI_Cancel>(::GetProcAddress(m_driver, "BioAPI_Cancel"));
	if (pf_)
		ret_ = pf_(a_0);
	return ret_;
}

BioAPI_RETURN CDriverFunctor::BioAPI_Capture(
		BioAPI_HANDLE       a_0,
		BioAPI_BIR_PURPOSE  a_1,
		BioAPI_BIR_SUBTYPE  a_2,
		const BioAPI_BIR_BIOMETRIC_DATA_FORMAT* a_3,
		BioAPI_BIR_HANDLE*  a_4,
		int32_t             a_5,
		BioAPI_BIR_HANDLE*  a_6
	)
{
	BioAPI_RETURN ret_ = BioAPIERR_FUNCTION_FAILED;

	details::PF_BioAPI_Capture pf_ = reinterpret_cast<details::PF_BioAPI_Capture>(::GetProcAddress(m_driver, "BioAPI_Capture"));
	if (pf_)
		ret_ = pf_(a_0, a_1, a_2, a_3, a_4, a_5, a_6);
	return ret_;
}

BioAPI_RETURN CDriverFunctor::BioAPI_CreateTemplate(
		BioAPI_HANDLE            a_0,
		const BioAPI_INPUT_BIR*  a_1,
		const BioAPI_INPUT_BIR*  a_2,
		const BioAPI_BIR_BIOMETRIC_DATA_FORMAT* a_3,
		BioAPI_BIR_HANDLE*       a_4,
		const BioAPI_DATA*       a_5,
		BioAPI_UUID*             a_6
	)
{
	BioAPI_RETURN ret_ = BioAPIERR_FUNCTION_FAILED;

	details::PF_BioAPI_CreateTemplate pf_ = reinterpret_cast<details::PF_BioAPI_CreateTemplate>(::GetProcAddress(m_driver, "BioAPI_CreateTemplate"));
	if (pf_)
		ret_ = pf_(a_0, a_1, a_2, a_3, a_4, a_5, a_6);
	return ret_;
}

BioAPI_RETURN CDriverFunctor::BioAPI_Enroll(
		BioAPI_HANDLE            a_0,
		BioAPI_BIR_PURPOSE       a_1,
		BioAPI_BIR_SUBTYPE       a_2,
		const BioAPI_BIR_BIOMETRIC_DATA_FORMAT* a_3,
		const BioAPI_INPUT_BIR*  a_4,
		BioAPI_BIR_HANDLE*       a_5,
		const BioAPI_DATA*       a_6,
		int32_t                  a_7,
		BioAPI_BIR_HANDLE*       a_8,
		BioAPI_UUID*             a_9
	)
{
	BioAPI_RETURN ret_ = BioAPIERR_FUNCTION_FAILED;

	details::PF_BioAPI_Enroll pf_ = reinterpret_cast<details::PF_BioAPI_Enroll>(::GetProcAddress(m_driver, "BioAPI_Enroll"));
	if (pf_)
		ret_ = pf_(a_0, a_1, a_2, a_3, a_4, a_5, a_6, a_7, a_8, a_9);
	return ret_;
}

BioAPI_RETURN CDriverFunctor::BioAPI_EnumBSPs(BioAPI_BSP_SCHEMA** a_0, uint32_t* a_1)
{
	BioAPI_RETURN ret_ = BioAPIERR_FUNCTION_FAILED;

	details::PF_BioAPI_EnumBSPs pf_ = reinterpret_cast<details::PF_BioAPI_EnumBSPs>(::GetProcAddress(m_driver, "BioAPI_EnumBSPs"));
	if (pf_)
		ret_ = pf_(a_0, a_1);
	return ret_;
}

BioAPI_RETURN CDriverFunctor::BioAPI_Free(void* a_0)
{
	BioAPI_RETURN ret_ = BioAPIERR_FUNCTION_FAILED;

	details::PF_BioAPI_Free pf_ = reinterpret_cast<details::PF_BioAPI_Free>(::GetProcAddress(m_driver, "BioAPI_Free"));
	if (pf_)
		ret_ = pf_(a_0);
	return ret_;
}

BioAPI_RETURN CDriverFunctor::BioAPI_FreeBIRHandle(BioAPI_HANDLE a_0, BioAPI_BIR_HANDLE a_1)
{
	BioAPI_RETURN ret_ = BioAPIERR_FUNCTION_FAILED;

	details::PF_BioAPI_FreeBIRHandle pf_ = reinterpret_cast<details::PF_BioAPI_FreeBIRHandle>(::GetProcAddress(m_driver, "BioAPI_FreeBIRHandle"));
	if (pf_)
		ret_ = pf_(a_0, a_1);
	return ret_;
}

BioAPI_RETURN CDriverFunctor::BioAPI_GetBIRFromHandle(BioAPI_HANDLE a_0, BioAPI_BIR_HANDLE a_1, BioAPI_BIR* a_2)
{
	BioAPI_RETURN ret_ = BioAPIERR_FUNCTION_FAILED;

	details::PF_BioAPI_GetBIRFromHandle pf_ = reinterpret_cast<details::PF_BioAPI_GetBIRFromHandle>(::GetProcAddress(m_driver, "BioAPI_GetBIRFromHandle"));
	if (pf_)
		ret_ = pf_(a_0, a_1, a_2);
	return ret_;
}

BioAPI_RETURN CDriverFunctor::BioAPI_GetFrameworkInfo(BioAPI_FRAMEWORK_SCHEMA* a_0)
{
	BioAPI_RETURN ret_ = BioAPIERR_FUNCTION_FAILED;

	details::PF_BioAPI_GetFrameworkInfo pf_ = reinterpret_cast<details::PF_BioAPI_GetFrameworkInfo>(::GetProcAddress(m_driver, "BioAPI_GetFrameworkInfo"));
	if (pf_)
		ret_ = pf_(a_0);
	return ret_;
}

BioAPI_RETURN CDriverFunctor::BioAPI_Init(BioAPI_VERSION a_0)
{
	BioAPI_RETURN ret_ = BioAPIERR_FUNCTION_FAILED;

	details::PF_BioAPI_Init pf_ = reinterpret_cast<details::PF_BioAPI_Init>(::GetProcAddress(m_driver, "BioAPI_Init"));
	if (pf_)
		ret_ = pf_(a_0);
	return ret_;
}

BioAPI_RETURN CDriverFunctor::BioAPI_Process(BioAPI_HANDLE a_0, const BioAPI_INPUT_BIR* a_1, const BioAPI_BIR_BIOMETRIC_DATA_FORMAT* a_2, BioAPI_BIR_HANDLE* a_3)
{
	BioAPI_RETURN ret_ = BioAPIERR_FUNCTION_FAILED;

	details::PF_BioAPI_Process pf_ = reinterpret_cast<details::PF_BioAPI_Process>(::GetProcAddress(m_driver, "BioAPI_Process"));
	if (pf_)
		ret_ = pf_(a_0, a_1, a_2, a_3);
	return ret_;
}

BioAPI_RETURN CDriverFunctor::BioAPI_QueryUnits(const BioAPI_UUID* a_0, BioAPI_UNIT_SCHEMA** a_1, uint32_t* a_2)
{
	BioAPI_RETURN ret_ = BioAPIERR_FUNCTION_FAILED;

	details::PF_BioAPI_QueryUnits pf_ = reinterpret_cast<details::PF_BioAPI_QueryUnits>(::GetProcAddress(m_driver, "BioAPI_QueryUnits"));
	if (pf_)
		ret_ = pf_(a_0, a_1, a_2);
	return ret_;
}

BioAPI_RETURN CDriverFunctor::BioAPI_Terminate(void)
{
	BioAPI_RETURN ret_ = BioAPIERR_FUNCTION_FAILED;

	details::PF_BioAPI_Terminate pf_ = reinterpret_cast<details::PF_BioAPI_Terminate>(::GetProcAddress(m_driver, "BioAPI_Terminate"));
	if (pf_)
		ret_ = pf_();
	return ret_;
}

BioAPI_RETURN CDriverFunctor::BioAPI_VerifyMatch(
		BioAPI_HANDLE       a_0,
		BioAPI_FMR          a_1,
		const BioAPI_INPUT_BIR* a_2,
		const BioAPI_INPUT_BIR* a_3,
		BioAPI_BIR_HANDLE*  a_4,
		BioAPI_BOOL*        a_5,
		BioAPI_FMR*         a_6,
		BioAPI_DATA*        a_7
	)
{
	BioAPI_RETURN ret_ = BioAPIERR_FUNCTION_FAILED;

	details::PF_BioAPI_VerifyMatch pf_ = reinterpret_cast<details::PF_BioAPI_VerifyMatch>(::GetProcAddress(m_driver, "BioAPI_VerifyMatch"));
	if (pf_)
		ret_ = pf_(a_0, a_1, a_2, a_3, a_4, a_5, a_6, a_7);
	return ret_;
}

////////////////////////////////////////////////////////////////////////////

TErrorRef    CDriverFunctor::Error(void)const
{
	return m_error;
}

HRESULT      CDriverFunctor::Initialize(void)
{
	if (this->IsInitialized())
		return (m_error = ERROR_ALREADY_INITIALIZED);
	m_driver = ::LoadLibrary(_T("HiBioAPI.dll"));
	if (!m_driver)
		return (m_error = ::GetLastError());
	else
		return (m_error = S_OK);
}

bool         CDriverFunctor::IsInitialized(void)const
{
	return (NULL != m_driver);
}

HRESULT      CDriverFunctor::Terminate(void)
{
	/*if (!this->IsInitialized())
		return (m_error = OLE_E_BLANK);*/

	const BOOL ret_ = ::FreeLibrary(m_driver); m_driver = NULL;
	return (m_error = ( ret_ ? S_OK : ::GetLastError()));
}

////////////////////////////////////////////////////////////////////////////

namespace shared { namespace recognition { namespace client { namespace Hitachi
{
	BioAPI_RETURN Zw_BioAPI_BSPAttach(
			const BioAPI_UUID* a_0,
			BioAPI_VERSION     a_1,
			const BioAPI_UNIT_LIST_ELEMENT* a_2,
			uint32_t           a_3,
			BioAPI_HANDLE*     a_4
		)
	{
		return details::DriverFunctor_GetObject().BioAPI_BSPAttach(a_0, a_1, a_2, a_3, a_4);
	}

	BioAPI_RETURN Zw_BioAPI_BSPDetach(BioAPI_HANDLE a_0)
	{
		return details::DriverFunctor_GetObject().BioAPI_BSPDetach(a_0);
	}

	BioAPI_RETURN Zw_BioAPI_BSPLoad(const BioAPI_UUID* a_0, BioAPI_EventHandler a_1, void* a_2)
	{
		return details::DriverFunctor_GetObject().BioAPI_BSPLoad(a_0, a_1, a_2);
	}

	BioAPI_RETURN Zw_BioAPI_BSPUnload(const BioAPI_UUID* a_0, BioAPI_EventHandler a_1, void* a_2)
	{
		return details::DriverFunctor_GetObject().BioAPI_BSPUnload(a_0, a_1, a_2);
	}

	BioAPI_RETURN Zw_BioAPI_Cancel(BioAPI_HANDLE a_0)
	{
		return details::DriverFunctor_GetObject().BioAPI_Cancel(a_0);
	}

	BioAPI_RETURN Zw_BioAPI_Capture(
			BioAPI_HANDLE       a_0,
			BioAPI_BIR_PURPOSE  a_1,
			BioAPI_BIR_SUBTYPE  a_2,
			const BioAPI_BIR_BIOMETRIC_DATA_FORMAT* a_3,
			BioAPI_BIR_HANDLE*  a_4,
			int32_t             a_5,
			BioAPI_BIR_HANDLE*  a_6
		)
	{
		return details::DriverFunctor_GetObject().BioAPI_Capture(a_0, a_1, a_2, a_3, a_4, a_5, a_6);
	}

	BioAPI_RETURN Zw_BioAPI_CreateTemplate(
			BioAPI_HANDLE           a_0,
			const BioAPI_INPUT_BIR* a_1,
			const BioAPI_INPUT_BIR* a_2,
			const BioAPI_BIR_BIOMETRIC_DATA_FORMAT* a_3,
			BioAPI_BIR_HANDLE*      a_4,
			const BioAPI_DATA*      a_5,
			BioAPI_UUID*            a_6
		)
	{
		return details::DriverFunctor_GetObject().BioAPI_CreateTemplate(a_0, a_1, a_2, a_3, a_4, a_5, a_6);
	}

	BioAPI_RETURN  Zw_BioAPI_Enroll(
			BioAPI_HANDLE           a_0,
			BioAPI_BIR_PURPOSE      a_1,
			BioAPI_BIR_SUBTYPE      a_2,
			const BioAPI_BIR_BIOMETRIC_DATA_FORMAT* a_3,
			const BioAPI_INPUT_BIR* a_4,
			BioAPI_BIR_HANDLE*      a_5,
			const BioAPI_DATA*      a_6,
			int32_t                 a_7,
			BioAPI_BIR_HANDLE*      a_8,
			BioAPI_UUID*            a_9
		)
	{
		return details::DriverFunctor_GetObject().BioAPI_Enroll(a_0, a_1, a_2, a_3, a_4, a_5, a_6, a_7, a_8, a_9);
	}

	BioAPI_RETURN Zw_BioAPI_EnumBSPs(BioAPI_BSP_SCHEMA** a_0, uint32_t* a_1)
	{
		return details::DriverFunctor_GetObject().BioAPI_EnumBSPs(a_0, a_1);
	}

	BioAPI_RETURN Zw_BioAPI_Free(void* a_0)
	{
		return details::DriverFunctor_GetObject().BioAPI_Free(a_0);
	}

	BioAPI_RETURN Zw_BioAPI_FreeBIRHandle(BioAPI_HANDLE a_0, BioAPI_BIR_HANDLE a_1)
	{
		return details::DriverFunctor_GetObject().BioAPI_FreeBIRHandle(a_0, a_1);
	}

	BioAPI_RETURN Zw_BioAPI_GetBIRFromHandle(BioAPI_HANDLE a_0, BioAPI_BIR_HANDLE a_1, BioAPI_BIR* a_2)
	{
		return details::DriverFunctor_GetObject().BioAPI_GetBIRFromHandle(a_0, a_1, a_2);
	}

	BioAPI_RETURN Zw_BioAPI_GetFrameworkInfo(BioAPI_FRAMEWORK_SCHEMA* a_0)
	{
		return details::DriverFunctor_GetObject().BioAPI_GetFrameworkInfo(a_0);
	}

	BioAPI_RETURN Zw_BioAPI_Init(BioAPI_VERSION a_0)
	{
		HRESULT hr_ = details::DriverFunctor_GetObject().Initialize();
		if (S_OK == hr_)
			return details::DriverFunctor_GetObject().BioAPI_Init(a_0);
		else
			return BioAPIERR_FUNCTION_FAILED;
	}

	BioAPI_RETURN Zw_BioAPI_Process(BioAPI_HANDLE a_0, const BioAPI_INPUT_BIR* a_1, const BioAPI_BIR_BIOMETRIC_DATA_FORMAT* a_2, BioAPI_BIR_HANDLE* a_3)
	{
		return details::DriverFunctor_GetObject().BioAPI_Process(a_0, a_1, a_2, a_3);
	}

	BioAPI_RETURN Zw_BioAPI_QueryUnits(const BioAPI_UUID* a_0, BioAPI_UNIT_SCHEMA** a_1, uint32_t* a_2)
	{
		return details::DriverFunctor_GetObject().BioAPI_QueryUnits(a_0, a_1, a_2);
	}

	BioAPI_RETURN Zw_BioAPI_Terminate(void)
	{
		BioAPI_RETURN ret_ = details::DriverFunctor_GetObject().BioAPI_Terminate();
		details::DriverFunctor_GetObject().Terminate();
		return ret_;
	}

	BioAPI_RETURN Zw_BioAPI_VerifyMatch(
			BioAPI_HANDLE  a_0,
			BioAPI_FMR     a_1,
			const BioAPI_INPUT_BIR* a_2,
			const BioAPI_INPUT_BIR* a_3,
			BioAPI_BIR_HANDLE*      a_4,
			BioAPI_BOOL*   a_5,
			BioAPI_FMR*    a_6,
			BioAPI_DATA*   a_7
		)
	{
		return details::DriverFunctor_GetObject().BioAPI_VerifyMatch(a_0, a_1, a_2, a_3, a_4, a_5, a_6, a_7);
	}
}}}}