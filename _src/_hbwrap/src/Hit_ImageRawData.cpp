/*
	Created by Tech_dog (VToropov) on 26-Mar-2014 at 10:05:11pm, GMT+4, Saint-Petersburg, Wednesday;
	This is Shared Recognition Hitachi BioAPI Image Processing Raw Data class implementation file.
*/
#include "StdAfx.h"
#include "Hit_ImageRawData.h"

using namespace shared;
using namespace shared::recognition;
using namespace shared::recognition::client;
using namespace shared::recognition::client::Hitachi;

////////////////////////////////////////////////////////////////////////////

CImageRawData::CImageRawData(const eProcessorType type, const INT_PTR provider_handle):
	m_proc_type(type),
	m_p_data(NULL),
	m_provider(provider_handle),
	m_this_ref(*this)
{
}

CImageRawData::~CImageRawData(void)
{
	Clear();
}

////////////////////////////////////////////////////////////////////////////

HRESULT           CImageRawData::Clear(void)
{
	BioAPI_RETURN ret__ = (BioAPI_RETURN)GetSuccessResultCode();
	if (NULL != m_p_data)
	{
		BioAPI_BIR_HANDLE image_handle = (BioAPI_BIR_HANDLE)m_p_data; m_p_data = NULL;
		BioAPI_HANDLE provider_handle = (BioAPI_HANDLE)m_provider;
		ret__ = Zw_BioAPI_FreeBIRHandle(provider_handle, image_handle);
	}
	if (NULL != m_fv_compatible_data.m_psa)
	{
		m_fv_compatible_data.Destroy();
	}
	m_error.SetDeviceCode((INT)ret__);
	return m_error.GetHresult();
}

HRESULT           CImageRawData::DetachTo(IImageRawData& raw_ref)
{
	if (raw_ref.GetProcessorType() != shared::recognition::eHibio)
		return DISP_E_TYPEMISMATCH;
	HRESULT hr__ = S_OK;
	try
	{
		CImageRawData* pRawData = dynamic_cast<CImageRawData*>(&raw_ref);
		if (pRawData)
		{
			CImageRawData& hibi_raw_ref = *pRawData;
			hibi_raw_ref.SetData(m_p_data, 0);
			hibi_raw_ref.__SetProvider(m_provider);
			m_p_data = NULL;
			m_error.SetHresult(OLE_E_BLANK);
		}
	}
	catch(::std::bad_alloc&)
	{
		ATLASSERT(FALSE);
		hr__ = DISP_E_TYPEMISMATCH;
	}
	return  hr__;
}

INT_PTR           CImageRawData::GetData(void) const
{
	return m_p_data;
}

const IError&     CImageRawData::GetLastError_Ref(void) const
{
	return m_error;
}

eProcessorType    CImageRawData::GetProcessorType(void) const
{
	return m_proc_type;
}
const T_RAW_DATA& CImageRawData::GetRawData_Ref(void) const
{
	if (NULL == m_fv_compatible_data.m_psa && NULL != m_p_data)
		m_this_ref.__PtrToArray();
	return m_fv_compatible_data;
}

T_RAW_DATA&       CImageRawData::GetRawData_Ref(void)
{
	if (NULL == m_fv_compatible_data.m_psa && NULL != m_p_data)
		m_this_ref.__PtrToArray();
	return m_fv_compatible_data;
}

HRESULT           CImageRawData::SetData(const INT_PTR  pData, const INT nSize)
{
	nSize; // doesn't have a matter
	if (!pData)
		return E_INVALIDARG;
	Clear();
	m_p_data = pData;
	m_error.SetHresult(S_OK);
	return S_OK;
}

HRESULT           CImageRawData::SetRawData(const T_RAW_DATA&)
{
	return E_NOTIMPL;
}

////////////////////////////////////////////////////////////////////////////

void    CImageRawData::__SetProvider(const INT_PTR provider_handle)
{
	m_provider = provider_handle;
}

HRESULT CImageRawData::__ToVariant(_variant_t& vData)const
{
	HRESULT hr_ = S_FALSE;
	if (NULL == m_fv_compatible_data.m_psa && NULL != m_p_data)
		hr_ = m_this_ref.__PtrToArray();
	if (S_OK == hr_)
	{
		vData.Clear();
		hr_ = m_this_ref.m_fv_compatible_data.CopyTo(&vData.parray);
		if (S_OK == hr_)
			vData.vt = VT_ARRAY|VT_UI1;
	}
	return hr_;
}

////////////////////////////////////////////////////////////////////////////

HRESULT CImageRawData::__PtrToArray(void)
{
	if (NULL == m_p_data)
		return OLE_E_BLANK;
	if (NULL != m_fv_compatible_data.m_psa)
		m_fv_compatible_data.Destroy();
	BioAPI_BIR_HANDLE image_handle = (BioAPI_BIR_HANDLE)m_p_data;
	BioAPI_HANDLE provider_handle = (BioAPI_HANDLE)m_provider;
	BioAPI_BIR bir__ = {0};
	BioAPI_RETURN ret__ = Zw_BioAPI_GetBIRFromHandle(provider_handle, image_handle, &bir__);
	m_error.SetDeviceCode((INT)ret__);
	HRESULT hr__ = m_error.GetHresult();
	if (S_OK != hr__)
		return  hr__;
	const INT nMemorySize = sizeof(bir__.Header)
							+ sizeof(bir__.BiometricData.Length)
							+ sizeof(BYTE) * bir__.BiometricData.Length
							+ sizeof(bir__.SecurityBlock.Length)
							+ sizeof(BYTE) * bir__.SecurityBlock.Length;

	shared::lite::data::CByteArray pData(nMemorySize);

	if (pData.GetLastResult() == S_OK)
	{
		PBYTE ptr__ = pData.GetData();
		::memcpy((void*)ptr__, (void*)&bir__.Header, sizeof(bir__.Header));
		ptr__ += sizeof(bir__.Header);
		::memcpy((void*)ptr__, (void*)&bir__.BiometricData.Length, sizeof(bir__.BiometricData.Length));
		ptr__ += sizeof(bir__.BiometricData.Length);
		if (bir__.BiometricData.Length > 0 && NULL != bir__.BiometricData.Data)
		{
			::memcpy((void*)ptr__, (void*)bir__.BiometricData.Data, sizeof(BYTE) * bir__.BiometricData.Length);
		}
		ptr__ +=(sizeof(BYTE) * bir__.BiometricData.Length);
		::memcpy((void*)ptr__, (void*)&bir__.SecurityBlock.Length, sizeof(bir__.SecurityBlock.Length));
		ptr__ += sizeof(bir__.SecurityBlock.Length);
		if (bir__.SecurityBlock.Length > 0 && NULL != bir__.SecurityBlock.Data)
		{
			::memcpy((void*)ptr__, (void*)bir__.SecurityBlock.Data, sizeof(BYTE) * bir__.SecurityBlock.Length);
		}
	}
	if (NULL != bir__.BiometricData.Data)
	{
		Zw_BioAPI_Free(bir__.BiometricData.Data);
		bir__.BiometricData.Data = NULL;
	}
	if (NULL != bir__.SecurityBlock.Data)
	{
		Zw_BioAPI_Free(bir__.SecurityBlock.Data);
		bir__.SecurityBlock.Data = NULL;
	}
	if (S_OK == hr__) // BIR data is successfully copied to byte array
	{
		SAFEARRAY* psa = NULL;
		hr__ = pData.CopyTo(psa);
		if (S_OK == hr__)
		{
			m_fv_compatible_data.Attach(psa);
			psa = NULL;
		}
	}
	m_error.SetHresult(hr__);
	return  hr__;
}