#ifndef _SHAREDHITACHIBIOAPIBIRHOLDER_H_D841F18D_BC50_4d71_88D6_444844772097_INCLUDED
#define _SHAREDHITACHIBIOAPIBIRHOLDER_H_D841F18D_BC50_4d71_88D6_444844772097_INCLUDED
/*
	Created by Tech_dog (VToropov) on 27-Mar-2014 at 4:37:50am, GMT+4, Saint-Petersburg, Thursday;
	This is Shared Recognition Hitachi BioAPI BIR Holder class declaration file.
*/
#include "Hit_Error.h"

namespace shared { namespace recognition { namespace client { namespace Hitachi
{
	class CBIRHolder
	{
	private:
		BioAPI_BIR        m_bir;
		HRESULT           m_hResult;
		BioAPI_HANDLE     m_provider;
		CError            m_error;
	public:
		CBIRHolder(void);
		CBIRHolder(const BioAPI_BIR&);
		CBIRHolder(const BioAPI_HANDLE provider_handle);
		CBIRHolder(SAFEARRAY*);
		~CBIRHolder(void);
	public:
		HRESULT           Clear(void);
		HRESULT           Create(const _variant_t&);
		HRESULT           Create(const BioAPI_BIR&);
		HRESULT           Create(const INT_PTR);
		HRESULT           Create(LPCTSTR file_);
		HRESULT           Create(SAFEARRAY*);
		const BioAPI_BIR& GetBir_Ref(void) const;
		BioAPI_BIR&       GetBir_Ref(void);
		const CError&     GetError_Ref(void) const;
		HRESULT           GetLastResult(void) const;
		bool              IsFailure(void) const;
	private:
		void            __clear(void);
	private:
		CBIRHolder(const CBIRHolder&);
		CBIRHolder& operator= (const CBIRHolder&);
	};
}}}}

#endif/*_SHAREDHITACHIBIOAPIBIRHOLDER_H_D841F18D_BC50_4d71_88D6_444844772097_INCLUDED*/