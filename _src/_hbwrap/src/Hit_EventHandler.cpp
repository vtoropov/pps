/*
	Created by Tech_dog (VToropov) on 22-Mar-2014 at 2:41:08pm, GMT+4, Moscow Region,
	Rail Road Train #43, Coatch #6, Place #1, Saturday;
	This is Shared Recognition Hitachi SDK wrapper event handler class implementation file.
*/
#include "StdAfx.h"
#include "Hit_EventHandler.h"

using namespace shared;
using namespace shared::recognition;
using namespace shared::recognition::client;
using namespace shared::recognition::client::Hitachi;

////////////////////////////////////////////////////////////////////////////

CEventHandler::CEventHandler(IGenericCallback& callback_ref):
	m_callback_ref(callback_ref),
	m_state_callback_set(false)
{
}

CEventHandler::~CEventHandler(void)
{
}

////////////////////////////////////////////////////////////////////////////

const CError&  CEventHandler::GetLastError_Ref(void) const
{
	return m_error;
}

bool           CEventHandler::IsStateCallbackSet(void) const
{
	return m_state_callback_set;
}

HRESULT        CEventHandler::SetStateCallback(const INT device_handle)
{
	BioAPI_HANDLE handle__ = (BioAPI_HANDLE)device_handle; handle__;
	/*const BioAPI_RETURN ret__ = BioAPI_SetGUICallbacks(handle__, CEventHandler::StreamCallback, NULL, CEventHandler::StateCallback, (void*)this);
	m__error.SetDeviceCode(ret__);*/
	const HRESULT hr__ = m_error.GetHresult();
	m_state_callback_set = (S_OK == hr__);
	return hr__;
}

////////////////////////////////////////////////////////////////////////////

BioAPI_RETURN CEventHandler::EventCallback( __in_opt const BioAPI_UUID*,
											__in     BioAPI_UNIT_ID,
											__in_opt void* pNotifyCallbackCtx,
											__in     const BioAPI_UNIT_SCHEMA,
											__in     BioAPI_EVENT eventType)
{
	pNotifyCallbackCtx;
	switch (eventType)
	{
	case BioAPI_NOTIFY_INSERT:
		{
		} break;
	case BioAPI_NOTIFY_REMOVE:
		{
		} break;
	case BioAPI_NOTIFY_FAULT:
		{
		} break;
	case BioAPI_NOTIFY_SOURCE_PRESENT:
		{
		} break;
	case BioAPI_NOTIFY_SOURCE_REMOVED:
		{
		} break;
	default:;
	}
	return BioAPI_OK;
}

BioAPI_RETURN CEventHandler::StateCallback( __in_opt void* pNotifyCallbackCtx,
											__in     BioAPI_GUI_STATE,
											__in_opt BioAPI_GUI_RESPONSE* pResponse,
											__in     BioAPI_GUI_MESSAGE msg__,
											__in     BioAPI_GUI_PROGRESS,
											__in_opt const BioAPI_GUI_BITMAP*)
{
	CEventHandler* pHandler = reinterpret_cast<CEventHandler*>(pNotifyCallbackCtx);
	if (NULL == pHandler)
		return BioAPI_OK;
	HRESULT hr__ = S_OK;
	switch (msg__)
	{
	case UIT_Cancelled:
		{
			hr__ = (*pHandler).m_callback_ref.OnDeviceStateChanged(UIT_Cancelled);
		} break;
	case UIT_Detection:
		{
		} break;
	case UIT_Failure:
		{
		} break;
	case UIT_Process:
		{
		} break;
	case UIT_Success:
		{
		} break;
	case UIT_Timout:
		{
		} break;
	default:;
	}
	if (NULL != pResponse)
		(*pResponse) = (SUCCEEDED(hr__) ? BioAPI_CONTINUE : BioAPI_CANCEL);
	return BioAPI_OK;
}

BioAPI_RETURN CEventHandler::StreamCallback(__in_opt void* pNotifyCallbackCtx,
											__in_opt const BioAPI_GUI_BITMAP*)
{
	pNotifyCallbackCtx;
	return BioAPI_OK;
}