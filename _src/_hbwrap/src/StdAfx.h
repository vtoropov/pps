#ifndef _SHAREDHITACHIWRAPPERLAYERPRECOMPILED_H_1B4BCDA8_FB55_4c32_9EB7_47F5637A9780_INCLUDED
#define _SHAREDHITACHIWRAPPERLAYERPRECOMPILED_H_1B4BCDA8_FB55_4c32_9EB7_47F5637A9780_INCLUDED
/*
	Created by Tech_dog (VToropov) on 21-Mar-2014 at 1:12:26pm, GMT+4, Taganrog, Friday;
	This is Shared Recognition Hitachi BioAPI Wrapper Library Precompiled Header definition file.
*/

#define WIN32_LEAN_AND_MEAN

#ifndef  WINVER
#define  WINVER       0x0501 // this is for use WindowsXP (or later) specific feature(s)
#endif   WINVER

#ifndef _WIN32_WINNT
#define _WIN32_WINNT  0x0501 // this is for use WindowsXP (or later) specific feature(s)
#endif  _WIN32_WINNT

#ifndef _WIN32_IE
#define _WIN32_IE     0x0600 // this is for use IE 6 (or later) specific feature(s)
#endif  _WIN32_IE

#include <atlbase.h>
#include <atlstr.h>
#include <comdef.h>
#include <new>
#include <map>
#include <set>
#include <vector>
#include <typeinfo>
#include <deque>

#include <atlsafe.h>
#include <atlcomtime.h>
#include <comutil.h>

#include "Shared_EventLogger.h"
////////////////////////////////////////////////////////////////////////////
//
// the Hitachi specific headers
//
////////////////////////////////////////////////////////////////////////////

#include "bioapi.h"
#include "bioapi_type.h"

#include "Hit_DriverFunctor.h"

#pragma warning(disable: 4481) // nonstandard extension used: override specifier 'override'

#endif/*_SHAREDHITACHIWRAPPERLAYERPRECOMPILED_H_1B4BCDA8_FB55_4c32_9EB7_47F5637A9780_INCLUDED*/