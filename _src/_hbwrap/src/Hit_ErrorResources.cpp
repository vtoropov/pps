/*
	Created by Tech_dog (VToropov) on 22-Mar-2014 at 12:28:48pm, GMT+4, Moscow Region;
	Rail Road Train #43, Coatch #6, Place #1, Saturday;
	This is Shared Recognition Hitachi SDK Error Resource implementation file. 
*/
#include "StdAfx.h"
#include "Hit_ErrorResources.h"

namespace shared { namespace recognition { namespace client { namespace Hitachi { namespace details
{
	::ATL::CAtlString Error_GetString(const DWORD dwResId)
	{
		::ATL::CAtlString cs_str;
		switch(dwResId)
		{
		case IDS_ERROR_0x000101:  cs_str= _T("Internal error");                                                                 break;
		case IDS_ERROR_0x000102:  cs_str= _T("Memory error; The memory of PC is insufficient");                                 break;
 		case IDS_ERROR_0x000103:  cs_str= _T("Invalid pointer; The pointer inputted to function is invalid");                   break;
		case IDS_ERROR_0x000104:  cs_str= _T("Invalid input pointer");                                                          break;
		case IDS_ERROR_0x000105:  cs_str= _T("Invalid output pointer");                                                         break;
		case IDS_ERROR_0x000106:  cs_str= _T("Specified function is not supported by BSP");                                     break;
		case IDS_ERROR_0x000108:  cs_str= _T("Function failed by unknown reason");                                              break;
		case IDS_ERROR_0x000109:  cs_str= _T("Invalid UUID");                                                                   break;
		case IDS_ERROR_0x00010A:  cs_str= _T("Incompatible version; Version Number inputted to BioAPI_Init or BioAPI_BSPAttach is not 0x20");                                                       break;
		case IDS_ERROR_0x00010B:  cs_str= _T("Invalid data");                                                                   break;
		case IDS_ERROR_0x00010D:  cs_str= _T("Too many handles");                                                               break;
		case IDS_ERROR_0x00010E:  cs_str= _T("Timeout");                                                                        break;
		case IDS_ERROR_0x00010F:  cs_str= _T("Invalid BIR");                                                                    break;
		case IDS_ERROR_0x000112:  cs_str= _T("BIR is not inputted");                                                            break;
		case IDS_ERROR_0x000113:  cs_str= _T("Format not supported");                                                           break;
		case IDS_ERROR_0x000115:  cs_str= _T("Inconsistent specification of usage");                                            break;
		case IDS_ERROR_0x000116:  cs_str= _T("BIR process not completed");                                                      break;
		case IDS_ERROR_0x000117:  cs_str= _T("Specified usage is not supported");                                               break;
		case IDS_ERROR_0x000118:  cs_str= _T("Cancelled by user");                                                              break;
		case IDS_ERROR_0x000119:  cs_str= _T("Device in use");                                                                  break;
		case IDS_ERROR_0x00011A:  cs_str= _T("Invalid BSP handle");                                                             break;
		case IDS_ERROR_0x00011B:  cs_str= _T("Framework is not initialized");                                                   break;
		case IDS_ERROR_0x00011C:  cs_str= _T("Invalid BIR handle");                                                             break;
		case IDS_ERROR_0x000202:  cs_str= _T("Loading of BSP failed; The initialization of BSP failed");                        break;
		case IDS_ERROR_0x000203:  cs_str= _T("BSP is not loaded");                                                              break;
		case IDS_ERROR_0x000205:  cs_str= _T("Invalid Device ID; There is a possibility that the device has trouble");          break;
		case IDS_ERROR_0x000206:  cs_str= _T("Invalid Category");                                                               break;
		case IDS_ERROR_0x000501:  cs_str= _T("Unsuccessful operation due to low quality of the sample; There is a possibility that the state is bad as the surface of the finger is dirty etc.");   break;
		case IDS_ERROR_0x000107:  cs_str= _T("Requested resource access was rejected by operating system");                     break;
		case IDS_ERROR_0x00010C:  cs_str= _T("Cannot be captured");                                                             break;
		case IDS_ERROR_0x000110:  cs_str= _T("Signature failed");                                                               break;
		case IDS_ERROR_0x000111:  cs_str= _T("Payload cannot be saved");                                                        break;
		case IDS_ERROR_0x000114:  cs_str= _T("Unable to import");                                                               break;
		case IDS_ERROR_0x00011D:  cs_str= _T("Calibration failed");                                                             break;
		case IDS_ERROR_0x00011E:  cs_str= _T("No preset BIR");                                                                  break;
		case IDS_ERROR_0x00011F:  cs_str= _T("Decryption of BIR failed");                                                       break;
		case IDS_ERROR_0x000201:  cs_str= _T("Component file reference not found");                                             break;
		case IDS_ERROR_0x000204:  cs_str= _T("Device is not inserted");                                                         break;
		case IDS_ERROR_0x000300:  cs_str= _T("Invalid Database Handle");                                                        break;
		case IDS_ERROR_0x000301:  cs_str= _T("Database cannot be opened");                                                      break;
		case IDS_ERROR_0x000302:  cs_str= _T("Database is locked");                                                             break;
		case IDS_ERROR_0x000303:  cs_str= _T("No Database");                                                                    break;
		case IDS_ERROR_0x000304:  cs_str= _T("Database already exists");                                                        break;
		case IDS_ERROR_0x000305:  cs_str= _T("Invalid Database name");                                                          break;
		case IDS_ERROR_0x000306:  cs_str= _T("Record not found");                                                               break;
		case IDS_ERROR_0x000307:  cs_str= _T("Invalid Marker Handle");                                                          break;
		case IDS_ERROR_0x000308:  cs_str= _T("Database is open");                                                               break;
		case IDS_ERROR_0x000309:  cs_str= _T("Invalid access request");                                                         break;
		case IDS_ERROR_0x00030A:  cs_str= _T("End of Database");                                                                break;
		case IDS_ERROR_0x00030B:  cs_str= _T("Unable to create Database");                                                      break;
		case IDS_ERROR_0x00030C:  cs_str= _T("Unable to close Database");                                                       break;
		case IDS_ERROR_0x00030D:  cs_str= _T("Unable to delete Database");                                                      break;
		case IDS_ERROR_0x00030E:  cs_str= _T("Database destroyed");                                                             break;
		case IDS_ERROR_0x000400:  cs_str= _T("Position error");                                                                 break;
		case IDS_ERROR_0x000401:  cs_str= _T("Out of frame");                                                                   break;
		case IDS_ERROR_0x000402:  cs_str= _T("Invalid horizontal position");                                                    break;
		case IDS_ERROR_0x000403:  cs_str= _T("Invalid vertical position");                                                      break;
		case IDS_ERROR_0x000404:  cs_str= _T("Invalid vertical position");                                                      break;
		case IDS_ERROR_0x000405:  cs_str= _T("Too much on the right-hand side");                                                break;
		case IDS_ERROR_0x000406:  cs_str= _T("Too much on the left-hand side");                                                 break;
		case IDS_ERROR_0x000407:  cs_str= _T("Too high");                                                                       break;
		case IDS_ERROR_0x000408:  cs_str= _T("Too low");                                                                        break;
		case IDS_ERROR_0x000409:  cs_str= _T("Too far");                                                                        break;
		case IDS_ERROR_0x00040A:  cs_str= _T("Too close");                                                                      break;
		case IDS_ERROR_0x00040B:  cs_str= _T("Too much on front side");                                                         break;
		case IDS_ERROR_0x00040C:  cs_str= _T("Too much on rear side");                                                          break;
		case IDS_ERROR_0x000801:  cs_str= _T("Error in common key setting; The initialization to encrypt BIR is not done");     break;
		case IDS_ERROR_0x000802:  cs_str= _T("Invalid encryption algorithm");                                                   break;
		case IDS_ERROR_0x000803:  cs_str= _T("Invalid key type information");                                                   break;
		case IDS_ERROR_0x000804:  cs_str= _T("Invalid authorization PIN; The specified PIN data is invalid");                   break;
		case IDS_ERROR_0x000805:  cs_str= _T("Invalid application name");                                                       break;
		case IDS_ERROR_0x000806:  cs_str= _T("Error in function restriction; The called function is restricted");               break;
		case IDS_ERROR_0x000807:  cs_str= _T("Internal error; Initialization of Hitachi secure extensions is not executed; The privileges of user are incorrect");                                  break;
		case IDS_ERROR_0x000808:  cs_str= _T("Error in function assignment");                                                   break;
		case IDS_ERROR_0x000809:  cs_str= _T("Invalid type");                                                                   break;
		case IDS_ERROR_0x00080A:  cs_str= _T("The value of MaxFMRRequested exceeds the upper bound value that is set by HiBioAPI_InitVerifyParameter or HiBioAPI_InitIdentifyParameter");           break;
		case IDS_ERROR_0x00080B:  cs_str= _T("Invalid number of iteration allowance; Negative value is specified");             break;
		case IDS_ERROR_0x00080C:  cs_str= _T("Invalid cease of operation hour; Negative value is specified");                   break;
		case IDS_ERROR_0x00080D:  cs_str= _T("Invalid security code generation method");                                        break;
		case IDS_ERROR_0x00080E:  cs_str= _T("Invalid Key ID setting");                                                         break;
		case IDS_ERROR_0x00080F:  cs_str= _T("Deletion failed");                                                                break;
		case IDS_ERROR_0x000810:  cs_str= _T("Unmatched BSP Hash value");                                                       break;
		case IDS_ERROR_0x000811:  cs_str= _T("Invalid challenge code");                                                         break;
		case IDS_ERROR_0x000812:  cs_str= _T("Error in security code generation; Initialization of Hitachi secure extensions is not executed");                                                     break;
		case IDS_ERROR_0x000813:  cs_str= _T("No corresponding matching results");                                              break;
		case IDS_ERROR_0x000814:  cs_str= _T("Invalid certificate information");                                                break;
		case IDS_ERROR_0x000815:  cs_str= _T("No signature; Specified BIR has no signature");                                   break;
		case IDS_ERROR_0x000816:  cs_str= _T("Invalid signature information; The BIR data is broken");                          break;
		case IDS_ERROR_0x000817:  cs_str= _T("Encryption error; In the environment to which AES is not supported (ex. Windows 2000, etc.), CALG_AES_256 is specified for Algorithm_ID of HiBioAPI_InitSecretKey"); break;
		case IDS_ERROR_0x000818:  cs_str= _T("Decryption error; The value of algorithm_ID or inputseed of HiBioAPI_InitSecretKey is different past (when BIR is obtained) and now");                break;
		case IDS_ERROR_0x000819:  cs_str= _T("Error in number of iterations; The continuous failure frequency of the attestation reached a permissible upper bound. Thereafter, the system is locked until the set suspension time passes");break;
		case IDS_ERROR_0x00081A:  cs_str= _T("Status update error");                                                            break;
		case IDS_ERROR_0x00081B:  cs_str= _T("In cease of operation; The system is being locked. The system cannot be used until the suspension time passes");                                      break;
		case IDS_ERROR_0x00081C:  cs_str= _T("MAXFMRR value error");                                                            break;
		case IDS_ERROR_0x00081D:  cs_str= _T("Hitachi Extension API not used");                                                 break;
		case IDS_ERROR_0x00081E:  cs_str= _T("No BSP or unable to capture BSP pass");                                           break;
		case IDS_ERROR_0x000010:  cs_str= _T("Unable to capture device pass; The unit is not connected");                       break;
		case IDS_ERROR_0x000011:  cs_str= _T("Unable to open device; The device driver is not installed; Load of PC is very high");                                                                 break;
		case IDS_ERROR_0x000012:  cs_str= _T("Invalid Device Handle; USB port of PC is broken; The device unit is broken");     break;
		case IDS_ERROR_0x000013:  cs_str= _T("Unable to acquire OS Version");                                                   break;
		case IDS_ERROR_0x000014:  cs_str= _T("Invalid OS Version (Other than 2K/XP)");                                          break;
		case IDS_ERROR_0x000015:  cs_str= _T("Invalid scanning size: Speed of USB is not enough; Load of PC is very high; USB port of PC or device unit is broken");                                break;
		case IDS_ERROR_0x000016:  cs_str= _T("Unable to secure memory; The memory of PC is insufficient");                      break;
		case IDS_ERROR_0x000017:  cs_str= _T("Unable to create Event; The memory of PC or resources of OS is insufficient");    break;
		case IDS_ERROR_0x000018:  cs_str= _T("Unable to create Thread");                                                        break;
		case IDS_ERROR_0x000019:  cs_str= _T("Unable to create Mutex");                                                         break;
		case IDS_ERROR_0x00001A:  cs_str= _T("Parameter is out of range or invalid");                                           break;
		case IDS_ERROR_0x00001B:  cs_str= _T("API not supported");                                                              break;
		case IDS_ERROR_0x00001C:  cs_str= _T("Other process and thread are in open processing (Retry recommended)");            break;
		case IDS_ERROR_0x00001D:  cs_str= _T("Wrong parameter");                                                                break;
		case IDS_ERROR_0x00001E:  cs_str= _T("Sharing of Work Key failed: Speed of USB is not enough; Load of PC is very high; USB port of PC or device unit is broken");                           break;
		case IDS_ERROR_0x00001F:  cs_str= _T("Invalid address used for FlashROM access");                                       break;
		case IDS_ERROR_0x000021:  cs_str= _T("Command ends in error");                                                          break;
		case IDS_ERROR_0x000022:  cs_str= _T("Lack of memory, or assigned area is small: Speed of USB is not enough; Load of PC is very high; USB port of PC or device unit is broken");            break;
		case IDS_ERROR_0x000023:  cs_str= _T("Recovery of authentication device to try and succeed: Speed of USB is not enough; Load of PC is very high; USB port of PC or device unit is broken"); break;
		case IDS_ERROR_0x020001:  cs_str= _T("Delayed transmission of graphical data: Speed of USB is not enough; Load of PC is very high; USB port of PC or device unit is broken");               break;
		default:
			cs_str.Format(_T("Undefined error code=%d"), dwResId);
		}
		return cs_str;
	}
}}}}}