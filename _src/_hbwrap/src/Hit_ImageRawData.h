#ifndef _SHAREDCLIENTHITACHBIOAPYIMAGEPROCESSINGRAWDATA_H_900158C8_A926_464a_B7A7_6A29DCF2803E_INCLUDED
#define _SHAREDCLIENTHITACHBIOAPYIMAGEPROCESSINGRAWDATA_H_900158C8_A926_464a_B7A7_6A29DCF2803E_INCLUDED
/*
	Created by Tech_dog (VToropov) on 26-Mar-2014 at 9:57:16pm, GMT+4, Saint-Petersburg, Wednesday;
	This is Shared Recognition Hitachi BioAPI Image Processing Raw Data class declaration file.
*/
#include "ImageProcessingCommonDefs.h"
#include "Hit_Error.h"
#include "Shared_RawData.h"

namespace shared { namespace recognition { namespace client { namespace Hitachi
{
	using shared::recognition::IError;
	using shared::recognition::eProcessorType;
	using shared::lite::data::T_RAW_DATA;

	class CImageRawData:
		public shared::recognition::IImageRawData
	{
	private:
		mutable CImageRawData&    m_this_ref;
		CError                    m_error;
		eProcessorType            m_proc_type;
		T_RAW_DATA                m_fv_compatible_data;
		INT_PTR                   m_p_data;
		INT_PTR                   m_provider;
	public:
		CImageRawData(const eProcessorType, const INT_PTR provider_handle);
		~CImageRawData(void);
	public:
		virtual HRESULT           Clear(void) override sealed;
		virtual HRESULT           DetachTo(IImageRawData&) override sealed;
		virtual INT_PTR           GetData(void) const override sealed;
		virtual const IError&     GetLastError_Ref(void) const override sealed;
		virtual eProcessorType    GetProcessorType(void) const override sealed;
		virtual const T_RAW_DATA& GetRawData_Ref(void) const override sealed;
		virtual T_RAW_DATA&       GetRawData_Ref(void) override sealed;
		virtual HRESULT           SetData(const INT_PTR  pData, const INT nSize) override sealed;
		virtual HRESULT           SetRawData(const T_RAW_DATA&) override sealed;
	public:
		void    __SetProvider(const INT_PTR);
		HRESULT __ToVariant(_variant_t&)const;
	private:
		HRESULT __PtrToArray(void);
	private:
		CImageRawData(const CImageRawData&);
		CImageRawData& operator= (const CImageRawData&);
	};
}}}}

#endif/*_SHAREDCLIENTHITACHBIOAPYIMAGEPROCESSINGRAWDATA_H_900158C8_A926_464a_B7A7_6A29DCF2803E_INCLUDED*/