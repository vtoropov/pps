#ifndef _SHAREDHITACHBIOAPISDKINITIALIZER_H_90876DE8_559D_45c5_A429_70F476CAEACE_INCLUDED
#define _SHAREDHITACHBIOAPISDKINITIALIZER_H_90876DE8_559D_45c5_A429_70F476CAEACE_INCLUDED
/*
	Created by Tech_dog (VToropov) on 22-Mar-2014 at 11:09:04am, GMT+4, Moscow Region,
	Rail Road Train #43, Coatch #6, Place#1, Saturday;
	This is Shared Recognition Hitachi BioAPI SDK Initializer class declaration file.
*/
#include "ImageProcessor.h"
#include "Hit_Error.h"
#include "Hit_EventHandler.h"

namespace shared { namespace recognition { namespace client { namespace Hitachi
{
	using shared::recognition::IDeviceIdentifierEnumerator;
	using shared::recognition::IError;
	using shared::recognition::eProcessorType;

	class CIdentifierEnum:
		public shared::recognition::IDeviceIdentifierEnumerator
	{
	private:
		typedef ::std::vector<::ATL::CAtlString>   TIdentifiers;
	private:
		TIdentifiers            m_ids;
		CError                  m_error;
	public:
		CIdentifierEnum(void);
		~CIdentifierEnum(void);
	public:
		virtual LPCTSTR         GetBSPOf(const INT nIndex) const override sealed;
		virtual INT             GetCount(void) const override sealed;
		virtual LPCTSTR         GetDefault(void) const override sealed;
		virtual const IError&   GetLastError_Ref(void) const override sealed;
		virtual HRESULT         Initialize(void) override sealed;
	public:
		CError&   GetLastError_Ref(void); // to share object error between initializer and enumerator
	private:
		CIdentifierEnum(const CIdentifierEnum&);
		CIdentifierEnum& operator= (const CIdentifierEnum&);
	};
	class CInitializer:
		public shared::recognition::IInitializer,
		public IGenericCallback
	{
	private:
		INT_PTR                       m_dev_handle;
		INT                           m_dev_result;
	private:
		BioAPI_UNIT_SCHEMA*           m_unit_schemas;
		UINT                          m_unit_schemas_num;
		CEventHandler                 m_event_handler;
	private:
		enum eInternalPartStateFlags
		{
			IPF__CORE_SUCCESS        = 0,
			IPF__BSP_LOAD_SUCCESS    = 1,
			IPF__UNIT_SCHEMA_SUCCESS = 2,
			IPF__BSP_ATTACH_SUCCESS  = 3,
		};
		static const INT eMilestones = CInitializer::IPF__BSP_ATTACH_SUCCESS + 1;
	private:
		CIdentifierEnum               m_id_enum;
		CError&                       m_error_ref;
		bool                          m_flags[eMilestones];
	public:
		CInitializer(void);
		~CInitializer(void);
	public:
		virtual const IDeviceIdentifierEnumerator&  GetDeviceIdentifierEnum_Ref(void) const override sealed;
		virtual INT_PTR               GetHandle(void)        const override sealed;
		virtual const IError&         GetLastError_Ref(void) const override sealed;
		virtual eProcessorType        GetProcessorType(void) const override sealed;
		virtual HRESULT               Initialize(void)             override sealed;
		virtual bool                  IsInitialized(void)    const override sealed;
		virtual HRESULT               Terminate(void)              override sealed;
	public:
		const CIdentifierEnum&        GetIdentifierEnum_Ref(void) const;
	private:
		CInitializer(const CInitializer&);
		CInitializer& operator= (const CInitializer&);
	};

	HRESULT   CreateInitializer(CInitializer*&);
	HRESULT   DestroyInitializer_Safe(CInitializer*&);
}}}}

#endif/*_SHAREDHITACHBIOAPISDKINITIALIZER_H_90876DE8_559D_45c5_A429_70F476CAEACE_INCLUDED*/