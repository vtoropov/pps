#ifndef _SHAREDHITACHIBIOAPISDKFUNCTOR_H_3834200F_4C39_4c32_9699_2998440660C9_INCLUDED
#define _SHAREDHITACHIBIOAPISDKFUNCTOR_H_3834200F_4C39_4c32_9699_2998440660C9_INCLUDED
/*
	Created by Tech_dog(VToropov) on 03-Mar-2015 at 10:09:35pm, GMT+3, Taganrog, Tuesday;
	This is Hitachi Driver Dynamic Functor class declaration file.
*/
#include "Shared_SystemError.h"

namespace shared { namespace recognition { namespace client { namespace Hitachi
{
	BioAPI_RETURN Zw_BioAPI_BSPAttach(
			const BioAPI_UUID*,
			BioAPI_VERSION,
			const BioAPI_UNIT_LIST_ELEMENT*,
			uint32_t,
			BioAPI_HANDLE*
		);
	BioAPI_RETURN Zw_BioAPI_BSPDetach(BioAPI_HANDLE);
	BioAPI_RETURN Zw_BioAPI_BSPLoad(const BioAPI_UUID*, BioAPI_EventHandler, void*);
	BioAPI_RETURN Zw_BioAPI_BSPUnload(const BioAPI_UUID*, BioAPI_EventHandler, void*);
	BioAPI_RETURN Zw_BioAPI_Cancel(BioAPI_HANDLE);
	BioAPI_RETURN Zw_BioAPI_Capture(
			BioAPI_HANDLE,
			BioAPI_BIR_PURPOSE,
			BioAPI_BIR_SUBTYPE,
			const BioAPI_BIR_BIOMETRIC_DATA_FORMAT*,
			BioAPI_BIR_HANDLE*,
			int32_t,
			BioAPI_BIR_HANDLE*
		);
	BioAPI_RETURN Zw_BioAPI_CreateTemplate(
			BioAPI_HANDLE,
			const BioAPI_INPUT_BIR*,
			const BioAPI_INPUT_BIR*,
			const BioAPI_BIR_BIOMETRIC_DATA_FORMAT*,
			BioAPI_BIR_HANDLE*,
			const BioAPI_DATA*,
			BioAPI_UUID*
		);
	BioAPI_RETURN  Zw_BioAPI_Enroll(
			BioAPI_HANDLE,
			BioAPI_BIR_PURPOSE,
			BioAPI_BIR_SUBTYPE,
			const BioAPI_BIR_BIOMETRIC_DATA_FORMAT*,
			const BioAPI_INPUT_BIR*,
			BioAPI_BIR_HANDLE*,
			const BioAPI_DATA*,
			int32_t,
			BioAPI_BIR_HANDLE*,
			BioAPI_UUID*
		);
	BioAPI_RETURN Zw_BioAPI_EnumBSPs(BioAPI_BSP_SCHEMA**, uint32_t*);
	BioAPI_RETURN Zw_BioAPI_Free(void*);
	BioAPI_RETURN Zw_BioAPI_FreeBIRHandle(BioAPI_HANDLE, BioAPI_BIR_HANDLE);
	BioAPI_RETURN Zw_BioAPI_GetBIRFromHandle(BioAPI_HANDLE, BioAPI_BIR_HANDLE, BioAPI_BIR*);
	BioAPI_RETURN Zw_BioAPI_GetFrameworkInfo(BioAPI_FRAMEWORK_SCHEMA*);
	BioAPI_RETURN Zw_BioAPI_Init(BioAPI_VERSION);
	BioAPI_RETURN Zw_BioAPI_Process(BioAPI_HANDLE, const BioAPI_INPUT_BIR*, const BioAPI_BIR_BIOMETRIC_DATA_FORMAT*, BioAPI_BIR_HANDLE*);
	BioAPI_RETURN Zw_BioAPI_QueryUnits(const BioAPI_UUID*, BioAPI_UNIT_SCHEMA**, uint32_t*);
	BioAPI_RETURN Zw_BioAPI_Terminate(void);
	BioAPI_RETURN Zw_BioAPI_VerifyMatch(
			BioAPI_HANDLE,
			BioAPI_FMR,
			const BioAPI_INPUT_BIR*,
			const BioAPI_INPUT_BIR*,
			BioAPI_BIR_HANDLE*,
			BioAPI_BOOL*,
			BioAPI_FMR*,
			BioAPI_DATA*
		);
}}}}

#endif/*_SHAREDHITACHIBIOAPISDKFUNCTOR_H_3834200F_4C39_4c32_9699_2998440660C9_INCLUDED*/