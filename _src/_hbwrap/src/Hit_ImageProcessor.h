#ifndef _SHAREDHITACHIIMAGEPROCESSOR_H_C8B6486B_1410_4c00_AC3B_57008C7A7C45_INCLUDED
#define _SHAREDHITACHIIMAGEPROCESSOR_H_C8B6486B_1410_4c00_AC3B_57008C7A7C45_INCLUDED
/*
	Created by Tech_dog (VToropov) on 23-Mar-2014 at 2:43:05pm, GMT+4, Saint-Petersburg, Sunday;
	This is Shared Recognition Hitachi BioAPI SDK Image Processor class declaration file.
*/
#include "ImageProcessor.h"
#include "Hit_Error.h"
#include "Hit_ImageRawData.h"

namespace shared { namespace recognition { namespace client { namespace Hitachi
{
	using shared::recognition::IError;
	using shared::recognition::eProcessorType;
	using shared::recognition::IInitializer;
	using shared::recognition::IInterruptProcess;
	using shared::recognition::ePersistenceType;

	class CImageProcessor:
		public shared::recognition::IImageProcessor
	{
	private:
		const IInitializer&    m_init_ref;
		mutable CError         m_error;
		CImageRawData          m_cached;   
	public:
		CImageProcessor(const IInitializer&);
		~CImageProcessor(void);
	public: // IImageProcessor
		virtual HRESULT        Cancel(void)                                      override sealed;
		virtual HRESULT        CreateEnrollmentTemplate(_variant_t& vTemplate)   override sealed;
		virtual HRESULT        CreateVerificationTemplate(_variant_t& vTemplate) override sealed;
		virtual HRESULT        Enroll(_variant_t& vTemplate)                     override sealed; // takes an enroll template 3 times and returns the best one
		virtual const IError&  GetLastError_Ref(void)                      const override sealed;
		virtual eProcessorType GetProcessorType(void)                      const override sealed;
		virtual HRESULT        IsAlive(void)                               const override sealed;
		virtual HRESULT        VerifyMatch(const _variant_t& verifyTempl,  const _variant_t& enrollTempl) override sealed;
	private:
		CImageProcessor(const CImageProcessor&);
		CImageProcessor& operator= (const CImageProcessor&);
	};
}}}}

#endif/*_SHAREDHITACHIIMAGEPROCESSOR_H_C8B6486B_1410_4c00_AC3B_57008C7A7C45_INCLUDED*/