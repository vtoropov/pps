#ifndef _UIXCTRLBUTTONCTRL_H_81EC4C24_8073_49ef_9939_3D5C401193D4_INCLUDED
#define _UIXCTRLBUTTONCTRL_H_81EC4C24_8073_49ef_9939_3D5C401193D4_INCLUDED
/*
	Created by Tech_dog (VToropov) on 8-Feb-2015 at 6:22:03pm, GMT+3, Taganrog, Sunday;
	This is UIX library custom button control class declaration file.
*/
#include "UIX_ControlBase.h"
#include "UIX_GdiObject.h"

namespace ex_ui { namespace controls { namespace _impl
{
	class CButtonWnd;
}}}

namespace ex_ui { namespace controls
{
	using ex_ui::controls::_impl::CButtonWnd;
	using ex_ui::controls::defs::IControlNotify;
	using ex_ui::draw::defs::IRenderer;

	class CButton
	{
	protected:
		CButtonWnd*    m_wnd_ptr;
		CControlCrt    m_crt;
	public:
		CButton(CControlCrt&);
		CButton(CControlCrt&, const UINT ctrlId);
		CButton(const UINT ctrlId, IRenderer& parent_rnd, IControlNotify&);
		~CButton(void);
	public:
		const CColour& BackColor(void)const;
		VOID           BackColor(const COLORREF, const BYTE _alpha);
		HRESULT        Create(const HWND hParent, const LPRECT, const bool bHidden);
		HRESULT        Destroy(void);
		HRESULT        Enable (const bool);
		ATL::CWindow   GetWindow(void) const;
		HRESULT        SetImage(const DWORD dwState, const UINT nResId, const HINSTANCE hResourceModule = NULL);
		HRESULT        Subclass(::ATL::CWindow&);
	private:
		CButton(const CButton&);
		CButton& operator= (const CButton&);
	};
}}

#endif/*_UIXCTRLBUTTONCTRL_H_81EC4C24_8073_49ef_9939_3D5C401193D4_INCLUDED*/