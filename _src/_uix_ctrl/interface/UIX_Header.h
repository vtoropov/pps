#ifndef _UIXCTRLHEADERCONTROl_H_EAA0819E_9D06_4c60_9676_9791E2B4A8F7_INCLUDED
#define _UIXCTRLHEADERCONTROl_H_EAA0819E_9D06_4c60_9676_9791E2B4A8F7_INCLUDED
/*
	Created by Tech_dog (VToropov) on 8-Mar-2015 at 12:43:50pm, GMT+3, Taganrog, Sunday;
	This is Shared UIX library Header custom control class declaration file.
*/
#include <atlctrls.h>

namespace ex_ui { namespace controls
{
	class CHeader
	{
	private:
		class CHeaderWnd :
			public ::ATL::CWindowImpl<CHeaderWnd, ::WTL::CHeaderCtrl>
		{
		};
	};
}}

#endif/*_UIXCTRLHEADERCONTROl_H_EAA0819E_9D06_4c60_9676_9791E2B4A8F7_INCLUDED*/