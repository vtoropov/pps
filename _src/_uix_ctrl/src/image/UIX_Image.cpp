/*
	Created by Tech_dog (VToropov) on 7-Feb-2015 at 7:02:24pm, GMT+3, Taganrog, Saturday;
	This is UIX library PNG image control class implementation file.
*/
#include "StdAfx.h"
#include "UIX_Image.h"
#include "UIX_ImageShaperDef.h"
#include "UIX_ImageRendererDef.h"

using namespace ex_ui;
using namespace ex_ui::draw;
using namespace ex_ui::draw::common;
using namespace ex_ui::controls;
using namespace ex_ui::controls::_impl;

////////////////////////////////////////////////////////////////////////////

CImage::CImageWnd::CImageWnd(CImage& ctrl_ref):
	m_dwStyle(eImageStyle::eNone),
	m_pParentRenderer(NULL),
	m_pShaper(NULL),
	m_ctrl_ref(ctrl_ref),
	m_pNotify(NULL),
	m_pRenderer(NULL)
{
	try { m_pShaper = new CImageShaperDefaultImpl(ctrl_ref, m_image_ptr); } catch (::std::bad_alloc&) { ATLASSERT(FALSE); }
	try { m_pRenderer = new CImageRendererDefaultImpl(ctrl_ref, m_pShaper); } catch (::std::bad_alloc&) { ATLASSERT(FALSE); }
}

CImage::CImageWnd::~CImageWnd(void)
{
	if (NULL != m_pRenderer){ try { delete m_pRenderer; m_pRenderer = NULL;} catch(...) { ATLASSERT(FALSE); }}
	if (NULL != m_pShaper){ try { delete m_pShaper; m_pShaper = NULL; } catch (...) { ATLASSERT(FALSE);}}
}

////////////////////////////////////////////////////////////////////////////

LRESULT CImage::CImageWnd::OnCreate (UINT, WPARAM, LPARAM, BOOL&)
{
	return 0;
}

LRESULT CImage::CImageWnd::OnDestroy(UINT, WPARAM, LPARAM, BOOL&)
{
	return 0;
}

LRESULT CImage::CImageWnd::OnErase  (UINT, WPARAM  wParam, LPARAM, BOOL&)
{
	RECT rc_ = {0};
	GetClientRect(&rc_);
	if (::IsRectEmpty(&rc_))
		return 1;
	CZBuffer dc_(reinterpret_cast<HDC>(wParam), rc_);
	if (NULL != m_pRenderer)
	{
		m_pRenderer->Draw(dc_, rc_);
	}
	return 0;
}

LRESULT CImage::CImageWnd::OnLButtonDn (UINT, WPARAM, LPARAM, BOOL& bHandled)
{
	if (NULL != m_pNotify)
	{
		bHandled = TRUE;
		(*m_pNotify).IControlNotify_OnClick(m_ctrl_ref.GetIdentifier());
	}
	return 0;
}

//////////////////////////////////////////////////////////////////////////

HRESULT  CImage::CImageWnd::_UpdateLayout(void)
{
	if (NULL == m_pShaper)
		return OLE_E_BLANK;
	
	m_pShaper->RecalcLayout();
	const RECT rcArea = m_pShaper->GetRectangle(CImageShaperDefaultImpl::eRT_Entire);
		TWindow::SetWindowPos(NULL, &rcArea, SWP_NOZORDER|SWP_NOACTIVATE);

	return S_OK;
}

////////////////////////////////////////////////////////////////////////////

CImage::CImage(IRenderer* pParentRenderer):
	m_wnd(*this),
	m_ctrlId(0),
	m_resId(0),
	m_bk_clr(RGB(0xff, 0xff, 0xff), 254)
{
	m_wnd.m_pParentRenderer = pParentRenderer;
}

CImage::~CImage(void)
{
}

////////////////////////////////////////////////////////////////////////////

const
CColour&   CImage::BackColor(void)const
{
	return m_bk_clr;
}

VOID       CImage::BackColor(const COLORREF clr, const BYTE _alpha)
{
	m_bk_clr.SetColorFromRGBA(clr, _alpha);
}

HRESULT    CImage::Clear(void)
{
	HRESULT hr_ = m_wnd.m_image_ptr.Destroy();
	if (m_wnd.IsWindow())
	{
		m_wnd.Invalidate();
	}
	m_resId = 0;
	return  hr_;
}

HRESULT    CImage::Create(const HWND hParent, const RECT& rcArea, const HWND hAfter, const UINT ctrlId)
{
	if (!::IsWindow(hParent))
		return OLE_E_INVALIDHWND;

	if (m_wnd.IsWindow())
		return HRESULT_FROM_WIN32(ERROR_ALREADY_EXISTS);

	if (::IsRectEmpty(&rcArea))
		return E_INVALIDARG;

	RECT rc_ = rcArea;
	DWORD dStyle = WS_CHILD|WS_VISIBLE|WS_CLIPCHILDREN;

	m_wnd.Create(hParent, rc_, NULL, dStyle);
	if (!m_wnd.IsWindow())
		return HRESULT_FROM_WIN32(::GetLastError());

	if (NULL == hAfter)
		m_wnd.SetWindowPos(NULL, 0, 0, 0, 0, SWP_NOACTIVATE|SWP_NOMOVE|SWP_NOSIZE|SWP_NOZORDER);
	else
		m_wnd.SetWindowPos(hAfter, 0, 0, 0, 0, SWP_NOACTIVATE|SWP_NOMOVE|SWP_NOSIZE);
	m_ctrlId = ctrlId;
	return S_OK;
}

HRESULT    CImage::Destroy(void)
{
	if (!m_wnd.IsWindow())
		return S_FALSE;
	m_wnd.SendMessage(WM_CLOSE);
	m_wnd.m_image_ptr.Destroy();
	return S_OK;
}

const
CPngBitmapPtr& CImage::GetBitmapPtrRef(void)const
{
	return m_wnd.m_image_ptr;
}

UINT       CImage::GetIdentifier(void)const
{
	return m_ctrlId;
}

IRenderer* CImage::GetParentRendererPtr(void)const
{
	return m_wnd.m_pParentRenderer;
}

IRenderer* CImage::GetRendererPtr(void)const
{
	return m_wnd.m_pRenderer;
}

UINT       CImage::GetResourceId(void)const
{
	return m_resId;
}

HRESULT    CImage::GetSize(SIZE& size) const
{
	if (!m_wnd.m_image_ptr.IsValidObject())
	{
		return m_wnd.m_image_ptr.GetLastResult();
	}
	size = m_wnd.m_image_ptr.GetObjectRef().GetSize();
	return S_OK;
}

DWORD      CImage::Style(void) const
{
	return m_wnd.m_dwStyle;
}

HRESULT    CImage::Style(const DWORD dwStyle)
{
	m_wnd.m_dwStyle = dwStyle;
	return S_OK;
}

CWindow&   CImage::GetWindow_Ref(void)
{
	return m_wnd;
}

HRESULT    CImage::Refresh(void)
{
	if (!m_wnd)
		return OLE_E_BLANK;
	m_wnd.Invalidate(TRUE);
	return S_OK;
}

HRESULT    CImage::SetImage(const UINT nResId)
{
	if (!nResId)
		return E_INVALIDARG;
	if (nResId == m_resId)
		return S_OK;
	const HINSTANCE hInstance = ::ATL::_AtlBaseModule.GetModuleInstance();

	Gdiplus::Bitmap* pBitmap = NULL;
	HRESULT hr_ = CGdiPlusPngLoader::LoadResource(nResId, hInstance, pBitmap);
	if (S_OK != hr_)
		return  hr_;

	hr_ = m_wnd.m_image_ptr.Attach(pBitmap);
	hr_ = m_wnd.m_image_ptr.GetLastResult();
	if (S_OK == hr_ && m_wnd.IsWindow())
	{
		m_wnd.Invalidate();
		m_resId = nResId;
	}
	return  hr_;
}

HRESULT    CImage::SetParentNotifyPtr(IControlNotify* p__)
{
	m_wnd.m_pNotify = p__;
	return S_OK;
}

HRESULT    CImage::SetParentRendererPtr(IRenderer* p__)
{
	m_wnd.m_pParentRenderer = p__;
	return S_OK;
}

HRESULT    CImage::UpdateLayout(void)
{
	HRESULT hr_ = m_wnd._UpdateLayout();
	return  hr_;
}