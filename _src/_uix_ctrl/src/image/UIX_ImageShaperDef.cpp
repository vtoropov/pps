/*
	Created by Tech_dog (VToropov) on 7-Feb-2015 at 7:27:42PM, GMT+3, Taganrog, Saturday;
	This is UIX library image control shaper class implementation file.
*/
#include "StdAfx.h"
#include "UIX_ImageShaperDef.h"

using namespace ex_ui;
using namespace ex_ui::controls;
using namespace ex_ui::controls::_impl;

using namespace ex_ui::draw::common;

////////////////////////////////////////////////////////////////////////////

namespace ex_ui { namespace controls { namespace _impl { namespace details
{
	class CImageLayout
	{
	private:
		::ATL::CWindow&    m_ctrl_ref;
	public:
		CImageLayout(::ATL::CWindow& ctrl_ref) : m_ctrl_ref(ctrl_ref){}
		~CImageLayout(void){}
	public:
		HRESULT        RecalcRectangle(const CPngBitmapPtr& img_ptr_ref, const DWORD dStyle, RECT& __in_out_ref)
		{
			SIZE szImage = {0};
			HRESULT hr_  = CImageLayout::GetImageSize(img_ptr_ref, szImage);
			if (S_OK != hr_)
				return  hr_;
			hr_ = this->RecalcRectangle(szImage, dStyle, __in_out_ref);
			return  hr_;
		}
		HRESULT        RecalcRectangle(const SIZE& img_size_ref, const DWORD dwStyle, RECT& __in_out_ref)
		{
			dwStyle;
			RECT rcArea = {0};
			m_ctrl_ref.GetWindowRect(&rcArea);
			::ATL::CWindow parent = m_ctrl_ref.GetParent();
			::MapWindowPoints(HWND_DESKTOP, parent, (LPPOINT)&rcArea, 0x2);

			rcArea.top = rcArea.top + (__H(rcArea) - img_size_ref.cy) / 2;
			rcArea.left = rcArea.left  + (__W(rcArea) - img_size_ref.cx) / 2;
			rcArea.right = rcArea.left + img_size_ref.cx;
			rcArea.bottom = rcArea.top + img_size_ref.cy;

			__in_out_ref = rcArea;

			return S_OK;
		}
	public:
		static HRESULT GetImageSize(const CPngBitmapPtr& img_ptr_ref, SIZE& __in_out_ref)
		{
			if (false == img_ptr_ref.IsValidObject())
				return img_ptr_ref.GetLastResult();
			CPngBitmap* pPngBitmap = img_ptr_ref.GetObject();
			if (NULL == pPngBitmap)
				return E_UNEXPECTED;
			Gdiplus::Bitmap* pBitmap = pPngBitmap->GetPtr();
			if (NULL == pBitmap)
				return E_UNEXPECTED;
			SIZE szImage = {pBitmap->GetWidth(), pBitmap->GetHeight()};
			__in_out_ref = szImage;
			return S_OK;
		}
	};
}}}}

////////////////////////////////////////////////////////////////////////////

CImageShaperDefaultImpl::CImageShaperDefaultImpl(CImage& ctrl_ref, CPngBitmapPtr& img_ref):
m_ctrl_ref(ctrl_ref),
m_image_ref(img_ref)
{
	::memset((void*)m_rects, 0, sizeof(RECT)*_countof(m_rects));
}

CImageShaperDefaultImpl::~CImageShaperDefaultImpl(void)
{
}

////////////////////////////////////////////////////////////////////////////

RECT         CImageShaperDefaultImpl::GetDefaultWindowRect(void) const
{
	RECT rc_ = {0, 0, 100, 100};
	if (m_image_ref.IsValidObject())
	{
		const SIZE sz_ = m_image_ref.GetObjectRef().GetSize();
		rc_.right  = sz_.cx;
		rc_.bottom = sz_.cy;
	}
	return rc_;
}

const RECT&  CImageShaperDefaultImpl::GetRectangle(const CImageShaperDefaultImpl::eRectangleType eType) const
{
	switch (eType)
	{
	case CImageShaperDefaultImpl::eRT_Entire:    return m_rects[eType];
	case CImageShaperDefaultImpl::eRT_Image:     return m_rects[eType];
	default:
		ATLASSERT(FALSE);
	}
	return m_rects[0];
}

HRESULT      CImageShaperDefaultImpl::RecalcLayout(void)
{
	RECT rc_ = {0};
	details::CImageLayout layout_(m_ctrl_ref.GetWindow_Ref());

	layout_.RecalcRectangle(m_image_ref, m_ctrl_ref.Style(), rc_);
	m_rects[CImageShaperDefaultImpl::eRT_Entire] = rc_;

	SIZE szImage = {0};
	HRESULT hr_ = details::CImageLayout::GetImageSize(m_image_ref, szImage);
	if (S_OK != hr_)
		return  hr_;

	if (!szImage.cx) // dummy check but we never get "divided by zero" exception!
		return (hr_ = E_INVALIDARG);

	RECT draw_ = {0};
	if (this->_AlignToImage())
	{
		draw_ = rc_; // client rectangle is already re-calculated in accordance with image size and an anchor
		::OffsetRect(&draw_, -draw_.left, -draw_.top);
	}
	else if (this->_StretchImage())
	{
		const float factor_ = (float(szImage.cy) / float(szImage.cx));
		if (factor_ > 1.0f) // some sort of portrait picture
		{
			if (szImage.cy > __H(rc_))
			{
				const INT width_ = INT(float(__H(rc_))/factor_);
				const INT left_ = (__W(rc_) - width_) / 2;
				::SetRect(&draw_, left_, 0, left_ + width_, __H(rc_));
			}
			else
			{
				const INT top_  = (__H(rc_) - szImage.cy) / 2;
				const INT left_ = (__W(rc_) - szImage.cx) / 2;
				::SetRect(&draw_, left_, top_, left_ + szImage.cx, top_ + szImage.cy);
			}
		}
		else // some sort of landscape picture
		{
			if (szImage.cx > __W(rc_))
			{
				const INT height_ = INT(float(__W(rc_)) * factor_);
				const INT top_ = (__H(rc_) - height_) / 2;
				::SetRect(&draw_, 0, top_, __W(rc_), top_ + height_);
			}
			else
			{
				const INT top_  = (__H(rc_) - szImage.cy) / 2;
				const INT left_ = (__W(rc_) - szImage.cx) / 2;
				::SetRect(&draw_, left_, top_, left_ + szImage.cx, top_ + szImage.cy);
			}
		}
	}
	else
	{
		const INT left_ = rc_.left + (__W(rc_) - szImage.cx) / 2;
		const INT top_ = rc_.top + (__H(rc_) - szImage.cy) / 2;
		::SetRect(&draw_, left_, top_, left_ + szImage.cx, top_ + szImage.cy);
	}
	m_rects[CImageShaperDefaultImpl::eRT_Image] = draw_;
	return  hr_;
}

////////////////////////////////////////////////////////////////////////////

bool         CImageShaperDefaultImpl::_AlignToImage(void) const
{
	return true;
}

bool         CImageShaperDefaultImpl::_StretchImage(void) const
{
	return (0 != (m_ctrl_ref.Style() & eImageStyle::eStretch));
}