#ifndef _UIXCTRLBUTTONCTRLWINDOW_H_60162DBE_A995_4d16_BAF3_87148A3B1B08_INCLUDED
#define _UIXCTRLBUTTONCTRLWINDOW_H_60162DBE_A995_4d16_BAF3_87148A3B1B08_INCLUDED
/*
	Created by Tech_dog(VToropov) on 8-Feb-2015 at 6:58:21pm, GMT+3, Taganrog, Sunday;
	This is UIX library custom button control window declaration file.
*/
#include "UIX_ControlBase.h"

namespace ex_ui { namespace controls { namespace _impl
{
	class CButtonWnd:
		public  ::ATL::CWindowImpl<CButtonWnd>,
		public  CControlBase
	{
		typedef ::ATL::CWindowImpl<CButtonWnd> TWindow;
	public:
		CButtonWnd(CControlCrt&);
		~CButtonWnd(void);
	public:
		DECLARE_WND_CLASS(_T("ex_ui::controls::Button::Window"));
		BEGIN_MSG_MAP(CButtonWnd)
			MESSAGE_HANDLER(WM_CREATE      , OnCreate     )
			MESSAGE_HANDLER(WM_DESTROY     , OnDestroy    )
			MESSAGE_HANDLER(WM_ENABLE      , OnEnable     )
			MESSAGE_HANDLER(WM_LBUTTONDOWN , OnLButtonDown)
			MESSAGE_HANDLER(WM_LBUTTONUP   , OnLButtonUp  )
			MESSAGE_HANDLER(WM_ERASEBKGND  , OnEraseBkgnd )
			MESSAGE_HANDLER(WM_PAINT       , OnPaint      )
			MESSAGE_HANDLER(WM_MOUSEMOVE   , OnMouseMove  )
			MESSAGE_HANDLER(WM_MOUSELEAVE  , OnMouseLeave )
		END_MSG_MAP()
	private:
		LRESULT OnCreate     (UINT, WPARAM, LPARAM, BOOL&);
		LRESULT OnDestroy    (UINT, WPARAM, LPARAM, BOOL&);
		LRESULT OnEnable     (UINT, WPARAM, LPARAM, BOOL&);
		LRESULT OnLButtonDown(UINT, WPARAM, LPARAM, BOOL&);
		LRESULT OnLButtonUp  (UINT, WPARAM, LPARAM, BOOL&);
		LRESULT OnEraseBkgnd (UINT, WPARAM, LPARAM, BOOL&);
		LRESULT OnMouseLeave (UINT, WPARAM, LPARAM, BOOL&);
		LRESULT OnMouseMove  (UINT, WPARAM, LPARAM, BOOL&);
		LRESULT OnPaint      (UINT, WPARAM, LPARAM, BOOL&);
	public:
		virtual void    GetDefaultSize(SIZE&) override sealed;
		virtual HRESULT RecalcWindowRect(RECT& _rc_out) override sealed;
	public:
		HRESULT         SetState(const DWORD);
	};
}}}

#endif/*_UIXCTRLBUTTONCTRLWINDOW_H_60162DBE_A995_4d16_BAF3_87148A3B1B08_INCLUDED*/