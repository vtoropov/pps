/*
	Created by Tech_dog(VToropov) on 8-Feb-2015 at 7:14:13pm, GMT+3, Taganrog, Sunday;
	This is UIX library custom button control window implementation file.
*/
#include "StdAfx.h"
#include "UIX_ButtonWnd.h"

using namespace ex_ui;
using namespace ex_ui::controls;
using namespace ex_ui::controls::_impl;

////////////////////////////////////////////////////////////////////////////

namespace ex_ui { namespace controls { namespace _impl { namespace details
{
	class ButtonControl_DefaultShaper
	{
	public:
		enum _enum {
			default_h  = 16,
			default_w  = 25,
		};
	};
}}}}

////////////////////////////////////////////////////////////////////////////

CButtonWnd::CButtonWnd(CControlCrt& crt_ref) : CControlBase(*this, crt_ref)
{
}

CButtonWnd::~CButtonWnd(void)
{
	if (TWindow::IsWindow())
		TWindow::SendMessage(WM_CLOSE);
}

////////////////////////////////////////////////////////////////////////////

LRESULT CButtonWnd::OnCreate     (UINT, WPARAM, LPARAM, BOOL&)
{
	CControlBase::m_state.Reset();
	CControlBase::m_bTracked = false;
	return 0;
}

LRESULT CButtonWnd::OnDestroy    (UINT, WPARAM, LPARAM, BOOL&)
{
	CControlBase::m_images.Clear();
	return 0;
}

LRESULT CButtonWnd::OnEnable     (UINT, WPARAM wParam, LPARAM, BOOL&)
{
	const bool bEnabled = (FALSE != wParam);
	if (true == bEnabled)
		CControlBase::m_state.dwState = eControlState::eNormal;
	else
		CControlBase::m_state.dwState = eControlState::eDisabled;
	CControlBase::Refresh(true);
	return 0;
}

LRESULT CButtonWnd::OnEraseBkgnd (UINT, WPARAM wParam, LPARAM, BOOL&)
{
	HDC hDC  = (HDC)wParam;
	RECT rc_ = {0};
	TWindow::GetClientRect(&rc_);
	CZBuffer dc_(hDC, rc_);
	// 1) draw own background
	const CColour& clr_bk = CControlBase::m_crt.BackColor(); 
	dc_.DrawSolidRect(rc_, clr_bk);
	// 2) draws a parent background
	CControlBase::DrawBkgnd(dc_);
	// 3) draws an image as button body
	CControlBase::DrawImage(dc_);
	return 0;
}

LRESULT CButtonWnd::OnLButtonDown(UINT, WPARAM, LPARAM, BOOL& bHandled)
{
	bHandled = TRUE;
	m_state.dwState = eControlState::ePressed;
	CControlBase::Refresh(false);
	return 0;
}

LRESULT CButtonWnd::OnLButtonUp  (UINT, WPARAM, LPARAM lParam, BOOL& bHandled)
{
	bHandled = TRUE;
	RECT rc_ = {0};
	const POINT pt_ = {GET_X_LPARAM(lParam), GET_Y_LPARAM(lParam)};
	TWindow::GetClientRect(&rc_);
	CControlBase::m_state.dwState = (::PtInRect(&rc_, pt_) ? eControlState::eHovered : eControlState::eNormal);
	if (!TWindow::IsWindowEnabled())
		CControlBase::m_state.dwState = eControlState::eDisabled;
	CControlBase::Refresh(false);
	if (CControlBase::m_state.IsHovered()) // a user releases a left mouse button over the button control, that is, a click event occurs
	{
		CControlBase::m_crt.CtrlEventSink_Ref().IControlNotify_OnClick(CControlBase::m_crt.CtrlId());
	}
	return 0;
}

LRESULT CButtonWnd::OnMouseLeave (UINT, WPARAM, LPARAM, BOOL& bHandled)
{
	bHandled = TRUE;
	const bool b_mouse_leave = CControlBase::m_bTracked;
	if (b_mouse_leave)
	{
		// draws normal state
		CControlBase::m_state.dwState = eControlState::eNormal;
		if (!TWindow::IsWindowEnabled())
			CControlBase::m_state.dwState = eControlState::eDisabled;
		CControlBase::Refresh(false);
	}
	CControlBase::m_bTracked = false;
	return 0;
}

LRESULT CButtonWnd::OnMouseMove  (UINT, WPARAM, LPARAM, BOOL& bHandled)
{
	bHandled = TRUE;
	const bool b_mouse_enter = !CControlBase::m_bTracked;
	CControlBase::SetMouseTrack();
	if (b_mouse_enter)
	{
		// draws hovered state
		CControlBase::m_state.dwState = eControlState::eHovered;
		if (!TWindow::IsWindowEnabled())
			CControlBase::m_state.dwState = eControlState::eDisabled;
		CControlBase::Refresh(false);
	}
	return 0;
}

LRESULT CButtonWnd::OnPaint      (UINT, WPARAM, LPARAM, BOOL& bHandled)
{
	bHandled = TRUE;
	WTL::CPaintDC dc_(TWindow::m_hWnd);
	this->OnEraseBkgnd(0, (WPARAM)(HDC)dc_, 0, bHandled);
	return 0;
}

////////////////////////////////////////////////////////////////////////////

void    CButtonWnd::GetDefaultSize(SIZE& sz_)
{
	sz_.cx = details::ButtonControl_DefaultShaper::default_w;
	sz_.cy = details::ButtonControl_DefaultShaper::default_h;
}

HRESULT CButtonWnd::RecalcWindowRect(RECT& _rc_out)
{
	HRESULT hr_ = CControlBase::RecalcWindowRect(_rc_out);
	return  hr_;
}

////////////////////////////////////////////////////////////////////////////

HRESULT CButtonWnd::SetState(const DWORD dwState)
{
	const bool bChanged = (dwState != CControlBase::m_state.dwState);
	if (bChanged)
		CControlBase::m_state.dwState = dwState;
	return (bChanged ? S_OK : S_FALSE);
}