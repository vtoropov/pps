/*
	Created by Tech_dog (VToropov) on 17-Mar-2015 at 1:56:57pm, GMT+3, Taganrog, Tuesday;
	This is UIX library label control window class implementation file.
*/
#include "StdAfx.h"
#include "UIX_LabelWnd.h"

using namespace ex_ui;
using namespace ex_ui::controls;
using namespace ex_ui::controls::_impl;

#include "UIX_GdiProvider.h"

using namespace ex_ui::draw;
////////////////////////////////////////////////////////////////////////////

CLabelWnd::CLabelWnd(CControlCrt& crt_ref) : m_crt(crt_ref), m_fore(::GetSysColor(COLOR_WINDOWTEXT))
{
}

CLabelWnd::~CLabelWnd(void)
{
}

////////////////////////////////////////////////////////////////////////////

LRESULT CLabelWnd::OnEraseBkgnd (UINT, WPARAM wParam, LPARAM, BOOL&)
{
	RECT rc_ = {0};
	TWindow::GetClientRect(&rc_);
	const HDC hDC = reinterpret_cast<HDC>(wParam);
	CZBuffer dc_(hDC, rc_);
	// 1) draws parent background
	m_crt.CtrlParentRenderer_Ref().DrawParentBackground(TWindow::m_hWnd, dc_, rc_);
	// 2) draws the text
	{
		::ATL::CAtlString csText;
		TWindow::GetWindowText(csText);
		LPCTSTR pszText = csText.GetString();
		dc_.DrawTextExt(pszText, _T("Calibri"), 24, rc_, m_fore, DT_CENTER|DT_VCENTER|DT_WORDBREAK|DT_NOCLIP);
	}
	return 0;
}

LRESULT CLabelWnd::OnPaint      (UINT, WPARAM, LPARAM, BOOL&)
{
	WTL::CPaintDC dc_(m_hWnd);
	return 0;
}

LRESULT CLabelWnd::OnTextChange (UINT, WPARAM, LPARAM, BOOL& bHandled)
{
	bHandled = FALSE;
	TWindow::RedrawWindow(NULL, NULL, RDW_ERASE|RDW_INVALIDATE|RDW_ERASENOW);
	return 0;
}

////////////////////////////////////////////////////////////////////////////

HRESULT CLabelWnd::ForeColor(const COLORREF _clr)
{
	const bool bChanged = (_clr != m_fore);
	if (bChanged)
		m_fore = _clr;
	return (bChanged ? S_OK : S_FALSE);
}