#ifndef _UIXCTRLLIBRARYPRECOMPILEDHEADER_H_6B4C4186_CC8B_45ca_98D7_F7703D25F571_INCLUDED
#define _UIXCTRLLIBRARYPRECOMPILEDHEADER_H_6B4C4186_CC8B_45ca_98D7_F7703D25F571_INCLUDED
/*
	Created by Tech_dog(VToropov) on 9-Feb-2015 at 7:12:59pm, GMT+3, Taganrog, Monday;
	This is UIX Control library precompiled headers definition file.
*/

#define WIN32_LEAN_AND_MEAN

#ifndef  WINVER
#define  WINVER         0x0501  // this is for use Windows XP (or later) specific feature(s)
#endif   WINVER

#ifndef _WIN32_WINNT
#define _WIN32_WINNT    0x0501  // this is for use Windows XP (or later) specific feature(s)
#endif  _WIN32_WINNT

#ifndef _WIN32_IE
#define _WIN32_IE       0x0603  // this is for use IE 6 SP3 (or later) specific feature(s)
#endif  _WIN32_IE

#pragma warning(disable: 4481)  // nonstandard extension used: override specifier 'override'
#pragma warning(disable: 4996)  // security warning: function or variable may be unsafe

#include <atlbase.h>
#include <atlwin.h>
#include <atlstr.h>
#include <atlsafe.h>
#include <comdef.h>
#include <comutil.h>
#include <vector>
#include <map>


#endif/*_UIXCTRLLIBRARYPRECOMPILEDHEADER_H_6B4C4186_CC8B_45ca_98D7_F7703D25F571_INCLUDED*/