/*
	Created by Tech_dog (VToropov) on 19-Mar-2014 at 11;25:35am, GMT+3, Taganrog, Wednesday;
	This is Shared Lite Generic Asynchronous Event Runnable Object class implementation file.
*/
#include "StdAfx.h"
#include "Shared_GenericRunnableObject.h"

using namespace shared;
using namespace shared::lite;
using namespace shared::lite::events;

////////////////////////////////////////////////////////////////////////////

CGenericRunnableObject::CGenericRunnableObject(TRunnableFunc func, IGenericEventNotify& sink_ref, const UINT eventId):
	m_async_evt(sink_ref, eventId),
	m_hThread(NULL),
	m_hEvent(NULL),
	m_bStopped(true),
	m_function(func)
{
	m_hEvent = ::CreateEvent(0, TRUE, TRUE, 0);
	if (INVALID_HANDLE_VALUE == m_hEvent || NULL == m_hEvent)
	{
		ATLASSERT(FALSE); m_hEvent = NULL;
	}
	m_async_evt.Create();
}

CGenericRunnableObject::~CGenericRunnableObject(void)
{
	m_async_evt.Destroy();
	if (m_hEvent)
	{
		::CloseHandle(m_hEvent); 
		m_hEvent = NULL;
	}
}

////////////////////////////////////////////////////////////////////////////

CGenericEvent&   CGenericRunnableObject::Event(void)
{
	return m_async_evt;
}

bool             CGenericRunnableObject::IsStopped(void) const
{
	return m_bStopped;
}

void             CGenericRunnableObject::MarkCompleted(void)
{
	if (m_hEvent)
	{
		::SetEvent(m_hEvent);
	}
}

HRESULT          CGenericRunnableObject::Start(void)
{
	if (NULL == m_hEvent) return OLE_E_BLANK;
	if (!IsStopped()) return S_OK;
	if (m_hThread)
	{
		::CloseHandle(m_hThread);
		m_hThread = NULL;
	}
	m_hThread  = (HANDLE)::_beginthreadex(NULL, NULL, m_function, this, CREATE_SUSPENDED, 0);
	if (INVALID_HANDLE_VALUE == m_hThread || NULL == m_hThread)
	{
		return E_OUTOFMEMORY;
	}
	m_bStopped = false;
	::ResetEvent(m_hEvent);
	::SetThreadPriority(m_hThread, BELOW_NORMAL_PRIORITY_CLASS);
	::ResumeThread(m_hThread);
	HRESULT hr__ = S_OK;
	return  hr__;
}

HRESULT          CGenericRunnableObject::Stop(const bool bForced)
{
	if (IsStopped()) return S_OK;
	m_bStopped = true;
	if (NULL == m_hThread) return S_OK;
	if (bForced)
	{
		const DWORD dwRet = ::WaitForSingleObject(m_hEvent, 2000); // wait for 5 sec
		if (WAIT_OBJECT_0!=dwRet)
		{
			if (m_hThread)
			{
				::TerminateThread(m_hThread, 0xffff);
			}
		}
	}
	else
	{
		int cnt__ = 0;
		while (WAIT_OBJECT_0 != ::WaitForSingleObject(m_hEvent, 200))
		{
			::Sleep(100);
			cnt__ ++;
			if (cnt__ > 10) // anti-blocker, actually should never happen
			{
				const bool bForced = true;
				return Stop(bForced);
			}
		}
	}
	if (m_hThread)
	{
		::CloseHandle(m_hThread);
		m_hThread = NULL;
	}
	HRESULT hr__ = S_OK;
	return  hr__;
}

HRESULT          CGenericRunnableObject::Wait(const DWORD dPeriod)
{
	const DWORD dResult = ::WaitForSingleObject(m_hEvent, (dPeriod < 1 ? INFINITE : dPeriod));
	return (WAIT_OBJECT_0 == dResult ? S_OK : S_FALSE);
}

////////////////////////////////////////////////////////////////////////////

CGenericWaitObject::CGenericWaitObject(const DWORD nTimeSlice, const DWORD nTimeFrame):
	m_nTimeSlice(nTimeSlice),
	m_nTimeFrame(nTimeFrame),
	m_nCurrent(0)
{
	ATLASSERT(m_nTimeSlice);
	ATLASSERT(m_nTimeFrame);
}

CGenericWaitObject::~CGenericWaitObject(void)
{
}

////////////////////////////////////////////////////////////////////////////

bool CGenericWaitObject::IsElapsed(void) const
{
	return (m_nCurrent >= m_nTimeFrame);
}

bool CGenericWaitObject::IsReset(void) const
{
	return (m_nCurrent == 0);
}

void CGenericWaitObject::Reset(const DWORD nTimeSlice, const DWORD nTimeFrame)
{
	if (nTimeSlice != 0xffffffff) m_nTimeSlice = nTimeSlice;
	if (nTimeFrame != 0xffffffff) m_nTimeFrame = nTimeFrame;
	ATLASSERT(m_nTimeSlice);
	ATLASSERT(m_nTimeFrame);
	m_nCurrent = 0;
}

void CGenericWaitObject::Wait(void)
{
	::Sleep(m_nTimeSlice);
	m_nCurrent += m_nTimeSlice;
}