/*
	Created by Tech_dog (VToropov) on 19-Mar-2014 at 8:55:09am, GMT+3, Taganrog, Wednesday;
	This is Shared Lite Raw Data class(es) implementation file.
*/
#include "StdAfx.h"
#include "Shared_RawData.h"

using namespace shared;
using namespace shared::lite;
using namespace shared::lite::data;

////////////////////////////////////////////////////////////////////////////

CByteArray::CByteArray(void):
	m_pData(NULL),
	m_hResult(OLE_E_BLANK),
	m_size(0)
{
}

CByteArray::CByteArray(const _variant_t& var_ref):
	m_pData(NULL),
	m_hResult(OLE_E_BLANK),
	m_size(0)
{
	CRawDataAccessor acc_rw(var_ref);
	m_hResult = acc_rw.Error();
	if (S_OK == m_hResult)
	{
		LONG nSize = 0;
		m_hResult = acc_rw.GetSize(nSize);
		if (S_OK == m_hResult)
		{
			PBYTE pData = acc_rw.AccessData();
			try
			{
				m_pData = new BYTE[nSize];
				::memcpy((void*)m_pData, (const void*)pData, nSize);
				m_size = (INT)nSize;
			}
			catch(::std::bad_alloc&)
			{
				m_hResult = E_OUTOFMEMORY;
			}
		}
	}
}

CByteArray::CByteArray(const INT nSize):
	m_pData(NULL),
	m_hResult(OLE_E_BLANK),
	m_size(nSize)
{
	if (nSize < 1)
		m_hResult = E_INVALIDARG;
	else
	{
		try
		{
			m_pData = new BYTE[nSize];
			::memset((void*)m_pData, 0, sizeof(BYTE) * nSize);
			m_hResult = S_OK;
		}
		catch(::std::bad_alloc&)
		{
			m_hResult = E_OUTOFMEMORY;
			TRACE_FUNC();
			TRACE_ERR__a1(_T("Error occurred during the allocation of the required memory block of %d byte(s);"), nSize);
		}
	}
}

CByteArray::CByteArray(const SAFEARRAY* psa):
	m_pData(NULL),
	m_hResult(OLE_E_BLANK),
	m_size(0)
{
	if (!psa)
		m_hResult = E_INVALIDARG;
	else
	{
		SAFEARRAY* psa_local = const_cast<SAFEARRAY*>(psa);
		CRawDataAccessor raw_acc(psa_local);
		m_hResult = raw_acc.Error();
		if (S_OK != m_hResult)
			return;
		LONG n_size = 0;
		m_hResult = raw_acc.GetSize(n_size);
		if (S_OK != m_hResult)
			return;
		if (n_size < 1)
		{
			m_hResult = E_FAIL;
			return;
		}
		try
		{
			m_pData = new BYTE[n_size];
			::memcpy((void*)m_pData, (const void*)raw_acc.AccessData(), n_size);
			m_size = (INT)n_size;
		}
		catch(::std::bad_alloc&)
		{
			m_hResult = E_OUTOFMEMORY;
			TRACE_FUNC();
			TRACE_ERR__a1(_T("Error occurred during the allocation of the required memory block of %d byte(s);"), n_size);
		}
	}
}

CByteArray::CByteArray(PBYTE pData, const INT nSize):
	m_pData(NULL),
	m_hResult(OLE_E_BLANK),
	m_size(nSize)
{
	this->Attach(pData, nSize);
}

CByteArray::~CByteArray(void)
{
	if (m_pData)
	{
		try
		{
			delete [] m_pData; m_pData = NULL;
		}
		catch (...)
		{
			TRACE_FUNC();
			TRACE_ERROR(_T("Error occurred while trying to free the allocated memory block"));
			ATLASSERT(0);
		}
	}
	m_size = 0;
	m_hResult = OLE_E_BLANK;
}

////////////////////////////////////////////////////////////////////////////

HRESULT      CByteArray::Attach(PBYTE pData, const INT nSize)
{
	if (!pData || nSize < 1)
		return E_INVALIDARG;
	if (m_pData)
	{
		try { delete [] m_pData; m_pData = NULL; } catch (...){ ATLASSERT(0); }
	}
	m_hResult = S_OK;
	m_pData = pData;
	m_size = nSize;
	return m_hResult;
}

HRESULT      CByteArray::CopyTo(_variant_t& var_ref)const
{
	if (VT_EMPTY != var_ref.vt)
		return HRESULT_FROM_WIN32(ERROR_OBJECT_ALREADY_EXISTS);
	var_ref.Clear();
	var_ref.parray = NULL;
	HRESULT hr__ = this->CopyTo(var_ref.parray);
	if (S_OK == hr__)
		var_ref.vt = VT_ARRAY|VT_UI1;
	return hr__;
}

HRESULT      CByteArray::CopyTo(SAFEARRAY*& ptr_ref) const
{
	if (!IsValid())
	{
		TRACE_FUNC();
		TRACE_ERROR(_T("The byte array pointer is not initialized"));
		return OLE_E_BLANK;
	}
	if (ptr_ref)
	{
		TRACE_FUNC();
		TRACE_ERROR(_T("The safe array pointer provided points to the existing object"));
		return HRESULT_FROM_WIN32(ERROR_OBJECT_ALREADY_EXISTS);
	}
	SAFEARRAYBOUND bound[1] ={0};
	bound[0].lLbound        = 0;
	bound[0].cElements      = (ULONG)m_size;
	ptr_ref = ::SafeArrayCreate(VT_UI1, 1, bound);

	if (!ptr_ref)
	{
		TRACE_FUNC();
		TRACE_ERROR(_T("Cannot allocate the safe array system memory block;"));
		return E_OUTOFMEMORY;
	}
	UCHAR HUGEP* pData = NULL;
	HRESULT hr__ = ::SafeArrayAccessData(ptr_ref, (void HUGEP**)&pData);
	if (S_OK == hr__)
	{
		errno_t err_no = ::memcpy_s((void*)pData, m_size, (const void*)m_pData, m_size);
		if (err_no)
		{
			hr__ = E_OUTOFMEMORY;
			TRACE_FUNC();
			TRACE_ERROR(_T("The error occurred while copying data to the safe array"));
		}
		::SafeArrayUnaccessData(ptr_ref);
		pData = NULL;
	}
	else
	{
		TRACE_FUNC();
		TRACE_ERROR(_T("Cannot access the safe system memory block;"));
	}
	return  hr__;
}

BYTE*        CByteArray::Detach(void)
{
	BYTE* ret__ = m_pData;
	{
		m_pData   = NULL;
		m_hResult = OLE_E_BLANK;
		m_size    = 0;
	}
	return ret__;
}

const PBYTE  CByteArray::GetData(void) const
{
	return m_pData;
}

PBYTE        CByteArray::GetData(void)
{
	return m_pData;
}

HRESULT      CByteArray::GetLastResult(void) const
{
	return m_hResult;
}

PBYTE*       CByteArray::GetPtr(void)
{
	return &m_pData;
}

PBYTE&       CByteArray::GetRef(void)
{
	return m_pData;
}

INT          CByteArray::GetSize(void) const
{
	return m_size;
}

bool         CByteArray::IsValid(void) const
{
	return (NULL != m_pData && 0 != m_size);
}

////////////////////////////////////////////////////////////////////////////

CRawDataAccessor::CRawDataAccessor(const _variant_t& var_ref):
	m_raw_data_ptr(NULL),
	m_data_ptr(NULL)
{
	m_error.Reset();
	if (!(VT_ARRAY & var_ref.vt) || !(VT_UI1 & var_ref.vt))
	{
		m_error = DISP_E_TYPEMISMATCH;
	}
	else if (!var_ref.parray)
	{
		m_error = E_INVALIDARG;
	}
	else
	{
		m_error = ::SafeArrayAccessData(var_ref.parray, (void HUGEP**)&m_data_ptr);
		if (!m_error)
			m_raw_data_ptr = var_ref.parray;
	}
}

CRawDataAccessor::CRawDataAccessor(T_RAW_DATA& raw_data_ref):
	m_raw_data_ptr(raw_data_ref.m_psa),
	m_data_ptr(NULL)
{
	if (m_raw_data_ptr)
		m_error = ::SafeArrayAccessData(m_raw_data_ptr, (void HUGEP**)&m_data_ptr);
}

CRawDataAccessor::CRawDataAccessor(SAFEARRAY* psa):
	m_raw_data_ptr(psa),
	m_data_ptr(NULL)
{
	if (m_raw_data_ptr)
		m_error = ::SafeArrayAccessData(m_raw_data_ptr, (void HUGEP**)&m_data_ptr);
}

CRawDataAccessor::~CRawDataAccessor(void)
{
	if (!m_error)
	{
		::SafeArrayUnaccessData(m_raw_data_ptr);
		m_raw_data_ptr = NULL;
		m_error.Reset();
	}
	m_data_ptr = NULL;
}

////////////////////////////////////////////////////////////////////////////

PBYTE     CRawDataAccessor::AccessData(void)
{
	return (PBYTE)m_data_ptr;
}

CONST CSysError& CRawDataAccessor::Error(void) const
{
	return m_error;
}

HRESULT   CRawDataAccessor::GetSize(LONG& n_size_ref) const
{
	if (!this->IsValid())
		return OLE_E_BLANK;
	n_size_ref   = 0;
	HRESULT hr__ = S_OK;
	{
		const UINT n_el_size = ::SafeArrayGetElemsize(m_raw_data_ptr);
		if (!n_el_size) return DISP_E_TYPEMISMATCH;
		LONG n_lbound = 0;
		hr__ = ::SafeArrayGetLBound(m_raw_data_ptr, 1, &n_lbound);
		if (S_OK != hr__) return hr__;
		LONG n_ubound = 0;
		hr__ = ::SafeArrayGetUBound(m_raw_data_ptr, 1, &n_ubound);
		if (S_OK != hr__) return hr__;
		n_size_ref = (n_ubound - n_lbound + 1) * (LONG)n_el_size;
	}
	return  hr__;
}

bool      CRawDataAccessor::IsValid(void) const
{
	return (!m_error);
}

////////////////////////////////////////////////////////////////////////////

namespace shared { namespace lite { namespace data { namespace details
{
	HRESULT Base64_CalcDecodeBufferLen(const long enc_buf_len, long& dec_buf_len) // calculates decoded string buffer length from encoded one
	{
		dec_buf_len = 0;
		if (!(enc_buf_len > 0))
			return DISP_E_BUFFERTOOSMALL;
		if (enc_buf_len % 4)
			return MSSIPOTF_E_PCONST_CHECK;
		dec_buf_len = (enc_buf_len + 3) / 4 * 3;
		return S_OK;
	}
	HRESULT Base64_CalcEncodeBufferLen(const long dec_buf_len, long& enc_buf_len)
	{
		enc_buf_len = 0;
		if (!(dec_buf_len > 0))
			return DISP_E_BUFFERTOOSMALL;
		const long iTriples = dec_buf_len / 3 + (dec_buf_len % 3 ? 1 : 0);
		enc_buf_len  = iTriples * 4;
		return S_OK;
	}

	static const CHAR Base64_Chars[] = "ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz0123456789+/";
	static const INT  Base64_Pad     = 0x3d;   // '='
}}}}

////////////////////////////////////////////////////////////////////////////

CBase64::CBase64(void)
{
	::memset((void*)m_mtx, 65, sizeof(BYTE) * _countof(m_mtx));
	// creates a decode matrix
	for (int i = 0; i <= 63; i++)
	{
		m_mtx[(INT)details::Base64_Chars[i]] = (BYTE)i;
	}
	// adds padding character
	m_mtx[details::Base64_Pad] = 64;
}

CBase64::~CBase64(void)
{
}

////////////////////////////////////////////////////////////////////////////

HRESULT      CBase64::Decode(const PBYTE pSrc, const long nSrcSize, PBYTE* ppDest, long& refBytesCopied)
{
	if (!pSrc || !ppDest)
		return E_POINTER;
	HRESULT hr = details::Base64_CalcDecodeBufferLen(nSrcSize, refBytesCopied);
	if (S_OK != hr)
		return hr;
	try
	{
		// 1) allocates a destination buffer
		BYTE* pDest = new BYTE[refBytesCopied + 1];
		pDest[refBytesCopied] = 0;
		::memset((void*)pDest,  0, sizeof(char) * refBytesCopied);
		// 2) starts to decode using decode matrix that is created in this object constructor
		long i_iter = 0;
		for (long i = 0; i < nSrcSize; i += 4, i_iter += 3)
		{
			// 2.1) puts the first base64 char on the first output buffer byte
			pDest[i_iter] = m_mtx[(int)pSrc[i]] << 2;
			// 2.2) tests for the string end
			if ((i + 1) < nSrcSize)
				if (pSrc[i + 1] != details::Base64_Pad)
				{
					// 2.3) adds to the first output byte 2 bit of the second base64 char
					pDest[i_iter] += (m_mtx[(int)pSrc[i + 1]] >> 4) & 0x3;
					// 2.4) puts remaining 4 bits of the second base64 char on the second output buffer byte
					pDest[i_iter + 1] = m_mtx[(int)pSrc[i + 1]] << 4;
					// 2.5) tests for the string end again
					if ((i + 2) < nSrcSize)
						if (pSrc[i + 2] != details::Base64_Pad)
						{
							// 2.6) adds to the second output bite 4 bit of the third base64 char
							pDest[i_iter + 1] += (m_mtx[(int)pSrc[i + 2]] >> 2) & 0xf;
							// 2.7) puts the remaining 2 bits of the third base64 char on the third output byte
							pDest[i_iter + 2]  = m_mtx[(int)pSrc[i + 2]] << 6;
							// 2.8) test for the string end again
							if ((i + 1) < nSrcSize)
								if (pSrc[i + 3] != details::Base64_Pad)
								{
									// 2.9) adds to the third output byte the forth base64 char
									BYTE bt__ = m_mtx[(int)pSrc[i + 3]];
									pDest[i_iter + 2] = pDest[i_iter + 2] + bt__;
								}
						}
				}
		}
		*ppDest = (PBYTE)pDest;
	}
	catch (::std::bad_alloc&)
	{
		hr = E_OUTOFMEMORY;
	}
	return hr;
}

HRESULT      CBase64::Encode(PBYTE pSrc, const long nSrcSize, PBYTE* ppDest, long& refBytesCopied)
{
	if (!pSrc || !ppDest)
		return E_POINTER;
	HRESULT hr = details::Base64_CalcEncodeBufferLen(nSrcSize, refBytesCopied);
	if (S_OK != hr)
		return hr;
	try
	{
		PBYTE pDest = new BYTE[refBytesCopied];
		::memset((void*)pDest, details::Base64_Pad, sizeof(BYTE) * refBytesCopied);
		DWORD dwCode      = 0;
		BYTE  uIndices[4] ={0};
		long i_iter = 0;
		for (long i = 0; i < nSrcSize; i += 3)
		{
			// 1) packs 3 byte of the source into the double word
			// 1.1) moves a first byte to the low nibble of the high word
			dwCode = pSrc[i] << 16;
			if ((i + 1) < nSrcSize)
			{
				// 1.2) moves a second byte to the high nibble of the low word
				dwCode += pSrc[i + 1] << 8;
				if ((i + 2) < nSrcSize)
				{
					// 1.3) moves a third byte to the low nibble of the low word
					dwCode += pSrc[i + 2];
				}
			}
			// 2) calculates indices to map to the ANSII character array (must be from 0...to 64, i.e. 4 indices of 6 bit shift)
			//    that is 3 bytes of the source are devided to 4 part of the target indices (to map to ANSII characters)
			uIndices[0] = (BYTE)(dwCode >> 18) & 63;
			uIndices[1] = (BYTE)(dwCode >> 12) & 63;
			uIndices[2] = (BYTE)(dwCode >>  6) & 63;
			uIndices[3] = (BYTE)(dwCode >>  0) & 63;
			// 3) writes mapped ANSII characters to the destination buffer
			pDest[i_iter + 0] = details::Base64_Chars[uIndices[0]];
			pDest[i_iter + 1] = details::Base64_Chars[uIndices[1]];
			// 3.1) checks for the tail
			if ((i + 1) < nSrcSize)
			{
				pDest[i_iter + 2] = details::Base64_Chars[uIndices[2]];
				if ((i + 2) < nSrcSize)
				{
					pDest[i_iter + 3] = details::Base64_Chars[uIndices[3]];
				}
			}
			i_iter += 4;
		}
		*ppDest = pDest;
	}
	catch (::std::bad_alloc&)
	{
		hr = E_OUTOFMEMORY;
	}
	return hr;
}

HRESULT      CBase64::IsBase64(const char* pSrc, const long nSrcSize) const
{
	if (!pSrc)
		return E_POINTER;
	if (!(nSrcSize > 0) || (nSrcSize % 4))
		return HRESULT_FROM_WIN32(ERROR_INVALID_DATA);
	// test a string provided
	long i_it  = 0;
	for (long i = 0; i < nSrcSize; i++)
	{
		i_it = (long)pSrc[i];
		switch ((int)m_mtx[i_it])
		{
		case 65:
			{
				// invalid character found
				return HRESULT_FROM_WIN32(ERROR_INVALID_DATATYPE);
			} break;
		case 64:
			{
				if (i < nSrcSize - 3)
				{
					// a padding character at invalid position
					return HRESULT_FROM_WIN32(ERROR_INVALID_DATATYPE);
				}
			} break;
		}
	}
	return S_OK;
}

////////////////////////////////////////////////////////////////////////////

HRESULT      CBase64::Decode(LPCTSTR pSrc, _variant_t& vDecoded)
{
	if (!pSrc || ::_tcslen(pSrc) < 1)
		return E_INVALIDARG;
	CT2A psza(pSrc);
	const LONG nSizeInBytes = (LONG)::strlen(psza);
	HRESULT hr_ = this->IsBase64(psza, nSizeInBytes);
	if (S_OK != hr_)
		return  hr_;
	PBYTE pRawData = NULL;
	LONG  nBytesCopied = 0;
	hr_ = this->Decode((const PBYTE)(LPSTR)psza, nSizeInBytes, &pRawData, nBytesCopied);
	if (S_OK != hr_)
		return  hr_;
	CByteArray raw_data(pRawData, nBytesCopied);
	hr_ = raw_data.GetLastResult();
	if (S_OK != hr_)
		return  hr_;
	vDecoded.Clear();
	hr_ = raw_data.CopyTo(vDecoded);
	return hr_;
}

HRESULT      CBase64::Decode(LPCTSTR pSrc, ::ATL::CAtlString& csDecoded)
{
	if (!pSrc || ::_tcslen(pSrc) < 1)
		return E_INVALIDARG;
	CT2A psza(pSrc);
	const LONG nSizeInBytes = (LONG)::strlen(psza);
	HRESULT hr_ = this->IsBase64(psza, nSizeInBytes);
	if (S_OK != hr_)
		return  hr_;
	PBYTE pRawData = NULL;
	LONG  nBytesCopied = 0;
	hr_ = this->Decode((const PBYTE)(LPSTR)psza, nSizeInBytes, &pRawData, nBytesCopied);
	if (S_OK != hr_)
		return  hr_;
	CByteArray raw_data(pRawData, nBytesCopied);
	hr_ = raw_data.GetLastResult();
	if (S_OK != hr_)
		return  hr_;
	::ATL::CAtlString cs_buffer((PCHAR)raw_data.GetRef(), nBytesCopied);
	csDecoded = cs_buffer;
	return hr_;
}

HRESULT      CBase64::Encode(const _variant_t& vSrc, ::ATL::CAtlString& csEncoded)
{
	CByteArray raw_data(vSrc);
	HRESULT hr_ = raw_data.GetLastResult();
	if (S_OK != hr_)
		return  hr_;
	PBYTE pEncoded = NULL;
	LONG  nBytesCopied = 0;
	hr_ = this->Encode(raw_data.GetRef(), (const long)raw_data.GetSize(), &pEncoded, nBytesCopied);
	if (S_OK != hr_)
		return  hr_;
	CByteArray encoded(pEncoded, (INT)nBytesCopied);
	
	if (!csEncoded.IsEmpty())csEncoded.Empty();

	::ATL::CAtlString cs_buffer((PCHAR)pEncoded, nBytesCopied);
	csEncoded = cs_buffer;
	return hr_;
}

HRESULT      CBase64::Encode(const ::ATL::CAtlString& cs_src, ::ATL::CAtlString& csEncoded)
{
	if (cs_src.IsEmpty())
		return E_INVALIDARG;
	CT2A psza(cs_src.GetString());
	const LONG nSizeInBytes = (LONG)::strlen(psza);
	PBYTE pRawData = NULL;
	LONG  nBytesCopied = 0;
	HRESULT hr_ = this->Encode((const PBYTE)(LPSTR)psza, nSizeInBytes, &pRawData, nBytesCopied);
	if (S_OK != hr_)
		return  hr_;
	CByteArray raw_data(pRawData, nBytesCopied);
	hr_ = raw_data.GetLastResult();
	if (S_OK != hr_)
		return  hr_;
	::ATL::CAtlString cs_buffer((PCHAR)raw_data.GetRef(), nBytesCopied);
	csEncoded = cs_buffer;
	return hr_;
}

////////////////////////////////////////////////////////////////////////////

namespace shared { namespace lite { namespace data
{
	INT   CompareSafeArrays(const _variant_t& var_l, const _variant_t& var_r)
	{
		if (var_l.vt != var_r.vt)
			return -1;
		if (0 == (var_l.vt & VT_ARRAY))
			return -1;
		;
		if (var_l.parray == var_r.parray)
			return 0;
		CRawDataAccessor acc_l(var_l);
		CRawDataAccessor acc_r(var_r);
		
		if (!acc_r.IsValid() &&  acc_l.IsValid()) return  1;
		if (!acc_l.IsValid() &&  acc_r.IsValid()) return -1;
		if (!acc_r.IsValid() && !acc_l.IsValid()) return  0;

		LONG size_l = 0;
		LONG size_r = 0;
		
		if (S_OK != acc_l.GetSize(size_l)) return -1;
		if (S_OK != acc_r.GetSize(size_r)) return  1;

		if (size_l > size_r) return  1;
		if (size_l < size_r) return -1;

		PVOID pData_l = (PVOID)acc_l.AccessData();
		PVOID pData_r = (PVOID)acc_r.AccessData();

		if (!pData_l &&  pData_r) return -1;
		if (!pData_r &&  pData_l) return  1;
		if (!pData_r && !pData_l) return  0;

		return ::memcmp(pData_l, pData_r, size_l);
	}
}}}