/*
	Created by Tech_dog (VToropov) on 19-Mar-2014 at 7:06:34am, GMT+4, Taganrog, Wednesday;
	This is Shared Lite System Error class implementation file.
*/
#include "StdAfx.h"
#include "Shared_SystemError.h"

using namespace shared::lite::common;

/////////////////////////////////////////////////////////////////////////////

namespace shared { namespace lite { namespace common { namespace details
{
	void   SysError_FormatMessage(const DWORD dwError, ::ATL::CAtlString& buffer_ref)
	{
		TCHAR szBuffer[_MAX_PATH] = {0};
	
		::FormatMessage(
				FORMAT_MESSAGE_FROM_SYSTEM,
				NULL,
				dwError,
				MAKELANGID(LANG_NEUTRAL, SUBLANG_CUSTOM_DEFAULT),
				szBuffer,
				_MAX_PATH - 1,
				NULL
			);
		buffer_ref = szBuffer;
	}

	void   SysError_FormatMessage(const HRESULT hError, ::ATL::CAtlString& buffer_ref)
	{
		TCHAR szBuffer[_MAX_PATH] = {0};
	
		::FormatMessage(
				FORMAT_MESSAGE_FROM_SYSTEM,
				NULL,
				hError,
				MAKELANGID(LANG_NEUTRAL, SUBLANG_CUSTOM_DEFAULT),
				szBuffer,
				_MAX_PATH - 1,
				NULL
			);
		buffer_ref = szBuffer;
	}

	void   SysError_NormalizeMessage(::ATL::CAtlString& buffer_ref)
	{
		if (!buffer_ref.IsEmpty())
		{
			buffer_ref.Replace(_T("\r\n"), _T(" "));
			buffer_ref.Replace(_T("\r")  , _T(" "));
			buffer_ref.Replace(_T("\n")  , _T(" "));
		}
	}
}}}}

/////////////////////////////////////////////////////////////////////////////

CSysError::CSysError(void) :
	m_dwError(ERROR_SUCCESS),
	m_hrError(S_OK)
{
}

CSysError::CSysError(const DWORD dwError) :
	m_dwError(dwError),
	m_hrError(HRESULT_FROM_WIN32(dwError))
{
	details::SysError_FormatMessage(m_dwError, m_buffer);
	details::SysError_NormalizeMessage(m_buffer);
}

CSysError::CSysError(const HRESULT hrError):
	m_dwError(CSysError::dwEmpty),
	m_hrError(OLE_E_BLANK)
{
	this->SetHresult(hrError);
}

CSysError::~CSysError(void)
{
}

/////////////////////////////////////////////////////////////////////////////

void       CSysError::Clear(void)
{
	m_dwError = ERROR_SUCCESS;
	m_hrError = HRESULT_FROM_WIN32(m_dwError);
	m_buffer  = _T("No error");
}

LPCTSTR    CSysError::GetDescription(void) const
{
	return m_buffer.GetString();
}

CAtlString CSysError::GetFormattedDetails(const bool bMultiline)const
{
	static LPCTSTR lpszPattern = _T(" code=0x%x; desc=%s");

	CAtlString cs_pattern(lpszPattern);
	if (bMultiline)
		cs_pattern.Replace(_T("; "), _T("\n\t\t"));

	CAtlString cs_details;
	cs_details.Format(
			cs_pattern,
			this->GetHresult(),
			this->HasDetails() ? this->GetDescription() : _T("#n/a")
		);
	return cs_details;
}

HRESULT    CSysError::GetHresult(void) const
{
	return m_hrError;
}

bool       CSysError::HasDetails(void) const
{
	return !m_buffer.IsEmpty();
}

void       CSysError::Reset(void)
{
	m_dwError = CSysError::dwEmpty;
	this->SetHresult(OLE_E_BLANK);
}

void       CSysError::SetHresult(const HRESULT hResult)
{
	m_hrError = hResult;
	if (S_OK == m_hrError)
	{
		m_dwError= ERROR_SUCCESS;
		if (!m_buffer.IsEmpty())m_buffer.Empty();
		return;
	}
	::ATL::CComPtr<IErrorInfo> sp;
	if (S_OK == ::GetErrorInfo(0, &sp) && sp)
	{
		_com_error err(m_hrError, sp, true);
		m_buffer = (LPCTSTR)err.Description();
	}
	else
	{
		_com_error err(m_hrError);
		m_buffer = (LPCTSTR)err.Description();
	}
	if (m_buffer.IsEmpty())
		details::SysError_FormatMessage(m_hrError, m_buffer);
	details::SysError_NormalizeMessage(m_buffer);
	m_dwError = (m_hrError & 0x0000FFFF);
}

void       CSysError::SetState(const DWORD  dwError, LPCTSTR pDescription)
{
	const HRESULT hError = HRESULT_FROM_WIN32(dwError);
	this->SetState(hError, pDescription);
}

void       CSysError::SetState(const HRESULT hError, LPCTSTR pDescription)
{
	m_hrError = hError;
	if (!pDescription || !::_tcslen(pDescription))
	{
		if (!m_buffer.IsEmpty()) m_buffer.Empty();
	}
	else
		m_buffer  = pDescription;
	m_dwError = (m_hrError & 0x0000FFFF);
}

void       CSysError::SetUnknownMessage(void)
{
	m_buffer = _T("Unknown error occurred. There is no detailed information.");
}

/////////////////////////////////////////////////////////////////////////////

CSysError& CSysError::operator= (const HRESULT hResult)
{
	this->SetHresult(hResult);
	return *this;
}

/////////////////////////////////////////////////////////////////////////////

CSysError::operator bool(void)const
{
	return (m_hrError != S_OK || m_dwError != ERROR_SUCCESS);
}

CSysError::operator HRESULT(void) const
{
	return m_hrError;
}

/////////////////////////////////////////////////////////////////////////////

CSysErrorSafe::CSysErrorSafe(CGenericSyncObject& _sync) : m_sync_obj(_sync)
{
}

/////////////////////////////////////////////////////////////////////////////

CSysErrorSafe& CSysErrorSafe::operator= (const CSysError& _err)
{
	SAFE_LOCK(m_sync_obj);
	((CSysError&) *this) = _err;
	return *this;
}

/////////////////////////////////////////////////////////////////////////////

VOID          CSysErrorSafe::Clear(void)
{
	SAFE_LOCK(m_sync_obj);
	TBase::Clear();
}

VOID          CSysErrorSafe::SetState(const DWORD  dwError, LPCTSTR pszDescription)
{
	SAFE_LOCK(m_sync_obj);
	TBase::SetState(dwError, pszDescription);
}

VOID          CSysErrorSafe::SetState(const HRESULT hError, LPCTSTR pszDescription)
{
	SAFE_LOCK(m_sync_obj);
	TBase::SetState(hError, pszDescription);
}

/////////////////////////////////////////////////////////////////////////////

CSysErrorSafe& CSysErrorSafe::operator= (const DWORD _code)
{
	SAFE_LOCK(m_sync_obj);
	TBase::SetHresult(__HRESULT_FROM_WIN32(_code));	
	return *this;
}

CSysErrorSafe& CSysErrorSafe::operator= (const HRESULT hResult)
{
	SAFE_LOCK(m_sync_obj);
	TBase::SetHresult(hResult);	
	return *this;
}

/////////////////////////////////////////////////////////////////////////////

CSysErrorSafe::operator bool(void)const
{
	SAFE_LOCK(m_sync_obj);
	return TBase::operator bool();
}

CSysErrorSafe::operator HRESULT(void) const
{
	SAFE_LOCK(m_sync_obj);
	return TBase::GetHresult();
}