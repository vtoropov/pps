/*
	Created by Tech_dog (VToropov) on 21-Mar-2014 at 12:20:42pm, GMT+4, Taganrog, Friday;
	This is Shared Lite Generic Synchronize Object class implementation file.
*/
#include "StdAfx.h"
#include "Shared_GenericSyncObject.h"

using namespace shared;
using namespace shared::lite;
using namespace shared::lite::runnable;

////////////////////////////////////////////////////////////////////////////

CGenericSyncObject::CGenericSyncObject(void)
{
	::InitializeCriticalSection(&m_sec);
}

CGenericSyncObject::~CGenericSyncObject(void)
{
	::DeleteCriticalSection(&m_sec);
	::memset((void*)&m_sec, 0, sizeof(CRITICAL_SECTION));
}

////////////////////////////////////////////////////////////////////////////

void CGenericSyncObject::Lock(void) const
{
	::EnterCriticalSection(&m_sec);
}
void CGenericSyncObject::Unlock(void) const
{
	::LeaveCriticalSection(&m_sec);
}