/*
	Created by Tech_dog (VToropov) on 19-Mar-2014 at 11:13:00am, GMT+4, Taganrog, Wednesday;
	This is Shared Lite Generic Event class implementation file.
*/
#include "StdAfx.h"
#include "Shared_GenericEvent.h"

using namespace shared;
using namespace shared::lite;
using namespace shared::lite::events;

////////////////////////////////////////////////////////////////////////////

CGenericEvent::CMessageHandler::CMessageHandler(IGenericEventNotify& sink_ref, const UINT eventId):
	m_sink_ref(sink_ref),
	m_event_id(eventId)
{
}

CGenericEvent::CMessageHandler::~CMessageHandler(void)
{
}

////////////////////////////////////////////////////////////////////////////

LRESULT  CGenericEvent::CMessageHandler::OnGenericEventNotify(UINT, WPARAM, LPARAM lParam, BOOL&)
{
	const UINT evtId = (0 != lParam ? static_cast<UINT>(lParam) : m_event_id);
	m_sink_ref.GenericEvent_OnNotify(evtId);
	return 0;
}

////////////////////////////////////////////////////////////////////////////

CGenericEvent::CGenericEvent(IGenericEventNotify& sink_ref, const UINT eventId):
	m_handler(sink_ref, eventId)
{
}

CGenericEvent::~CGenericEvent(void)
{
	Destroy();
}

////////////////////////////////////////////////////////////////////////////

HRESULT      CGenericEvent::Create(void)
{
	if (m_handler.IsWindow())
		return S_OK;
	m_handler.Create(HWND_MESSAGE);
	return (m_handler.IsWindow() ? S_OK : HRESULT_FROM_WIN32(::GetLastError()));
}

HRESULT      CGenericEvent::Destroy(void)
{
	if (!m_handler.IsWindow())
		return S_OK;
	m_handler.DestroyWindow();
	return S_OK;
}

HWND         CGenericEvent::GetHandle_Safe(void) const
{
	return (m_handler.IsWindow() ? m_handler.m_hWnd : NULL);
}

////////////////////////////////////////////////////////////////////////////

HRESULT  CGenericEvent::Fire(const bool bAsynch)
{
	const HWND hHandler = GetHandle_Safe();
	return CGenericEvent::Fire(hHandler, bAsynch);
}

HRESULT  CGenericEvent::Fire(const bool bAsynch, const UINT evtId)
{
	const HWND hHandler = GetHandle_Safe();
	return CGenericEvent::Fire(hHandler, bAsynch, evtId);
}

////////////////////////////////////////////////////////////////////////////

HRESULT  CGenericEvent::Fire(const HWND hHandler, const bool bAsynch)
{
	if (!::IsWindow(hHandler))
		return OLE_E_INVALIDHWND;
	if (bAsynch)
	{
		::PostMessage(hHandler, CGenericEvent::WM_INTERNAL_MSG, (WPARAM)0, (LPARAM)0);
	}
	else
	{
		::SendMessage(hHandler, CGenericEvent::WM_INTERNAL_MSG, (WPARAM)0, (LPARAM)0);
	}
	return S_OK;
}

HRESULT  CGenericEvent::Fire(const HWND hHandler, const bool bAsynch, const UINT evtId)
{
	if (!::IsWindow(hHandler))
		return OLE_E_INVALIDHWND;
	if (bAsynch)
	{
		::PostMessage(hHandler, CGenericEvent::WM_INTERNAL_MSG, (WPARAM)0, (LPARAM)evtId);
	}
	else
	{
		::SendMessage(hHandler, CGenericEvent::WM_INTERNAL_MSG, (WPARAM)0, (LPARAM)evtId);
	}
	return S_OK;
}