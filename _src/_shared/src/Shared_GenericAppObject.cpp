/*
	Created by Tech_dog (VToropov) on 19-Mar-2014 at 8:24:43am, GMT+4, Taganrog, Wednesday;
	This is Shared Lite Generic Application Object class implementation file.
*/
#include "StdAfx.h"
#include "Shared_GenericAppObject.h"

using namespace shared;
using namespace shared::lite;
using namespace shared::lite::common;

////////////////////////////////////////////////////////////////////////////

namespace shared { namespace lite { namespace common { namespace details
{
	enum Application_ProcState
	{
		Application_ProcState_DoNotCare   = 0x0,
		Application_ProcState_1stInstance = 0x1,
		Application_ProcState_IsRunning   = 0x2,
	};
	static LPCTSTR   Application_ProcessName(CONST shared::lite::common::CApplication& app_obj_ref)
	{
		static ::ATL::CAtlString  proc_name;
		if (proc_name.IsEmpty())
		{
			proc_name.Format(_T("Global\\%s|C9A74DC6-0A84-4c60-BA20-59A516289ED0"), app_obj_ref.GetName());
			proc_name.Replace(_T(" "), _T("_"));
		}
		return proc_name.GetString();
	}
}}}}

////////////////////////////////////////////////////////////////////////////

CApplication::CProcess::CProcess(const CApplication& app_obj_ref) : 
	m_app_obj_ref(app_obj_ref),
	m_mutex(NULL),
	m_proc_state(details::Application_ProcState_DoNotCare)
{
}

CApplication::CProcess::~CProcess(VOID)
{
	UnregisterSingleton();
}

////////////////////////////////////////////////////////////////////////////

bool      CApplication::CProcess::IsSingleton(void) const
{
	return (details::Application_ProcState_1stInstance == m_proc_state);
}

HRESULT   CApplication::CProcess::RegisterSingleton(LPCTSTR pMutexName)
{
	HRESULT hr__ = S_OK;
	if (m_mutex_name.IsEmpty())
		m_mutex_name = (!pMutexName ? details::Application_ProcessName(m_app_obj_ref) : pMutexName);

	if (details::Application_ProcState_DoNotCare == m_proc_state && !m_mutex)
	{
		m_mutex = ::CreateMutex(NULL, FALSE, m_mutex_name.GetString());
		const DWORD dError = ::GetLastError();
		if (NULL == m_mutex && ERROR_ACCESS_DENIED == dError) // the synch object already exists and we cannot get an access to it
		{
			m_proc_state = details::Application_ProcState_IsRunning;
			m_mutex = ::OpenMutex(MUTEX_ALL_ACCESS, FALSE, m_mutex_name.GetString());  // this is superfluous call, but nevertheless it is made for the sake of clarity
			hr__ = HRESULT_FROM_WIN32(ERROR_OBJECT_ALREADY_EXISTS);
		}
		else if (ERROR_ALREADY_EXISTS == dError)
		{
			m_proc_state = details::Application_ProcState_IsRunning;
			hr__ = HRESULT_FROM_WIN32(ERROR_OBJECT_ALREADY_EXISTS);
		}
		else
			m_proc_state = details::Application_ProcState_1stInstance;
	}
	else
		hr__ = HRESULT_FROM_WIN32(ERROR_OBJECT_ALREADY_EXISTS);
	return hr__;
}

HRESULT   CApplication::CProcess::UnregisterSingleton(VOID)
{
	if (details::Application_ProcState_1stInstance == m_proc_state)
		if (NULL != m_mutex)
		{
			::CloseHandle(m_mutex); m_mutex = NULL;
			return S_OK;
		}
	return S_FALSE;
}

////////////////////////////////////////////////////////////////////////////

namespace shared { namespace lite { namespace common { namespace details
{
	using shared::lite::common::CApplication;

	struct Version_LanguageCodePage
	{
		WORD wLanguage;
		WORD wCodePage;
	};
	static HRESULT  Version_FixedFileInfo(LPVOID p_raw_data, VS_FIXEDFILEINFO& info_ref)
	{
		if (!p_raw_data)
			return E_INVALIDARG;
		LPVOID pVersionPtr = NULL;
		UINT   uiVerLength = NULL;
		if (!::VerQueryValue(p_raw_data, _T ("\\"), &pVersionPtr, &uiVerLength))
			return E_OUTOFMEMORY;
		if (uiVerLength != sizeof(VS_FIXEDFILEINFO))
			return DISP_E_TYPEMISMATCH;
		if (!pVersionPtr)
			return E_OUTOFMEMORY;
		info_ref = *(VS_FIXEDFILEINFO*) pVersionPtr; // scare!
		return S_OK;
	}
	static LPCTSTR  Version_LanguageDefault(VOID)
	{
		static LPCTSTR pLanguage = _T("\\StringFileInfo\\040904B0\\"); // 040904B0 means US English, Unicode code page
		return pLanguage;
	}
	static HRESULT  Version_LanguageCurrent(LPVOID p_raw_data, Version_LanguageCodePage& cp_ref)
	{
		if (!p_raw_data)
			return E_INVALIDARG;
		LPVOID pVersionPtr = NULL;
		UINT   uiVerLength = NULL;
		if (!::VerQueryValue(p_raw_data, _T("\\VarFileInfo\\Translation"), (LPVOID*)&pVersionPtr, &uiVerLength))
			return E_OUTOFMEMORY;
		if (uiVerLength != sizeof(Version_LanguageCodePage))
			return DISP_E_TYPEMISMATCH;
		if (!pVersionPtr)
			return E_OUTOFMEMORY;
		cp_ref = *((Version_LanguageCodePage*)pVersionPtr);
		return S_OK;
	}
	static HRESULT  Version_QueryStringValue(LPVOID p_raw_data, LPCTSTR p_qry, ::ATL::CAtlString& result_ref)
	{
		if (!p_qry || ::_tcslen(p_qry) < 1)
			return E_INVALIDARG;
		Version_LanguageCodePage cp__ = {0};
		HRESULT hr__ = Version_LanguageCurrent(p_raw_data, cp__);
		if (S_OK != hr__)
			return  hr__;
		::ATL::CAtlString s_qry;
		s_qry.Format(_T("\\StringFileInfo\\%04x%04x\\%s"), cp__.wLanguage, cp__.wCodePage, p_qry);
		LPVOID pVersionPtr = NULL;
		UINT   uiVerLength = NULL;
		if (!::VerQueryValue(p_raw_data, s_qry.GetString(), (LPVOID*)&pVersionPtr, &uiVerLength))
		{
			// tries to get default language string block
			s_qry.Format(_T("%s%s"), Version_LanguageDefault(), p_qry);
			if (!::VerQueryValue(p_raw_data, s_qry.GetString(), (LPVOID*)&pVersionPtr, &uiVerLength))
				return E_OUTOFMEMORY;
		}
		if (uiVerLength > 0)
			result_ref = (LPCTSTR)pVersionPtr;
		return  hr__;
	}
}}}}

////////////////////////////////////////////////////////////////////////////

CApplication::CVersion::CVersion(void):
	m_hResult(OLE_E_BLANK),
	m_pVerInfo(NULL)
{
	TCHAR sModulePath[_MAX_PATH] = {0};
	const DWORD dwResult = ::GetModuleFileName(NULL, sModulePath, _MAX_PATH);
	if ( !dwResult || ERROR_INSUFFICIENT_BUFFER == dwResult )
	{
		m_hResult = DISP_E_BUFFERTOOSMALL;
		return;
	}
	DWORD dwDummy = 0;
	// determines the size buffer needed to store the version information
	DWORD dwVerInfoSize = ::GetFileVersionInfoSize(sModulePath, &dwDummy);
	if ( !dwVerInfoSize )
	{
		m_hResult = OLE_E_BLANK; // no version info
		return;
	}
	try
	{
		m_pVerInfo = new char[dwVerInfoSize];
		::memset(m_pVerInfo, 0, dwVerInfoSize);
	}
	catch(::std::bad_alloc&)
	{
		m_hResult = E_OUTOFMEMORY;
		ATLASSERT(FALSE);
	}
	// reads the version info block into the buffer
	if (!::GetFileVersionInfo(sModulePath, dwDummy, dwVerInfoSize, m_pVerInfo))
	{
		const DWORD dwError = ::GetLastError();
		m_hResult = HRESULT_FROM_WIN32(dwError);
	}
	else
	{
		m_hResult = S_OK;
	}
}

CApplication::CVersion::~CVersion(void)
{
	if (m_pVerInfo)
	{
		try
		{
			delete [] m_pVerInfo;
		}
		catch (...){}
		m_pVerInfo = NULL;
	}
}

////////////////////////////////////////////////////////////////////////////

::ATL::CAtlString CApplication::CVersion::CompanyName(void) const
{
	::ATL::CAtlString cs_company;
	HRESULT hr__ = details::Version_QueryStringValue(m_pVerInfo, _T("CompanyName"), cs_company);
	if (S_OK != hr__)
		cs_company = _T("#n/a");
	return cs_company;
}

::ATL::CAtlString CApplication::CVersion::CopyRight(void) const
{
	::ATL::CAtlString cs_cr;
	HRESULT hr__ = details::Version_QueryStringValue(m_pVerInfo, _T("LegalCopyRight"), cs_cr);
	if (S_OK != hr__)
		cs_cr = _T("#n/a");
	return cs_cr;
}

::ATL::CAtlString CApplication::CVersion::FileDescription(void) const
{
	::ATL::CAtlString cs_desc;
	HRESULT hr__ = details::Version_QueryStringValue(m_pVerInfo, _T("FileDescription"), cs_desc);
	if (S_OK != hr__)
		cs_desc = _T("#n/a");
	return cs_desc;
}

::ATL::CAtlString CApplication::CVersion::FileType(void) const
{
	::ATL::CAtlString cs_file_type("#n/a");
	VS_FIXEDFILEINFO fi = {0};
	const HRESULT hr__ = details::Version_FixedFileInfo(m_pVerInfo, fi);
	if (S_OK == hr__)
	{
		switch (fi.dwFileType)
		{
		case VFT_DRV:
			{
				switch (fi.dwFileSubtype) 
				{
				case VFT2_DRV_DISPLAY:        cs_file_type = _T("Display driver");       break;
				case VFT2_DRV_INSTALLABLE:    cs_file_type = _T("Installable driver");   break;
				case VFT2_DRV_KEYBOARD:       cs_file_type = _T("Keyboard driver");      break;
				case VFT2_DRV_LANGUAGE:       cs_file_type = _T("Language driver");      break;
				case VFT2_DRV_MOUSE:          cs_file_type = _T("Mouse driver");         break;
				case VFT2_DRV_NETWORK:        cs_file_type = _T("Network driver");       break;
				case VFT2_DRV_PRINTER:        cs_file_type = _T("Printer driver");       break;
				case VFT2_DRV_SOUND:          cs_file_type = _T("Sound driver");         break;
				case VFT2_DRV_SYSTEM:         cs_file_type = _T("System driver");        break;
				case VFT2_UNKNOWN:            cs_file_type = _T("Unknown driver");       break;
				}
			} break;
		case VFT_FONT:
			{
				switch (fi.dwFileSubtype)
				{
				case VFT2_FONT_RASTER:        cs_file_type = _T("Raster font");          break;
				case VFT2_FONT_TRUETYPE:      cs_file_type = _T("Truetype font");        break;
				case VFT2_FONT_VECTOR:        cs_file_type = _T("Vector font");          break;
				case VFT2_UNKNOWN:            cs_file_type = _T("Unknown font");         break;
				}
			} break;
		case VFT_APP:                         cs_file_type = _T("Application");          break;
		case VFT_DLL:                         cs_file_type = _T("Dynamic link library"); break;
		case VFT_STATIC_LIB:                  cs_file_type = _T("Static link library");  break;
		case VFT_VXD:                         cs_file_type = _T("Virtual device");       break;
		case VFT_UNKNOWN:                     cs_file_type = _T("Unknown type");         break;
		}
	}
	return cs_file_type;
}

::ATL::CAtlString CApplication::CVersion::FileVersion(void) const
{
	::ATL::CAtlString cs_file_version;
	HRESULT hr__ = details::Version_QueryStringValue(m_pVerInfo, _T("FileVersion"), cs_file_version);
	if (S_OK != hr__)
		cs_file_version = _T("#n/a");
	return  cs_file_version;
}

::ATL::CAtlString CApplication::CVersion::FileVersionFixed(void) const
{
	::ATL::CAtlString cs_file_ver("#n/a");
	VS_FIXEDFILEINFO fi = {0};
	const HRESULT hr__ = details::Version_FixedFileInfo(m_pVerInfo, fi);
	if (S_OK == hr__)
	{
		cs_file_ver.Format(_T("%d.%d.%d.%d"), 
			HIWORD(fi.dwFileVersionMS), LOWORD(fi.dwFileVersionMS),
			HIWORD(fi.dwFileVersionLS), LOWORD(fi.dwFileVersionLS));
	}
	return cs_file_ver;
}

::ATL::CAtlString CApplication::CVersion::InternalName(void) const
{
	::ATL::CAtlString cs_int_name;
	HRESULT hr__ = details::Version_QueryStringValue(m_pVerInfo, _T("InternalName"), cs_int_name);
	if (S_OK != hr__)
		cs_int_name = _T("#n/a");
	return  cs_int_name;
}

::ATL::CAtlString CApplication::CVersion::OriginalFileName(void) const
{
	::ATL::CAtlString cs_origin;
	HRESULT hr__ = details::Version_QueryStringValue(m_pVerInfo, _T("OriginalFileName"), cs_origin);
	if (S_OK != hr__)
		cs_origin = _T("#n/a");
	return  cs_origin;
}

::ATL::CAtlString CApplication::CVersion::Platform(void) const
{
	::ATL::CAtlString cs_platform("#n/a");
	VS_FIXEDFILEINFO fi = {0};
	const HRESULT hr__ = details::Version_FixedFileInfo(m_pVerInfo, fi);
	if (S_OK == hr__)
	{
		switch (fi.dwFileOS)
		{
		case VOS_DOS:                cs_platform = _T("MS-DOS");                           break;
		case VOS_DOS_WINDOWS16:      cs_platform = _T("16-bit windows running on MS-DOS"); break;
		case VOS_DOS_WINDOWS32:      cs_platform = _T("Win32 API running on MS-DOS");      break;
		case VOS_OS216:              cs_platform = _T("16-bit OS/2");                      break;
		case VOS_OS216_PM16:         cs_platform = _T("16-bit Presentation manager running on 16-bit OS/2"); break;
		case VOS_OS232:              cs_platform = _T("32-bit OS/2");                      break;
		case VOS_NT:                 cs_platform = _T("Windows NT");                       break;
		case VOS_NT_WINDOWS32:       cs_platform = _T("Win32 API on Windows NT");          break;
		case VOS_UNKNOWN:            cs_platform = _T("Unknown OS");                       break;
		}
	}
	return cs_platform;
}

::ATL::CAtlString CApplication::CVersion::ProductName(void) const
{
	::ATL::CAtlString cs_prod_name;
	HRESULT hr__ = details::Version_QueryStringValue(m_pVerInfo, _T("ProductName"), cs_prod_name);
	if (S_OK != hr__)
		cs_prod_name = _T("#n/a");
	return  cs_prod_name;
}

::ATL::CAtlString CApplication::CVersion::ProductVersion(void) const
{
	::ATL::CAtlString cs_prod_version;
	HRESULT hr__ = details::Version_QueryStringValue(m_pVerInfo, _T("ProductVersion"), cs_prod_version);
	if (S_OK != hr__)
		cs_prod_version = _T("#n/a");
	return  cs_prod_version;
}

::ATL::CAtlString CApplication::CVersion::ProductVersionFixed(void) const
{
	::ATL::CAtlString cs_prod_ver("#n/a");
	VS_FIXEDFILEINFO fi = {0};
	const HRESULT hr__ = details::Version_FixedFileInfo(m_pVerInfo, fi);
	if (S_OK == hr__)
	{
		cs_prod_ver.Format(_T("%d.%d.%d.%d"), 
			HIWORD(fi.dwProductVersionMS), LOWORD(fi.dwProductVersionMS),
			HIWORD(fi.dwProductVersionLS), LOWORD(fi.dwProductVersionLS));
	}
	return cs_prod_ver;
}

////////////////////////////////////////////////////////////////////////////

CApplication::CCommandLine::CCommandLine(void)
{
	::ATL::CAtlString cs_cmd_line = ::GetCommandLine();
	INT n_count = 0;
	LPWSTR* pCmdArgs = ::CommandLineToArgvW(cs_cmd_line.GetString(), &n_count);
	if (n_count && pCmdArgs)
	{
		try
		{
			m_module_full_path = pCmdArgs[0];
			for (INT i__ = 1; i__ < n_count; i__+= 2)
			{
				::ATL::CAtlString cs_key = pCmdArgs[i__]; cs_key.Replace(_T("-"), _T("")); cs_key.Replace(_T("/"), _T(""));
				::ATL::CAtlString cs_arg(_T(""));
				if (i__ + 1 < n_count)
					cs_arg = pCmdArgs[i__ + 1];
				m_args.insert(::std::make_pair(cs_key, cs_arg));
			}
		} catch (::std::bad_alloc&){}
	}
	if (pCmdArgs)
	{
		::LocalFree(pCmdArgs); pCmdArgs = NULL;
	}
}

CApplication::CCommandLine::~CCommandLine(void)
{
	m_args.clear();
}

////////////////////////////////////////////////////////////////////////////

::ATL::CAtlString   CApplication::CCommandLine::Argument(LPCTSTR pName)const
{
	TArguments::const_iterator it__ = m_args.find(::ATL::CAtlString(pName));
	if (it__ == m_args.end())
		return ::ATL::CAtlString();
	else
		return it__->second;
}

INT                 CApplication::CCommandLine::Count(void)const
{
	return (INT)m_args.size();
}

bool                CApplication::CCommandLine::Has(LPCTSTR pArgName)const
{
	TArguments::const_iterator it__ = m_args.find(::ATL::CAtlString(pArgName));
	return (it__ != m_args.end());
}

::ATL::CAtlString   CApplication::CCommandLine::ModuleFullPath(void)const
{
	return m_module_full_path;
}

////////////////////////////////////////////////////////////////////////////

CApplication::CApplication(void):
	m_smart_ass(*this),
	m_hResult(OLE_E_BLANK),
	m_process(*this)
{
	LPCTSTR p = this->GetName();
	if (p)
	{
		if (0 != m_app_name.CompareNoCase(_T("#n/a")))
			m_hResult = S_OK;
	}
}

CApplication::~CApplication(void)
{
}

////////////////////////////////////////////////////////////////////////////

const CApplication::CCommandLine& CApplication::CommandLine(void) const
{
	return m_cmd_line;
}

////////////////////////////////////////////////////////////////////////////

HRESULT     CApplication::GetLastResult(void) const
{
	return m_hResult;
}

LPCTSTR     CApplication::GetName(void) const
{
	if (m_app_name.IsEmpty())
	{
		m_smart_ass.m_app_name = m_version.ProductName();
	}
	return m_app_name.GetString();
}

HRESULT     CApplication::GetPath(::ATL::CAtlString& __in_out_ref) const
{
	static const INT max_path_length_ = _MAX_DRIVE + _MAX_DIR + _MAX_FNAME + _MAX_EXT;
	TCHAR buffer[max_path_length_] = {0};
	const BOOL ret__ = ::GetModuleFileName(NULL, buffer, _countof(buffer));
	if (NULL == ret__)
		return HRESULT_FROM_WIN32(::GetLastError());
	TCHAR drive__[_MAX_DRIVE] = {0};
	TCHAR folder__[_MAX_DIR]  = {0};
	::_tsplitpath_s(buffer, drive__, _countof(drive__), folder__, _countof(folder__), NULL, 0, NULL, 0);
	::_tmakepath_s(buffer, _countof(buffer), drive__, folder__, NULL, NULL);
	::ATL::CAtlString app_folder__(buffer);
	if (app_folder__.GetAt(app_folder__.GetLength() - 1) != _T('\\'))
		app_folder__ += _T("\\");
	__in_out_ref = app_folder__;
	return S_OK;
}

HRESULT     CApplication::GetPathFromAppFolder(LPCTSTR pPattern, ::ATL::CAtlString& __in_out_ref) const
{
	if (!pPattern || ::_tcslen(pPattern) < 1)
		return E_INVALIDARG;
	::ATL::CAtlString cs_pattern(pPattern);
	const bool bUseExeCurrentFolder = CApplication::IsRelatedToAppFolder(pPattern);
	if (!bUseExeCurrentFolder)
		return S_FALSE;
	if (__in_out_ref.IsEmpty() != true)
		__in_out_ref.Empty();
	::ATL::CAtlString app_path_;
	HRESULT hr__ = this->GetPath(app_path_);
	if (S_OK != hr__)
		return  hr__;
	__in_out_ref+= app_path_.GetString();
	__in_out_ref+= cs_pattern.Right(cs_pattern.GetLength() - 1);  // adds the pattern without the dot symbol
	__in_out_ref.Replace(_T("\\\\"), _T("\\"));                   // assumes that the app path is NOT a network share
	return  hr__;
}

bool        CApplication::Is64bit(VOID)const
{
#if defined(WIN64)
	return true;
#else
	return false;
#endif
}

////////////////////////////////////////////////////////////////////////////

const CApplication::CProcess& CApplication::Process(VOID)const
{
	return m_process;
}

CApplication::CProcess&       CApplication::Process(VOID)
{
	return m_process;
}

const CApplication::CVersion& CApplication::Version(VOID)const
{
	return m_version;
}

////////////////////////////////////////////////////////////////////////////

bool        CApplication::IsRelatedToAppFolder(LPCTSTR pPath)
{
	if (!pPath || ::_tcslen(pPath) < 1)
		return false;
	::ATL::CAtlString cs_path(pPath);
	return (0 == cs_path.Find(_T(".\\")) || 0 == cs_path.Find(_T("./")));
}

////////////////////////////////////////////////////////////////////////////

namespace shared { namespace lite { namespace common { namespace details
{
	static HICON Application_LoadIcon(const UINT nIconResId, const bool bTreatAsBigIcon)
	{
		const SIZE szIcon = {
					::GetSystemMetrics(bTreatAsBigIcon ? SM_CXICON : SM_CXSMICON), 
					::GetSystemMetrics(bTreatAsBigIcon ? SM_CYICON : SM_CYSMICON)
				};
		const HINSTANCE hInstance = ::ATL::_AtlBaseModule.GetModuleInstance();
		const HICON hIcon = (HICON)::LoadImage(hInstance, MAKEINTRESOURCE(nIconResId), 
			IMAGE_ICON, szIcon.cx, szIcon.cy, LR_DEFAULTCOLOR);
		return hIcon;
	}
}}}}

////////////////////////////////////////////////////////////////////////////

CApplicationIconLoader::CApplicationIconLoader(const UINT nIconId)
{
	m__big   = details::Application_LoadIcon(nIconId, true);
	m__small = details::Application_LoadIcon(nIconId, false);
}

CApplicationIconLoader::CApplicationIconLoader(const UINT nSmallIconId, const UINT nBigIconId)
{
	m__big   = details::Application_LoadIcon(nBigIconId, true);
	m__small = details::Application_LoadIcon(nSmallIconId, false);
}

CApplicationIconLoader::~CApplicationIconLoader(void)
{
	if (NULL != m__big)
	{
		::DestroyIcon(m__big); m__big = NULL;
	}
	if (NULL != m__small)
	{
		::DestroyIcon(m__small); m__small = NULL;
	}
}

////////////////////////////////////////////////////////////////////////////

HICON   CApplicationIconLoader::DetachBigIcon(void)
{
	HICON tmp__ = m__big;
	m__big = NULL;
	return tmp__;
}

HICON   CApplicationIconLoader::DetachSmallIcon(void)
{
	HICON tmp__ = m__small;
	m__small = NULL;
	return tmp__;
}

HICON   CApplicationIconLoader::GetBigIcon(void) const
{
	return m__big;
}

HICON   CApplicationIconLoader::GetSmallIcon(void) const
{
	return m__small;
}

////////////////////////////////////////////////////////////////////////////

namespace shared { namespace lite { namespace common { namespace details
{
	class ApplicationCursor_Data
	{
	public:
		HCURSOR  m_hNewCursor;
		HCURSOR  m_hOldCursor;
		bool     m_InUse;
	public:
		ApplicationCursor_Data(void): m_hNewCursor(NULL), m_hOldCursor(NULL), m_InUse(false)
		{
		}
	};

	static ApplicationCursor_Data& ApplicationCursor_GetDataRef(void)
	{
		static ApplicationCursor_Data data;
		return data;
	}
}}}}

////////////////////////////////////////////////////////////////////////////

CApplicationCursor::CApplicationCursor(LPCTSTR lpstrCursor) : m_resource_owner(false)
{
	details::ApplicationCursor_Data& data_ref = details::ApplicationCursor_GetDataRef();
	if (false == data_ref.m_InUse)
	{
		data_ref.m_InUse = m_resource_owner = true;
		data_ref.m_hNewCursor = ::LoadCursor(NULL, lpstrCursor);
		data_ref.m_hOldCursor = ::SetCursor(data_ref.m_hNewCursor);
	}
}

CApplicationCursor::~CApplicationCursor(void)
{
	if (true == m_resource_owner)
	{
		details::ApplicationCursor_Data& data_ref = details::ApplicationCursor_GetDataRef();
		::SetCursor(data_ref.m_hOldCursor);
		data_ref.m_InUse = m_resource_owner = false;
	}
}