#ifndef _SHAREDLITEEVENTLOGGER_H_48DF5BDB_D9A2_4b59_8925_7B36976454A9_INCLUDED
#define _SHAREDLITEEVENTLOGGER_H_48DF5BDB_D9A2_4b59_8925_7B36976454A9_INCLUDED
/*
	Created by Tech_dog (VToropov) on 19-Mar-2014 at 7:36:14am, GMT+3, Taganrog, Wednesday;
	This is Shared Lite Common Event Logger class declaration file.
*/
#include "Shared_SystemError.h"
#include "Shared_GenericSyncObject.h"

namespace shared { namespace lite { namespace log
{
	using shared::lite::common::CSysError;
	using shared::lite::runnable::CGenericSyncObject; 

	class eEventType
	{
	public:
		enum _enum{
			eInfo    = 0,
			eWarning = 1,
			eError   = 2,
		};
	};

	class eEventPersistance
	{
	public:
		enum _enum{
			eNone    = 0,
			eFile    = 1, // default
			eJournal = 2, // system event journal
		};
	};

	class eEventLoggerOption
	{
	public:
		enum _enum{
			eNone           = 0x0,
			eUseSingleFile  = 0x1,
			eOverwriteFile  = 0x2,
			eLogErrorsOnly  = 0x4,
			eLoggingIsOff   = 0x8
		};
	};

	class CEventLogger
	{
	private:
		DWORD                    m_dwOptions;
		FILE*                    m_file;
		CGenericSyncObject       m_sync_obj;
		CSysError                m_log_error;
		eEventPersistance::_enum m_pers_state;
	public:
		CEventLogger(const eEventPersistance::_enum = eEventPersistance::eFile, const DWORD dwOptions = eEventLoggerOption::eNone);
		~CEventLogger(void);
	public:
		const CSysError&         Error(void)const;
		bool                     IsTurnedOn(void)const;
		HRESULT                  ToEvent(LPCTSTR pMessage, const eEventType::_enum = eEventType::eInfo) const;
		HRESULT                  ToEvent(LPCTSTR pMessage, const DWORD   dError) const;
		HRESULT                  ToEvent(LPCTSTR pMessage, const HRESULT hError) const;
		HRESULT                  ToFile (LPCTSTR pMessage, const eEventType::_enum = eEventType::eInfo) const;
		HRESULT                  ToFile (LPCTSTR pMessage, const DWORD   dError) const;
		HRESULT                  ToFile (LPCTSTR pMessage, const HRESULT hError) const;
		HRESULT                  ToLog  (LPCTSTR pMessage, const eEventType::_enum = eEventType::eInfo) const;
		HRESULT                  ToLog  (LPCTSTR pMessage, const DWORD   dError) const;
		HRESULT                  ToLog  (LPCTSTR pMessage, const HRESULT hError) const;
	private:
		CEventLogger(const CEventLogger&);
		CEventLogger& operator= (const CEventLogger&);
	};

	const CEventLogger&  Logger(void);

	class CEventLog__function_auto_entry
	{
	private:
		const CEventLogger& m__log_ref;
		::ATL::CAtlString   m__func_name;
		bool                m__removal_ctx;
	public:
		CEventLog__function_auto_entry(const CEventLogger&, LPCTSTR func_name);
		~CEventLog__function_auto_entry(void);
	};
}}}

#define TRACE_FUNC()     shared::lite::log::CEventLog__function_auto_entry entry(shared::lite::log::Logger(), _T(__FUNCTION__));
#define TRACE_INFO(msg)  shared::lite::log::Logger().ToLog(msg, shared::lite::log::eEventType::eInfo);
#define TRACE_WARN(msg)  shared::lite::log::Logger().ToLog(msg, shared::lite::log::eEventType::eWarning);
#define TRACE_ERROR(msg) shared::lite::log::Logger().ToLog(msg, shared::lite::log::eEventType::eError);

#define TRACE_ERR__a1(pattern, arg) { ::ATL::CAtlString cs__fmt; cs__fmt.Format(pattern, arg); TRACE_ERROR(cs__fmt.GetString());}
#define TRACE_INFO_a1(pattern, arg) { ::ATL::CAtlString cs__fmt; cs__fmt.Format(pattern, arg); TRACE_INFO (cs__fmt.GetString());}
#define TRACE_WARN_a1(pattern, arg) { ::ATL::CAtlString cs__fmt; cs__fmt.Format(pattern, arg); TRACE_WARN (cs__fmt.GetString());}

#define TRACE_ERR__a2(pattern, arg1, arg2) { ::ATL::CAtlString cs__fmt; cs__fmt.Format(pattern, arg1, arg2); TRACE_ERROR(cs__fmt.GetString());}

#endif/*_SHAREDLITEEVENTLOGGER_H_48DF5BDB_D9A2_4b59_8925_7B36976454A9_INCLUDED*/