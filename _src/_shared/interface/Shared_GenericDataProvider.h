#ifndef _SHAREDLITEGENERICDATAPROVIDER_H_8DF3A5FD_461F_4ade_84B5_123B21305FA6_INCLUDED
#define _SHAREDLITEGENERICDATAPROVIDER_H_8DF3A5FD_461F_4ade_84B5_123B21305FA6_INCLUDED
/*
	Created by Tech_dog (VToropov) on 25-Mar-2014 at 2:25:02pm, GMT+4 Saint-Petersburg, Tuesday;
	This is Shared Lite Generic Data Provider class declaration file.
*/
#include <map>
namespace shared { namespace lite { namespace data
{
	interface IDataRecord
	{
		virtual HRESULT            Clear(void)                                           PURE; // clears a record, i.e removes all fields and data
		virtual _variant_t         Data(LPCTSTR pFieldName)                        CONST PURE; // gets data of the field specified
		virtual _variant_t*        DataPtr(LPCTSTR pFieldName)                           PURE; // gets pointer to data by the field name, if not found, NULL is returned
		virtual const _variant_t&  DataRef(LPCTSTR pFieldName)                     CONST PURE; // gets data reference of the field specified (read-only), if not found, empry object is returned
		virtual HRESULT            Empty(void)                                           PURE; // clears a record data, the fields spec is kept
		virtual HRESULT            Insert(LPCTSTR pFieldName, const _variant_t& vData)   PURE; // inserts a field and associated data
		virtual bool               IsEmpty(void)                                   CONST PURE; // checks the emptiness of the record, returns true if no field exists
	};

	class CDataRecord:
		public IDataRecord
	{
		typedef ::std::map<::ATL::CAtlString, _variant_t>  TDataRow;
	private:
		TDataRow                   m_data;
	public:
		CDataRecord(void);
		~CDataRecord(void);
	public:
		virtual HRESULT            Clear(void)override sealed;
		virtual _variant_t         Data(LPCTSTR pFieldName) CONST override sealed;
		virtual _variant_t*        DataPtr(LPCTSTR pFieldName) override sealed;
		virtual const _variant_t&  DataRef(LPCTSTR pFieldName) CONST override sealed;
		virtual HRESULT            Empty(void) override sealed;
		virtual HRESULT            Insert(LPCTSTR pFieldName, const _variant_t& vData) override sealed;
		virtual bool               IsEmpty(void) CONST override sealed;
	};

	class CDataSet
	{
		typedef ::std::map<::ATL::CAtlString, CDataRecord>  TOneKeySet;
	private:
		TOneKeySet                 m_records;
		mutable  TOneKeySet::const_iterator m_current;
	public:
		CDataSet(void);
		~CDataSet(void);
	public:
		virtual HRESULT            Add(LPCTSTR pKey, CDataRecord*&);                           // creates an empty record object and returns reference to it
		virtual HRESULT            Clear(void);                                                // destroys all records if any
		virtual LONG               Count(void)const;                                           // counts records in the data set
		virtual const CDataRecord* First(void)const;                                           // gets the first record pointer if any, otherwise NULL is returned
		virtual HRESULT            Insert(LPCTSTR pKey, const CDataRecord&);                   // makes a record copy
		virtual const CDataRecord* Next(void)const;                                            // returns next record after the first one or after the record after the last call this method
		virtual HRESULT            Remove(LPCTSTR pKey);                                       // removes a record by the key value provided
		virtual const CDataRecord& Record(LPCTSTR pKey) const;                                 // gets records by key value, if record is not found, returns empty record reference
		virtual CDataRecord*       Record(LPCTSTR pKey);                                       // gets a pointer to the record by the key specified
	};
}}}

#endif/*_SHAREDLITEGENERICDATAPROVIDER_H_8DF3A5FD_461F_4ade_84B5_123B21305FA6_INCLUDED*/