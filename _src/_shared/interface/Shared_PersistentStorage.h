#ifndef _SHAREDLITEPERSISTENTSTORAGE_H_7E2C8E5E_F55B_40f8_9A14_AF78E1ABA691_INCLUDED
#define _SHAREDLITEPERSISTENTSTORAGE_H_7E2C8E5E_F55B_40f8_9A14_AF78E1ABA691_INCLUDED
/*
	Created by Tech_dog (VToropov) on 19-Mar-2014 at 11:44:29am, GMT+4, Taganrog, Wednesday;
	This is Shared Lite Persistent Storage class(es) declaration file.
*/
namespace shared { namespace lite { namespace persistent { namespace factories
{
	class CPrivateProfileSectionFactory;
}}}}

namespace shared { namespace lite { namespace persistent
{
	using shared::lite::persistent::factories::CPrivateProfileSectionFactory;

	class CPrivateProfile;
	class CPrivateProfileItem
	{
	private:
		::ATL::CAtlString             m_name;
		::ATL::CAtlString             m_value;
		::ATL::CAtlString             m_comment;
	public:
		CPrivateProfileItem(void);
		CPrivateProfileItem(LPCTSTR pName, LPCTSTR pValue, LPCTSTR pComment);
		~CPrivateProfileItem(void);
	public:
		const ::ATL::CAtlString&      Comment(void) const;
		::ATL::CAtlString&            Comment(void);
		bool                          IsValid(void) const;
		const ::ATL::CAtlString&      Name(void) const;
		::ATL::CAtlString&            Name(void);
		const ::ATL::CAtlString&      Value(void) const;
		::ATL::CAtlString&            Value(void);
	public:
		CPrivateProfileItem(const CPrivateProfileItem&);
		CPrivateProfileItem& operator= (const CPrivateProfileItem&);
	};

	class CPrivateProfileSection
	{
		friend class CPrivateProfileSectionFactory;
		friend class CPrivateProfile;
		typedef ::std::map<::ATL::CAtlString, INT>   TItemsIndex; // by pair name
		typedef ::std::vector<CPrivateProfileItem>   TItemsArray;
	private:
		TItemsIndex                   m_index;
		TItemsArray                   m_items;
		::ATL::CAtlString             m_name;
	public:
		CPrivateProfileSection(void);
		CPrivateProfileSection(LPCTSTR pName);
		~CPrivateProfileSection(void);
	public:
		HRESULT                       AddItem(LPCTSTR pszName);
		bool                          IsEmpty(void) const;
		bool                          IsValid(void) const;
		INT                           ItemCount(void) const;         // a number of elements in the section
		const CPrivateProfileItem&    ItemOf(const INT nIndex) const;
		CPrivateProfileItem&          ItemOf(const INT nIndex);
		const CPrivateProfileItem&    ItemOf(LPCTSTR pszName) const;
		CPrivateProfileItem&          ItemOf(LPCTSTR pszName);
		const ::ATL::CAtlString&      Name(void) const;              // section name accessor [read only]
		::ATL::CAtlString&            Name(void);                    // section name accessor
	public:
		CPrivateProfileSection(const CPrivateProfileSection&);
		CPrivateProfileSection& operator= (const CPrivateProfileSection&);
	};

	class CPrivateProfile
	{
	public:
		enum eStorageMode
		{
			SM_UseRegular      = 0x0,    // default
			SM_UseGlobalIndex  = 0x1,    // all data are saved in one table, an access key is composed by section name + separator (optional) + item name
		};
	private:
		friend class CPrivateProfile;
		typedef ::std::map<::ATL::CAtlString, INT>      TSectionIndex; // by section name
		typedef ::std::vector<CPrivateProfileSection>   TSectionArray;
	private:
		TSectionIndex                 m_index;
		TSectionArray                 m_sections;
		HRESULT                       m_result;
		eStorageMode                  m_store_mode;
		CAtlString                    m_store_path;
	public:
		CPrivateProfile(void);
		CPrivateProfile(LPCTSTR pFile);
		~CPrivateProfile(void);
	public:
		HRESULT                       AddSection(LPCTSTR pszSection);
		HRESULT                       Create(LPCTSTR pszFile);
		HRESULT                       CreateGlobal  (LPCTSTR pszFile, LPCTSTR p__key__separator = NULL);
		bool                          GetBoolValue  (LPCTSTR pszSection, LPCTSTR pszItem, const bool bDefaultValue)const;
		DWORD                         GetDwordValue (LPCTSTR pszSection, LPCTSTR pszItem, const DWORD dwDefaultValue)const;
		HRESULT                       GetLastResult (void) const;
		eStorageMode                  GetStorageMode(void) const;
		CAtlString                    GetStringValue(LPCTSTR pszSection, LPCTSTR pszItem, LPCTSTR pszDefaultValue)const;
		bool                          IsEmpty(void) const;
		bool                          IsValid(void) const;
		INT                           SectionCount(void) const;         // a number of sections in the profile
		const CPrivateProfileSection& SectionOf(const INT nIndex)const;
		CPrivateProfileSection&       SectionOf(const INT nIndex);
		const CPrivateProfileSection& SectionOf(LPCTSTR pName)const;
		CPrivateProfileSection&       SectionOf(LPCTSTR pName);
		HRESULT                       SetStringValue(LPCTSTR pszSection, LPCTSTR pszItem, LPCTSTR pszValue);
	private:
		CPrivateProfile(const CPrivateProfile&);
		CPrivateProfile& operator= (const CPrivateProfile&);
	};

	namespace factories
	{
		using shared::lite::persistent::CPrivateProfileSection;

		class CPrivateProfileSectionFactory
		{
		private:
			CPrivateProfileSection&     m__sec__ref;
		public:
			CPrivateProfileSectionFactory(CPrivateProfileSection&);
			~CPrivateProfileSectionFactory(void);
		public:
			HRESULT        Clear(void);
		};
	}

	class CRegistryStorage
	{
	private:
		HKEY         m_hRoot;
	public:
		CRegistryStorage(const HKEY hRoot);
		~CRegistryStorage(void);
	public:
		HRESULT      Load(LPCTSTR pFolder, LPCTSTR pProperty, ::ATL::CAtlString& __in_out_value);
		HRESULT      Load(LPCTSTR pFolder, LPCTSTR pProperty, LONG& __in_out_value);
		HRESULT      Save(LPCTSTR pFolder, LPCTSTR pProperty, const ::ATL::CAtlString& __in_value);
		HRESULT      Save(LPCTSTR pFolder, LPCTSTR pProperty, const LONG __in_value);
	private:
		CRegistryStorage(const CRegistryStorage&);
		CRegistryStorage& operator= (const CRegistryStorage&);
	};

	class eCsvFileOption
	{
	public:
		enum _enum{
			eNone              = 0x0,
			eUseCommaSeparator = 0x1,
			eUseSemicolon      = 0x2,
		};
	};

	class CCsvFile
	{
	public:
		typedef ::std::vector<::ATL::CAtlString>  THeader;
		typedef ::std::vector<::ATL::CAtlString>  TRow;
	private:
		typedef ::std::vector<TRow>               TTable;
	private:
		const DWORD     m_dwOptions;
		mutable HRESULT m_result;
		THeader         m_header;
		TTable          m_table;
		bool            m_has_header;
		DWORD           m_src_size;        // source file size in bytes;
		CAtlString      m_src_path;
	public:
		CCsvFile(const DWORD dwOptions = eCsvFileOption::eNone);
		~CCsvFile(void);
	public:
		HRESULT         AddRow(const TRow&);
		HRESULT         AppendToFile(LPCTSTR pFile, const TRow&);         // opens the file specified and appends the row to the file
		LPCTSTR         Cell(const INT row__, const INT col__) const;
		HRESULT         Clear(void);
		HRESULT         Create(const ::ATL::CAtlString& buffer_ref, const bool bHasHeader = false);
		DOUBLE          Double(const INT row__, const INT col__) const;
		LONG            FieldCount(void) const;
		HRESULT         GetLastResult(void) const;
		bool            HasHeader(void) const;
		const THeader&  Header(void) const;
		HRESULT         Header(const THeader&);
		HRESULT         Load(LPCTSTR pFile, const bool bHasHeader = false);
		const TRow&     Row(const INT row__) const;
		LONG            RowCount(void) const;
		HRESULT         Save(LPCTSTR pFile, const bool bHasHeader = false);
		LPCTSTR         SourceFilePath(void)const;
		DWORD           SourceFileSize(void)const;                        // gets source file size in bytes
	public:
		static HRESULT  CreateFileFromHeader(LPCTSTR lpszFilePath, const THeader&, const DWORD dwOptions);
		static bool     IsFileExist(LPCTSTR lpszFilePath);
	};
}}}

#endif/*_SHAREDLITEPERSISTENTSTORAGE_H_7E2C8E5E_F55B_40f8_9A14_AF78E1ABA691_INCLUDED*/