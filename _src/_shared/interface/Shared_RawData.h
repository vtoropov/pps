#ifndef _SHAREDLITERAWDATA_H_72229F9D_92B6_4ecf_B0AA_07793704BF29_INCLUDED
#define _SHAREDLITERAWDATA_H_72229F9D_92B6_4ecf_B0AA_07793704BF29_INCLUDED
/*
	Created by Tech_dog (VToropov) on 19-Mar-2014 at 6:56:06am, GMT+4, Taganrog, Wednesday;
	This is Shared Lite Raw Data class(es) declaration file.
*/
#include "Shared_SystemError.h"

namespace shared { namespace lite { namespace data
{
	using shared::lite::common::CSysError;
	typedef ::ATL::CComSafeArray<BYTE>  T_RAW_DATA;

	class CByteArray
	{
	private:
		BYTE*             m_pData;
		HRESULT           m_hResult;
		INT               m_size;
	public:
		CByteArray(void);                              // creates the empty object;
		CByteArray(const _variant_t&);                 // creates memory block from variant; it is expected that the variant has VT_ARRAY|VTU1 data type
		CByteArray(const INT nSize);                   // creates a plain memory block of the specified size
		CByteArray(const SAFEARRAY*);                  // creates a copy of the safe array data as a plain memory block
		CByteArray(PBYTE pData, const INT nSize);      // attaches to the provided byte array of the specified size
		~CByteArray(void);
	public:
		HRESULT           Attach(PBYTE pData, const INT nSize);
		HRESULT           CopyTo(_variant_t&)const;
		HRESULT           CopyTo(SAFEARRAY*&) const;        // creates new safe array and copies the data to it
		BYTE*             Detach(void);
		const PBYTE       GetData(void) const;
		PBYTE             GetData(void);
		HRESULT           GetLastResult(void) const;
		PBYTE*            GetPtr(void);
		PBYTE&            GetRef(void);
		INT               GetSize(void) const;
		bool              IsValid(void) const;
	private:
		CByteArray(const CByteArray&);
		CByteArray& operator= (const CByteArray&);
	};

	class CRawDataAccessor
	{
	private:
		CSysError         m_error;
		SAFEARRAY*        m_raw_data_ptr;
	private:
		UCHAR HUGEP*      m_data_ptr;
	public:
		CRawDataAccessor(const _variant_t&);               // variant type must be VT_ARRAY|VT_UI1
		CRawDataAccessor(T_RAW_DATA&);
		CRawDataAccessor(SAFEARRAY*);                      // safe array must contain VT_UI1 data
		~CRawDataAccessor(void);
	public:
		PBYTE             AccessData(void);                // gets access to safe array data pointer
		CONST CSysError&  Error(VOID) CONST;               // gets error object reference
		HRESULT           GetSize(LONG& n_size_ref) const; // gets the size of the safe array data in bytes
		bool              IsValid(void) const;             // checks the validity of the data accessor object
	private:
		CRawDataAccessor(const CRawDataAccessor&);
		CRawDataAccessor& operator= (const CRawDataAccessor&);
	};

	class CBase64
	{
	private:
		BYTE         m_mtx[256];  // decode matrix
	public:
		CBase64(void);
		~CBase64(void);
	public:
		HRESULT      Decode(const PBYTE pSrc, const long nSrcSize, PBYTE* ppDest, long& refBytesCopied);
		HRESULT      Encode(const PBYTE pSrc, const long nSrcSize, PBYTE* ppDest, long& refBytesCopied);
		HRESULT      IsBase64(const char* pSrc, const long iSrcSize) const;
	public:
		HRESULT      Decode(LPCTSTR pSrc, _variant_t& vDecoded);
		HRESULT      Decode(LPCTSTR pSrc, ::ATL::CAtlString& csDecoded);
		HRESULT      Encode(const _variant_t& vSrc, ::ATL::CAtlString& csEncoded);
		HRESULT      Encode(const ::ATL::CAtlString& cs_src, ::ATL::CAtlString& csEncoded);
	private:
		CBase64(const CBase64&);
		CBase64& operator= (const CBase64&);
	};
	// the guys from Microsoft are very lazy to make comparison function for variants that have safearray data type
	INT   CompareSafeArrays(const _variant_t&, const _variant_t&); // returns 0 if safe arrays are equals, otherwise 1 or -1
}}}

#endif/*_SHAREDLITERAWDATA_H_72229F9D_92B6_4ecf_B0AA_07793704BF29_INCLUDED*/