#ifndef _SHAREDLITEDATETIMEFORMAT_H_371B4EE5_2144_4248_BE81_2856CDBC8530_INCLUDED
#define _SHAREDLITEDATETIMEFORMAT_H_371B4EE5_2144_4248_BE81_2856CDBC8530_INCLUDED
/*
	Created by Tech_dog (VToropov) on 20-Mar-2014 at 7:49:48pm, GMT+4, Taganrog, Thursday;
	This is Shared Lite DateTime Utility class declaration file.
*/

namespace shared { namespace lite { namespace format
{
	class eDataFormatType
	{
	public:
		enum _enum {
			eNone      = 0x0,
			eDateShort = 0x1,
			eTimeShort = 0x2,
		};
	};

	class CDateTimeFormat
	{
	private:
		mutable ::ATL::CAtlString   m_buffer;
	public:
		CDateTimeFormat(void);
		~CDateTimeFormat(void);
	public:
		CAtlString GetFormatString(const DWORD dFlags) const;
		LPCTSTR    ToDate(const SYSTEMTIME&) const;
		LPCTSTR    ToDate(const SYSTEMTIME&, LPCTSTR pCustomPattern) const;
		LPCTSTR    ToDateTime(const SYSTEMTIME&) const;
		LPCTSTR    ToDateTime(const SYSTEMTIME& dt_ref, const SYSTEMTIME& tm_ref) const;
		LPCTSTR    ToTime(const SYSTEMTIME&) const;
		LPCTSTR    ToTime(const SYSTEMTIME&, LPCTSTR pCustomPattern) const;
	public:
		static LPCTSTR    GetDefaultDateFormat(void);
		static LPCTSTR    GetDefaultDateTimeFormat(void);
		static LPCTSTR    GetDefaultTimeFormat(void);
	};
}
namespace data 
{
	class CTimestamp
	{
	private:
		time_t            m_value;
	public:
		CTimestamp(void);
	public:
		VOID              SetCurrentTime(void);
		time_t            Value(void)const;
		VOID              Value(const time_t);
		CAtlString        ValueAsFormattedText(void)const;
		CAtlString        ValueAsText(void)const;
		HRESULT           ValueAsText(LPCTSTR pszTimestamp, const bool bValidateData);
	};
}}}

#endif/*_SHAREDLITEDATETIMEFORMAT_H_371B4EE5_2144_4248_BE81_2856CDBC8530_INCLUDED*/