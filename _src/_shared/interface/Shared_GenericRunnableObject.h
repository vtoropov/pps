#ifndef _SHAREDLITEGENERICRUNNABLEOBJECT_H_F6EC76FE_16C4_4b39_B83B_3AB849B334B8_INCLUDED
#define _SHAREDLITEGENERICRUNNABLEOBJECT_H_F6EC76FE_16C4_4b39_B83B_3AB849B334B8_INCLUDED
/*
	Created by Tech_dog (VToropov) on 19-Mar-2014 at 11:21:09am, GMT+3, Taganrog, Wednesday;
	This is Shared Generic Asynchronous Event Runnable Object class declaration file.
*/
#include "Shared_GenericEvent.h"

namespace shared { namespace lite { namespace events
{
	typedef unsigned int (__stdcall *TRunnableFunc)(void*);

	class CGenericRunnableObject
	{
	private:
		CGenericEvent            m_async_evt;
		HANDLE                   m_hThread;             // thread handle that runs a procedure
		volatile HANDLE          m_hEvent;              // synch primitive
		volatile mutable bool    m_bStopped;
		TRunnableFunc            m_function;
	protected:
		CGenericRunnableObject(TRunnableFunc, IGenericEventNotify&, const UINT eventId);
		virtual ~CGenericRunnableObject(void);
	public:
		virtual CGenericEvent&   Event(void);
		virtual bool             IsStopped(void) const;
		virtual void             MarkCompleted(void);
		virtual HRESULT          Start(void);
		virtual HRESULT          Stop(const bool bForced);
		virtual HRESULT          Wait(const DWORD = 0);
	private:
		CGenericRunnableObject(const CGenericRunnableObject&);
		CGenericRunnableObject& operator= (const CGenericRunnableObject&);
	};

	class CGenericWaitObject
	{
	protected:
		DWORD     m_nTimeSlice;    // in milliseconds
		DWORD     m_nCurrent;      // current time
		DWORD     m_nTimeFrame;
	public:
		CGenericWaitObject(const DWORD nTimeSlice, const DWORD nTimeFrame);
		virtual ~CGenericWaitObject(void);
	public:
		virtual bool IsElapsed(void) const;
		virtual bool IsReset(void) const;
		virtual void Reset(const DWORD nTimeSlice = (DWORD)-1, const DWORD nTimeFrame = (DWORD)-1);
		virtual void Wait(void);
	};
}}}

#endif/*_SHAREDLITEGENERICRUNNABLEOBJECT_H_F6EC76FE_16C4_4b39_B83B_3AB849B334B8_INCLUDED*/