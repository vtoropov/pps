#ifndef _SHAREDLITESYSTEMERROR_H_6C4A76ED_2AC1_4de4_9F33_6518E9902E99_INCLUDED
#define _SHAREDLITESYSTEMERROR_H_6C4A76ED_2AC1_4de4_9F33_6518E9902E99_INCLUDED
/*
	Created by Tech_dog (VToropov) on 19-Mar-2014 at 7:02:24am, GMT+4, Taganrog, Wednesday;
	This is Shared Lite System Error class declaration file.
*/
#include "Shared_GenericSyncObject.h"

namespace shared { namespace lite { namespace common
{
	class CSysError
	{
	private:
		::ATL::CAtlString m_buffer;
		DWORD             m_dwError;
		HRESULT           m_hrError;
	public:
		static const DWORD dwEmpty = (DWORD)(-1);
	public:
		CSysError(void);
		CSysError(const DWORD dwError);
		CSysError(const HRESULT);
		~CSysError(void);
	public:
		void       Clear(void);                                          // sets the error object to success state
		LPCTSTR    GetDescription(void) const;                           // gets the current description
		CAtlString GetFormattedDetails(const bool bMultiline=false)const;// gets formatted string like this: module, code, description, source
		HRESULT    GetHresult(void) const;                               // gets the current result code
		bool       HasDetails(void) const;                               // checks the error description buffer, if empty, returns false, otherwise, true
		void       Reset(void);                                          // re-sets the error object to empty/initial state (OLE_E_BLANK)
		void       SetHresult(const HRESULT);                            // sets the result and updates the error description
		void       SetState(const DWORD  dwError, LPCTSTR pDescription); // sets the object state manually
		void       SetState(const HRESULT hError, LPCTSTR pDescription); // sets the object state manually
		void       SetUnknownMessage(void);                              // sets the description to the unknown error message
	public:
		CSysError& operator= (const HRESULT);
	public:
		operator bool(void)const;
		operator HRESULT(void) const;
	};

	using shared::lite::runnable::CGenericSyncObject;

	class CSysErrorSafe : public CSysError
	{
		typedef CSysError TBase;
	private:
		CGenericSyncObject&  m_sync_obj;
	public:
		CSysErrorSafe(CGenericSyncObject&);
	public:
		CSysErrorSafe& operator= (const CSysError&);
	public:
		VOID          Clear(void);
		VOID          SetState(const DWORD  dwError, LPCTSTR pszDescription);
		VOID          SetState(const HRESULT hError, LPCTSTR pszDescription);
	public:
		CSysErrorSafe& operator= (const DWORD);
		CSysErrorSafe& operator= (const HRESULT);
	public:
		operator bool(void)const;
		operator HRESULT(void) const;
	private:
		CSysErrorSafe(const CSysErrorSafe&);   // cannot copy a sync object reference from a source
	};
}}}
typedef const shared::lite::common::CSysError& TErrorRef;

#endif/*_SHAREDLITESYSTEMERROR_H_6C4A76ED_2AC1_4de4_9F33_6518E9902E99_INCLUDED*/