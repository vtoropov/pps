/*
	Created by Tech_dog (VToropov) on 8-Feb-2015 at 5:41:58pm, GMT+3, Taganrog, Sunday;
	This is UIX lite custom control base class(es) implementation file.
*/
#include "StdAfx.h"
#include "UIX_ControlBase.h"

using namespace ex_ui;
using namespace ex_ui::lite;
using namespace ex_ui::lite::common;
using namespace ex_ui::lite::controls;

////////////////////////////////////////////////////////////////////////////

CRendererDefaultImpl::CRendererDefaultImpl(void)
{
}

CRendererDefaultImpl::~CRendererDefaultImpl(void)
{
}

////////////////////////////////////////////////////////////////////////////

HRESULT   CRendererDefaultImpl::DrawParentBackground(const HWND hChild, const HDC hSurface, const RECT& rcDrawArea)
{
	hChild;
	if (NULL == hSurface)
		return E_INVALIDARG;
	CColour  clr_(RGB(0xff, 0x00, 0x00));
	Gdiplus::Graphics     gp_(hSurface);
	Gdiplus::SolidBrush   br_(clr_);
	const Gdiplus::Status st_ = gp_.FillRectangle(&br_, rcDrawArea.left, rcDrawArea.top, __W(rcDrawArea), __H(rcDrawArea));
	return GdiplusStatusToHresult(st_);
}

////////////////////////////////////////////////////////////////////////////

CControlCrt::CControlCrt(const UINT ctrlId, CControlCrt& crt_ref) : 
	m_sink_ref(crt_ref.m_sink_ref),
	m_renderer_ref(crt_ref.m_renderer_ref),
	m_ctrlId(ctrlId)
{
}

CControlCrt::CControlCrt(const UINT ctrlId, IRenderer& parent_rnd, IControlNotify& snk_ref):
	m_sink_ref(snk_ref),
	m_renderer_ref(parent_rnd),
	m_ctrlId(ctrlId)
{
}

CControlCrt::~CControlCrt(void)
{
}

////////////////////////////////////////////////////////////////////////////

IControlNotify&   CControlCrt::CtrlEventSink_Ref(void)
{
	return m_sink_ref;
}

UINT              CControlCrt::CtrlId(void) const
{
	return m_ctrlId;
}

IRenderer&        CControlCrt::CtrlParentRenderer_Ref(void)
{
	return m_renderer_ref;
}

////////////////////////////////////////////////////////////////////////////

CControlBase::CControlBase(::ATL::CWindow& ctrlWnd, const CControlCrt& crt):
	m_bTracked(false),
	m_crt(crt),
	m_ctrlWnd(ctrlWnd)
{
}

CControlBase::~CControlBase(void)
{
}

typedef const CControlBase::ControlState& ConstStateRef;
////////////////////////////////////////////////////////////////////////////

HRESULT       CControlBase::GetImageSize(SIZE& _sz_out)const
{
	HRESULT hr_ = this->GetImageSize(eControlState::eNormal, _sz_out);
	return  hr_;
}

HRESULT       CControlBase::GetImageSize(const DWORD dState, SIZE& _sz_out) const
{
	if (0 < m_images.Count())
	{
		Gdiplus::Bitmap* pBitmap = m_images.Get(dState);
		if (!pBitmap)
			return TYPE_E_ELEMENTNOTFOUND;
		_sz_out.cx = pBitmap->GetWidth();
		_sz_out.cy = pBitmap->GetHeight();
		return S_OK;
	}
	return S_FALSE;
}

ConstStateRef CControlBase::GetState(void) const
{
	return m_state;
}

HRESULT       CControlBase::Refresh(const bool bAsynch)
{
	if (!m_ctrlWnd.IsWindow() || !m_ctrlWnd.IsWindowVisible())
		return S_FALSE;
	if (bAsynch)
	{
		m_ctrlWnd.Invalidate(TRUE);
	}
	else
	{
		m_ctrlWnd.RedrawWindow(0, 0, RDW_ERASE|RDW_INVALIDATE|RDW_ERASENOW|RDW_NOCHILDREN);
	}
	return S_OK;
}

HRESULT       CControlBase::SetImage(const DWORD dwState, const UINT nResId, const HMODULE hResourceModule)
{
	if (m_images.Has(dwState))
		m_images.Remove(dwState);

	Gdiplus::Bitmap* pBitmap = NULL;
	HRESULT hr_ = CGdiPlusPngLoader::LoadResource(nResId, hResourceModule, pBitmap);
	if (S_OK != hr_)
		return  hr_;
	hr_ = m_images.Add(dwState, pBitmap);
	if (S_OK != hr_)
	{
		try
		{
			delete pBitmap; pBitmap = NULL;
		} catch(...){ hr_ = E_OUTOFMEMORY; }
	}
	return  hr_;
}

////////////////////////////////////////////////////////////////////////////

void          CControlBase::DrawBkgnd(CZBuffer& dc_)
{
	RECT rc_ = {0};
	m_ctrlWnd.GetWindowRect(&rc_);
	if (::IsRectEmpty(&rc_))
		return;
	::OffsetRect(&rc_, -rc_.left, -rc_.top);
	m_crt.CtrlParentRenderer_Ref().DrawParentBackground(m_ctrlWnd.m_hWnd, dc_, rc_);
}

void          CControlBase::DrawImage(CZBuffer& dc_)
{
	Gdiplus::Bitmap* pBitmap = m_images.Get(m_state.dwState);
	if (!pBitmap)
	{
		if (eControlState::eDisabled & m_state.dwState)
			pBitmap = m_images.Get(eControlState::eDisabled);
		if (pBitmap == NULL)
			return;
	}
	Gdiplus::Graphics g_(dc_);
	const POINT anchor_ = {0, 0};
	g_.DrawImage(pBitmap, anchor_.x, anchor_.y, pBitmap->GetWidth(), pBitmap->GetHeight());
}

void          CControlBase::DrawImage(CZBuffer& dc_, const RECT& rcDrawArea)
{
	this->DrawImage(dc_, rcDrawArea, m_state.dwState);
}

void          CControlBase::DrawImage(CZBuffer& dc_, const RECT& rcDrawArea, const DWORD dState)
{
	Gdiplus::Bitmap* pBitmap = m_images.Get(dState);
	if (!pBitmap)
	{
		if (eControlState::eDisabled & dState)
			pBitmap = m_images.Get(eControlState::eDisabled);
		if (pBitmap == NULL)
			return;
	}
	Gdiplus::Graphics g_(dc_);
	const POINT anchor_ = {rcDrawArea.left, rcDrawArea.top};
	g_.DrawImage(pBitmap, anchor_.x, anchor_.y, pBitmap->GetWidth(), pBitmap->GetHeight());
}

HRESULT       CControlBase::RecalcWindowRect(RECT& _rc_out)
{
	if (0 < m_images.Count())
	{
		Gdiplus::Bitmap* pBitmap = m_images.Get(eControlState::eNormal);
		if (!pBitmap)
			return TYPE_E_ELEMENTNOTFOUND;
		_rc_out.right  = _rc_out.left + pBitmap->GetWidth();
		_rc_out.bottom = _rc_out.top  + pBitmap->GetHeight();
	}
	else
	{
		SIZE sz_ = {0, 0};
		this->GetDefaultSize(sz_);
		_rc_out.right  = _rc_out.left + sz_.cx;
		_rc_out.bottom = _rc_out.top  + sz_.cy;
	}
	return S_OK;
}

void          CControlBase::SetMouseTrack(void)
{
	if (m_bTracked)
		return;
	TRACKMOUSEEVENT tm = {0};
	tm.cbSize    = sizeof(TRACKMOUSEEVENT);
	tm.dwFlags   = TME_LEAVE;
	tm.hwndTrack = m_ctrlWnd;
	BOOL bResult = ::TrackMouseEvent(&tm); ATLASSERT(bResult);
	if ( bResult )
		m_bTracked = true;
}