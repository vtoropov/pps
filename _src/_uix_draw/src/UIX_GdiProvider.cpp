/*
	Created by Tech_dog (VToropov) on 23-Mar-2014 at 8:53:20pm, GMT+4, Saint-Petersburg, Sunday;
	This is UIX Draw library GDI Provider class implementation file.
*/
#include "StdAfx.h"
#include "UIX_GdiProvider.h"

using namespace ex_ui;
using namespace ex_ui::draw;
using namespace ex_ui::draw::common;

typedef Gdiplus::Bitmap TBitmap;
////////////////////////////////////////////////////////////////////////////

namespace ex_ui { namespace draw
{
	HRESULT   GdiplusStatusToHresult(const Gdiplus::Status st__)
	{
		HRESULT hr__ = E_UNEXPECTED;
		switch (st__)
		{
		case Gdiplus::Ok:                        hr__ = S_OK;                                        break;
		case Gdiplus::GenericError:              hr__ = E_FAIL;                                      break;
		case Gdiplus::InvalidParameter:          hr__ = E_INVALIDARG;                                break;
		case Gdiplus::OutOfMemory:               hr__ = E_OUTOFMEMORY;                               break;
		case Gdiplus::ObjectBusy:                hr__ = HRESULT_FROM_WIN32(ERROR_BUSY);              break;
		case Gdiplus::InsufficientBuffer:        hr__ = DISP_E_BUFFERTOOSMALL;                       break;
		case Gdiplus::NotImplemented:            hr__ = E_NOTIMPL;                                   break;
		case Gdiplus::Win32Error:                hr__ = E_FAIL;                                      break;
		case Gdiplus::WrongState:                hr__ = HRESULT_FROM_WIN32(ERROR_INVALID_STATE);     break;
		case Gdiplus::Aborted:                   hr__ = E_ABORT;                                     break;
		case Gdiplus::FileNotFound:              hr__ = HRESULT_FROM_WIN32(ERROR_FILE_NOT_FOUND);    break;
		case Gdiplus::ValueOverflow:             hr__ = DISP_E_OVERFLOW;                             break;
		case Gdiplus::AccessDenied:              hr__ = E_ACCESSDENIED;                              break;
		case Gdiplus::UnknownImageFormat:        hr__ = HRESULT_FROM_WIN32(ERROR_UNSUPPORTED_TYPE);  break;
		case Gdiplus::FontFamilyNotFound:        hr__ = DISP_E_UNKNOWNNAME;                          break;
		case Gdiplus::FontStyleNotFound:         hr__ = HRESULT_FROM_WIN32(ERROR_UNSUPPORTED_TYPE);  break;
		case Gdiplus::NotTrueTypeFont:           hr__ = DISP_E_TYPEMISMATCH;                         break;
		case Gdiplus::UnsupportedGdiplusVersion: hr__ = HRESULT_FROM_WIN32(ERROR_PRODUCT_VERSION);   break;
		case Gdiplus::GdiplusNotInitialized:     hr__ = OLE_E_BLANK;                                 break;
		case Gdiplus::PropertyNotFound:          hr__ = HRESULT_FROM_WIN32(ERROR_UNKNOWN_PROPERTY);  break;
		case Gdiplus::PropertyNotSupported:      hr__ = HRESULT_FROM_WIN32(ERROR_UNKNOWN_PROPERTY);  break;
		default:
			break;
		}
		return  hr__;
	}
}}

////////////////////////////////////////////////////////////////////////////

namespace ex_ui { namespace draw { namespace details
{
	void GdiProvider_DrawGradRect(const HDC hDC, const RECT& rcDraw, const COLORREF clrFrom, const COLORREF clrUpto, const bool bVertical)
	{
		TRIVERTEX        vert[2] = {0};
		GRADIENT_RECT    gRect   = {0};
		vert [0] .x      = rcDraw.left;
		vert [0] .y      = rcDraw.top;
		vert [0] .Red    = GetRValue(clrFrom)<<8;
		vert [0] .Green  = GetGValue(clrFrom)<<8;
		vert [0] .Blue   = GetBValue(clrFrom)<<8;
		vert [0] .Alpha  = 0x0000;

		vert [1] .x      = rcDraw.right;
		vert [1] .y      = rcDraw.bottom; 
		vert [1] .Red    = GetRValue(clrUpto)<<8;
		vert [1] .Green  = GetGValue(clrUpto)<<8;
		vert [1] .Blue   = GetBValue(clrUpto)<<8;
		vert [1] .Alpha  = 0x0000;

		gRect.UpperLeft  = 0;
		gRect.LowerRight = 1;
		::GradientFill(hDC, vert, 2, &gRect, 1, bVertical ? GRADIENT_FILL_RECT_V : GRADIENT_FILL_RECT_H);
	}

	class CRectFEx
	{
	private:
		Gdiplus::RectF   m_rc;
	public:
		CRectFEx(const RECT& rc_) : m_rc((Gdiplus::REAL)rc_.left, (Gdiplus::REAL)rc_.top, (Gdiplus::REAL)__W(rc_), (Gdiplus::REAL)__H(rc_))
		{
		}
	public:
		operator Gdiplus::RectF*(void){ return &m_rc; }
		operator Gdiplus::RectF&(void){ return  m_rc; }
	};
}}}

////////////////////////////////////////////////////////////////////////////

CZBuffer::CZBuffer(const HDC hDC, const RECT& rcPaint):
	m_hOrigin(hDC),
	m_hBmpOld(NULL)
{
	m_rcPaint = rcPaint;
	TBaseDC::CreateCompatibleDC(m_hOrigin);
	ATLASSERT(m_hDC != NULL);

	const int iWidth  = m_rcPaint.right - m_rcPaint.left;
	const int iHeight = m_rcPaint.bottom - m_rcPaint.top;

	m_surface.CreateCompatibleBitmap(m_hOrigin, iWidth, iHeight);
	if (0==m_surface)
	{
		BITMAPINFO bmpinf = {0};
		bmpinf.bmiHeader.biSize   = sizeof( bmpinf );
		bmpinf.bmiHeader.biWidth  = iWidth;
		bmpinf.bmiHeader.biHeight = iHeight;
		bmpinf.bmiHeader.biPlanes = 1;
		bmpinf.bmiHeader.biBitCount = 32;
		bmpinf.bmiHeader.biCompression = BI_RGB;
		m_surface = ::CreateDIBSection(hDC, &bmpinf, DIB_RGB_COLORS, NULL, NULL, 0);
		if(!m_surface)
		{
			DWORD dwError = ::GetLastError(); dwError;
			ATLASSERT(0);
		};
	}
	m_hBmpOld = TBaseDC::SelectBitmap(m_surface);
	TBaseDC::SetViewportOrg(-m_rcPaint.left, -m_rcPaint.top);
}

CZBuffer::~CZBuffer(VOID)
{
	const int iWidth  = m_rcPaint.right - m_rcPaint.left;
	const int iHeight = m_rcPaint.bottom - m_rcPaint.top;
	::BitBlt(m_hOrigin, m_rcPaint.left, m_rcPaint.top, iWidth, iHeight, m_hDC, m_rcPaint.left, m_rcPaint.top, SRCCOPY);
	TBaseDC::SelectBitmap(m_hBmpOld);
}

////////////////////////////////////////////////////////////////////////////

HRESULT CZBuffer::CopyTo(HBITMAP& hBitmap)
{
	if (hBitmap)
		return HRESULT_FROM_WIN32(ERROR_OBJECT_ALREADY_EXISTS);

	HRESULT hr_ = S_OK;
	HDC hCompatible = ::CreateCompatibleDC(*this);
	if (!hCompatible)
		return HRESULT_FROM_WIN32(::GetLastError());

	BITMAPINFO bmpinf = {0};
	{
		bmpinf.bmiHeader.biSize        = sizeof(BITMAPINFO);
		bmpinf.bmiHeader.biWidth       = __W(m_rcPaint);
		bmpinf.bmiHeader.biHeight      = __H(m_rcPaint);
		bmpinf.bmiHeader.biPlanes      = 1;
		bmpinf.bmiHeader.biBitCount    = 32;
		bmpinf.bmiHeader.biCompression = BI_RGB;
	}
	hBitmap = ::CreateDIBSection(*this, &bmpinf, DIB_RGB_COLORS, NULL, NULL, 0);
	{
		HGDIOBJ prev = ::SelectObject(hCompatible, (HGDIOBJ)hBitmap);
		hr_ = this->CopyTo(hCompatible, 0, 0);
		::SelectObject(hCompatible, prev);
	}
	::DeleteDC(hCompatible);
	hCompatible = NULL;
	return  hr_;
}

HRESULT CZBuffer::CopyTo(CONST HDC hCompatibleDC, const INT _x, const INT _y, const BYTE _alpha)
{
	if (!this->IsValid())
		return OLE_E_BLANK;

	const SIZE sz_ = {__W(m_rcPaint), __H(m_rcPaint)};

	if (255 == _alpha)
	{
		const BOOL bResult = ::BitBlt(hCompatibleDC, _x, _y, sz_.cx, sz_.cy, TBaseDC::m_hDC, m_rcPaint.left, m_rcPaint.top, SRCCOPY);
		return (!bResult ? HRESULT_FROM_WIN32(::GetLastError()) : S_OK);
	}
	else
	{
		BLENDFUNCTION bf = {0};
		bf.BlendOp       = AC_SRC_OVER;
		bf.BlendFlags    = 0;
		bf.SourceConstantAlpha = _alpha;
		bf.AlphaFormat         = 0;
		const BOOL bResult = ::AlphaBlend(
				hCompatibleDC,
				_x,
				_y,
				sz_.cx,
				sz_.cy,
				TBaseDC::m_hDC,
				m_rcPaint.left,
				m_rcPaint.top,
				sz_.cx,
				sz_.cy,
				bf
			);
		return (bResult ? S_OK : HRESULT_FROM_WIN32(::GetLastError()));
	}
}

VOID    CZBuffer::DrawGragRect(const RECT& rcDraw, const COLORREF clrFrom, const COLORREF clrUpto, const bool bVertical, const BYTE ba) const
{
	if (::IsRectEmpty(&rcDraw) || !TBaseDC::m_hDC)
		return;
	if (ba < 1) // fully transparent
		return;

	if (ba < 255)
	{
		HDC hdcMem = ::CreateCompatibleDC(TBaseDC::m_hDC);
		SIZE sz = {rcDraw.right - rcDraw.left, rcDraw.bottom - rcDraw.top}; 

		HBITMAP hbmpCanvas = ::CreateCompatibleBitmap(TBaseDC::m_hDC, sz.cx, sz.cy);
		if (hbmpCanvas)
		{
			RECT rcFrame = {0, 0, sz.cx, sz.cy};
			const int iZPoint  = ::SaveDC(hdcMem);
			::SelectObject(hdcMem, (HGDIOBJ)hbmpCanvas);

			details::GdiProvider_DrawGradRect(hdcMem, rcFrame, clrFrom, clrUpto, bVertical);

			BLENDFUNCTION blfun = {0}; 
			blfun.BlendOp       = AC_SRC_OVER; 
			blfun.BlendFlags    = 0; 
			blfun.SourceConstantAlpha = BYTE(int(255 * ba/100)); 
			blfun.AlphaFormat   = 0;

			::AlphaBlend(TBaseDC::m_hDC, rcDraw.left, rcDraw.top, sz.cx, sz.cy, hdcMem, 0, 0, sz.cx, sz.cy, blfun);

			::RestoreDC(hdcMem, iZPoint);
			::DeleteObject((HGDIOBJ)hbmpCanvas);
		}
		::DeleteDC(hdcMem);
		return;
	}
	else
		details::GdiProvider_DrawGradRect(TBaseDC::m_hDC, rcDraw, clrFrom, clrUpto, bVertical);
}

VOID    CZBuffer::DrawLine(const INT _x0, const INT _y0, const INT _x1, const INT _y1, const COLORREF _clr, const INT nThickness)
{
	::WTL::CPen cPen;
	cPen.CreatePen(PS_SOLID, nThickness, _clr);

	const INT nSave = TBaseDC::SaveDC();
	TBaseDC::SelectPen(cPen);

	TBaseDC::MoveTo(_x0, _y0);
	TBaseDC::LineTo(_x1, _y1);

	TBaseDC::RestoreDC(nSave);
}

VOID    CZBuffer::DrawSolidRect(const RECT& rcDraw, const CColour& clr)const
{
	Gdiplus::Rect rc0(rcDraw.left, rcDraw.top, __W(rcDraw), __H(rcDraw));
	Gdiplus::SolidBrush br0(clr);
	Gdiplus::Graphics gp_(TBaseDC::m_hDC);
	gp_.FillRectangle(&br0, rc0);
}

VOID    CZBuffer::DrawSolidRect(const RECT& rcDraw, const COLORREF clrFill, const BYTE _alpha) const
{
	if (::IsRectEmpty(&rcDraw) || CLR_NONE == clrFill)
		return;
	if (eAlphaValue::eTransparent == _alpha)
		return;

	CColour clr(clrFill, _alpha);
	this->DrawSolidRect(rcDraw, clr);
}

VOID    CZBuffer::DrawTextExt(LPCTSTR pText, const HFONT fnt_, const RECT& rcDraw, const COLORREF clrFore, const DWORD fmt_)
{
	if (!pText || !::_tcslen(pText))
		return;
	if (::IsRectEmpty(&rcDraw))
		return;
	const HFONT    fnt_loc = (NULL == fnt_ ? (HFONT)::GetStockObject(DEFAULT_GUI_FONT) : fnt_);
	const COLORREF clr_loc = (CLR_NONE == clrFore ? ::GetSysColor(COLOR_WINDOWTEXT) : clrFore);
	const DWORD    fmt_loc = (0 == fmt_ ? DT_LEFT | DT_VCENTER | DT_WORDBREAK | DT_NOCLIP | DT_NOPREFIX : fmt_);
	const INT   nSavePoint = TBaseDC::SaveDC();
	TBaseDC::SelectFont(fnt_loc);
	TBaseDC::SetBkMode(TRANSPARENT);
	TBaseDC::SetTextColor(clr_loc);
	TBaseDC::DrawText(pText, -1, const_cast<LPRECT>(&rcDraw), fmt_loc);
	TBaseDC::RestoreDC(nSavePoint);
}

VOID    CZBuffer::DrawTextExt(
				LPCTSTR pszText,
				LPCTSTR pszFontFamily,
				const DWORD dwFontSize,
				const RECT& rcDraw,
				const COLORREF clrFore,
				const DWORD dwFormat
			)
{
	if (!pszText
		|| !(::_tcslen(pszText) > 0)
		|| !pszFontFamily
		|| !(::_tcslen(pszFontFamily) > 0)
		|| !(dwFontSize > 0)
		|| ::IsRectEmpty(&rcDraw))
	return;

	Gdiplus::Graphics gp_(TBaseDC::m_hDC);
	details::CRectFEx rc_(rcDraw);

	CColour     color_(clrFore == CLR_NONE ? ::GetSysColor(COLOR_WINDOWTEXT) : clrFore);
	Gdiplus::Font fnt_(pszFontFamily, (Gdiplus::REAL)dwFontSize);

	Gdiplus::SolidBrush br_(color_);
	Gdiplus::StringFormat sfmt(Gdiplus::StringFormatFlagsNoClip);

	if (false){}
	else if (DT_RIGHT  & dwFormat)  sfmt.SetAlignment(Gdiplus::StringAlignmentFar );
	else if (DT_CENTER & dwFormat)  sfmt.SetAlignment(Gdiplus::StringAlignmentCenter);
	else                            sfmt.SetAlignment(Gdiplus::StringAlignmentNear);

	if (false){}
	else if (DT_VCENTER & dwFormat) sfmt.SetLineAlignment(Gdiplus::StringAlignmentCenter);
	else if (DT_BOTTOM  & dwFormat) sfmt.SetLineAlignment(Gdiplus::StringAlignmentFar);
	else                            sfmt.SetLineAlignment(Gdiplus::StringAlignmentNear);

	gp_.SetTextRenderingHint(Gdiplus::TextRenderingHintAntiAlias);
	gp_.DrawString(
			pszText,
			(INT)::_tcslen(pszText),
			&fnt_,
			rc_,
			&sfmt,
			&br_
		);
}

bool    CZBuffer::IsValid(void)const
{
	return (!TBaseDC::IsNull() && !::IsRectEmpty(&m_rcPaint) && !m_surface.IsNull());
}

////////////////////////////////////////////////////////////////////////////

CGdiPlusLibLoader::CGdiPlusLibLoader(void) : m_gdiPlusToken(0)
{
	Gdiplus::GdiplusStartupInput gdiplusStartupInput;
	Gdiplus::GdiplusStartup(&m_gdiPlusToken, &gdiplusStartupInput, NULL);
}

CGdiPlusLibLoader::~CGdiPlusLibLoader(void)
{
	Gdiplus::GdiplusShutdown(m_gdiPlusToken);
	m_gdiPlusToken = 0;
}

////////////////////////////////////////////////////////////////////////////

namespace ex_ui { namespace draw { namespace details
{
	struct GdiProvider_ResourceInfo
	{
		PVOID   __res;
		DWORD   __size;
		GdiProvider_ResourceInfo(void) : __res(0), __size(0) {}
	};

	static HRESULT GdiProvider_GetResourcePtr(const ATL::_U_STRINGorID RID, const HMODULE hModule, GdiProvider_ResourceInfo& res__)
	{
		if (!RID.m_lpstr)
			return E_INVALIDARG;

		HRSRC hResource = ::FindResource(hModule, RID.m_lpstr, _T("PNG"));
		if (!hResource)
			return OLE_E_CANT_BINDTOSOURCE;

		res__.__size = ::SizeofResource(hModule, hResource);
		if (!res__.__size)
			return ERROR_EMPTY;

		res__.__res = ::LockResource(::LoadResource(hModule, hResource));
		if (!res__.__res)
			return STG_E_LOCKVIOLATION;
		else
			return S_OK;
	}

	class  GdiProvider_GlobalAllocator
	{
	private:
		HGLOBAL    m_hGlobal;
		HRESULT    m_hResult;
		DWORD      m_dwSize;
		PVOID      m_pVoid;
	public:
		GdiProvider_GlobalAllocator::GdiProvider_GlobalAllocator(const DWORD size__, const bool fixed__ = false):
			m_hGlobal(NULL),
			m_hResult(OLE_E_BLANK),
			m_dwSize(size__),
			m_pVoid(NULL)
		{
			try
			{
				m_hGlobal = ::GlobalAlloc(fixed__ ? GMEM_FIXED : GMEM_MOVEABLE, m_dwSize);
				m_hResult = (m_hGlobal != NULL ? S_OK : E_OUTOFMEMORY);
				if (S_OK == m_hResult)
				{
					m_pVoid = ::GlobalLock(m_hGlobal);
					if (!m_pVoid)
						 m_hResult = HRESULT_FROM_WIN32(::GetLastError());
				}
			}
			catch(...)
			{
				m_hResult = E_OUTOFMEMORY;
			}
		}
			GdiProvider_GlobalAllocator::~GdiProvider_GlobalAllocator(void)
		{
			m_pVoid = NULL;
			if (m_hGlobal)
			{
				::GlobalUnlock(m_hGlobal);
				::GlobalFree(m_hGlobal);
				m_hGlobal = NULL;
			}
		}
	public:
		HGLOBAL    GdiProvider_GlobalAllocator::GetHandle(void) const
		{
			return m_hGlobal;
		}
		HRESULT    GdiProvider_GlobalAllocator::GetLastResult(void) const
		{
			return m_hResult;
		}
		PVOID      GdiProvider_GlobalAllocator::GetPtr(void) const
		{
			return m_pVoid;
		}
		DWORD      GdiProvider_GlobalAllocator::GetSize(void) const
		{
			return m_dwSize;
		}
	private:
		GdiProvider_GlobalAllocator(const GdiProvider_GlobalAllocator&);
		GdiProvider_GlobalAllocator& operator=(const GdiProvider_GlobalAllocator&);
	};
}}}

////////////////////////////////////////////////////////////////////////////

HRESULT     CGdiPlusPngLoader::LoadResource(const ATL::_U_STRINGorID RID, const HMODULE hModule, Gdiplus::Bitmap*& ptr_ref)
{
	if (ptr_ref)
		return HRESULT_FROM_WIN32(ERROR_OBJECT_ALREADY_EXISTS);

	details::GdiProvider_ResourceInfo res__;
	HRESULT hr = details::GdiProvider_GetResourcePtr(RID, hModule, res__);
	if (S_OK != hr)
		return hr;

	details::GdiProvider_GlobalAllocator ga__(res__.__size, false);
	if (S_OK != (hr = ga__.GetLastResult()))
		return hr;

	::CopyMemory(ga__.GetPtr(), res__.__res, res__.__size);

	::ATL::CComPtr<IStream> pStream;
	hr = ::CreateStreamOnHGlobal(ga__.GetHandle(), FALSE, &pStream);
	if (S_OK==hr)
	{
		ptr_ref = Gdiplus::Bitmap::FromStream(pStream);
		if (ptr_ref)
		{
			hr = ((*ptr_ref).GetLastStatus() == Gdiplus::Ok ? S_OK : E_OUTOFMEMORY);
		}
		else
		{
			hr = E_OUTOFMEMORY;
		}
	}
	return hr;
}

HRESULT     CGdiPlusPngLoader::LoadResource(const ATL::_U_STRINGorID RID, const HMODULE hModule, HBITMAP& hBitmap)
{
	if (hBitmap)
		return HRESULT_FROM_WIN32(ERROR_OBJECT_ALREADY_EXISTS);
	Gdiplus::Bitmap* pBitmap = NULL;
	HRESULT hr_ = CGdiPlusPngLoader::LoadResource(RID, hModule, pBitmap);
	if (S_OK != hr_)
		return  hr_;
	if (!pBitmap)
		return E_UNEXPECTED;
	pBitmap->GetHBITMAP(0, &hBitmap);
	try
	{
		delete pBitmap; pBitmap = NULL;
	}
	catch(...){ return E_OUTOFMEMORY; }
	return hr_;
}