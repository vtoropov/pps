/*
	Created by Tech_dog (VToropov) on 6-Feb-2015 at 9:35:04PM, GMT+3, Taganrog, Friday;
	This is UIX draw library renderer class(es) implementation file.
*/
#include "StdAfx.h"
#include "UIX_Renderer.h"
#include "UIX_GdiProvider.h"

using namespace ex_ui;
using namespace ex_ui::draw;
using namespace ex_ui::draw::common;
using namespace ex_ui::draw::renderers;

////////////////////////////////////////////////////////////////////////////

CBackgroundRendererDefImpl::CBackgroundRendererDefImpl(::ATL::CWindow&  owner_ref):
	m_owner_ref(owner_ref),
	m_x_shift(0),
	m_y_shift(0)
{
}

CBackgroundRendererDefImpl::~CBackgroundRendererDefImpl(void)
{
}

////////////////////////////////////////////////////////////////////////////

HRESULT   CBackgroundRendererDefImpl::DrawParentBackground(const HWND hChild, const HDC hSurface, const RECT& rcDrawArea)
{
	if (::IsRectEmpty(&rcDrawArea) || NULL == hSurface || !::IsWindow(hChild))
		return E_INVALIDARG;

	if (!m_bkgnd_cache.IsValidObject())
		return m_bkgnd_cache.GetLastResult();

	CPngBitmap* pPngBitmap = m_bkgnd_cache.GetObject();
	if (!pPngBitmap || !pPngBitmap->IsValid())
		return E_UNEXPECTED;

	POINT ptAnchor = {0};
	if (m_owner_ref.IsWindow())
	{
		RECT rcThisWnd = {0};
		m_owner_ref.GetWindowRect(&rcThisWnd);
		RECT rcChild = {0};
		::GetWindowRect(hChild, &rcChild);
		ptAnchor.x = (rcChild.left - rcThisWnd.left) - m_x_shift;
		ptAnchor.y = (rcChild.top - rcThisWnd.top) - m_y_shift;
	}
	if (true)
	{
		RECT clipping__ = rcDrawArea;
		::OffsetRect(&clipping__, ptAnchor.x, ptAnchor.y);
		CGdiplusBitmapWrap wrap(pPngBitmap->GetPtr(), false);
		Gdiplus::Bitmap* pTarget = NULL;
		HRESULT hr__ = wrap.Clip(clipping__, pTarget);
		if (S_OK == hr__)
		{
			Gdiplus::Graphics gp__(hSurface);
			gp__.DrawImage(pTarget, rcDrawArea.left, rcDrawArea.top);
			delete pTarget; pTarget = NULL;
		}
	}
	return S_OK;
}

HRESULT   CBackgroundRendererDefImpl::GetImageSize(SIZE& __in_out_ref) const
{
	if (!m_bkgnd_cache.IsValidObject())
		return m_bkgnd_cache.GetLastResult();
	const CPngBitmap* pPngBitmap = m_bkgnd_cache.GetObject();
	if (!pPngBitmap || !pPngBitmap->IsValid())
		return E_UNEXPECTED;
	Gdiplus::Bitmap* pBitmap = pPngBitmap->GetPtr();
	if (NULL == pBitmap)
		return E_UNEXPECTED;
	__in_out_ref.cx = (LONG)(*pBitmap).GetWidth();
	__in_out_ref.cy = (LONG)(*pBitmap).GetHeight();
	return S_OK;
}

HRESULT   CBackgroundRendererDefImpl::SetVerticalShift(const INT y__)
{
	const bool bChanged = (y__ != m_y_shift);
	if (bChanged)
	{
		m_y_shift = y__;
	}
	return (true == bChanged ? S_OK : S_FALSE);
}

////////////////////////////////////////////////////////////////////////////

CBackgroundTileRenderer::CBackgroundTileRenderer(::ATL::CWindow&  owner_ref):
	TBase(owner_ref)
{
}

CBackgroundTileRenderer::CBackgroundTileRenderer(const UINT nResId, ::ATL::CWindow&  owner_ref):
	TBase(owner_ref)
{
	ATLASSERT(nResId);
	const HINSTANCE hInstance = ::ATL::_AtlBaseModule.GetModuleInstance();

	Gdiplus::Bitmap* pTileBitmap = NULL;

	HRESULT hr_ = CGdiPlusPngLoader::LoadResource(nResId, hInstance, pTileBitmap);
	if (S_OK == hr_)
		m_tile_ptr.Attach(pTileBitmap);
}

CBackgroundTileRenderer::~CBackgroundTileRenderer(void)
{
}

////////////////////////////////////////////////////////////////////////////

HRESULT   CBackgroundTileRenderer::DrawBackground(const HDC hSurface, const RECT& rcDrawArea)
{
	CZBuffer dc__(hSurface, rcDrawArea);
	HRESULT  hr__ = Draw(dc__, rcDrawArea);
	return   hr__;
}

////////////////////////////////////////////////////////////////////////////

HRESULT   CBackgroundTileRenderer::Draw(CZBuffer& dc__, const RECT& rcDrawArea)
{
	if (::IsRectEmpty(&rcDrawArea))
		return E_INVALIDARG;
	HRESULT hr__ = m_tile_ptr.GetLastResult();
	if (S_OK != hr__)
		return  hr__;
	
	hr__ = __create_cache(rcDrawArea);
	if (S_OK != hr__)
		return  hr__;
	
	CPngBitmap* p_png_bkgnd__ = TBase::m_bkgnd_cache.GetObject();
	if (!p_png_bkgnd__)
		return E_UNEXPECTED;
	Gdiplus::Bitmap*  p_bkgnd_bmp = p_png_bkgnd__->GetPtr();
	if (!p_bkgnd_bmp)
		return E_UNEXPECTED;

	Gdiplus::Graphics gp__(dc__);
	gp__.DrawImage(p_bkgnd_bmp, rcDrawArea.left, rcDrawArea.top, __W(rcDrawArea), __H(rcDrawArea));

	return S_OK;
}

HRESULT   CBackgroundTileRenderer::InitializeFromFile(LPCTSTR pszFilePath)
{
	if (!pszFilePath || !::_tcslen(pszFilePath))
		return E_INVALIDARG;

	CPngBitmap png;

	HRESULT hr_ = png.Load(pszFilePath);
	if (S_OK == hr_)
	{
		Gdiplus::Bitmap*  pTileBitmap = png.Detach();
		m_tile_ptr.Attach(pTileBitmap);
	}
	return hr_;
}

////////////////////////////////////////////////////////////////////////////

HRESULT   CBackgroundTileRenderer::__create_cache(const RECT& rcDrawArea)
{
	if (TBase::m_bkgnd_cache.IsValidObject()) // it is already cached
		return S_OK;

	CPngBitmap* p_png_bmp__ = m_tile_ptr.GetObject();
	if (!p_png_bmp__)
		return E_UNEXPECTED;
	Gdiplus::Bitmap* pBitmap = p_png_bmp__->GetPtr();
	if (!pBitmap)
		return E_UNEXPECTED;

	SIZE szImage = {0, 0};
	szImage.cy   = pBitmap->GetHeight();
	szImage.cx   = pBitmap->GetWidth();
	
	HRESULT hr__ = TBase::m_bkgnd_cache.Create(__W(rcDrawArea), __H(rcDrawArea));
	if (S_OK != hr__)
		return  hr__;
	CPngBitmap* p_png_bkgnd__ = TBase::m_bkgnd_cache.GetObject();
	if (!p_png_bkgnd__)
		return E_UNEXPECTED;
	Gdiplus::Bitmap*  p_bkgnd_bmp = p_png_bkgnd__->GetPtr();
	if (!p_bkgnd_bmp)
		return E_UNEXPECTED;

	Gdiplus::Graphics gr__(p_bkgnd_bmp);

	INT top__  = rcDrawArea.top;

	while (true)
	{
		INT left__ = rcDrawArea.left;
		while (true)
		{
			gr__.DrawImage(pBitmap, left__, top__, szImage.cx, szImage.cy);
			left__ += szImage.cx;
			if (rcDrawArea.right <= left__)
				break;
		}
		top__ += szImage.cy;
		if (rcDrawArea.bottom <= top__)
			break;
	}
	return S_OK;
}