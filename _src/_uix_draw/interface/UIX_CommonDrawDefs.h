#ifndef _UIXDRAWCOMMONDEFINITIONS_H_AD2F5A63_32F0_40b2_935F_DFEBF2590E79_INCLUDED
#define _UIXDRAWCOMMONDEFINITIONS_H_AD2F5A63_32F0_40b2_935F_DFEBF2590E79_INCLUDED
/*
	Created by Tech_dog (VToropov) on 5-Feb-2015 at 11:41:14pm, GMT+3, Taganrog, Thursday;
	This is UIX Draw library common definition/class declaration file.
*/
#include <gdiplus.h>

#define __HRESULT_FROM_LASTERROR()   HRESULT_FROM_WIN32(::GetLastError())
#define __H(rc__)    (rc__.bottom - rc__.top)
#define __W(rc__)    (rc__.right - rc__.left)

#define RGBA(r,g,b,a)	( RGB(r,g,b) | (((DWORD)(BYTE)(a))<<24) )
#define GetAValue(_clr)	( 0 == LOBYTE((_clr)>>24) ? 255 : LOBYTE((_clr)>>24) )

namespace ex_ui { namespace draw { namespace defs
{
	interface IRenderer
	{
		virtual   HRESULT  DrawBackground(const HDC hSurface, const RECT& rcDrawArea) {hSurface; rcDrawArea; return E_NOTIMPL;}
		virtual   HRESULT  DrawParentBackground(const HWND hChild, const HDC hSurface, const RECT& rcDrawArea) PURE;
	};
}}}

#endif/*_UIXDRAWCOMMONDEFINITIONS_H_AD2F5A63_32F0_40b2_935F_DFEBF2590E79_INCLUDED*/