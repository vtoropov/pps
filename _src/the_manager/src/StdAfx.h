#ifndef __PLATINUMCLIENTAPPLICATIONPRECOMPILEDHEADER_H_69884967_B636_49f2_B4E8_1AD03C90BD38_INCLUDED
#define __PLATINUMCLIENTAPPLICATIONPRECOMPILEDHEADER_H_69884967_B636_49f2_B4E8_1AD03C90BD38_INCLUDED
/*
	Created by Tech_dog (VToropov) on 22-Mar-2014 at 7:37:47pm, GMT+4, Moscow Region,
	Rail Road Train #43, Coatch #6, Place #1, Saturday;
	This is Platinum Payroll Systems Client application precompiled headers definition file.
*/
#include "PlatinumClient_TargetVersion.h"

#pragma warning(disable: 4481)  // nonstandard extension used: override specifier 'override'
#pragma warning(disable: 4996)  // security warning: function or variable may be unsafe

#include <atlbase.h>
#include <atlstr.h>             // important order, this file must be incloded before any includes of the WTL headers
#include <atlapp.h>

extern CAppModule _Module;

#include <atlwin.h>
#include <atlframe.h>
#include <atlctrls.h>
#include <atldlgs.h>
#include <atlcrack.h>
#include <atlsafe.h>
#include <atltheme.h>

#include <comutil.h>

#include "Shared_EventLogger.h"
#include <map>
#include <vector>

#if defined WIN64
  #pragma comment(linker, "/manifestdependency:\"type='win32' name='Microsoft.Windows.Common-Controls' version='6.0.0.0' processorArchitecture='amd64' publicKeyToken='6595b64144ccf1df' language='*'\"")
#elif defined WIN32
  #pragma comment(linker, "/manifestdependency:\"type='win32' name='Microsoft.Windows.Common-Controls' version='6.0.0.0' processorArchitecture='x86' publicKeyToken='6595b64144ccf1df' language='*'\"")
#elif defined _M_IA64
  #pragma comment(linker, "/manifestdependency:\"type='win32' name='Microsoft.Windows.Common-Controls' version='6.0.0.0' processorArchitecture='ia64' publicKeyToken='6595b64144ccf1df' language='*'\"")
#else
  #pragma comment(linker, "/manifestdependency:\"type='win32' name='Microsoft.Windows.Common-Controls' version='6.0.0.0' processorArchitecture='*' publicKeyToken='6595b64144ccf1df' language='*'\"")
#endif

#if defined(_DEBUG)
	#pragma comment(lib, "UIX_Draw_V9D.lib")
	#pragma comment(lib, "UIX_Frame_V9D.lib")
#else
	#pragma comment(lib, "UIX_Draw_V9.lib")
	#pragma comment(lib, "UIX_Frame_V9.lib")
#endif

#endif/*__PLATINUMCLIENTAPPLICATIONPRECOMPILEDHEADER_H_69884967_B636_49f2_B4E8_1AD03C90BD38_INCLUDED*/