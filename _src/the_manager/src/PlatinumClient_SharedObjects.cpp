/*
	Created by Tech_dog (VToropov) on 9-Apr-2014 at 4:31:45pm, GMT+4, Saint-Petersburg, Wednesday;
	This is Platinum Manager Application Shared Objects class implementation file.
*/
#include "StdAfx.h"
#include "PlatinumClient_SharedObjects.h"
#include "PPS_Component_DspElement.h"

using namespace Platinum;
using namespace Platinum::client;
using namespace Platinum::client::manager;

extern Platinum::client::UI::common::CDisplayingElement& Global_GetDisplayElement(void);
////////////////////////////////////////////////////////////////////////////

CSharedObjects::CSharedObjects(TMgrSettings& sets_ref) : TBase(sets_ref), m_mgr_settings(sets_ref), m_connector(*this)
{
	if (S_OK != TBase::m_hResult)
	{
		Global_GetDisplayElement().Display(m_proc_init.GetObject_Ref().GetLastError_Ref().GetDescription());
		Global_GetDisplayElement().SetErrorState();
	}
}

CSharedObjects::~CSharedObjects(void)
{
}

////////////////////////////////////////////////////////////////////////////

HRESULT       CSharedObjects::DeviceNotification_OnChange(const eDeviceNotification::_e eType, const _variant_t&)
{
	switch (eType)
	{
	case eDeviceNotification::eArrival:
		{
			if (!this->m_proc_init.GetObject_Ref().IsInitialized())
			{
				HRESULT hr_  = this->m_proc_init.GetObject_Ref().Initialize();
				if (S_OK == hr_)
					Global_GetDisplayElement().SetSuspendState();
				else
					Global_GetDisplayElement().SetErrorState();
			}
		} break;
	case eDeviceNotification::eRemoval:
		{
		} break;
	}
	return S_OK;
}

////////////////////////////////////////////////////////////////////////////

const
TMgrSettings& CSharedObjects::MgrSettings(void)const
{
	return m_mgr_settings;
}

TMgrSettings& CSharedObjects::MgrSettings(void)
{
	return m_mgr_settings;
}