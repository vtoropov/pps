#ifndef _PLATINUMMANNAGERCOMMONSETTINGS_H_73140164_F3A9_4b23_8780_1521BB126F97_INCLUDED
#define _PLATINUMMANNAGERCOMMONSETTINGS_H_73140164_F3A9_4b23_8780_1521BB126F97_INCLUDED
/*
	Created by Tech_dog (VToropov) on 5-Jun-2015 at 5:20:30am, GMT+8, Phuket, Rawai, Friday;
	This is Platinum Manager Application Common Settings class declaration file.
*/
#include "PPS_CommonSettings.h"

namespace Platinum { namespace client { namespace manager
{
	class CCommonSettings :
		public Platinum::client::common::CCommonSettings
	{
	private:
		bool           m_bAutoRefresh;
		bool           m_bCheckDuplicate;
	public:
		CCommonSettings(void);
		~CCommonSettings(void);
	public:
		bool           AutoRefreshClocking(void) const;
		HRESULT        AutoRefreshClocking(const bool);
		bool           CheckDuplicateImages(void)const;
		HRESULT        CheckDuplicateImages(const bool);
	public:
		HRESULT        Load(void);
		HRESULT        Save(void);
	};
}}}

typedef Platinum::client::manager::CCommonSettings TMgrSettings;

#endif/*_PLATINUMMANNAGERCOMMONSETTINGS_H_73140164_F3A9_4b23_8780_1521BB126F97_INCLUDED*/