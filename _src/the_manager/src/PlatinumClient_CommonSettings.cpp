/*
	Created by Tech_dog (VToropov) on 5-Jun-2015 at 1:33:03pm, GMT+8, Phuket, Rawai, Friday;
	This is Platinum Manager Application Common Settings class implementation file.
*/
#include "StdAfx.h"
#include "PlatinumClient_CommonSettings.h"
#include "Shared_PersistentStorage.h"

using namespace Platinum;
using namespace Platinum::client;
using namespace Platinum::client::manager;

using namespace Platinum;
using namespace shared::lite;
using namespace shared::lite::persistent;

////////////////////////////////////////////////////////////////////////////

namespace Platinum { namespace client { namespace manager { namespace details
{
	LPCTSTR CommonSettings_GetFolderName(VOID)
	{
		static LPCTSTR  pFolder = _T("Properties\\CommonSettings");
		return pFolder;
	}

	static LPCTSTR  CommonSettings_CheckDuplicates        = _T("CheckDuplicates");
	static LPCTSTR  CommonSettings_AutoRefreshClocking    = _T("AutoRefreshClocking");
}}}}

////////////////////////////////////////////////////////////////////////////

CCommonSettings::CCommonSettings(void) : m_bAutoRefresh(false), m_bCheckDuplicate(false)
{
	this->Load();
}

CCommonSettings::~CCommonSettings(void)
{
	this->Save();
}

////////////////////////////////////////////////////////////////////////////

bool           CCommonSettings::AutoRefreshClocking(void) const
{
	return m_bAutoRefresh;
}

HRESULT        CCommonSettings::AutoRefreshClocking(const bool bSet)
{
	m_bAutoRefresh = bSet;
	return S_OK;
}

bool           CCommonSettings::CheckDuplicateImages(void)const
{
	return m_bCheckDuplicate;
}

HRESULT        CCommonSettings::CheckDuplicateImages(const bool bSet)
{
	m_bCheckDuplicate = bSet;
	return S_OK;
}

////////////////////////////////////////////////////////////////////////////

HRESULT        CCommonSettings::Load(void)
{
	CRegistryStorage pers_(HKEY_CURRENT_USER);
	::ATL::CAtlString csFolder = details::CommonSettings_GetFolderName();

	HRESULT hr_ = S_OK;
	{
		LONG nValue = 0;
		hr_ = pers_.Load(csFolder.GetString(), details::CommonSettings_AutoRefreshClocking, nValue);
		if (S_OK == hr_)
			this->AutoRefreshClocking(0 != nValue);
	}
	{
		LONG nValue = 0;
		hr_ = pers_.Load(csFolder.GetString(), details::CommonSettings_CheckDuplicates, nValue);
		if (S_OK == hr_)
			this->CheckDuplicateImages(0 != nValue);
	}
	return  hr_;
}

HRESULT        CCommonSettings::Save(void)
{
	CRegistryStorage pers_(HKEY_CURRENT_USER);
	::ATL::CAtlString csFolder = details::CommonSettings_GetFolderName();

	HRESULT hr_ = S_OK;
	{
		LONG nValue = (this->AutoRefreshClocking() ? 1 : 0);
		hr_ = pers_.Save(csFolder.GetString(), details::CommonSettings_AutoRefreshClocking, nValue);
		if (S_OK == hr_)
			this->AutoRefreshClocking(0 != nValue);
	}
	{
		LONG nValue = (this->CheckDuplicateImages() ? 1 : 0);
		hr_ = pers_.Save(csFolder.GetString(), details::CommonSettings_CheckDuplicates, nValue);
		if (S_OK == hr_)
			this->CheckDuplicateImages(0 != nValue);
	}
	return  hr_;
}