/*
	Created by Tech_dog (VToropov) on 23-Mar-2014 at 7:18:32am, GMT+4, Saint-Petersburg Region,
	Rail Road Train #43, Coatch #6, Place #1, Sunday;
	This is Platinum Payroll Systems Client Main Frame class implementation file.
*/
#include "StdAfx.h"
#include "PlatinumClient_MainFrame.h"
#include "PlatinumClient_Resource.h"
#include "Shared_GenericAppObject.h"

using namespace Platinum;
using namespace Platinum::client;
using namespace Platinum::client::UI;
using namespace shared::lite::common;

extern CApplication&       Global_GetAppObjectRef(void); 
////////////////////////////////////////////////////////////////////////////

namespace global
{
	using Platinum::client::UI::common::CDisplayingElement;
	using Platinum::client::UI::common::CDisplayingElementNone;

	static CDisplayingElement*  DisplayElement_Volatile = NULL; // TODO: to make thread-safe access
	static CDisplayingElement&  DisplayElement_Object(void)
	{
		static CDisplayingElementNone none_;
		return (DisplayElement_Volatile == NULL ? none_ : *DisplayElement_Volatile);
	}
}

namespace Platinum { namespace client { namespace UI { namespace details
{
	class CMainFrame_Layout
	{
	private:
		CWindow&    m_main_frm_ref;
		RECT        m_client_area;
	public:
		CMainFrame_Layout(CWindow& main_frm_ref) : m_main_frm_ref(main_frm_ref)
		{
			if (m_main_frm_ref)
				m_main_frm_ref.GetClientRect(&m_client_area);
			else
				::SetRectEmpty(&m_client_area);
		}
	public:
		VOID                 RecalcPosition(void)
		{
			RECT rcWindow = {0};
			m_main_frm_ref.GetWindowRect(&rcWindow);
			const RECT rcScreen = CMainFrame_Layout::GetAvailableArea();
			const SIZE sz_delta = {(__W(rcWindow) - __W(rcScreen)), (__H(rcWindow) - __H(rcScreen))};
			if (0 < sz_delta.cx)
				rcWindow.right -= sz_delta.cx;
			if (0 < sz_delta.cy)
				rcWindow.bottom -= sz_delta.cy;
			if (0 < sz_delta.cx ||
				0 < sz_delta.cy)
			{
				m_main_frm_ref.SetWindowPos(NULL, &rcWindow, SWP_NOZORDER|SWP_NOACTIVATE|SWP_NOMOVE);
				m_main_frm_ref.GetClientRect(&m_client_area);
			}
			m_main_frm_ref.CenterWindow();
		}
	private:
		static RECT          GetAvailableArea(void)
		{
			const POINT ptZero = {0};
			const HMONITOR hMonitor = ::MonitorFromPoint(ptZero, MONITOR_DEFAULTTOPRIMARY);
			MONITORINFO mInfo  = {0};
			mInfo.cbSize = sizeof(MONITORINFO);
			::GetMonitorInfo(hMonitor, &mInfo);
			return mInfo.rcWork;
		}
	};
}}}}

Platinum::client::UI::common::CDisplayingElement& Global_GetDisplayElement(void)
{
	return global::DisplayElement_Object();
}
////////////////////////////////////////////////////////////////////////////

CMainFrame::CMainDialogImpl::CMainDialogImpl(CSharedObjects& obj_ref): 
	IDD(IDD_PLATINUM_MAIN_DLG),
	m_header(IDR_PLATINUM_MAIN_DLG_HEADER),
	m_dsp_el(m_status),
	m_tabset(obj_ref),
	m_objects(obj_ref)
{
}

CMainFrame::CMainDialogImpl::~CMainDialogImpl(void)
{
}

////////////////////////////////////////////////////////////////////////////

LRESULT CMainFrame::CMainDialogImpl::OnClose     (WORD wNotifyCode, WORD wID, HWND hWndCtl, BOOL& bHandled)
{
	wNotifyCode; wID; hWndCtl; bHandled;
	TBaseDlg::EndDialog(IDCANCEL);
	return 0;
}

LRESULT CMainFrame::CMainDialogImpl::OnDestroy   (UINT uMsg, WPARAM wParam, LPARAM lParam,  BOOL& bHandled)
{
	uMsg; wParam; lParam; bHandled = FALSE;
	global::DisplayElement_Volatile = NULL;
	m_tabset.Destroy();
	m_status.Destroy();
	m_header.Destroy();
	return 0;
}

LRESULT CMainFrame::CMainDialogImpl::OnEraseBknd (UINT uMsg, WPARAM wParam, LPARAM lParam,  BOOL& bHandled)
{
	uMsg; wParam; lParam; bHandled = FALSE;
	return 0;
}

LRESULT CMainFrame::CMainDialogImpl::OnInitDialog(UINT uMsg, WPARAM wParam, LPARAM lParam,  BOOL& bHandled)
{
	uMsg; wParam; lParam; bHandled;
	details::CMainFrame_Layout layout(*this);
	layout.RecalcPosition();
	{
		shared::lite::common::CApplicationIconLoader loader(IDR_PLATINUM_MAIN_DLG_ICON);
		TBaseDlg::SetIcon(loader.DetachBigIcon(), TRUE);
		TBaseDlg::SetIcon(loader.DetachSmallIcon(), FALSE);
	}
	HRESULT hr_ = S_OK;
	{
		hr_ = m_status.Create(TBaseDlg::m_hWnd);
		if (S_OK == hr_)
			global::DisplayElement_Volatile = &m_dsp_el;
	}
	{
		hr_ = m_header.Create(TBaseDlg::m_hWnd);
	}
	RECT rcTab = {0};
	if (TBaseDlg::GetClientRect(&rcTab))
	{
		rcTab.top    += m_header.GetSize().cy;
		rcTab.bottom -= m_status.GetSize().cy;
		hr_ = m_tabset.Create(TBaseDlg::m_hWnd, rcTab);
	}
	if (!m_objects.IsInitialized())
	{
		Global_GetDisplayElement().Display(m_objects.Initializer().GetLastError_Ref().GetDescription());
		Global_GetDisplayElement().SetErrorState();
	}
	CApplication& the_app = Global_GetAppObjectRef();
	{
		::ATL::CAtlString cs_title;
		cs_title.Format(
				_T("%s %s"),
				the_app.Version().ProductName(),
				the_app.Version().ProductVersion()
			);
		TBaseDlg::SetWindowText(cs_title);
	}
	return 0;
}

LRESULT CMainFrame::CMainDialogImpl::OnOwnerDraw (UINT uMsg, WPARAM wParam, LPARAM lParam,  BOOL& bHandled)
{
	uMsg; wParam; lParam; bHandled = FALSE;
	if (lParam)
	{
		LPDRAWITEMSTRUCT lpDrawItem = (LPDRAWITEMSTRUCT)lParam;
		if (lpDrawItem)
		{
			m_status.Update(*lpDrawItem);
		}
	}
	return 0;
}

LRESULT CMainFrame::CMainDialogImpl::OnSysCommand(UINT uMsg, WPARAM wParam, LPARAM lParam,  BOOL& bHandled)
{
	uMsg; wParam; lParam; bHandled = FALSE;
	switch (wParam)
	{
	case SC_CLOSE:
		{
			EndDialog(IDCANCEL);
		} break;
	}
	return 0;
}

LRESULT CMainFrame::CMainDialogImpl::OnTabNotify (INT idCtrl, LPNMHDR pnmh, BOOL& bHandled)
{
	idCtrl; pnmh; bHandled = TRUE;
	m_tabset.UpdateLayout();
	return 0;
}

////////////////////////////////////////////////////////////////////////////

CMainFrame::CMainFrame(CSharedObjects& obj_ref) : m_dlg(obj_ref)
{
}

CMainFrame::~CMainFrame(void)
{
}

////////////////////////////////////////////////////////////////////////////

HRESULT    CMainFrame::DoModal(void)
{
	INT_PTR result = m_dlg.DoModal();
	return (result == IDCANCEL ? S_FALSE : S_OK);
}