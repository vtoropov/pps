/*
	Created by Tech_dog (VToropov) on 23-Mar-2014 at 6:00:47pm, GMT+4, Saint-Petersburg, Sunday;
	This is Platinum Payroll Systems Client UI Component Status Bar control wrapper class implementation file.
*/
#include "StdAfx.h"
#include "PlatinumClient_Resource.h"
#include "PPS_Component_StatusBarEx.h"

using namespace Platinum::client::ControlFlow;
using namespace Platinum::client::UI;
using namespace Platinum::client::UI::components;

#include "Shared_SystemError.h"

using namespace shared::lite::common;

/////////////////////////////////////////////////////////////////////////////

namespace Platinum { namespace client { namespace UI { namespace components { namespace details
{
	struct StatusBar_ColorInfo
	{
		COLORREF   clr0;
		COLORREF   clr1;
		COLORREF   fore;
		StatusBar_ColorInfo(void) : clr0(CLR_NONE), clr1(CLR_NONE), fore(::GetSysColor(COLOR_WINDOWTEXT)) {}
	};

	class StatusBar_DrawingPart
	{
	public:
		enum _enum {
			eEntire = 0,
			eTop    = 1,
		};
	};

	using Platinum::client::ControlFlow::eControlFlowState;

	HRESULT StatusBar_GetColorInfoFromState(const eControlFlowState::_enum eState, StatusBar_ColorInfo& clr_info, const StatusBar_DrawingPart::_enum ePart)
	{
		switch (eState)
		{
		case eControlFlowState::eError:
			{
				if (StatusBar_DrawingPart::eEntire == ePart)
				{
					clr_info.clr0 = RGB(200,  28,  23);
					clr_info.clr1 = RGB(233,  97,  69);
				}
				else
				{
					clr_info.clr0 = RGB(246, 183, 192);
					clr_info.clr1 = RGB(221,  61,  80);
				}
				clr_info.fore = RGB(0xff, 0xff, 0xff);
			} break;
		case eControlFlowState::eSuspend:
			{
				if (StatusBar_DrawingPart::eEntire == ePart)
				{
					clr_info.clr0 = RGB(210, 200,   4);
					clr_info.clr1 = RGB(243, 231,   4);
				}
				else
				{
					clr_info.clr0 = RGB(251, 250, 193);
					clr_info.clr1 = RGB(223, 211,  90);
				}
				clr_info.fore = COLORREF(0);
			} break;
		case eControlFlowState::eWaiting:
			{
				if (StatusBar_DrawingPart::eEntire == ePart)
				{
					clr_info.clr0 = RGB(  0,  54, 116);
					clr_info.clr1 = RGB(  3, 104, 179);
				}
				else
				{
					clr_info.clr0 = RGB( 83, 200, 248);
					clr_info.clr1 = RGB(  4,  80, 155);
				}
				clr_info.fore = RGB(0xff, 0xff, 0xff);
			} break;
		case eControlFlowState::eWorking:
			{
				if (StatusBar_DrawingPart::eEntire == ePart)
				{
					clr_info.clr0 = RGB( 35, 101,  41);
					clr_info.clr1 = RGB( 96, 173,  93);
				}
				else
				{
					clr_info.clr0 = RGB(207, 223, 210);
					clr_info.clr1 = RGB( 67, 132,  84);
				}
				clr_info.fore = RGB(0xff, 0xff, 0xff);
			} break;
		default:
			ATLASSERT(0);
			return DISP_E_TYPEMISMATCH;
		}
		return S_OK;
	}

	static CAtlString StatusBar_GetTextFromState(const eControlFlowState::_enum eState)
	{
		::ATL::CAtlString cs_text(_T("Undefined"));
		switch(eState)
		{
		case eControlFlowState::eError:    cs_text = _T("Error");     break;
		case eControlFlowState::eSuspend:  cs_text = _T("Suspended"); break;
		case eControlFlowState::eWaiting:  cs_text = _T("Waiting");   break;
		case eControlFlowState::eWorking:  cs_text = _T("Working");   break;
		}
		return cs_text;
	}
}}}}}

/////////////////////////////////////////////////////////////////////////////

CStatusBar::CStatusBar(void) : m_state(eControlFlowState::eSuspend)
{
	m_size.cx = m_size.cy = 0;
}

CStatusBar::~CStatusBar(void)
{
}

/////////////////////////////////////////////////////////////////////////////

HRESULT         CStatusBar::Create(const HWND hParent)
{
	if (m_control.IsWindow())
		return S_OK;
	if (!::IsWindow(hParent))
		return E_INVALIDARG;
	RECT rcClient = {0};
	if (!::GetClientRect(hParent, &rcClient))
		return E_UNEXPECTED;

	// creates a status bar
	m_control.Create(
			hParent ,
			rcClient,
			0,
			WS_CHILD|WS_VISIBLE|WS_CLIPCHILDREN|WS_CLIPSIBLINGS|SBT_TOOLTIPS,
			0,
			IDC_PLATINUM_MAIN_DLG_STATUS
		);
	if (!m_control)
		return (HRESULT_FROM_WIN32(::GetLastError()));

	RECT rcStatus = {0};
	m_control.GetWindowRect(&rcStatus);
	::OffsetRect(&rcStatus, -rcStatus.left, -rcStatus.top);

	m_size.cx = rcStatus.right;
	m_size.cy = rcStatus.bottom;

	ATL::CSimpleArray<int> arrWidth;
	arrWidth.Add(m_size.cx - 270);
	arrWidth.Add(m_size.cx - 140);
	arrWidth.Add(m_size.cx -  25);
	arrWidth.Add(m_size.cx -   5);

	m_control.SetParts(arrWidth.GetSize(), arrWidth.GetData());
	m_control.SetText (CStatusBarPane::eIndicator, _T(""), SBT_OWNERDRAW);
	m_control.SetText (CStatusBarPane::eQueue,     _T(""));
	m_control.SetText (CStatusBarPane::eMessage,   _T("Ready"));

	HFONT hFont = CWindow(hParent).GetFont();
	m_control.SetFont(hFont);

	return S_OK;
}

HRESULT         CStatusBar::Destroy(void)
{
	if (!m_control.IsWindow())
		return S_FALSE;
	m_control.SendMessage(WM_CLOSE);
	return S_OK;
}

const SIZE&     CStatusBar::GetSize(void) const
{
	return m_size;
}

bool            CStatusBar::IsValid(void) const
{
	return !!m_control.IsWindow();
}

HRESULT         CStatusBar::SetText(const INT nPanel, LPCTSTR lpText)
{
	if (CStatusBarPane::eIndicator == nPanel) // this text is dependent on current control state only!
		return DISP_E_BADINDEX;
	if (CStatusBarPane::eMessage   == nPanel)
		if (lpText)
			m_control.SetTipText(nPanel, lpText);
	return (m_control.SetText(nPanel, lpText) ? S_OK : E_UNEXPECTED);
}

HRESULT         CStatusBar::SetState(const eControlFlowState::_enum eState)
{
	const bool bChanged = (eState != m_state);
	if (bChanged)
	{
		m_state = eState;
		m_control.RedrawWindow(NULL, NULL, RDW_ERASE|RDW_INVALIDATE|RDW_ERASENOW);
	}
	return (bChanged ? S_OK : S_FALSE);
}

LRESULT         CStatusBar::Update (const DRAWITEMSTRUCT& cData)
{
	if (cData.itemID != CStatusBarPane::eIndicator)
		return 0;
	RECT rect = cData.rcItem;
	if(WTL::CTheme::IsThemingSupported())
	{
		rect.right -= 5;
	}
	if(rect.right < 1)
		return 0;

	ex_ui::draw::CZBuffer z_buffer(cData.hDC, rect);

	RECT rectPart1 = rect;
	RECT rectPart2 = rect;

	rectPart2.bottom = (rect.bottom + rect.top) /2;

	details::StatusBar_ColorInfo clr_schema;

	details::StatusBar_GetColorInfoFromState(m_state, clr_schema, details::StatusBar_DrawingPart::eEntire);

	z_buffer.DrawGragRect(rectPart1, clr_schema.clr0, clr_schema.clr1, true, 255);

	details::StatusBar_GetColorInfoFromState(m_state, clr_schema, details::StatusBar_DrawingPart::eTop);

	z_buffer.DrawGragRect(rectPart2, clr_schema.clr0, clr_schema.clr1, true, 255);

	int iZPoint = z_buffer.SaveDC();

	rect.top += 2;

	z_buffer.SelectFont(m_font.GetHandle());
	z_buffer.SetBkMode(TRANSPARENT);
	z_buffer.SetTextColor(clr_schema.fore);

	::ATL::CAtlString cs_text = details::StatusBar_GetTextFromState(m_state);
	z_buffer.DrawText(cs_text.GetString(), -1, &rect, DT_VCENTER|DT_CENTER);

	z_buffer.RestoreDC(iZPoint);

	return 0;
}

/////////////////////////////////////////////////////////////////////////////

CStatusBarDisplayElement::CStatusBarDisplayElement(CStatusBar& status_ref) : m_status(status_ref)
{
}

CStatusBarDisplayElement::~CStatusBarDisplayElement(void)
{
}

/////////////////////////////////////////////////////////////////////////////

HRESULT     CStatusBarDisplayElement::Destroy(void)
{
	return S_OK;
}

HRESULT     CStatusBarDisplayElement::Display(LPCTSTR pInfo)
{
	m_status.SetText(CStatusBarPane::eMessage, pInfo);
	return S_OK;
}

HRESULT     CStatusBarDisplayElement::Display(const UINT nFormatId, LPCTSTR parg1)
{
	::ATL::CAtlString csBuffer;
	csBuffer.Format(nFormatId, parg1);
	m_status.SetText(CStatusBarPane::eMessage, csBuffer.GetString());
	return S_OK;
}

HRESULT     CStatusBarDisplayElement::Display(const UINT nFormatId, HRESULT parg1)
{
	shared::lite::common::CSysError err_(parg1);

	::ATL::CAtlString csBuffer;
	csBuffer.Format(nFormatId, err_.GetDescription());
	m_status.SetText(CStatusBarPane::eMessage, csBuffer.GetString());
	return S_OK;
}

bool        CStatusBarDisplayElement::IsValid(void) const
{
	return m_status.IsValid();
}

HRESULT     CStatusBarDisplayElement::SetControlState(const DWORD dState)
{
	if (eControlFlowState::eError == dState
		|| eControlFlowState::eSuspend == dState
		|| eControlFlowState::eWaiting == dState
		|| eControlFlowState::eWorking == dState)
	{
		m_status.SetState(eControlFlowState::_enum(dState));
		return S_OK;
	}
	else
		return S_FALSE;
}

HRESULT     CStatusBarDisplayElement::SetCountInfo   (LPCTSTR pPattern, const INT nCount)
{
	::ATL::CAtlString cs_count;
	if (!pPattern || ::_tcslen(pPattern) < 2)
		cs_count.Format(_T("Item(s): %d"), nCount);
	else
		cs_count.Format(pPattern, nCount);
	HRESULT hr_ = m_status.SetText(CStatusBarPane::eQueue, cs_count.GetString());
	return  hr_;
}

HRESULT     CStatusBarDisplayElement::SetCountInfo   (const UINT nPatternId, const INT nCount)
{
	::ATL::CAtlString cs_pattern;
	cs_pattern.LoadString(nPatternId);

	return this->SetCountInfo(cs_pattern.GetString(), nCount);
}

HRESULT     CStatusBarDisplayElement::SetErrorState  (LPCTSTR pDetails)
{
	if (pDetails)
		this->Display(pDetails);
	m_status.SetState(eControlFlowState::eError);
	return S_OK;
}

HRESULT     CStatusBarDisplayElement::SetSuspendState(LPCTSTR pDetails)
{
	if (pDetails)
		this->Display(pDetails);
	m_status.SetState(eControlFlowState::eSuspend);
	return S_OK;
}

HRESULT     CStatusBarDisplayElement::SetWaitingState(LPCTSTR pDetails)
{
	if (pDetails)
		this->Display(pDetails);
	m_status.SetState(eControlFlowState::eWaiting);
	return S_OK;
}

HRESULT     CStatusBarDisplayElement::SetWorkingState(LPCTSTR pDetails)
{
	if (pDetails)
		this->Display(pDetails);
	m_status.SetState(eControlFlowState::eWorking);
	return S_OK;
}