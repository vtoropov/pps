/*
	Created by Tech_dog (VToropov) on 24-Mar-2014 at 10:47:01am, GMT+4, Saint-Petersburg, Monday;
	This is Platinum Client UI Component Enrollment Tab Page class implementation file.
*/
#include "StdAfx.h"
#include "PlatinumClient_Resource.h"
#include "PPS_Component_TabPageEnroll.h"
#include "PPS_EmployeeRecordDlg.h"
#include "PPS_Component_DspElement.h"

using namespace Platinum::client::data;
using namespace Platinum::client::UI;
using namespace Platinum::client::UI::common;
using namespace Platinum::client::UI::components;
using namespace Platinum::client::UI::dialogs;

#include "Shared_GenericAppObject.h"
#include "Shared_SystemError.h"

using namespace shared::lite::common;

extern CApplication&       Global_GetAppObjectRef(void);
extern CDisplayingElement& Global_GetDisplayElement(void);
/////////////////////////////////////////////////////////////////////////////

namespace Platinum { namespace client { namespace UI { namespace components { namespace details
{	
	class TabPage_EnrollColumnSpec
	{
	public:
		enum _enum {
			eCode     = 0,
			eEmployee = 1,
			eWorkArea = 2,
			eVeinData = 3,
		};
	private:
		RawListViewColumnSpec      m_cols;
		RawListViewColumnSpecItem  m_empty;
	public:
		TabPage_EnrollColumnSpec(void)
		{
			m_cols.push_back(RawListViewColumnSpecItem(_T("Employee Code")     , 100,  LVCFMT_LEFT  ));
			m_cols.push_back(RawListViewColumnSpecItem(_T("Employee Name")     , 300,  LVCFMT_LEFT  ));
			m_cols.push_back(RawListViewColumnSpecItem(_T("Default Work Area") , 230,  LVCFMT_LEFT  ));
			m_cols.push_back(RawListViewColumnSpecItem(_T("Vein Scan")         ,  70,  LVCFMT_CENTER));
		}
		~TabPage_EnrollColumnSpec(void){}
	public:
		INT                              Count(void)const { return (INT)m_cols.size(); }
		const RawListViewColumnSpecItem& Column(const INT nIndex)const
		{
			if (0 > nIndex || nIndex > this->Count() - 1)
				return this->m_empty;
			else
				return m_cols[nIndex];
		}
		const RawListViewColumnSpec&     Columns(void)const
		{
			return m_cols;
		}
	};

	class TabPage_EnrollLayout
	{
		enum _enum {
			list_top_margin = 50,
			btn_horz_margin = 13,
			btn_horz_gap    =  5,
		};
	private:
		::ATL::CWindow&    m_page_ref;
		RECT               m_area;
	public:
		TabPage_EnrollLayout(::ATL::CWindow& page_ref) : m_page_ref(page_ref)
		{
			m_page_ref.GetClientRect(&m_area);
		}
	public:
		VOID    AppendRow(const CEmployeeDataRecord& rec_ref)
		{
			::ATL::CWindow list = m_page_ref.GetDlgItem(IDC_ENROLL_TAB_LIST);
			CListViewWrap  wrapper(list);
			INT nRow = -1; wrapper.AddRow(nRow, true);
			if (-1 != nRow)
			{
				wrapper.Cell(nRow, TabPage_EnrollColumnSpec::eCode,     rec_ref.Code());
				wrapper.Cell(nRow, TabPage_EnrollColumnSpec::eEmployee, rec_ref.Name());
				wrapper.Cell(nRow, TabPage_EnrollColumnSpec::eWorkArea, rec_ref.WorkArea());
				
				CEmployeeDataRecord_ValidateRule validator(rec_ref);
				switch (validator.ValidateVein())
				{
				case S_OK:    wrapper.Cell(nRow, TabPage_EnrollColumnSpec::eVeinData, _T("Valid")); break;
				case S_FALSE: wrapper.Cell(nRow, TabPage_EnrollColumnSpec::eVeinData, _T("#n/a"));  break;
				default:
				              wrapper.Cell(nRow, TabPage_EnrollColumnSpec::eVeinData, _T("Invalid"));
				}
				::ATL::CWindow btn_edit = m_page_ref.GetDlgItem(IDC_ENROLL_TAB_EDIT);
				::ATL::CWindow btn_delete = m_page_ref.GetDlgItem(IDC_ENROLL_TAB_DELETE);
				btn_edit.EnableWindow(TRUE);
				btn_delete.EnableWindow(TRUE);
			}
		}
		HRESULT InitializeList(void)
		{
			TabPage_EnrollColumnSpec cols_spec;
			::ATL::CWindow list = m_page_ref.GetDlgItem(IDC_ENROLL_TAB_LIST);
			CListViewWrap  wrapper(list);
			wrapper.Configure();
			HRESULT hr_ =  wrapper.AddColumns(cols_spec.Columns());
			return  hr_;
		}
		VOID    RemoveCurrentRow()
		{
			::ATL::CWindow list = m_page_ref.GetDlgItem(IDC_ENROLL_TAB_LIST);
			CListViewWrap wrapper(list);
			wrapper.RemoveCurrent();
			{
				const INT nRows = wrapper.RowsCount();
				::ATL::CWindow btn_edit = m_page_ref.GetDlgItem(IDC_ENROLL_TAB_EDIT);
				::ATL::CWindow btn_delete = m_page_ref.GetDlgItem(IDC_ENROLL_TAB_DELETE);
				btn_edit.EnableWindow(0 < nRows);
				btn_delete.EnableWindow(0 < nRows);
			}
		}
		VOID    UpdateCurrentRow(const CEmployeeDataRecord& rec_ref)
		{
			::ATL::CWindow list = m_page_ref.GetDlgItem(IDC_ENROLL_TAB_LIST);
			CListViewWrap wrapper(list);
			const INT nRow = wrapper.SelectedRow();
			if (-1 != nRow)
			{
				wrapper.Cell(nRow, TabPage_EnrollColumnSpec::eCode,     rec_ref.Code());
				wrapper.Cell(nRow, TabPage_EnrollColumnSpec::eEmployee, rec_ref.Name());
				wrapper.Cell(nRow, TabPage_EnrollColumnSpec::eWorkArea, rec_ref.WorkArea());
				
				CEmployeeDataRecord_ValidateRule validator(rec_ref);
				switch (validator.ValidateVein())
				{
				case S_OK:    wrapper.Cell(nRow, TabPage_EnrollColumnSpec::eVeinData, _T("Valid")); break;
				case S_FALSE: wrapper.Cell(nRow, TabPage_EnrollColumnSpec::eVeinData, _T("#n/a"));  break;
				default:
				              wrapper.Cell(nRow, TabPage_EnrollColumnSpec::eVeinData, _T("Invalid"));
				}
			}
		}
		HRESULT UpdateList(CSharedObjects& obj_ref)
		{
			::ATL::CWindow list = m_page_ref.GetDlgItem(IDC_ENROLL_TAB_LIST);
			if (!list)
				return OLE_E_INVALIDHWND;
			CListViewWrap wrapper(list);
			const INT nRowCount = obj_ref.Cache().Count();
			if (nRowCount < 1)
				return S_OK;
			for (INT i_ = 0; i_ < nRowCount; i_++)
			{
				INT nRow = -1;
				HRESULT hr_ = wrapper.AddRow(nRow, false);
				if (S_OK != hr_)
					break;
				const CEmployeeDataRecord& record = obj_ref.Cache().Item(i_);
				if (!record.IsValid())
					continue;
				wrapper.Cell(nRow, TabPage_EnrollColumnSpec::eCode,     record.Code());
				wrapper.Cell(nRow, TabPage_EnrollColumnSpec::eEmployee, record.Name());
				wrapper.Cell(nRow, TabPage_EnrollColumnSpec::eWorkArea, record.WorkArea());
				{
					CEmployeeDataRecord_ValidateRule validator(record);
					const HRESULT  hr_ = validator.ValidateVein();
					if (S_FALSE == hr_)
						wrapper.Cell(nRow, TabPage_EnrollColumnSpec::eVeinData, _T("#n/a"));
					else if (S_OK == hr_)
						wrapper.Cell(nRow, TabPage_EnrollColumnSpec::eVeinData, _T("Valid"));
					else
						wrapper.Cell(nRow, TabPage_EnrollColumnSpec::eVeinData, _T("Not Valid"));
				}
			}
			return S_OK;
		}
		VOID    UpdateLayout(VOID)
		{
			::ATL::CWindow list = m_page_ref.GetDlgItem(IDC_ENROLL_TAB_LIST);
			if (list)
			{
				RECT rcList   = m_area;
				rcList.top   += TabPage_EnrollLayout::list_top_margin;
				rcList.right -= 2;
				list.SetWindowPos(NULL, &rcList, SWP_NOACTIVATE|SWP_NOZORDER);
			}
			INT nLeft = m_area.right - TabPage_EnrollLayout::btn_horz_margin;
			const INT ctrl_ids[] = {
					IDC_ENROLL_TAB_DELETE,
					IDC_ENROLL_TAB_EDIT,
					IDC_ENROLL_TAB_NEW
				};
			for (INT i_ = 0; i_ < _countof(ctrl_ids); i_++)
			{
				::ATL::CWindow btn_ctrl = m_page_ref.GetDlgItem(ctrl_ids[i_]);
				if (btn_ctrl)
				{
					RECT rc_btn = {0};
					btn_ctrl.GetWindowRect(&rc_btn); ::MapWindowPoints(HWND_DESKTOP, m_page_ref, reinterpret_cast<LPPOINT>(&rc_btn), 0x2);

					const INT n_width = __W(rc_btn);
					nLeft -=  n_width;

					rc_btn.left = nLeft;
					rc_btn.right = rc_btn.left + n_width;

					btn_ctrl.SetWindowPos(NULL, &rc_btn, SWP_NOZORDER|SWP_NOACTIVATE|SWP_NOSIZE);
					nLeft -= TabPage_EnrollLayout::btn_horz_gap;
				}
			}
		}
	};	

	static HRESULT    TabPage_EnrollHelper_AddNew(CSharedObjects& obj_ref, CEnrollDataProvider& provider, CEmployeeDataRecord& record)
	{
		CEmployeeRecordDlg  dlg(obj_ref);
		HRESULT hr_ = dlg.DoModal(record, true);
		if (S_OK != hr_)
			return  hr_;
		hr_ = provider.Append(record);
		if (HRESULT_FROM_WIN32(ERROR_OBJECT_ALREADY_EXISTS) == hr_)
		{
			::ATL::CAtlString cs_msg;
			cs_msg.Format(_T("The record with employee code [%s] already exists."), record.Code());
			AtlMessageBox(::GetActiveWindow(), cs_msg.GetString(), Global_GetAppObjectRef().GetName(), MB_ICONEXCLAMATION|MB_OK);
		}
		else if (S_OK == hr_) hr_ = provider.Save();
		return  hr_;
	}

	static CAtlString TabPage_EnrollHelper_GetSelected(::ATL::CWindow& list)
	{
		CListViewWrap wrapper(list);
		::ATL::CAtlString cs_code = wrapper.ActiveCell(0);
		if (cs_code.IsEmpty())
			AtlMessageBox(
				::GetActiveWindow(),
				_T("No employee record is selected."),
				Global_GetAppObjectRef().GetName(),
				MB_ICONEXCLAMATION|MB_OK
			);
		return cs_code;
	}

	static HRESULT    TabPage_EnrollHelper_Remove(CSharedObjects& obj_ref, CEnrollDataProvider& provider, const ::ATL::CAtlString& cs_code)
	{
		obj_ref;
		HRESULT hr_ = provider.Remove(cs_code);
		if (S_OK == hr_)
		{
			hr_ = provider.Save();
		}
		return hr_;
	}

	static VOID       TabPage_EnrollHelper_SaveError(const HRESULT hr_)
	{
		CSysError sys_err(hr_);
		if (sys_err.HasDetails() == false)
			sys_err.SetUnknownMessage();
		::ATL::CAtlString cs_error;
		cs_error.Format(_T("Error occurred while saving data: code=0x%x; disc=%s"), hr_, sys_err.GetDescription());
		Global_GetDisplayElement().SetErrorState(cs_error.GetString());
		TRACE_FUNC();
		TRACE_ERROR(cs_error.GetString());
	}

	static HRESULT    TabPage_EnrollHelper_Update(CSharedObjects& obj_ref, CEnrollDataProvider& provider, const ::ATL::CAtlString& cs_code, CEmployeeDataRecord& updated)
	{
		CEmployeeDataRecord record = provider.Record(cs_code);
		if (!record.IsValid())
		{
			::ATL::CAtlString cs_msg;
			cs_msg.Format(_T("The record with employee code [%s] is not found."), record.Code());
			AtlMessageBox(::GetActiveWindow(), cs_msg.GetString(), Global_GetAppObjectRef().GetName(), MB_ICONEXCLAMATION|MB_OK);
			return TYPE_E_ELEMENTNOTFOUND;
		}
		CEmployeeRecordDlg  dlg(obj_ref);
		HRESULT hr_ = dlg.DoModal(record, false);
		if (S_OK != hr_)
			return  hr_;
		hr_ = provider.Update(record);
		if (S_OK == hr_)
		{
			updated = record;
			hr_ = provider.Save();
		}
		return  hr_;
	}

	static VOID       TabPage_EnrollHelper_OnNotifyEvent(::ATL::CWindow& page_, WPARAM wParam, LPARAM lParam, BOOL& bHandled)
	{
		if (IDC_ENROLL_TAB_LIST != (INT)wParam)
			return;
		LPNMHDR pNotifyHeader = (LPNMHDR)lParam;
		if (!pNotifyHeader)
			return;
		if (NM_RCLICK != pNotifyHeader->code)
			return;
		::WTL::CMenu ctx_menu  = ::LoadMenu(::GetModuleHandle(NULL), MAKEINTRESOURCE(IDC_ENROLL_TAB_MENU));
		const HMENU sub_menu  = ::GetSubMenu(ctx_menu, 0);
		::WTL::CMenuHandle menu_(sub_menu);
		if (menu_.IsMenu())
		{
			{
				::ATL::CWindow list = page_.GetDlgItem(IDC_ENROLL_TAB_LIST);
				CListViewWrap wrapper(list);
				const INT nRows = wrapper.RowsCount();
				const UINT uFlags = MF_BYCOMMAND | (0 < nRows ? MF_ENABLED : MF_DISABLED|MF_GRAYED);
				menu_.EnableMenuItem(IDC_ENROLL_TAB_EDIT, uFlags);
				menu_.EnableMenuItem(IDC_ENROLL_TAB_DELETE, uFlags);
				menu_.SetMenuDefaultItem(IDC_ENROLL_TAB_EDIT, FALSE);
			}
			POINT pt_ = {0};
			::GetCursorPos(&pt_);
			const WORD wCmdId = (WORD)::TrackPopupMenuEx(menu_, TPM_BOTTOMALIGN|TPM_RETURNCMD|TPM_NONOTIFY, pt_.x, pt_.y, page_.m_hWnd, NULL);
			bHandled = TRUE;
			page_.SendMessage(WM_COMMAND, MAKEWPARAM(wCmdId, 0), 0);
		}
	}

	static VOID       TabPage_EnrollHelper_ShowStatus(CSharedObjects& obj_ref, LPCTSTR pszStatus, const HRESULT hr)
	{
		if (obj_ref.IsInitialized())
		{
			if (S_OK == hr)
				Global_GetDisplayElement().SetSuspendState(pszStatus);
			else if (S_FALSE == hr)
				Global_GetDisplayElement().SetSuspendState(_T("The operation is cancelled by a user"));
			else
				Global_GetDisplayElement().SetErrorState(pszStatus);
		}
		else
			Global_GetDisplayElement().SetErrorState(obj_ref.Initializer().GetLastError_Ref().GetDescription());
	}

	static VOID       TabPage_EnrollHelper_ShowStatus(CSharedObjects& obj_ref, const UINT nStatusId, const HRESULT hr)
	{
		::ATL::CAtlString cs_status;
		cs_status.LoadString(nStatusId);
		TabPage_EnrollHelper_ShowStatus(obj_ref, cs_status.GetString(), hr);
	}

	static VOID       TabPage_EnrollHelper_Reconnect (CSharedObjects& obj_ref)
	{
		/*if (obj_ref.IsInitialized() == false)*/
			obj_ref.Initialize();
	}
}}}}}

/////////////////////////////////////////////////////////////////////////////

CTabPageEnroll::CTabPageEnroll(::WTL::CTabCtrl& ctrl_ref, Platinum::client::manager::CSharedObjects& obj_ref):
	TBasePage(IDD_PLATINUM_MAIN_DLG_ENROLL_TAB, ctrl_ref, *this),
	m_objects(obj_ref),
	m_provider(obj_ref),
	m_sorter(obj_ref, m_list)
{
}

CTabPageEnroll::~CTabPageEnroll(void)
{
}

/////////////////////////////////////////////////////////////////////////////

LRESULT CTabPageEnroll::TabPage__OnEvent(UINT uMsg, WPARAM wParam, LPARAM lParam, BOOL& bHandled)
{
	uMsg; wParam; lParam; bHandled = FALSE;
	switch (uMsg)
	{
	case WM_SHOWWINDOW:
		{
			if (wParam)
			{
				Global_GetDisplayElement().SetCountInfo(IDS_ENROLL_DATA_COUNT, m_provider.RecordCount());
				details::TabPage_EnrollHelper_ShowStatus(this->m_objects, _T("Ready"), S_OK);
			}
		} break;
	case WM_INITDIALOG:
		{
			if (TBasePage::m_bInitilized)
				break;
			TBasePage::m_bInitilized = true;

			CApplicationCursor wc;

			m_list = this->GetDlgItem(IDC_ENROLL_TAB_LIST);

			details::TabPage_EnrollLayout layout(*this);
			layout.UpdateLayout();
			HRESULT hr_ = layout.InitializeList();
			if (S_OK == hr_)
			{	
				hr_ = m_provider.Initialize();
				if (m_provider.RecordCount())
				{
					::ATL::CWindow btn_edit = TBasePage::GetDlgItem(IDC_ENROLL_TAB_EDIT);
					::ATL::CWindow btn_del = TBasePage::GetDlgItem(IDC_ENROLL_TAB_DELETE);
					btn_edit.EnableWindow(TRUE);
					btn_del.EnableWindow(TRUE);
				}
				Global_GetDisplayElement().SetCountInfo(IDS_ENROLL_DATA_COUNT, m_provider.RecordCount());
				if (S_OK != hr_)
				{
					CSysError sys_err(hr_);
					if (sys_err.HasDetails() == false)
						sys_err.SetUnknownMessage();
					ATL::CAtlString cs_error;
					cs_error.Format(_T("Error occurred while opening the data file: code=0x%x, desc=%s"), hr_, sys_err.GetDescription());
					Global_GetDisplayElement().SetErrorState(cs_error.GetString());
					TRACE_FUNC();
					TRACE_ERROR(cs_error.GetString());
				}
				layout.UpdateList(m_objects);
			}
			bHandled = TRUE;
		} break;
	case WM_NOTIFY:
		{
			if (!bHandled) details::TabPage_EnrollHelper_OnNotifyEvent(*this, wParam, lParam, bHandled);
			if (!bHandled)
			{

				const LPNMHDR pNotifyHeader = reinterpret_cast<LPNMHDR>(lParam);
				if (LVN_COLUMNCLICK == pNotifyHeader->code)
				{
					const LPNMLISTVIEW pLVNotify = reinterpret_cast<LPNMLISTVIEW>(lParam);
					const INT nIndex = pLVNotify->iSubItem;
					m_sorter.Sort(nIndex);
					bHandled = true;
				}
				if (NM_DBLCLK == pNotifyHeader->code)
				{
					::ATL::CWindow list = TBasePage::GetDlgItem(IDC_ENROLL_TAB_LIST);
					HRESULT hr_ = S_OK;
					::ATL::CAtlString cs_code = details::TabPage_EnrollHelper_GetSelected(list);
					if (!cs_code.IsEmpty())
					{
						CEmployeeDataRecord updated;
						hr_ = details::TabPage_EnrollHelper_Update(m_objects, m_provider, cs_code, updated);
						if (S_OK == hr_)
						{
							details::TabPage_EnrollLayout layout(*this);
							layout.UpdateCurrentRow(updated);
						}
					}
					if (FAILED(hr_))
						details::TabPage_EnrollHelper_SaveError(hr_);
					else
						details::TabPage_EnrollHelper_ShowStatus(this->m_objects, IDS_ENROLL_DATA_SAVED, hr_);
					bHandled = TRUE;
				}
			}
		} break;
	case WM_COMMAND:
		{
			if (!TBasePage::m_bInitilized)
				break;
			const WORD wNotify = HIWORD(wParam); wNotify;
			const WORD ctrlId  = LOWORD(wParam); ctrlId;
			if (false){}
			else if (IDC_ENROLL_TAB_RECONNECT == ctrlId)
			{
				details::TabPage_EnrollHelper_Reconnect(m_objects);
			}
			else if (IDC_ENROLL_TAB_NEW == ctrlId || IDC_ENROLL_TAB_EDIT == ctrlId)
			{
				const bool bNewData = (IDC_ENROLL_TAB_NEW == ctrlId);
				::ATL::CWindow list = TBasePage::GetDlgItem(IDC_ENROLL_TAB_LIST);
				HRESULT hr_ = S_OK;
				if (bNewData)
				{
					CEmployeeDataRecord new_rec;
					hr_ = details::TabPage_EnrollHelper_AddNew(m_objects, m_provider, new_rec);
					if (S_OK == hr_)
					{
						details::TabPage_EnrollLayout layout(*this);
						layout.AppendRow(new_rec);
						Global_GetDisplayElement().SetCountInfo(IDS_ENROLL_DATA_COUNT, m_provider.RecordCount());
					}
				}
				else
				{
					::ATL::CAtlString cs_code = details::TabPage_EnrollHelper_GetSelected(list);
					if (!cs_code.IsEmpty())
					{
						CEmployeeDataRecord updated;
						hr_ = details::TabPage_EnrollHelper_Update(m_objects, m_provider, cs_code, updated);
						if (S_OK == hr_)
						{
							details::TabPage_EnrollLayout layout(*this);
							layout.UpdateCurrentRow(updated);
						}
					}
				}
				if (FAILED(hr_))
					details::TabPage_EnrollHelper_SaveError(hr_);
				else
					details::TabPage_EnrollHelper_ShowStatus(this->m_objects, IDS_ENROLL_DATA_SAVED, hr_);
				bHandled = TRUE;
			}
			else if (IDC_ENROLL_TAB_DELETE == ctrlId)
			{
				::ATL::CWindow list = TBasePage::GetDlgItem(IDC_ENROLL_TAB_LIST);
				::ATL::CAtlString cs_code = details::TabPage_EnrollHelper_GetSelected(list);
				if (!cs_code.IsEmpty())
				{
					CListViewWrap wrapper(list);
					::ATL::CAtlString cs_msg;
					cs_msg.Format(_T("Do you want to delete [%s] employee from the list?"), wrapper.ActiveCell(1));
					const INT nResult = AtlMessageBox(*this, cs_msg.GetString(), Global_GetAppObjectRef().GetName(), MB_YESNO|MB_ICONQUESTION);
					if (IDYES == nResult)
					{
						HRESULT hr_ = details::TabPage_EnrollHelper_Remove(m_objects, m_provider, cs_code);
						if (S_OK == hr_)
						{
							details::TabPage_EnrollLayout layout(*this);
							layout.RemoveCurrentRow();
						}
						if (FAILED(hr_))
							details::TabPage_EnrollHelper_SaveError(hr_);
						else
							details::TabPage_EnrollHelper_ShowStatus(this->m_objects, IDS_ENROLL_DATA_SAVED, hr_);
						Global_GetDisplayElement().SetCountInfo(IDS_ENROLL_DATA_COUNT, m_provider.RecordCount());
					}
				}
				bHandled = TRUE;
			}
		} break;
	}
	return 0;
}

void    CTabPageEnroll::UpdateLayout(void)
{
}

HRESULT CTabPageEnroll::Validate(void)const
{
	return S_OK;
}