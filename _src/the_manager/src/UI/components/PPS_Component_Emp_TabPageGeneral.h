#ifndef __PLATINUMCLIENTEMPLOYEERECORDGENERALTABPAGE_H_A6FA2CF5_AB7A_4b33_BCD1_F804EFB5B3C7_INCLUDED
#define __PLATINUMCLIENTEMPLOYEERECORDGENERALTABPAGE_H_A6FA2CF5_AB7A_4b33_BCD1_F804EFB5B3C7_INCLUDED
/*
	Created by Tech_dog (VToropov) on 24-Mar-2014 at 6:50:27pm, GMT+4, Saint-Petersburg, Monday;
	This is Platinum Client Employee Record General Tab Page class declaration file.
*/
#include "PPS_Component_TabPage_Base.h"
#include "PPS_EmployeeDataRecord.h"

namespace Platinum { namespace client { namespace UI { namespace components
{
	using Platinum::client::data::CEmployeeDataRecord;
	class CTabPageEmpRecGeneral:
		public CTabPageBase, 
		public ITabPageCallback
	{
		typedef CTabPageBase  TBasePage;
	private:
		ITabSetCallback&      m_tabs_sink;
		CEmployeeDataRecord&  m_record;
	public:
		CTabPageEmpRecGeneral(::WTL::CTabCtrl&, ITabSetCallback& tabs_sink, CEmployeeDataRecord&);
		~CTabPageEmpRecGeneral(void);
	public:
		virtual LRESULT    TabPage__OnEvent(UINT uMsg, WPARAM wParam, LPARAM lParam, BOOL& bHandled) override sealed;
		virtual void       UpdateLayout(void) override sealed;
		virtual HRESULT    Validate(void)const override sealed;
	};
}}}}

#endif/*__PLATINUMCLIENTEMPLOYEERECORDGENERALTABPAGE_H_A6FA2CF5_AB7A_4b33_BCD1_F804EFB5B3C7_INCLUDED*/