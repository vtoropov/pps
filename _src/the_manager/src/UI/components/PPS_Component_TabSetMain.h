#ifndef __PLATINUMCLIENTUICOMPONENTMAINTABSET_H_C1D3252F_0483_400e_8105_1C83788BAD75_INCLUDED
#define __PLATINUMCLIENTUICOMPONENTMAINTABSET_H_C1D3252F_0483_400e_8105_1C83788BAD75_INCLUDED
/*
	Created by Tech_dog (VToropov) on 24-Mar-2014 at 10:57:40am, GMT+4, Saint-Petersburg, Monday;
	This is Platinum Client UI Component Main Form Tab Set class declaration file.
*/
#include "PPS_Component_TabPageEnroll.h"
#include "PPS_Component_TabPageTrack.h"
#include "PlatinumClient_SharedObjects.h"

namespace Platinum { namespace client { namespace UI { namespace components
{
	using Platinum::client::manager::CSharedObjects;
	class CTabSet
	{
	private:
		::WTL::CTabCtrl   m_cTabCtrl;
		CTabPageEnroll    m_tab_enroll;
		CTabPageTrack     m_tab_track;
	public:
		CTabSet(CSharedObjects&);
		~CTabSet(void);
	public:
		HRESULT           Create(const HWND hParent, const RECT& rcArea);
		HRESULT           Destroy(void);
		void              UpdateLayout(void);
	};
}}}}

#endif/*__PLATINUMCLIENTUICOMPONENTMAINTABSET_H_C1D3252F_0483_400e_8105_1C83788BAD75_INCLUDED*/