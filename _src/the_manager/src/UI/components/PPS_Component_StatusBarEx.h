#ifndef __PLATINUMUICOMPONENTSTATUSBAREX_H_05F5D57B_1C47_4e69_8938_6DA5D6E3FB9C_INCLUDED
#define __PLATINUMUICOMPONENTSTATUSBAREX_H_05F5D57B_1C47_4e69_8938_6DA5D6E3FB9C_INCLUDED
/*
	Created by Tech_dog (VToropov) on 23-Mar-2014 at 4:44:46pm, GMT+4, Saint-Petersburg, Sunday;
	This is Platinum Payroll Systems Client UI Component Status Bar control wrapper class declaration file.
*/
#include "PPS_ControlFlowDefs.h"
#include "UIX_GdiProvider.h"
#include "PPS_Component_DspElement.h"

namespace Platinum { namespace client { namespace UI { namespace components
{
	using Platinum::client::ControlFlow::eControlFlowState;
	using ex_ui::draw::common::CFont;

	class CStatusBarPane
	{
	public:
		enum _enum{
			eMessage   = 0,
			eQueue     = 1,
			eIndicator = 2,
			eGap       = 3
		};
	};

	class CStatusBar
	{
	private:
		::WTL::CStatusBarCtrl    m_control;       // status bar control object
		mutable SIZE             m_size;          // status bar control size
		eControlFlowState::_enum m_state;
		CFont                    m_font;
	public:
		CStatusBar(void);
		~CStatusBar(void);
	public:
		HRESULT                  Create(const HWND hParent);
		HRESULT                  Destroy(void);
		const SIZE&              GetSize(void) const;
		bool                     IsValid(void) const;
		HRESULT                  SetText(const INT nPanel, LPCTSTR lpText);
		HRESULT                  SetState(const eControlFlowState::_enum);
		LRESULT                  Update (const DRAWITEMSTRUCT&);
	private:
		CStatusBar(const CStatusBar&);
		CStatusBar& operator= (const CStatusBar&);
	};

	class CStatusBarDisplayElement:
		public Platinum::client::UI::common::CDisplayingElement
	{
	private:
		CStatusBar&         m_status;
	public:
		CStatusBarDisplayElement(CStatusBar&);
		~CStatusBarDisplayElement(void);
	public:
		virtual HRESULT     Destroy(void) override;
		virtual HRESULT     Display(LPCTSTR pInfo) override;
		virtual HRESULT     Display(const UINT nFormatId, LPCTSTR parg1) override;
		virtual HRESULT     Display(const UINT nFormatId, HRESULT parg1) override;
		virtual bool        IsValid(void) const override;
		virtual HRESULT     SetControlState(const DWORD) override;
		virtual HRESULT     SetCountInfo   (LPCTSTR pPattern, const INT nCount) override;
		virtual HRESULT     SetCountInfo   (const UINT nPatternId, const INT nCount) override;
		virtual HRESULT     SetErrorState  (LPCTSTR pDetails=NULL) override;
		virtual HRESULT     SetSuspendState(LPCTSTR pDetails=NULL) override;
		virtual HRESULT     SetWaitingState(LPCTSTR pDetails=NULL) override;
		virtual HRESULT     SetWorkingState(LPCTSTR pDetails=NULL) override;
	private:
		CStatusBarDisplayElement(const CStatusBarDisplayElement&);
		CStatusBarDisplayElement& operator= (const CStatusBarDisplayElement&);
	};
}}}}

#endif/*__PLATINUMUICOMPONENTSTATUSBAREX_H_05F5D57B_1C47_4e69_8938_6DA5D6E3FB9C_INCLUDED*/