/*
	Created by Tech_dog (VToropov) on 27-Mar-2014 at 1:19:25am, GMT+4, Saint-Petersburg, Thirsday;
	This is Platinum Client UI Component Time Tracking Tab Page class implementation file.
*/
#include "StdAfx.h"
#include "PPS_Component_TabPageTrack.h"
#include "PPS_Component_DspElement.h"
#include "PPS_CommonSettings.h"
#include "PPS_ListView_Wrap.h"
#include "PPS_DataProvider_CommonDefs.h"
#include "Shared_GenericAppObject.h"
#include "Shared_SystemError.h"
#include "Shared_PersistentStorage.h"
#include "PlatinumClient_Resource.h"
#include "PlatinumClient_SharedObjects.h"
#include "UIX_CommonDrawDefs.h"

using namespace Platinum;
using namespace Platinum::client;
using namespace Platinum::client::UI;
using namespace Platinum::client::UI::common;
using namespace Platinum::client::UI::components;
using namespace shared::lite::common;
using namespace shared::lite::persistent;
using namespace Platinum::client::data;
using namespace Platinum::client::common;

extern CApplication&       Global_GetAppObjectRef(void);
extern CDisplayingElement& Global_GetDisplayElement(void);
extern CCommonSettings&    Global_GetSettingsRef(void);
////////////////////////////////////////////////////////////////////////////

namespace Platinum { namespace client { namespace UI { namespace components { namespace details
{
	using Platinum::client::common::CSharedObjects;
	using Platinum::client::data::CTimeTrackRecord;
	using Platinum::client::data::CTimeTrackDataSpec;
	using Platinum::client::data::CTimeTrackDataProvider;
	using Platinum::client::data::CEmployeeDataRecord;
	using Platinum::client::data::CEmployeeFvData;
	using Platinum::client::common::CEmployeeRecsCache;

	class  TabPage_TrackColumnSpec
	{
	public:
		enum _enum {
			eCode     = 0,
			eName     = 1,
			eDate     = 2,
			eTime     = 3,
			eWorkArea = 4,
		};
	private:
		RawListViewColumnSpec      m_cols;
		RawListViewColumnSpecItem  m_empty;
	public:
		TabPage_TrackColumnSpec(void)
		{
			CAtlString cs_title;
			cs_title.LoadString(IDS_TRACK_COLUMN_CODE); m_cols.push_back(RawListViewColumnSpecItem(cs_title , 180,  LVCFMT_LEFT  ));
			cs_title.LoadString(IDS_TRACK_COLUMN_NAME); m_cols.push_back(RawListViewColumnSpecItem(cs_title , 200,  LVCFMT_LEFT  ));
			cs_title.LoadString(IDS_TRACK_COLUMN_DATE); m_cols.push_back(RawListViewColumnSpecItem(cs_title ,  95,  LVCFMT_LEFT  ));
			cs_title.LoadString(IDS_TRACK_COLUMN_TIME); m_cols.push_back(RawListViewColumnSpecItem(cs_title ,  95,  LVCFMT_LEFT  ));
			cs_title.LoadString(IDS_TRACK_COLUMN_WA)  ; m_cols.push_back(RawListViewColumnSpecItem(cs_title , 130,  LVCFMT_LEFT  ));
		}
		~TabPage_TrackColumnSpec(void){}
	public:
		INT                TabPage_TrackColumnSpec::Count(void)const { return (INT)m_cols.size(); }
		const RawListViewColumnSpecItem& TabPage_TrackColumnSpec::Column(const INT nIndex)const
		{
			if (0 > nIndex || nIndex > this->Count() - 1)
				return this->m_empty;
			else
				return m_cols[nIndex];
		}
		const RawListViewColumnSpec&     TabPage_TrackColumnSpec::Columns(void)const
		{
			return m_cols;
		}
	};

	class  TabPage_TrackLayout
	{
		enum _enum {
			list_top_margin = 50,
			btn_horz_margin = 13,
			btn_horz_gap    =  5,
		};
	private:
		::ATL::CWindow&    m_page_ref;
		RECT               m_area;
	public:
		TabPage_TrackLayout(::ATL::CWindow& page_ref) : m_page_ref(page_ref)
		{
			m_page_ref.GetClientRect(&m_area);
		}
	public:
		VOID    TabPage_TrackLayout::AppendRow(const CTimeTrackRecord& rec_ref)
		{
			::ATL::CWindow list = m_page_ref.GetDlgItem(IDC_TRACK_TAB_LIST);
			CListViewWrap  wrapper(list);
			INT nRow = -1; wrapper.AddRow(nRow, true);
			if (-1 != nRow)
			{
				wrapper.Cell(nRow, TabPage_TrackColumnSpec::eCode,     rec_ref.Code());
				wrapper.Cell(nRow, TabPage_TrackColumnSpec::eName,     rec_ref.Name());
				wrapper.Cell(nRow, TabPage_TrackColumnSpec::eDate,     rec_ref.Date());
				wrapper.Cell(nRow, TabPage_TrackColumnSpec::eTime,     rec_ref.Time());
				wrapper.Cell(nRow, TabPage_TrackColumnSpec::eWorkArea, rec_ref.WorkArea());
			}
		}

		HRESULT TabPage_TrackLayout::InitializeList(void)
		{
			TabPage_TrackColumnSpec cols_spec;
			::ATL::CWindow list = m_page_ref.GetDlgItem(IDC_TRACK_TAB_LIST);
			CListViewWrap  wrapper(list);
			wrapper.Configure();
			HRESULT hr_ =  wrapper.AddColumns(cols_spec.Columns());
			return  hr_;
		}
		VOID    TabPage_TrackLayout::UpdateLayout(VOID)
		{
			::ATL::CWindow list = m_page_ref.GetDlgItem(IDC_TRACK_TAB_LIST);
			if (list)
			{
				RECT rcList   = m_area;
				rcList.top   += TabPage_TrackLayout::list_top_margin;
				rcList.right -= 2;
				list.SetWindowPos(NULL, &rcList, SWP_NOACTIVATE|SWP_NOZORDER);
			}
			INT nLeft = m_area.right - TabPage_TrackLayout::btn_horz_margin;
			const INT ctrl_ids[] = {
					IDC_TRACK_TAB_REFRESH,
					IDC_TRACK_AUTO_REFRESH
				};
			for (INT i_ = 0; i_ < _countof(ctrl_ids); i_++)
			{
				::ATL::CWindow btn_ctrl = m_page_ref.GetDlgItem(ctrl_ids[i_]);
				if (btn_ctrl)
				{
					RECT rc_btn = {0};
					btn_ctrl.GetWindowRect(&rc_btn); ::MapWindowPoints(HWND_DESKTOP, m_page_ref, reinterpret_cast<LPPOINT>(&rc_btn), 0x2);

					const INT n_width = __W(rc_btn);
					nLeft -=  n_width;

					rc_btn.left = nLeft;
					rc_btn.right = rc_btn.left + n_width;

					btn_ctrl.SetWindowPos(NULL, &rc_btn, SWP_NOZORDER|SWP_NOACTIVATE|SWP_NOSIZE);
					nLeft -= TabPage_TrackLayout::btn_horz_gap;
				}
			}
		}
		HRESULT TabPage_TrackLayout::UpdateList(const CTimeTrackDataProvider& provider, const bool bSelectLastRow = false)
		{
			::ATL::CWindow list = m_page_ref.GetDlgItem(IDC_TRACK_TAB_LIST);
			if (!list)
				return OLE_E_BLANK;
			CListViewWrap  wrapper(list);
			if (wrapper.RowsCount())
				wrapper.RemoveAllRows();
			const INT nRows = provider.RecordCount();
			if (nRows < 1)
				return S_OK;
			HRESULT hr_ = wrapper.RemoveAllRows(); 
			for (INT i_ = 0; i_ < nRows; i_++)
			{
				const CTimeTrackRecord& record = provider.Record(i_);
				if (!record.IsValid())
					continue;
				INT nRow  = -1;
				hr_ = wrapper.AddRow(nRow);
				if (S_OK != hr_)
					break;
				wrapper.Cell(nRow, TabPage_TrackColumnSpec::eCode, record.Code());
				wrapper.Cell(nRow, TabPage_TrackColumnSpec::eName, record.Name());
				wrapper.Cell(nRow, TabPage_TrackColumnSpec::eDate, record.Date());
				wrapper.Cell(nRow, TabPage_TrackColumnSpec::eTime, record.Time());
				wrapper.Cell(nRow, TabPage_TrackColumnSpec::eWorkArea, record.WorkArea());
			}
			if (bSelectLastRow)
				wrapper.SelectedRow(nRows - 1);
			return hr_;
		}
		HRESULT TabPage_TrackLayout::UpdateListEx(const CTimeTrackDataProvider& provider, const bool bSelectLastRow = false)
		{
			::ATL::CWindow list = m_page_ref.GetDlgItem(IDC_TRACK_TAB_LIST);
			if (!list)
				return OLE_E_BLANK;
			CListViewWrap  wrapper(list);
			const INT nRows    = wrapper.RowsCount();
			const INT nRecords = provider.RecordCount();
			
			if (nRecords < nRows)
				return this->UpdateList(provider, bSelectLastRow);
			else if (nRecords == nRows)
				return S_OK;
			else
			{
				for (INT i_ = nRows; i_ < nRecords; i_++)
				{
					const CTimeTrackRecord& record = provider.Record(i_);
					this->AppendRow(record);
				}
				if (bSelectLastRow)
					wrapper.SelectedRow(nRecords - 1);
			}
			return S_OK;
		}
	};	

	static HRESULT TabPage_TrackHelper_AddNew(CSharedObjects& obj_ref, CTimeTrackDataProvider& provider, CTimeTrackRecord& new_entry)
	{
		const INT nCount = obj_ref.Cache().Count();
		if (nCount < 1)
			return S_FALSE;
		_variant_t vProcessed;
		Global_GetDisplayElement().SetWaitingState(_T("Waiting for finger vein scanning..."));
		HRESULT hr_ = obj_ref.Processor().CreateVerificationTemplate(vProcessed);
		if (S_OK != hr_)
		{
			Global_GetDisplayElement().SetErrorState(obj_ref.Error().GetDescription());
			return hr_;
		}
		Global_GetDisplayElement().SetWorkingState(_T("Verifying finger vein..."));
		for (INT i_ = 0; i_ < nCount; i_++)
		{
			const CEmployeeDataRecord& employee = obj_ref.Cache().Item(i_);
			if (!employee.IsValid())
				continue;
			const CEmployeeFvData& fv_data = employee.FvData();
			if (fv_data.IsEmpty())
				continue;
			bool bVerified = false;
			const INT nImages = fv_data.Count();
			for ( INT j_ = 0; j_ < nImages; j_++)
			{
				const CEmployeeFvImage& fv_image = fv_data.Image(j_);
				if (!fv_image.IsValid())
					continue;
				hr_ = obj_ref.Processor().VerifyMatch(vProcessed, fv_image.Data());
				if (FAILED(hr_))
				{
					Global_GetDisplayElement().SetErrorState(obj_ref.Error().GetDescription());
					return hr_;
				}
				else if (S_FALSE == hr_)
					continue;
				else
				{
					bVerified = true;
					break;
				}
			}
			if (bVerified)
			{
				new_entry = employee;
				hr_ = provider.Append(new_entry);
				Global_GetDisplayElement().SetSuspendState(_T("Verification is successful"));
				return hr_;
			}
		}
		Global_GetDisplayElement().SetSuspendState(_T("Verification is not successful"));
		return  hr_;
	}

	static VOID    TabPage_TrackHelper_ShowStatus(CSharedObjects& obj_ref, LPCTSTR pszStatus, const HRESULT hr)
	{
		if (obj_ref.IsInitialized())
		{
			if (S_OK == hr)
				Global_GetDisplayElement().SetSuspendState(pszStatus);
			else
				Global_GetDisplayElement().SetErrorState(pszStatus);
		}
		else
			Global_GetDisplayElement().SetErrorState(obj_ref.Initializer().GetLastError_Ref().GetDescription());
	}

	static VOID    TabPage_TrackHelper_ShowStatus(CSharedObjects& obj_ref, const UINT nStatusId, const HRESULT hr)
	{
		::ATL::CAtlString cs_status;
		cs_status.LoadString(nStatusId);
		TabPage_TrackHelper_ShowStatus(obj_ref, cs_status.GetString(), hr);
	}

	static UINT    TabPage_TrackTimerId(void)
	{
		static const DWORD timer_id = ::GetTickCount();
		return timer_id;
	}

	static UINT    TabPage_TrackTimerFreq(void)
	{
		static bool bInitialized = false;
		static DWORD timer_freq  = 1000;
		if (bInitialized == false)
		{
			bInitialized = true;
			::ATL::CAtlString cs_profile = Global_GetSettingsRef().Storage().WorkAreaProfilePath();
			CPrivateProfile profile;
			HRESULT hr_  =  profile.Create(cs_profile.GetString());
			if (S_OK == hr_)
			{
				const CPrivateProfileSection& sec_ref = profile.SectionOf(_T("Common"));
				if (sec_ref.IsValid())
				{
					const CPrivateProfileItem& item_ref = sec_ref.ItemOf(_T("AutoRefreshPeriod"));
					if (item_ref.IsValid())
					{
						::ATL::CAtlString cs_period = item_ref.Value();
						if (!cs_period.IsEmpty())
						{
							const LONG nPeriod = ::_tstol(cs_period.GetString());
							if (0 < nPeriod)
								if (timer_freq < (DWORD)nPeriod)
									timer_freq = (DWORD)nPeriod;
						}
					}
				}
			}
		}
		return timer_freq;
	}
}}}}}

////////////////////////////////////////////////////////////////////////////

CTabPageTrack::CTabPageTrack(::WTL::CTabCtrl& ctrl_ref, Platinum::client::manager::CSharedObjects& obj_ref):
	TBasePage(IDD_PLATINUM_MAIN_DLG_TRACK_TAB, ctrl_ref, *this),
	m_objects(obj_ref),
	m_evt_timer(NULL),
	m_bSuspended(true)
{
}

CTabPageTrack::~CTabPageTrack(void)
{
#if 0 // it looks like this is the old bug: tracking entries must be saved only by clocking application
	HRESULT hr_ = m_provider.Save();
	if (FAILED(hr_))
		AtlMessageBox(
			::GetActiveWindow(),
			_T("Error occurred while saving the time tracking records"),
			Global_GetAppObjectRef().GetName(),
			MB_ICONSTOP|MB_OK
		);
#endif
}

////////////////////////////////////////////////////////////////////////////

LRESULT CTabPageTrack::TabPage__OnEvent(UINT uMsg, WPARAM wParam, LPARAM lParam, BOOL& bHandled)
{
	uMsg; wParam; lParam; bHandled = FALSE;
	switch (uMsg)
	{
	case WM_SHOWWINDOW:
		{
			if (wParam)
			{
				Global_GetDisplayElement().SetCountInfo(IDS_TRACK_DATA_COUNT, m_provider.RecordCount());
				details::TabPage_TrackHelper_ShowStatus(this->m_objects, _T("Ready"), S_OK);
			}
			m_bSuspended = (FALSE == wParam);
		} break;
	case WM_INITDIALOG:
		{
			if (TBasePage::m_bInitilized)
				break;
			details::TabPage_TrackLayout layout(*this);
			layout.InitializeList();
			layout.UpdateLayout();
			{				
				HRESULT hr_ = m_provider.Initialize(true, this->m_objects.Cache());
				if (S_OK != hr_)
				{
					CSysError sys_err(hr_);
					if (sys_err.HasDetails() == false)
						sys_err.SetUnknownMessage();
					ATL::CAtlString cs_error;
					cs_error.Format(
							IDS_TRACK_DATA_OPEN_ERR,
							hr_,
							sys_err.GetDescription()
						);
					Global_GetDisplayElement().SetErrorState(cs_error.GetString());
				}
			}
			layout.UpdateList(m_provider);
			{
				const bool bChecked  = m_objects.MgrSettings().AutoRefreshClocking();
				::WTL::CButton ctrl_ = TBasePage::GetDlgItem(IDC_TRACK_AUTO_REFRESH);
				ctrl_.SetCheck(bChecked ? BST_CHECKED : BST_UNCHECKED);
				if (bChecked && !m_evt_timer)
				{
					m_evt_timer = TBasePage::SetTimer(details::TabPage_TrackTimerId(), details::TabPage_TrackTimerFreq());
				}
			}
			TBasePage::m_bInitilized = true;
			bHandled = TRUE;
		} break;
	case WM_COMMAND:
		{
			if (!TBasePage::m_bInitilized)
				break;
			const WORD wNotify = HIWORD(wParam); wNotify;
			const WORD ctrlId  = LOWORD(wParam); ctrlId;
			if (false){}
			else if (IDC_TRACK_TAB_REFRESH == ctrlId)
			{
				m_provider.Refresh(this->m_objects.Cache());
				details::TabPage_TrackLayout layout(*this);
				layout.UpdateList(m_provider, true);
				Global_GetDisplayElement().SetCountInfo(
							IDS_TRACK_DATA_COUNT,
							m_provider.RecordCount()
						);
				bHandled = TRUE;
			}
			else if (IDC_TRACK_AUTO_REFRESH == ctrlId)
			{
				::WTL::CButton ctrl_ = TBasePage::GetDlgItem(ctrlId);
				const bool bChecked  = (BST_CHECKED == ctrl_.GetCheck());
				m_objects.MgrSettings().AutoRefreshClocking(bChecked);
				if (false){}
				else if (bChecked && !m_evt_timer)
				{
					m_evt_timer = TBasePage::SetTimer(details::TabPage_TrackTimerId(), details::TabPage_TrackTimerFreq());
				}
				else if (!bChecked && m_evt_timer)
				{
					TBasePage::KillTimer(m_evt_timer); m_evt_timer = NULL;
				}
				bHandled = TRUE;
			}
		} break;
	case WM_DESTROY:
		{
			if (m_evt_timer)
			{
				TBasePage::KillTimer(m_evt_timer); m_evt_timer = NULL;
			}
		} break;
	case WM_TIMER:
		{
			if ((UINT)wParam == details::TabPage_TrackTimerId())
			{
				if (!m_bSuspended)
				{
					m_provider.Refresh(this->m_objects.Cache());
					details::TabPage_TrackLayout layout(*this);
					layout.UpdateListEx(m_provider, true);
					Global_GetDisplayElement().SetCountInfo(
							IDS_TRACK_DATA_COUNT,
							m_provider.RecordCount()
						);
				}
				bHandled = TRUE;
			}
		} break;
	}
	return 0;
}

void    CTabPageTrack::UpdateLayout(void)
{
}

HRESULT CTabPageTrack::Validate(void)const
{
	return S_OK;
}