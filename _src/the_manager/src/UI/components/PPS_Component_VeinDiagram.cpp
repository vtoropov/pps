/*
	Created by Tech_dog (VToropov) on 7-Mar-2016 at 7:07:47pm, GMT+7, Phuket, Rawai, Monday;
	This is Finger Vein Graphical Diagram class implementation file.
*/
#include "StdAfx.h"
#include "PPS_Component_VeinDiagram.h"
#include "PPS_DataProvider_CommonDefs.h"

using namespace Platinum::client::UI::components;
using namespace Platinum::client::data;

#include "UIX_GdiProvider.h"

using namespace ex_ui::draw;

#include "Shared_RawData.h"

using namespace shared::lite::data;

#include "PlatinumClient_Resource.h"

/////////////////////////////////////////////////////////////////////////////

namespace Platinum { namespace client { namespace UI { namespace components { namespace details
{
	class CVeinDiagram_Layout
	{
	public:
		enum {
			e_col_count = 24,
			e_row_count = 25,
			e_pix_per_w = 10,
			e_pix_per_h =  3,
			e_pxa_per_w =  1,  // for histogram mode
			e_pxa_per_h = 23,  // for histogram mode
		};
	public:
		CVeinDiagram_Layout(void)
		{
		}
	public:
		SIZE    DefaultSize(const DWORD _style)const
		{
			const bool bHistogram = (CVeinDiagram::eStyleHistogram == _style);
			if (bHistogram)
			{
				const LONG lRowExpected = 3;
				const SIZE sz_ = {
					0,
					CVeinDiagram_Layout::e_pxa_per_h * lRowExpected + 2                      // border is 1px
				};
				return sz_;
			}
			else
			{
				const SIZE sz_ = {
					CVeinDiagram_Layout::e_col_count * CVeinDiagram_Layout::e_pix_per_w + 2, // border is 1px
					CVeinDiagram_Layout::e_row_count * CVeinDiagram_Layout::e_pix_per_h + 2  // border is 1px
				};
				return sz_;
			}
		}
	};
}}}}}

/////////////////////////////////////////////////////////////////////////////

CVeinDiagram::CVeinDiagramWnd::CVeinDiagramWnd(void) : m_style(CVeinDiagram::eStyleHistogram)
{
}

CVeinDiagram::CVeinDiagramWnd::~CVeinDiagramWnd(void)
{
}

/////////////////////////////////////////////////////////////////////////////

LRESULT   CVeinDiagram::CVeinDiagramWnd::OnCreate (UINT, WPARAM, LPARAM, BOOL&)
{
	return 0;
}

LRESULT   CVeinDiagram::CVeinDiagramWnd::OnDestroy(UINT, WPARAM, LPARAM, BOOL& bHandled)
{
	bHandled = FALSE;
	return 0;
}

LRESULT   CVeinDiagram::CVeinDiagramWnd::OnEraseBg(UINT, WPARAM wParam, LPARAM, BOOL&)
{
	RECT rc_0 = {0};
	TWindow::GetClientRect(&rc_0);

	CZBuffer z_buf(
			reinterpret_cast<HDC>(wParam),
			rc_0
		);
	CWindow parent_ = TWindow::GetParent();
	parent_.SendMessage(
			WM_PRINTCLIENT,
			reinterpret_cast<WPARAM>((HDC)z_buf),
			PRF_CLIENT
		);

	const COLORREF clr_ = ::GetSysColor(COLOR_3DDKSHADOW);

	z_buf.Draw3dRect(
			&rc_0,
			clr_ ,
			clr_
		);

	if (!(m_vein.vt & VT_ARRAY))
		return 0;

	CRawDataAccessor acc_(m_vein);
	if (!acc_.IsValid())
		return 0;

	LONG lSize_ = 0;
	HRESULT hr_ = acc_.GetSize(lSize_);
	if (FAILED(hr_))
		return 0;

	LONG ptr_ = 0, left_ = 0, top_ = 1;
	const bool bHistogram = (CVeinDiagram::eStyleHistogram == m_style);

	if (bHistogram)
	{
		for (; ptr_ < lSize_;)
		{
			for (INT j_ = 0; j_ < static_cast<INT>(__W(rc_0)); j_++)
			{
				left_ = j_;

				RECT rc_ = {
					left_,
					top_ ,
					left_ +  details::CVeinDiagram_Layout::e_pxa_per_w,
					top_  +  details::CVeinDiagram_Layout::e_pxa_per_h
				};
				if (ptr_ < lSize_)
				{
					BYTE alpha_ = acc_.AccessData()[ptr_];						
					z_buf.DrawSolidRect(rc_, RGB(0, 75, 106), alpha_);
				}
				else
					break;
				left_ += details::CVeinDiagram_Layout::e_pxa_per_w;
				ptr_  += 1;
			}
			top_ += details::CVeinDiagram_Layout::e_pxa_per_h; // editbox standard height
			/*if (top_ < rc_0.bottom - details::CVeinDiagram_Layout::e_pix_per_h)
				z_buf.DrawLine(
					rc_0.left,
					top_,
					rc_0.right,
					top_,
					clr_
				);*/
		}
	}
	else
	{
		for (INT j_ = 0; j_ < details::CVeinDiagram_Layout::e_row_count; j_++)
		{
			left_ = 1;
			for (INT i_ = 0; i_ < details::CVeinDiagram_Layout::e_col_count; i_++)
			{
				RECT rc_ = {
					left_,
					top_ ,
					left_ + details::CVeinDiagram_Layout::e_pix_per_w,
					top_  + details::CVeinDiagram_Layout::e_pix_per_h
				};

				if (ptr_ < lSize_)
				{
					BYTE alpha_ = acc_.AccessData()[ptr_];						
					z_buf.DrawSolidRect(rc_, RGB(0, 75, 106), alpha_);
				}
				left_ += details::CVeinDiagram_Layout::e_pix_per_w;
				ptr_  += 1;
			}
			top_ += details::CVeinDiagram_Layout::e_pix_per_h;
		}
	}
	return 0;
}

/////////////////////////////////////////////////////////////////////////////

CVeinDiagram::CVeinDiagram(void)
{
}

CVeinDiagram::~CVeinDiagram(void)
{
}

/////////////////////////////////////////////////////////////////////////////

HRESULT       CVeinDiagram::Create(HWND hParent, const RECT& rcArea)
{
	if (m_wnd.IsWindow())
		return (HRESULT_FROM_WIN32(ERROR_OBJECT_ALREADY_EXISTS));

	if (!::IsWindow(hParent))
		return OLE_E_INVALIDHWND;

	if (::IsRectEmpty(&rcArea))
		return OLE_E_INVALIDRECT;

	RECT rc_ = rcArea;
	m_wnd.Create(hParent, rc_, NULL, WS_VISIBLE|WS_CHILD, 0, IDC_EMPLOYEE_FV_PAGE_DATA_DGRM);

	if (!m_wnd)
		return (HRESULT_FROM_WIN32(::GetLastError()));
	else
		return (S_OK);
}

SIZE          CVeinDiagram::DefaultSize(void)const
{
	details::CVeinDiagram_Layout layout_;
	return layout_.DefaultSize(m_wnd.m_style);
}

HRESULT       CVeinDiagram::Destroy(void)
{
	if (!m_wnd)
		return S_OK;
	else
	{
		m_wnd.SendMessage(WM_CLOSE);
		return S_OK;
	}
}

HRESULT       CVeinDiagram::SetData(const _variant_t& _vein)
{
	HRESULT hr_ = ValidateVeinData(_vein);
	if (FAILED(hr_))
	{
		m_wnd.m_vein.Clear();
		if (m_wnd)
			m_wnd.Invalidate();
		return hr_;
	}

	m_wnd.m_vein = _vein;
	if (m_wnd)
		m_wnd.Invalidate();

	return hr_;
}

HRESULT       CVeinDiagram::SetStyle(const DWORD _style)
{
	const bool bChanged = (_style != m_wnd.m_style);
	switch (_style)
	{
	case CVeinDiagram::eStyleDiagram:
	case CVeinDiagram::eStyleHistogram:
		{
			m_wnd.m_style = _style;
		} break;
	default:
		return E_INVALIDARG;
	}

	if (bChanged)
		if (m_wnd)
			m_wnd.Invalidate();

	HRESULT hr_ = S_OK;
	return  hr_;
}