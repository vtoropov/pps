#ifndef __PLATINUMCLIENTUICOMPONENTTRACKTABPAGE_H_467279D3_54AA_463e_88AE_21A1862EBD49_INCLUDED
#define __PLATINUMCLIENTUICOMPONENTTRACKTABPAGE_H_467279D3_54AA_463e_88AE_21A1862EBD49_INCLUDED
/*
	Created by Tech_dog (VToropov) on 27-Mar-2014 at 1:17:22am, GMT+4, Saint-Petersburg, Thirsday;
	This is Platinum Client UI Component Time Tracking Tab Page class declaration file.
*/
#include "PPS_Component_TabPage_Base.h"
#include "PlatinumClient_SharedObjects.h"
#include "PPS_DataProvider_4_TimeTrack.h"

namespace Platinum { namespace client { namespace UI { namespace components
{
	using Platinum::client::manager::CSharedObjects;
	using Platinum::client::data::CTimeTrackDataProvider;

	class CTabPageTrack:
		public CTabPageBase, 
		public ITabPageCallback
	{
		typedef CTabPageBase   TBasePage;
	private:
		volatile bool          m_bSuspended;
		UINT_PTR               m_evt_timer;
		CSharedObjects&        m_objects;
		CTimeTrackDataProvider m_provider;
	public:
		CTabPageTrack(::WTL::CTabCtrl&, CSharedObjects&);
		~CTabPageTrack(void);
	public:
		virtual LRESULT        TabPage__OnEvent(UINT uMsg, WPARAM wParam, LPARAM lParam, BOOL& bHandled) override sealed;
		virtual void           UpdateLayout(void) override sealed;
		virtual HRESULT        Validate(void)const override sealed;
	};
}}}}

#endif/*__PLATINUMCLIENTUICOMPONENTTRACKTABPAGE_H_467279D3_54AA_463e_88AE_21A1862EBD49_INCLUDED*/