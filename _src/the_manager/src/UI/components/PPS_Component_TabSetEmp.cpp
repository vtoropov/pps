/*
	Created by Tech_dog (VToropov) on 24-Mar-2014 at 8:57:37pm, GMT+4, Saint-Petersburg, Monday;
	This is Platinum Client Employee Record Dialog Tab Set class implementation file.
*/
#include "StdAfx.h"
#include "PPS_Component_TabSetEmp.h"
#include "PlatinumClient_Resource.h"

using namespace Platinum;
using namespace Platinum::client;
using namespace Platinum::client::UI;
using namespace Platinum::client::UI::components;

////////////////////////////////////////////////////////////////////////////

CTabSetEmployeeRec::CTabSetEmployeeRec(ITabSetCallback& dlg_sink, CSharedObjects& obj_ref) : 
	m_tab_general(m_cTabCtrl, (ITabSetCallback&)*this, m_record[CTabSetEmployeeRec::eCurrentRecord]),
	m_tab_vein(m_cTabCtrl, (ITabSetCallback&)*this, m_record[CTabSetEmployeeRec::eCurrentRecord], obj_ref),
	m_validator(m_record[CTabSetEmployeeRec::eCurrentRecord]),
	m_dlg_sink(dlg_sink)
{
}

CTabSetEmployeeRec::~CTabSetEmployeeRec(void)
{
}

////////////////////////////////////////////////////////////////////////////

HRESULT         CTabSetEmployeeRec::Create(const HWND hParent, const RECT& rcArea)
{
	if(!::IsWindow(hParent) || ::IsRectEmpty(&rcArea))
		return E_INVALIDARG;
	RECT rcTabs = rcArea;
	::InflateRect(&rcTabs, -5, -3);
	m_cTabCtrl.Create(hParent, rcTabs, 0, WS_CHILD|WS_VISIBLE|WS_CLIPCHILDREN|WS_CLIPSIBLINGS, WS_EX_CONTROLPARENT, IDC_EMPLOYEE_DLG_TABSET);
	m_cTabCtrl.SetFont(::ATL::CWindow(hParent).GetFont());
	m_cTabCtrl.AddItem(_T("General"));
	{
		m_tab_general.Create(m_cTabCtrl.m_hWnd);
		m_tab_general.SetWindowPos(HWND_TOP, 0, 0, 0, 0, SWP_NOSIZE|SWP_NOMOVE|SWP_SHOWWINDOW);
	}
	m_cTabCtrl.AddItem(_T("Finger Vein"));
	{
		m_tab_vein.Create(m_cTabCtrl.m_hWnd);
		m_tab_vein.SetWindowPos(HWND_TOP, 0, 0, 0, 0, SWP_NOSIZE|SWP_NOMOVE|SWP_HIDEWINDOW);
	}
	return S_OK;
}

HRESULT         CTabSetEmployeeRec::Destroy(void)
{
	if (m_tab_general.IsWindow())
	{
		m_tab_general.DestroyWindow();
		m_tab_general.m_hWnd = NULL;
	}
	if (m_tab_vein.IsWindow())
	{
		m_tab_vein.DestroyWindow();
		m_tab_vein.m_hWnd = NULL;
	}
	return S_OK;
}

HRESULT         CTabSetEmployeeRec::Initialize(const CEmployeeDataRecord& pCurrent, const bool bNewData)
{
	if (!bNewData)
	{
		m_record[CTabSetEmployeeRec::eCurrentRecord] = pCurrent;
		m_record[CTabSetEmployeeRec::ePriorRecord] = pCurrent;
	}
	return S_OK;
}

void            CTabSetEmployeeRec::UpdateLayout(void)
{
	const INT nTabIndex = m_cTabCtrl.GetCurSel();
	switch (nTabIndex)
	{
	case 0:
		{
			if (m_tab_general.IsWindow())  m_tab_general.ShowWindow(SW_SHOW);
			if (m_tab_vein.IsWindow())     m_tab_vein.ShowWindow(SW_HIDE);
		} break;
	case 1:
		{
			if (m_tab_vein.IsWindow())     m_tab_vein.ShowWindow(SW_SHOW);
			if (m_tab_general.IsWindow())  m_tab_general.ShowWindow(SW_HIDE);
		} break;
	default:;
			if (m_tab_general.IsWindow())  m_tab_general.ShowWindow(SW_SHOW);
			if (m_tab_vein.IsWindow())     m_tab_vein.ShowWindow(SW_HIDE);
	}
}

void            CTabSetEmployeeRec::SetPage(const INT nIndex)
{
	switch (nIndex)
	{
	case 0:
	case 1:
		{
			m_cTabCtrl.SetCurSel(nIndex);
			this->UpdateLayout();
		} break;
	}
}

////////////////////////////////////////////////////////////////////////////

bool    CTabSetEmployeeRec::TabSet__IsNewMode(void)
{
	return m_dlg_sink.TabSet__IsNewMode();
}

HRESULT CTabSetEmployeeRec::TabSet__OnDataChanged(const UINT pageId, const bool bChanged)
{
	bChanged;
	const bool bChanged_ = m_record[CTabSetEmployeeRec::eCurrentRecord] != m_record[CTabSetEmployeeRec::ePriorRecord];
	m_dlg_sink.TabSet__OnDataChanged(pageId, bChanged_);
	return S_OK;
}

////////////////////////////////////////////////////////////////////////////

const CTabPageEmpRecGeneral&  CTabSetEmployeeRec::GeneralPage(void) const
{
	return m_tab_general;
}

CTabPageEmpRecGeneral&        CTabSetEmployeeRec::GeneralPage(void)
{
	return m_tab_general;
}

////////////////////////////////////////////////////////////////////////////

const CEmployeeDataRecord&    CTabSetEmployeeRec::CurrentRecord(void)const
{
	return m_record[CTabSetEmployeeRec::eCurrentRecord];
}

CEmployeeDataRecord&          CTabSetEmployeeRec::CurrentRecord(void)
{
	return m_record[CTabSetEmployeeRec::eCurrentRecord];
}

const CEmployeeDataRecord_ValidateRule& CTabSetEmployeeRec::Validator(void)const
{
	return m_validator;
}