#ifndef __PLATINUMCLIENTUICOMPONENTENROLLTABPAGE_H_0CA4CEFA_996C_478f_BC62_B2620E4B07F3_INCLUDED
#define __PLATINUMCLIENTUICOMPONENTENROLLTABPAGE_H_0CA4CEFA_996C_478f_BC62_B2620E4B07F3_INCLUDED
/*
	Created by Tech_dog (VToropov) on 24-Mar-2014 at 10:39:46am, GMT+4, Saint-Petersburg, Monday;
	This is Platinum Client UI Component Enrollment Tab Page class declaration file.
*/
#include "PlatinumClient_SharedObjects.h"
#include "PPS_Component_TabPage_Base.h"
#include "PPS_DataProvider_4_Enroll.h"
#include "PPS_ListView_Wrap.h"

namespace Platinum { namespace client { namespace UI { namespace components
{
	using Platinum::client::data::CEnrollDataProvider;
	using Platinum::client::manager::CSharedObjects;
	using Platinum::client::UI::common::CListViewSorter;

	class CTabPageEnroll:
		public CTabPageBase, 
		public ITabPageCallback
	{
		typedef CTabPageBase  TBasePage;
	private:
		CSharedObjects&       m_objects;
		CEnrollDataProvider   m_provider;
		CWindow               m_list;
		CListViewSorter       m_sorter;
	public:
		CTabPageEnroll(::WTL::CTabCtrl&, CSharedObjects&);
		~CTabPageEnroll(void);
	public:
		virtual LRESULT    TabPage__OnEvent(UINT uMsg, WPARAM wParam, LPARAM lParam, BOOL& bHandled) override sealed;
		virtual void       UpdateLayout(void) override sealed;
		virtual HRESULT    Validate(void)const override sealed;
	};
}}}}

#endif/*__PLATINUMCLIENTUICOMPONENTENROLLTABPAGE_H_0CA4CEFA_996C_478f_BC62_B2620E4B07F3_INCLUDED*/