/*
	Created by Tech_dog (VToropov) on 21-Feb-2016 at 6:13:31pm, GMT+7, Phuket, Rawai, Sunday;
	This is Platinum Client UI Component Setup Tab Page class implementation file.
*/
#include "StdAfx.h"
#include "PlatinumClient_Resource.h"
#include "PPS_Component_TabPageSetup.h"

using namespace Platinum::client::UI::components;

/////////////////////////////////////////////////////////////////////////////

CTabPageSetup::CTabPageSetup(::WTL::CTabCtrl& ctrl_ref, Platinum::client::manager::CSharedObjects& obj_ref):
	TBasePage(IDD_PLATINUM_MAIN_DLG_SETUP_TAB, ctrl_ref, *this),
	m_objects(obj_ref)
{
}

CTabPageSetup::~CTabPageSetup(void)
{
}

/////////////////////////////////////////////////////////////////////////////

LRESULT CTabPageSetup::TabPage__OnEvent(UINT uMsg, WPARAM wParam, LPARAM lParam, BOOL& bHandled)
{
	uMsg; wParam; lParam; bHandled = FALSE;
	switch (uMsg)
	{
	case WM_SHOWWINDOW:
		{
			if (wParam)
			{
			}
		} break;
	case WM_INITDIALOG:
		{
			if (TBasePage::m_bInitilized)
				break;
			TBasePage::m_bInitilized = true;
			bHandled = TRUE;
		} break;
	case WM_COMMAND:
		{
			if (!TBasePage::m_bInitilized)
				break;
			const WORD wNotify = HIWORD(wParam); wNotify;
			const WORD ctrlId  = LOWORD(wParam); ctrlId;
			bHandled = TRUE;
		} break;
	}
	return 0;
}

void    CTabPageSetup::UpdateLayout(void)
{
}

HRESULT CTabPageSetup::Validate(void)const
{
	return S_OK;
}