/*
	Created by Tech_dog (VToropov) on 24-Mar-2014 at 10:26:34am, GMT+4, Saint-Petersburg, Monday;
	This is Platinum Client UI Component Tab Page Base class implementation file.
*/
#include "StdAfx.h"
#include "PPS_Component_TabPage_Base.h"
#include "UIX_GdiProvider.h"

using namespace Platinum;
using namespace Platinum::client;
using namespace Platinum::client::UI;
using namespace Platinum::client::UI::components;

CTabPageBase::CTabPageBase(const UINT RID, ::WTL::CTabCtrl& ctrl_ref, ITabPageCallback& evt_snk_ref):
	m_ctrl_ref(ctrl_ref),
	m_evt_sink(evt_snk_ref),
	IDD(RID),
	m_bInitilized(false)
{
}

CTabPageBase::~CTabPageBase(void)
{
}

////////////////////////////////////////////////////////////////////////////

LRESULT   CTabPageBase::OnPageInit (UINT uMsg, WPARAM wParam, LPARAM lParam, BOOL& bHandled)
{
	RECT rcArea = {0};
	m_ctrl_ref.GetWindowRect(&rcArea);
	m_ctrl_ref.ScreenToClient(&rcArea);
	m_ctrl_ref.AdjustRect(false, &rcArea);
	TBaseDialog::SetWindowPos(0, rcArea.left, rcArea.top, __W(rcArea), __H(rcArea), SWP_NOZORDER|SWP_NOACTIVATE);
	if (::WTL::CTheme::IsThemingSupported())
	{
		HRESULT hr__ = ::EnableThemeDialogTexture(*this, ETDT_ENABLETAB);
		ATLVERIFY(SUCCEEDED(hr__));
	}
	m_evt_sink.TabPage__OnEvent(uMsg, wParam, lParam, bHandled);
	return 0;
}

LRESULT   CTabPageBase::OnPagePaint(UINT uMsg, WPARAM wParam, LPARAM lParam, BOOL& bHandled)
{
	uMsg; wParam; lParam; bHandled = FALSE;
	::WTL::CPaintDC dc(TBaseDialog::m_hWnd);
	return 0;
}