#ifndef _PPSCOMPONENTVEINLIST_H_45674FBF_665C_4382_B590_29524F0E1D4E_INCLUDED
#define _PPSCOMPONENTVEINLIST_H_45674FBF_665C_4382_B590_29524F0E1D4E_INCLUDED
/*
	Created by Tech_dog (VToropov) on 17-Mar-2016 at 10:35:00pm, GMT+7, Phuket, Rawai, Thursday;
	This is Platinum client vein image list view class declaration file.
*/
#include "PPS_EmployeeDataRecord.h"

namespace Platinum { namespace client { namespace UI { namespace components
{
	using Platinum::client::data::CEmployeeDataRecord;

	class CVeinList
	{
	private:
		CWindow     m_list;
	public:
		CVeinList(void);
		~CVeinList(void);
	public:
		HRESULT     Destroy(void);
		HWND        Host(void)const;
		bool        IsValid(void)const;
		INT         SelectedItem(void)const;
		HRESULT     Subclass(HWND hParent, const UINT ctrlId);
		HRESULT     Update(const CEmployeeDataRecord&);
	};
}}}}

#endif/*_PPSCOMPONENTVEINLIST_H_45674FBF_665C_4382_B590_29524F0E1D4E_INCLUDED*/