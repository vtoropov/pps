#ifndef _PPSCOMPONENTVEINDIAGRAM_H_7BFEDF67_A48D_4d57_9B58_670CA0AC41D5_INCLUDED
#define _PPSCOMPONENTVEINDIAGRAM_H_7BFEDF67_A48D_4d57_9B58_670CA0AC41D5_INCLUDED
/*
	Created by Tech_dog (VToropov) on 7-Mar-2016 at 6:28:43pm, GMT+7, Phuket, Rawai, Monday;
	This is Finger Vein Graphical Diagram class declaration file.
*/

namespace Platinum { namespace client { namespace UI { namespace components
{
	class CVeinDiagram
	{
	public:
		enum {
			eStyleDiagram   = 0, // default
			eStyleHistogram = 1
		};
	private:
		class CVeinDiagramWnd :
			public  ::ATL::CWindowImpl<CVeinDiagramWnd>
		{
			typedef ::ATL::CWindowImpl<CVeinDiagramWnd> TWindow;
			friend class CVeinDiagram;
		private:
			_variant_t     m_vein;
			DWORD          m_style;
		public:
			BEGIN_MSG_MAP(CVeinDiagramWnd)
				MESSAGE_HANDLER(WM_CREATE     ,   OnCreate )
				MESSAGE_HANDLER(WM_DESTROY    ,   OnDestroy)
				MESSAGE_HANDLER(WM_ERASEBKGND ,   OnEraseBg)
			END_MSG_MAP()
		public:
			CVeinDiagramWnd(void);
			~CVeinDiagramWnd(void);
		private:
			LRESULT   OnCreate (UINT, WPARAM, LPARAM, BOOL&);
			LRESULT   OnDestroy(UINT, WPARAM, LPARAM, BOOL&);
			LRESULT   OnEraseBg(UINT, WPARAM, LPARAM, BOOL&);
		};
	private:
		CVeinDiagramWnd   m_wnd;
	public:
		CVeinDiagram(void);
		~CVeinDiagram(void);
	public:
		HRESULT       Create(HWND hParent, const RECT& rcArea);
		SIZE          DefaultSize(void)const;
		HRESULT       Destroy(void);
		HRESULT       SetData(const _variant_t& _vein);
		HRESULT       SetStyle(const DWORD);
	private:
		CVeinDiagram(const CVeinDiagram&);
		CVeinDiagram& operator= (const CVeinDiagram&);
	};
}}}}

#endif/*_PPSCOMPONENTVEINDIAGRAM_H_7BFEDF67_A48D_4d57_9B58_670CA0AC41D5_INCLUDED*/