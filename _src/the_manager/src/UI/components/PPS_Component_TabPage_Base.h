#ifndef __PLATINUMCLIENTUICOMPONENTTABPAGEBASE_H_BA5CE25E_1FA7_4df1_BA78_73EAA49DDE11_INCLUDED
#define __PLATINUMCLIENTUICOMPONENTTABPAGEBASE_H_BA5CE25E_1FA7_4df1_BA78_73EAA49DDE11_INCLUDED
/*
	Created by Tech_dog (VToropov) on 24-Mar-2014 at 10:18:20am, GMT+4, Saint-Petersburg, Monday;
	This is Platinum Client UI Component Tab Page Base class declaration file.
*/

namespace Platinum { namespace client { namespace UI { namespace components
{
	interface ITabPageCallback
	{
		virtual LRESULT TabPage__OnEvent(UINT uMsg, WPARAM wParam, LPARAM lParam, BOOL& bHandled) PURE;
	};
	interface ITabSetCallback
	{
		virtual bool    TabSet__IsNewMode(void) PURE;
		virtual HRESULT TabSet__OnDataChanged(const UINT pageId, const bool bChanged) PURE;
		virtual HRESULT TabSet__OnDataComplete(const UINT pageId) {pageId; return E_NOTIMPL;}
	};

	class CTabPageBase:
		public  ::ATL::CDialogImpl<CTabPageBase>
	{
		typedef ::ATL::CDialogImpl<CTabPageBase> TBaseDialog;
	public:
		const UINT IDD;
	protected:
		bool                m_bInitilized;
		::WTL::CTabCtrl&    m_ctrl_ref;      // parent tabbed control reference
		ITabPageCallback&   m_evt_sink;
	protected:
		CTabPageBase(const UINT RID, ::WTL::CTabCtrl&, ITabPageCallback&);
		virtual ~CTabPageBase(void);
	public:
		BEGIN_MSG_MAP(CTabPageBase)
			MESSAGE_HANDLER    (WM_INITDIALOG,   OnPageInit )
			MESSAGE_HANDLER    (WM_PAINT,        OnPagePaint)
			m_evt_sink.TabPage__OnEvent(uMsg, wParam, lParam, bHandled);
			if (bHandled)
				return TRUE;
		END_MSG_MAP()
	protected:
		virtual LRESULT     OnPageInit (UINT uMsg, WPARAM wParam, LPARAM lParam, BOOL& bHandled);
		virtual LRESULT     OnPagePaint(UINT uMsg, WPARAM wParam, LPARAM lParam, BOOL& bHandled);
	public:
		virtual void        UpdateLayout(void)  = 0;
		virtual HRESULT     Validate(void)const = 0;
	};
}}}}

#endif/*__PLATINUMCLIENTUICOMPONENTTABPAGEBASE_H_BA5CE25E_1FA7_4df1_BA78_73EAA49DDE11_INCLUDED*/