#ifndef _PPSCOMPONENTTABPAGESETUP_H_08BC375E_C9E2_4a72_B5E8_9DA5EB8AB669_INCLUDED
#define _PPSCOMPONENTTABPAGESETUP_H_08BC375E_C9E2_4a72_B5E8_9DA5EB8AB669_INCLUDED
/*
	Created by Tech_dog (VToropov) on 21-Feb-2016 at 6:05:17pm, GMT+7, Phuket, Rawai, Sunday;
	This is Platinum Client UI Component Setup Tab Page class declaration file.
*/
#include "PlatinumClient_SharedObjects.h"
#include "PPS_Component_TabPage_Base.h"

namespace Platinum { namespace client { namespace UI { namespace components
{
	using Platinum::client::manager::CSharedObjects;

	class CTabPageSetup:
		public CTabPageBase, 
		public ITabPageCallback
	{
		typedef CTabPageBase  TBasePage;
	private:
		CSharedObjects&       m_objects;
	public:
		CTabPageSetup(::WTL::CTabCtrl&, CSharedObjects&);
		~CTabPageSetup(void);
	public:
		virtual LRESULT    TabPage__OnEvent(UINT uMsg, WPARAM wParam, LPARAM lParam, BOOL& bHandled) override sealed;
		virtual void       UpdateLayout(void) override sealed;
		virtual HRESULT    Validate(void)const override sealed;
	};
}}}}

#endif/*_PPSCOMPONENTTABPAGESETUP_H_08BC375E_C9E2_4a72_B5E8_9DA5EB8AB669_INCLUDED*/