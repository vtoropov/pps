/*
	Created by Tech_dog (VToropov) on 24-Mar-2014 at 6:57:14pm, GMT+4, Saint-Petersburg, Monday;
	This is Platinum Client Employee Record General Tab Page class implementation file.
*/
#include "StdAfx.h"
#include "PPS_Component_Emp_TabPageGeneral.h"
#include "PlatinumClient_Resource.h"
#include "PlatinumClient_CommonSettings.h"
#include "PPS_DataListWrapWA.h"

using namespace Platinum;
using namespace Platinum::client;
using namespace Platinum::client::UI;
using namespace Platinum::client::UI::components;
using namespace Platinum::client::data;
using namespace Platinum::client::data::wrappers;

extern TMgrSettings& Global_GetMgrSettingsRef(void);

////////////////////////////////////////////////////////////////////////////

namespace Platinum { namespace client { namespace UI { namespace components { namespace details
{
	class TabPage_InputHandleHelper
	{
	private:
		::ATL::CWindow       m_ctrl;
	public:
		TabPage_InputHandleHelper(::ATL::CWindow& parent_ref, const UINT ctrlId)
		{
			m_ctrl = parent_ref.GetDlgItem(ctrlId);
		}
		~TabPage_InputHandleHelper(void){}
	public:
		HRESULT  GetCtrlItem(::ATL::CAtlString& buf_ref)
		{
			if (!m_ctrl) return OLE_E_BLANK;
			::WTL::CComboBox combo = m_ctrl;
			const INT nIndex = combo.GetCurSel();
			if (nIndex != CB_ERR)
			{
				combo.GetLBText(nIndex, buf_ref);
			}
			return S_OK;
		}
		HRESULT  GetCtrlText(::ATL::CAtlString& buf_ref)
		{
			if (!m_ctrl) return OLE_E_BLANK;
			m_ctrl.GetWindowText(buf_ref);
			return S_OK;
		}
		HRESULT  SetCtrlText(LPCTSTR pText)
		{
			if (!m_ctrl) return OLE_E_BLANK;
			const BOOL bResult = m_ctrl.SetWindowText(pText);
			return (bResult ? S_OK : S_FALSE);
		}
		HRESULT  SetCtrlItem(LPCTSTR pText)
		{
			if (!m_ctrl)
				return OLE_E_BLANK;
			::WTL::CComboBox combo = m_ctrl;
			const INT nIndex = combo.SelectString(-1, pText);
			return (CB_ERR != nIndex ? S_OK : S_FALSE);
		}
		HRESULT  SetCtrlBannerText(LPCTSTR pszText, const bool bComboCtrlType)
		{
			if (!m_ctrl)
				return OLE_E_BLANK;
			if (bComboCtrlType)
			{
				::WTL::CComboBox cbo_ = m_ctrl;
				cbo_.SetCueBannerText(pszText);
			}
			else
			{
				::WTL::CEdit edt_ = m_ctrl;
				edt_.SetCueBannerText(pszText, TRUE);
			}
			return S_OK;
		}
	};
}}}}}

/////////////////////////////////////////////////////////////////////////////

CTabPageEmpRecGeneral::CTabPageEmpRecGeneral(::WTL::CTabCtrl& ctrl_ref, ITabSetCallback& tabs_sink, CEmployeeDataRecord& rec_ref) : 
	TBasePage(IDD_EMPLOYEE_DLG_TAB_GENERAL, ctrl_ref, *this),
	m_tabs_sink(tabs_sink), m_record(rec_ref)
{
}

CTabPageEmpRecGeneral::~CTabPageEmpRecGeneral(void)
{
}

/////////////////////////////////////////////////////////////////////////////

LRESULT    CTabPageEmpRecGeneral::TabPage__OnEvent(UINT uMsg, WPARAM wParam, LPARAM lParam, BOOL& bHandled)
{
	uMsg; wParam; lParam; bHandled = FALSE;
	switch (uMsg)
	{
	case WM_INITDIALOG:
		{
			if (TBasePage::m_bInitilized)
				break;
			TBasePage::m_bInitilized = true;

			CWorkAreaDataListWrap list(Global_GetMgrSettingsRef());
			list.AttachTo(*this, IDC_EMPLOYEE_GENERAL_PAGE_AREA);
			list.SetHeight(14, true);
			list.SeedData();

			details::TabPage_InputHandleHelper ctrl_code(*this, IDC_EMPLOYEE_GENERAL_PAGE_CODE);
			details::TabPage_InputHandleHelper ctrl_name(*this, IDC_EMPLOYEE_GENERAL_PAGE_NAME);
			details::TabPage_InputHandleHelper ctrl_area(*this, IDC_EMPLOYEE_GENERAL_PAGE_AREA);
			
			ctrl_code.SetCtrlBannerText(_T("Type employee code"), false);
			ctrl_name.SetCtrlBannerText(_T("Type employee name"), false);
			ctrl_area.SetCtrlBannerText(_T("No work area selected"), true);

			if (!m_tabs_sink.TabSet__IsNewMode())
			{
				ctrl_code.SetCtrlText(m_record.Code());
				ctrl_name.SetCtrlText(m_record.Name());
				ctrl_area.SetCtrlItem(m_record.WorkArea());
			}
			bHandled = TRUE;
		} break;
	case WM_COMMAND:
		{
			if (!TBasePage::m_bInitilized)
				break;
			bHandled = TRUE;
			const WORD wNotify = HIWORD(wParam); wNotify;
			const WORD ctrlId  = LOWORD(wParam); ctrlId;
			if (false){}
			else if (EN_CHANGE == wNotify
				&& IDC_EMPLOYEE_GENERAL_PAGE_CODE == ctrlId)
			{
				details::TabPage_InputHandleHelper helper(*this, ctrlId);
				::ATL::CAtlString  cs_code;
				helper.GetCtrlText(cs_code);
				m_record.Code(cs_code);
			}
			else if (EN_CHANGE == wNotify
				&& IDC_EMPLOYEE_GENERAL_PAGE_NAME == ctrlId)
			{
				details::TabPage_InputHandleHelper helper(*this, ctrlId);
				::ATL::CAtlString  cs_name;
				helper.GetCtrlText(cs_name);
				m_record.Name(cs_name);
			}
			else if (CBN_SELCHANGE == wNotify
				&& IDC_EMPLOYEE_GENERAL_PAGE_AREA == ctrlId)
			{
				details::TabPage_InputHandleHelper helper(*this, ctrlId);
				::ATL::CAtlString  cs_wa;
				helper.GetCtrlItem(cs_wa);
				m_record.WorkArea (cs_wa);
			}
			else
				bHandled = FALSE;
			if (bHandled)
				m_tabs_sink.TabSet__OnDataChanged(TBasePage::IDD, true);
		} break;
	}
	return 0;
}

void       CTabPageEmpRecGeneral::UpdateLayout(void)
{
}

HRESULT    CTabPageEmpRecGeneral::Validate(void)const
{
	return  E_NOTIMPL;
}