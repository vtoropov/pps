/*
	Created by Tech_dog (VToropov) on 24-Mar-2014 at 11:03:30am, GMT+4, Saint-Petersburg, Monday;
	This is Platinum Client UI Component Main Form Tab Set class implementation file.
*/
#include "StdAfx.h"
#include "PPS_Component_TabSetMain.h"
#include "PlatinumClient_Resource.h"

using namespace Platinum;
using namespace Platinum::client;
using namespace Platinum::client::UI;
using namespace Platinum::client::UI::components;

////////////////////////////////////////////////////////////////////////////

namespace Platinum { namespace client { namespace UI { namespace components { namespace details
{
	class  TabSet_Layout
	{
	};
}}}}}

////////////////////////////////////////////////////////////////////////////

CTabSet::CTabSet(CSharedObjects& obj_ref) : m_tab_enroll(m_cTabCtrl, obj_ref), m_tab_track(m_cTabCtrl, obj_ref)
{
}

CTabSet::~CTabSet(void)
{
}

////////////////////////////////////////////////////////////////////////////

HRESULT       CTabSet::Create(const HWND hParent, const RECT& rcArea)
{
	if(!::IsWindow(hParent) || ::IsRectEmpty(&rcArea))
		return E_INVALIDARG;
	RECT rcTabs = rcArea;
	::InflateRect(&rcTabs, -5, -3);
	m_cTabCtrl.Create(hParent, rcTabs, 0, WS_CHILD|WS_VISIBLE|WS_CLIPCHILDREN|WS_CLIPSIBLINGS, WS_EX_CONTROLPARENT, IDD_PLATINUM_MAIN_DLG_TABSET);
	m_cTabCtrl.SetFont(::ATL::CWindow(hParent).GetFont());
	m_cTabCtrl.AddItem(_T("Employee Maintenance"));
	{
		m_tab_enroll.Create(m_cTabCtrl.m_hWnd);
		m_tab_enroll.SetWindowPos(HWND_TOP, 0, 0, 0, 0, SWP_NOSIZE|SWP_NOMOVE|SWP_SHOWWINDOW);
	}
	m_cTabCtrl.AddItem(_T("Time Recording"));
	{
		m_tab_track.Create(m_cTabCtrl.m_hWnd);
		m_tab_track.SetWindowPos(HWND_TOP, 0, 0, 0, 0, SWP_NOSIZE|SWP_NOMOVE|SWP_HIDEWINDOW);
	}
	return S_OK;
}

HRESULT       CTabSet::Destroy(void)
{
	if (m_tab_enroll.IsWindow())
	{
		m_tab_enroll.DestroyWindow();
		m_tab_enroll.m_hWnd = NULL;
	}
	if (m_tab_track.IsWindow())
	{
		m_tab_track.DestroyWindow();
		m_tab_track.m_hWnd = NULL;
	}
	return S_OK;
}

void          CTabSet::UpdateLayout(void)
{
	const INT nTabIndex = m_cTabCtrl.GetCurSel();
	switch (nTabIndex)
	{
	case 0:
		{
			if (m_tab_enroll.IsWindow())      m_tab_enroll.ShowWindow(SW_SHOW);
			if (m_tab_track.IsWindow())       m_tab_track.ShowWindow(SW_HIDE);
		} break;
	case 1:
		{
			if (m_tab_track.IsWindow())       m_tab_track.ShowWindow(SW_SHOW);
			if (m_tab_enroll.IsWindow())      m_tab_enroll.ShowWindow(SW_HIDE);
		} break;
	default:
			if (m_tab_enroll.IsWindow())      m_tab_enroll.ShowWindow(SW_SHOW);
			if (m_tab_track.IsWindow())       m_tab_track.ShowWindow(SW_HIDE);
	}
}