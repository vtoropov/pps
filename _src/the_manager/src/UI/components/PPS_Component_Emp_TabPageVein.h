#ifndef __PLATINUMCLIENTEMPLOYEERECORDVEINTABPAGE_H_1AAFCE4F_77E4_4726_BDC8_F86772CB596A_INCLUDED
#define __PLATINUMCLIENTEMPLOYEERECORDVEINTABPAGE_H_1AAFCE4F_77E4_4726_BDC8_F86772CB596A_INCLUDED
/*
	Created by Tech_dog (VToropov) on 26-Mar-2014 at 8:44:37pm, GMT+4, Saint-Petersburg, Wednesday;
	This is Platinum Client Employee Record Finger Vein Tab Page class declaration file.
*/
#include "PPS_Component_TabPage_Base.h"
#include "PPS_Component_VeinDiagram.h"
#include "PPS_EmployeeDataRecord.h"
#include "PPS_Component_VeinList.h"

#include "PlatinumClient_SharedObjects.h"

namespace Platinum { namespace client { namespace UI { namespace components
{
	using Platinum::client::data::CEmployeeDataRecord;
	using Platinum::client::manager::CSharedObjects;

	class CTabPageEmpRecVein:
		public CTabPageBase, 
		public ITabPageCallback
	{
		typedef CTabPageBase  TBasePage;
	private:
		CVeinList             m_vein_list;
		CVeinDiagram          m_diagram;
		ITabSetCallback&      m_tabs_sink;
		CEmployeeDataRecord&  m_record;
		CSharedObjects&       m_objects;
	public:
		CTabPageEmpRecVein(::WTL::CTabCtrl&, ITabSetCallback& tabs_sink, CEmployeeDataRecord&, CSharedObjects&);
		~CTabPageEmpRecVein(void);
	public:
		virtual LRESULT    TabPage__OnEvent(UINT uMsg, WPARAM wParam, LPARAM lParam, BOOL& bHandled) override sealed;
		virtual void       UpdateLayout(void) override sealed;
		virtual HRESULT    Validate(void)const override sealed;
	};
}}}}

#endif/*__PLATINUMCLIENTEMPLOYEERECORDVEINTABPAGE_H_1AAFCE4F_77E4_4726_BDC8_F86772CB596A_INCLUDED*/