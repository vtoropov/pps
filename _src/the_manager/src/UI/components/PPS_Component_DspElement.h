#ifndef __PLATINUMCLIENTDISPLAYINGELEMENT_H_EB5D9A5A_3D65_49b7_8C08_B93D4F4AE129_INCLUDED
#define __PLATINUMCLIENTDISPLAYINGELEMENT_H_EB5D9A5A_3D65_49b7_8C08_B93D4F4AE129_INCLUDED
/*
	Created by Tech_dog (VToropov) on 23-Mar-2014 at 5:05:20pm, GMT+4, Saint-Petersburg, Sunday;
	This is Platinum Payroll Systems Client Application Displaying Element class declaration file.
*/

namespace Platinum { namespace client { namespace UI { namespace common
{
	class CDisplayingElement
	{
	protected:
		::ATL::CWindow      m_wnd;
		bool                m_bManaged; // if true, the incapsulated window has to be destroyed
	public:
		CDisplayingElement(void);
		virtual ~CDisplayingElement(void);
	public:
		virtual HRESULT     CreateFromDialogControl(const HWND hDialog, const UINT nCtrlId);
		virtual HRESULT     Destroy(void);
		virtual HRESULT     Display(LPCTSTR pInfo);
		virtual HRESULT     Display(const UINT nFormatId, LPCTSTR parg1);
		virtual HRESULT     Display(const UINT nFormatId, HRESULT parg1);
		virtual bool        IsValid(void) const;
		virtual HRESULT     SetControlState(const DWORD);
		virtual HRESULT     SetCountInfo   (LPCTSTR pPattern, const INT nCount);
		virtual HRESULT     SetCountInfo   (const UINT nPatternId, const INT nCount);
		virtual HRESULT     SetErrorState  (LPCTSTR pDetails=NULL);
		virtual HRESULT     SetSuspendState(LPCTSTR pDetails=NULL);
		virtual HRESULT     SetWaitingState(LPCTSTR pDetails=NULL);
		virtual HRESULT     SetWorkingState(LPCTSTR pDetails=NULL);
	private:
		CDisplayingElement(const CDisplayingElement&);
		CDisplayingElement& operator= (const CDisplayingElement&);
	};
	class CDisplayingElementNone:
		public CDisplayingElement
	{
	public:
		CDisplayingElementNone(void);
		~CDisplayingElementNone(void);
	public:
		virtual HRESULT     CreateFromDialogControl(const HWND hDialog, const UINT nCtrlId) override sealed;
		virtual HRESULT     Destroy(void) override sealed;
		virtual HRESULT     Display(LPCTSTR pInfo) override sealed;
		virtual HRESULT     Display(const UINT nFormatId, LPCTSTR parg1) override sealed;
		virtual HRESULT     Display(const UINT nFormatId, HRESULT parg1) override sealed;
		virtual bool        IsValid(void) const override sealed;
	private:
		CDisplayingElementNone(const CDisplayingElementNone&);
		CDisplayingElementNone& operator= (const CDisplayingElementNone&);
	};
}}}}
#endif/*__PLATINUMCLIENTDISPLAYINGELEMENT_H_EB5D9A5A_3D65_49b7_8C08_B93D4F4AE129_INCLUDED*/