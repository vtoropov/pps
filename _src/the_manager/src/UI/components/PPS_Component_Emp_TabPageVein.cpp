/*
	Created by Tech_dog (VToropov) on 26-Mar-2014 at 8:47:52pm, GMT+4, Saint-Petersburg, Wednesday;
	This is Platinum Client Employee Record Finger Vein Tab Page class implementation file.
*/
#include "StdAfx.h"
#include "PPS_Component_Emp_TabPageVein.h"
//#include "PPS_DataProvider_CommonDefs.h"
#include "PPS_Component_DspElement.h"
#include "PPS_FVCheckDlg.h"
#include "PlatinumClient_CommonSettings.h"
#include "PlatinumClient_Resource.h"

//using namespace Platinum::client::UI;
using namespace Platinum::client::UI::common;
using namespace Platinum::client::UI::components;
using namespace Platinum::client::UI::dialogs;
using namespace Platinum::client::data;
//using namespace Platinum::client::common;

#include "Shared_GenericAppObject.h"

using namespace shared::lite::common;

extern TMgrSettings&       Global_GetMgrSettingsRef(void);
extern CDisplayingElement& Global_GetDisplayElement(void);
extern CApplication&       Global_GetAppObjectRef(void); 

#include "UIX_CommonDrawDefs.h"
#include "UIX_GdiProvider.h"

using namespace ex_ui::draw;

/////////////////////////////////////////////////////////////////////////////

namespace Platinum { namespace client { namespace UI { namespace components { namespace details
{
	void TabPage_VeinUpdateState(CVeinList& _list, const CEmployeeDataRecord& rec_ref, const CSharedObjects& obj_ref)
	{
		CWindow host_ = _list.Host();
		if (!host_)
			return;
		_list.Update(rec_ref);
		const bool bEnabled = (!rec_ref.FvData().IsEmpty() && obj_ref.Initializer().IsInitialized());
		const UINT ctrlId_[] = {
			IDC_EMPLOYEE_FV_PAGE_DATA_EDIT  ,
			IDC_EMPLOYEE_FV_PAGE_DATA_CLEAR ,
			IDC_EMPLOYEE_FV_PAGE_DATA_REMOVE,
			IDC_EMPLOYEE_FV_PAGE_DATA_NEW   ,
		//	IDC_EMPLOYEE_FV_PAGE_DATA_SIMCHK,
		};
		for (INT i_ = 0; i_ < _countof(ctrlId_); i_++)
		{
			CWindow ctrl_ = host_.GetDlgItem(ctrlId_[i_]);
			if (!ctrl_)
				continue;
			if (ctrlId_[i_] != IDC_EMPLOYEE_FV_PAGE_DATA_NEW)
				ctrl_.EnableWindow(bEnabled);
			else
				ctrl_.EnableWindow(obj_ref.Initializer().IsInitialized());
		}
	}

	INT  TabPage_VeinCheckIndex (CVeinList& _list, const CEmployeeDataRecord& rec_ref)
	{
		INT nIndex = -1;
		if (!_list.IsValid())
			AtlMessageBox(
				::GetActiveWindow(),
				_T("The unexpected error: the FV list is not created."),
				Global_GetAppObjectRef().GetName(),
				MB_ICONEXCLAMATION|MB_OK
			);
		else if (rec_ref.FvData().Count() < 1) {}
		else
		{
			nIndex = _list.SelectedItem();
			if (nIndex < 0)
				AtlMessageBox(
					::GetActiveWindow(),
					_T("No finger vein image is selected in the list."),
					Global_GetAppObjectRef().GetName(),
					MB_ICONEXCLAMATION|MB_OK
				);
		}
		return nIndex;
	}

	INT  TabPage_VeinCheckDuplicates(::ATL::CWindow& page_ref, const CEmployeeDataRecord& rec_ref, CSharedObjects& obj_ref, const INT nExcludeIndex = -1)
	{
		if (!obj_ref.MgrSettings().CheckDuplicateImages())
			return true;
		const INT nImages = rec_ref.FvData().Count();
		if (1 > nImages || (2 > nImages && 0 == nExcludeIndex))
			return true;
		_variant_t vProcessed;
		HRESULT hr_ = obj_ref.Processor().CreateVerificationTemplate(vProcessed);
		if (S_OK != hr_)
			return true;
		for ( INT i_ = 0; i_ < nImages; i_++ )
		{
			if (nExcludeIndex == i_)
				continue;
			const CEmployeeFvImage& fv_image = rec_ref.FvData().Image(i_);
			if (!fv_image.IsValid())
				continue;
			hr_ = obj_ref.Processor().VerifyMatch(vProcessed, fv_image.Data());
			if (FAILED(hr_))
				return true;
			else if (S_FALSE == hr_)
					continue;
			else
			{
				::WTL::CListBox fv_list = page_ref.GetDlgItem(IDC_EMPLOYEE_FV_PAGE_DATA_STATE);
				fv_list.SetCurSel(i_);
				AtlMessageBox(::GetActiveWindow(),
								_T("This finger is already registered. Please see the selection in the list."),
								Global_GetAppObjectRef().GetName(),
								MB_ICONEXCLAMATION|MB_OK);
				return false;
			}
		}
		return true;
	}

	class CTabPageEmpRecVein_Layout
	{
		enum {
			e_ctrl_gap_h = 10,
		};
	private:
		CWindow& m_page_ref;
		RECT     m_client_area;
	public:
		CTabPageEmpRecVein_Layout(CWindow& _page_ref) : m_page_ref(_page_ref)
		{
			if (m_page_ref)
				m_page_ref.GetClientRect(&m_client_area);
			else
				::SetRectEmpty(&m_client_area);
		}
	public:
		RECT    GetDiagramRect(const SIZE& _sz_def)const
		{
			RECT rc_lst_ = {0};
			CWindow lst_ = m_page_ref.GetDlgItem(IDC_EMPLOYEE_FV_PAGE_DATA_STATE);
			if (lst_)
			{
				lst_.GetWindowRect(&rc_lst_);
				::MapWindowPoints(HWND_DESKTOP, m_page_ref, (LPPOINT)&rc_lst_, 0x2);
			}
			RECT rc_cbx_ = {0};
			CWindow cbx_ = m_page_ref.GetDlgItem(IDC_EMPLOYEE_FV_PAGE_DATA_EXFUNC);
			if (lst_)
			{
				cbx_.GetWindowRect(&rc_cbx_);
				::MapWindowPoints(HWND_DESKTOP, m_page_ref, (LPPOINT)&rc_cbx_, 0x2);
			}

			const INT nLeft = rc_lst_.left;
			const INT nTop  = rc_cbx_.bottom + CTabPageEmpRecVein_Layout::e_ctrl_gap_h;
			RECT rc_ = {
				nLeft,
				nTop ,
				nLeft  +(_sz_def.cx ? _sz_def.cx : __W(rc_lst_)),
				nTop   + _sz_def.cy
			};
			return rc_;
		}
	};
}}}}}

/////////////////////////////////////////////////////////////////////////////

CTabPageEmpRecVein::CTabPageEmpRecVein(::WTL::CTabCtrl& ctrl_ref, ITabSetCallback& tabs_sink, CEmployeeDataRecord& rec_ref, 
									Platinum::client::manager::CSharedObjects& obj_ref) : 
	TBasePage(IDD_EMPLOYEE_DLG_TAB_VEIN, ctrl_ref, *this),
	m_tabs_sink(tabs_sink), m_record(rec_ref),
	m_objects(obj_ref)
{
}

CTabPageEmpRecVein::~CTabPageEmpRecVein(void)
{
}

/////////////////////////////////////////////////////////////////////////////

LRESULT    CTabPageEmpRecVein::TabPage__OnEvent(UINT uMsg, WPARAM wParam, LPARAM lParam, BOOL& bHandled)
{
	uMsg; wParam; lParam; bHandled = FALSE;
	switch (uMsg)
	{
	case WM_INITDIALOG:
		{
			if (TBasePage::m_bInitilized)
				break;
			m_vein_list.Subclass(*this, IDC_EMPLOYEE_FV_PAGE_DATA_STATE);
			{
				const bool bChecked  = m_objects.MgrSettings().CheckDuplicateImages();
				::WTL::CButton ctrl_ = TBasePage::GetDlgItem(IDC_EMPLOYEE_FV_PAGE_DATA_CHECK);
				if (ctrl_)
					ctrl_.SetCheck(static_cast<INT>(bChecked));
			}
			{
				HBITMAP hBitmap = NULL;
				HRESULT hr_ = CGdiPlusPngLoader::LoadResource(
								IDR_PLATINUM_MAIN_DLG_WARN_16px,
								NULL,
								hBitmap
							);
				if (!FAILED(hr_))
				{
					::WTL::CStatic ctrl_ = TBasePage::GetDlgItem(IDC_EMPLOYEE_FV_PAGE_DATA_WARN16);
					if (ctrl_)
						ctrl_.SetBitmap(hBitmap);
					::DeleteObject(hBitmap); hBitmap = NULL;
				}
			}
			details::CTabPageEmpRecVein_Layout layout_(*this);
			const SIZE sz_ = m_diagram.DefaultSize();
			const RECT rc_ = layout_.GetDiagramRect(sz_);

			m_diagram.Create(*this, rc_);

			details::TabPage_VeinUpdateState(m_vein_list, m_record, m_objects);
			bHandled = TRUE;
			TBasePage::m_bInitilized = true;
		} break;
	case WM_DESTROY:
		{
			m_vein_list.Destroy();
			m_diagram.Destroy();
		} break;
	case WM_NOTIFY:
		{
			const INT  ctrlId = static_cast<INT>(wParam);
			const NMHDR* pNotify = reinterpret_cast<NMHDR*>(lParam);
			if (false){}
			else if (IDC_EMPLOYEE_FV_PAGE_DATA_STATE == ctrlId)
			{
				if (pNotify && pNotify->code ==  LVN_ITEMCHANGED)
				{
					NMLISTVIEW* pSelected = reinterpret_cast<NMLISTVIEW*>(lParam);
					if (pSelected)
					{
						if (LVIF_STATE & pSelected->uChanged)
						{
							if (pSelected->uNewState & LVIS_FOCUSED)
							{
								const INT nIndex = details::TabPage_VeinCheckIndex(m_vein_list, m_record);
								if (LB_ERR != nIndex)
									m_diagram.SetData(m_record.FvData().Image(nIndex).Data());
								else
									m_diagram.SetData(_variant_t());
							}
						}
					}
				}
			}
		} break;
	case WM_COMMAND:
		{
			if (!TBasePage::m_bInitilized)
				break;
			bHandled = TRUE;
			const WORD wNotify = HIWORD(wParam); wNotify;
			const WORD ctrlId  = LOWORD(wParam); ctrlId;
			if (false){}
			else if (IDC_EMPLOYEE_FV_PAGE_DATA_NEW    == ctrlId)
			{
				Global_GetDisplayElement().SetWaitingState(_T("Waiting for finger vein scanning..."));
				if (!details::TabPage_VeinCheckDuplicates(*this, m_record, m_objects, -1))
				{
					Global_GetDisplayElement().SetSuspendState(_T("Ready"));
					return 0;
				}
				const bool bUseExtFunc = !!::WTL::CButton(TBasePage::GetDlgItem(IDC_EMPLOYEE_FV_PAGE_DATA_EXFUNC)).GetCheck();
				_variant_t vTemplate;
				HRESULT hr_ = S_OK;
				if (bUseExtFunc)
					hr_ = m_objects.Processor().Enroll(vTemplate);
				else
					hr_ = m_objects.Processor().CreateEnrollmentTemplate(vTemplate);

				if (S_OK == hr_)
				{
					Global_GetDisplayElement().SetWorkingState(_T("Processing vein data..."));
					if (S_OK == m_record.FvData().Add(vTemplate, NULL))
					{
						m_record.FvData().Changed(true);
						m_tabs_sink.TabSet__OnDataChanged(TBasePage::IDD, true);
						details::TabPage_VeinUpdateState(m_vein_list, m_record, m_objects);
					}
					Global_GetDisplayElement().SetSuspendState(_T("Ready"));
				}
				else
					Global_GetDisplayElement().SetErrorState(m_objects.Error().GetDescription());
			}
			else if (IDC_EMPLOYEE_FV_PAGE_DATA_EDIT   == ctrlId)
			{
				const INT nIndex = details::TabPage_VeinCheckIndex(m_vein_list, m_record);
				if (0 > nIndex)
					return 0;
				Global_GetDisplayElement().SetWaitingState(_T("Waiting for finger vein scanning..."));
				if (!details::TabPage_VeinCheckDuplicates(*this, m_record, m_objects, nIndex))
				{
					Global_GetDisplayElement().SetSuspendState(_T("Ready"));
					return 0;
				}
				const bool bUseExtFunc = !!::WTL::CButton(TBasePage::GetDlgItem(IDC_EMPLOYEE_FV_PAGE_DATA_EXFUNC)).GetCheck();
				_variant_t vTemplate;
				HRESULT hr_ = S_OK;
				if (bUseExtFunc)
					hr_ = m_objects.Processor().Enroll(vTemplate);
				else
					hr_ = m_objects.Processor().CreateEnrollmentTemplate(vTemplate);

				if (S_OK == hr_)
				{
					Global_GetDisplayElement().SetWorkingState(_T("Processing vein data..."));
					if (S_OK == m_record.FvData().Update(nIndex, vTemplate))
					{
						m_record.FvData().Changed(true);
						m_tabs_sink.TabSet__OnDataChanged(TBasePage::IDD, true);
						details::TabPage_VeinUpdateState(m_vein_list, m_record, m_objects);
						m_diagram.SetData(_variant_t());
					}
					Global_GetDisplayElement().SetSuspendState(_T("Ready"));
				}
				else
					Global_GetDisplayElement().SetErrorState(m_objects.Error().GetDescription());
			}
			else if (IDC_EMPLOYEE_FV_PAGE_DATA_REMOVE == ctrlId)
			{
				const INT nIndex = details::TabPage_VeinCheckIndex(m_vein_list, m_record);
				if (0 > nIndex)
					return 0;
				const INT nResult = AtlMessageBox(
										*this,
										_T("Do you want to remove the selected finger vein data?"),
										Global_GetAppObjectRef().GetName(),
										MB_ICONQUESTION|MB_OKCANCEL
									);
				if (IDOK == nResult)
				{
					m_record.FvData().Remove(nIndex);
					m_record.FvData().Changed(true);
					m_tabs_sink.TabSet__OnDataChanged(TBasePage::IDD, true);
					details::TabPage_VeinUpdateState(m_vein_list, m_record, m_objects);
					m_diagram.SetData(_variant_t());
				}
			}
			else if (IDC_EMPLOYEE_FV_PAGE_DATA_CLEAR  == ctrlId)
			{
				if (m_record.FvData().IsEmpty())
					return 0;
				const INT nResult = AtlMessageBox(
										*this,
										_T("Do you want to remove all finger vein data?"),
										Global_GetAppObjectRef().GetName(),
										MB_ICONQUESTION|MB_OKCANCEL
									);
				if (IDOK == nResult)
				{
					m_record.FvData().Clear();
					m_record.FvData().Changed(true);
					m_tabs_sink.TabSet__OnDataChanged(TBasePage::IDD, true);
					details::TabPage_VeinUpdateState(m_vein_list, m_record, m_objects);
					m_diagram.SetData(_variant_t());
				}
			}
			else if (IDC_EMPLOYEE_FV_PAGE_DATA_CHECK  == ctrlId)
			{
				::WTL::CButton ctrl_ = TBasePage::GetDlgItem(ctrlId);
				const bool bChecked  = (BST_CHECKED == ctrl_.GetCheck());
				m_objects.MgrSettings().CheckDuplicateImages(bChecked);
			}
			else if (IDC_EMPLOYEE_FV_PAGE_DATA_SIMCHK == ctrlId)
			{
				CEnrollDataProvider provider(m_objects);
				CFVCheckDlg dlg_(m_objects, provider);
				dlg_.DoModal(m_record);
			}
			else
				bHandled = FALSE;
			if (bHandled)
				m_tabs_sink.TabSet__OnDataChanged(TBasePage::IDD, true);
		} break;
	}
	return 0;
}

void       CTabPageEmpRecVein::UpdateLayout(void)
{
}

HRESULT    CTabPageEmpRecVein::Validate(void)const
{
	return  E_NOTIMPL;
}