/*
	Created by Tech_dog (VToropov) on 17-Mar-2016 at 10:47:08pm, GMT+7, Phuket, Rawai, Thursday;
	This is Platinum client vein image list view class implementation file.
*/
#include "StdAfx.h"
#include "PPS_Component_VeinList.h"
#include "PPS_DataProvider_CommonDefs.h"

using namespace Platinum::client::UI::components;
using namespace Platinum::client::data;

/////////////////////////////////////////////////////////////////////////////

CVeinList::CVeinList(void)
{
}

CVeinList::~CVeinList(void)
{
}

/////////////////////////////////////////////////////////////////////////////

HRESULT     CVeinList::Destroy(void)
{
	if (!m_list)
		return S_OK;
	m_list.SendMessage(WM_CLOSE);
	return S_OK;
}

HWND        CVeinList::Host(void)const
{
	if (!m_list)
		return NULL;
	else
		return m_list.GetParent();
}

bool        CVeinList::IsValid(void)const
{
	return !!m_list.IsWindow();
}

INT         CVeinList::SelectedItem(void)const
{
	if (!m_list)
		return -1;
	const INT nSelected = ListView_GetNextItem(m_list, -1, LVNI_SELECTED);
	return nSelected;
}

HRESULT     CVeinList::Subclass(HWND hParent, const UINT ctrlId)
{
	if (m_list)
		return OLE_E_INVALIDHWND;
	if (!ctrlId)
		return E_INVALIDARG;

	CWindow host_ = hParent;

	m_list = host_.GetDlgItem(static_cast<INT>(ctrlId));
	if (!m_list)
		return E_INVALIDARG;

	RECT rc_ = {0};
	m_list.GetWindowRect(&rc_);
	::MapWindowPoints(HWND_DESKTOP, host_, (LPPOINT)&rc_, 0x2);

	m_list.DestroyWindow();

	m_list.Create(
			_T("SysListView32"),
			host_,
			rc_,
			NULL,
			WS_CHILD|WS_VISIBLE|LVS_REPORT|LVS_SINGLESEL,
			WS_EX_CLIENTEDGE,
			ctrlId
		);
	if (!m_list)
		return (HRESULT_FROM_WIN32(::GetLastError()));

	const DWORD ext_style_ = LVS_EX_FULLROWSELECT
	                        |LVS_EX_LABELTIP
	                        |LVS_EX_DOUBLEBUFFER;

	ListView_SetExtendedListViewStyle((HWND)m_list, ext_style_|LVS_SHOWSELALWAYS);
	const HFONT hFont = host_.GetFont();
	m_list.SetFont(hFont);
	{
		CWindow header_ = ListView_GetHeader(m_list);
		if (header_)
			header_.SetFont(hFont);
	}

	LVCOLUMN lvc ={0};
	lvc.mask     = LVCF_FMT | LVCF_WIDTH | LVCF_TEXT | LVCF_SUBITEM;
	lvc.fmt      = LVCFMT_LEFT;
	lvc.pszText  = _T("FV ##");
	lvc.cx       = 100;
	lvc.iSubItem =   0;

	ListView_InsertColumn(m_list, 0, &lvc);

	lvc.pszText  = _T("Status");
	lvc.cx       =  60;
	lvc.iSubItem =   1;

	ListView_InsertColumn(m_list, 1, &lvc);

	lvc.pszText  = _T("Timestamp");
	lvc.cx       =  90;
	lvc.iSubItem =   2;

	ListView_InsertColumn(m_list, 2, &lvc);

	return S_OK;
}

HRESULT     CVeinList::Update(const CEmployeeDataRecord& _record)
{
	if (!m_list)
		return OLE_E_BLANK;

	::WTL::CListViewCtrl list_ = m_list;
	if (list_.GetItemCount())
		list_.DeleteAllItems();

	const CEmployeeFvData& fv_data = _record.FvData();
	const INT nImages = fv_data.Count();

	if (nImages < 1)
		list_.SetItemText(0, 0, _T("[No images]"));
	else
	{
		::ATL::CAtlString cs_buffer;

		CEmployeeDataRecord_ValidateRule validator(_record);
		for (INT i_ = 0; i_ < nImages; i_++)
		{
			cs_buffer.Format(
					_T("Finger Vein #%02d"),
					(i_ + 1)
				);
			list_.InsertItem(i_, cs_buffer);

			const CEmployeeFvImage& fv_image = fv_data.Image(i_);
			const HRESULT hr_ = ValidateVeinData(fv_image.Data());

			if (false){}
			else if (S_OK == hr_)    cs_buffer = _T("Valid");
			else if (S_FALSE == hr_) cs_buffer = _T("Empty");
			else                     cs_buffer = _T("Not valid");

			list_.SetItemText(i_, 1, cs_buffer);
			list_.SetItemText(i_, 2, fv_image.Timestamp().ValueAsText());
		}
		ListView_SetItemState (list_, nImages - 1, LVIS_SELECTED, LVIS_SELECTED);
		ListView_EnsureVisible(list_, nImages - 1, FALSE);
	}

	return S_OK;
}