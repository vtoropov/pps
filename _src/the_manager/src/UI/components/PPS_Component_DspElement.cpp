/*
	Created by Tech_dog (VToropov) on 23-Mar-2014 at 5:17:34pm, GMT+4, Saint-Petersburg, Sunday;
	This is Platinum Payroll Systems Client Application Displaying Element class implementation file.
*/
#include "StdAfx.h"
#include "PPS_Component_DspElement.h"
#include "Shared_SystemError.h"

using namespace Platinum;
using namespace Platinum::client;
using namespace Platinum::client::UI;
using namespace Platinum::client::UI::common;

CDisplayingElement::CDisplayingElement(void):
	m_bManaged(false)
{
}

CDisplayingElement::~CDisplayingElement(void)
{
	Destroy();
}

////////////////////////////////////////////////////////////////////////////

HRESULT     CDisplayingElement::CreateFromDialogControl(const HWND hDialog, const UINT nCtrlId)
{
	if (IsValid())
		return HRESULT_FROM_WIN32(ERROR_OBJECT_ALREADY_EXISTS);
	m_bManaged = false;
	m_wnd = ::GetDlgItem(hDialog, nCtrlId);
	if (!IsValid())
		return HRESULT_FROM_WIN32(::GetLastError());
	else
		return S_OK;
}

HRESULT     CDisplayingElement::Destroy(void)
{
	if (!m_bManaged)
		return S_FALSE;
	if (!IsValid())
		return S_FALSE;
	m_wnd.SendMessage(WM_CLOSE);
	return S_OK;
}

HRESULT     CDisplayingElement::Display(const UINT nFormatId, HRESULT parg1)
{
	shared::lite::common::CSysError err_(parg1);

	::ATL::CAtlString csBuffer;
	csBuffer.Format(nFormatId, err_.GetDescription());

	return Display(csBuffer.GetString());
}

HRESULT     CDisplayingElement::Display(const UINT nFormatId, LPCTSTR parg1)
{
	if (!parg1)
		return E_INVALIDARG;
	
	::ATL::CAtlString csBuffer;
	csBuffer.Format(nFormatId, parg1);

	return Display(csBuffer.GetString());
}

HRESULT     CDisplayingElement::Display(LPCTSTR pInfo)
{
	if (!IsValid())
		return OLE_E_BLANK;
	if (!pInfo)
		return S_FALSE;
	m_wnd.SetWindowText(pInfo);
	return S_OK;
}

bool        CDisplayingElement::IsValid(void) const
{
	return (!!m_wnd.IsWindow());
}

HRESULT     CDisplayingElement::SetControlState(const DWORD){ return S_OK; }

HRESULT     CDisplayingElement::SetCountInfo   (LPCTSTR pPattern, const INT nCount) { pPattern; nCount; return S_OK; }

HRESULT     CDisplayingElement::SetCountInfo   (const UINT nPatternId, const INT nCount) {nPatternId; nCount; return S_OK; }

HRESULT     CDisplayingElement::SetErrorState  (LPCTSTR pDetails) { pDetails; return S_OK; }

HRESULT     CDisplayingElement::SetSuspendState(LPCTSTR pDetails) { pDetails; return S_OK; }

HRESULT     CDisplayingElement::SetWaitingState(LPCTSTR pDetails) { pDetails; return S_OK; }

HRESULT     CDisplayingElement::SetWorkingState(LPCTSTR pDetails) { pDetails; return S_OK; }

////////////////////////////////////////////////////////////////////////////

CDisplayingElementNone::CDisplayingElementNone(void)
{
}

CDisplayingElementNone::~CDisplayingElementNone(void)
{
}

////////////////////////////////////////////////////////////////////////////

HRESULT     CDisplayingElementNone::CreateFromDialogControl(const HWND hDialog, const UINT nCtrlId)
{
	hDialog; nCtrlId;
	return S_OK;
}

HRESULT     CDisplayingElementNone::Destroy(void)
{
	return S_OK;
}

HRESULT     CDisplayingElementNone::Display(LPCTSTR pInfo)
{
	pInfo;
	return S_OK;
}

HRESULT     CDisplayingElementNone::Display(const UINT nFormatId, LPCTSTR parg1)
{
	nFormatId; parg1;
	return S_OK;
}

HRESULT     CDisplayingElementNone::Display(const UINT nFormatId, HRESULT parg1)
{
	nFormatId; parg1;
	return S_OK;
}

bool        CDisplayingElementNone::IsValid(void) const
{
	return true;
}