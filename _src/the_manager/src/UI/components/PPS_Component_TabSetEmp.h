#ifndef __PLATINUMCLIENTEMPLOYEERECORDDIALOGTABSET_H_C3F38C27_C0D3_4e01_9A2D_DE86F0525A54_INCLUDED
#define __PLATINUMCLIENTEMPLOYEERECORDDIALOGTABSET_H_C3F38C27_C0D3_4e01_9A2D_DE86F0525A54_INCLUDED
/*
	Created by Tech_dog (VToropov) on 24-Mar-2014 at 8:34:27pm, GMT+4, Saint-Petersburg, Monday;
	This is Platinum Client Employee Record Dialog Tab Set class declaration file.
*/
#include "PPS_Component_Emp_TabPageGeneral.h"
#include "PPS_Component_Emp_TabPageVein.h"
#include "PlatinumClient_SharedObjects.h"

namespace Platinum { namespace client { namespace UI { namespace components
{
	using Platinum::client::data::CEmployeeDataRecord;
	using Platinum::client::data::CEmployeeDataRecord_ValidateRule;
	using Platinum::client::manager::CSharedObjects;

	class CTabSetEmployeeRec : public ITabSetCallback
	{
		enum { ePriorRecord = 0, eCurrentRecord = 1 };
	private:
		ITabSetCallback&      m_dlg_sink;
		::WTL::CTabCtrl       m_cTabCtrl;
		CEmployeeDataRecord   m_record[2];
		CTabPageEmpRecGeneral m_tab_general;
		CTabPageEmpRecVein    m_tab_vein;
	private:
		CEmployeeDataRecord_ValidateRule m_validator;
	public:
		CTabSetEmployeeRec(ITabSetCallback& dlg_sink, CSharedObjects&);
		~CTabSetEmployeeRec(void);
	public:
		HRESULT               Create(const HWND hParent, const RECT& rcArea);
		HRESULT               Destroy(void);
		HRESULT               Initialize(const CEmployeeDataRecord& pCurrent, const bool bNewData);
		void                  UpdateLayout(void);
		void                  SetPage(const INT nIndex);
	public:
		const CTabPageEmpRecGeneral&  GeneralPage(void) const;
		      CTabPageEmpRecGeneral&  GeneralPage(void)      ;
	public:
		const CEmployeeDataRecord&    CurrentRecord(void)const;
		      CEmployeeDataRecord&    CurrentRecord(void);
		const CEmployeeDataRecord_ValidateRule& Validator(void)const;
	private: // ITabSetCallback
		virtual bool    TabSet__IsNewMode(void) override sealed;
		virtual HRESULT TabSet__OnDataChanged(const UINT pageId, const bool bChanged) override sealed;
	};
}}}}

#endif/*__PLATINUMCLIENTEMPLOYEERECORDDIALOGTABSET_H_C3F38C27_C0D3_4e01_9A2D_DE86F0525A54_INCLUDED*/