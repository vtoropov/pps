/*
	Created by Tech_dog (VToropov) on 24-Mar-2014 at 3:30:36pm, GMT+4, Saint-Petersburg, Monday;
	This is Platinum Client UI ListView Control Wrapper class implementation file.
*/
#include "StdAfx.h"
#include "PPS_ListView_Wrap.h"

using namespace Platinum;
using namespace Platinum::client;
using namespace Platinum::client::UI;
using namespace Platinum::client::UI::common;

////////////////////////////////////////////////////////////////////////////

CListViewWrap::CListViewWrap(::ATL::CWindow& lst_ref) : m_list(lst_ref)
{
	ATLASSERT(m_list.IsWindow());
}

CListViewWrap::~CListViewWrap(void)
{
}

////////////////////////////////////////////////////////////////////////////

CAtlString    CListViewWrap::ActiveCell(const INT nCol)const
{
	const INT nRow = this->SelectedRow();
	return this->Cell(nRow, nCol);
}

HRESULT       CListViewWrap::AddColumns(const RawListViewColumnSpecItem* pSpec, const INT nItemCount)
{
	if (!pSpec || nItemCount < 1)
		return E_INVALIDARG;
	if (!m_list.IsWindow())
		return OLE_E_BLANK;
	for (INT i_ = 0; i_ < nItemCount; i_++)
	{
		const RawListViewColumnSpecItem& col_fmt = pSpec[i_];
		this->AppendColumn(col_fmt);
	}
	return S_OK;
}

HRESULT       CListViewWrap::AddColumns(const ::std::vector<RawListViewColumnSpecItem>& spec_ref)
{
	HRESULT hr_ = this->AddColumns(&spec_ref.front(), (INT)spec_ref.size());
	return  hr_;
}

HRESULT       CListViewWrap::AddRow(INT& newRowIndex, const bool bEnsureVisible)
{
	if (!m_list.IsWindow())
		return OLE_E_BLANK;
	const INT row__ = ListView_GetItemCount(m_list);
	LVITEM lvi   = {0};
	lvi.mask     = LVIF_TEXT;
	lvi.iItem    = row__;
	lvi.pszText  = _T("");
	if ( -1 == ListView_InsertItem(m_list, &lvi))
			return E_OUTOFMEMORY;
	newRowIndex = row__;
	if (bEnsureVisible)
		ListView_EnsureVisible(m_list, row__, FALSE);
	return S_OK;
}

HRESULT       CListViewWrap::AppendColumn(const RawListViewColumnSpecItem& item_ref)
{
	if (!m_list.IsWindow())
		return OLE_E_BLANK;
	const INT nIndex = this->Columns();
	::ATL::CAtlString cs_title(item_ref.csTitle);
	LVCOLUMN lvc = {0};
	lvc.mask     = LVCF_FMT | LVCF_WIDTH | LVCF_TEXT | LVCF_SUBITEM;
	lvc.fmt      = item_ref.dwAlign;
	lvc.pszText  = cs_title.GetBuffer();
	lvc.cx       = item_ref.nWidth;
	ListView_InsertColumn(m_list, nIndex, &lvc);
	return S_OK;
}

CAtlString    CListViewWrap::Cell(const INT nRow, const INT nCol)const
{
	::ATL::CAtlString cs_cell;
	if (m_list.IsWindow())
	{
		m_list.GetItemText(nRow, nCol, cs_cell);
	}
	return cs_cell;
}

HRESULT       CListViewWrap::Cell(const INT nRow, const INT nCol, LPCTSTR pText)
{
	const BOOL bResult = m_list.SetItemText(nRow, nCol, pText);
	return (bResult ? S_OK : S_FALSE);
}

INT           CListViewWrap::Columns(void)const
{
	if (!m_list.IsWindow())
		return 0;
	else
		return Header_GetItemCount(ListView_GetHeader(m_list));
}

HRESULT       CListViewWrap::Configure(void)
{
	if (!m_list.IsWindow())
		return OLE_E_BLANK;
	const LONG_PTR style__ = ::GetWindowLongPtr(m_list, GWL_STYLE);
	::SetWindowLongPtr(m_list, GWL_STYLE, style__ | LVS_SHOWSELALWAYS | LVS_SINGLESEL);
	ListView_SetExtendedListViewStyle(m_list, LVS_EX_FULLROWSELECT|LVS_EX_DOUBLEBUFFER);
	::ATL::CWindow parent = m_list.GetParent();
	const HFONT hFont = parent.GetFont();
	m_list.SetFont(hFont);
	return S_OK;
}

HRESULT       CListViewWrap::RemoveAllRows(void)
{
	if (!m_list.IsWindow())
		return OLE_E_BLANK;
	const BOOL bResult = ListView_DeleteAllItems(m_list);
	return (bResult ? S_OK : S_FALSE);
}

HRESULT       CListViewWrap::RemoveCurrent(void)
{
	const INT nIndex = this->SelectedRow();
	if (-1 == nIndex)
		return DISP_E_BADINDEX;
	const BOOL bResult = ListView_DeleteItem(m_list, nIndex);
	if (bResult)
	{
		if (false){}
		else if (nIndex < this->RowsCount())
			this->SelectedRow(nIndex);
		else if (nIndex - 1 > -1)
			this->SelectedRow(nIndex - 1);
	}
	return (bResult ? S_OK : E_FAIL);
}

INT           CListViewWrap::Rows(void)const
{
	if (m_list.IsWindow())
		return ListView_GetItemCount(m_list);
	else
		return 0;
}

INT           CListViewWrap::RowsCount(void)const
{
	if (!m_list.IsWindow())
		return 0;
	const INT nCount = ListView_GetItemCount(m_list);
	return nCount;
}

INT           CListViewWrap::SelectedRow(void)const
{
	INT nIndex = -1;
	if (m_list.IsWindow())
		nIndex = ListView_GetSelectionMark(m_list);
	return nIndex;
}

HRESULT       CListViewWrap::SelectedRow(const INT nRow)
{
	if (!m_list.IsWindow())
		return OLE_E_BLANK;
	if (0 > nRow || (this->RowsCount() - 1) < nRow)
		return DISP_E_BADINDEX;
	ListView_SetItemState(m_list, nRow, LVIS_SELECTED, LVIS_SELECTED);
	ListView_EnsureVisible(m_list, nRow, FALSE);
	return S_OK;
}

////////////////////////////////////////////////////////////////////////////

CListViewHeaderWrap::CListViewHeaderWrap(::ATL::CWindow& lst_ref) : m_list(lst_ref)
{
}

CListViewHeaderWrap::~CListViewHeaderWrap(void)
{
}

////////////////////////////////////////////////////////////////////////////

VOID          CListViewHeaderWrap::ClearAll(void)
{
	::WTL::CHeaderCtrl  header = ListView_GetHeader(m_list);
	const INT nCount =  header.GetItemCount();

	for (INT i_ = 0; i_ < nCount; i_++)
		this->SortOrder(i_, eListViewSort::eNone);
}

bool          CListViewHeaderWrap::IsValid(void)const
{
	::WTL::CHeaderCtrl  header = ListView_GetHeader(m_list);
	return !!header.IsWindow();
}

eListViewSort::_e
              CListViewHeaderWrap::SortOrder(const INT nIndex) const
{
	::WTL::CHeaderCtrl  header = ListView_GetHeader(m_list);

	HDITEM hItem = {0};
	hItem.mask = HDI_FORMAT;

	if (!header.GetItem(nIndex, &hItem))
		return eListViewSort::eNone;
	else
	{
		if (false){}
		else if (HDF_SORTUP   & hItem.fmt) return eListViewSort::eAscending;
		else if (HDF_SORTDOWN & hItem.fmt) return eListViewSort::eDescending;
		else                               return eListViewSort::eNone;
	}
}

HRESULT       CListViewHeaderWrap::SortOrder(const INT nIndex, const eListViewSort::_e eSortOrder)
{
	::WTL::CHeaderCtrl  header = ListView_GetHeader(m_list);

	HDITEM hItem = {0};
	hItem.mask = HDI_FORMAT;

	if (!header.GetItem(nIndex, &hItem))
		return DISP_E_BADINDEX;

	switch (eSortOrder)
	{
	case eListViewSort::eAscending:
		{
			hItem.fmt &= ~HDF_SORTDOWN;
			hItem.fmt |=  HDF_SORTUP;
		} break;
	case eListViewSort::eDescending:
		{
			hItem.fmt &= ~HDF_SORTUP;
			hItem.fmt |=  HDF_SORTDOWN;
		} break;
	default:
			hItem.fmt &= ~HDF_SORTDOWN;
			hItem.fmt &= ~HDF_SORTUP;
	}
	return (!header.SetItem(nIndex, &hItem) ? E_FAIL : S_OK);
}

////////////////////////////////////////////////////////////////////////////

namespace Platinum { namespace client { namespace UI { namespace common { namespace details
{
	struct ListViewSorter_CompareData
	{
		HWND   _list;
		eListViewSort::_e _sort;
		INT    _col;

		ListViewSorter_CompareData(void) : _list(NULL), _sort(eListViewSort::eNone), _col(-1){}
		ListViewSorter_CompareData(const HWND hList, const eListViewSort::_e eSort, const INT nCol) : _list(hList), _sort(eSort), _col(nCol){}
	};

	static INT CALLBACK ListViewSorter_CompareFunc(LPARAM lParam1, LPARAM lParam2, LPARAM lParamSort)
	{
		const ListViewSorter_CompareData* pData = reinterpret_cast<ListViewSorter_CompareData*>(lParamSort);
		if (!pData)
			return 0;
		static const INT nLen = 255;

		TCHAR pszItem_1[nLen] = {0};
		TCHAR pszItem_2[nLen] = {0};

		ListView_GetItemText(pData->_list, static_cast<INT>(lParam1), pData->_col, pszItem_1, sizeof(pszItem_1));
		ListView_GetItemText(pData->_list, static_cast<INT>(lParam2), pData->_col, pszItem_2, sizeof(pszItem_2));

		const INT nResult = ::_tcscmp(pszItem_1, pszItem_2);

		return (nResult && eListViewSort::eDescending == pData->_sort ? -nResult : nResult);
	}
}}}}}

////////////////////////////////////////////////////////////////////////////

CListViewSorter::CListViewSorter(const CSharedObjects& shared_ref, ::ATL::CWindow& lst_ref) : m_last(-1), m_shared(shared_ref), m_header(lst_ref), m_list(lst_ref)
{
}

CListViewSorter::~CListViewSorter(void)
{
}

////////////////////////////////////////////////////////////////////////////

HRESULT       CListViewSorter::Sort(const INT nIndex)
{
	HRESULT hr_ = S_OK;
	if (nIndex != m_last && -1 != m_last)
		m_header.SortOrder(m_last, eListViewSort::eNone);

	eListViewSort::_e eCurrent = m_header.SortOrder(nIndex);

	if (false){}
	else if (eListViewSort::eNone == eCurrent) eCurrent = eListViewSort::eAscending;
	else if (eListViewSort::eAscending == eCurrent) eCurrent = eListViewSort::eDescending;
	else if (eListViewSort::eDescending == eCurrent) eCurrent = eListViewSort::eAscending;

	m_header.SortOrder(nIndex, eCurrent);

	details::ListViewSorter_CompareData cmp_data(m_list, eCurrent, nIndex);

	ListView_SortItemsEx(m_list, details::ListViewSorter_CompareFunc, reinterpret_cast<LPARAM>(&cmp_data));

	m_last = nIndex;

	return  hr_;
}