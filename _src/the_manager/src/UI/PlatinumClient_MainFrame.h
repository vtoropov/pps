#ifndef __PLATINUMCLIENTMAINFRAME_H_71502B8F_7B1B_488f_B7E2_BBD4D3DFD307_INCLUDED
#define __PLATINUMCLIENTMAINFRAME_H_71502B8F_7B1B_488f_B7E2_BBD4D3DFD307_INCLUDED
/*
	Created by Tech_dog (VToropov) on 23-Mar-2014 at 6:45:49am, GMT+4, Saint-Petersburg Region,
	Rail Road Train #43, Coatch #6, Place #1, Sunday;
	This is Platinum Payroll Systems Client Main Frame class declaration file.
*/
#include "PPS_Component_StatusBarEx.h"
#include "PPS_Component_TabSetMain.h" 
#include "PlatinumClient_SharedObjects.h"
#include "UIX_ImgHeader.h"

namespace Platinum { namespace client { namespace UI
{
	using Platinum::client::manager::CSharedObjects;
	using Platinum::client::UI::components::CStatusBar;
	using Platinum::client::UI::components::CStatusBarDisplayElement;
	using Platinum::client::UI::components::CTabSet;

	using ex_ui::frames::CImageHeader;

	class CMainFrame
	{
	private:
		class CMainDialogImpl : public ::ATL::CDialogImpl<CMainDialogImpl>
		{
			typedef ::ATL::CDialogImpl<CMainDialogImpl> TBaseDlg;
		private:
			CStatusBar               m_status;
			CImageHeader             m_header;
			CTabSet                  m_tabset;
			CStatusBarDisplayElement m_dsp_el;
			CSharedObjects&          m_objects;
		public:
			UINT IDD;
		public:
			BEGIN_MSG_MAP(CMainDialogImpl)
				MESSAGE_HANDLER     (WM_DESTROY,      OnDestroy   )
				MESSAGE_HANDLER     (WM_DRAWITEM,     OnOwnerDraw )
				MESSAGE_HANDLER     (WM_INITDIALOG,   OnInitDialog)
				MESSAGE_HANDLER     (WM_SYSCOMMAND,   OnSysCommand)		
				NOTIFY_CODE_HANDLER (TCN_SELCHANGE,   OnTabNotify )
			END_MSG_MAP()
		public:
			CMainDialogImpl(CSharedObjects&);
			~CMainDialogImpl(void);
		private:
			LRESULT OnClose     (WORD wNotifyCode, WORD wID, HWND hWndCtl, BOOL& bHandled);
			LRESULT OnDestroy   (UINT uMsg, WPARAM wParam, LPARAM lParam,  BOOL& bHandled);
			LRESULT OnEraseBknd (UINT uMsg, WPARAM wParam, LPARAM lParam,  BOOL& bHandled);
			LRESULT OnInitDialog(UINT uMsg, WPARAM wParam, LPARAM lParam,  BOOL& bHandled);
			LRESULT OnOwnerDraw (UINT uMsg, WPARAM wParam, LPARAM lParam,  BOOL& bHandled);
			LRESULT OnSysCommand(UINT uMsg, WPARAM wParam, LPARAM lParam,  BOOL& bHandled);
			LRESULT OnTabNotify (INT idCtrl, LPNMHDR pnmh, BOOL& bHandled);
		};
	private:
		CMainFrame::CMainDialogImpl  m_dlg;
	public:
		CMainFrame(CSharedObjects&);
		~CMainFrame(void);
	public:
		HRESULT    DoModal(void);
	private:
		CMainFrame(const CMainFrame&);
		CMainFrame& operator= (const CMainFrame&);
	};
}}}

#endif/*__PLATINUMCLIENTMAINFRAME_H_71502B8F_7B1B_488f_B7E2_BBD4D3DFD307_INCLUDED*/