/*
	Created by Tech_dog (VToropov) on 24-Mar-2014 at 7:54:08pm, GMT+4, Saint-Petersburg, Monday;
	This is Platinum Client Employee Record Dialog class implementation file.
*/
#include "StdAfx.h"
#include "PPS_EmployeeRecordDlg.h"
#include "PlatinumClient_Resource.h"

using namespace Platinum::client::UI::dialogs;
using namespace Platinum::client::UI::components;

#include "Shared_GenericAppObject.h"

extern shared::lite::common::CApplication& Global_GetAppObjectRef(void); 
////////////////////////////////////////////////////////////////////////////

namespace Platinum { namespace client { namespace UI { namespace dialogs { namespace details
{
	class CEmployeeRecordDlg_Layout
	{
	private:
		::ATL::CWindow&      m_dlg_ref;
		RECT                 m_client_area;
	public:
		CEmployeeRecordDlg_Layout(::ATL::CWindow& dlg_ref) : m_dlg_ref(dlg_ref)
		{
			if (!m_dlg_ref.GetClientRect(&m_client_area))
			{
				::SetRectEmpty(&m_client_area);
			}
		}
		~CEmployeeRecordDlg_Layout(void){}
	public:
		VOID                 RecalcPosition(void)
		{
			RECT rcDialog = {0};
			m_dlg_ref.GetWindowRect(&rcDialog);
			const RECT rcScreen = CEmployeeRecordDlg_Layout::GetAvailableArea();
			const INT n_wnd_delta = __H(rcDialog) - __H(rcScreen);
			if (0 < n_wnd_delta)
			{
				::ATL::CWindow btn_okay = m_dlg_ref.GetDlgItem(IDOK);
				::ATL::CWindow btn_cancel = m_dlg_ref.GetDlgItem(IDCANCEL);

				RECT rc_okay = {0};
				RECT rc_cancel = {0};

				btn_okay.GetWindowRect(&rc_okay);     ::MapWindowPoints(HWND_DESKTOP, m_dlg_ref, reinterpret_cast<LPPOINT>(&rc_okay), 0x2);
				btn_cancel.GetWindowRect(&rc_cancel); ::MapWindowPoints(HWND_DESKTOP, m_dlg_ref, reinterpret_cast<LPPOINT>(&rc_cancel), 0x2);

				const INT n_btn_delta = m_client_area.bottom - rc_okay.top;
				const INT n_btn_height = __H(rc_okay);

				rcDialog.bottom -= n_wnd_delta;
				m_dlg_ref.SetWindowPos(NULL, &rcDialog, SWP_NOACTIVATE|SWP_NOZORDER|SWP_NOMOVE);
				m_dlg_ref.GetClientRect(&m_client_area);

				rc_okay.top = m_client_area.bottom - n_btn_delta;
				rc_okay.bottom = rc_okay.top + n_btn_height;

				rc_cancel.top = rc_okay.top;
				rc_cancel.bottom = rc_okay.bottom;

				btn_okay.SetWindowPos(NULL, &rc_okay, SWP_NOACTIVATE|SWP_NOZORDER|SWP_NOSIZE);
				btn_cancel.SetWindowPos(NULL, &rc_cancel, SWP_NOACTIVATE|SWP_NOZORDER|SWP_NOSIZE);
			}
			m_dlg_ref.CenterWindow();
		}

		RECT                 RecalcTabArea(const SIZE& szHeader)
		{
			RECT rcTab = m_client_area;
			rcTab.top += szHeader.cy;
			::ATL::CWindow btn = m_dlg_ref.GetDlgItem(IDOK);
			if (btn)
			{
				RECT rc_btn = {0};
				if (btn.GetWindowRect(&rc_btn))
				{
					::MapWindowPoints(HWND_DESKTOP, m_dlg_ref, (LPPOINT)&rc_btn, 0x2);
					rcTab.bottom = rc_btn.top - 5;
				}
			}
			return rcTab;
		}
	public:
		static RECT          GetAvailableArea(void)
		{
			const POINT ptZero = {0};
			const HMONITOR hMonitor = ::MonitorFromPoint(ptZero, MONITOR_DEFAULTTOPRIMARY);
			MONITORINFO mInfo  = {0};
			mInfo.cbSize = sizeof(MONITORINFO);
			::GetMonitorInfo(hMonitor, &mInfo);
			return mInfo.rcWork;
		}
	};
}}}}}

////////////////////////////////////////////////////////////////////////////

CEmployeeRecordDlg::CEmployeeRecordDlgImpl::CEmployeeRecordDlgImpl(CSharedObjects& obj_ref) : 
	IDD(IDD_EMPLOYEE_DLG),
	m_header(IDR_EMPLOYEE_DLG_HEADER),
	m_tabset(*this, obj_ref),
	m_bNewData(true)
{
}

CEmployeeRecordDlg::CEmployeeRecordDlgImpl::~CEmployeeRecordDlgImpl(void)
{
}

////////////////////////////////////////////////////////////////////////////

LRESULT CEmployeeRecordDlg::CEmployeeRecordDlgImpl::OnConfirm   (WORD wNotifyCode, WORD wID, HWND hWndCtl, BOOL& bHandled)
{
	wNotifyCode; wID; hWndCtl; bHandled;
	HRESULT hr_ = m_tabset.Validator().Validate();
	if (S_OK != hr_)
	{
		AtlMessageBox(
				TBaseDlg::m_hWnd,
				m_tabset.Validator().Details(),
				Global_GetAppObjectRef().GetName(),
				MB_ICONEXCLAMATION|MB_OK
			);
		m_tabset.SetPage(0);
	}
	else
	{
		hr_ = m_tabset.Validator().ValidateVein();
		if (S_FALSE == hr_) // no vein data
		{
			const INT nResult = AtlMessageBox(
									TBaseDlg::m_hWnd,
									_T("Do you want to proceed without vein data?"), 
									Global_GetAppObjectRef().GetName(),
									MB_ICONQUESTION|MB_OKCANCEL
								);
			if (IDOK != nResult) // a user cancels the exiting
			{
				m_tabset.SetPage(1);
				return 0;
			}
		}
		if (FAILED(hr_)) // the vein data exists but something is wrong
		{
			AtlMessageBox(
					TBaseDlg::m_hWnd,
					m_tabset.Validator().Details(),
					Global_GetAppObjectRef().GetName(),
					MB_ICONEXCLAMATION|MB_OK
				);
			m_tabset.SetPage(1);
			return 0;
		}
		TBaseDlg::EndDialog(IDOK);
	}
	return 0;
}

LRESULT CEmployeeRecordDlg::CEmployeeRecordDlgImpl::OnDestroy   (UINT uMsg, WPARAM wParam, LPARAM lParam,  BOOL& bHandled)
{
	uMsg; wParam; lParam; bHandled = FALSE;
	m_tabset.Destroy();
	m_header.Destroy();
	return 0;
}

LRESULT CEmployeeRecordDlg::CEmployeeRecordDlgImpl::OnDismiss   (WORD wNotifyCode, WORD wID, HWND hWndCtl, BOOL& bHandled)
{
	wNotifyCode; wID; hWndCtl; bHandled;
	TBaseDlg::EndDialog(IDCANCEL);
	return 0;
}

LRESULT CEmployeeRecordDlg::CEmployeeRecordDlgImpl::OnInitDialog(UINT uMsg, WPARAM wParam, LPARAM lParam,  BOOL& bHandled)
{
	uMsg; wParam; lParam; bHandled;
	{
		shared::lite::common::CApplicationIconLoader loader(IDR_PLATINUM_MAIN_DLG_ICON);
		TBaseDlg::SetIcon(loader.DetachBigIcon(), TRUE);
		TBaseDlg::SetIcon(loader.DetachSmallIcon(), FALSE);
	}
	details::CEmployeeRecordDlg_Layout layout(*this);
	{
		::ATL::CAtlString cs_title(_T("Employee"));
		if (this->m_bNewData)
			cs_title += _T(" [New]");
		TBaseDlg::SetWindowText(cs_title.GetString());
	}
	HRESULT hr_ = S_OK;
	{
		hr_ = m_header.Create(TBaseDlg::m_hWnd);
	}
	layout.RecalcPosition();
	{
		const RECT rcTab = layout.RecalcTabArea(m_header.GetSize());
		hr_ = m_tabset.Create(TBaseDlg::m_hWnd, rcTab);
	}
	return 0;
}

LRESULT CEmployeeRecordDlg::CEmployeeRecordDlgImpl::OnSysCommand(UINT uMsg, WPARAM wParam, LPARAM lParam,  BOOL& bHandled)
{
	uMsg; wParam; lParam; bHandled = FALSE;
	switch (wParam)
	{
	case SC_CLOSE:
		{
			EndDialog(IDCANCEL);
		} break;
	}
	return 0;
}

LRESULT CEmployeeRecordDlg::CEmployeeRecordDlgImpl::OnTabNotify (INT idCtrl, LPNMHDR pnmh, BOOL& bHandled)
{
	idCtrl; pnmh; bHandled = TRUE;
	m_tabset.UpdateLayout();
	return 0;
}

////////////////////////////////////////////////////////////////////////////

HRESULT CEmployeeRecordDlg::CEmployeeRecordDlgImpl::Initialize(CEmployeeDataRecord& rec_ref, const bool bNewData)
{
	this->m_bNewData = bNewData;
	this->m_tabset.Initialize(rec_ref, bNewData);
	return S_OK;
}

CTabSetEmployeeRec& CEmployeeRecordDlg::CEmployeeRecordDlgImpl::Tabs(void)
{
	return m_tabset;
}

////////////////////////////////////////////////////////////////////////////

bool    CEmployeeRecordDlg::CEmployeeRecordDlgImpl::TabSet__IsNewMode(void)
{
	return this->m_bNewData;
}

HRESULT CEmployeeRecordDlg::CEmployeeRecordDlgImpl::TabSet__OnDataChanged(const UINT pageId, const bool bChanged)
{
	pageId;
	::ATL::CWindow confirm = TBaseDlg::GetDlgItem(IDOK);
	confirm.EnableWindow((BOOL)bChanged);
	return S_OK;
}

HRESULT CEmployeeRecordDlg::CEmployeeRecordDlgImpl::TabSet__OnDataComplete(const UINT pageId)
{
	pageId;
	return S_OK;
}

////////////////////////////////////////////////////////////////////////////

CEmployeeRecordDlg::CEmployeeRecordDlg(CSharedObjects& obj_ref) : m_dlg(obj_ref)
{
}

CEmployeeRecordDlg::~CEmployeeRecordDlg(void)
{
}

////////////////////////////////////////////////////////////////////////////

HRESULT    CEmployeeRecordDlg::DoModal(CEmployeeDataRecord& rec_ref, const bool bNewData)
{
	m_dlg.Initialize(rec_ref, bNewData);
	const INT_PTR result = m_dlg.DoModal();
	if (IDOK == result)
		rec_ref = m_dlg.Tabs().CurrentRecord();
	return (result == IDCANCEL ? S_FALSE : S_OK);
}