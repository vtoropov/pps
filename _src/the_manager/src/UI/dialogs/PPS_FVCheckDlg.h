#ifndef _PPSFVCHECK_H_81742F57_B836_46d5_AC3F_CF49D4E46D29_INCLUDED
#define _PPSFVCHECK_H_81742F57_B836_46d5_AC3F_CF49D4E46D29_INCLUDED
/*
	Created by Tech_dog (VToropov) on 16-Mar-2016 at 2:03:33pm, GMT+7, Phuket, Rawai, Wednesday;
	This is Platinum Client Finger Vein Check Dialog class declaration file.
*/
#include "PlatinumClient_SharedObjects.h"
#include "PPS_ActionToCheckFV.h"
#include "PPS_DataProvider_4_Enroll.h"

namespace Platinum { namespace client { namespace UI { namespace dialogs
{
	using Platinum::client::data::CEmployeeDataRecord;
	using Platinum::client::data::CEnrollDataProvider;

	using Platinum::client::manager::CSharedObjects;

	using Platinum::client::ControlFlow::CActionToCheckFv;

	class CFVCheckDlg
	{
	private:
		class CFVCheckDlgImpl :
			public  ::ATL::CDialogImpl<CFVCheckDlgImpl>
		{
			typedef ::ATL::CDialogImpl<CFVCheckDlgImpl> TBaseDlg;
		private:
			CActionToCheckFv      m_action;
			CEmployeeDataRecord   m_record;
			INT                   m_fvIndex;
		public:
			UINT IDD;
		public:
			BEGIN_MSG_MAP(CFVCheckDlgImpl)
				MESSAGE_HANDLER     (WM_DESTROY   ,   OnDestroy   )
				MESSAGE_HANDLER     (WM_INITDIALOG,   OnInitDialog)
				MESSAGE_HANDLER     (WM_SYSCOMMAND,   OnSysCommand)
				COMMAND_ID_HANDLER  (IDOK         ,   OnConfirm   )
				COMMAND_ID_HANDLER  (IDCANCEL     ,   OnDismiss   )
			END_MSG_MAP()
		public:
			CFVCheckDlgImpl(CSharedObjects&, CEnrollDataProvider&);
			~CFVCheckDlgImpl(void);
		private:
			LRESULT OnConfirm   (WORD wNotifyCode, WORD wID     , HWND hWndCtl , BOOL& bHandled);
			LRESULT OnDestroy   (UINT uMsg       , WPARAM wParam, LPARAM lParam, BOOL& bHandled);
			LRESULT OnDismiss   (WORD wNotifyCode, WORD wID     , HWND hWndCtl , BOOL& bHandled);
			LRESULT OnInitDialog(UINT uMsg       , WPARAM wParam, LPARAM lParam, BOOL& bHandled);
			LRESULT OnSysCommand(UINT uMsg       , WPARAM wParam, LPARAM lParam, BOOL& bHandled);
		public:
			VOID    Initialize(const CEmployeeDataRecord&, const INT nFvIndex);
		};
	private:
		CFVCheckDlgImpl   m_dlg;
	public:
		CFVCheckDlg(CSharedObjects&, CEnrollDataProvider&);
		~CFVCheckDlg(void);
	public:
		HRESULT    DoModal(const CEmployeeDataRecord&, const INT nFvIndex = -1);
	private:
		CFVCheckDlg(const CFVCheckDlg&);
		CFVCheckDlg& operator= (const CFVCheckDlg&);
	};
}}}}

#endif/*_PPSFVCHECK_H_81742F57_B836_46d5_AC3F_CF49D4E46D29_INCLUDED*/