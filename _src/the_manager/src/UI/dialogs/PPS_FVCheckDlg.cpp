/*
	Created by Tech_dog (VToropov) on 16-Mar-2016 at 2:32:56pm, GMT+7, Phuket, Rawai, Wednesday;
	This is Platinum Client Finger Vein Check Dialog class implementation file.
*/
#include "StdAfx.h"
#include "PPS_FVCheckDlg.h"
#include "PlatinumClient_Resource.h"

using namespace Platinum::client::UI::dialogs;

#include "Shared_GenericAppObject.h"

/////////////////////////////////////////////////////////////////////////////

CFVCheckDlg::CFVCheckDlgImpl::CFVCheckDlgImpl(CSharedObjects& _shared, CEnrollDataProvider& _provider): 
	IDD(IDD_PLATINUM_SIMCHECK_DLG),
	m_fvIndex(-1),
	m_record (false),
	m_action (_shared, _provider)
{
}

CFVCheckDlg::CFVCheckDlgImpl::~CFVCheckDlgImpl(void)
{
}

/////////////////////////////////////////////////////////////////////////////

LRESULT CFVCheckDlg::CFVCheckDlgImpl::OnConfirm   (WORD     , WORD wID     , HWND hWndCtl , BOOL& bHandled)
{
	wID; hWndCtl; bHandled;
	return 0;
}

LRESULT CFVCheckDlg::CFVCheckDlgImpl::OnDestroy   (UINT uMsg, WPARAM wParam, LPARAM lParam, BOOL& bHandled)
{
	uMsg; wParam; lParam; bHandled;
	if (m_action.IsRunning())
		m_action.Stop();
	return 0;
}

LRESULT CFVCheckDlg::CFVCheckDlgImpl::OnDismiss   (WORD     , WORD wID     , HWND hWndCtl , BOOL& bHandled)
{
	wID; hWndCtl; bHandled;
	return 0;
}

LRESULT CFVCheckDlg::CFVCheckDlgImpl::OnInitDialog(UINT uMsg, WPARAM wParam, LPARAM lParam, BOOL& bHandled)
{
	uMsg; wParam; lParam; bHandled;
	TBaseDlg::CenterWindow();
	{
		shared::lite::common::CApplicationIconLoader loader(IDR_PLATINUM_MAIN_DLG_ICON);
		TBaseDlg::SetIcon(loader.DetachBigIcon(), TRUE);
		TBaseDlg::SetIcon(loader.DetachSmallIcon(), FALSE);
	}
	m_action.Start(m_record, m_fvIndex);
	return 0;
}

LRESULT CFVCheckDlg::CFVCheckDlgImpl::OnSysCommand(UINT uMsg, WPARAM wParam, LPARAM lParam, BOOL& bHandled)
{
	uMsg; wParam; lParam; bHandled = FALSE;
	switch (wParam)
	{
	case SC_CLOSE:
		{
			EndDialog(IDCANCEL);
		} break;
	}
	return 0;
}

/////////////////////////////////////////////////////////////////////////////

VOID    CFVCheckDlg::CFVCheckDlgImpl::Initialize(const CEmployeeDataRecord& _record, const INT nFvIndex)
{
	m_record = _record;
	m_fvIndex = nFvIndex;
}

/////////////////////////////////////////////////////////////////////////////

CFVCheckDlg::CFVCheckDlg(CSharedObjects& _shared, CEnrollDataProvider& _provider) : m_dlg(_shared, _provider)
{
}

CFVCheckDlg::~CFVCheckDlg(void)
{
}

/////////////////////////////////////////////////////////////////////////////

HRESULT    CFVCheckDlg::DoModal(const CEmployeeDataRecord& _record, const INT nFvIndex)
{
	m_dlg.Initialize(_record, nFvIndex);

	const INT_PTR result = m_dlg.DoModal();

	return (result == IDCANCEL ? S_FALSE : S_OK);
}