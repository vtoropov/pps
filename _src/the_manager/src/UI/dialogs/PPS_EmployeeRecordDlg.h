#ifndef __PLATINUMCLIENTEMPLOYEERECORDDIALOG_H_D46011DF_5524_4e42_BF27_B25731D51EAC_INCLUDED
#define __PLATINUMCLIENTEMPLOYEERECORDDIALOG_H_D46011DF_5524_4e42_BF27_B25731D51EAC_INCLUDED
/*
	Created by Tech_dog (VToropov) on 24-Mar-2014 at 6:15:36pm, GMT+4, Saint-Petersburg, Monday;
	This is Platinum Client Employee Record Dialog class declaration file.
*/
#include "UIX_ImgHeader.h"
#include "PPS_Component_TabSetEmp.h"
#include "PlatinumClient_SharedObjects.h"

namespace Platinum { namespace client { namespace UI { namespace dialogs
{
	using Platinum::client::UI::components::CTabSetEmployeeRec;
	using Platinum::client::data::CEmployeeDataRecord;
	using Platinum::client::manager::CSharedObjects;

	using ex_ui::frames::CImageHeader;

	class CEmployeeRecordDlg
	{
	private:
		class CEmployeeRecordDlgImpl :
			public ::ATL::CDialogImpl<CEmployeeRecordDlgImpl>,
			public Platinum::client::UI::components::ITabSetCallback
		{
			typedef ::ATL::CDialogImpl<CEmployeeRecordDlgImpl> TBaseDlg;
		private:
			bool                     m_bNewData;
			CImageHeader             m_header;
			CTabSetEmployeeRec       m_tabset;
		public:
			UINT IDD;
		public:
			BEGIN_MSG_MAP(CEmployeeRecordDlgImpl)
				MESSAGE_HANDLER     (WM_DESTROY   ,   OnDestroy   )
				MESSAGE_HANDLER     (WM_INITDIALOG,   OnInitDialog)
				MESSAGE_HANDLER     (WM_SYSCOMMAND,   OnSysCommand)
				COMMAND_ID_HANDLER  (IDOK         ,   OnConfirm   )
				COMMAND_ID_HANDLER  (IDCANCEL     ,   OnDismiss   )
				NOTIFY_CODE_HANDLER (TCN_SELCHANGE,   OnTabNotify )
			END_MSG_MAP()
		public:
			CEmployeeRecordDlgImpl(CSharedObjects&);
			~CEmployeeRecordDlgImpl(void);
		public:
			HRESULT             Initialize(CEmployeeDataRecord&, const bool bNewData);
			CTabSetEmployeeRec& Tabs(void);
		private:
			LRESULT OnConfirm   (WORD wNotifyCode, WORD wID     , HWND hWndCtl , BOOL& bHandled);
			LRESULT OnDestroy   (UINT uMsg       , WPARAM wParam, LPARAM lParam, BOOL& bHandled);
			LRESULT OnDismiss   (WORD wNotifyCode, WORD wID     , HWND hWndCtl , BOOL& bHandled);
			LRESULT OnInitDialog(UINT uMsg       , WPARAM wParam, LPARAM lParam, BOOL& bHandled);
			LRESULT OnSysCommand(UINT uMsg       , WPARAM wParam, LPARAM lParam, BOOL& bHandled);
			LRESULT OnTabNotify (INT idCtrl      , LPNMHDR pnmh , BOOL& bHandled);
		private: // ITabSetCallback
			virtual bool    TabSet__IsNewMode(void) override sealed;
			virtual HRESULT TabSet__OnDataChanged(const UINT pageId, const bool bChanged) override sealed;
			virtual HRESULT TabSet__OnDataComplete(const UINT pageId) override sealed;
		};
	private:
		CEmployeeRecordDlg::CEmployeeRecordDlgImpl  m_dlg;
	public:
		CEmployeeRecordDlg(CSharedObjects&);
		~CEmployeeRecordDlg(void);
	public:
		HRESULT    DoModal(CEmployeeDataRecord&, const bool bNewData);
	private:
		CEmployeeRecordDlg(const CEmployeeRecordDlg&);
		CEmployeeRecordDlg& operator= (const CEmployeeRecordDlg&);
	};
}}}}

#endif/*__PLATINUMCLIENTEMPLOYEERECORDDIALOG_H_D46011DF_5524_4e42_BF27_B25731D51EAC_INCLUDED*/