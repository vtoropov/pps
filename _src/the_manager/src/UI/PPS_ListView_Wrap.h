#ifndef __PLATINUMCLIENTUILISTVIEWCTRLWRAPPER_H_8EED25C2_E06D_4fe4_9529_098279D3A5A1_INCLUDED
#define __PLATINUMCLIENTUILISTVIEWCTRLWRAPPER_H_8EED25C2_E06D_4fe4_9529_098279D3A5A1_INCLUDED
/*
	Created by Tech_dog (VToropov) on 24-Mar-2014 at 3:19:48pm, GMT+4, Saint-Petersburg, Monday;
	This is Platinum Client UI ListView Control Wrapper class declaration file.
*/
#include "PlatinumClient_SharedObjects.h"

namespace Platinum { namespace client { namespace UI { namespace common
{
	using Platinum::client::manager::CSharedObjects;

	struct RawListViewColumnSpecItem
	{
		::ATL::CAtlString csTitle;
		UINT              nWidth;
		DWORD             dwAlign;
		RawListViewColumnSpecItem(void): nWidth(0), dwAlign(0){}
		RawListViewColumnSpecItem(LPCTSTR pTitle_, const UINT nWidth_, const DWORD dwAlign_): csTitle(pTitle_), nWidth(nWidth_), dwAlign(dwAlign_) {}
	};

	typedef ::std::vector<RawListViewColumnSpecItem> RawListViewColumnSpec;

	class CListViewWrap
	{
	private:
		::WTL::CListViewCtrl m_list;
	public:
		CListViewWrap(::ATL::CWindow& lst_ref);
		~CListViewWrap(void);
	public:
		::ATL::CAtlString  ActiveCell(const INT nCol)const;
		HRESULT            AddColumns(const RawListViewColumnSpecItem*, const INT nItemCount);
		HRESULT            AddColumns(const ::std::vector<RawListViewColumnSpecItem>&);
		HRESULT            AddRow(INT& newRowIndex, const bool bEnsureVisible = false);
		HRESULT            AppendColumn(const RawListViewColumnSpecItem&);
		::ATL::CAtlString  Cell(const INT nRow, const INT nCol)const;
		HRESULT            Cell(const INT nRow, const INT nCol, LPCTSTR pText);
		INT                Columns(void)const;
		HRESULT            Configure(void);
		HRESULT            RemoveAllRows(void);
		HRESULT            RemoveCurrent(void);
		INT                Rows(void)const;
		INT                RowsCount(void)const;
		INT                SelectedRow(void)const;
		HRESULT            SelectedRow(const INT nRow);
	};

	class eListViewSort
	{
	public:
		enum _e{
			eNone  = 0,
			eAscending = 1,
			eDescending = 2,
		};
	};

	class CListViewHeaderWrap
	{
	private:
		::ATL::CWindow&     m_list;
	public:
		CListViewHeaderWrap(::ATL::CWindow& lst_ref);
		~CListViewHeaderWrap(void);
	public:
		VOID                ClearAll(void);
		bool                IsValid(void)const;
		eListViewSort::_e   SortOrder(const INT nIndex) const;
		HRESULT             SortOrder(const INT nIndex, const eListViewSort::_e);
	};

	class CListViewSorter
	{
	private:
		const
		CSharedObjects&     m_shared;
		CListViewHeaderWrap m_header;
		INT                 m_last;    // last sorted column index
		::ATL::CWindow&     m_list;
	public:
		CListViewSorter(const CSharedObjects&, ::ATL::CWindow& lst_ref);
		~CListViewSorter(void);
	public:
		HRESULT             Sort(const INT nIndex);
	};
}}}}

#endif/*__PLATINUMCLIENTUILISTVIEWCTRLWRAPPER_H_8EED25C2_E06D_4fe4_9529_098279D3A5A1_INCLUDED*/