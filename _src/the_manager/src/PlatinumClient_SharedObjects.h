#ifndef __PLATINUMMANAGERAPPLICATIONSHAREDOBJECTS_H_9F1DDA92_DE6F_4b1a_A849_396EA8DB8C8D_INCLUDED
#define __PLATINUMMANAGERAPPLICATIONSHAREDOBJECTS_H_9F1DDA92_DE6F_4b1a_A849_396EA8DB8C8D_INCLUDED
/*
	Created by Tech_dog (VToropov) on 9-Apr-2014 at 4:19:33pm, GMT+4, Saint-Petersburg, Wednesday;
	This is Platinum Manager Application Shared Objects class declaratioon file.
*/
#include "PPS_SharedObjects.h"
#include "PPS_USB_Connector.h"
#include "PlatinumClient_CommonSettings.h"

namespace Platinum { namespace client { namespace manager
{
	using Platinum::client::usb::IDeviceNotification;
	using Platinum::client::usb::eDeviceNotification;
	using Platinum::client::usb::CEventConnector;

	class CSharedObjects : 
		public Platinum::client::common::CSharedObjects,
		public IDeviceNotification
	{
		typedef Platinum::client::common::CSharedObjects TBase;
	private:
		CEventConnector      m_connector;
		TMgrSettings&        m_mgr_settings;
	public:
		CSharedObjects(TMgrSettings&);
		~CSharedObjects(void);
	private: // IDeviceNotification
		virtual  HRESULT     DeviceNotification_OnChange(const eDeviceNotification::_e, const _variant_t& dev_path) override sealed;
	public:
		const
		TMgrSettings&        MgrSettings(void)const;
		TMgrSettings&        MgrSettings(void);
	};
}}}

#endif/*__PLATINUMMANAGERAPPLICATIONSHAREDOBJECTS_H_9F1DDA92_DE6F_4b1a_A849_396EA8DB8C8D_INCLUDED*/