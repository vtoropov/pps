/*
	Created by Tech_dog (VToropov) on 22-Mar-2014 at 8:02:21pm, GMT+4, Moscow Region,
	Rail Road Train #43, Coatch #6, Place #1, Saturday;
	This is Platinum Payroll Systems Client Application Entry Point implementation file.
*/
#include "StdAfx.h"
#include "Shared_GenericAppObject.h"
#include "Shared_SystemCore.h"
#include "PlatinumClient_CommonSettings.h"
#include "PlatinumClient_SharedObjects.h"
#include "PlatinumClient_MainFrame.h"
#include "UIX_GdiProvider.h"

CAppModule _Module;

using namespace Platinum;
using namespace Platinum::client;
using namespace Platinum::client::common;

using namespace Platinum;
using namespace shared::lite;
using namespace shared::lite::common;

namespace global 
{
	CApplication&    GetAppObjectRef(void)
	{
		static CApplication app_obj;
		return app_obj;
	}

	using Platinum::client::common::CCommonSettings;

	CCommonSettings& GetSettingsRef(void)
	{
		static CCommonSettings settings;
		return settings;
	}

	TMgrSettings&    GetMgrSettingsRef(void)
	{
		static TMgrSettings settings;
		return settings;
	}
}

CApplication&    Global_GetAppObjectRef(void)
{
	return global::GetAppObjectRef();
}

CCommonSettings& Global_GetSettingsRef(void)
{
	return global::GetSettingsRef();
}

TMgrSettings&    Global_GetMgrSettingsRef(void)
{
	return global::GetMgrSettingsRef();
}

INT RunModal(LPTSTR lpstrCmdLine = 0, int nCmdShow = SW_SHOW)
{
	TRACE_FUNC();
	lpstrCmdLine; nCmdShow;

	INT nRet = 0;
	{
		shared::lite::common::CApplication& the___platinum__app = global::GetAppObjectRef();
		HRESULT hr__ = the___platinum__app.Process().RegisterSingleton(_T("Global\\Platinum_Client"));
		if (S_OK != hr__)
		{
			AtlMessageBox(HWND_DESKTOP, _T("You can run only one instance of the Platinum Manager application."), 
							the___platinum__app.GetName(), MB_OK|MB_ICONEXCLAMATION|MB_SETFOREGROUND);
			return nRet;
		}
		Platinum::client::manager::CCommonSettings& settings = global::GetMgrSettingsRef();
		Platinum::client::manager::CSharedObjects shared_objs(settings);
		Platinum::client::UI::CMainFrame main_frame(shared_objs);
		{
			hr__ = main_frame.DoModal();
		}
		nRet = (!FAILED(hr__) ? 0 : 1);
	}
	return nRet;
}

int WINAPI _tWinMain(HINSTANCE hInstance, HINSTANCE hPrevInstance, LPTSTR lpstrCmdLine, int nCmdShow)
{
	hInstance; hPrevInstance; lpstrCmdLine; nCmdShow;
	shared::lite::sys_core::CComAutoInitializer com(false);

	HRESULT hr__ = ::CoInitializeSecurity(NULL, -1, NULL, NULL,
					RPC_C_AUTHN_LEVEL_NONE, RPC_C_IMP_LEVEL_IDENTIFY, NULL, EOAC_NONE, NULL);
	ATLASSERT(SUCCEEDED(hr__));
	/*
		Tech_dog commented on 09-Feb-2010 at 12:47:50pm:
		________________________________________________
		we need to assign lib id to link ATL DLL statically,
		otherwise we get annoying fucking message "Did you forget to pass the LIBID to CComModule::Init?"
	*/
	_Module.m_libid = LIBID_ATLLib;
	INT nResult = 0;
	// this resolves ATL window thunking problem when Microsoft Layer for Unicode (MSLU) is used
	::DefWindowProc(NULL, 0, 0, 0L);

	AtlInitCommonControls(ICC_BAR_CLASSES);	// add flags to support other controls

	hr__ = _Module.Init(NULL, hInstance);
	if(!SUCCEEDED(hr__))
	{
		ATLASSERT(FALSE);
		return 1;
	}

	TRACE_FUNC();
	
	ex_ui::draw::CGdiPlusLibLoader  gdi_loader;
	nResult = RunModal(lpstrCmdLine, nCmdShow);
	_Module.Term();
	
	return nResult;
}