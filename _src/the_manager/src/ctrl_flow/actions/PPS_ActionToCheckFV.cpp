/*
	Created by Tech_dog (VToropov) on 16-Mar-2016 at 7:07:07pm, GMT+7, Phuket, Rawai, Wednesday;
	This is Platinum client FV check asynchronous action class implementation file.
*/
#include "StdAfx.h"
#include "PPS_ActionToCheckFV.h"

using namespace Platinum::client::ControlFlow;

#include "Shared_SystemCore.h"
#include "Shared_GenericRunnableObject.h"

using namespace shared::lite::sys_core;
using namespace shared::lite::runnable;

#include "PPS_DataProvider_CommonDefs.h"

using namespace Platinum::client::data;

/////////////////////////////////////////////////////////////////////////////

CActionToCheckFv::CActionToCheckFv(Platinum::client::manager::CSharedObjects& _shared, CEnrollDataProvider& _provider) :
	m_error (m_sync_obj),
	m_state (CThreadState::eStopped),
	m_shared(_shared),
	m_provider(_provider),
	m_fvIndex (-1),
	m_record(false)
{
}

CActionToCheckFv::~CActionToCheckFv(void)
{
}

/////////////////////////////////////////////////////////////////////////////

CSysError    CActionToCheckFv::Error(void)const
{
	CSysError err_obj;
	{
		SAFE_LOCK(m_sync_obj);
		err_obj = m_error;
	}
	return err_obj;
}

bool         CActionToCheckFv::IsRunning(void)const
{
	bool bRunning = false;
	{
		SAFE_LOCK(m_sync_obj);
		bRunning = !!(CThreadState::eWorking & m_state);
	}
	return bRunning;
}

HRESULT      CActionToCheckFv::Start(const CEmployeeDataRecord& _record, const INT nFvIndex)
{
	if (this->IsRunning())
	{
		m_error.SetState(
				(DWORD)ERROR_INVALID_STATE,
				_T("Action is already running")
			);
		return m_error;
	}

	m_record  = _record;
	m_fvIndex = nFvIndex;

	m_crt.IsStopped(false);

	if (!CThreadPool::QueueUserWorkItem(&CActionToCheckFv::ActionWorkerThread, this))
		m_error = ::GetLastError();
	else if (m_error)
		m_error.Clear();

	if (m_error)
		m_state = CThreadState::eError;
	else
		m_state = CThreadState::eWorking;

	return m_error;
}

DWORD        CActionToCheckFv::State(void)const
{
	DWORD state_ = 0;
	{
		SAFE_LOCK(m_sync_obj);
		state_ = m_state;
	}
	return state_;
}

HRESULT      CActionToCheckFv::Stop (void)
{
	if (!this->IsRunning())
	{
		m_error.SetState(
				(DWORD)ERROR_INVALID_STATE,
				_T("Action is already stopped")
			);
		return m_error;
	}
	m_crt.IsStopped(true);

	const DWORD dwResult = ::WaitForSingleObject(m_crt.EventObject(), INFINITE);

	if (dwResult != WAIT_OBJECT_0)
		m_error = ::GetLastError();
	else if (m_error)
		m_error.Clear();

	if (m_error)
		m_state = CThreadState::eError;
	else
		m_state = CThreadState::eStopped;

	return m_error;
}

/////////////////////////////////////////////////////////////////////////////

VOID         CActionToCheckFv::ActionWorkerThread(void)
{
	CComAutoInitializer com_core(false);

	HRESULT hr_ = S_OK;

	while (!m_crt.IsStopped())
	{
		for (INT j_ = 0; j_ < m_record.FvData().Count(); j_++)
		{
			if ( j_ == m_fvIndex || m_fvIndex == -1)
			{
				const _variant_t& fv_data = m_record.FvData().Image(j_).Data();
				hr_ = ValidateVeinData(fv_data);
				if (S_OK != hr_)
					continue;

				hr_ = m_shared.Processor().VerifyMatch(fv_data, fv_data);
				CAtlString cs_error = m_shared.Processor().GetLastError_Ref().GetDescription();
				AtlMessageBox(NULL, cs_error.GetString());
			}
		}
		break;
	}
	::SetEvent(m_crt.EventObject());
}