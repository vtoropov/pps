#ifndef _PPSACTIONTOCHECKFV_H_61C18B73_81DE_49b6_932A_311DF04585F8_INCLUDED
#define _PPSACTIONTOCHECKFV_H_61C18B73_81DE_49b6_932A_311DF04585F8_INCLUDED
/*
	Created by Tech_dog (VToropov) on 16-Mar-2016 at 4:38:05pm, GMT+7, Phuket, Rawai, Wednesday;
	This is Platinum client FV check asynchronous action class declaration file.
*/
#include "PPS_ControlFlowDefs.h"
#include "PPS_DataProvider_4_Enroll.h"

#include "Shared_SystemError.h"
#include "Shared_GenericSyncObject.h"
#include "Shared_SystemThreadPool.h"

#include "PlatinumClient_SharedObjects.h"

namespace Platinum { namespace client { namespace ControlFlow
{
	using shared::lite::common::CSysError;
	using shared::lite::common::CSysErrorSafe;
	using shared::lite::runnable::CGenericSyncObject;

	using shared::lite::sys_core::CThreadCrtData;
	using shared::lite::sys_core::CThreadState;

	using Platinum::client::data::CEmployeeDataRecord;
	using Platinum::client::data::CEnrollDataProvider;
	using Platinum::client::manager::CSharedObjects;

	class CActionToCheckFv
	{
	private:
		CSharedObjects&      m_shared;
		CEnrollDataProvider& m_provider;
	private:
		CThreadCrtData       m_crt;
		CGenericSyncObject   m_sync_obj;
	private:
		volatile
		DWORD                m_state;
		CSysErrorSafe        m_error;
		CEmployeeDataRecord  m_record;
		INT                  m_fvIndex;
	public:
		CActionToCheckFv(CSharedObjects&, CEnrollDataProvider&);
		~CActionToCheckFv(void);
	public:
		CSysError            Error(void)const;
		bool                 IsRunning(void)const;
		HRESULT              Start(const CEmployeeDataRecord&, const INT nFvIndex);
		DWORD                State(void)const;
		HRESULT              Stop (void);
	private:
		VOID                 ActionWorkerThread(void);
	};
}}}

#endif/*_PPSACTIONTOCHECKFV_H_61C18B73_81DE_49b6_932A_311DF04585F8_INCLUDED*/