#ifndef __PLATINUMCLIENTCONTROLFLOWDEFS_H_E8D39A45_36F0_41e2_8561_F07892B174CE_INCLUDED
#define __PLATINUMCLIENTCONTROLFLOWDEFS_H_E8D39A45_36F0_41e2_8561_F07892B174CE_INCLUDED
/*
	Created by Tech_dog (VToropov) on 23-Mar-2014 at 4:52:13pm, GMT+4, Saint-Petersburg, Sunday;
	This is Platinum Payroll Systems Client Control Flow Definitions file.
*/

namespace Platinum { namespace client { namespace ControlFlow
{
	class eControlFlowState
	{
	public:
		enum _enum{
			eSuspend    = 0, // default, yellow
			eWorking    = 1, // green
			eError      = 2, // red
			eWaiting    = 3, // blue
		};
	};

	class eProcessFlowState
	{
	public:
		enum _enum {
			eNotSpecified      =  0,  // common unspecified state
			eWaitingForVerify  =  1,  // relates to checkout page
			eProcessingVerify  =  2,  // relates to checkout page
			eWaitingForEnroll  =  3,  // relates to enrolment page
			eProcessingEnroll  =  4,  // relates to enrolment page
		};
	};
}}}
#endif/*__PLATINUMCLIENTCONTROLFLOWDEFS_H_E8D39A45_36F0_41e2_8561_F07892B174CE_INCLUDED*/