/*
	Created by Tech_dog (VToropov) on 24-Mar-2014 at 3:08:02pm, GMT+4, Saint-Petersburg, Monday;
	This is Platinum Client Enrollment Data Provider class implementation file.
*/
#include "StdAfx.h"
#include "PPS_DataProvider_4_Enroll.h"
#include "PPS_ListView_Wrap.h"
#include "PPS_DataProvider_CommonDefs.h"
#include "PPS_DataProvider_4_FvData.h"

using namespace Platinum::client::data;
using namespace Platinum::client::UI;
using namespace Platinum::client::UI::common;
using namespace Platinum::client::common;

#include "Shared_PersistentStorage.h"
#include "Shared_GenericAppObject.h"

using namespace shared::lite::common;
using namespace shared::lite::data;
using namespace shared::lite::persistent;

////////////////////////////////////////////////////////////////////////////

CEnrollDataProvider::CEnrollDataProvider(Platinum::client::manager::CSharedObjects& obj_ref) : m_bDirty(false), TBaseProvider(obj_ref)
{
}

CEnrollDataProvider::~CEnrollDataProvider(void)
{
}

////////////////////////////////////////////////////////////////////////////

namespace Platinum { namespace client { namespace data { namespace details
{
	static const CEmployeeDataRecord& EnrollProvider_InvalidRecordRef(void)
	{
		static CEmployeeDataRecord record_(false);
		return record_;
	}

	static HRESULT EnrollProvider_CreateHeader(CCsvFile::THeader& header_ref)
	{
		CEnrollRecordSpec spec_;
		header_ref = spec_.CreateHeader();
		return S_OK;
	}

	static HRESULT EnrollProvider_CreateRow(CCsvFile::TRow& row_ref, const CEmployeeDataRecord& rec_ref)
	{
		const INT nFields = CEnrollRecordSpec::nFieldCount;
		for ( INT i_ = 0; i_ < nFields; i_++)
		{
			try
			{
				switch(i_)
				{
				case CEnrollRecordSpec::eCode:     row_ref.push_back(::ATL::CAtlString(rec_ref.Code()));     break;
				case CEnrollRecordSpec::eEmployee: row_ref.push_back(::ATL::CAtlString(rec_ref.Name()));     break;
				case CEnrollRecordSpec::eWorkArea: row_ref.push_back(::ATL::CAtlString(rec_ref.WorkArea())); break;
				default:
					return DISP_E_BADINDEX;
				}
			} catch (::std::bad_alloc&){ return E_OUTOFMEMORY; }
		}
		return S_OK;
	}
}}}}

////////////////////////////////////////////////////////////////////////////

HRESULT         CEnrollDataProvider::Append(const CEmployeeDataRecord& new_rec)
{
	HRESULT hr_ = m_objects.Cache().Append(new_rec);
	if (S_OK == hr_)
		m_bDirty = true;
	return hr_;
}

HRESULT         CEnrollDataProvider::Remove(const ::ATL::CAtlString& cs_code)
{
	HRESULT hr_ = m_objects.Cache().Remove(cs_code);
	if (S_OK == hr_)
		m_bDirty = true;
	return hr_;
}

HRESULT         CEnrollDataProvider::Save(void)
{
	if (!m_bDirty)
		return S_OK;

	CApplicationCursor wait_cursor;

	::ATL::CAtlString cs_storage;
	HRESULT hr_ = Platinum::client::data::GetStorageFolder(cs_storage);
	if (S_OK != hr_)
		return  hr_;

	cs_storage+= _T("employee_records.csv");
	CCsvFile csv(eCsvFileOption::eUseCommaSeparator);
	{
		CCsvFile::THeader header;
		hr_ = details::EnrollProvider_CreateHeader(header);
		if (S_OK != hr_)
			return  hr_;
		hr_ = csv.Header(header);
		if (S_OK != hr_)
			return  hr_;
	}
	CBase64 base_64;
	{
		const INT nRecs = m_objects.Cache().Count();
		for ( INT i_ = 0; i_ < nRecs; i_++)
		{
			CCsvFile::TRow row;
			const CEmployeeDataRecord& record = m_objects.Cache().Item(i_);
			hr_ = details::EnrollProvider_CreateRow(row, record);
			if (S_OK != hr_)
				break;
			hr_ = csv.AddRow(row);
			if (S_OK != hr_)
				break;
		}
	}
	if (S_OK == hr_)
		hr_ = csv.Save(cs_storage, true);
	if (S_OK == hr_)
	{
		CEnrollFvProvider fv_provider(m_objects);
		hr_ = fv_provider.Save(true);
	}
	return  hr_;
}

////////////////////////////////////////////////////////////////////////////

HRESULT                    CEnrollDataProvider::Update(const CEmployeeDataRecord& rec_ref)
{
	HRESULT hr_ = m_objects.Cache().ItemOf(rec_ref);
	if (S_OK == hr_)
		m_bDirty = true;
	return hr_;
}