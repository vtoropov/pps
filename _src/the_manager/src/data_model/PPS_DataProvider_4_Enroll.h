#ifndef __PLATINUMCLIENTENROLLMENTDATAPROVIDER_H_91BA89CB_11EA_42f4_A160_29E7C5EF9A78_INCLUDED
#define __PLATINUMCLIENTENROLLMENTDATAPROVIDER_H_91BA89CB_11EA_42f4_A160_29E7C5EF9A78_INCLUDED
/*
	Created by Tech_dog (VToropov) on 24-Mar-2014 at 1:04:30pm, GMT+4, Saint-Petersburg, Monday;
	This is Platinum Client Enrollment Data Provider class declaration file.
*/
#include "PPS_DataProvider_4_Enroll[RA].h"
#include "PlatinumClient_SharedObjects.h"

namespace Platinum { namespace client { namespace data
{
	class CEnrollDataProvider : public CEnrollDataProvider_RA
	{
		typedef CEnrollDataProvider_RA TBaseProvider;
	private:
		bool         m_bDirty;
	public:
		CEnrollDataProvider(Platinum::client::manager::CSharedObjects&);
		~CEnrollDataProvider(void);
	public:
		HRESULT      Save(void);
	public:
		HRESULT      Append(const CEmployeeDataRecord&);
		HRESULT      Remove(const ::ATL::CAtlString& cs_code);
		HRESULT      Update(const CEmployeeDataRecord&);      // doesn't care about original employee code, if the code was changed, new object automatically added
	};
}}}

#endif/*__PLATINUMCLIENTENROLLMENTDATAPROVIDER_H_91BA89CB_11EA_42f4_A160_29E7C5EF9A78_INCLUDED*/