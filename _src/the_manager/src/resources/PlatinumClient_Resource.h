//{{NO_DEPENDENCIES}}
// Microsoft Visual C++ generated include file.
// Used by PlatinumClient_Resource.rc
//
// Created by Tech_dog (VToropov) on 23-Mar-2014 at 7:28:34am, GMT+4,
// Saint-Petersburg Region, Rail Road Train #43, Coatch #6, Place #1, Sunday;
// This is Platinum Client Resource Declaration file.
//
#define IDD_PLATINUM_MAIN_DLG                             101
#define IDR_PLATINUM_MAIN_DLG_ICON                        103
#define IDC_PLATINUM_MAIN_DLG_STATUS                      105

/////////////////////////////////////////////////////////////////////////////
//
// Main dialog identifiers
//
/////////////////////////////////////////////////////////////////////////////

#define IDR_PLATINUM_MAIN_DLG_HEADER                      1001
#define IDD_PLATINUM_MAIN_DLG_TABSET                      1003
#define IDD_PLATINUM_MAIN_DLG_ENROLL_TAB                  1005
#define IDD_PLATINUM_MAIN_DLG_TRACK_TAB                   1007
#define IDD_PLATINUM_MAIN_DLG_SETUP_TAB                   1009
#define IDR_PLATINUM_MAIN_DLG_WARN_16px                   1011

/////////////////////////////////////////////////////////////////////////////
//
// Main dialog employee maintenance page identifiers
//
/////////////////////////////////////////////////////////////////////////////

#define IDC_ENROLL_TAB_LIST                               2001
#define IDC_ENROLL_TAB_NEW                                2003
#define IDC_ENROLL_TAB_EDIT                               2005
#define IDC_ENROLL_TAB_DELETE                             2007
#define IDC_ENROLL_TAB_MENU                               2009
#define IDC_ENROLL_TAB_RECONNECT                          2011
#define IDS_ENROLL_DATA_SAVED                             2013
#define IDS_ENROLL_DATA_COUNT                             2015

/////////////////////////////////////////////////////////////////////////////
//
// Main dialog time tracking page identifiers
//
/////////////////////////////////////////////////////////////////////////////

#define IDC_TRACK_TAB_LIST                                3001
#define IDC_TRACK_TAB_REFRESH                             3003
#define IDC_TRACK_AUTO_REFRESH                            3005
#define IDS_TRACK_COLUMN_CODE                             3007
#define IDS_TRACK_COLUMN_NAME                             3009
#define IDS_TRACK_COLUMN_DATE                             3011
#define IDS_TRACK_COLUMN_TIME                             3013
#define IDS_TRACK_COLUMN_WA                               3015
#define IDS_TRACK_DATA_COUNT                              3017
#define IDS_TRACK_DATA_OPEN_ERR                           3019

/////////////////////////////////////////////////////////////////////////////
//
// Main dialog setup page identifiers
//
/////////////////////////////////////////////////////////////////////////////


/////////////////////////////////////////////////////////////////////////////
//
// Employee record dialog identifiers
//
/////////////////////////////////////////////////////////////////////////////

#define IDD_EMPLOYEE_DLG                                  4001
#define IDR_EMPLOYEE_DLG_HEADER                           4003
#define IDC_EMPLOYEE_DLG_TABSET                           4005
#define IDD_EMPLOYEE_DLG_TAB_GENERAL                      4007
#define IDD_EMPLOYEE_DLG_TAB_VEIN                         4009

/////////////////////////////////////////////////////////////////////////////
//
// Employee record dialog general page identifiers
//
/////////////////////////////////////////////////////////////////////////////

#define IDC_EMPLOYEE_GENERAL_PAGE_CODE                    5001
#define IDC_EMPLOYEE_GENERAL_PAGE_NAME                    5003
#define IDC_EMPLOYEE_GENERAL_PAGE_AREA                    5005

/////////////////////////////////////////////////////////////////////////////
//
// Employee record dialog finger vein page identifiers
//
/////////////////////////////////////////////////////////////////////////////

#define IDC_EMPLOYEE_FV_PAGE_DATA_STATE                   6001
#define IDC_EMPLOYEE_FV_PAGE_DATA_EDIT                    6003
#define IDC_EMPLOYEE_FV_PAGE_DATA_CLEAR                   6005
#define IDC_EMPLOYEE_FV_PAGE_DATA_NEW                     6007
#define IDC_EMPLOYEE_FV_PAGE_DATA_REMOVE                  6009
#define IDC_EMPLOYEE_FV_PAGE_DATA_CHECK                   6011
#define IDC_EMPLOYEE_FV_PAGE_DATA_DGRM                    6013
#define IDC_EMPLOYEE_FV_PAGE_DATA_EXFUNC                  6015
#define IDC_EMPLOYEE_FV_PAGE_DATA_SIMCHK                  6017
#define IDC_EMPLOYEE_FV_PAGE_DATA_WARN16                  6019

/////////////////////////////////////////////////////////////////////////////

#define IDD_PLATINUM_SIMCHECK_DLG                         7001
#define IDC_PLATINUM_SIMCHECK_LABEL                       7003
#define IDC_PLATINUM_SIMCHECK_PROG                        7005


