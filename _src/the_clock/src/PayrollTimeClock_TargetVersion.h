#ifndef __PAYROLLTIMECLOCKTARGETVERSION_H_17A2AE79_D453_4a39_999F_7518BBB98C14_INCLUDED
#define __PAYROLLTIMECLOCKTARGETVERSION_H_17A2AE79_D453_4a39_999F_7518BBB98C14_INCLUDED
/*
	Created by Tech_dog (VToropov) on 8-Apr-2014 at 4:04:24pm, GMT+4, Saint-Petersburg, Tuesday;
	----------------------------------------------------------------------------------------------
	The following macros define the minimum required platform.  The minimum required platform
	is the earliest version of Windows, Internet Explorer etc. that has the necessary features to run 
	your application.  The macros work by enabling all features available on platform versions up to and 
	including the version specified.

	Modify the following defines if you have to target a platform prior to the ones specified below.
	Refer to MSDN for the latest info on corresponding values for different platforms.
*/

#ifndef WINVER                 // Specifies that the minimum required platform is Windows Vista.
#define WINVER 0x0600          // Change this to the appropriate value to target other versions of Windows.
#endif

#ifndef _WIN32_WINNT           // Specifies that the minimum required platform is Windows Vista.
#define _WIN32_WINNT 0x0600    // Change this to the appropriate value to target other versions of Windows.
#endif

#ifndef _WIN32_WINDOWS         // Specifies that the minimum required platform is Windows Vista.
#define _WIN32_WINDOWS 0x0600  // Change this to the appropriate value to target Windows Me or later.
#endif

#ifndef _WIN32_IE              // Specifies that the minimum required platform is Internet Explorer 9.0.
#define _WIN32_IE 0x0900       // Change this to the appropriate value to target other versions of IE.
#endif

#endif/*__PAYROLLTIMECLOCKTARGETVERSION_H_17A2AE79_D453_4a39_999F_7518BBB98C14_INCLUDED*/