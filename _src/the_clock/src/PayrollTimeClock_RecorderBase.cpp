/*
	Created by Tech_dog (VToropov) on 5-Feb-2015 at 2:24:21am, GMT+3, Taganrog, Thursday;
	This is Platinum Payroll System Time Clock Recorder Base class implementation file.
*/
#include "StdAfx.h"
#include "PayrollTimeClock_RecorderBase.h"
#include "PPS_DataProvider_CommonDefs.h"
#include "PPS_DataProvider_4_Enroll[RA].h"
#include "PPS_DataProvider_4_TimeTrack.h"
#include "Shared_SystemError.h"

using namespace Payroll;
using namespace Payroll::time_clock;

using namespace Platinum;
using namespace Platinum::client;
using namespace Platinum::client::data;

using namespace shared::lite;
using namespace shared::lite::common;

/////////////////////////////////////////////////////////////////////////////

CRecorderBase::CRecorderBase(Payroll::time_clock::common::CSharedObjects& objs_ref) : m_bInitialized(false), m_shared(objs_ref)
{
}

CRecorderBase::~CRecorderBase(void)
{
}

/////////////////////////////////////////////////////////////////////////////

const
CEmployeeDataRecordEx&  CRecorderBase::Cached(void)const
{
	return m_cached;
}

CEmployeeDataRecordEx&  CRecorderBase::Cached(void)
{
	return m_cached;
}

LPCTSTR    CRecorderBase::GetLastSaveTime(void)const
{
	return m_last_time;
}

HRESULT    CRecorderBase::Initialize(void)
{
	if (this->IsInitialized())
		return S_OK;
	HRESULT hr_ = this->Reinitialize();
	return  hr_;
}

bool       CRecorderBase::IsInitialized(void)const
{
	return m_bInitialized;
}

HRESULT    CRecorderBase::Reinitialize(void)
{
	Platinum::client::data::CEnrollDataProvider_RA provider(m_shared);
	HRESULT hr_ = provider.Initialize();
	m_bInitialized = (S_OK == hr_);
	if (FAILED(hr_))
	{
		TRACE_FUNC();
		TRACE_ERR__a1(_T("Error occurred while initializing the data: 0x%x"), hr_);
	}
	return  hr_;
}

HRESULT    CRecorderBase::Save(void)
{
	HRESULT hr_ = this->Save(m_cached);
	return  hr_;
}

HRESULT    CRecorderBase::Save(CEmployeeDataRecordEx& _result)
{
	HRESULT hr_ = S_OK;
	CTimeTrackRecord clock_record;
	clock_record = _result;

	CTimeTrackDataProvider provider;
	hr_ = provider.SaveEntry(clock_record);

	if (S_OK == hr_)
	{
		m_last_time.Format(_T("%s %s"), clock_record.Time(), clock_record.Date());

		::ATL::CAtlString cs_msg;
		cs_msg.Format(_T("Verified: %s"), clock_record.Code());
		TRACE_INFO(cs_msg.GetString());
	}
	else
	{
		CSysError sys_err(hr_);
		if (sys_err.HasDetails() == false)
			sys_err.SetUnknownMessage();
		::ATL::CAtlString cs_error;
		cs_error.Format(_T("The error occurred while saving data: code=0x%x; desc=%s"), hr_, sys_err.GetDescription());
		TRACE_ERROR(cs_error.GetString());
	}
	return hr_;
}

HRESULT    CRecorderBase::Verify(const _variant_t& vProcessed, ICanContinue& snk_ref)
{
	m_cached.Clear();
	HRESULT hr_ = this->Verify(vProcessed, m_cached, snk_ref);
	return  hr_;
}

HRESULT    CRecorderBase::Verify(const _variant_t& vProcessed, CEmployeeDataRecordEx& _result, ICanContinue& snk_ref)
{
	bool bVerified = false;
	HRESULT hr_ = S_OK;

	const INT nCount = m_shared.Cache().Count();

	for (INT i_ = 0; i_ < nCount; i_++)
	{
		if (!snk_ref.ICanContinue_CheckState())
			break;
		const CEmployeeDataRecord& employee = m_shared.Cache().Item(i_);
		if (!employee.IsValid())
			continue;
		const CEmployeeFvData& fv_data = employee.FvData();
		if (fv_data.IsEmpty())
			continue;
		const INT nImages = fv_data.Count();
		for ( INT j_ = 0; j_ < nImages; j_++)
		{
			if (!snk_ref.ICanContinue_CheckState())
				break;
			const CEmployeeFvImage& fv_image = fv_data.Image(j_);
			if (!fv_image.IsValid())
				continue;
			hr_ = m_shared.Processor().VerifyMatch(vProcessed, fv_image.Data());
			if (FAILED(hr_))
			{
				::ATL::CAtlString cs_error;
				cs_error.Format(_T("Error occurred: code=0x%x; desc=%s"), hr_, m_shared.Error().GetDescription());
				TRACE_ERROR(cs_error.GetString());
			}
			else if (S_FALSE == hr_)
				continue;
			else
			{
				_result   = employee;
				_result.SelectedFvIndex(j_);
				bVerified = true;
				break;
			}
		}
		if (bVerified)
			break;
	}
	return (bVerified ? S_OK : S_FALSE);
}