#ifndef __PAYROLLTIMECLOCKAPPLICATIONCOMMONSETTINGS_H_CE0C3D82_4384_46a5_83E9_8C1FA74672CB_INCLUDED
#define __PAYROLLTIMECLOCKAPPLICATIONCOMMONSETTINGS_H_CE0C3D82_4384_46a5_83E9_8C1FA74672CB_INCLUDED
/*
	Created by Tech_dog (VToropov) on 8-Apr-2014 at 4:33:02pm, GMT+4, Saint-Petersburg, Tuesday;
	This is Payroll Time Clock Application Common Settings class declaration file.
*/
#include "PPS_CommonSettings.h"

namespace Payroll { namespace time_clock { namespace common
{
	class eFvCustomizeType
	{
	public:
		enum _e{
			eNone     = 0,
			eBorder   = 1,
			eHide     = 2,
			eMinimize = 4,
		};
	};

	class CFontInfo
	{
	private:
		::ATL::CAtlString     m_family;
		DWORD                 m_size;   // in points
	public:
		CFontInfo(void);
		~CFontInfo(void);
	public:
		LPCTSTR               Family(void)   const;
		HRESULT               Family(LPCTSTR)     ;
		DWORD                 Size  (void)   const;
		HRESULT               Size  (const DWORD) ;
	};

	class CCommonSettings :
		public  Platinum::client::common::CCommonSettings
	{
		typedef Platinum::client::common::CCommonSettings TBase;
	public:
		eFvCustomizeType::_e  FvCustomization(void)     const;
		LPCTSTR               GetAlwaysCostWA(void)     const;
		LPCTSTR               GetBkgndFilePath(void)    const;
		COLORREF              GetEmpForeColor(void)     const; // employee name's and clocking time's forecolor;
		LPCTSTR               GetFailureSoundFile(void) const;
		LPCTSTR               GetLogoutWA(void)         const;
		::ATL::CAtlString     GetPassword(void)         const;
		DWORD                 GetSelectWAtimout(void)   const;
		LPCTSTR               GetSuccessSoundFile(void) const;
		const CFontInfo&      GetWAfontInfo(void)       const;
		DWORD                 GetWAlistWidth(void)      const; // 0 - not specified, (-1) - auto, otherwise, the width in pixels;
		bool                  HasBkgndFilePath(void)    const;
		bool                  HasPassword(void)         const;
		bool                  IsBannerVisible(void)     const;
		bool                  IsFVStatusVisible(void)   const;
		bool                  IsLandscape (void)        const; // for debug purpose, if true, the main window has landscape orientation, otherwise, portrait;
		bool                  RunMaximized(void)        const;
		bool                  IsWorkAreaPromptMode(void)const; // by default is on, i.e. work area dialog appears for user input
	};
}}}

#endif/*__PAYROLLTIMECLOCKAPPLICATIONCOMMONSETTINGS_H_CE0C3D82_4384_46a5_83E9_8C1FA74672CB_INCLUDED*/