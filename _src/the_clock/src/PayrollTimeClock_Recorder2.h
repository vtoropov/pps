#ifndef __PAYROLLTIMECLOCKRECORDER2_H_1C9A15A3_2479_4548_8F47_7BE135418B9C_INCLUDED
#define __PAYROLLTIMECLOCKRECORDER2_H_1C9A15A3_2479_4548_8F47_7BE135418B9C_INCLUDED
/*
	Created by Tech_dog (VToropov) on 4-Feb-2015 at 2:23:18am, GMT+3, Taganrog, Wednesday;
	This is Platinum Payroll System Time Clock Extended Recorder class declaration file.
*/
#include "PayrollTimeClock_RecorderBase.h"

namespace Payroll { namespace time_clock
{
	using Payroll::time_clock::common::CSharedObjects;

	class CRecorder2 : public CRecorderBase
	{
		typedef CRecorderBase TRecorderBase;
	public:
		CRecorder2(CSharedObjects&);
		~CRecorder2(void);
	public:
		HRESULT         Record(void) override sealed;
	};
}}

#endif/*__PAYROLLTIMECLOCKRECORDER2_H_1C9A15A3_2479_4548_8F47_7BE135418B9C_INCLUDED*/