/*
	Created by Tech_dog (VToropov) on 8-Apr-2014 at 5:19:09pm, GMT+4, Saint-Petersburg, Tuesday;
	This is Payroll Time Clock Application Shared Objects class implementation file.
*/
#include "StdAfx.h"
#include "PayrollTimeClock_SharedObjects.h"
#include "PayrollTimeClock_RecorderBase.h"

using namespace Payroll;
using namespace Payroll::time_clock;
using namespace Payroll::time_clock::common;

CSharedObjects::CSharedObjects(CCommonSettings& sets_ref) : TSharedObjectBase(sets_ref), m_pRecorder(NULL), m_settings2(sets_ref)
{
}

CSharedObjects::~CSharedObjects(void)
{
}

////////////////////////////////////////////////////////////////////////////

CRecorderBase* 
const            CSharedObjects::Recorder(void)
{
	return m_pRecorder;
}

VOID             CSharedObjects::Recorder(CRecorderBase* const pRecorder)
{
	m_pRecorder = pRecorder;
}

const
CCommonSettings& CSharedObjects::Settings(void) const
{
	return m_settings2;
}

CCommonSettings& CSharedObjects::Settings(void)
{
	return m_settings2;
}