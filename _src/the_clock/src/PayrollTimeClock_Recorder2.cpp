/*
	Created by Tech_dog (VToropov) on 5-Feb-2015 at 8:37:00pm, GMT+3, Taganrog, Thursday;
	This is Platinum Payroll System Time Clock Extended Recorder class implementation file.
*/
#include "StdAfx.h"
#include "PayrollTimeClock_Recorder2.h"
#include "PayrollTimeClock_FS_Window.h"

using namespace Payroll;
using namespace Payroll::time_clock;
using namespace Payroll::time_clock::UI;

////////////////////////////////////////////////////////////////////////////

CRecorder2::CRecorder2(Payroll::time_clock::common::CSharedObjects& objs_ref) : TRecorderBase(objs_ref)
{
	TRecorderBase::m_shared.Recorder(this);
}

CRecorder2::~CRecorder2(void)
{
	TRecorderBase::m_shared.Recorder(NULL);
}


////////////////////////////////////////////////////////////////////////////

HRESULT    CRecorder2::Record(void)
{
	::WTL::CMessageLoop theLoop;
	_Module.AddMessageLoop(&theLoop);

	CMainFrame main_frame(TRecorderBase::m_shared);

	HRESULT hr_ = main_frame.Create();
	if (S_OK == hr_)
		theLoop.Run();
	return  hr_;
}