/*
	Created by Tech_dog (VToropov) on 9-Apr-2014 at 6:11:00pm, GMT+4, Saint-Petersburg, Wednesday;
	This is Platinum Payroll System Time Clock Recorder class implementation file.
*/
#include "StdAfx.h"
#include "PayrollTimeClock_Recorder.h"
#include "PPS_EmployeeDataRecord.h"
#include "PayrollTimeClock_Resource.h"
#include "PayrollTimeClock_RecordDlg.h"
#include "PayrollTimeClock_FviDlg.h"
#include "Shared_GenericAppObject.h"
#include "Shared_SystemError.h"

using namespace Payroll;
using namespace Payroll::time_clock;
using namespace Payroll::time_clock::UI;
using namespace Platinum;
using namespace Platinum::client;
using namespace Platinum::client::data;
using namespace shared::lite;
using namespace shared::lite::common;

////////////////////////////////////////////////////////////////////////////

CRecorder::CRecorder(Payroll::time_clock::common::CSharedObjects& objs_ref) : TRecorderBase(objs_ref)
{
}

CRecorder::~CRecorder(void)
{
}

////////////////////////////////////////////////////////////////////////////

HRESULT     CRecorder::Record(void)
{
	TRACE_FUNC();
	if (!this->IsInitialized())
		return OLE_E_BLANK;
	const INT nCount = m_shared.Cache().Count();
	if (nCount < 1)
		return OLE_E_BLANK;
	HRESULT hr_  = S_FALSE;
	bool bReInitilize  = false;

	while(true)
	{
		if (bReInitilize)
		{
			bReInitilize = false;
			hr_ = this->Reinitialize();
			if (FAILED(hr_))
				break;
		}
		bool bVerified = false;
		CFvDialogWrapper fv_dlg(m_shared);
		fv_dlg.Create();

		_variant_t vProcessed;
		hr_ = m_shared.Processor().CreateVerificationTemplate(vProcessed);
		fv_dlg.Destroy();

		if (S_FALSE == hr_)
		{
			if (IDYES == AtlMessageBox(
					::GetDesktopWindow(),
					IDS_PAYROLL_TC_END_PROCESS,
					global::GetAppObjectRef().GetName(),
					MB_ICONQUESTION|MB_YESNO|MB_SETFOREGROUND|MB_TOPMOST
				))
				break;
			else
				continue;
		}
		if (FAILED(hr_))
			break;
		CEmployeeDataRecordEx emp_record;
		hr_ = TRecorderBase::Verify(vProcessed, emp_record);
		bVerified = (S_OK == hr_);

		if (bVerified)
		{
			CRecordDlg dlg(m_shared, emp_record);
			eRecordDlgResult::_enum eResult = dlg.DoModal();
			if (eRecordDlgResult::eConfirm == eResult)
				hr_ = TRecorderBase::Save(emp_record);
			else if (eRecordDlgResult::eClose == eResult)
				break;
		}
		else
		{
			if (IDRETRY == AtlMessageBox(
					::GetActiveWindow(),
					IDS_PAYROLL_TC_NOT_SUCCESS,
					global::GetAppObjectRef().Version().ProductName().GetString(),
					MB_ICONSTOP|MB_RETRYCANCEL|MB_SETFOREGROUND|MB_TOPMOST
			))
			{
				bReInitilize = true;
			}
			else
			if (IDYES == AtlMessageBox(
					::GetDesktopWindow(),
					IDS_PAYROLL_TC_END_PROCESS,
					Global_GetAppObjectRef().GetName(),
					MB_ICONQUESTION|MB_YESNO|MB_SETFOREGROUND|MB_TOPMOST
				))
			{
				break;
			}
		}
	}
	return  hr_;
}