/*
	Created by Tech_dog (VToropov) on 28-Mar-2015 at 9:42:10pm, GMT+9, Thailand Phuket, Saturday;
	This is Platinum Payroll System Time Clock Windowless Recorder class implementation file.
*/
#include "StdAfx.h"
#include "PayrollTimeClock_Recorder0.h"
#include "PPS_EmployeeDataRecord.h"
#include "PayrollTimeClock_Resource.h"
#include "PayrollTimeClock_RecordDlg.h"
#include "PayrollTimeClock_FviDlg.h"
#include "Shared_GenericAppObject.h"
#include "Shared_SystemError.h"

using namespace Payroll;
using namespace Payroll::time_clock;
using namespace Payroll::time_clock::UI;
using namespace Platinum;
using namespace Platinum::client;
using namespace Platinum::client::data;
using namespace shared::lite;
using namespace shared::lite::common;

////////////////////////////////////////////////////////////////////////////

CRecorder0::CMessageHandler::CMessageHandler(IGenericEventNotify& _snk) : m_sink(_snk), m_evt_timer(NULL)
{
}

CRecorder0::CMessageHandler::~CMessageHandler(void)
{
}

////////////////////////////////////////////////////////////////////////////

LRESULT     CRecorder0::CMessageHandler::OnTimer(UINT, LPARAM, WPARAM, BOOL& bHandled)
{
	bHandled = TRUE;
	if (this->IsWindow())
		m_sink.GenericEvent_OnNotify(0);
	return 0;
}

////////////////////////////////////////////////////////////////////////////

HRESULT     CRecorder0::CMessageHandler::_Create(const DWORD dwPeriod)
{
	if (this->IsWindow())
		return HRESULT_FROM_WIN32(ERROR_ALREADY_EXISTS);
	if (NULL == this->Create(HWND_MESSAGE))
		return HRESULT_FROM_WIN32(::GetLastError());
	m_evt_timer = this->SetTimer(1, dwPeriod);
	HRESULT hr_ = S_OK;
	return  hr_;
}

HRESULT     CRecorder0::CMessageHandler::_Destroy(void)
{
	if (!this->IsWindow())
		return S_FALSE;
	if (m_evt_timer)
	{
		this->KillTimer(m_evt_timer); m_evt_timer = NULL;
	}
	this->SendMessage(WM_CLOSE);
	HRESULT hr_ = S_OK;
	return  hr_;
}

////////////////////////////////////////////////////////////////////////////

CRecorder0::CRecorder0(Payroll::time_clock::common::CSharedObjects& objs_ref) : TRecorderBase(objs_ref), m_timer(*this)
{
}

CRecorder0::~CRecorder0(void)
{
}

////////////////////////////////////////////////////////////////////////////

HRESULT     CRecorder0::Record(void)
{
	TRACE_FUNC();
	if (!this->IsInitialized())
		return OLE_E_BLANK;
	const INT nCount = m_shared.Cache().Count();
	if (nCount < 1)
		return OLE_E_BLANK;
	HRESULT hr_  = S_FALSE;
	bool bReInitilize  = false;

	while(true)
	{
		if (bReInitilize)
		{
			bReInitilize = false;
			hr_ = this->Reinitialize();
			if (FAILED(hr_))
				break;
		}
		bool bVerified = false;
		CFvDialogWrapper fv_dlg(m_shared);
		fv_dlg.Create();

		_variant_t vProcessed;
		hr_ = m_shared.Processor().CreateVerificationTemplate(vProcessed);
		fv_dlg.Destroy();

		if (S_FALSE == hr_)
		{
			if (IDYES == AtlMessageBox(
					::GetDesktopWindow(),
					IDS_PAYROLL_TC_END_PROCESS,
					global::GetAppObjectRef().GetName(),
					MB_ICONQUESTION|MB_YESNO|MB_SETFOREGROUND|MB_TOPMOST
				))
				break;
			else
				continue;
		}
		if (FAILED(hr_))
			break;
		CEmployeeDataRecordEx emp_record;
		hr_ = TRecorderBase::Verify(vProcessed, emp_record);
		bVerified = (S_OK == hr_);

		if (bVerified)
		{
			hr_ = m_timer._Create(2200);
			::PlaySound( (LPCTSTR) IDR_PAYROLL_TC_SND_SUCCESS, NULL, SND_RESOURCE|SND_ASYNC );

			LPCTSTR pszAlwaysCostWA = m_shared.Settings().GetAlwaysCostWA();
			if (::_tcslen(pszAlwaysCostWA))
				emp_record.WorkArea(pszAlwaysCostWA);

			::ATL::CAtlString cs_message;
			cs_message.Format(IDS_PAYROLL_TC_REC_SUCCESS, emp_record.Name());
			AtlMessageBox(
					NULL,
					cs_message.GetString(),
					_T("Authentication"),
					MB_ICONEXCLAMATION|MB_OK|MB_SETFOREGROUND|MB_TOPMOST
				);
			hr_ = m_timer._Destroy();
			hr_ = TRecorderBase::Save(emp_record);
		}
		else
		{
			if (IDRETRY == AtlMessageBox(
					::GetActiveWindow(),
					IDS_PAYROLL_TC_NOT_SUCCESS,
					global::GetAppObjectRef().Version().ProductName().GetString(),
					MB_ICONSTOP|MB_RETRYCANCEL|MB_SETFOREGROUND|MB_TOPMOST
			))
			{
				bReInitilize = true;
			}
			else
			if (IDYES == AtlMessageBox(
					::GetDesktopWindow(),
					IDS_PAYROLL_TC_END_PROCESS,
					Global_GetAppObjectRef().GetName(),
					MB_ICONQUESTION|MB_YESNO|MB_SETFOREGROUND|MB_TOPMOST
				))
			{
				break;
			}
		}
	}
	return  hr_;
}

////////////////////////////////////////////////////////////////////////////

HRESULT     CRecorder0::GenericEvent_OnNotify(const UINT eventId)
{
	eventId;
	::ATL::CWindow msg_box = ::GetActiveWindow();
	if (msg_box)
		msg_box.SendMessage(WM_CLOSE);
	return S_OK;
}