/*
	Created by Tech_dog (VToropov) on 8-Apr-2014 at 4:11:44pm, GMT+4, Saint-Petersburg, Tuesday;
	This is Payroll Time Clock application precompiled headers implementation file.
*/

#include "StdAfx.h"

#if (_ATL_VER < 0x0700)
#include <atlimpl.cpp>
#endif //(_ATL_VER < 0x0700)

namespace global
{
	using shared::lite::common::CApplication;
	using Payroll::time_clock::common::CCommonSettings;

	CApplication& GetAppObjectRef(void)
	{
		static CApplication app_obj;
		return app_obj;
	}
	
	CCommonSettings& GetSettingsRef(void)
	{
		static CCommonSettings settings;
		return settings;
	}
}

TApplication& Global_GetAppObjectRef(void)
{
	return global::GetAppObjectRef();
}

TCommonSettings&  Global_GetSettingsRef(void)
{
	return global::GetSettingsRef();
}