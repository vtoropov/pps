#ifndef _PAYROLLTIMEWORKAREASELECTPAGE_H_59824B08_F76D_433b_B61D_1ED18CD3F8B0_INCLUDED
#define _PAYROLLTIMEWORKAREASELECTPAGE_H_59824B08_F76D_433b_B61D_1ED18CD3F8B0_INCLUDED
/*
	Created by Tech_dog(VToropov) on 12-Feb-2015 at 0:15:50am, GMT+3, Taganrog, Thursday;
	This is Payroll Time Work Area Select Page class declaration file.
*/
#include "UIX_PanelBase.h"
#include "UIX_CommonDrawDefs.h"
#include "UIX_CommonCtrlDefs.h"
#include "UIX_Image.h"
#include "UIX_Button.h"
#include "PayrollTimeClock_SharedObjects.h"

namespace Payroll { namespace time_clock { namespace UI
{
	using ex_ui::draw::defs::IRenderer;
	using ex_ui::controls::defs::IControlNotify;
	using ex_ui::controls::CImage;
	using ex_ui::controls::CButton;
	using ex_ui::frames::IMessageHandler;
	using ex_ui::frames::CPanelBase;

	using Payroll::time_clock::common::CSharedObjects;

	class CMainFramePage_1 :
		public  CPanelBase,
		public  IMessageHandler
	{
		typedef CPanelBase TPageBase;
	private:
		IRenderer&         m_parent_rnd_ref;
		IControlNotify&    m_parent_snk_ref;
		CSharedObjects&    m_shared;
	private:
		CImage             m_lst_msg;
		CButton            m_cfm_but;
		CButton            m_out_but;
	private:
		UINT_PTR           m_evt_timer;
	public:
		CMainFramePage_1(IRenderer&, IControlNotify&, CSharedObjects&);
		~CMainFramePage_1(void);
	public: // CPanelBase
		virtual HRESULT    Create(const HWND hParent, const RECT& rcArea, const bool bVisible) override sealed;
		virtual HRESULT    Destroy(void) override sealed;
		virtual HRESULT    Hide(void) override sealed;
		virtual HRESULT    Show(void) override sealed;
		virtual HRESULT    UpdateLayout(LPRECT const = NULL) override sealed;
	private: // IMessageHandler
		virtual LRESULT    MessageHandler_OnMessage(UINT, WPARAM, LPARAM, BOOL&) override sealed;
	};
}}}

#endif/*_PAYROLLTIMEWORKAREASELECTPAGE_H_59824B08_F76D_433b_B61D_1ED18CD3F8B0_INCLUDED*/