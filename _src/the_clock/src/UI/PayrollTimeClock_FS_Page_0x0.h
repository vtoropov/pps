#ifndef _PAYROLLTIMECLOCKINGSCANPAGE_H_C06D6DA5_B7C8_4109_A8D4_100FE1E9A98A_INCLUDED
#define _PAYROLLTIMECLOCKINGSCANPAGE_H_C06D6DA5_B7C8_4109_A8D4_100FE1E9A98A_INCLUDED
/*
	Created by Tech_dog(VToropov) on 11-Feb-2015 at 2:55:54am, GMT+3, Taganrog, Wednesday;
	This is Payroll Time Clocking Finger Scan Page class declaration file.
*/
#include "UIX_PanelBase.h"
#include "UIX_CommonDrawDefs.h"
#include "UIX_CommonCtrlDefs.h"
#include "UIX_Image.h"
#include "UIX_Button.h"
#include "PayrollTimeClock_ActionToScan.h"
#include "PayrollTimeClock_SharedObjects.h"
#include "PayrollTimeClock_FviFrame.h"

namespace Payroll { namespace time_clock { namespace UI
{
	using ex_ui::draw::defs::IRenderer;
	using ex_ui::controls::defs::IControlNotify;
	using ex_ui::controls::CImage;
	using ex_ui::controls::CButton;
	using ex_ui::frames::IMessageHandler;
	using ex_ui::frames::CPanelBase;

	using Payroll::time_clock::common::CSharedObjects;
	using Payroll::time_clock::ctrl_flow::CFingerScanAction;
	using shared::lite::events::IGenericEventNotify;

	class CMainFramePage_0 :
		public  CPanelBase,
		public  IMessageHandler,
		public  IControlNotify,
		public  IGenericEventNotify
	{
		typedef CPanelBase TPageBase;
	private:
		IRenderer&         m_parent_rnd_ref;
		IControlNotify&    m_parent_snk_ref;
		CSharedObjects&    m_shared;
	private:
		CFingerScanAction  m_action;
		CFvDialogFrame     m_fv_dlg;
		CImage             m_fv_msg;
	private:
		UINT_PTR           m_evt_timer_0; // vein device readyness
		UINT_PTR           m_evt_timer_1; // removing 'unsuccessful' message
	public:
		CMainFramePage_0(IRenderer&, IControlNotify&, CSharedObjects&);
		~CMainFramePage_0(void);
	public: // CPanelBase
		virtual HRESULT    Create(const HWND hParent, const RECT& rcArea, const bool bVisible) override sealed;
		virtual HRESULT    Destroy(void) override sealed;
		virtual HRESULT    Hide(void) override sealed;
		virtual HRESULT    Show(void) override sealed;
		virtual HRESULT    UpdateLayout(LPRECT const = NULL) override sealed;
	private: // IControlNotify
		virtual HRESULT    IControlNotify_OnClick(const UINT ctrlId) override sealed;
	private: // IMessageHandler
		virtual LRESULT    MessageHandler_OnMessage(UINT, WPARAM, LPARAM, BOOL&) override sealed;
	private: // IGenericEventNotify
		virtual HRESULT    GenericEvent_OnNotify(const UINT eventId) override sealed;
	public:
		VOID               CustomizeFvDialog(void);
	};
}}}


#endif/*_PAYROLLTIMECLOCKINGSCANPAGE_H_C06D6DA5_B7C8_4109_A8D4_100FE1E9A98A_INCLUDED*/