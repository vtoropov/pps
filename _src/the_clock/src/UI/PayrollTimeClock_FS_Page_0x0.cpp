/*
	Created by Tech_dog(VToropov) on 11-Feb-2015 at 11:19:51am, GMT+3, Taganrog, Wednesday;
	This is Payroll Time Clocking Finger Scan Page class implementation file.
*/
#include "StdAfx.h"
#include "PayrollTimeClock_FS_Page_0x0.h"
#include "PayrollTimeClock_Resource.h"
#include "PayrollTimeClock_RecorderBase.h"
#include "UIX_GdiObject.h"

using namespace Payroll;
using namespace Payroll::time_clock;
using namespace Payroll::time_clock::common;
using namespace Payroll::time_clock::UI;

using namespace ex_ui;
using namespace ex_ui::controls;

////////////////////////////////////////////////////////////////////////////

namespace Payroll { namespace time_clock { namespace UI { namespace details
{
	UINT  MainFramePage_0_EventTimerId_0(void)
	{
		static const DWORD timer_id = ::GetTickCount();
		return timer_id;
	}

	UINT  MainFramePage_0_EventTimerId_1(void)
	{
		static const DWORD timer_id = MainFramePage_0_EventTimerId_0() + 1;
		return timer_id;
	}

	UINT  MainFramePage_0_EventTimerFreq_0(void)
	{
		static const DWORD timer_freq = 333;
		return timer_freq;
	}

	UINT  MainFramePage_0_EventTimerFreq_1(void)
	{
		static const DWORD timer_freq = 3333;
		return timer_freq;
	}

	class CMainFramePage_0_Layout
	{
	public:
		enum { // from PSD files
			fv_frame_w = 390,
			fv_frame_h = 270,
			fv_msg_w   = 450,
			fv_msg_h   = 310,
		};
	private:
		CWindow&  m_page_ref;
		RECT      m_page_rect;
	public:
		CMainFramePage_0_Layout(CWindow& page_ref) : m_page_ref(page_ref)
		{
			::SetRectEmpty(&m_page_rect);
			if (m_page_ref.IsWindow())
				m_page_ref.GetClientRect(&m_page_rect);
		}
	public:
		RECT     GetFvFrameRect(void)const
		{
			const INT nLeft = (__W(m_page_rect) - CMainFramePage_0_Layout::fv_frame_w * 1) / 2;
			const INT nTop  = (__H(m_page_rect) - CMainFramePage_0_Layout::fv_frame_h * 2) / 2;
			RECT rc_ = {0};
			::SetRect(
					&rc_,
					nLeft,
					nTop,
					nLeft + CMainFramePage_0_Layout::fv_frame_w,
					nTop  + CMainFramePage_0_Layout::fv_frame_h
				);
			return rc_;
		}
		RECT     GetFvMessageRect(void)const
		{
			RECT rc_ = {0}, rc_frm = this->GetFvFrameRect();

			const INT nLeft = (__W(m_page_rect) - CMainFramePage_0_Layout::fv_msg_w) / 2;
			const INT nTop  = rc_frm.bottom;

			::SetRect(
					&rc_,
					nLeft,
					nTop,
					nLeft + CMainFramePage_0_Layout::fv_msg_w,
					nTop  + CMainFramePage_0_Layout::fv_msg_h
				);
			return rc_;
		}
	};
}}}}

CMainFramePage_0::CMainFramePage_0(IRenderer& rnd_ref, IControlNotify& snk_ref, CSharedObjects& obj_ref):
	TPageBase(IDD_PAYROLL_TC_PAGE_SCAN, *this),
	m_parent_rnd_ref(rnd_ref),
	m_parent_snk_ref(snk_ref),
	m_evt_timer_0(NULL),
	m_evt_timer_1(NULL),
	m_action(obj_ref, *this),
	m_fv_dlg(obj_ref, *this, rnd_ref),
	m_shared(obj_ref)
{
	m_fv_msg.SetParentRendererPtr(&rnd_ref);
}

CMainFramePage_0::~CMainFramePage_0(void)
{
}

////////////////////////////////////////////////////////////////////////////

HRESULT    CMainFramePage_0::Create(const HWND hParent, const RECT& rcArea, const bool bVisible)
{
	HRESULT hr_ = TPageBase::Create(hParent, rcArea, bVisible);
	if (S_OK != hr_)
		return  hr_;
	CWindow& host = TPageBase::GetWindow_Ref();

	details::CMainFramePage_0_Layout layout(host);
	{
		RECT rc_ = layout.GetFvFrameRect();
		hr_ = m_fv_dlg.Create(host, rc_);
		if (!m_shared.Settings().IsFVStatusVisible())
			m_fv_dlg.Hide();
	}
	{
		RECT rc_ = layout.GetFvMessageRect();
		hr_ = m_fv_msg.Create(host, rc_);
		hr_ = m_fv_msg.SetImage(IDR_PAYROLL_FS_WND_PAGE_0_FVD_MSG_0);
		hr_ = m_fv_msg.UpdateLayout();
	}
	return hr_;
}

HRESULT    CMainFramePage_0::Destroy(void)
{
	if (m_evt_timer_0)
	{
		TPageBase::GetWindow_Ref().KillTimer(m_evt_timer_0); m_evt_timer_0 = NULL;
	}
	if (!m_action.IsStopped())m_action.Stop(true);
	m_fv_msg.Destroy();
	m_fv_dlg.Destroy();
	HRESULT hr_ = TPageBase::Destroy();
	return  hr_;
}

HRESULT    CMainFramePage_0::Hide(void)
{
	HRESULT hr_ = TPageBase::Hide();
	if (S_OK == hr_)
	{
		if (!m_action.IsStopped())
			m_action.Stop(true);

		if (m_fv_msg.GetWindow_Ref().IsWindow())
		{
			hr_ = m_fv_msg.SetImage(IDR_PAYROLL_FS_WND_PAGE_0_FVD_MSG_0);
			hr_ = m_fv_msg.UpdateLayout();
		};
		m_fv_dlg.Hide();
		if (m_evt_timer_0) { TPageBase::GetWindow_Ref().KillTimer(m_evt_timer_0); m_evt_timer_0 = NULL; }
		if (m_evt_timer_1) { TPageBase::GetWindow_Ref().KillTimer(m_evt_timer_1); m_evt_timer_1 = NULL; }
	}
	return  hr_;
}

HRESULT    CMainFramePage_0::Show(void)
{
	HRESULT hr_ = TPageBase::Show();
	if (S_OK == hr_)
	{
		if (m_shared.Settings().IsFVStatusVisible())
			m_fv_dlg.Show();
		CWindow& host = TPageBase::GetWindow_Ref();

		m_evt_timer_0 = host.SetTimer(
				details::MainFramePage_0_EventTimerId_0(),
				details::MainFramePage_0_EventTimerFreq_0()
			);
	}
	return  hr_;
}

HRESULT    CMainFramePage_0::UpdateLayout(LPRECT const pRect)
{
	HRESULT hr_ = TPageBase::UpdateLayout(pRect);

	details::CMainFramePage_0_Layout layout(TPageBase::GetWindow_Ref());

	if (m_fv_dlg.WindowRef().IsWindow())
	{
		RECT rc_ = layout.GetFvFrameRect();
		hr_ = m_fv_dlg.WindowRef().SetWindowPos(NULL, &rc_, SWP_NOZORDER|SWP_NOACTIVATE);
	}
	if (m_fv_msg.GetWindow_Ref().IsWindow())
	{
		RECT rc_ = layout.GetFvMessageRect();
		m_fv_msg.GetWindow_Ref().SetWindowPos(NULL, &rc_, SWP_NOZORDER|SWP_NOACTIVATE);
	}
	return  hr_;
}

////////////////////////////////////////////////////////////////////////////

HRESULT    CMainFramePage_0::IControlNotify_OnClick(const UINT ctrlId)
{
	switch(ctrlId)
	{
	case NULL:
	default:;
	}
	return S_OK;
}

////////////////////////////////////////////////////////////////////////////

LRESULT    CMainFramePage_0::MessageHandler_OnMessage(UINT uMsg, WPARAM wParam, LPARAM, BOOL& bHandled)
{
	bHandled = FALSE;
	switch (uMsg)
	{
	case WM_TIMER:
		{
			::ATL::CWindow& host = TPageBase::GetWindow_Ref();
			const UINT uTimerId = static_cast<UINT>(wParam);

			if (uTimerId == details::MainFramePage_0_EventTimerId_0())
			{
				host.KillTimer(m_evt_timer_0); m_evt_timer_0 = NULL;
				if (host.IsWindowVisible() && m_action.IsStopped())
				{
					HRESULT hr_ = m_action.Start();
					if (S_OK == hr_)
					{
						this->CustomizeFvDialog();
						m_parent_snk_ref.IControlNotify_OnClick(IDC_PAYROLL_TC_PAGE_SCAN_START);
					}
				}
			}
			if (uTimerId == details::MainFramePage_0_EventTimerId_1())
			{
				host.KillTimer(m_evt_timer_1); m_evt_timer_1 = NULL;
				if (host.IsWindowVisible() && m_fv_msg.GetWindow_Ref().IsWindow())
				{
					m_fv_msg.SetImage(IDR_PAYROLL_FS_WND_PAGE_0_FVD_MSG_0);
					m_fv_msg.UpdateLayout();
					m_fv_msg.Refresh();
				}
			}
		} break;
	}
	return 0;
}

////////////////////////////////////////////////////////////////////////////

HRESULT    CMainFramePage_0::GenericEvent_OnNotify(const UINT eventId)
{
	if (IDC_PAYROLL_TC_PAGE_SCAN_PROCESS == eventId)
	{
		m_fv_dlg.Hide();
		m_fv_msg.SetImage(IDR_PAYROLL_FS_WND_PAGE_0_FVD_MSG_3);
		m_fv_msg.UpdateLayout();
		m_fv_msg.Refresh();
		return S_OK;
	}

	HRESULT hr_ = m_action.Stop(false); // TODO: the runnable object should manage its state after working thread exits;
	if (S_OK == hr_ && TPageBase::GetWindow_Ref().IsWindowVisible())
	{
		hr_ = m_action.Result();
		if (false){}
		else if (S_OK == hr_)
		{
			m_parent_snk_ref.IControlNotify_OnClick(IDC_PAYROLL_TC_PAGE_SCAN_OK);
		}
		else if (S_FALSE == hr_)
		{
			if (m_fv_msg.GetWindow_Ref().IsWindow() && NULL == m_evt_timer_0)
			{
				m_fv_msg.SetImage(IDR_PAYROLL_FS_WND_PAGE_0_FVD_MSG_1);
				m_fv_msg.UpdateLayout();
				m_fv_msg.Refresh();

				LPCTSTR pszSoundFile = m_shared.Settings().GetFailureSoundFile();
				if (pszSoundFile)
					::PlaySound( pszSoundFile, NULL, SND_FILENAME|SND_ASYNC );

				CRecorderBase* const pRecorder = m_shared.Recorder(); // re-initializes the finger-vein data, maybe new data has arrived.
				if (pRecorder)
					pRecorder->Reinitialize();

				if (m_shared.Settings().IsFVStatusVisible())
					m_fv_dlg.Show();
				m_evt_timer_0 = TPageBase::GetWindow_Ref().SetTimer(
						details::MainFramePage_0_EventTimerId_0(),
						details::MainFramePage_0_EventTimerFreq_0()
					);
				if (m_evt_timer_1 == NULL)
					m_evt_timer_1 = TPageBase::GetWindow_Ref().SetTimer(
						details::MainFramePage_0_EventTimerId_1(),
						details::MainFramePage_0_EventTimerFreq_1()
					);
			}
		}
		else
		{
			m_parent_snk_ref.IControlNotify_OnClick(IDC_PAYROLL_TC_PAGE_SCAN_ERR);
		}
	}
	return  hr_;
}

////////////////////////////////////////////////////////////////////////////

VOID       CMainFramePage_0::CustomizeFvDialog(void)
{
	if (!m_shared.IsInitialized())
		return;
	const eFvCustomizeType::_e eCustomizationType = m_shared.Settings().FvCustomization();
	if (eCustomizationType != eFvCustomizeType::eNone)
		m_fv_dlg.CustomizeDialog();
}