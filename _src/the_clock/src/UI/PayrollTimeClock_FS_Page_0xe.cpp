/*
	Created by Tech_dog (VToropov) on 10-Feb-2015 at 12:25:18pm, GMT+3, Taganrog, Tuesday;
	This is Payroll Time Clocking Device Error Page class implementation file.
*/
#include "StdAfx.h"
#include "PayrollTimeClock_FS_Page_0xe.h"
#include "PayrollTimeClock_Resource.h"

using namespace Payroll;
using namespace Payroll::time_clock;
using namespace Payroll::time_clock::UI;

using namespace ex_ui;
using namespace ex_ui::controls;

////////////////////////////////////////////////////////////////////////////

namespace Payroll { namespace time_clock { namespace UI { namespace details
{
	class CMainFramePage_0e_Layout
	{
	public:
		enum { // from PSD files
			err_msg_w = 800,
			err_msg_h =  95,
			err_img_w = 300,
			err_img_h = 300,
			btn_rty_h = 101,
			btn_rty_w = 446,
		};
	private:
		CWindow&  m_page_ref;
		RECT      m_page_rect;
	public:
		CMainFramePage_0e_Layout(CWindow& page_ref) : m_page_ref(page_ref)
		{
			::SetRectEmpty(&m_page_rect);
			if (m_page_ref.IsWindow())
				m_page_ref.GetClientRect(&m_page_rect);
		}
	public:
		RECT      GetImageRect(void)const
		{
			RECT rc_ = this->GetMessageRect();
			rc_.left = m_page_rect.left + (__W(m_page_rect) - CMainFramePage_0e_Layout::err_img_w)/ 2;
			rc_.top -= CMainFramePage_0e_Layout::err_img_h  - 5;
			return rc_;
		}

		RECT      GetMessageRect(void)const
		{
			RECT rc_ = m_page_rect;
			rc_.top   += (__H(rc_) - CMainFramePage_0e_Layout::err_msg_h)/2;
			rc_.bottom = rc_.top   + CMainFramePage_0e_Layout::err_msg_h;
			rc_.left  += (__W(rc_) - CMainFramePage_0e_Layout::err_msg_w)/2;
			rc_.right  = rc_.left  + CMainFramePage_0e_Layout::err_msg_w;
			return rc_;
		}
		RECT      GetRetryBtnRect(void)const
		{
			RECT rc_ = this->GetMessageRect();
			rc_.top  = rc_.bottom  + 30;
			rc_.bottom = rc_.top   + CMainFramePage_0e_Layout::btn_rty_h;
			rc_.right  = m_page_rect.right - 30;
			rc_.left   = rc_.right - CMainFramePage_0e_Layout::btn_rty_w;
			return rc_;
		}
	};
}}}}

////////////////////////////////////////////////////////////////////////////

CMainFramePage_0e::CMainFramePage_0e(IRenderer& rnd_ref, IControlNotify& snk_ref):
	TPageBase(IDD_PAYROLL_TC_PAGE_ERR, *this),
	m_parent_rnd_ref(rnd_ref),
	m_parent_snk_ref(snk_ref),
	m_btn_retry(IDC_PAYROLL_FS_WND_PAGE_E_RTY, rnd_ref, snk_ref)
{
	m_err_img.BackColor(RGB(0xee, 0xee, 0xee), 255);
	m_err_img.SetParentRendererPtr(&m_parent_rnd_ref);
	m_err_msg.SetParentRendererPtr(&m_parent_rnd_ref);

	HRESULT hr_ = S_OK;
	const HINSTANCE hInstance = ::ATL::_AtlBaseModule.GetModuleInstance();
	hr_ = m_btn_retry.SetImage(eControlState::eNormal  , IDR_PAYROLL_FS_WND_PAGE_E_RTY_N, hInstance); ATLASSERT(S_OK == hr_);
	hr_ = m_btn_retry.SetImage(eControlState::eHovered , IDR_PAYROLL_FS_WND_PAGE_E_RTY_H, hInstance); ATLASSERT(S_OK == hr_);
	hr_ = m_btn_retry.SetImage(eControlState::ePressed , IDR_PAYROLL_FS_WND_PAGE_E_RTY_P, hInstance); ATLASSERT(S_OK == hr_);

	m_btn_retry.BackColor(RGB(0xee, 0xee, 0xee), 255);
}

CMainFramePage_0e::~CMainFramePage_0e(void)
{
}

////////////////////////////////////////////////////////////////////////////

HRESULT    CMainFramePage_0e::Create(const HWND hParent, const RECT& rcArea, const bool bVisible)
{
	HRESULT hr_ = TPageBase::Create(hParent, rcArea, bVisible);
	if (S_OK != hr_)
		return  hr_;
	details::CMainFramePage_0e_Layout layout(TPageBase::GetWindow_Ref());
	{
		RECT rc_ = layout.GetImageRect();
		hr_ = m_err_img.Create(TPageBase::GetWindow_Ref(), rc_);
		hr_ = m_err_img.SetImage(IDR_PAYROLL_FS_WND_PAGE_E_IMG);
		hr_ = m_err_img.UpdateLayout();
	}
	{
		RECT rc_ = layout.GetMessageRect();
		hr_ = m_err_msg.Create(TPageBase::GetWindow_Ref(), rc_);
		hr_ = m_err_msg.SetImage(IDR_PAYROLL_FS_WND_PAGE_E_MSG);
		hr_ = m_err_msg.UpdateLayout();
	}
	{
		RECT rc_ = layout.GetRetryBtnRect();
		hr_ = m_btn_retry.Create(TPageBase::GetWindow_Ref(), &rc_, false);
	}
	return hr_;
}

HRESULT    CMainFramePage_0e::Destroy(void)
{
	m_err_img.Destroy();
	m_err_msg.Destroy();
	m_btn_retry.Destroy();
	HRESULT hr_ = TPageBase::Destroy();
	return  hr_;
}

HRESULT    CMainFramePage_0e::UpdateLayout(LPRECT const pRect)
{
	HRESULT hr_ = TPageBase::UpdateLayout(pRect);

	details::CMainFramePage_0e_Layout layout(TPageBase::GetWindow_Ref());

	if (m_err_img.GetWindow_Ref().IsWindow())
	{
		RECT rc_ = layout.GetImageRect();
		m_err_img.GetWindow_Ref().SetWindowPos(NULL, &rc_, SWP_NOZORDER|SWP_NOSIZE|SWP_NOACTIVATE|SWP_NOSENDCHANGING);
	}
	if (m_err_msg.GetWindow_Ref().IsWindow())
	{
		RECT rc_ = layout.GetMessageRect();
		m_err_msg.GetWindow_Ref().SetWindowPos(NULL, &rc_, SWP_NOZORDER|SWP_NOSIZE|SWP_NOACTIVATE|SWP_NOSENDCHANGING);
	}
	if (m_btn_retry.GetWindow().IsWindow())
	{
		RECT rc_ = layout.GetRetryBtnRect();
		m_btn_retry.GetWindow().SetWindowPos(NULL, &rc_, SWP_NOZORDER|SWP_NOSIZE|SWP_NOACTIVATE|SWP_NOSENDCHANGING);
	}
	return  hr_;
}

////////////////////////////////////////////////////////////////////////////

HRESULT    CMainFramePage_0e::IControlNotify_OnClick(const UINT ctrlId)
{
	switch(ctrlId)
	{
	case NULL:
	default:;
	}
	return S_OK;
}