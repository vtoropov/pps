#ifndef __PAYROLLTIMECLOCKINGRECORDWINDOW_H_2AEE06FE_4C70_4b13_A5CB_E51E85C3B41B_INCLUDED
#define __PAYROLLTIMECLOCKINGRECORDWINDOW_H_2AEE06FE_4C70_4b13_A5CB_E51E85C3B41B_INCLUDED
/*
	Created by Tech_dog (VToropov) on 4-Feb-2015 at 2:40:31am, GMT+3, Taganrog, Wednesday;
	This is Platinum Payroll Systems Time Clocking Record Full-Screen Window class declaration file.
*/
#include "UIX_Renderer.h"
#include "UIX_Image.h"
#include "PayrollTimeClock_FS_Page_0xe.h"
#include "PayrollTimeClock_FS_Page_0x0.h"
#include "PayrollTimeClock_FS_Page_0x1.h"
#include "PayrollTimeClock_FS_Page_0x2.h"
#include "PayrollTimeClock_FS_Page_0x3.h"
#include "PayrollTimeClock_FS_Page_0xse.h"
#include "PayrollTimeClock_SharedObjects.h"
#include "PayrollTimeClock_ActionToLockSysWnd.h"

namespace Payroll { namespace time_clock { namespace UI
{
	using ex_ui::controls::defs::IControlNotify;
	using ex_ui::controls::CImage;
	using ex_ui::draw::renderers::CBackgroundTileRenderer;

	using Payroll::time_clock::common::CSharedObjects;
	using Payroll::time_clock::ctrl_flow::CSysWndLockAction;

	class CMainFrame : public IControlNotify
	{
	private:
		class CMainFrameWnd : public ::ATL::CWindowImpl<CMainFrameWnd>, public  IGenericEventNotify
		{
			typedef ::ATL::CWindowImpl<CMainFrameWnd> TWindow;
			friend class CMainFrame;
		private:
			CBackgroundTileRenderer     m_bkgnd_renderer;
			IControlNotify&             m_ctrl_notify;
			CImage                      m_header;
			CSharedObjects&             m_shared;
		private:
			CSysWndLockAction           m_wnd_locker;
			CMainFramePage_0e           m_page_e;
			CMainFramePage_1e           m_page_s;
			CMainFramePage_0            m_page_0;
			CMainFramePage_1            m_page_1;
			CMainFramePage_2            m_page_2;
			CMainFramePage_3            m_page_3;
		public:
			DECLARE_WND_CLASS_EX(_T("Payroll::time_clock::MainFrameWnd"), CS_HREDRAW | CS_VREDRAW | CS_DBLCLKS, COLOR_3DLIGHT + 1);
		public:
			BEGIN_MSG_MAP(CMainFrameWnd)
				MESSAGE_HANDLER(WM_CREATE        , OnCreate  )
				MESSAGE_HANDLER(WM_DESTROY       , OnDestroy )
				MESSAGE_HANDLER(WM_ERASEBKGND    , OnErase   )
				MESSAGE_HANDLER(WM_KEYDOWN       , OnKeyDown )
				MESSAGE_HANDLER(WM_SYSCOMMAND    , OnSysCmd  )
				MESSAGE_HANDLER(WM_SIZE          , OnSize    )
			END_MSG_MAP()
		private:
			LRESULT OnCreate  (UINT, WPARAM, LPARAM, BOOL&);
			LRESULT OnDestroy (UINT, WPARAM, LPARAM, BOOL&);
			LRESULT OnErase   (UINT, WPARAM, LPARAM, BOOL&);
			LRESULT OnKeyDown (UINT, WPARAM, LPARAM, BOOL&);
			LRESULT OnSysCmd  (UINT, WPARAM, LPARAM, BOOL&);
			LRESULT OnSize    (UINT, WPARAM, LPARAM, BOOL&);
		public:
			CMainFrameWnd(IControlNotify&, CSharedObjects&);
			~CMainFrameWnd(void);
		private: // IGenericEventNotify
			virtual HRESULT    GenericEvent_OnNotify(const UINT eventId) override sealed;
		};
	private:
		CSharedObjects&     m_shared;
		CMainFrameWnd       m_frame;
	public:
		CMainFrame(CSharedObjects&);
		~CMainFrame(void);
	private: // IControlNotify
		virtual    HRESULT  IControlNotify_OnClick(const UINT ctrlId) override sealed;
	public:
		HRESULT             Create(void);
	private:
		CMainFrame(const CMainFrame&);
		CMainFrame& operator= (const CMainFrame&);
	};
}}}

#endif/*__PAYROLLTIMECLOCKINGRECORDWINDOW_H_2AEE06FE_4C70_4b13_A5CB_E51E85C3B41B_INCLUDED*/