#ifndef _PAYROLLTIMECLOCKPASSWORDPAGE_H_D47F004C_2740_442b_BFE0_FBCAC7609BF7_INCLUDED
#define _PAYROLLTIMECLOCKPASSWORDPAGE_H_D47F004C_2740_442b_BFE0_FBCAC7609BF7_INCLUDED
/*
	Created by Tech_dog (VToropov) on 16-Feb-2015 at 6:28:14am, GMT+3, Taganrog, Monday;
	This is Payroll Time Clocking Password Page class declaration file.
*/
#include "UIX_PanelBase.h"
#include "UIX_CommonDrawDefs.h"
#include "UIX_CommonCtrlDefs.h"
#include "UIX_Image.h"
#include "UIX_Button.h"
#include "PayrollTimeClock_SharedObjects.h"

namespace Payroll { namespace time_clock { namespace UI
{
	using ex_ui::draw::defs::IRenderer;
	using ex_ui::controls::defs::IControlNotify;
	using ex_ui::controls::CImage;
	using ex_ui::controls::CButton;
	using ex_ui::frames::IMessageHandler;
	using ex_ui::frames::CPanelBase;

	using Payroll::time_clock::common::CSharedObjects;

	class CEditCtrl_Ex : public ::ATL::CWindowImpl<CEditCtrl_Ex>
	{
	private:
		IControlNotify&   m_sink;
	public:
		BEGIN_MSG_MAP(CEditCtrl_Ex)
			MESSAGE_HANDLER(WM_KEYDOWN, OnKeyDown )
		END_MSG_MAP()
	private:
		LRESULT OnKeyDown (UINT, WPARAM, LPARAM, BOOL&);
	public:
		CEditCtrl_Ex(IControlNotify&);
	};

	class CMainFramePage_3 :
		public  CPanelBase,
		public  IMessageHandler
	{
		typedef CPanelBase TPageBase;
	private:
		IRenderer&         m_parent_rnd_ref;
		IControlNotify&    m_parent_snk_ref;
		CSharedObjects&    m_shared;
	private:
		mutable CImage     m_res_msg;
		CButton            m_btn_cls;
		CButton            m_btn_ret;
		CAtlString         m_password;
		CEditCtrl_Ex       m_pass_ctrl;
	public:
		CMainFramePage_3(IRenderer&, IControlNotify&, CSharedObjects&);
		~CMainFramePage_3(void);
	public: // CPanelBase
		virtual HRESULT    Create(const HWND hParent, const RECT& rcArea, const bool bVisible) override sealed;
		virtual HRESULT    Destroy(void) override sealed;
		virtual HRESULT    Show(void) override sealed;
		virtual HRESULT    UpdateLayout(LPRECT const = NULL) override sealed;
	private: // IMessageHandler
		virtual LRESULT    MessageHandler_OnMessage(UINT, WPARAM, LPARAM, BOOL&) override sealed;
	public:
		HRESULT            ComparePassword(void)const;
		HRESULT            OnEnter(void);
	};
}}}

#endif/*_PAYROLLTIMECLOCKPASSWORDPAGE_H_D47F004C_2740_442b_BFE0_FBCAC7609BF7_INCLUDED*/