#ifndef _PAYROLLTIMECLOCKINGFVSCANNERFRAME_H_AF3B2AC1_C0EB_42d9_B211_09AFF4A734B7_INCLUDED
#define _PAYROLLTIMECLOCKINGFVSCANNERFRAME_H_AF3B2AC1_C0EB_42d9_B211_09AFF4A734B7_INCLUDED
/*
	Created by Tech_dog (VToropov) on 11-Feb-2015 at 4:27:06pm, GMT+3, Taganrog, Wednesday;
	This is Payroll Time Clocking FV Scanner Dialog Frame class(es) declaration file.
*/
#include "UIX_Image.h"
#include "UIX_CommonDrawDefs.h"
#include "PayrollTimeClock_ActionToFvCustom.h"
#include "PayrollTimeClock_SharedObjects.h"

namespace Payroll { namespace time_clock { namespace UI
{
	using ex_ui::controls::CImage;
	using ex_ui::draw::defs::IRenderer;

	using Payroll::time_clock::common::CSharedObjects;
	using Payroll::time_clock::ctrl_flow::CFvCustomizeAction;
	using shared::lite::events::IGenericEventNotify;

	class CFvDialogFrame :
		public  IGenericEventNotify
	{
	private:
		class CFvDialogFrameWnd : 
			public  ::ATL::CWindowImpl<CFvDialogFrameWnd>
		{
			typedef ::ATL::CWindowImpl<CFvDialogFrameWnd> TWindow;
		private:
			CSharedObjects& m_shared;
			IRenderer&      m_parent_rnd;
			CImage          m_fv_msg;
		public:
			DECLARE_WND_CLASS(_T("Payroll::time_clock::FvDialogFrame::Window"));

			BEGIN_MSG_MAP(CFvDialogFrameWnd)
				MESSAGE_HANDLER(WM_CREATE     , OnCreate )
				MESSAGE_HANDLER(WM_DESTROY    , OnDestroy)
			END_MSG_MAP()
		private:
			LRESULT OnCreate  (UINT, WPARAM, LPARAM, BOOL&);
			LRESULT OnDestroy (UINT, WPARAM, LPARAM, BOOL&);
		public:
			CFvDialogFrameWnd(CSharedObjects&, IRenderer&);
			~CFvDialogFrameWnd(void);
		};
	private:
		CFvDialogFrameWnd   m_fv_dlg;
		CFvCustomizeAction  m_fv_act;
	public:
		CFvDialogFrame(CSharedObjects&, IGenericEventNotify&, IRenderer&);
		~CFvDialogFrame(void);
	private: // IGenericEventNotify
		virtual HRESULT    GenericEvent_OnNotify(const UINT eventId) override sealed;
	public:
		HRESULT             Create(const HWND hParent, const RECT rcArea);
		HRESULT             CustomizeDialog(void);
		HRESULT             Destroy(void);
		HRESULT             Hide(void);
		HRESULT             Show(void);
		CWindow&            WindowRef(void);
	};
}}}


#endif/*_PAYROLLTIMECLOCKINGFVSCANNERFRAME_H_AF3B2AC1_C0EB_42d9_B211_09AFF4A734B7_INCLUDED*/