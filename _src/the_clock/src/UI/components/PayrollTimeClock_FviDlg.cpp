/*
	Created by Tech_dog (VToropov) on 26-May-2015, at 10:15:32am, GMT+8, Phuket, Tuesday;
	This is Time Clocking FV Scanner Dialog Wrapper class implementation file.
*/
#include "StdAfx.h"
#include "PayrollTimeClock_FviDlg.h"
#include "Shared_GenericSyncObject.h"

using namespace Payroll;
using namespace Payroll::time_clock;
using namespace Payroll::time_clock::UI;

using namespace shared::lite::runnable;
////////////////////////////////////////////////////////////////////////////

namespace Payroll { namespace time_clock { namespace UI { namespace details
{
	class FvDialogWrapper_Layout
	{
	private:
		CWindow      m_dlg;
		RECT         m_dlg_rc; // dialog window rectangle in screen coordinates;
		TITLEBARINFOEX m_info;
	public:
		FvDialogWrapper_Layout(const CWindow& dlg_ref) : m_dlg(dlg_ref)
		{
			if (m_dlg) {
				m_dlg.GetWindowRect(&m_dlg_rc);
				::RtlZeroMemory(&m_info, sizeof(TITLEBARINFOEX));
				m_info.cbSize = sizeof(TITLEBARINFOEX);
				// https://docs.microsoft.com/en-us/windows/win32/menurc/wm-gettitlebarinfoex
				m_dlg.SendMessageW(WM_GETTITLEBARINFOEX, 0, (LPARAM)&m_info);
			}
			else
				::SetRectEmpty(&m_dlg_rc);
		}
	public:
		bool         IsMinimizeButtonPressed(const POINT _pt)const
		{
			if (!::PtInRect(&m_dlg_rc, _pt))
				return false;
#if (0)
			INT nLeft = m_dlg_rc.right;
			nLeft -= ::GetSystemMetrics(SM_CXDLGFRAME);
			nLeft -= ::GetSystemMetrics(SM_CXSIZE) * 3;

			const RECT rcButton = {
					nLeft,
					m_dlg_rc.top,
					::GetSystemMetrics(SM_CXSIZE) + nLeft,
					::GetSystemMetrics(SM_CYSIZE) + m_dlg_rc.top 
				};

			return !!::PtInRect(&rcButton, _pt);
#else
			// https://docs.microsoft.com/en-us/windows/win32/api/winuser/ns-winuser-titlebarinfoex
			static const DWORD n_min_button_ndx = 2;
			return !!::PtInRect(&m_info.rgrect[n_min_button_ndx], _pt);
#endif
		}
	};

	CGenericSyncObject&
	                 FvDialogWrapper_SafeGuardRef(void)
	{
		static CGenericSyncObject sync_obj;
		return sync_obj;
	}

	#define SAFE_LOCK_DLG() SAFE_LOCK(details::FvDialogWrapper_SafeGuardRef());

	HHOOK&           FvDialogWrapper_HookHandle(VOID)
	{
		static HHOOK hHandle = NULL;
		return hHandle;
	}

	CWindow&         FvDialogWrapper_DlgHandle(void)
	{
		static CWindow dlgHandle;
		return dlgHandle;
	}

	LRESULT CALLBACK FvDialogWrapper_HookProc(INT nCode, WPARAM wParam, LPARAM lParam)
	{
		if (HC_ACTION == nCode)
		{
			switch (wParam)
			{
			case WM_LBUTTONUP:
				{
					MOUSEHOOKSTRUCT*  pMouseData = reinterpret_cast<MOUSEHOOKSTRUCT*>(lParam);
					SAFE_LOCK_DLG();
					if (FvDialogWrapper_DlgHandle() == pMouseData->hwnd)
					{
						FvDialogWrapper_Layout layout_(pMouseData->hwnd);
						if (layout_.IsMinimizeButtonPressed(pMouseData->pt))
						{
							WINDOWPLACEMENT wnd_pace = {0};
							wnd_pace.length = sizeof(wnd_pace);
							FvDialogWrapper_DlgHandle().GetWindowPlacement(&wnd_pace);
							wnd_pace.showCmd = SW_SHOWMINIMIZED;
							FvDialogWrapper_DlgHandle().SetWindowPlacement(&wnd_pace);
						}
					}
				} break;
			}
		}
		return ::CallNextHookEx(FvDialogWrapper_HookHandle(), nCode, wParam, lParam);
	}
}}}}
using namespace Payroll::time_clock::UI::details;
////////////////////////////////////////////////////////////////////////////

CFvDialogWrapper::CFvDialogWrapperWnd::CFvDialogWrapperWnd(void)
{
}

CFvDialogWrapper::CFvDialogWrapperWnd::~CFvDialogWrapperWnd(void)
{
}

////////////////////////////////////////////////////////////////////////////

LRESULT CFvDialogWrapper::CFvDialogWrapperWnd::OnHitTest (UINT uMsg, WPARAM wParam, LPARAM lParam, BOOL& bHandled)
{
	bHandled = FALSE; uMsg;
	const LRESULT lResult = TWindow::DefWindowProc(uMsg, wParam, lParam); 
	return lResult;
}

LRESULT CFvDialogWrapper::CFvDialogWrapperWnd::OnSysCmd  (UINT uMsg, WPARAM, LPARAM, BOOL& bHandled)
{
	bHandled = FALSE; uMsg;
	return 0;
}

////////////////////////////////////////////////////////////////////////////

CFvDialogWrapper::CFvDialogWrapper(CSharedObjects& obj_ref) : m_shared(obj_ref), m_fv_act(obj_ref, *this)
{
}

CFvDialogWrapper::~CFvDialogWrapper(void)
{
}

////////////////////////////////////////////////////////////////////////////

HRESULT    CFvDialogWrapper::GenericEvent_OnNotify(const UINT eventId)
{
	eventId;
	m_fv_act.Stop(false);
	HWND hDialog = m_fv_act.Dialog();
	if (::IsWindow(hDialog))
	{
		SAFE_LOCK_DLG();
		m_fv_dlg.SubclassWindow(hDialog); 
		details::FvDialogWrapper_DlgHandle() = hDialog;
	}
	return S_OK;
}

////////////////////////////////////////////////////////////////////////////

HRESULT    CFvDialogWrapper::Create(void)
{
	const eFvCustomizeType::_e eType = m_shared.Settings().FvCustomization();
	if (eFvCustomizeType::eMinimize != eType)
		return S_OK;

	HRESULT hr_ = S_OK;
	if (FvDialogWrapper_HookHandle())
		return (hr_ = HRESULT_FROM_WIN32(ERROR_OBJECT_ALREADY_EXISTS));

	const HINSTANCE hInstance = ::ATL::_AtlBaseModule.GetModuleInstance();
	FvDialogWrapper_HookHandle() = ::SetWindowsHookEx(
										WH_MOUSE,
										details::FvDialogWrapper_HookProc,
										hInstance,
										::GetCurrentThreadId()
									);
	if (!FvDialogWrapper_HookHandle())
		return (hr_ = HRESULT_FROM_WIN32(::GetLastError()));
	if (m_fv_act.IsStopped())
		hr_ = m_fv_act.Start();
	return  hr_;
}

HRESULT    CFvDialogWrapper::Destroy(void)
{
	if (m_fv_act.IsStopped() == false)
		m_fv_act.Stop(false);
	if (FvDialogWrapper_HookHandle())
	{
		::UnhookWindowsHookEx(FvDialogWrapper_HookHandle());
		FvDialogWrapper_HookHandle() = NULL;
	}
	SAFE_LOCK_DLG();
	details::FvDialogWrapper_DlgHandle() = NULL;
	return S_OK;
}