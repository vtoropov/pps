/*
	Created by Tech_dog (VToropov) on 11-Feb-2015 at 4:39:23pm, GMT+3, Taganrog, Wednesday;
	This is Payroll Time Clocking FV Scanner Dialog Frame class(es) implementation file.
*/
#include "StdAfx.h"
#include "PayrollTimeClock_FviFrame.h"
#include "PayrollTimeClock_Resource.h"
#include "UIX_GdiProvider.h"

using namespace Payroll;
using namespace Payroll::time_clock;
using namespace Payroll::time_clock::UI;

using namespace ex_ui::draw;

////////////////////////////////////////////////////////////////////////////

namespace Payroll { namespace time_clock { namespace UI { namespace details
{
	class CFvDialogFrame_Layout
	{
	private:
		CWindow&  m_frame_ref;
		RECT      m_frame_rect;
	public:
		CFvDialogFrame_Layout(CWindow& frame_ref) : m_frame_ref(frame_ref)
		{
			::SetRectEmpty(&m_frame_rect);
			if (m_frame_ref.IsWindow())
				m_frame_ref.GetClientRect(&m_frame_rect);
		}
	public:
		RECT      GetMsgRect(void)const
		{
			RECT rc_ = m_frame_rect;
			return rc_;
		}
	};
}}}}

////////////////////////////////////////////////////////////////////////////

CFvDialogFrame::CFvDialogFrameWnd::CFvDialogFrameWnd(CSharedObjects& shared_ref, IRenderer& rnd_ref) : m_parent_rnd(rnd_ref), m_shared(shared_ref)
{
	m_fv_msg.SetParentRendererPtr(&m_parent_rnd);
}

CFvDialogFrame::CFvDialogFrameWnd::~CFvDialogFrameWnd(void)
{
}

////////////////////////////////////////////////////////////////////////////

LRESULT CFvDialogFrame::CFvDialogFrameWnd::OnCreate  (UINT, WPARAM, LPARAM, BOOL&)
{
	details::CFvDialogFrame_Layout layout(*this);
	HRESULT hr_ = S_OK;
	{
		RECT rc_ = layout.GetMsgRect();
		hr_ = m_fv_msg.Create(*this, rc_);
		hr_ = m_fv_msg.SetImage(IDR_PAYROLL_FS_WND_PAGE_0_FVD_MSG_2);
		hr_ = m_fv_msg.UpdateLayout();
	}
	return 0;
}

LRESULT CFvDialogFrame::CFvDialogFrameWnd::OnDestroy (UINT, WPARAM, LPARAM, BOOL& bHandled)
{
	bHandled = FALSE;
	m_fv_msg.Destroy();
	return 0;
}

////////////////////////////////////////////////////////////////////////////

CFvDialogFrame::CFvDialogFrame(CSharedObjects& shared_ref, IGenericEventNotify& snk_ref, IRenderer& rnd_ref) : 
	m_fv_dlg(shared_ref, rnd_ref),
	m_fv_act(shared_ref, *this, m_fv_dlg)
{
	snk_ref;
}

CFvDialogFrame::~CFvDialogFrame(void)
{
}

////////////////////////////////////////////////////////////////////////////

HRESULT    CFvDialogFrame::GenericEvent_OnNotify(const UINT eventId)
{
	eventId;
	m_fv_act.Stop(false);
	return S_OK;
}

////////////////////////////////////////////////////////////////////////////

HRESULT    CFvDialogFrame::Create(const HWND hParent, const RECT rcArea)
{
	if (!::IsWindow(hParent))
		return E_INVALIDARG;

	if (m_fv_dlg.IsWindow())
		return S_OK;

	RECT rc_ = rcArea;

	HWND hWnd = m_fv_dlg.Create(hParent, rc_, 0, WS_VISIBLE|WS_CHILD|WS_CLIPCHILDREN);
	return (NULL == hWnd ? HRESULT_FROM_WIN32(::GetLastError()) : S_OK);
}

HRESULT    CFvDialogFrame::CustomizeDialog(void)
{
	HRESULT hr_ = S_OK;
	if (m_fv_act.IsStopped())
		hr_ = m_fv_act.Start();
	return  hr_;
}

HRESULT    CFvDialogFrame::Destroy(void)
{
	if (!m_fv_act.IsStopped())
		m_fv_act.Stop(false);

	if (!m_fv_dlg.IsWindow())
		return S_OK;
	m_fv_dlg.SendMessage(WM_CLOSE);
	return S_OK;
}

HRESULT    CFvDialogFrame::Hide(void)
{
	if (!m_fv_dlg.IsWindow())
		return OLE_E_BLANK;
	m_fv_dlg.ShowWindow(SW_HIDE);
	return S_OK;
}

HRESULT    CFvDialogFrame::Show(void)
{
	if (!m_fv_dlg.IsWindow())
		return OLE_E_BLANK;
	m_fv_dlg.ShowWindow(SW_SHOW);
	return S_OK;
}

CWindow&   CFvDialogFrame::WindowRef(void)
{
	return m_fv_dlg;
}