#ifndef _TIMECLOCKINGFVSCANNERDIALOGWRAPPER_H_29A9699D_2EB8_454c_B3A6_2A4255F4E514_INCLUDED
#define _TIMECLOCKINGFVSCANNERDIALOGWRAPPER_H_29A9699D_2EB8_454c_B3A6_2A4255F4E514_INCLUDED
/*
	Created by Tech_dog (VToropov) on 24-May-2015, at 6:46:16pm, GMT+8, Phuket, Sunday;
	This is Time Clocking FV Scanner Dialog Wrapper class declaration file.
*/
#include "PayrollTimeClock_SharedObjects.h"
#include "PayrollTimeClock_ActionToFvMin.h"

namespace Payroll { namespace time_clock { namespace UI
{
	using Payroll::time_clock::common::CSharedObjects;
	using Payroll::time_clock::common::eFvCustomizeType;
	using Payroll::time_clock::ctrl_flow::CFvMinimizeAction;
	using shared::lite::events::IGenericEventNotify;

	class CFvDialogWrapper :
		public  IGenericEventNotify
	{
	private:
		class CFvDialogWrapperWnd: 
			public  ::ATL::CWindowImpl<CFvDialogWrapperWnd>
		{
			typedef ::ATL::CWindowImpl<CFvDialogWrapperWnd> TWindow;
		public:
			DECLARE_WND_CLASS(_T("Payroll::time_clock::FvDialogWrapper::Window"));
		public:
			BEGIN_MSG_MAP(CFvDialogWrapperWnd)
				MESSAGE_HANDLER(WM_NCHITTEST , OnHitTest)
				MESSAGE_HANDLER(WM_SYSCOMMAND, OnSysCmd )
			END_MSG_MAP()
		private:
			LRESULT OnHitTest (UINT, WPARAM, LPARAM, BOOL&);
			LRESULT OnSysCmd  (UINT, WPARAM, LPARAM, BOOL&);
		public:
			CFvDialogWrapperWnd(void);
			~CFvDialogWrapperWnd(void);
		};
	private:
		CSharedObjects&       m_shared;
		CFvDialogWrapperWnd   m_fv_dlg;
		CFvMinimizeAction     m_fv_act;
	public:
		CFvDialogWrapper(CSharedObjects&);
		~CFvDialogWrapper(void);
	private: // IGenericEventNotify
		virtual HRESULT    GenericEvent_OnNotify(const UINT eventId) override sealed;
	public:
		HRESULT     Create(void);
		HRESULT     Destroy(void);
	};
}}}

#endif/*_TIMECLOCKINGFVSCANNERDIALOGWRAPPER_H_29A9699D_2EB8_454c_B3A6_2A4255F4E514_INCLUDED*/