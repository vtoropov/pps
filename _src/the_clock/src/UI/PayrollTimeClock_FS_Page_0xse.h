#ifndef _PAYROLLTIMECLOCKINGSAVEERRORPAGE_H_18CBAE4E_504C_45b9_907D_81E32A1B9160_INCLUDED
#define _PAYROLLTIMECLOCKINGSAVEERRORPAGE_H_18CBAE4E_504C_45b9_907D_81E32A1B9160_INCLUDED
/*
	Created by Tech_dog(VToropov) on 16-Feb-2015 at 3:48:31am, GMT+3, Taganrog, Monday;
	This is Payroll Time Clocking Data Saving Error Page class declaration file.
*/
#include "UIX_PanelBase.h"
#include "UIX_CommonDrawDefs.h"
#include "UIX_CommonCtrlDefs.h"
#include "UIX_Image.h"
#include "UIX_Button.h"
#include "PayrollTimeClock_SharedObjects.h"

namespace Payroll { namespace time_clock { namespace UI
{
	using ex_ui::draw::defs::IRenderer;
	using ex_ui::controls::defs::IControlNotify;
	using ex_ui::controls::CImage;
	using ex_ui::controls::CButton;
	using ex_ui::frames::IMessageHandler;
	using ex_ui::frames::CPanelBase;

	using Payroll::time_clock::common::CSharedObjects;

	class CMainFramePage_1e :
		public  CPanelBase,
		public  IMessageHandler
	{
		typedef CPanelBase TPageBase;
	private:
		IRenderer&         m_parent_rnd_ref;
		IControlNotify&    m_parent_snk_ref;
		CSharedObjects&    m_shared;
	private:
		CImage             m_res_msg;
		CButton            m_btn_ret;
	public:
		CMainFramePage_1e(IRenderer&, IControlNotify&, CSharedObjects&);
		~CMainFramePage_1e(void);
	public: // CPanelBase
		virtual HRESULT    Create(const HWND hParent, const RECT& rcArea, const bool bVisible) override sealed;
		virtual HRESULT    Destroy(void) override sealed;
		virtual HRESULT    UpdateLayout(LPRECT const = NULL) override sealed;
	private: // IMessageHandler
		virtual LRESULT    MessageHandler_OnMessage(UINT, WPARAM, LPARAM, BOOL&) override sealed;
	};
}}}

#endif/*_PAYROLLTIMECLOCKINGSAVEERRORPAGE_H_18CBAE4E_504C_45b9_907D_81E32A1B9160_INCLUDED*/