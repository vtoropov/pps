#ifndef _PAYROLLTIMECLOCKINGDEVICEERRORPAGE_H_29B83947_AD0D_4857_B2DB_9CA77CFE0C50_INCLUDED
#define _PAYROLLTIMECLOCKINGDEVICEERRORPAGE_H_29B83947_AD0D_4857_B2DB_9CA77CFE0C50_INCLUDED
/*
	Created by Tech_dog (VToropov) on 9-Feb-2015 at 10:47:32pm, GMT+3, Taganrog, Monday;
	This is Payroll Time Clocking Device Error Page class declaration file.
*/
#include "UIX_PanelBase.h"
#include "UIX_CommonDrawDefs.h"
#include "UIX_CommonCtrlDefs.h"
#include "UIX_Image.h"
#include "UIX_Button.h"

namespace Payroll { namespace time_clock { namespace UI
{
	using ex_ui::draw::defs::IRenderer;
	using ex_ui::controls::defs::IControlNotify;
	using ex_ui::controls::CImage;
	using ex_ui::controls::CButton;
	using ex_ui::frames::CMessageHandlerDefImpl;
	using ex_ui::frames::CPanelBase;

	class CMainFramePage_0e :
		public  CPanelBase,
		public  CMessageHandlerDefImpl,
		public  IControlNotify
	{
		typedef CPanelBase TPageBase;
	private:
		IRenderer&         m_parent_rnd_ref;
		IControlNotify&    m_parent_snk_ref;
	private:
		CImage             m_err_img;
		CImage             m_err_msg;
		CButton            m_btn_retry;
	public:
		CMainFramePage_0e(IRenderer&, IControlNotify&);
		~CMainFramePage_0e(void);
	public: // CPanelBase
		virtual HRESULT    Create(const HWND hParent, const RECT& rcArea, const bool bVisible) override sealed;
		virtual HRESULT    Destroy(void) override sealed;
		virtual HRESULT    UpdateLayout(LPRECT const = NULL) override sealed;
	private: // IControlNotify
		virtual HRESULT    IControlNotify_OnClick(const UINT ctrlId) override sealed;
	};
}}}

#endif/*_PAYROLLTIMECLOCKINGDEVICEERRORPAGE_H_29B83947_AD0D_4857_B2DB_9CA77CFE0C50_INCLUDED*/