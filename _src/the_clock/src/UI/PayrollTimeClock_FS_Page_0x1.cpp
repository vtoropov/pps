/*
	Created by Tech_dog(VToropov) on 12-Feb-2015 at 1:27:05am, GMT+3, Taganrog, Thursday;
	This is Payroll Time Work Area Select Page class implementation file.
*/
#include "StdAfx.h"
#include "PayrollTimeClock_FS_Page_0x1.h"
#include "PayrollTimeClock_Resource.h"
#include "UIX_GdiObject.h"
#include "PPS_DataListWrapWA.h"
#include "PayrollTimeClock_RecorderBase.h"

using namespace Payroll;
using namespace Payroll::time_clock;
using namespace Payroll::time_clock::common;
using namespace Payroll::time_clock::UI;

using namespace ex_ui;
using namespace ex_ui::controls;

using namespace Platinum::client::data::wrappers;

////////////////////////////////////////////////////////////////////////////

namespace Payroll { namespace time_clock { namespace UI { namespace details
{
	UINT  CMainFramePage_1_EventTimerId(void)
	{
		static const DWORD timer_id = ::GetTickCount() + 3;
		return timer_id;
	}

	using Payroll::time_clock::common::CCommonSettings;

	class CMainFramePage_1_Layout
	{
	public:
		enum { // from PSD files
			wa_list_m  =  15, // list margins
			wa_list_w  = 350,
			wa_list_h  = 400,
			wa_msg_w   = 450,
			wa_msg_h   =  96,
			wa_cfm_w   = 413,
			wa_cfm_h   = 101,
			wa_out_w   = 446,
			wa_out_h   = 101,
		};
	private:
		const
		CCommonSettings& m_settings;
		CWindow&         m_page_ref;
		RECT             m_page_rect;
	public:
		CMainFramePage_1_Layout(CWindow& page_ref, const CCommonSettings& sets_ref) : m_page_ref(page_ref), m_settings(sets_ref)
		{
			::SetRectEmpty(&m_page_rect);
			if (m_page_ref.IsWindow())
				m_page_ref.GetClientRect(&m_page_rect);
		}
	public:
		RECT     GetCfmRect(void)const
		{
			RECT rc_ = {0};
			const INT nLeft = __W(m_page_rect) / 2 - CMainFramePage_1_Layout::wa_cfm_w;
			const INT nTop  = this->GetMsgRect().bottom;

			::SetRect(
					&rc_,
					nLeft,
					nTop,
					nLeft + CMainFramePage_1_Layout::wa_cfm_w,
					nTop  + CMainFramePage_1_Layout::wa_cfm_h
				);
			return rc_;
		}

		RECT     GetMsgRect(void)const
		{
			const INT nLeft = (__W(m_page_rect) - CMainFramePage_1_Layout::wa_msg_w) / 2;
			const INT nTop  = this->GetListRect().bottom;
			RECT rc_ = {0};
			::SetRect(
					&rc_,
					nLeft,
					nTop,
					nLeft + CMainFramePage_1_Layout::wa_msg_w,
					nTop  + CMainFramePage_1_Layout::wa_msg_h
				);
			return rc_;
		}

		RECT     GetListRect(void)const
		{
			RECT rc_ = {0};
			DWORD dwWidth   = m_settings.GetWAlistWidth();
			if ((DWORD)-1  == dwWidth)
			{
				const DWORD dwFontSize = m_settings.GetWAfontInfo().Size();
				const FLOAT dwFontRatio = FLOAT(dwFontSize)/32.0f;

				dwWidth =  static_cast<DWORD>(FLOAT(CMainFramePage_1_Layout::wa_cfm_w) * dwFontRatio);
			}
			if (dwWidth > 0)
			{
				if (dwWidth < CMainFramePage_1_Layout::wa_list_w)
					dwWidth = CMainFramePage_1_Layout::wa_list_w;
				if (dwWidth > static_cast<DWORD>(__W(m_page_rect)))
					dwWidth = __W(m_page_rect);
			}
			else
				dwWidth = CMainFramePage_1_Layout::wa_list_w;

			const INT nLeft = (__W(m_page_rect) - dwWidth) / 2;
			const INT nTop  = (CMainFramePage_1_Layout::wa_list_m);
			const INT nHeight =__H(m_page_rect) / 2 + CMainFramePage_1_Layout::wa_cfm_h;
			::SetRect(
					&rc_,
					nLeft,
					nTop,
					nLeft + dwWidth,
					nTop  + nHeight
				);
			return rc_;
		}

		RECT     GetOutRect(void)const
		{
			RECT rc_ = {0};
			const INT nLeft = __W(m_page_rect) / 2;
			const INT nTop  = this->GetMsgRect().bottom;

			::SetRect(
					&rc_,
					nLeft,
					nTop,
					nLeft + CMainFramePage_1_Layout::wa_out_w,
					nTop  + CMainFramePage_1_Layout::wa_out_h
				);
			return rc_;
		}
	};
}}}}

////////////////////////////////////////////////////////////////////////////

CMainFramePage_1::CMainFramePage_1(IRenderer& rnd_ref, IControlNotify& snk_ref, CSharedObjects& obj_ref):
	TPageBase(IDD_PAYROLL_TC_PAGE_WA, *this),
	m_parent_rnd_ref(rnd_ref),
	m_parent_snk_ref(snk_ref),
	m_shared(obj_ref),
	m_cfm_but(IDC_PAYROLL_FS_WND_PAGE_1_WA_CFM, rnd_ref, snk_ref),
	m_out_but(IDC_PAYROLL_FS_WND_PAGE_1_WA_OUT, rnd_ref, snk_ref),
	m_evt_timer(NULL)
{
	m_lst_msg.SetParentRendererPtr(&rnd_ref);
	HRESULT hr_ = S_OK;
	const HINSTANCE hInstance = ::ATL::_AtlBaseModule.GetModuleInstance();
	{
		hr_ = m_cfm_but.SetImage(eControlState::eNormal   , IDR_PAYROLL_FS_WND_PAGE_1_WA_CFM_N, hInstance); ATLASSERT(S_OK == hr_);
		hr_ = m_cfm_but.SetImage(eControlState::eDisabled , IDR_PAYROLL_FS_WND_PAGE_1_WA_CFM_D, hInstance); ATLASSERT(S_OK == hr_);
		hr_ = m_cfm_but.SetImage(eControlState::ePressed  , IDR_PAYROLL_FS_WND_PAGE_1_WA_CFM_P, hInstance); ATLASSERT(S_OK == hr_);
		hr_ = m_cfm_but.SetImage(eControlState::eHovered  , IDR_PAYROLL_FS_WND_PAGE_1_WA_CFM_H, hInstance); ATLASSERT(S_OK == hr_);
	}
	{
		hr_ = m_out_but.SetImage(eControlState::eNormal   , IDR_PAYROLL_FS_WND_PAGE_1_WA_OUT_N, hInstance); ATLASSERT(S_OK == hr_);
		hr_ = m_out_but.SetImage(eControlState::ePressed  , IDR_PAYROLL_FS_WND_PAGE_1_WA_OUT_P, hInstance); ATLASSERT(S_OK == hr_);
		hr_ = m_out_but.SetImage(eControlState::eHovered  , IDR_PAYROLL_FS_WND_PAGE_1_WA_OUT_H, hInstance); ATLASSERT(S_OK == hr_);
	}
}

CMainFramePage_1::~CMainFramePage_1(void)
{
}

////////////////////////////////////////////////////////////////////////////

HRESULT    CMainFramePage_1::Create(const HWND hParent, const RECT& rcArea, const bool bVisible)
{
	HRESULT hr_ = TPageBase::Create(hParent, rcArea, bVisible);
	if (S_OK != hr_)
		return  hr_;
	CWindow host = TPageBase::GetWindow_Ref();

	details::CMainFramePage_1_Layout layout(host, m_shared.Settings());

	::ATL::CWindow list = TPageBase::GetControl_Safe(IDC_PAYROLL_TC_PAGE_WA_LIST);
	if (list)
	{
		const RECT rc_ = layout.GetListRect();
		const CFontInfo& fnt_info = m_shared.Settings().GetWAfontInfo();

		draw::common::CFont fnt_(fnt_info.Family(), draw::common::eCreateFontOption::eExactSize, fnt_info.Size());
		list.SetFont(fnt_.Detach());
		list.MoveWindow(&rc_);
		{
			CWorkAreaListBoxWrap wrap(list, m_shared.Settings());
			wrap.SeedData();
			::SetFocus(list);
		}
	}
	{
		const RECT rc_ = layout.GetMsgRect();
		hr_ = m_lst_msg.Create(host, rc_);
		hr_ = m_lst_msg.SetImage(IDR_PAYROLL_FS_WND_PAGE_1_WA_MSG_0);
		hr_ = m_lst_msg.UpdateLayout();
	}
	{
		RECT rc_ = layout.GetCfmRect();
		hr_ = m_cfm_but.Create(host, &rc_, false);
		hr_ = m_cfm_but.Enable(false);
	}
	{
		RECT rc_ = layout.GetOutRect();
		hr_ = m_out_but.Create(host, &rc_, false);
	}
	return hr_;
}

HRESULT    CMainFramePage_1::Destroy(void)
{
	if (m_evt_timer)
	{
		TPageBase::GetWindow_Ref().KillTimer(m_evt_timer); m_evt_timer = NULL;
	}
	m_out_but.Destroy();
	m_cfm_but.Destroy();
	m_lst_msg.Destroy();
	HRESULT hr_ = TPageBase::Destroy();
	return  hr_;
}

HRESULT    CMainFramePage_1::Hide(void)
{
	if (m_evt_timer)
	{
		TPageBase::GetWindow_Ref().KillTimer(m_evt_timer); m_evt_timer = NULL;
	}
	HRESULT hr_ = TPageBase::Hide();
	return  hr_;
}

HRESULT    CMainFramePage_1::Show(void)
{
	INT nIndex = LB_ERR;

	::WTL::CListBox list = TPageBase::GetControl_Safe(IDC_PAYROLL_TC_PAGE_WA_LIST);
	if (list)
	{
		CRecorderBase* const pRecorder = m_shared.Recorder();
		if (pRecorder)
			nIndex = list.SelectString(-1, pRecorder->Cached().WorkArea());
		list.SetCurSel(nIndex);
	}
	if (m_cfm_but.GetWindow().IsWindow())
		m_cfm_but.Enable(nIndex != LB_ERR);

	HRESULT hr_ = TPageBase::Show();
	m_evt_timer = TPageBase::GetWindow_Ref().SetTimer(
						details::CMainFramePage_1_EventTimerId(),
						m_shared.Settings().GetSelectWAtimout()
					);
	return  hr_;
}

HRESULT    CMainFramePage_1::UpdateLayout(LPRECT const pRect)
{
	HRESULT hr_ = TPageBase::UpdateLayout(pRect);

	details::CMainFramePage_1_Layout layout(TPageBase::GetWindow_Ref(), m_shared.Settings());

	::ATL::CWindow list = TPageBase::GetControl_Safe(IDC_PAYROLL_TC_PAGE_WA_LIST);
	if (list)
	{
		RECT rc_ = layout.GetListRect();
		list.MoveWindow(&rc_);
	}
	if (m_lst_msg.GetWindow_Ref().IsWindow())
	{
		RECT rc_ = layout.GetMsgRect();
		m_lst_msg.GetWindow_Ref().MoveWindow(&rc_);
	}
	CWindow cfm_wnd = m_cfm_but.GetWindow();
	if (cfm_wnd.IsWindow())
	{
		RECT rc_ = layout.GetCfmRect();
		cfm_wnd.MoveWindow(&rc_);
	}
	CWindow out_wnd = m_out_but.GetWindow();
	if (out_wnd.IsWindow())
	{
		RECT rc_ = layout.GetOutRect();
		out_wnd.MoveWindow(&rc_);
	}
	return  hr_;
}


////////////////////////////////////////////////////////////////////////////

LRESULT    CMainFramePage_1::MessageHandler_OnMessage(UINT uMsg, WPARAM wParam, LPARAM, BOOL& bHandled)
{
	bHandled = FALSE;
	switch (uMsg)
	{
	case WM_COMMAND:
		{
			const WORD wNotifyCode = HIWORD(wParam);
			const WORD wCtrlId     = LOWORD(wParam);wCtrlId;

			if (LBN_SELCHANGE  == wNotifyCode)
			{
				::WTL::CListBox list = TPageBase::GetControl_Safe(IDC_PAYROLL_TC_PAGE_WA_LIST);
				if (list)
				{
					CRecorderBase* const pRecorder = m_shared.Recorder();
					if (pRecorder)
					{
						CAtlString cs_wa;
						list.GetText(list.GetCurSel(), cs_wa);
						pRecorder->Cached().WorkArea(cs_wa.GetString());
					}
				}
				m_cfm_but.Enable(true);
			}
		} break;
	case WM_TIMER:
		{
			if (m_evt_timer)
			{
				TPageBase::GetWindow_Ref().KillTimer(m_evt_timer); m_evt_timer = NULL;
			}
			m_parent_snk_ref.IControlNotify_OnClick(IDC_PAYROLL_FS_WND_PAGE_1_TIMEOUT);
		} break;
	}
	return 0;
}