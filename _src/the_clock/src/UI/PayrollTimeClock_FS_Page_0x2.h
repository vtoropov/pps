#ifndef _PAYROLLTIMECLOCKINGFINISHPAGE_H_43481E68_B2E0_4320_8A18_D3196736E397_INCLUDED
#define _PAYROLLTIMECLOCKINGFINISHPAGE_H_43481E68_B2E0_4320_8A18_D3196736E397_INCLUDED
/*
	Created by Tech_dog(VToropov) on 13-Feb-2015 at 1:04:22pm, GMT+3, Taganrog, Friday;
	This is Payroll Time Clocking Congratulation Page class declaration file.
*/
#include "UIX_PanelBase.h"
#include "UIX_CommonDrawDefs.h"
#include "UIX_CommonCtrlDefs.h"
#include "UIX_Image.h"
#include "UIX_Button.h"
#include "UIX_Label.h"
#include "PayrollTimeClock_SharedObjects.h"

namespace Payroll { namespace time_clock { namespace UI
{
	using ex_ui::draw::defs::IRenderer;
	using ex_ui::controls::defs::IControlNotify;
	using ex_ui::controls::CImage;
	using ex_ui::controls::CButton;
	using ex_ui::controls::CLabel;
	using ex_ui::controls::CControlCrt;
	using ex_ui::frames::IMessageHandler;
	using ex_ui::frames::CPanelBase;

	using Payroll::time_clock::common::CSharedObjects;

	class CMainFramePage_2 :
		public  CPanelBase,
		public  IMessageHandler
	{
		typedef CPanelBase TPageBase;
	private:
		IRenderer&         m_parent_rnd_ref;
		IControlNotify&    m_parent_snk_ref;
		CSharedObjects&    m_shared;
	private:
		CImage             m_res_msg;
		CLabel             m_emp_name;
	private:
		UINT_PTR           m_evt_timer;
	public:
		CMainFramePage_2(IRenderer&, IControlNotify&, CSharedObjects&);
		~CMainFramePage_2(void);
	public: // CPanelBase
		virtual HRESULT    Create(const HWND hParent, const RECT& rcArea, const bool bVisible) override sealed;
		virtual HRESULT    Destroy(void) override sealed;
		virtual HRESULT    Show(void) override sealed;
		virtual HRESULT    UpdateLayout(LPRECT const = NULL) override sealed;
	private: // IMessageHandler
		virtual LRESULT    MessageHandler_OnMessage(UINT, WPARAM, LPARAM, BOOL&) override sealed;
	};
}}}

#endif/*_PAYROLLTIMECLOCKINGFINISHPAGE_H_43481E68_B2E0_4320_8A18_D3196736E397_INCLUDED*/