/*
	Created by Tech_dog (VToropov) on 7-Feb-2015 at 0:12:15am, GMT+3, Taganrog, Saturday;
	This is Platinum Payroll Systems Time Clocking Record Full-Screen Window class implementation file.
*/
#include "StdAfx.h"
#include "PayrollTimeClock_FS_Window.h"
#include "PayrollTimeClock_Resource.h"
#include "PayrollTimeClock_RecorderBase.h"
#include "UIX_GdiProvider.h"
#include "Shared_GenericAppObject.h"

using namespace Payroll;
using namespace Payroll::time_clock;
using namespace Payroll::time_clock::common;
using namespace Payroll::time_clock::UI;

using namespace ex_ui::draw;
using namespace ex_ui::draw::common;

using namespace shared::lite::common;
////////////////////////////////////////////////////////////////////////////

namespace Payroll { namespace time_clock { namespace UI { namespace details
{
	using Payroll::time_clock::common::CCommonSettings;

	class CMainFrame_Layout
	{
		enum _e{
			header_x = 1200,
			header_y =  100,
		};
	private:
		::ATL::CWindow    m_frame_ref;
		RECT              m_client_area;
		const
		CCommonSettings&  m_sets_ref;
	public:
		CMainFrame_Layout(::ATL::CWindow& frm_ref, const CCommonSettings& sets_ref) : m_frame_ref(frm_ref), m_sets_ref(sets_ref)
		{
			m_frame_ref.GetClientRect(&m_client_area);
		}
	public:
		RECT      GetHeaderRect(void) const
		{
			SIZE sz_ = {CMainFrame_Layout::header_x, CMainFrame_Layout::header_y};
			RECT rc_ = {0};

			const INT n_left = m_client_area.left + (__W(m_client_area) - sz_.cx) / 2;
			::SetRect(
					&rc_,
					n_left,
					0,
					sz_.cx + n_left,
					sz_.cy
				);
			return rc_;
		}
		RECT      GetPageRect(void) const
		{
			RECT rc_ = m_client_area;
			if (m_sets_ref.IsBannerVisible())
				rc_.top += CMainFrame_Layout::header_y;
			return rc_;
		}
	public:
		static RECT GetMonitorRect(void)
		{
			RECT rc_ = {0};
			const POINT ptZero = { 0, 0 };
			const HMONITOR hMonitor = ::MonitorFromPoint(ptZero, MONITOR_DEFAULTTOPRIMARY);

			MONITORINFO mInfo = {0};
			mInfo.cbSize = sizeof(MONITORINFO);

			if (::GetMonitorInfo(hMonitor, &mInfo))
			{
				rc_ = mInfo.rcMonitor;
			}
			return rc_;
		}
	};

	HHOOK& CMainFrame_MouseHookRef(void)
	{
		static HHOOK g_hHook = NULL;
		return g_hHook;
	}

	class CMainFrame_MouseHook;

	CMainFrame_MouseHook& CMainFrame_MouseHookObject(void);

	class CMainFrame_MouseHook
	{
	private:
		RECT          m_ltc_rect; // left-top corner rectangle
		volatile HWND m_wnd_obj;  // main frame handle
	public:
		CMainFrame_MouseHook(void)
		{
			::SetRectEmpty(&m_ltc_rect);
			this->Update();
		}
	public:
		VOID   Install(const HWND hOwner)
		{
			if (CMainFrame_MouseHookRef())
				return;
			m_wnd_obj = hOwner;
			CMainFrame_MouseHookRef() = ::SetWindowsHookEx(
							WH_MOUSE,
							CMainFrame_MouseHook::HookProc,
							ATL::_AtlBaseModule.GetModuleInstance(),
							::GetCurrentThreadId()
						);
		}

		VOID   Uninstall(void)
		{
			m_wnd_obj = NULL;
			if (!CMainFrame_MouseHookRef())
				return;
			::UnhookWindowsHookEx(CMainFrame_MouseHookRef());
			CMainFrame_MouseHookRef() = NULL;
		}

		VOID   Update(void)
		{
			m_ltc_rect = CMainFrame_Layout::GetMonitorRect();
			m_ltc_rect.right   = m_ltc_rect.left + 50;
			m_ltc_rect.bottom  = m_ltc_rect.top  + 50;
		}
	public:
		static LRESULT CALLBACK HookProc(INT nCode, WPARAM wParam, LPARAM lParam)
		{
			BOOL bHandled = FALSE;
			if (HC_ACTION == nCode)
			{
				const UINT uMsg = (UINT)wParam;
				if (WM_LBUTTONDBLCLK == uMsg)
				{
					PMOUSEHOOKSTRUCT p = (PMOUSEHOOKSTRUCT)lParam;
					if (p)
					{
						RECT rc_ = CMainFrame_MouseHookObject().m_ltc_rect;
						const bool bDoAction = !!::PtInRect(&rc_, p->pt);
						if (bDoAction)
						{
							HWND hOwner = CMainFrame_MouseHookObject().m_wnd_obj;
							::SendMessage(hOwner, WM_SYSCOMMAND, (WPARAM)SC_CLOSE, 0);
						}
					}
				}
			}
			if (!bHandled)
				return ::CallNextHookEx(
						CMainFrame_MouseHookRef(),
						nCode,
						wParam,
						lParam
					);
			else
				return TRUE;
		}
	};

	CMainFrame_MouseHook& CMainFrame_MouseHookObject(void)
	{
		static CMainFrame_MouseHook hook_obj;
		return hook_obj;
	}

}}}}

////////////////////////////////////////////////////////////////////////////

CMainFrame::CMainFrameWnd::CMainFrameWnd(IControlNotify& notify_ref, CSharedObjects& obj_ref) : 
	m_ctrl_notify(notify_ref),
	m_bkgnd_renderer(IDR_PAYROLL_FS_WND_BKGND_TILE, *this),
	m_shared(obj_ref),
	m_page_e(m_bkgnd_renderer, m_ctrl_notify),
	m_page_0(m_bkgnd_renderer, m_ctrl_notify, obj_ref),
	m_page_1(m_bkgnd_renderer, m_ctrl_notify, obj_ref),
	m_page_2(m_bkgnd_renderer, m_ctrl_notify, obj_ref),
	m_page_3(m_bkgnd_renderer, m_ctrl_notify, obj_ref),
	m_page_s(m_bkgnd_renderer, m_ctrl_notify, obj_ref),
	m_wnd_locker(obj_ref, *this, *this)
{
	const bool bHasBkgndExternalFile = m_shared.Settings().HasBkgndFilePath();
	if (bHasBkgndExternalFile)
		m_bkgnd_renderer.InitializeFromFile(m_shared.Settings().GetBkgndFilePath());
}

CMainFrame::CMainFrameWnd::~CMainFrameWnd(void)
{
}

////////////////////////////////////////////////////////////////////////////

LRESULT CMainFrame::CMainFrameWnd::OnCreate  (UINT, WPARAM, LPARAM, BOOL&)
{
	::ATL::CAtlString cs_title;
	cs_title.LoadString(IDS_PAYROLL_FS_WND_TITLE_TEXT);

	TWindow::SetWindowText(cs_title);

	details::CMainFrame_Layout layout(*this, m_shared.Settings());
	{
		CApplicationIconLoader icons(IDR_PAYROLL_TC_DLG_ICON);
		TWindow::SetIcon(icons.DetachBigIcon());
		TWindow::SetIcon(icons.DetachSmallIcon(), FALSE);
	}

	HRESULT hr_ = S_OK;
	if (m_shared.Settings().IsBannerVisible())
	{
		const RECT rc_ = layout.GetHeaderRect();
		hr_ = m_header.SetParentRendererPtr(&m_bkgnd_renderer);
		hr_ = m_header.Create(*this, rc_, NULL, IDR_PAYROLL_FS_WND_HEADER_IMG);
		hr_ = m_header.SetImage(IDR_PAYROLL_FS_WND_HEADER_IMG);
		hr_ = m_header.UpdateLayout();
	}
	const bool bError = !m_shared.IsInitialized();
	{
		RECT rc_ = layout.GetPageRect();
		hr_ = m_page_e.Create(*this, rc_, bError);
		if (S_OK == hr_)
		{
			hr_ = m_page_e.SetParentRenderer(&m_bkgnd_renderer);
			hr_ = m_page_e.UpdateLayout();
		}
	}
	{
		RECT rc_ = layout.GetPageRect();
		hr_ = m_page_0.Create(*this, rc_, false);
		if (S_OK == hr_)
		{
			hr_ = m_page_0.SetParentRenderer(&m_bkgnd_renderer);
		}
	}
	{
		RECT rc_ = layout.GetPageRect();
		hr_ = m_page_1.Create(*this, rc_, false);
		if (S_OK == hr_)
		{
			hr_ = m_page_1.SetParentRenderer(&m_bkgnd_renderer);
		}
	}
	{
		RECT rc_ = layout.GetPageRect();
		hr_ = m_page_2.Create(*this, rc_, false);
		if (S_OK == hr_)
		{
			hr_ = m_page_2.SetParentRenderer(&m_bkgnd_renderer);
		}
	}
	{
		RECT rc_ = layout.GetPageRect();
		hr_ = m_page_3.Create(*this, rc_, false);
		if (S_OK == hr_)
		{
			hr_ = m_page_3.SetParentRenderer(&m_bkgnd_renderer);
		}
	}
	{
		RECT rc_ = layout.GetPageRect();
		hr_ = m_page_s.Create(*this, rc_, false);
		if (S_OK == hr_)
		{
			hr_ = m_page_s.SetParentRenderer(&m_bkgnd_renderer);
		}
	}
	if (!bError)
		hr_ = m_page_0.Show();
	details::CMainFrame_MouseHookObject().Install(*this);
	m_wnd_locker.Start();
	return 0;
}

LRESULT CMainFrame::CMainFrameWnd::OnDestroy (UINT, WPARAM, LPARAM, BOOL&)
{
	m_wnd_locker.Stop(false);
	details::CMainFrame_MouseHookObject().Uninstall();
	m_page_s.Destroy();
	m_page_e.Destroy();
	m_page_3.Destroy();
	m_page_2.Destroy();
	m_page_1.Destroy();
	m_page_0.Destroy();
	m_header.Destroy();
	return 0;
}

LRESULT CMainFrame::CMainFrameWnd::OnErase   (UINT uMsg, WPARAM wParam, LPARAM lParam, BOOL& bHandled)
{
	uMsg; wParam; lParam; bHandled = TRUE;
	RECT rc_ = {0};
	TWindow::GetClientRect(&rc_);
	{
		CZBuffer dc_(reinterpret_cast<HDC>(wParam), rc_);
		dc_.FillSolidRect(&rc_, RGB(0xee, 0xee, 0xee));
		m_bkgnd_renderer.Draw(dc_, rc_);
	}
	return 0;
}

LRESULT CMainFrame::CMainFrameWnd::OnKeyDown (UINT, WPARAM wParam, LPARAM, BOOL& bHandled)
{
	wParam; bHandled = TRUE;
	if (VK_ESCAPE == wParam)
	{
		return this->OnSysCmd(0, (WPARAM)SC_CLOSE, 0, bHandled);
	}
#if defined(_DEBUG) ||  defined(_DEMO_PRJ_932CE492_8857_4d7c_BAD1_25BECD3816AD)
	else if (VK_UP == wParam || VK_DOWN == wParam)
	{
		const bool bUseLandscape = m_shared.Settings().IsLandscape();
		if (!bUseLandscape)
		{
			const POINT ptZero = { 0, 0 };
			const HMONITOR hMonitor = ::MonitorFromPoint(ptZero, MONITOR_DEFAULTTOPRIMARY);
			MONITORINFO mInfo = {0};
			mInfo.cbSize = sizeof(MONITORINFO);
			if (::GetMonitorInfo(hMonitor, &mInfo))
			{
				RECT rc_ = {0};
				if (TWindow::GetWindowRect(&rc_))
				{
					if (VK_DOWN == wParam)
					{
						const INT nTop = mInfo.rcMonitor.top;
						::SetRect(&rc_, rc_.left, nTop, rc_.left + __W(rc_), nTop + __H(rc_));
					}
					else
					{
						const INT nTop = mInfo.rcMonitor.bottom  - __H(rc_);
						::SetRect(&rc_, rc_.left, nTop, rc_.left + __W(rc_), nTop + __H(rc_));
					}
					TWindow::SetWindowPos(NULL, &rc_, SWP_NOZORDER|SWP_NOSIZE);
				}
			}
		}
	}
#endif
	else
		bHandled = FALSE;
	return 0;
}

LRESULT CMainFrame::CMainFrameWnd::OnSysCmd  (UINT, WPARAM wParam, LPARAM, BOOL& bHandled)
{
	bHandled = FALSE;
	switch (wParam)
	{
	case SC_CLOSE:
		{
			bHandled = TRUE;
			const bool bHasPassword = m_shared.Settings().HasPassword();
			if (!bHasPassword)
			{
				::PostQuitMessage(0);
				return 0;
			}

			if (m_page_3.GetWindow_Ref().IsWindowVisible())
			{
				HRESULT hr_ = m_page_3.ComparePassword();
				if (S_OK == hr_)
					::PostQuitMessage(0);
				else
				{
				}
			}
			else
			{
				m_ctrl_notify.IControlNotify_OnClick(IDC_PAYROLL_FS_WND_PAGE_3_SHOW);
			}
		} break;
	}
	return 0;
}

LRESULT CMainFrame::CMainFrameWnd::OnSize    (UINT, WPARAM, LPARAM lParam, BOOL&)
{
	static RECT rc_ = {0};
	RECT rc_new = {0, 0, LOWORD(lParam), HIWORD(lParam)};
	
	if (::IsRectEmpty(&rc_new))
		return 0;

	if (::EqualRect(&rc_, &rc_new))
		return 0;
	
	rc_ = rc_new;
	details::CMainFrame_MouseHookObject().Update();

	details::CMainFrame_Layout layout(*this, m_shared.Settings());
	rc_ = layout.GetPageRect();

	if (m_page_0.IsValid())m_page_0.UpdateLayout(&rc_);
	if (m_page_1.IsValid())m_page_1.UpdateLayout(&rc_);
	if (m_page_2.IsValid())m_page_2.UpdateLayout(&rc_);
	if (m_page_3.IsValid())m_page_3.UpdateLayout(&rc_);
	if (m_page_e.IsValid())m_page_e.UpdateLayout(&rc_);
	if (m_page_s.IsValid())m_page_s.UpdateLayout(&rc_);

	return 0;
}

////////////////////////////////////////////////////////////////////////////

HRESULT CMainFrame::CMainFrameWnd::GenericEvent_OnNotify(const UINT eventId)
{
	eventId;
	return S_OK;
}

////////////////////////////////////////////////////////////////////////////

CMainFrame::CMainFrame(CSharedObjects& obj_ref) : m_frame(*this, obj_ref), m_shared(obj_ref)
{
}

CMainFrame::~CMainFrame(void)
{
	if (m_frame.IsWindow()) m_frame.SendMessage(WM_CLOSE);
}

////////////////////////////////////////////////////////////////////////////

HRESULT  CMainFrame::IControlNotify_OnClick(const UINT ctrlId)
{
	switch (ctrlId)
	{
	case IDC_PAYROLL_FS_WND_PAGE_E_RTY:
		{
			if (!m_shared.IsInitialized())
			{
				HRESULT hr_ = m_shared.Initialize();
				if (S_OK == hr_)
					this->IControlNotify_OnClick(IDC_PAYROLL_FS_WND_PAGE_E_SV_AGAIN);
			}
		} break;
	case IDC_PAYROLL_FS_WND_EXIT:
		{
			BOOL bHandled = TRUE;
			m_frame.OnSysCmd(0, (WPARAM)SC_CLOSE, 0, bHandled);
		} break;
	case IDC_PAYROLL_FS_WND_PAGE_3_SHOW:
		{
			m_frame.m_page_e.Hide();
			m_frame.m_page_0.Hide();
			m_frame.m_page_1.Hide();
			m_frame.m_page_2.Hide();
			m_frame.m_page_s.Hide();
			m_frame.m_page_3.Show();
		} break;
	case IDC_PAYROLL_TC_PAGE_SCAN_OK:
		{
			m_frame.m_page_e.Hide();
			m_frame.m_page_0.Hide();
			m_frame.m_page_2.Hide();
			m_frame.m_page_3.Hide();
			m_frame.m_page_s.Hide();
			const bool bWAPrompt = m_shared.Settings().IsWorkAreaPromptMode();
			if (bWAPrompt)
				m_frame.m_page_1.Show();
			else
				this->IControlNotify_OnClick(IDC_PAYROLL_FS_WND_PAGE_1_WA_CFM);
		} break;
	case IDC_PAYROLL_TC_PAGE_SCAN_ERR:
		{
			m_frame.m_page_0.Hide();
			m_frame.m_page_1.Hide();
			m_frame.m_page_2.Hide();
			m_frame.m_page_3.Hide();
			m_frame.m_page_s.Hide();
			m_frame.m_page_e.Show();
		} break;
	case IDC_PAYROLL_FS_WND_PAGE_1_WA_CFM:
		{
			bool bSaved = false;
			CRecorderBase* const pRecorder = m_shared.Recorder();
			if (pRecorder)
			{
				HRESULT hr_ = pRecorder->Save();
				bSaved = (S_OK == hr_);
			}
			m_frame.m_page_e.Hide();
			m_frame.m_page_0.Hide();
			m_frame.m_page_1.Hide();
			m_frame.m_page_3.Hide();
			if (bSaved)
			{
				m_frame.m_page_s.Hide();
				m_frame.m_page_2.Show();
			}
			else
			{
				m_frame.m_page_2.Hide();
				m_frame.m_page_s.Show();
			}
		} break;
	case IDC_PAYROLL_FS_WND_PAGE_1_WA_OUT:
		{
			bool bSaved = false;
			CRecorderBase* const pRecorder = m_shared.Recorder();
			if (pRecorder)
			{
				CAtlString cs_wa = m_shared.Settings().GetLogoutWA();
				pRecorder->Cached().WorkArea(cs_wa.GetString());

				HRESULT hr_ = pRecorder->Save();
				bSaved = (S_OK == hr_);
			}
			m_frame.m_page_e.Hide();
			m_frame.m_page_0.Hide();
			m_frame.m_page_1.Hide();
			m_frame.m_page_3.Hide();
			if (bSaved)
			{
				m_frame.m_page_s.Hide();
				m_frame.m_page_2.Show();
			}
			else
			{
				m_frame.m_page_2.Hide();
				m_frame.m_page_s.Show();
			}
		} break;
	case IDC_PAYROLL_FS_WND_PAGE_2_FINISHED:
	case IDC_PAYROLL_FS_WND_PAGE_E_SV_AGAIN:
	case IDC_PAYROLL_FS_WND_PAGE_3_RET:
	case IDC_PAYROLL_FS_WND_PAGE_1_TIMEOUT:
		{
			m_frame.m_page_e.Hide();
			m_frame.m_page_1.Hide();
			m_frame.m_page_2.Hide();
			m_frame.m_page_3.Hide();
			m_frame.m_page_s.Hide();
			m_frame.m_page_0.Show();
		} break;
	default:;
	}
	return S_OK;
}

////////////////////////////////////////////////////////////////////////////

HRESULT  CMainFrame::Create(void)
{
	if (m_frame.IsWindow())
		return HRESULT_FROM_WIN32(ERROR_OBJECT_ALREADY_EXISTS);

	const POINT ptZero = { 0, 0 };
	const HMONITOR hMonitor = ::MonitorFromPoint(ptZero, MONITOR_DEFAULTTOPRIMARY);

	MONITORINFO mInfo = {0};
	mInfo.cbSize = sizeof(MONITORINFO);

	if (!::GetMonitorInfo(hMonitor, &mInfo))
		return HRESULT_FROM_WIN32(::GetLastError());

	RECT rc_ = {0};
#if defined(_DEBUG)
	const bool bUseLandscape = m_shared.Settings().IsLandscape();
	if (!bUseLandscape)
	{
		SIZE sz_ = {800, 1280};
		const INT nLeft = mInfo.rcMonitor.left + (__W(mInfo.rcMonitor) - sz_.cx) / 2;
		const INT nTop  = mInfo.rcMonitor.top  + (__H(mInfo.rcMonitor) - sz_.cy) / 2;
			::SetRect(
					&rc_,
					nLeft,
					nTop,
					nLeft + sz_.cx,
					nTop  + sz_.cy
				);
	}
	else
		rc_ = mInfo.rcMonitor;
#else
	rc_ = mInfo.rcMonitor;
#endif
	DWORD dwStyleEx = 0;
	const eFvCustomizeType::_e eCustomizationType = m_shared.Settings().FvCustomization();

	if (eFvCustomizeType::eHide == eCustomizationType)
		dwStyleEx |= WS_EX_TOPMOST;

	m_frame.Create(::GetDesktopWindow(), rc_, NULL, WS_POPUP|WS_VISIBLE|WS_CLIPCHILDREN, dwStyleEx);
	if (!m_frame.IsWindow())
		return HRESULT_FROM_WIN32(::GetLastError());
	return  S_OK;
}