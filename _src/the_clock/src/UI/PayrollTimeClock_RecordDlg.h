#ifndef __PAYROLLTIMECLOCKINGRECORDDIALOG_H_197CB134_31FF_4a4f_92F4_55934883060D_INCLUDED
#define __PAYROLLTIMECLOCKINGRECORDDIALOG_H_197CB134_31FF_4a4f_92F4_55934883060D_INCLUDED
/*
	Created by Tech_dog (VToropov) on 9-Apr-2014 at 8:55:09pm, GMT+4, Saint-Petersburg, Wednesday;
	This is Platinum Payroll Systems Time Clocking Record Dialog class declaration file.
*/
#include "UIX_ImgHeader.h"
#include "PPS_EmployeeDataRecord.h"
#include "PayrollTimeClock_SharedObjects.h"

namespace Payroll { namespace time_clock { namespace UI
{
	using ex_ui::frames::CImageHeader;
	using Platinum::client::data::CEmployeeDataRecord;
	using Payroll::time_clock::common::CSharedObjects;

	class eRecordDlgResult
	{
	public:
		enum _enum {
			eConfirm  = 0x0,
			eCancel   = 0x1,
			eClose    = 0x2
		};
	};

	class CRecordDlg
	{
	private:
		class CRecordDlgImpl :
			public  ::ATL::CDialogImpl<CRecordDlgImpl>
		{
			friend class CRecordDlg;
			typedef ::ATL::CDialogImpl<CRecordDlgImpl> TBaseDlg;
		private:
			eRecordDlgResult::_enum  m_result;
			CImageHeader             m_header;
			CEmployeeDataRecord&     m_record;
			const CSharedObjects&    m_shared;
		public:
			UINT IDD;
		public:
			BEGIN_MSG_MAP(CRecordDlgImpl)
				MESSAGE_HANDLER     (WM_DESTROY     , OnDestroy   )
				MESSAGE_HANDLER     (WM_INITDIALOG  , OnInitDialog)
				MESSAGE_HANDLER     (WM_SYSCOMMAND  , OnSysCommand)
				COMMAND_ID_HANDLER  (IDOK           , OnConfirm   )
				COMMAND_ID_HANDLER  (IDCANCEL       , OnDismiss   )
				COMMAND_CODE_HANDLER(CBN_SELCHANGE  , OnWA_Changed)
			END_MSG_MAP()
		public:
			CRecordDlgImpl(const CSharedObjects&, CEmployeeDataRecord&);
			~CRecordDlgImpl(void);
		private:
			LRESULT OnConfirm   (WORD , WORD   , HWND   , BOOL&);
			LRESULT OnDestroy   (UINT , WPARAM , LPARAM , BOOL&);
			LRESULT OnDismiss   (WORD , WORD   , HWND   , BOOL&);
			LRESULT OnInitDialog(UINT , WPARAM , LPARAM , BOOL&);
			LRESULT OnSysCommand(UINT , WPARAM , LPARAM , BOOL&);
			LRESULT OnWA_Changed(WORD , WORD   , HWND   , BOOL&);
		};
	private:
		CRecordDlg::CRecordDlgImpl m_dlg;
	public:
		CRecordDlg(const CSharedObjects&, CEmployeeDataRecord&);
		~CRecordDlg(void);
	public:
		eRecordDlgResult::_enum DoModal(void);
	};
}}}

#endif/*__PAYROLLTIMECLOCKINGRECORDDIALOG_H_197CB134_31FF_4a4f_92F4_55934883060D_INCLUDED*/