/*
	Created by Tech_dog(VToropov) on 16-Feb-2015 at 4:00:22am, GMT+3, Taganrog, Monday;
	This is Payroll Time Clocking Data Saving Error Page class implementation file.
*/
#include "StdAfx.h"
#include "PayrollTimeClock_FS_Page_0xse.h"
#include "PayrollTimeClock_Resource.h"

using namespace Payroll;
using namespace Payroll::time_clock;
using namespace Payroll::time_clock::UI;

using namespace ex_ui;
using namespace ex_ui::controls;

////////////////////////////////////////////////////////////////////////////

namespace Payroll { namespace time_clock { namespace UI { namespace details
{
	class CMainFramePage_1e_Layout
	{
	public:
		enum { // from PSD files
			res_msg_w   = 390,
			res_msg_h   = 370,
			ret_btn_w   = 446,
			ret_btn_h   = 101
		};
	private:
		CWindow&  m_page_ref;
		RECT      m_page_rect;
	public:
		CMainFramePage_1e_Layout(CWindow& page_ref) : m_page_ref(page_ref)
		{
			::SetRectEmpty(&m_page_rect);
			if (m_page_ref.IsWindow())
				m_page_ref.GetClientRect(&m_page_rect);
		}
	public:
		RECT     GetMsgRect(void)const
		{
			const INT nLeft = (__W(m_page_rect) - CMainFramePage_1e_Layout::res_msg_w * 1) / 2;
			const INT nTop  = (__H(m_page_rect) - CMainFramePage_1e_Layout::res_msg_h * 2) / 2;
			RECT rc_ = {0};
			::SetRect(
					&rc_,
					nLeft,
					nTop,
					nLeft + CMainFramePage_1e_Layout::res_msg_w,
					nTop  + CMainFramePage_1e_Layout::res_msg_h
				);
			return rc_;
		}

		RECT     GetRetryRect(void)const
		{
			const RECT rc_msg = this->GetMsgRect();

			const INT nLeft = (__W(m_page_rect) - CMainFramePage_1e_Layout::ret_btn_w) / 2;
			const INT nTop  = rc_msg.bottom;

			RECT rc_ = {0};
			::SetRect(
					&rc_,
					nLeft,
					nTop,
					nLeft + CMainFramePage_1e_Layout::ret_btn_w,
					nTop  + CMainFramePage_1e_Layout::ret_btn_h
				);
			return rc_;
		}
	};
}}}}

////////////////////////////////////////////////////////////////////////////

CMainFramePage_1e::CMainFramePage_1e(IRenderer& rnd_ref, IControlNotify& snk_ref, CSharedObjects& obj_ref):
	TPageBase(IDD_PAYROLL_TC_PAGE_SAVE_ERR, *this),
	m_parent_rnd_ref(rnd_ref),
	m_parent_snk_ref(snk_ref),
	m_shared(obj_ref),
	m_btn_ret(IDC_PAYROLL_FS_WND_PAGE_E_SV_AGAIN, rnd_ref, snk_ref)
{
	m_res_msg.SetParentRendererPtr(&rnd_ref);
	HRESULT hr_ = S_OK;
	const HINSTANCE hInstance = ::ATL::_AtlBaseModule.GetModuleInstance();
	hr_ = m_btn_ret.SetImage(eControlState::eNormal   , IDR_PAYROLL_FS_WND_PAGE_3_RET_N, hInstance); ATLASSERT(S_OK == hr_);
	hr_ = m_btn_ret.SetImage(eControlState::eDisabled , IDR_PAYROLL_FS_WND_PAGE_3_RET_N, hInstance); ATLASSERT(S_OK == hr_);
	hr_ = m_btn_ret.SetImage(eControlState::ePressed  , IDR_PAYROLL_FS_WND_PAGE_3_RET_P, hInstance); ATLASSERT(S_OK == hr_);
	hr_ = m_btn_ret.SetImage(eControlState::eHovered  , IDR_PAYROLL_FS_WND_PAGE_3_RET_H, hInstance); ATLASSERT(S_OK == hr_);
}

CMainFramePage_1e::~CMainFramePage_1e(void)
{
}

////////////////////////////////////////////////////////////////////////////

HRESULT    CMainFramePage_1e::Create(const HWND hParent, const RECT& rcArea, const bool bVisible)
{
	HRESULT hr_ = TPageBase::Create(hParent, rcArea, bVisible);
	if (S_OK != hr_)
		return  hr_;
	CWindow host = TPageBase::GetWindow_Ref();

	details::CMainFramePage_1e_Layout layout(host);

	{
		const RECT rc_ = layout.GetMsgRect();
		hr_ = m_res_msg.Create(host, rc_);
		hr_ = m_res_msg.SetImage(IDR_PAYROLL_FS_WND_PAGE_E_SV_MSG_0);
		hr_ = m_res_msg.UpdateLayout();
	}
	{
		RECT rc_ = layout.GetRetryRect();
		hr_ = m_btn_ret.Create(host, &rc_, false);
	}
	return hr_;
}

HRESULT    CMainFramePage_1e::Destroy(void)
{
	m_btn_ret.Destroy();
	m_res_msg.Destroy();
	HRESULT hr_ = TPageBase::Destroy();
	return  hr_;
}

HRESULT    CMainFramePage_1e::UpdateLayout(LPRECT const pRect)
{
	HRESULT hr_ = TPageBase::UpdateLayout(pRect);

	details::CMainFramePage_1e_Layout layout(TPageBase::GetWindow_Ref());

	if (m_res_msg.GetWindow_Ref().IsWindow())
	{
		RECT rc_ = layout.GetMsgRect();
		m_res_msg.GetWindow_Ref().MoveWindow(&rc_);
	}
	CWindow btn_wnd = m_btn_ret.GetWindow();
	if (btn_wnd.IsWindow())
	{
		RECT rc_ = layout.GetRetryRect();
		btn_wnd.MoveWindow(&rc_);
	}

	return  hr_;
}

////////////////////////////////////////////////////////////////////////////

LRESULT    CMainFramePage_1e::MessageHandler_OnMessage(UINT uMsg, WPARAM wParam, LPARAM, BOOL& bHandled)
{
	bHandled = FALSE;
	switch (uMsg)
	{
	case WM_COMMAND:
		{
			const WORD wNotifyCode = HIWORD(wParam);wNotifyCode;
			const WORD wCtrlId     = LOWORD(wParam);wCtrlId;
		} break;
	}
	return 0;
}