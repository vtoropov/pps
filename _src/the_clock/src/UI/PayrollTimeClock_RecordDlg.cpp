/*
	Created by Tech_dog (VToropov) on 9-Apr-2014 at 9:04:27pm, GMT+4, Saint-Petersburg, Wednesday;
	This is Platinum Payroll Systems Time Clocking Record Dialog class implementation file.
*/
#include "StdAfx.h"
#include "PayrollTimeClock_RecordDlg.h"
#include "PayrollTimeClock_Resource.h"
#include "Shared_GenericAppObject.h"
#include "UIX_GdiProvider.h"
#include "PPS_DataListWrapWA.h"

using namespace Payroll;
using namespace Payroll::time_clock;
using namespace Payroll::time_clock::UI;
using namespace Platinum::client::data::wrappers;
using namespace shared::lite::common;

using namespace ex_ui::draw;
using namespace ex_ui::draw::common;

////////////////////////////////////////////////////////////////////////////

CRecordDlg::CRecordDlgImpl::CRecordDlgImpl(const CSharedObjects& objs_ref, CEmployeeDataRecord& rec_ref): 
	IDD(IDD_PAYROLL_TC_DLG_MAIN),
	m_header(IDR_PAYROLL_TC_DLG_HEADER),
	m_record(rec_ref),
	m_shared(objs_ref),
	m_result(eRecordDlgResult::eCancel)
{
}

CRecordDlg::CRecordDlgImpl::~CRecordDlgImpl(void)
{
}

////////////////////////////////////////////////////////////////////////////

LRESULT CRecordDlg::CRecordDlgImpl::OnConfirm   (WORD wNotifyCode, WORD wID, HWND hWndCtl, BOOL& bHandled)
{
	wNotifyCode; wID; hWndCtl; bHandled;
	::WTL::CComboBox   wa_ = TBaseDlg::GetDlgItem(IDC_PAYROLL_TC_DLG_AREA);
	const INT nIndex = wa_.GetCurSel();
	if (CB_ERR == nIndex)
	{
		AtlMessageBox(
				TBaseDlg::m_hWnd,
				IDS_PAYROLL_TC_NOT_WA_CHOSEN,
				global::GetAppObjectRef().GetName(),
				MB_ICONEXCLAMATION|MB_OK|MB_SETFOREGROUND|MB_TOPMOST
			);
	}
	else
	{
		m_result = eRecordDlgResult::eConfirm;
		TBaseDlg::EndDialog(IDOK);
	}
	return 0;
}

LRESULT CRecordDlg::CRecordDlgImpl::OnDestroy   (UINT uMsg, WPARAM wParam, LPARAM lParam,  BOOL& bHandled)
{
	uMsg; wParam; lParam; bHandled = FALSE;
	m_header.Destroy();
	return 0;
}

LRESULT CRecordDlg::CRecordDlgImpl::OnDismiss   (WORD wNotifyCode, WORD wID, HWND hWndCtl, BOOL& bHandled)
{
	wNotifyCode; wID; hWndCtl; bHandled;
	m_result = eRecordDlgResult::eCancel;
	TBaseDlg::EndDialog(IDCANCEL);
	return 0;
}

LRESULT CRecordDlg::CRecordDlgImpl::OnInitDialog(UINT uMsg, WPARAM wParam, LPARAM lParam,  BOOL& bHandled)
{
	uMsg; wParam; lParam; bHandled;
	{
		WINDOWPLACEMENT wnd_place = {0};
		wnd_place.length = sizeof(WINDOWPLACEMENT);
		TBaseDlg::GetWindowPlacement(&wnd_place);
		if (true || SW_SHOWMINIMIZED == wnd_place.showCmd)
		{
			wnd_place.showCmd = SW_RESTORE;
			TBaseDlg::SetWindowPlacement(&wnd_place);
			::SetForegroundWindow(TBaseDlg::m_hWnd);
		}
		TBaseDlg::SetWindowPos(HWND_TOPMOST, 0, 0, 0, 0, SWP_NOMOVE|SWP_NOSIZE);
		TBaseDlg::CenterWindow();
	}
	{
		::shared::lite::common::CApplicationIconLoader loader(IDR_PAYROLL_TC_DLG_ICON);
		TBaseDlg::SetIcon(loader.DetachBigIcon(), TRUE);
		TBaseDlg::SetIcon(loader.DetachSmallIcon(), FALSE);
	}
	HRESULT hr_ = S_OK;
	{
		hr_ = m_header.Create(TBaseDlg::m_hWnd);
	}
	{
		::ATL::CWindow wnd_code = TBaseDlg::GetDlgItem(IDC_PAYROLL_TC_DLG_CODE);
		wnd_code.SetWindowText(m_record.Code());
	}
	{
		::ATL::CWindow wnd_name = TBaseDlg::GetDlgItem(IDC_PAYROLL_TC_DLG_NAME);
		wnd_name.SetWindowText(m_record.Name());
	}
	CAtlString csAlwaysCostWA = m_shared.Settings().GetAlwaysCostWA();
	{
		CWorkAreaDataListWrap list(m_shared.Settings());
		list.AttachTo(*this, IDC_PAYROLL_TC_DLG_AREA);
		list.SetHeight(14, true);
		if (csAlwaysCostWA.IsEmpty())
		{
			list.SeedData();
			list.SelectItem(m_record.WorkArea());
			::SetFocus(list.GetCtrlHandle());
		}
		else
		{
			list.AppendItem(csAlwaysCostWA);
			list.SelectItem(csAlwaysCostWA);
			::EnableWindow(list.GetCtrlHandle(), FALSE);
			m_record.WorkArea(csAlwaysCostWA);
		}
	}
	{
		HBITMAP hBitmap = NULL;
		hr_ = CGdiPlusPngLoader::LoadResource(IDR_PAYROLL_TC_DLG_IMAGE, NULL, hBitmap);
		if (S_OK == hr_)
		{
			::WTL::CStatic control = TBaseDlg::GetDlgItem(IDC_PAYROLL_TC_DLG_IMAGE);
			if (control.IsWindow())
			{
				control.SetBitmap(hBitmap); ::DeleteObject(hBitmap); hBitmap = NULL;
			}
		}
	}
	CApplication& the_app = global::GetAppObjectRef();
	{
		::ATL::CAtlString cs_title;
		cs_title.Format(
				_T("%s %s"),
				the_app.Version().ProductName(),
				the_app.Version().ProductVersion()
			);
		TBaseDlg::SetWindowTextW(cs_title);
	}
	return 0;
}

LRESULT CRecordDlg::CRecordDlgImpl::OnSysCommand(UINT uMsg, WPARAM wParam, LPARAM lParam,  BOOL& bHandled)
{
	uMsg; wParam; lParam; bHandled = FALSE;
	switch (wParam)
	{
	case SC_CLOSE:
		{
			if (IDYES == AtlMessageBox(
							TBaseDlg::m_hWnd, 
							IDS_PAYROLL_TC_END_PROCESS,
							global::GetAppObjectRef().GetName(),
							MB_ICONQUESTION|MB_YESNO|MB_SETFOREGROUND|MB_TOPMOST
						))
			{
				m_result = eRecordDlgResult::eClose;
				TBaseDlg::EndDialog(IDCANCEL);
			}
			else
				bHandled = TRUE;
		} break;
	}
	return 0;
}

LRESULT CRecordDlg::CRecordDlgImpl::OnWA_Changed(WORD wNotifyCode, WORD wID, HWND hWndCtl, BOOL& bHandled)
{
	wNotifyCode; wID; hWndCtl; bHandled;
	switch (wID)
	{
	case IDC_PAYROLL_TC_DLG_AREA:
		{
			if (CBN_SELCHANGE == wNotifyCode)
			{
				::WTL::CComboBox combo = TBaseDlg::GetDlgItem(wID);
				const INT nIndex = combo.GetCurSel();
				if (nIndex != CB_ERR)
				{
					::ATL::CAtlString cs_wa;
					combo.GetLBText(nIndex, cs_wa);
					if (!cs_wa.IsEmpty())
						m_record.WorkArea(cs_wa.GetString());
				}
			}
		} break;
	default:
		bHandled = FALSE;
	}
	return 0;
}

////////////////////////////////////////////////////////////////////////////

CRecordDlg::CRecordDlg(const CSharedObjects& objs_ref, CEmployeeDataRecord& rec_ref) : m_dlg(objs_ref, rec_ref)
{
}

CRecordDlg::~CRecordDlg(void)
{
}

////////////////////////////////////////////////////////////////////////////

eRecordDlgResult::_enum CRecordDlg::DoModal(void)
{
	m_dlg.DoModal();
	return m_dlg.m_result;
}