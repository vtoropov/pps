/*
	Created by Tech_dog (VToropov) on 16-Feb-2015 at 6:31:16am, GMT+3, Taganrog, Monday;
	This is Payroll Time Clocking Password Page class implementation file.
*/
#include "StdAfx.h"
#include "PayrollTimeClock_FS_Page_0x3.h"
#include "PayrollTimeClock_Resource.h"

using namespace Payroll;
using namespace Payroll::time_clock;
using namespace Payroll::time_clock::UI;

using namespace ex_ui;
using namespace ex_ui::controls;

////////////////////////////////////////////////////////////////////////////

CEditCtrl_Ex::CEditCtrl_Ex(IControlNotify& snk_) : m_sink(snk_)
{
}

////////////////////////////////////////////////////////////////////////////

LRESULT CEditCtrl_Ex::OnKeyDown (UINT, WPARAM wParam, LPARAM, BOOL& bHandled)
{
	bHandled = FALSE;
	if (VK_RETURN == wParam)
	{
		bHandled = TRUE;
		m_sink.IControlNotify_OnClick(IDC_PAYROLL_FS_WND_EXIT);
	}
	return 0;
}

////////////////////////////////////////////////////////////////////////////

namespace Payroll { namespace time_clock { namespace UI { namespace details
{
	class CMainFramePage_3_Layout
	{
	public:
		enum { // from PSD files
			pmt_msg_w   = 450,
			pmt_msg_h   = 175,
			cls_btn_w   = 446,
			cls_btn_h   = 101,
			pwd_edt_w   = 450,
			pwd_edt_h   =  50,
			ret_btn_w   = 446,
			ret_btn_h   = 101,
		};
	private:
		CWindow&  m_page_ref;
		RECT      m_page_rect;
	public:
		CMainFramePage_3_Layout(CWindow& page_ref) : m_page_ref(page_ref)
		{
			::SetRectEmpty(&m_page_rect);
			if (m_page_ref.IsWindow())
				m_page_ref.GetClientRect(&m_page_rect);
		}
	public:
		RECT     GetCloseRect(void)const
		{
			const RECT rc_edit = this->GetPwdRect();

			const INT nLeft = (__W(m_page_rect) - CMainFramePage_3_Layout::cls_btn_w) / 2;
			const INT nTop  = rc_edit.bottom + 10;

			RECT rc_ = {0};
			::SetRect(
					&rc_,
					nLeft,
					nTop,
					nLeft + CMainFramePage_3_Layout::cls_btn_w,
					nTop  + CMainFramePage_3_Layout::cls_btn_h
				);
			return rc_;
		}

		RECT     GetPromptRect(void)const
		{
			const RECT rc_edit = this->GetPwdRect();

			const INT nLeft = (__W(m_page_rect) - CMainFramePage_3_Layout::pmt_msg_w) / 2;
			const INT nTop  = rc_edit.top - 10 - CMainFramePage_3_Layout::pmt_msg_h;
			RECT rc_ = {0};
			::SetRect(
					&rc_,
					nLeft,
					nTop,
					nLeft + CMainFramePage_3_Layout::pmt_msg_w,
					nTop  + CMainFramePage_3_Layout::pmt_msg_h
				);
			return rc_;
		}

		RECT     GetPwdRect(void)const
		{
			const INT nLeft = (__W(m_page_rect) - CMainFramePage_3_Layout::pwd_edt_w) / 2;
			const INT nTop  = (__H(m_page_rect) - CMainFramePage_3_Layout::pwd_edt_h) / 2;

			RECT rc_ = {0};
			::SetRect(
					&rc_,
					nLeft,
					nTop,
					nLeft + CMainFramePage_3_Layout::pwd_edt_w,
					nTop  + CMainFramePage_3_Layout::pwd_edt_h
				);
			return rc_;
		}

		RECT     GetRetryRect(void)const
		{
			const RECT rc_cls = this->GetCloseRect();

			const INT nLeft = (__W(m_page_rect) - CMainFramePage_3_Layout::ret_btn_w) / 2;
			const INT nTop  = rc_cls.bottom + 10;

			RECT rc_ = {0};
			::SetRect(
					&rc_,
					nLeft,
					nTop,
					nLeft + CMainFramePage_3_Layout::ret_btn_w,
					nTop  + CMainFramePage_3_Layout::ret_btn_h
				);
			return rc_;
		}
	};
}}}}

////////////////////////////////////////////////////////////////////////////

CMainFramePage_3::CMainFramePage_3(IRenderer& rnd_ref, IControlNotify& snk_ref, CSharedObjects& obj_ref):
	TPageBase(IDD_PAYROLL_TC_PAGE_PASSWORD, *this),
	m_parent_rnd_ref(rnd_ref),
	m_parent_snk_ref(snk_ref),
	m_shared(obj_ref),
	m_btn_cls(IDC_PAYROLL_FS_WND_EXIT, rnd_ref, snk_ref),
	m_btn_ret(IDC_PAYROLL_FS_WND_PAGE_3_RET, rnd_ref, snk_ref),
	m_pass_ctrl(snk_ref)
{
	m_res_msg.SetParentRendererPtr(&rnd_ref);
	HRESULT hr_ = S_OK;
	const HINSTANCE hInstance = ::ATL::_AtlBaseModule.GetModuleInstance();
	hr_ = m_btn_cls.SetImage(eControlState::eNormal   , IDR_PAYROLL_FS_WND_PAGE_3_CLS_N, hInstance); ATLASSERT(S_OK == hr_);
	hr_ = m_btn_cls.SetImage(eControlState::eDisabled , IDR_PAYROLL_FS_WND_PAGE_3_CLS_N, hInstance); ATLASSERT(S_OK == hr_);
	hr_ = m_btn_cls.SetImage(eControlState::ePressed  , IDR_PAYROLL_FS_WND_PAGE_3_CLS_P, hInstance); ATLASSERT(S_OK == hr_);
	hr_ = m_btn_cls.SetImage(eControlState::eHovered  , IDR_PAYROLL_FS_WND_PAGE_3_CLS_H, hInstance); ATLASSERT(S_OK == hr_);

	hr_ = m_btn_ret.SetImage(eControlState::eNormal   , IDR_PAYROLL_FS_WND_PAGE_3_RET_N, hInstance); ATLASSERT(S_OK == hr_);
	hr_ = m_btn_ret.SetImage(eControlState::eDisabled , IDR_PAYROLL_FS_WND_PAGE_3_RET_N, hInstance); ATLASSERT(S_OK == hr_);
	hr_ = m_btn_ret.SetImage(eControlState::ePressed  , IDR_PAYROLL_FS_WND_PAGE_3_RET_P, hInstance); ATLASSERT(S_OK == hr_);
	hr_ = m_btn_ret.SetImage(eControlState::eHovered  , IDR_PAYROLL_FS_WND_PAGE_3_RET_H, hInstance); ATLASSERT(S_OK == hr_);
}

CMainFramePage_3::~CMainFramePage_3(void)
{
}

////////////////////////////////////////////////////////////////////////////

HRESULT    CMainFramePage_3::Create(const HWND hParent, const RECT& rcArea, const bool bVisible)
{
	HRESULT hr_ = TPageBase::Create(hParent, rcArea, bVisible);
	if (S_OK != hr_)
		return  hr_;
	CWindow host = TPageBase::GetWindow_Ref();

	details::CMainFramePage_3_Layout layout(host);

	{
		const RECT rc_ = layout.GetPromptRect();
		hr_ = m_res_msg.Create(host, rc_);
		hr_ = m_res_msg.SetImage(IDR_PAYROLL_FS_WND_PAGE_3_MSG_0);
		hr_ = m_res_msg.UpdateLayout();
	}
	CWindow ctrl = TPageBase::GetControl_Safe(IDC_PAYROLL_FS_WND_PAGE_3_PWD);
	if (ctrl)
	{
		draw::common::CFont fnt_(NULL, draw::common::eCreateFontOption::eExactSize, 32);
		ctrl.SetFont(fnt_.Detach());
		RECT rc_ = layout.GetPwdRect();
		ctrl.MoveWindow(&rc_);
		m_pass_ctrl.SubclassWindow(ctrl);
	}
	{
		RECT rc_ = layout.GetCloseRect();
		hr_ = m_btn_cls.Create(host, &rc_, false);
	}
	{
		RECT rc_ = layout.GetRetryRect();
		hr_ = m_btn_ret.Create(host, &rc_, false);
	}
	return hr_;
}

HRESULT    CMainFramePage_3::Destroy(void)
{
	m_pass_ctrl.UnsubclassWindow();
	m_btn_ret.Destroy();
	m_btn_cls.Destroy();
	m_res_msg.Destroy();
	HRESULT hr_ = TPageBase::Destroy();
	return  hr_;
}

HRESULT    CMainFramePage_3::Show(void)
{
	if (!m_password.IsEmpty())m_password.Empty();

	if (m_res_msg.GetWindow_Ref().IsWindow())
	{
		m_res_msg.SetImage(IDR_PAYROLL_FS_WND_PAGE_3_MSG_0);
		m_res_msg.UpdateLayout();
		m_res_msg.Refresh();
	}

	CWindow ctrl = TPageBase::GetControl_Safe(IDC_PAYROLL_FS_WND_PAGE_3_PWD);
	if (ctrl)
	{
		ctrl.SetWindowText(NULL);
	}

	HRESULT hr_ = TPageBase::Show();
	return  hr_;
}

HRESULT    CMainFramePage_3::UpdateLayout(LPRECT const pRect)
{
	HRESULT hr_ = TPageBase::UpdateLayout(pRect);

	details::CMainFramePage_3_Layout layout(TPageBase::GetWindow_Ref());

	if (m_res_msg.GetWindow_Ref().IsWindow())
	{
		RECT rc_ = layout.GetPromptRect();
		m_res_msg.GetWindow_Ref().MoveWindow(&rc_);
	}
	CWindow cls_wnd = m_btn_cls.GetWindow();
	if (cls_wnd.IsWindow())
	{
		RECT rc_ = layout.GetCloseRect();
		cls_wnd.MoveWindow(&rc_);
	}
	CWindow ctrl = TPageBase::GetControl_Safe(IDC_PAYROLL_FS_WND_PAGE_3_PWD);
	if (ctrl)
	{
		RECT rc_ = layout.GetPwdRect();
		ctrl.MoveWindow(&rc_);
	}
	CWindow ret_wnd = m_btn_ret.GetWindow();
	if (ret_wnd.IsWindow())
	{
		RECT rc_ = layout.GetRetryRect();
		ret_wnd.MoveWindow(&rc_);
	}

	return  hr_;
}

////////////////////////////////////////////////////////////////////////////

LRESULT    CMainFramePage_3::MessageHandler_OnMessage(UINT uMsg, WPARAM wParam, LPARAM, BOOL& bHandled)
{
	bHandled = FALSE;
	switch (uMsg)
	{
	case WM_COMMAND:
		{
			const WORD wNotifyCode = HIWORD(wParam);wNotifyCode;
			const WORD wCtrlId     = LOWORD(wParam);wCtrlId;
			if (EN_CHANGE == wNotifyCode)
			{
				::WTL::CEdit ctrl = TPageBase::GetControl_Safe(IDC_PAYROLL_FS_WND_PAGE_3_PWD);
				if (ctrl)
					ctrl.GetWindowText(m_password);
			}
			if (EN_SETFOCUS == wNotifyCode)
			{
#include <shellapi.h>
				::ShellExecute(TPageBase::GetWindow_Ref(), _T("Open"), _T("TabTip.exe"), NULL, NULL, SW_SHOWNORMAL);
			}
		} break;
	}
	return 0;
}

////////////////////////////////////////////////////////////////////////////

HRESULT    CMainFramePage_3::ComparePassword(void)const
{
	if (!m_shared.Settings().HasPassword())
		return S_OK;
	HRESULT hr_ = S_FALSE;
	::ATL::CAtlString cs_password = m_shared.Settings().GetPassword();
	if (0 == cs_password.Compare(m_password))
		hr_ = S_OK;
	else
	{
		if (m_res_msg.GetWindow_Ref().IsWindow())
		{
			m_res_msg.SetImage(IDR_PAYROLL_FS_WND_PAGE_3_MSG_1);
			m_res_msg.UpdateLayout();
			m_res_msg.Refresh();
		}
	}
	return  hr_;
}

HRESULT    CMainFramePage_3::OnEnter(void)
{
	HRESULT hr_ = S_OK;
	CWindow act_ = ::GetActiveWindow();
	CWindow pss_ = TPageBase::GetControl_Safe(IDC_PAYROLL_FS_WND_PAGE_3_PWD);
	if (act_ == pss_)
		m_parent_snk_ref.IControlNotify_OnClick(IDC_PAYROLL_FS_WND_EXIT);
	return  hr_;
}