/*
	Created by Tech_dog(VToropov) on 15-Feb-2015 at 5:47:39pm, GMT+3, Taganrog, Sunday;
	This is Payroll Time Congratulation Page class implementation file.
*/
#include "StdAfx.h"
#include "PayrollTimeClock_FS_Page_0x2.h"
#include "PayrollTimeClock_Resource.h"
#include "PayrollTimeClock_RecorderBase.h"

using namespace Payroll;
using namespace Payroll::time_clock;
using namespace Payroll::time_clock::UI;

using namespace ex_ui;
using namespace ex_ui::controls;

////////////////////////////////////////////////////////////////////////////

namespace Payroll { namespace time_clock { namespace UI { namespace details
{
	UINT  MainFramePage_2_EventTimerId(void)
	{
		static const DWORD timer_id = ::GetTickCount();
		return timer_id;
	}

	UINT  MainFramePage_2_EventTimerFreq(void)
	{
		static const DWORD timer_freq = 2200;
		return timer_freq;
	}

	class CMainFramePage_2_Layout
	{
	public:
		enum { // from PSD files
			res_msg_w   = 720,
			res_msg_h   = 395,
			res_emp_w   = 800,
			res_emp_h   = 100,
		};
	private:
		CWindow&  m_page_ref;
		RECT      m_page_rect;
	public:
		CMainFramePage_2_Layout(CWindow& page_ref) : m_page_ref(page_ref)
		{
			::SetRectEmpty(&m_page_rect);
			if (m_page_ref.IsWindow())
				m_page_ref.GetClientRect(&m_page_rect);
		}
	public:
		RECT     GetEmpRect(void)const
		{
			const INT nLeft = 0;
			const INT nTop  = (__H(m_page_rect) - CMainFramePage_2_Layout::res_emp_h) / 2 - CMainFramePage_2_Layout::res_emp_h / 2 - 15; //TODO: remove magic numbers
			RECT rc_ = {0};
			::SetRect(
					&rc_,
					nLeft,
					nTop,
					nLeft + __W(m_page_rect),
					nTop  + CMainFramePage_2_Layout::res_emp_h
				);
			return rc_;
		}

		RECT     GetMsgRect(void)const
		{
			const INT nLeft = (__W(m_page_rect) - CMainFramePage_2_Layout::res_msg_w) / 2;
			const INT nTop  = (__H(m_page_rect) - CMainFramePage_2_Layout::res_msg_h) / 2;
			RECT rc_ = {0};
			::SetRect(
					&rc_,
					nLeft,
					nTop,
					nLeft + CMainFramePage_2_Layout::res_msg_w,
					nTop  + CMainFramePage_2_Layout::res_msg_h
				);
			return rc_;
		}
	};
}}}}

////////////////////////////////////////////////////////////////////////////

CMainFramePage_2::CMainFramePage_2(IRenderer& rnd_ref, IControlNotify& snk_ref, CSharedObjects& obj_ref):
	TPageBase(IDD_PAYROLL_TC_PAGE_FINISH, *this),
	m_parent_rnd_ref(rnd_ref),
	m_parent_snk_ref(snk_ref),
	m_shared(obj_ref),
	m_evt_timer(NULL),
	m_emp_name(CControlCrt(1, rnd_ref, snk_ref))
{
	m_res_msg.SetParentRendererPtr(&rnd_ref);
}

CMainFramePage_2::~CMainFramePage_2(void)
{
}

////////////////////////////////////////////////////////////////////////////

HRESULT    CMainFramePage_2::Create(const HWND hParent, const RECT& rcArea, const bool bVisible)
{
	HRESULT hr_ = TPageBase::Create(hParent, rcArea, bVisible);
	if (S_OK != hr_)
		return  hr_;
	CWindow host = TPageBase::GetWindow_Ref();

	details::CMainFramePage_2_Layout layout(host);
	{
		const RECT rc_ = layout.GetMsgRect();
		hr_ = m_res_msg.Create(host, rc_);
		hr_ = m_res_msg.SetImage(IDR_PAYROLL_FS_WND_PAGE_2_RS_MSG_0);
		hr_ = m_res_msg.UpdateLayout();
	}
	{
		const RECT rc_ = layout.GetEmpRect();
		hr_ = m_emp_name.Create(host, rc_, NULL);
		hr_ = m_emp_name.ForeColor(m_shared.Settings().GetEmpForeColor()); // can return S_FALSE;

	}
	return (hr_ = S_OK); // do not care about control creation for now;
}

HRESULT    CMainFramePage_2::Destroy(void)
{
	m_emp_name.Destroy();
	m_res_msg.Destroy();
	HRESULT hr_ = TPageBase::Destroy();
	return  hr_;
}

HRESULT    CMainFramePage_2::Show(void)
{
	HRESULT hr_ = TPageBase::Show();
	if (S_OK == hr_)
		TPageBase::GetWindow_Ref().Invalidate(TRUE);
	const CRecorderBase* const pRecorder = m_shared.Recorder();
	if (pRecorder)
	{
		::ATL::CAtlString cs_text;
		cs_text.Format(
				_T("%s at %s"),
				pRecorder->Cached().Name(),
				pRecorder->GetLastSaveTime()
			);
		m_emp_name.Text(cs_text);
	}
	LPCTSTR pszSoundFile = m_shared.Settings().GetSuccessSoundFile();
	if (pszSoundFile)
		::PlaySound( pszSoundFile, NULL, SND_FILENAME|SND_ASYNC );

	if (!m_evt_timer)
	{
		CWindow& host = TPageBase::GetWindow_Ref();
		m_evt_timer = host.SetTimer(
				details::MainFramePage_2_EventTimerId(),
				details::MainFramePage_2_EventTimerFreq()
			);
	}
	return  hr_;
}

HRESULT    CMainFramePage_2::UpdateLayout(LPRECT const pRect)
{
	HRESULT hr_ = TPageBase::UpdateLayout(pRect);

	details::CMainFramePage_2_Layout layout(TPageBase::GetWindow_Ref());

	if (m_res_msg.GetWindow_Ref().IsWindow())
	{
		RECT rc_ = layout.GetMsgRect();
		m_res_msg.GetWindow_Ref().MoveWindow(&rc_);
	}

	CWindow emp_ = m_emp_name.GetWindow();
	if (emp_.IsWindow())
	{
		RECT rc_ = layout.GetEmpRect();
		emp_.MoveWindow(&rc_);
	}
	return  hr_;
}

////////////////////////////////////////////////////////////////////////////

LRESULT    CMainFramePage_2::MessageHandler_OnMessage(UINT uMsg, WPARAM, LPARAM, BOOL& bHandled)
{
	bHandled = FALSE;
	switch (uMsg)
	{
	case WM_TIMER:
		{
			bHandled = TRUE;
			CWindow& host = TPageBase::GetWindow_Ref();

			host.KillTimer(m_evt_timer); m_evt_timer = NULL;
			if (host.IsWindowVisible())
			m_parent_snk_ref.IControlNotify_OnClick(IDC_PAYROLL_FS_WND_PAGE_2_FINISHED);
		} break;
	}
	return 0;
}