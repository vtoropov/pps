#ifndef __PAYROLLTIMECLOCKAPPLICATIONSHAREDOBJECTS_H_D9BA2E67_341A_42a4_896E_79179661D8AD_INCLUDED
#define __PAYROLLTIMECLOCKAPPLICATIONSHAREDOBJECTS_H_D9BA2E67_341A_42a4_896E_79179661D8AD_INCLUDED
/*
	Created by Tech_dog (VToropov) on 8-Apr-2014 at 4:40:40pm, GMT+4, Saint-Petersburg, Tuesday;
	This is Payroll Time Clock Application Shared Objects class declaratioon file.
*/
#include "PPS_SharedObjects.h"
#include "PayrollTimeClock_CommonSettings.h"

namespace Payroll { namespace time_clock
{
	class CRecorderBase;
}}

namespace Payroll { namespace time_clock { namespace common
{
	using Payroll::time_clock::CRecorderBase;
	using Payroll::time_clock::common::CCommonSettings;

	class CSharedObjects : public Platinum::client::common::CSharedObjects
	{
		typedef Platinum::client::common::CSharedObjects  TSharedObjectBase;
	private:
		CCommonSettings&     m_settings2;
		CRecorderBase*       m_pRecorder;
	public:
		CSharedObjects(CCommonSettings&);
		~CSharedObjects(void);
	public:
		CRecorderBase* const Recorder(void);
		VOID                 Recorder(CRecorderBase* const);
		const CCommonSettings&       Settings(void)      const; // gets common settings reference (read-only)
		CCommonSettings&             Settings(void)           ; // gets common settings reference (read-write)
	};
}}}

#endif/*__PAYROLLTIMECLOCKAPPLICATIONSHAREDOBJECTS_H_D9BA2E67_341A_42a4_896E_79179661D8AD_INCLUDED*/