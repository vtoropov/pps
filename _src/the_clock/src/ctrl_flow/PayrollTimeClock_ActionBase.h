#ifndef _PAYROLLTIMECLOCKASYNCHACTIONBASE_H__F63934AF_7A7E_433e_84E4_74A46E3F236D_INCLUDED
#define _PAYROLLTIMECLOCKASYNCHACTIONBASE_H__F63934AF_7A7E_433e_84E4_74A46E3F236D_INCLUDED
/*
	Created by Tech_dog (VToropov) on 11-Feb-2015 at 1:46:43pm, GMT+3, Taganrog, Wednesday;
	This is Payroll Time Clocking Asynchronous Action Base class declaration file.
*/
#include "Shared_GenericRunnableObject.h"
#include "PayrollTimeClock_SharedObjects.h"

namespace Payroll { namespace time_clock { namespace ctrl_flow
{
	using shared::lite::events::IGenericEventNotify;
	using shared::lite::events::CGenericRunnableObject;
	using shared::lite::events::TRunnableFunc;

	using Payroll::time_clock::common::CSharedObjects;

	interface ICanContinue
	{
		virtual bool          ICanContinue_CheckState(void) PURE;
	};

	class CActionBase:
		public  CGenericRunnableObject
	{
		typedef CGenericRunnableObject TRunnable;
	private:
		volatile bool     m_bInterrupted;
	protected:
		volatile HRESULT  m_result;
		CSharedObjects&   m_shared;
	protected:
		CActionBase(TRunnableFunc, IGenericEventNotify&, const UINT eventId, CSharedObjects&);
		virtual ~CActionBase(void);
	public:
		HRESULT           Result(void)const;
		void              Result(const HRESULT);
		CSharedObjects&   SharedOjects(void);
	protected:
		HRESULT           ReadyToStart(void) const;
		HRESULT           ReadyToStop(void) const;
	public:
		bool              Interrupted(void)const;
		VOID              Interrupted(const bool);
	private:
		CActionBase(const CActionBase&);
		CActionBase& operator= (const CActionBase&);
	};

	class CInterrupt : public ICanContinue
	{
	private:
		CActionBase&      m_action;
	public:
		CInterrupt(CActionBase& act_ref) : m_action(act_ref){}
	public:
		virtual bool      ICanContinue_CheckState(void) override sealed;
	};
}}}

#endif/*_PAYROLLTIMECLOCKASYNCHACTIONBASE_H__F63934AF_7A7E_433e_84E4_74A46E3F236D_INCLUDED*/