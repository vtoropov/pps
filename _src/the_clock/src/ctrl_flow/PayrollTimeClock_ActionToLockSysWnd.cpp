/*
	Created by Tech_dog (VToropov) on 16-Feb-2015 at 8:00:44pm, GMT+3, Taganrog, Monday;
	This is Payroll Time Clocking System Windows Locker class implementation file. 
*/
#include "StdAfx.h"
#include "PayrollTimeClock_ActionToLockSysWnd.h"
#include "PayrollTimeClock_RecorderBase.h"
#include "PayrollTimeClock_Resource.h"
#include "Shared_SystemCore.h"

using namespace Payroll;
using namespace Payroll::time_clock;
using namespace Payroll::time_clock::ctrl_flow;

////////////////////////////////////////////////////////////////////////////

namespace Payroll { namespace time_clock { namespace ctrl_flow { namespace details
{
	using shared::lite::sys_core::CComAutoInitializer;
	using Payroll::time_clock::ICanContinue;

	static unsigned int __stdcall ThreadFunction_LockSysWnd(PVOID pObject)
	{
		TRACE_FUNC();
		if (NULL == pObject)
			return 1;
		CGenericRunnableObject* pRunnable = NULL;
		CSysWndLockAction*      pAction   = NULL;
		try
		{
			pRunnable = reinterpret_cast<CGenericRunnableObject*>(pObject);
			pAction   = dynamic_cast<CSysWndLockAction*>(pRunnable);
		}
		catch (::std::bad_cast&) { return 1; }

		CComAutoInitializer com_lib(false);
		if (!com_lib.IsSuccess())
			return 1;

		CSysWndLockAction& act_ref = *pAction;
		CSharedObjects& obj_ref = act_ref.SharedOjects();obj_ref;

		CInterrupt clbk(act_ref);

		static LPCTSTR pszCharmBarCap = _T("Charm Bar");
		static LPCTSTR pszTaskBarCls = _T("Shell_TrayWnd");

		CWindow wnd_charm;
		CWindow wnd_task;

		HRESULT hr_ = S_OK;
		INT n_it = 0;
		do
		{
			if (!clbk.ICanContinue_CheckState())
				break;
			::Sleep(10);
			if (!wnd_charm)
				wnd_charm = ::FindWindow(NULL, pszCharmBarCap);
			if (!wnd_task)
				wnd_task = ::FindWindow(pszTaskBarCls, NULL);
			n_it += 1;
		} while (!wnd_charm && !wnd_task && n_it < 100);

		CWindow wnd_charm_parent = NULL;
		if (wnd_charm)
			wnd_charm_parent = wnd_charm.SetParent(act_ref.GetHiddenScreen());

		CWindow wnd_task_parent = NULL;
		if (wnd_task)
			wnd_task_parent = wnd_task.SetParent(act_ref.GetHiddenScreen());

		while(clbk.ICanContinue_CheckState())
			::Sleep(50);

		if (wnd_charm)
			wnd_charm.SetParent(wnd_charm_parent);

		if (wnd_task)
			wnd_task.SetParent(wnd_task_parent);

		act_ref.MarkCompleted();
		act_ref.Result(hr_);
		act_ref.Event().Fire();
		return 0;
	}
}}}}

////////////////////////////////////////////////////////////////////////////

VOID    CSysWndLockAction::CHiddenSceen::Create(const HWND)
{
	if (m_hidden.IsWindow() == FALSE)
		m_hidden.Create(HWND_MESSAGE);
}

VOID    CSysWndLockAction::CHiddenSceen::Destroy(void)
{
	if (m_hidden.IsWindow())
		m_hidden.SendMessage(WM_CLOSE);
}

CWindow&CSysWndLockAction::CHiddenSceen::GetWindow_Ref(void)
{
	return m_hidden;
}

#define TASK_ID (13)
////////////////////////////////////////////////////////////////////////////

CSysWndLockAction::CSysWndLockAction(CSharedObjects& shared_ref, IGenericEventNotify& snk_ref, ::ATL::CWindow& owner_ref):
	CActionBase(details::ThreadFunction_LockSysWnd, snk_ref, TASK_ID, shared_ref), m_owner_ref(owner_ref)
{
	m_screen.Create(owner_ref);
}

CSysWndLockAction::~CSysWndLockAction(void)
{
	m_screen.Destroy();
}

////////////////////////////////////////////////////////////////////////////

HRESULT   CSysWndLockAction::Stop(const bool bForced)
{
	if (CActionBase::IsStopped())
		return S_OK;
	this->Interrupted(true);
	HRESULT hr_ = CActionBase::Stop(bForced);
	this->Interrupted(false);
	return  hr_;
}

////////////////////////////////////////////////////////////////////////////

CWindow&  CSysWndLockAction::GetOwnerRef(void)const
{
	return m_owner_ref;
}

CWindow&  CSysWndLockAction::GetHiddenScreen(void)
{
	return m_screen.GetWindow_Ref();
}