/*
	Created by Tech_dog (VToropov) on 25-May-2015 at 11:13:31am, GMT+8, Phuket, Monday;
	This is Payroll Time Clocking Fv Dialog Minimize Asynchronous Action class implementation file.
*/
#include "StdAfx.h"
#include "PayrollTimeClock_ActionToFvMin.h"
#include "PayrollTimeClock_FvDialogFinder.h"
#include "PayrollTimeClock_Resource.h"
#include "Shared_SystemCore.h"
#include "Shared_GenericAppObject.h"

using namespace Payroll;
using namespace Payroll::time_clock;
using namespace Payroll::time_clock::ctrl_flow;
using namespace shared::lite::common;

////////////////////////////////////////////////////////////////////////////

namespace Payroll { namespace time_clock { namespace ctrl_flow { namespace details
{
	using shared::lite::sys_core::CComAutoInitializer;

	using Payroll::time_clock::ctrl_flow::ICanContinue;
	using Payroll::time_clock::common::eFvCustomizeType;

	static unsigned int __stdcall ThreadFunction_MinimizeFvDialog(PVOID pObject)
	{
		TRACE_FUNC();
		if (NULL == pObject)
			return 1;
		CGenericRunnableObject* pRunnable = NULL;
		CFvMinimizeAction*      pAction   = NULL;
		try
		{
			pRunnable = reinterpret_cast<CGenericRunnableObject*>(pObject);
			pAction   = dynamic_cast<CFvMinimizeAction*>(pRunnable);
		}
		catch (::std::bad_cast&) { return 1; }

		CComAutoInitializer com_lib(false);
		if (!com_lib.IsSuccess())
			return 1;

		CFvMinimizeAction& act_ref = *pAction;

		CInterrupt sink(act_ref);
		CFvDialogFinder finder(sink);

		HRESULT hr_ = finder.Find();
		::ATL::CWindow fvi_dlg = finder.Dialog();

		if (fvi_dlg)
		{
			CApplication& the_app = global::GetAppObjectRef();
			{
				::ATL::CAtlString cs_title;
				cs_title.Format(
						_T("%s %s"),
						the_app.Version().ProductName(),
						the_app.Version().ProductVersion()
					);
				fvi_dlg.SetWindowTextW(cs_title);
			}
			::Sleep(700); // it is for giving an ability to the FV dialog procedure to redraw client area;
			LONG_PTR lStyles = fvi_dlg.GetWindowLongPtr(GWL_STYLE);
			lStyles |= WS_MINIMIZEBOX;
			fvi_dlg.SetWindowLongPtr(GWL_STYLE, lStyles);
			fvi_dlg.SetWindowPos(NULL, 0, 0, 0, 0, SWP_NOSIZE|SWP_NOMOVE|SWP_NOZORDER|SWP_NOACTIVATE|SWP_FRAMECHANGED);
			act_ref.Dialog(fvi_dlg);
		}
		act_ref.MarkCompleted();
		act_ref.Result(hr_);
		act_ref.Event().Fire();
		return 0;
	}
}}}}

////////////////////////////////////////////////////////////////////////////

CFvMinimizeAction::CFvMinimizeAction(CSharedObjects& shared_ref, IGenericEventNotify& snk_ref):
	CActionBase(details::ThreadFunction_MinimizeFvDialog, snk_ref, IDD_PAYROLL_TC_PAGE_SCAN_FV_CUSTOM, shared_ref)
{
}

CFvMinimizeAction::~CFvMinimizeAction(void)
{
}

////////////////////////////////////////////////////////////////////////////

HRESULT   CFvMinimizeAction::Stop(const bool bForced)
{
	if (CActionBase::IsStopped())
		return S_OK;
	this->Interrupted(true);
	HRESULT hr_ = CActionBase::Stop(bForced);
	this->Interrupted(false);
	return  hr_;
}

////////////////////////////////////////////////////////////////////////////

VOID      CFvMinimizeAction::Dialog(const HWND hDialog)
{
	m_dlg = hDialog;
}

HWND      CFvMinimizeAction::Dialog(void)const
{
	return m_dlg;
}