/*
	Created by Tech_dog (VToropov) on 11-Feb-2015 at 1:56:13pm, GMT+3, Taganrog, Wednesday;
	This is Payroll Time Clocking Asynchronous Action Base class implementation file.
*/
#include "StdAfx.h"
#include "PayrollTimeClock_ActionBase.h"

using namespace Payroll;
using namespace Payroll::time_clock;
using namespace Payroll::time_clock::ctrl_flow;

////////////////////////////////////////////////////////////////////////////

CActionBase::CActionBase(TRunnableFunc func, IGenericEventNotify& sink_ref, const UINT eventId, CSharedObjects& shared_ref):
	TRunnable(func, sink_ref, eventId),
	m_result(OLE_E_BLANK),
	m_shared(shared_ref),
	m_bInterrupted(false)
{
}

CActionBase::~CActionBase(void)
{
}

////////////////////////////////////////////////////////////////////////////

HRESULT         CActionBase::Result(void)const
{
	return m_result;
}

void            CActionBase::Result(const HRESULT hr_)
{
	m_result = hr_;
}

CSharedObjects& CActionBase::SharedOjects(void)
{
	return m_shared;
}

////////////////////////////////////////////////////////////////////////////

HRESULT         CActionBase::ReadyToStart(void)const
{
	TRACE_FUNC();
	if (!TRunnable::IsStopped())
	{
		TRACE_WARN(_T("The asynchronous process is already started"));
		return S_FALSE;
	}
	return  S_OK;
}

HRESULT         CActionBase::ReadyToStop(void)const
{
	TRACE_FUNC();
	if (TRunnable::IsStopped())
	{
		TRACE_WARN(_T("The asynchronous process is not running"));
		return S_OK;
	}
	return  S_OK;
}

////////////////////////////////////////////////////////////////////////////

bool            CActionBase::Interrupted(void)const
{
	return m_bInterrupted;
}

VOID            CActionBase::Interrupted(const bool bInterrupted)
{
	m_bInterrupted = bInterrupted;
}

////////////////////////////////////////////////////////////////////////////

bool            CInterrupt::ICanContinue_CheckState(void)
{
	return !m_action.Interrupted();
}