#ifndef _PAYROLLTIMECLOCKFVDLGCUSTOMACTION_H_F327E27E_8569_4158_BE35_84DF0147690F_INCLUDED
#define _PAYROLLTIMECLOCKFVDLGCUSTOMACTION_H_F327E27E_8569_4158_BE35_84DF0147690F_INCLUDED
/*
	Created by Tech_dog (VToropov) on 14-Feb-2015 at 12:42:05pm, GMT+3, Taganrog, Saturday;
	This is Payroll Time Clocking Fv Dialog Customization Asynchronous Action class declaration file.
*/
#include "PayrollTimeClock_ActionBase.h"

namespace Payroll { namespace time_clock { namespace ctrl_flow
{
	using shared::lite::events::IGenericEventNotify;
	using Payroll::time_clock::common::CSharedObjects;

	class CFvCustomizeAction : public CActionBase
	{
	private:
		class CHiddenScreen
		{
		private:
			class CHiddenScreenWnd:
				public ::ATL::CWindowImpl<CHiddenScreenWnd>
			{
			public:
				DECLARE_EMPTY_MSG_MAP();
			};
		private:
			CHiddenScreenWnd m_hidden;
		public:
			VOID          Create(const HWND hParent);
			VOID          Destroy(void);
			CWindow&      GetWindow_Ref(void);
		};
	private:
		::ATL::CWindow&   m_owner_ref;
		CHiddenScreen     m_screen;
	public:
		CFvCustomizeAction(CSharedObjects&, IGenericEventNotify&, ::ATL::CWindow& owner_ref);
		~CFvCustomizeAction(void);
	public:
		virtual HRESULT   Stop(const bool bForced) override sealed;
	public:
		ATL::CWindow&     GetOwnerRef(void)const;
		ATL::CWindow&     GetHiddenScreen(void);
	};
}}}

#endif/*_PAYROLLTIMECLOCKFVDLGCUSTOMACTION_H_F327E27E_8569_4158_BE35_84DF0147690F_INCLUDED*/