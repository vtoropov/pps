#ifndef _PAYROLLTIMECLOCKFVDLGMINIMIZEACTION_H_06FDD910_2CF5_4eea_8D00_A368A59EE7BE_INCLUDED
#define _PAYROLLTIMECLOCKFVDLGMINIMIZEACTION_H_06FDD910_2CF5_4eea_8D00_A368A59EE7BE_INCLUDED
/*
	Created by Tech_dog (VToropov) on 25-May-2015 at 10:57:19am, GMT+8, Phuket, Monday;
	This is Payroll Time Clocking Fv Dialog Minimize Asynchronous Action class declaration file.
*/
#include "PayrollTimeClock_ActionBase.h"

namespace Payroll { namespace time_clock { namespace ctrl_flow
{
	using shared::lite::events::IGenericEventNotify;
	using Payroll::time_clock::common::CSharedObjects;

	class CFvMinimizeAction : public CActionBase
	{
	private:
		CWindow           m_dlg;
	public:
		CFvMinimizeAction(CSharedObjects&, IGenericEventNotify&);
		~CFvMinimizeAction(void);
	public:
		virtual HRESULT   Stop(const bool bForced) override sealed;
	public:
		VOID              Dialog(const HWND);
		HWND              Dialog(void)const;
	};
}}}

#endif/*_PAYROLLTIMECLOCKFVDLGMINIMIZEACTION_H_06FDD910_2CF5_4eea_8D00_A368A59EE7BE_INCLUDED*/