/*
	Created by Tech_dog (VToropov) on 11-Feb-2015 at 2:04:38pam, GMT+3, Taganrog, Wednesday;
	This is Payroll Time Clocking Finger Scan Asynchronous Action class implementation file.
*/
#include "StdAfx.h"
#include "PayrollTimeClock_ActionToScan.h"
#include "PayrollTimeClock_RecorderBase.h"
#include "PayrollTimeClock_Resource.h"
#include "Shared_SystemCore.h"

using namespace Payroll;
using namespace Payroll::time_clock;
using namespace Payroll::time_clock::ctrl_flow;

////////////////////////////////////////////////////////////////////////////

namespace Payroll { namespace time_clock { namespace ctrl_flow { namespace details
{
	using shared::lite::sys_core::CComAutoInitializer;
	using Payroll::time_clock::ICanContinue;

	static unsigned int __stdcall ThreadFunction_ScanFinger(PVOID pObject)
	{
		TRACE_FUNC();
		if (NULL == pObject)
			return 1;
		CGenericRunnableObject* pRunnable = NULL;
		CFingerScanAction*      pAction   = NULL;
		try
		{
			pRunnable = reinterpret_cast<CGenericRunnableObject*>(pObject);
			pAction   = dynamic_cast<CFingerScanAction*>(pRunnable);
		}
		catch (::std::bad_cast&) { return 1; }

		CComAutoInitializer com_lib(false);
		if (!com_lib.IsSuccess())
			return 1;

		CFingerScanAction& act_ref = *pAction;
		CSharedObjects& obj_ref = act_ref.SharedOjects();

		CInterrupt clbk(act_ref);

		_variant_t vProcessed;
		HRESULT hr_ = obj_ref.Processor().CreateVerificationTemplate(vProcessed);
		if (S_OK == hr_)
		{
			act_ref.Event().Fire(false, IDC_PAYROLL_TC_PAGE_SCAN_PROCESS);

			CRecorderBase* pRecorder = obj_ref.Recorder();
			if (pRecorder)
			{
				hr_ = pRecorder->Verify(vProcessed, clbk);
			}
		}
		act_ref.MarkCompleted();
		act_ref.Result(hr_);
		act_ref.Event().Fire();
		return 0;
	}
}}}}

////////////////////////////////////////////////////////////////////////////

CFingerScanAction::CFingerScanAction(CSharedObjects& shared_ref, IGenericEventNotify& snk_ref):
	CActionBase(details::ThreadFunction_ScanFinger, snk_ref, 1, shared_ref)
{
}

CFingerScanAction::~CFingerScanAction(void)
{
}

////////////////////////////////////////////////////////////////////////////

HRESULT   CFingerScanAction::Stop(const bool bForced)
{
	if (!CActionBase::IsStopped())
	{
		this->Interrupted(true);
		CActionBase::m_shared.Processor().Cancel();
	}
	HRESULT hr_ = CActionBase::Stop(bForced);
	this->Interrupted(false);
	return  hr_;
}