#ifndef _PAYROLLTIMECLOCKINGFVDIALOGFINDER_H_DEEB89A8_8994_4809_9584_A1A995B21383_INCLUDED
#define _PAYROLLTIMECLOCKINGFVDIALOGFINDER_H_DEEB89A8_8994_4809_9584_A1A995B21383_INCLUDED
/*
	Created by Tech_dog (VToropov) on 25-May-2015 at 1:21:59pm, GMT+8, Phuket, Monday;
	This is Payroll Time Clocking Fv Dialog Finder class declaration file.
*/
#include "PayrollTimeClock_ActionBase.h"

namespace Payroll { namespace time_clock { namespace ctrl_flow
{
	class CFvDialogFinder
	{
	private:
		ICanContinue&   m_sink;
		CWindow         m_dlg;
	public:
		CFvDialogFinder(ICanContinue&);
		~CFvDialogFinder(void);
	public:
		const
		CWindow&        Dialog(void)const;
		HRESULT         Find(void);
	};
}}}

#endif/*_PAYROLLTIMECLOCKINGFVDIALOGFINDER_H_DEEB89A8_8994_4809_9584_A1A995B21383_INCLUDED*/