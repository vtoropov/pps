#ifndef _PAYROLLTIMECLCOCKINGSYSWNDLOCKER_H_EFE87CB4_21E1_4ec5_8179_0629218BD496_INCLUDED
#define _PAYROLLTIMECLCOCKINGSYSWNDLOCKER_H_EFE87CB4_21E1_4ec5_8179_0629218BD496_INCLUDED
/*
	Created by Tech_dog (VToropov) on 16-Feb-2015 at 7:52:25pm, GMT+3, Taganrog, Monday;
	This is Payroll Time Clocking System Windows Locker class declaration file. 
*/
#include "PayrollTimeClock_ActionBase.h"

namespace Payroll { namespace time_clock { namespace ctrl_flow
{
	using shared::lite::events::IGenericEventNotify;
	using Payroll::time_clock::common::CSharedObjects;

	class CSysWndLockAction : public CActionBase
	{
	private:
		class CHiddenSceen
		{
		private:
			class CHiddenScreenWnd:
				public ::ATL::CWindowImpl<CHiddenScreenWnd>
			{
			public:
				DECLARE_EMPTY_MSG_MAP();
			};
		private:
			CHiddenScreenWnd m_hidden;
		public:
			VOID          Create(const HWND hParent);
			VOID          Destroy(void);
			CWindow&      GetWindow_Ref(void);
		};
	private:
		::ATL::CWindow&   m_owner_ref;
		CHiddenSceen      m_screen;
	public:
		CSysWndLockAction(CSharedObjects&, IGenericEventNotify&, ::ATL::CWindow& owner_ref);
		~CSysWndLockAction(void);
	public:
		virtual HRESULT   Stop(const bool bForced) override sealed;
	public:
		ATL::CWindow&     GetOwnerRef(void)const;
		ATL::CWindow&     GetHiddenScreen(void);
	};
}}}

#endif/*_PAYROLLTIMECLCOCKINGSYSWNDLOCKER_H_EFE87CB4_21E1_4ec5_8179_0629218BD496_INCLUDED*/