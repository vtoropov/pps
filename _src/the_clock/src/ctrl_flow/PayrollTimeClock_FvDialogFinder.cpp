/*
	Created by Tech_dog (VToropov) on 25-May-2015 at 1:49:41pm, GMT+8, Phuket, Monday;
	This is Payroll Time Clocking Fv Dialog Finder class implementation file.
*/
#include "StdAfx.h"
#include "PayrollTimeClock_FvDialogFinder.h"

using namespace Payroll;
using namespace Payroll::time_clock;
using namespace Payroll::time_clock::ctrl_flow;

////////////////////////////////////////////////////////////////////////////

CFvDialogFinder::CFvDialogFinder(ICanContinue& snk_ref) : m_sink(snk_ref)
{
}

CFvDialogFinder::~CFvDialogFinder(void)
{
}

////////////////////////////////////////////////////////////////////////////

const
CWindow&        CFvDialogFinder::Dialog(void)const
{
	return m_dlg;
}

HRESULT         CFvDialogFinder::Find(void)
{
	static LPCTSTR pszCaption = _T("Scaning FV Image");
	static LPCTSTR pszClsName = _T("#32770");

	HRESULT hr_ = S_FALSE;
	INT n_it = 0;
	HWND hDialog = NULL;
	do
	{
		if (!m_sink.ICanContinue_CheckState())
			break;
		::Sleep(10);
		hDialog = ::FindWindow(pszClsName, pszCaption);
		n_it += 1;
	} while (NULL == hDialog && n_it < 1000);

	m_dlg = hDialog;
	hr_ = (m_dlg ? S_OK : S_FALSE);

	return hr_;
}