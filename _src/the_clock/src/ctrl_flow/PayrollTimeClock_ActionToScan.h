#ifndef _PAYROLLTIMECLOCKFINGERSCANACTION_H_F7D66ADE_1672_496a_9C52_31F7F0EF3635_INCLUDED
#define _PAYROLLTIMECLOCKFINGERSCANACTION_H_F7D66ADE_1672_496a_9C52_31F7F0EF3635_INCLUDED
/*
	Created by Tech_dog (VToropov) on 11-Feb-2015 at 2:04:38pam, GMT+3, Taganrog, Wednesday;
	This is Payroll Time Clocking Finger Scan Asynchronous Action class declaration file.
*/
#include "PayrollTimeClock_ActionBase.h"

namespace Payroll { namespace time_clock { namespace ctrl_flow
{
	using shared::lite::events::IGenericEventNotify;
	using Payroll::time_clock::common::CSharedObjects;

	class CFingerScanAction : public CActionBase
	{
	public:
		CFingerScanAction(CSharedObjects&, IGenericEventNotify&);
		~CFingerScanAction(void);
	public:
		virtual HRESULT   Stop(const bool bForced) override sealed;
	};
}}}

#endif/*_PAYROLLTIMECLOCKFINGERSCANACTION_H_F7D66ADE_1672_496a_9C52_31F7F0EF3635_INCLUDED*/