/*
	Created by Tech_dog (VToropov) on 14-Feb-2015 at 12:55:49pam, GMT+3, Taganrog, Saturday;
	This is Payroll Time Clocking Fv Dialog Customization Asynchronous Action class implementation file.
*/
#include "StdAfx.h"
#include "PayrollTimeClock_ActionToFvCustom.h"
#include "PayrollTimeClock_RecorderBase.h"
#include "PayrollTimeClock_Resource.h"
#include "PayrollTimeClock_FvDialogFinder.h"
#include "Shared_SystemCore.h"

using namespace Payroll;
using namespace Payroll::time_clock;
using namespace Payroll::time_clock::ctrl_flow;

////////////////////////////////////////////////////////////////////////////

namespace Payroll { namespace time_clock { namespace ctrl_flow { namespace details
{
	using shared::lite::sys_core::CComAutoInitializer;

	using Payroll::time_clock::ctrl_flow::ICanContinue;
	using Payroll::time_clock::common::eFvCustomizeType;

	static unsigned int __stdcall ThreadFunction_CustomizeFvDialog(PVOID pObject)
	{
		TRACE_FUNC();
		if (NULL == pObject)
			return 1;
		CGenericRunnableObject* pRunnable = NULL;
		CFvCustomizeAction*     pAction   = NULL;
		try
		{
			pRunnable = reinterpret_cast<CGenericRunnableObject*>(pObject);
			pAction   = dynamic_cast<CFvCustomizeAction*>(pRunnable);
		}
		catch (::std::bad_cast&) { return 1; }

		CComAutoInitializer com_lib(false);
		if (!com_lib.IsSuccess())
			return 1;

		CFvCustomizeAction& act_ref = *pAction;
		CSharedObjects& obj_ref = act_ref.SharedOjects();

		CInterrupt sink(act_ref);
		CFvDialogFinder finder(sink);

		HRESULT hr_ = finder.Find();
		::ATL::CWindow fvi_dlg = finder.Dialog();

		if (fvi_dlg)
		{
			const eFvCustomizeType::_e eCustomizationType = obj_ref.Settings().FvCustomization();

			CWindow& owner_ref = act_ref.GetOwnerRef();
			if (eFvCustomizeType::eBorder == eCustomizationType)
			{
				LONG_PTR lStyles = fvi_dlg.GetWindowLongPtr(GWL_STYLE);
				lStyles &= ~(WS_CAPTION | WS_SYSMENU | DS_MODALFRAME | DS_3DLOOK | DS_FIXEDSYS);
				lStyles |= WS_CHILD;
				fvi_dlg.SetWindowLongPtr(GWL_STYLE, lStyles);
			}
			if (eFvCustomizeType::eBorder == eCustomizationType)
			{
				LONG_PTR lStyles = fvi_dlg.GetWindowLongPtr(GWL_EXSTYLE);
				lStyles &= ~(WS_EX_DLGMODALFRAME | WS_EX_WINDOWEDGE);
				fvi_dlg.SetWindowLongPtr(GWL_EXSTYLE, lStyles);
			}
			if (eFvCustomizeType::eBorder == eCustomizationType)
			{
				CWindow prev_parent = fvi_dlg.SetParent(owner_ref);
				if (!prev_parent)
				{
					DWORD dwError = ::GetLastError();
					dwError = dwError;
				}

				RECT rc_owner = {0};
				owner_ref.GetClientRect(&rc_owner);

				RECT rc_ = {0};
				::SetRect(
					&rc_,
					-12,
					-15,
					rc_owner.right,
					rc_owner.bottom
				);
				fvi_dlg.SetWindowPos(NULL, &rc_, SWP_NOZORDER | SWP_NOACTIVATE | SWP_FRAMECHANGED);
			}
			if (eFvCustomizeType::eHide == eCustomizationType)
			{
				fvi_dlg.SetWindowPos(NULL, 0, 0, 0, 0, SWP_NOMOVE|SWP_NOSIZE|SWP_NOACTIVATE|SWP_HIDEWINDOW);
				fvi_dlg.SetParent(act_ref.GetHiddenScreen());
			}

			CWindow parent_ = owner_ref.GetTopLevelWindow();

			if (parent_)
				::SetForegroundWindow(parent_);
			hr_ = S_OK;
		}

		act_ref.MarkCompleted();
		act_ref.Result(hr_);
		act_ref.Event().Fire();
		return 0;
	}
}}}}

////////////////////////////////////////////////////////////////////////////

VOID    CFvCustomizeAction::CHiddenScreen::Create(const HWND)
{
	if (m_hidden.IsWindow() == FALSE)
		m_hidden.Create(HWND_MESSAGE);
}

VOID    CFvCustomizeAction::CHiddenScreen::Destroy(void)
{
	if (m_hidden.IsWindow())
		m_hidden.SendMessage(WM_CLOSE);
}

CWindow&CFvCustomizeAction::CHiddenScreen::GetWindow_Ref(void)
{
	return m_hidden;
}

////////////////////////////////////////////////////////////////////////////

CFvCustomizeAction::CFvCustomizeAction(CSharedObjects& shared_ref, IGenericEventNotify& snk_ref, ::ATL::CWindow& owner_ref):
	CActionBase(details::ThreadFunction_CustomizeFvDialog, snk_ref, IDD_PAYROLL_TC_PAGE_SCAN_FV_CUSTOM, shared_ref), m_owner_ref(owner_ref)
{
	m_screen.Create(owner_ref);
}

CFvCustomizeAction::~CFvCustomizeAction(void)
{
	m_screen.Destroy();
}

////////////////////////////////////////////////////////////////////////////

HRESULT   CFvCustomizeAction::Stop(const bool bForced)
{
	if (CActionBase::IsStopped())
		return S_OK;
	this->Interrupted(true);
	HRESULT hr_ = CActionBase::Stop(bForced);
	this->Interrupted(false);
	return  hr_;
}

////////////////////////////////////////////////////////////////////////////

CWindow&  CFvCustomizeAction::GetOwnerRef(void)const
{
	return m_owner_ref;
}

CWindow&  CFvCustomizeAction::GetHiddenScreen(void)
{
	return m_screen.GetWindow_Ref();
}