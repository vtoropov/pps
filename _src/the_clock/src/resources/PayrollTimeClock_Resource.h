//{{NO_DEPENDENCIES}}
// Microsoft Visual C++ generated include file.
// Used by PayrollTimeClock_Resource.rc
//
// Created by Tech_dog (VToropov) on 8-Apr-2014 at 4:18:25pm, GMT+4, Saint-Petersburg, Tuesday;
// This is Payroll Time Clock application resource declaration file.
//
#define IDD_PAYROLL_TC_DLG_MAIN                           101
#define IDR_PAYROLL_TC_DLG_ICON                           103
#define IDR_PAYROLL_TC_DLG_HEADER                         105
#define IDC_PAYROLL_TC_DLG_CODE                           107
#define IDC_PAYROLL_TC_DLG_NAME                           109
#define IDC_PAYROLL_TC_DLG_AREA                           111
#define IDC_BITMPAP                                       113
#define IDC_PAYROLL_TC_DLG_IMAGE                          IDC_BITMPAP
#define IDR_PAYROLL_TC_DLG_IMAGE                          115
#define IDS_PAYROLL_TC_END_PROCESS                        117
#define IDR_PAYROLL_TC_SND_SUCCESS                        119
#define IDS_PAYROLL_TC_NOT_SUCCESS                        121
#define IDS_PAYROLL_TC_REC_SUCCESS                        123
#define IDS_PAYROLL_TC_NOT_WA_CHOSEN                      125

////////////////////////////////////////////////////////////////////////////
//
// executable module related messages
//
////////////////////////////////////////////////////////////////////////////

#define IDS_PAYROLL_EXE_MULTI_OBJ_ERR                    1001
#define IDS_PAYROLL_EXE_NO_REG_IMG_ERR                   1003
#define IDS_PAYROLL_EXE_APP_INIT_ERR                     1005

////////////////////////////////////////////////////////////////////////////
//
// full screen main window related resources
//
////////////////////////////////////////////////////////////////////////////

#define IDR_PAYROLL_FS_WND_BKGND_TILE                    2001
#define IDS_PAYROLL_FS_WND_TITLE_TEXT                    2003
#define IDR_PAYROLL_FS_WND_HEADER_IMG                    2005

////////////////////////////////////////////////////////////////////////////
//
// error page related resources
//
////////////////////////////////////////////////////////////////////////////

#define IDD_PAYROLL_TC_PAGE_ERR                          3001
#define IDC_PAYROLL_TC_PAGE_ERR_RETRY                    3003
#define IDR_PAYROLL_FS_WND_PAGE_E_MSG                    3005
#define IDR_PAYROLL_FS_WND_PAGE_E_IMG                    3007
#define IDR_PAYROLL_FS_WND_PAGE_E_RTY_N                  3009
#define IDR_PAYROLL_FS_WND_PAGE_E_RTY_H                  3011
#define IDR_PAYROLL_FS_WND_PAGE_E_RTY_P                  3013
#define IDC_PAYROLL_FS_WND_PAGE_E_RTY                    3013

////////////////////////////////////////////////////////////////////////////
//
// finger scan page related resources
//
////////////////////////////////////////////////////////////////////////////

#define IDD_PAYROLL_TC_PAGE_SCAN                         4001
#define IDC_PAYROLL_TC_PAGE_SCAN_START                   4003
#define IDC_PAYROLL_TC_PAGE_SCAN_PROCESS                 4005
#define IDC_PAYROLL_TC_PAGE_SCAN_OK                      4007
#define IDC_PAYROLL_TC_PAGE_SCAN_ERR                     4009
#define IDR_PAYROLL_FS_WND_PAGE_0_FVD_FRM                4011
#define IDR_PAYROLL_FS_WND_PAGE_0_FVD_MSG_0              4013
#define IDR_PAYROLL_FS_WND_PAGE_0_FVD_MSG_1              4015
#define IDR_PAYROLL_FS_WND_PAGE_0_FVD_MSG_2              4016
#define IDR_PAYROLL_FS_WND_PAGE_0_FVD_MSG_3              4017
#define IDD_PAYROLL_TC_PAGE_SCAN_FV_CUSTOM               4019

////////////////////////////////////////////////////////////////////////////
//
// work area page related resources
//
////////////////////////////////////////////////////////////////////////////

#define IDD_PAYROLL_TC_PAGE_WA                           5001
#define IDC_PAYROLL_TC_PAGE_WA_LIST                      5003
#define IDR_PAYROLL_FS_WND_PAGE_1_WA_MSG_0               5005
#define IDR_PAYROLL_FS_WND_PAGE_1_WA_CFM_N               5007
#define IDR_PAYROLL_FS_WND_PAGE_1_WA_CFM_D               5009
#define IDR_PAYROLL_FS_WND_PAGE_1_WA_CFM_P               5011
#define IDR_PAYROLL_FS_WND_PAGE_1_WA_CFM_H               5013
#define IDC_PAYROLL_FS_WND_PAGE_1_WA_CFM                 5013
#define IDC_PAYROLL_FS_WND_PAGE_1_TIMEOUT                5015
#define IDR_PAYROLL_FS_WND_PAGE_1_WA_OUT_N               5017
#define IDR_PAYROLL_FS_WND_PAGE_1_WA_OUT_H               5019
#define IDR_PAYROLL_FS_WND_PAGE_1_WA_OUT_P               5021
#define IDC_PAYROLL_FS_WND_PAGE_1_WA_OUT                 5023

////////////////////////////////////////////////////////////////////////////
//
// finish page related resources
//
////////////////////////////////////////////////////////////////////////////

#define IDD_PAYROLL_TC_PAGE_FINISH                       6001
#define IDR_PAYROLL_FS_WND_PAGE_2_RS_MSG_0               6003
#define IDC_PAYROLL_FS_WND_PAGE_2_FINISHED               6005

////////////////////////////////////////////////////////////////////////////
//
// data saving error page related resources
//
////////////////////////////////////////////////////////////////////////////

#define IDD_PAYROLL_TC_PAGE_SAVE_ERR                     7001
#define IDR_PAYROLL_FS_WND_PAGE_E_SV_MSG_0               7003
#define IDC_PAYROLL_FS_WND_PAGE_E_SV_AGAIN               7005
#define IDR_PAYROLL_FS_WND_PAGE_E_SV_RTY_N               7007
#define IDR_PAYROLL_FS_WND_PAGE_E_SV_RTY_H               7009
#define IDR_PAYROLL_FS_WND_PAGE_E_SV_RTY_P               7011

////////////////////////////////////////////////////////////////////////////
//
// password page related resources
//
////////////////////////////////////////////////////////////////////////////

#define IDD_PAYROLL_TC_PAGE_PASSWORD                     8001
#define IDC_PAYROLL_FS_WND_EXIT                          8003
#define IDC_PAYROLL_FS_WND_PAGE_3_SHOW                   8004
#define IDR_PAYROLL_FS_WND_PAGE_3_MSG_0                  8005
#define IDR_PAYROLL_FS_WND_PAGE_3_CLS_N                  8007
#define IDR_PAYROLL_FS_WND_PAGE_3_CLS_H                  8009
#define IDR_PAYROLL_FS_WND_PAGE_3_CLS_P                  8011
#define IDC_PAYROLL_FS_WND_PAGE_3_PWD                    8013
#define IDR_PAYROLL_FS_WND_PAGE_3_MSG_1                  8015
#define IDR_PAYROLL_FS_WND_PAGE_3_RET_N                  8017
#define IDR_PAYROLL_FS_WND_PAGE_3_RET_H                  8019
#define IDR_PAYROLL_FS_WND_PAGE_3_RET_P                  8021
#define IDC_PAYROLL_FS_WND_PAGE_3_RET                    8023

