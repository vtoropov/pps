/*
	Created by Tech_dog (VToropov) on 8-Apr-2014 at 4:37:13pm, GMT+4, Saint-Petersburg, Tuesday;
	This is Payroll Time Clock Application Common Settings class implementation file.
*/
#include "StdAfx.h"
#include "PayrollTimeClock_CommonSettings.h"
#include "Shared_GenericAppObject.h"
#include "Shared_PersistentStorage.h"

using namespace Platinum;
using namespace Platinum::client;
using namespace Platinum::client::common;

using namespace shared::lite::common;
using namespace shared::lite::persistent;

////////////////////////////////////////////////////////////////////////////

namespace Payroll { namespace time_clock { namespace common { namespace details
{
	CAtlString CommonSettings_GetAbsolutePath(LPCTSTR pszPath)
	{
		CAtlString cs_path;
		if (CApplication::IsRelatedToAppFolder(pszPath))
			global::GetAppObjectRef().GetPathFromAppFolder(pszPath, cs_path);
		else
			cs_path = pszPath;
		return cs_path;
	}
}}}}

////////////////////////////////////////////////////////////////////////////

namespace Payroll { namespace time_clock { namespace common
{
	CFontInfo::CFontInfo(void) : m_size(0)
	{
	}

	CFontInfo::~CFontInfo(void)
	{
	}

	LPCTSTR               CFontInfo::Family(void)const
	{
		return m_family;
	}

	HRESULT               CFontInfo::Family(LPCTSTR pszName)
	{
		m_family = pszName;
		return S_OK;
	}

	DWORD                 CFontInfo::Size  (void)const
	{
		return m_size;
	}

	HRESULT               CFontInfo::Size  (const DWORD dwPoints)
	{
		m_size = dwPoints;
		return S_OK;
	}
}}}

////////////////////////////////////////////////////////////////////////////

namespace Payroll { namespace time_clock { namespace common
{
	eFvCustomizeType::_e  CCommonSettings::FvCustomization(void)const
	{
		static eFvCustomizeType::_e eResult = eFvCustomizeType::eNone;
		static bool bInitialized = false;
		
		if (bInitialized)
			return eResult;

		bInitialized = true;
		::ATL::CAtlString cs_value = TBase::m_profile.GetStringValue(_T("Common"), _T("CustomizeFvDialog"), NULL);

		if (!cs_value.IsEmpty())
		{
			const LONG n_val =  ::_tstol(cs_value.GetString()); 
			switch (n_val)
			{
			case 1: eResult = eFvCustomizeType::eBorder;   break;
			case 2: eResult = eFvCustomizeType::eHide;     break;
			case 4: eResult = eFvCustomizeType::eMinimize; break;
			}
		}
		return eResult;
	}

	LPCTSTR               CCommonSettings::GetAlwaysCostWA(void)const
	{
		static CAtlString cs_wa;
		static bool bInitialized = false;
		if (bInitialized)
			return cs_wa;
		bInitialized = true;
		cs_wa = TBase::m_profile.GetStringValue(_T("Common"), _T("AlwaysCostWorkArea"), NULL);
		return cs_wa;
	}

	LPCTSTR               CCommonSettings::GetBkgndFilePath(void)const
	{
		static CAtlString cs_path;
		static bool bInitialized = false;
		if (bInitialized)
			return cs_path;
		bInitialized = true;
		cs_path = TBase::m_profile.GetStringValue(_T("UI"), _T("BkgndFilePath"), NULL);
		cs_path = details::CommonSettings_GetAbsolutePath(cs_path);
		return cs_path;
	}

	COLORREF              CCommonSettings::GetEmpForeColor(void)const
	{
		static COLORREF clrFore = ::GetSysColor(COLOR_WINDOWTEXT);
		static bool bInitialized = false;
		if (bInitialized)
			return clrFore;
		bInitialized = true;
		::ATL::CAtlString cs_rgb = TBase::m_profile.GetStringValue(_T("UI"), _T("ClockingTimeColor"), NULL);
		if (!cs_rgb.IsEmpty())
		{
			DWORD dwColor[3] = {0};
			INT n_pos = 0;
			INT n_ptr = 0;
			::ATL::CAtlString cs_clr = cs_rgb.Tokenize(_T(","), n_pos);

			while(!cs_clr.IsEmpty())
			{
				if (n_ptr > _countof(dwColor) - 1)
					break;
				dwColor[n_ptr] = static_cast<DWORD>(::_tstol(cs_clr.GetString()));
				n_ptr++;
				cs_clr = cs_rgb.Tokenize(_T(","), n_pos);
			}
			clrFore = RGB(dwColor[0], dwColor[1], dwColor[2]);
		}
		return clrFore;
	}

	LPCTSTR               CCommonSettings::GetFailureSoundFile(void) const
	{
		static ::ATL::CAtlString cs_path;
		static bool bInitialized = false;
		if (bInitialized)
			return cs_path;
		bInitialized = true;
		cs_path = TBase::m_profile.GetStringValue(_T("Common"), _T("SoundOnFailure"), NULL);
		cs_path = details::CommonSettings_GetAbsolutePath(cs_path);
		return cs_path;
	}

	LPCTSTR               CCommonSettings::GetLogoutWA(void)const
	{
		static CAtlString cs_out_wa;
		static bool bInitialized = false;
		if (bInitialized)
			return cs_out_wa;
		bInitialized = true;
		cs_out_wa = TBase::m_profile.GetStringValue(_T("Common"), _T("WALogoutDefault"), _T("0000"));
		return cs_out_wa;
	}

	::ATL::CAtlString     CCommonSettings::GetPassword(void)const
	{
		static CAtlString cs_result;
		static bool bInitialized = false;
		if (bInitialized)
			return cs_result;
		bInitialized = true;
		cs_result = TBase::m_profile.GetStringValue(_T("Common"), _T("PasswordToClose"), NULL);
		return cs_result;
	}

	DWORD                 CCommonSettings::GetSelectWAtimout(void)const
	{
		static DWORD dwTimeout = 5000;
		static bool bInitialized = false;
		if (bInitialized)
			return dwTimeout;
		bInitialized = true;
		dwTimeout = TBase::m_profile.GetDwordValue(_T("Common"), _T("WASelectTimeout"), 5000);
		if (dwTimeout < 5000)
			dwTimeout = 5000;

		return dwTimeout;
	}

	LPCTSTR               CCommonSettings::GetSuccessSoundFile(void)const
	{
		static ::ATL::CAtlString cs_path;
		static bool bInitialized = false;
		if (bInitialized)
			return cs_path;
		bInitialized = true;
		cs_path = TBase::m_profile.GetStringValue(_T("Common"), _T("SoundOnSuccess"), NULL);
		cs_path = details::CommonSettings_GetAbsolutePath(cs_path);
		return cs_path;
	}

	const CFontInfo&      CCommonSettings::GetWAfontInfo(void)const
	{
		static CFontInfo fnt_info;
		static bool bInitialized = false;
		if (!bInitialized)
		{
			bInitialized = true;
			DWORD dwSize = TBase::m_profile.GetDwordValue(_T("UI"), _T("WAListFontSize"), 32);
			if (dwSize <  8) dwSize =  8;
			if (dwSize > 96) dwSize = 96;
			fnt_info.Size(dwSize);
		}
		return fnt_info;
	}

	DWORD                 CCommonSettings::GetWAlistWidth(void)const
	{
		static DWORD dwWidth = 0;
		static bool bInitialized = false;
		if (!bInitialized)
		{
			bInitialized = true;
			::ATL::CAtlString cs_value = TBase::m_profile.GetStringValue(_T("UI"), _T("WAListWidth"), NULL);
			if (!cs_value.IsEmpty())
			{
				if (0 == cs_value.CompareNoCase(_T("auto")))
					dwWidth = (DWORD)-1;
				else
					dwWidth = static_cast<DWORD>(::_tstol(cs_value.GetString()));
			}
		}
		return dwWidth;
	}

	bool                  CCommonSettings::HasBkgndFilePath(void)const
	{
		::ATL::CAtlString cs_path = this->GetBkgndFilePath();
		return (!cs_path.IsEmpty());
	}

	bool                  CCommonSettings::HasPassword(void)const
	{
		CAtlString cs_result = this->GetPassword();
		return !cs_result.IsEmpty();
	}

	bool                  CCommonSettings::IsBannerVisible(void)const
	{
		static bool bResult = true;
		static bool bInitialized = false;
		if (bInitialized)
			return bResult;
		bInitialized = true;
		bResult = TBase::m_profile.GetBoolValue(_T("UI"), _T("IsBannerVisible"), bResult);
		return bResult;
	}

	bool                  CCommonSettings::IsFVStatusVisible(void)const
	{
		static bool bResult = false;
		static bool bInitialized = false;
		if (bInitialized)
			return bResult;
		bInitialized = true;
		bResult = TBase::m_profile.GetBoolValue(_T("UI"), _T("IsFVStatusVisible"), bResult);
		return bResult;
	}

	bool                  CCommonSettings::IsLandscape (void)const
	{
		static bool bResult = false;
		static bool bInitialized = false;
		if (bInitialized)
			return bResult;
		bInitialized = true;
		bResult = TBase::m_profile.GetBoolValue(_T("UI"), _T("UseLandscape"), bResult);
		return bResult;
	}

	bool                  CCommonSettings::IsWorkAreaPromptMode(void)const
	{
		static bool bResult = true;
		static bool bInitialized = false;
		if (bInitialized)
			return  bResult;

		bInitialized = true;
		bResult = !TBase::m_profile.GetBoolValue(_T("Common"), _T("NoWorkAreaPrompt"), !bResult);

		return bResult;
	}

	bool                  CCommonSettings::RunMaximized(void)const
	{
		static bool bResult = false;
		static bool bInitialized = false;
		if (bInitialized)
			return bResult;
		bInitialized = true;
		bResult = TBase::m_profile.GetBoolValue(_T("Common"), _T("RunMaximized"), bResult);
		return bResult;
	}
}}}