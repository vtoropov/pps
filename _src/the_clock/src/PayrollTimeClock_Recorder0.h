#ifndef __PAYROLLTIMECLOCKWNDLESSRECORDER_H_C0A452A7_1D28_4022_A60A_DDD90986174C_INCLUDED
#define __PAYROLLTIMECLOCKWNDLESSRECORDER_H_C0A452A7_1D28_4022_A60A_DDD90986174C_INCLUDED
/*
	Created by Tech_dog (VToropov) on 28-Mar-2015 at 2:12:28pm, GMT+9, Thailand Phuket, Saturday;
	This is Platinum Payroll System Time Clock Windowless Recorder class declaration file.
*/
#include "PayrollTimeClock_RecorderBase.h"
#include "Shared_GenericEvent.h"

namespace Payroll { namespace time_clock
{
	using Payroll::time_clock::common::CSharedObjects;
	using shared::lite::events::IGenericEventNotify;

	class CRecorder0 : public CRecorderBase, public IGenericEventNotify
	{
		typedef CRecorderBase TRecorderBase;
	private:
		class CMessageHandler:
			public ::ATL::CWindowImpl<CMessageHandler>
		{
		private:
			IGenericEventNotify&     m_sink;
			UINT_PTR                 m_evt_timer;
		public:
			BEGIN_MSG_MAP(CMessageHandler)
				MESSAGE_HANDLER(WM_TIMER,  OnTimer)
			END_MSG_MAP()
		public:
			CMessageHandler(IGenericEventNotify&);
			~CMessageHandler(void);
		private:
			LRESULT     OnTimer(UINT, LPARAM, WPARAM, BOOL&);
		public:
			HRESULT    _Create(const DWORD dwPeriod);
			HRESULT    _Destroy(void);
		};
	private:
		CMessageHandler m_timer;
	public:
		CRecorder0(CSharedObjects&);
		~CRecorder0(void);
	public:
		HRESULT         Record(void) override sealed;
	private:
		HRESULT         GenericEvent_OnNotify(const UINT eventId) override sealed;
	};
}}

#endif/*__PAYROLLTIMECLOCKWNDLESSRECORDER_H_C0A452A7_1D28_4022_A60A_DDD90986174C_INCLUDED*/