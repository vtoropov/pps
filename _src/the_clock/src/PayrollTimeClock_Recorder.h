#ifndef __PAYROLLTIMECLOCKRECORDER_H_D47D04E4_AF7D_4d2d_83CE_440AFA482722_INCLUDED
#define __PAYROLLTIMECLOCKRECORDER_H_D47D04E4_AF7D_4d2d_83CE_440AFA482722_INCLUDED
/*
	Created by Tech_dog (VToropov) on 9-Apr-2014 at 6:01:09pm, GMT+4, Saint-Petersburg, Wednesday;
	This is Platinum Payroll System Time Clock Recorder class declaration file.
*/
#include "PayrollTimeClock_RecorderBase.h"

namespace Payroll { namespace time_clock
{
	using Payroll::time_clock::common::CSharedObjects;

	class CRecorder : public CRecorderBase
	{
		typedef CRecorderBase TRecorderBase;
	public:
		CRecorder(CSharedObjects&);
		~CRecorder(void);
	public:
		HRESULT         Record(void) override sealed;
	};
}}

#endif/*__PAYROLLTIMECLOCKRECORDER_H_D47D04E4_AF7D_4d2d_83CE_440AFA482722_INCLUDED*/