#ifndef __PAYROLLTIMECLOCKRECORDERBASE_H_094940DF_E238_4610_A67D_5C9867479CCB_INCLUDED
#define __PAYROLLTIMECLOCKRECORDERBASE_H_094940DF_E238_4610_A67D_5C9867479CCB_INCLUDED
/*
	Created by Tech_dog (VToropov) on 5-Feb-2015 at 2:10:10am, GMT+3, Taganrog, Thursday;
	This is Platinum Payroll System Time Clock Recorder Base class declaration file.
*/
#include "PayrollTimeClock_SharedObjects.h"
#include "PayrollTimeClock_ActionBase.h"
#include "PPS_EmployeeDataRecord.h"

namespace Payroll { namespace time_clock
{
	using Payroll::time_clock::common::CSharedObjects;
	using Payroll::time_clock::ctrl_flow::ICanContinue;

	using Platinum::client::data::CEmployeeDataRecord;
	using Platinum::client::data::CEmployeeDataRecordEx;

	class CProgressCallback : public ICanContinue
	{
	public:
		virtual bool          ICanContinue_CheckState(void) override sealed { return true; }
	public:
		operator ICanContinue&(void) { return *this; }
	};

	class CRecorderBase
	{
	protected:
		bool                  m_bInitialized;
		CSharedObjects&       m_shared;
		CEmployeeDataRecordEx m_cached;
		::ATL::CAtlString     m_last_time;
	protected:
		CRecorderBase(CSharedObjects&);
		virtual ~CRecorderBase(void);
	public:
		virtual HRESULT Record(void) PURE;
	public:
		const
		CEmployeeDataRecordEx&Cached(void)const;
		CEmployeeDataRecordEx&Cached(void);
		LPCTSTR               GetLastSaveTime(void)const;
		HRESULT               Initialize(void);
		bool                  IsInitialized(void)const;
		HRESULT               Reinitialize(void);
	protected:
		HRESULT               Save(CEmployeeDataRecordEx& _result);
		HRESULT               Verify(const _variant_t& vProcessed, CEmployeeDataRecordEx& _result, ICanContinue& = CProgressCallback());
	public:
		HRESULT               Save(void);
		HRESULT               Verify(const _variant_t& vProcessed, ICanContinue&);
	};
}}

#endif/*__PAYROLLTIMECLOCKRECORDERBASE_H_094940DF_E238_4610_A67D_5C9867479CCB_INCLUDED*/