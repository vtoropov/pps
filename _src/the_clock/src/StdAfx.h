#ifndef __PAYROLLTIMECLOCKAPPLICATIONPRECOMPILEDHEADER_H_5EE3ED4B_E46C_474a_80F6_791FB2417E42_INCLUDED
#define __PAYROLLTIMECLOCKAPPLICATIONPRECOMPILEDHEADER_H_5EE3ED4B_E46C_474a_80F6_791FB2417E42_INCLUDED
/*
	Created by Tech_dog (VToropov) on 8-Apr-2014 at 3:58:32pm, GMT+4, Saint-Petersburg, Tuesday;
	This is Platinum Payroll Time Clock application precompiled headers definition file.
*/
#include "PayrollTimeClock_TargetVersion.h"

#pragma warning(disable: 4481)  // nonstandard extension used: override specifier 'override'
#pragma warning(disable: 4996)  // security warning: function or variable may be unsafe

#include <atlbase.h>
#include <atlstr.h>             // important order, this file must be incloded before any includes of the WTL headers
#include <atlapp.h>

extern CAppModule _Module;

#include <atlwin.h>
#include <atlframe.h>
#include <atlctrls.h>
#include <atldlgs.h>
#include <atlcrack.h>
#include <atlsafe.h>
#include <atltheme.h>

#include <comutil.h>

#include "Shared_EventLogger.h"
#include <map>
#include <vector>
#include <typeinfo.h>

#include <MMSystem.h>
////////////////////////////////////////////////////////////////////////////
//
// application global objects
//
////////////////////////////////////////////////////////////////////////////
#include "Shared_GenericAppObject.h"
#include "PayrollTimeClock_CommonSettings.h"

typedef shared::lite::common::CApplication          TApplication;
typedef Platinum::client::common::CCommonSettings   TCommonSettings; 

namespace global 
{
	using Payroll::time_clock::common::CCommonSettings;

	TApplication&    GetAppObjectRef(void);
	CCommonSettings& GetSettingsRef(void);
}

TApplication&     Global_GetAppObjectRef(void);
TCommonSettings&  Global_GetSettingsRef(void);

#pragma comment(lib, "Winmm.lib")

#if defined WIN64
  #pragma comment(linker, "/manifestdependency:\"type='win32' name='Microsoft.Windows.Common-Controls' version='6.0.0.0' processorArchitecture='amd64' publicKeyToken='6595b64144ccf1df' language='*'\"")
#elif defined WIN32
  #pragma comment(linker, "/manifestdependency:\"type='win32' name='Microsoft.Windows.Common-Controls' version='6.0.0.0' processorArchitecture='x86' publicKeyToken='6595b64144ccf1df' language='*'\"")
#elif defined _M_IA64
  #pragma comment(linker, "/manifestdependency:\"type='win32' name='Microsoft.Windows.Common-Controls' version='6.0.0.0' processorArchitecture='ia64' publicKeyToken='6595b64144ccf1df' language='*'\"")
#else
  #pragma comment(linker, "/manifestdependency:\"type='win32' name='Microsoft.Windows.Common-Controls' version='6.0.0.0' processorArchitecture='*' publicKeyToken='6595b64144ccf1df' language='*'\"")
#endif

#if defined(_DEBUG)
  #pragma comment(lib, "UIX_Draw_V9D.lib")
  #pragma comment(lib, "UIX_Ctrl_V9D.lib")
  #pragma comment(lib, "UIX_Frame_V9D.lib")
#else
  #pragma comment(lib, "UIX_Draw_V9.lib")
  #pragma comment(lib, "UIX_Ctrl_V9.lib")
  #pragma comment(lib, "UIX_Frame_V9.lib")
#endif

#endif/*__PAYROLLTIMECLOCKAPPLICATIONPRECOMPILEDHEADER_H_5EE3ED4B_E46C_474a_80F6_791FB2417E42_INCLUDED*/