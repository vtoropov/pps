/*
	Created by Tech_dog (VToropov) on 8-Apr-2014 at 5:27:33pm, GMT+4, Saint-Petersburg, Tuesday;
	This is Payroll Time Clock Application Entry Point implementation file.
*/
#include "StdAfx.h"
#include "Shared_SystemCore.h"
#include "UIX_GdiProvider.h"
#include "PayrollTimeClock_SharedObjects.h"
#include "PayrollTimeClock_Recorder.h"
#include "PayrollTimeClock_Recorder0.h"
#include "PayrollTimeClock_Recorder2.h"
#include "PayrollTimeClock_Resource.h"

CAppModule _Module;

using namespace Payroll;
using namespace Payroll::time_clock;
using namespace Payroll::time_clock::common;

using namespace Platinum;

using namespace shared::lite;
using namespace shared::lite::common;

class CSysHookFunctor
{
private:
	HINSTANCE    m_hInstance;
public:
	CSysHookFunctor(void) : m_hInstance(NULL)
	{
		m_hInstance = ::LoadLibrary(_T("Shared_SysHook.dll"));
		if (!m_hInstance)
			return;
	}
	~CSysHookFunctor(void)
	{
		if (m_hInstance)
		{
			this->Uninstall();
			::FreeLibrary(m_hInstance); m_hInstance = NULL;
		}
	}
public:
	VOID        Install(void)
	{
		if (m_hInstance)
		{
			typedef HRESULT (WINAPI *DLLPROC)(const BOOL);
			DLLPROC pfnInstallSysHook = reinterpret_cast<DLLPROC>(::GetProcAddress(m_hInstance, "InstallSysHook"));

			if (pfnInstallSysHook)
			{
				HRESULT hr_ = pfnInstallSysHook(TRUE);
				ATLVERIFY(SUCCEEDED(hr_));
			}
		}
	}
	VOID        Uninstall(void)
	{
		if (m_hInstance)
		{
			typedef HRESULT (WINAPI *DLLPROC)(VOID);
			DLLPROC pfnUninstallSysHook = reinterpret_cast<DLLPROC>(::GetProcAddress(m_hInstance, "UninstallSysHook"));

			if (pfnUninstallSysHook)
				pfnUninstallSysHook();
		}
	}
};

INT RunModal(VOID)
{
	TRACE_FUNC();

	CApplication& the_app = global::GetAppObjectRef();
	HRESULT hr_ = the_app.Process().RegisterSingleton(_T("Global\\PayrollTimeClock"));
	if (S_OK != hr_)
	{
		AtlMessageBox(HWND_DESKTOP, IDS_PAYROLL_EXE_MULTI_OBJ_ERR, the_app.GetName(), MB_OK|MB_ICONEXCLAMATION|MB_SETFOREGROUND);
		return 0;
	}
	CCommonSettings& settings = global::GetSettingsRef();
	Payroll::time_clock::common::CSharedObjects shared_objs(settings);

	const bool bFullScreen = settings.RunMaximized();
	if (!bFullScreen)
	{
		if (!shared_objs.IsInitialized())
		{
			::ATL::CAtlString cs_error = shared_objs.Initializer().GetLastError_Ref().GetDescription();
			AtlMessageBox(::GetActiveWindow(), cs_error.GetString(), the_app.GetName(), MB_ICONHAND|MB_OK);
			return 1;
		}

		const bool bNoPrompt = !settings.IsWorkAreaPromptMode();
		if (bNoPrompt)
		{
			CRecorder0 recorder(shared_objs);
			hr_ = recorder.Initialize();
			if (S_OK == hr_)
			{
				hr_ = recorder.Record();
			}
		}
		else
		{
			CRecorder recorder(shared_objs);
			hr_ = recorder.Initialize();
			if (S_OK == hr_)
			{
				hr_ = recorder.Record();
			}
		}
		if (OLE_E_BLANK == hr_)
		{
			AtlMessageBox(::GetActiveWindow(), IDS_PAYROLL_EXE_NO_REG_IMG_ERR, the_app.GetName(), MB_ICONEXCLAMATION|MB_OK);
		}
	}
	else
	{
		CRecorder2 recorder(shared_objs);
		hr_ = recorder.Initialize();
		if (S_OK == hr_)
		{
#if !defined(_DEBUG)
			CSysHookFunctor ll_hook;
			ll_hook.Install();
#endif
			hr_ = recorder.Record();
		}
	}

	if (FAILED(hr_))
	{
		CSysError sys_err(hr_);
		if (sys_err.HasDetails() == false)
			sys_err.SetUnknownMessage();

		::ATL::CAtlString cs_error;
		::ATL::CAtlString cs_pattern; cs_pattern.LoadString(IDS_PAYROLL_EXE_APP_INIT_ERR);

		cs_error.Format(cs_pattern, hr_, sys_err.GetDescription());
		AtlMessageBox(::GetActiveWindow(), cs_error.GetString(), the_app.GetName(), MB_ICONHAND|MB_OK);
	}
	return 0;
}

int WINAPI _tWinMain(HINSTANCE hInstance, HINSTANCE hPrevInstance, LPTSTR lpstrCmdLine, int nCmdShow)
{
	hInstance; hPrevInstance; lpstrCmdLine; nCmdShow;
	shared::lite::sys_core::CComAutoInitializer com(false);

	HRESULT hr__ = ::CoInitializeSecurity(NULL, -1, NULL, NULL,
					RPC_C_AUTHN_LEVEL_NONE, RPC_C_IMP_LEVEL_IDENTIFY, NULL, EOAC_NONE, NULL);
	ATLASSERT(SUCCEEDED(hr__));
	/*
		Tech_dog commented on 09-Feb-2010 at 12:47:50pm:
		________________________________________________
		we need to assign lib id to link ATL DLL statically,
		otherwise we get annoying fucking message "Did you forget to pass the LIBID to CComModule::Init?"
	*/
	_Module.m_libid = LIBID_ATLLib;
	INT nResult = 0;
	// this resolves ATL window thunking problem when Microsoft Layer for Unicode (MSLU) is used
	::DefWindowProc(NULL, 0, 0, 0L);

	AtlInitCommonControls(ICC_BAR_CLASSES);	// add flags to support other controls

	hr__ = _Module.Init(NULL, hInstance);
	if(!SUCCEEDED(hr__))
	{
		ATLASSERT(FALSE);
		return 1;
	}

	TRACE_FUNC();

	ex_ui::draw::CGdiPlusLibLoader  gdi_loader;

	nResult = RunModal();

	_Module.Term();

	return nResult;
}