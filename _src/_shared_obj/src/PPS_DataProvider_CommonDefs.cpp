/*
	Created by Tech_dog (VToropov) on 3-Apr-2014 at 10:14:08am, GMT+4, Saint-Petersburg, Thursday;
	This is Platinum Client Data Provider Common Definitions implementation file.
*/
#include "StdAfx.h"
#include "PPS_DataProvider_CommonDefs.h"
#include "PPS_CommonSettings.h"
#include "PPS_Resources.h"

using namespace Platinum::client::data;
using namespace Platinum::client::common;

#include "Shared_GenericAppObject.h"
#include "Shared_SystemError.h"

using namespace shared::lite::common;

extern CCommonSettings& Global_GetSettingsRef(void);
extern CApplication&    Global_GetAppObjectRef(void);
/////////////////////////////////////////////////////////////////////////////

namespace Platinum { namespace client { namespace data
{

	VOID       DisplayOpenDataFileError(const CAtlString& _file, const HRESULT _error)
	{
		CSysError err_obj(_error);
		CAtlString cs_error;
		cs_error.Format(
				_T("Error occurred while opening the file:\n%s%s"),
				_file,
				err_obj.GetFormattedDetails()
			);
		AtlMessageBox(
				NULL,
				cs_error.GetString(),
				Global_GetAppObjectRef().GetName(),
				MB_ICONSTOP|MB_OK|MB_SETFOREGROUND|MB_TOPMOST
			);
	}

	CAtlString GetAccessDeniedErrorMessage(LPCTSTR pszFolder, const HRESULT hError)
	{
		CSysError err_obj(hError);
		if (err_obj.HasDetails() == false)
			err_obj.SetUnknownMessage();

		ATL::CAtlString cs_error;
		cs_error.Format(
				IDS_PPS_SHARED_ACCESS_DENIED,
				pszFolder,
				hError,
				err_obj.GetDescription()
			);
		return cs_error;
	}

	HRESULT    GetBackupFolder(ATL::CAtlString& cs_folder)
	{
		cs_folder  = Global_GetSettingsRef().Storage().BackupFolder();
		if (cs_folder.IsEmpty())
			return S_FALSE;
		HRESULT hr_ = IsDataFolderExist(cs_folder, true);
		return  hr_;
	}

	HRESULT    GetStorageFolder(ATL::CAtlString& cs_storage, const bool bSuppressErrMessage)
	{
		cs_storage  = Global_GetSettingsRef().Storage().CustomFolder();
		HRESULT hr_ = IsDataFolderExist(cs_storage);
		if (S_OK != hr_)
		{
			if (!bSuppressErrMessage)
			{
				::ATL::CAtlString cs_error = GetAccessDeniedErrorMessage(cs_storage, hr_);
				AtlMessageBox(
						::GetActiveWindow(),
						cs_error.GetString(),
						Global_GetAppObjectRef().GetName(),
						MB_ICONEXCLAMATION|MB_OK
					);
			}
			cs_storage = Global_GetSettingsRef().Storage().DefaultFolder();
			hr_ = IsDataFolderExist(cs_storage);
		}
		return  hr_;
	}

	HRESULT    IsDataFolderExist(LPCTSTR pFolderPath, const bool bWithCreateOption)
	{
		if (!::PathFileExists(pFolderPath))
		{
			if (bWithCreateOption)
			{
				if (NULL == ::CreateDirectory(pFolderPath, NULL))
				{
					const DWORD dwError = ::GetLastError();
					if (ERROR_ALREADY_EXISTS != dwError)
						return HRESULT_FROM_WIN32(dwError);
				}
			}
			else
				return HRESULT_FROM_WIN32(ERROR_FILE_NOT_FOUND);
		}
		return S_OK;
	}

	HRESULT    ValidateVeinData(const _variant_t& fv_data, const bool bEmptyIsAllowed)
	{
		if (VT_EMPTY == fv_data.vt && bEmptyIsAllowed)
			return S_FALSE;
		if (0==(VT_ARRAY & fv_data.vt) || 0==(VT_UI1 & fv_data.vt))
			return DISP_E_TYPEMISMATCH;
		return S_OK;
	}
}}}

using namespace Platinum::client::data;

/////////////////////////////////////////////////////////////////////////////

CRecordSpecBase::CRecordSpecBase(void)
{
	m_range = ::std::make_pair(0, 0);
}

/////////////////////////////////////////////////////////////////////////////

CCsvFile::THeader CRecordSpecBase::CreateHeader(void)const
{
	CCsvFile::THeader header_;

	const INT count_ = this->FieldCount();
	for ( INT i_ = 0; i_ < count_; i_++ )
	{
		try
		{
			header_.push_back(this->FieldNameOf(i_));
		}
		catch(::std::bad_alloc&)
		{
			break;
		}
	}
	return header_;
}

INT               CRecordSpecBase::FieldCount(void)const
{
	return static_cast<INT>(m_fields.size());
}

CAtlString        CRecordSpecBase::FieldNameOf(const INT nIndex)const
{
	if (nIndex < 0 || nIndex > this->FieldCount() - 1)
		return CAtlString();
	else
		return m_fields[nIndex];
}

INT               CRecordSpecBase::GetHeaderLen(void)const
{
	INT len_ = 0;
	const INT count_ = this->FieldCount();
	for ( INT i_ = 0; i_ < count_; i_++ )
	{
		len_ += m_fields[i_].GetLength();
	}
	len_ += sizeof(TCHAR) * (count_ - 1); // takes into account field separator
	return len_;
}

HRESULT           CRecordSpecBase::ValidateData(const CCsvFile& _csv, const bool bShowError)
{
	const DWORD dwHeaderSize = this->GetHeaderLen();
	const DWORD dwFieldCount = static_cast<DWORD>(_csv.FieldCount());

	bool bSuccess = true;
	// (1) checks the acceptable range of header field count
	if (dwFieldCount < m_range.first || dwFieldCount > m_range.second)
		bSuccess = false;
	else
	{
		if (_csv.RowCount() < 1                        // (2) checks row count;
		    && (_csv.SourceFileSize() > dwHeaderSize)) // (3) checks data that exceeds the header size
			bSuccess = false;
	}

	if (!bSuccess)
	{
		if (bShowError)
		{
			CAtlString cs_err;
			cs_err.Format(
					_T("Data file:\n%s\nhas invalid data or incorrect code page."),
					_csv.SourceFilePath()
				);
			AtlMessageBox(
					NULL,
					cs_err.GetString(),
					Global_GetAppObjectRef().GetName(),
					MB_ICONSTOP|MB_OK|MB_SETFOREGROUND|MB_TOPMOST
				);
		}
		return HRESULT_FROM_WIN32(ERROR_INVALID_DATA);
	}
	else
		return S_OK;
}