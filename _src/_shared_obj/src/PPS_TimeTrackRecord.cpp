/*
	Created by Tech_dog (VToropov) on 27-Mar-2014 at 2:37:11am, GMT+4, Saint-Petersburg, Thursday;
	This is Platinum Client Time Tracking Data Record class implementation file.
*/
#include "StdAfx.h"
#include "PPS_TimeTrackRecord.h"
#include "Shared_DataFormat.h"

using namespace Platinum::client::data;

/////////////////////////////////////////////////////////////////////////////

CTimeTrackRecord::CTimeTrackRecord(const bool bValid):m_bValid(bValid)
{
}

CTimeTrackRecord::~CTimeTrackRecord(void)
{
}

/////////////////////////////////////////////////////////////////////////////

LPCTSTR     CTimeTrackRecord::Code(void) const
{
	return m_code.GetString();
}

HRESULT     CTimeTrackRecord::Code(LPCTSTR pCode)
{
	if (!pCode || !::_tcslen(pCode))
	{
		if (!m_code.IsEmpty())m_code.Empty();
	}
	else
		m_code = pCode;
	return S_OK;
}

LPCTSTR     CTimeTrackRecord::Date(void) const
{
	return m_date.GetString();
}

HRESULT     CTimeTrackRecord::Date(LPCTSTR pDate)
{
	if (!pDate || !::_tcslen(pDate))
	{
		if (!m_date.IsEmpty())m_date.Empty();
	}
	else
		m_date = pDate;
	return S_OK;
}

const
CTimestamp& CTimeTrackRecord::FvTimestamp(void)const
{
	return m_timestamp;
}

CTimestamp& CTimeTrackRecord::FvTimestamp(void)
{
	return m_timestamp;
}

bool        CTimeTrackRecord::IsValid(void)const
{
	return m_bValid;
}

LPCTSTR     CTimeTrackRecord::Name(void) const
{
	return m_name.GetString();
}

HRESULT     CTimeTrackRecord::Name(LPCTSTR pszName)
{
	if (!pszName || !::_tcslen(pszName))
	{
		if (!m_name.IsEmpty())m_name.Empty();
	}
	else
		m_name = pszName;
	return S_OK;
}

LPCTSTR     CTimeTrackRecord::Time(void) const
{
	return m_time.GetString();
}

HRESULT     CTimeTrackRecord::Time(LPCTSTR pTime)
{
	if (!pTime || !::_tcslen(pTime))
	{
		if (!m_time.IsEmpty())m_time.Empty();
	}
	else
		m_time = pTime;
	return S_OK;
}

LPCTSTR     CTimeTrackRecord::WorkArea(void)const
{
	return m_area.GetString();
}

HRESULT     CTimeTrackRecord::WorkArea(LPCTSTR pArea)
{
	if (!pArea || !::_tcslen(pArea))
	{
		if (!m_area.IsEmpty())m_area.Empty();
	}
	else
		m_area = pArea;
	return S_OK;
}

/////////////////////////////////////////////////////////////////////////////

bool CTimeTrackRecord::operator!=(const CTimeTrackRecord& rh_ref) const
{
	if (this->m_code != rh_ref.m_code)
		return true;
	if (this->m_date != rh_ref.m_date)
		return true;
	if (this->m_area != rh_ref.m_area)
		return true;
	if (this->m_time != rh_ref.m_time)
		return true;
	return false;
}

CTimeTrackRecord& CTimeTrackRecord::operator= (const CEmployeeDataRecord& rec_ref)
{
	shared::lite::format::CDateTimeFormat fmt;

	SYSTEMTIME st = {0};
	::GetLocalTime(&st);

	this->Code(rec_ref.Code());
	{
		static LPCTSTR pPattern = _T("DD/MM/YYYY");
		this->Date(fmt.ToDate(st, pPattern));
	}
	{
		static LPCTSTR pPattern = _T("hh:mm:ss");
		this->Time(fmt.ToTime(st, pPattern));
	}
	this->WorkArea(rec_ref.WorkArea());
	this->FvTimestamp().Value(0);
	return *this;
}

CTimeTrackRecord& CTimeTrackRecord::operator= (const CEmployeeDataRecordEx& rec_ref)
{
	*this = (CEmployeeDataRecord&)rec_ref;

	const INT nSelected = rec_ref.SelectedFvIndex();

	if (nSelected > -1)
		this->FvTimestamp() = rec_ref.FvData().Image(nSelected).Timestamp();
	else
		this->FvTimestamp().Value(0);
	return *this;
}

/////////////////////////////////////////////////////////////////////////////

CTimeTrackRecord_ValidateRule::CTimeTrackRecord_ValidateRule(const CTimeTrackRecord& rec_ref):m_rec_ref(rec_ref)
{
}

CTimeTrackRecord_ValidateRule::~CTimeTrackRecord_ValidateRule(void)
{
}

/////////////////////////////////////////////////////////////////////////////

LPCTSTR     CTimeTrackRecord_ValidateRule::Details(void)const
{
	return m_buffer.GetString();
}

HRESULT     CTimeTrackRecord_ValidateRule::Validate(void)const
{
	if (!m_buffer.IsEmpty())m_buffer.Empty();
	LPCTSTR pCode = m_rec_ref.Code();
	if (!pCode || !::_tcslen(pCode))
	{
		m_buffer = _T("Employee code is empty");
		return S_FALSE;
	}
	LPCTSTR pDate = m_rec_ref.Date();
	if (!pDate || !::_tcslen(pDate))
	{
		m_buffer = _T("The recording date is empty");
		return S_FALSE;
	}
	LPCTSTR pTime = m_rec_ref.Time();
	if (!pTime || !::_tcslen(pTime))
	{
		m_buffer = _T("The recording time is empty");
		return S_FALSE;
	}
	LPCTSTR pWorkArea = m_rec_ref.WorkArea();
	if (!pWorkArea || !::_tcslen(pWorkArea))
	{
		m_buffer = _T("Work area is not selected");
		return S_FALSE;
	}
	return S_OK;
}