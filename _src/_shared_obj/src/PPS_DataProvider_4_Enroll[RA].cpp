/*
	Created by Tech_dog (VToropov) on 9-Apr-2014 at 6:50:07pm, GMT+4, Saint-Petersburg, Wednesday;
	This is Platinum Client Enrollment Data Provider [Read Only Access] class implementation file.
*/
#include "StdAfx.h"
#include "PPS_DataProvider_4_Enroll[RA].h"
#include "PPS_DataProvider_CommonDefs.h"
#include "PPS_DataProvider_4_FvData.h"

using namespace Platinum::client::data;

#include "Shared_PersistentStorage.h"
#include "Shared_GenericAppObject.h"
#include "Shared_RawData.h"

using namespace shared::lite::data;
using namespace shared::lite::persistent;
using namespace shared::lite::common;

extern CApplication&    Global_GetAppObjectRef(void);
/////////////////////////////////////////////////////////////////////////////

CEnrollRecordSpec::CEnrollRecordSpec(void)
{
	const INT nFields = CEnrollRecordSpec::nFieldCount;
	for ( INT i_ = 0; i_ < nFields; i_++)
	{
		try
		{
			switch (i_)
			{
			case CEnrollRecordSpec::eCode     : TBase::m_fields.push_back(::ATL::CAtlString(_T("[Emp_Code]"))); break;
			case CEnrollRecordSpec::eEmployee : TBase::m_fields.push_back(::ATL::CAtlString(_T("[Emp_Name]"))); break;
			case CEnrollRecordSpec::eWorkArea : TBase::m_fields.push_back(::ATL::CAtlString(_T("[Emp_WA]")));   break;
			}
		} catch (::std::bad_alloc&){ break;}
	}
	m_range = ::std::make_pair(
				CEnrollRecordSpec::nFieldCount - 0,
				CEnrollRecordSpec::nFieldCount - 0
			);
}

/////////////////////////////////////////////////////////////////////////////

CEnrollDataProvider_RA::CEnrollDataProvider_RA(CSharedObjects& objs_ref) : m_objects(objs_ref)
{
}

CEnrollDataProvider_RA::~CEnrollDataProvider_RA(void)
{
}

/////////////////////////////////////////////////////////////////////////////

HRESULT          CEnrollDataProvider_RA::Initialize(void)
{
	CApplicationCursor wait_cursor;

	::ATL::CAtlString cs_storage;
	HRESULT hr_ = Platinum::client::data::GetStorageFolder(cs_storage);
	if (S_OK != hr_)
		return  hr_;

	m_objects.Cache().Clear();

	cs_storage+= _T("employee_records.csv");
	if (!CCsvFile::IsFileExist(cs_storage))
		return S_OK; // can be clean machine

	CCsvFile csv(eCsvFileOption::eUseCommaSeparator);
	hr_ = csv.Load(cs_storage, true);
	if (S_OK != hr_)
	{
		DisplayOpenDataFileError(cs_storage, hr_);
		return  hr_;
	}

	CBase64 base_64;
	const INT nFields = csv.FieldCount();
	const INT nRows = csv.RowCount();

	CEnrollRecordSpec spec_;
	hr_ = spec_.ValidateData(csv, true);
	if (FAILED(hr_))
		return hr_;

	for (INT i_ = 0; i_ < nRows; i_++)
	{
		const CCsvFile::TRow& row = csv.Row(i_);
		const INT nRowSize = (INT)row.size();
		CEmployeeDataRecord record;
		for (INT j_ = 0; j_ < nFields; j_++)
		{
			switch (j_)
			{
			case CEnrollRecordSpec::eCode:      record.Code    ((nRowSize > j_ ? row[j_].GetString() : _T("#n/a"))); break;
			case CEnrollRecordSpec::eEmployee:  record.Name    ((nRowSize > j_ ? row[j_].GetString() : _T("#n/a"))); break;
			case CEnrollRecordSpec::eWorkArea:  record.WorkArea((nRowSize > j_ ? row[j_].GetString() : _T("#n/a"))); break;
			default:;
			}
		}
		hr_ = m_objects.Cache().Append(record);
		if (S_OK != hr_)
			break;
	}
	if (S_OK == hr_)
	{
		CEnrollFvProvider fvi_provider(m_objects);
		hr_ = fvi_provider.Load(true);
	}
	return  hr_;
}

INT              CEnrollDataProvider_RA::RecordCount(void)const
{
	return m_objects.Cache().Count();
}

/////////////////////////////////////////////////////////////////////////////

const CEmployeeDataRecord& CEnrollDataProvider_RA::Record(const ::ATL::CAtlString& pEmployeeCode) const
{
	return m_objects.Cache().ItemOf(pEmployeeCode);
}