/*
	Created by Tech_dog (VToropov) on 25-Mar-2014 at 9:50:41pm, GMT+4, Saint-Petersburg, Tuesday;
	This is Platinum Client Work Area Data List Wrapper class implementation file.
*/
#include "StdAfx.h"
#include "PPS_DataListWrapWA.h"
#include "PPS_DataProvider_4_WA.h"
#include "Shared_GenericDataProvider.h"

using namespace Platinum;
using namespace Platinum::client;
using namespace Platinum::client::data;

using namespace Platinum::client::data::wrappers;

using namespace shared::lite;
using namespace shared::lite::data;

////////////////////////////////////////////////////////////////////////////

CWorkAreaDataListWrap::CWorkAreaDataListWrap(const CCommonSettings& settings_ref):m_settings(settings_ref)
{
}

CWorkAreaDataListWrap::~CWorkAreaDataListWrap(void)
{
}

////////////////////////////////////////////////////////////////////////////

HRESULT  CWorkAreaDataListWrap::SeedData(void)
{
	if (!TBaseWrap::IsValid())
		return OLE_E_BLANK;
	CWorkAreaDataProvider provider(m_settings);
	HRESULT hr_ = provider.Initialize();
	if (S_OK == hr_)
	{
		const CDataSet& ds_ref = provider.DataSet();
		const CDataRecord* pRecord = ds_ref.First();
		while(pRecord)
		{
			variant_t v_wa = pRecord->Data(_T("WorkArea"));
			if (VT_BSTR == v_wa.vt)
			{
				::ATL::CAtlString cs_wa(v_wa.bstrVal);
				const INT ndx__ = TBaseWrap::m_list.AddString(cs_wa.GetString());
				if (CB_ERR == ndx__)
					break;
			}
			pRecord = ds_ref.Next();
		}
	}
	return  hr_;
}

////////////////////////////////////////////////////////////////////////////

CWorkAreaListBoxWrap::CWorkAreaListBoxWrap(const CWindow& lst_box, const CCommonSettings& sets_ref):
	m_settings(sets_ref),
	m_list(lst_box)
{
}

CWorkAreaListBoxWrap::~CWorkAreaListBoxWrap(void)
{
}

////////////////////////////////////////////////////////////////////////////

HRESULT  CWorkAreaListBoxWrap::SeedData(void)
{
	if (!m_list.IsWindow())
		return OLE_E_BLANK;

	CWorkAreaDataProvider provider(m_settings);
	HRESULT hr_ = provider.Initialize();
	if (S_OK != hr_)
		return  hr_;

	::WTL::CListBox list  = m_list;
	const CDataSet& ds_ref = provider.DataSet();
	const CDataRecord* pRecord = ds_ref.First();
	while(pRecord)
	{
		variant_t v_wa = pRecord->Data(_T("WorkArea"));
		if (VT_BSTR == v_wa.vt)
		{
			::ATL::CAtlString cs_wa(v_wa.bstrVal);
			const INT ndx__ = list.AddString(cs_wa.GetString());
			if (LB_ERR == ndx__)
				break;
		}
		pRecord = ds_ref.Next();
	}
	return  hr_;
}