/*
	Created by Tech_dog (VToropov) 3-Apr-2014 at 9:20:43am, GMT+4, Saint-Petersburg, Thursday;
	This is Platinum Client Finger Vein Image Data Provider class implementation file.
*/
#include "StdAfx.h"
#include "PPS_DataProvider_4_FvData.h"
#include "PPS_DataProvider_CommonDefs.h"

using namespace Platinum::client::data;
using namespace Platinum::client::common;

#include "Shared_PersistentStorage.h"
#include "Shared_RawData.h"
#include "Shared_GenericAppObject.h"

using namespace shared::lite::data;
using namespace shared::lite::common;
using namespace shared::lite::persistent;

extern CApplication&    Global_GetAppObjectRef(void);

/////////////////////////////////////////////////////////////////////////////

CEnrollFvDataSpec::CEnrollFvDataSpec(void)
{
	const INT nFields = CEnrollFvDataSpec::nFieldCount;
	for ( INT i_ = 0; i_ < nFields; i_++)
	{
		::ATL::CAtlString cs_field;
		switch (i_)
		{
		case CEnrollFvDataSpec::eCode       : cs_field = _T("[Emp_Code]"); break;
		case CEnrollFvDataSpec::eFvImage    : cs_field = _T("[Fv_Image]"); break;
		case CEnrollFvDataSpec::eFvUid      : cs_field = _T("[Fv_Uid]");   break;
		}
		try
		{
			TBase::m_fields.push_back(cs_field);
		} catch (::std::bad_alloc&)
		{
			break;
		}
	}
	m_range = ::std::make_pair(
				CEnrollFvDataSpec::nFieldCount - 1,
				CEnrollFvDataSpec::nFieldCount - 0
			);
}

/////////////////////////////////////////////////////////////////////////////

namespace Platinum { namespace client { namespace data { namespace details
{
	using shared::lite::persistent::CCsvFile;
	using shared::lite::data::CBase64;

	using Platinum::client::data::CEnrollFvDataSpec;
	using Platinum::client::data::CEmployeeFvData;

	static HRESULT EnrollFvProvider_CreateHeader(CCsvFile::THeader& header_ref)
	{
		CEnrollFvDataSpec spec_;
		header_ref = spec_.CreateHeader();
		return S_OK;
	}

	static HRESULT EnrollFvProvider_CreateRow(CCsvFile::TRow& row_ref, LPCTSTR pszCode, const CEmployeeFvImage& _fv_image, CBase64& base_64)
	{
		if (!pszCode || ::_tcslen(pszCode) < 1)
			return E_INVALIDARG;

		if (!_fv_image.IsValid())
			return E_INVALIDARG;

		const INT nFields = CEnrollFvDataSpec::nFieldCount;
		for ( INT i_ = 0; i_ < nFields; i_++)
		{
			::ATL::CAtlString cs_encoded;
			try
			{
				switch(i_)
				{
				case CEnrollFvDataSpec::eCode:
					{
						::ATL::CAtlString cs_code(pszCode);
						const HRESULT hr_ = base_64.Encode(cs_code, cs_encoded);
						if (S_OK == hr_)
							row_ref.push_back(cs_encoded);
					} break;
				case CEnrollFvDataSpec::eFvImage:
					{
						const HRESULT hr_ = base_64.Encode(_fv_image.Data(), cs_encoded);
						if (S_OK == hr_)
							row_ref.push_back(cs_encoded);
					} break;
				case CEnrollFvDataSpec::eFvUid:
					{
						const HRESULT hr_ = base_64.Encode(_fv_image.Timestamp().ValueAsText(), cs_encoded);
						if (S_OK == hr_)
							row_ref.push_back(cs_encoded);
					} break;
				default:
					return DISP_E_BADINDEX;
				}
			} catch (::std::bad_alloc&){ return E_OUTOFMEMORY; }
		}
		return S_OK;
	}
}}}}

/////////////////////////////////////////////////////////////////////////////

CEnrollFvProvider::CEnrollFvProvider(CSharedObjects& obj_ref) : m_objects(obj_ref)
{
}

CEnrollFvProvider::~CEnrollFvProvider(void)
{
}

/////////////////////////////////////////////////////////////////////////////

HRESULT          CEnrollFvProvider::Load(const bool bSuppressAccessDeniedMessage)
{
	CApplicationCursor wait_cursor;

	::ATL::CAtlString cs_storage;
	HRESULT hr_ = Platinum::client::data::GetStorageFolder(cs_storage, bSuppressAccessDeniedMessage);
	if (S_OK != hr_)
		return  hr_;

	cs_storage += _T("employee_fv_data.csv");
	if (!CCsvFile::IsFileExist(cs_storage))
		return S_OK; // can be clean machine

	CCsvFile csv(eCsvFileOption::eUseCommaSeparator);
	hr_ = csv.Load(cs_storage, true);
	if (S_OK != hr_)
	{
		DisplayOpenDataFileError(cs_storage, hr_);
		return  hr_;
	}

	CBase64 base_64;
	const INT nRows = csv.RowCount();

	CEnrollFvDataSpec spec_;
	hr_ = spec_.ValidateData(csv, true);
	if (FAILED(hr_))
		return hr_;

	for (INT i_ = 0; i_ < nRows; i_++)
	{
		const CCsvFile::TRow& row = csv.Row(i_);
		const INT nRowSize = (INT)row.size();

		::ATL::CAtlString cs_code;
		_variant_t fv_image;
		::ATL::CAtlString cs_timestamp;

		for (INT j_ = 0; j_ < nRowSize && j_ < CEnrollFvDataSpec::nFieldCount; j_++)
		{
			switch (j_)
			{
			case CEnrollFvDataSpec::eCode:
				{
					hr_ = base_64.Decode(row[j_].GetString(), cs_code);
				} break;
			case CEnrollFvDataSpec::eFvImage:
				{
					hr_ = base_64.Decode(row[j_].GetString(), fv_image);
				} break;
			case CEnrollFvDataSpec::eFvUid:
				{
					hr_ = base_64.Decode(row[j_].GetString(), cs_timestamp);
				} break;
			}
		}
		if (!cs_code.IsEmpty() && S_OK == ValidateVeinData(fv_image))
		{
			CEmployeeDataRecord& record_ref = m_objects.Cache().ItemOf(cs_code);
			if (record_ref.IsValid())
			{
				hr_ = record_ref.FvData().Add(fv_image, cs_timestamp);
			}
		}
	}
	return  hr_;
}

HRESULT          CEnrollFvProvider::Save(const bool bSuppressAccessDeniedMessage)
{
	CApplicationCursor wait_cursor;

	::ATL::CAtlString cs_storage;
	HRESULT hr_ = Platinum::client::data::GetStorageFolder(cs_storage, bSuppressAccessDeniedMessage);
	if (S_OK != hr_)
		return  hr_;
	cs_storage+= _T("employee_fv_data.csv");
	CCsvFile csv(eCsvFileOption::eUseCommaSeparator);
	{
		CCsvFile::THeader header;
		hr_ = details::EnrollFvProvider_CreateHeader(header);
		if (S_OK != hr_)
			return  hr_;
		hr_ = csv.Header(header);
		if (S_OK != hr_)
			return  hr_;
	}
	CBase64 base_64;
	{
		const INT nRecs = m_objects.Cache().Count();
		for ( INT i_ = 0; i_ < nRecs; i_++)
		{
			const CEmployeeDataRecord& record = m_objects.Cache().Item(i_);
			const INT fv_count = record.FvData().Count();
			if (fv_count < 1)
				continue;
			for (INT j_ = 0; j_ < fv_count; j_++)
			{
				CCsvFile::TRow row;
				const CEmployeeFvImage& fv_image = record.FvData().Image(j_);
				if (S_OK == details::EnrollFvProvider_CreateRow(row, record.Code(), fv_image, base_64))
				{
					hr_ = csv.AddRow(row);
					if (S_OK != hr_)break;
				}
			}
			if (S_OK != hr_)break;
		}
	}
	if (S_OK == hr_)
		hr_ = csv.Save(cs_storage, true);
	return  hr_;
}