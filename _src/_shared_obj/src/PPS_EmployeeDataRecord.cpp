/*
	Created by Tech_dog (VToropov) on 24-Mar-2014 at 6:04:14pm, GMT+4, Saint-Petersburg, Monday;
	This is Platinum Client Employee Data Record class implementation file.
*/
#include "StdAfx.h"
#include "PPS_EmployeeDataRecord.h"
#include "PPS_DataProvider_CommonDefs.h"

using namespace Platinum::client::data;

/////////////////////////////////////////////////////////////////////////////

CEmployeeDataRecord::CEmployeeDataRecord(const bool bValid):m_bValid(bValid)
{
}

CEmployeeDataRecord::~CEmployeeDataRecord(void)
{
}

/////////////////////////////////////////////////////////////////////////////

HRESULT     CEmployeeDataRecord::Clear(void)
{
	if (!m_code.IsEmpty())m_code.Empty();
	if (!m_name.IsEmpty())m_name.Empty();
	if (!m_area.IsEmpty())m_area.Empty();
	if (!m_fv_data.IsEmpty())m_fv_data.Clear();
	m_fv_data.Changed(false);
	return S_OK;
}

LPCTSTR     CEmployeeDataRecord::Code(void) const
{
	return m_code.GetString();
}

HRESULT     CEmployeeDataRecord::Code(LPCTSTR pCode)
{
	if (!pCode || !::_tcslen(pCode))
	{
		if (!m_code.IsEmpty())m_code.Empty();
	}
	else
		m_code = pCode;
	return S_OK;
}

const CEmployeeFvData& CEmployeeDataRecord::FvData(void) const
{
	return m_fv_data;
}

CEmployeeFvData&       CEmployeeDataRecord::FvData(void)
{
	return m_fv_data;
}

bool        CEmployeeDataRecord::IsValid(void)const
{
	return m_bValid;
}

LPCTSTR     CEmployeeDataRecord::Name(void) const
{
	return m_name.GetString();
}

HRESULT     CEmployeeDataRecord::Name(LPCTSTR pName)
{
	if (!pName || !::_tcslen(pName))
	{
		if (!m_name.IsEmpty())m_name.Empty();
	}
	else
		m_name = pName;
	return S_OK;
}

LPCTSTR     CEmployeeDataRecord::WorkArea(void)const
{
	return m_area.GetString();
}

HRESULT     CEmployeeDataRecord::WorkArea(LPCTSTR pArea)
{
	if (!pArea || !::_tcslen(pArea))
	{
		if (!m_area.IsEmpty())m_area.Empty();
	}
	else
		m_area = pArea;
	return S_OK;
}

/////////////////////////////////////////////////////////////////////////////

bool CEmployeeDataRecord::operator!=(const CEmployeeDataRecord& rh_ref) const
{
	if (this->m_code != rh_ref.m_code)
		return true;
	if (this->m_name != rh_ref.m_name)
		return true;
	if (this->m_area != rh_ref.m_area)
		return true;
	if (this->m_fv_data != rh_ref.m_fv_data)
		return true;
	return false;
}

/////////////////////////////////////////////////////////////////////////////

CEmployeeDataRecord_ValidateRule::CEmployeeDataRecord_ValidateRule(const CEmployeeDataRecord& rec_ref):m_rec_ref(rec_ref)
{
}

CEmployeeDataRecord_ValidateRule::~CEmployeeDataRecord_ValidateRule(void)
{
}

/////////////////////////////////////////////////////////////////////////////

LPCTSTR     CEmployeeDataRecord_ValidateRule::Details(void)const
{
	return m_buffer.GetString();
}

HRESULT     CEmployeeDataRecord_ValidateRule::Validate(void)const
{
	if (!m_buffer.IsEmpty())m_buffer.Empty();
	LPCTSTR pCode = m_rec_ref.Code();
	if (!pCode || !::_tcslen(pCode))
	{
		m_buffer = _T("Employee code is empty");
		return S_FALSE;
	}
	LPCTSTR pName = m_rec_ref.Name();
	if (!pName || !::_tcslen(pName))
	{
		m_buffer = _T("Employee name is empty");
		return S_FALSE;
	}
	LPCTSTR pWorkArea = m_rec_ref.WorkArea();
	if (!pWorkArea || !::_tcslen(pWorkArea))
	{
		m_buffer = _T("Work area is not selected");
		return S_FALSE;
	}
	return S_OK;
}

HRESULT     CEmployeeDataRecord_ValidateRule::ValidateStrict(void)const
{
	HRESULT hr_ = this->Validate();
	if (S_OK != hr_)
		return  hr_;
	hr_ = this->ValidateVein();
	return hr_;
}

HRESULT     CEmployeeDataRecord_ValidateRule::ValidateVein(void)const
{
	if (!m_buffer.IsEmpty())m_buffer.Empty();
	const CEmployeeFvData& fv_data = m_rec_ref.FvData();
	const INT nCount = fv_data.Count();
	if (nCount < 1)
		return S_FALSE;
	for ( INT i_ = 0; i_ < nCount; i_++)
	{
		const CEmployeeFvImage& vein_ref = fv_data.Image(i_);
		if (!vein_ref.IsValid())
		{
			m_buffer = _T("The vein data is invalid");
			return HRESULT_FROM_WIN32(ERROR_INVALID_DATA);
		}
	}
	return S_OK;
}

/////////////////////////////////////////////////////////////////////////////

CEmployeeDataRecordEx::CEmployeeDataRecordEx(void) : TBase(true), m_selected(-1)
{
}

/////////////////////////////////////////////////////////////////////////////

INT         CEmployeeDataRecordEx::SelectedFvIndex(void)const
{
	return m_selected;
}

VOID        CEmployeeDataRecordEx::SelectedFvIndex(const INT _selected)
{
	m_selected = _selected;
}

/////////////////////////////////////////////////////////////////////////////

CEmployeeDataRecordEx& CEmployeeDataRecordEx::operator= (const CEmployeeDataRecord& _obj)
{
	*((CEmployeeDataRecord*)this) = _obj;
	this->m_selected = -1;
	return *this;
}