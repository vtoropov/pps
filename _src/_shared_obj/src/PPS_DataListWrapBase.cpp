/*
	Created by Tech_dog (VToropov) on 25-Mar-2014 at 1:38:38pm, GMT+4, Saint-Petersburg, Tuesday;
	This is Platinum Client Data List Control Wrapper Base class implementation file.
*/
#include "StdAfx.h"
#include "PPS_DataListWrapBase.h"

using namespace Platinum;
using namespace Platinum::client;
using namespace Platinum::client::data;
using namespace Platinum::client::data::wrappers;

namespace Platinum { namespace client { namespace data { namespace wrappers { namespace details
{
	static const INT DataListWrap_EditMargin = 3;
	static void DataListWrap_SetHeight_Pix(const HWND CB_hWnd, const INT nHeight)
	{
		const INT nPixHeight = nHeight - DataListWrap_EditMargin * 2;
		LRESULT res__ = 0;
		res__ = ::SendMessage(CB_hWnd, CB_SETITEMHEIGHT, (WPARAM)-1, nPixHeight); ATLASSERT(res__ != CB_ERR);
		res__ = ::SendMessage(CB_hWnd, CB_SETITEMHEIGHT, (WPARAM) 0, nPixHeight); ATLASSERT(res__ != CB_ERR);
	};
	static void DataListWrap_SetHeight_Dlu(const HWND hParent, const HWND CB_hWnd, const INT nHeight)
	{
		RECT rect = {0,0,0, nHeight};
		::MapDialogRect(hParent, &rect);
		DataListWrap_SetHeight_Pix(CB_hWnd, rect.bottom - rect.top);
	};
}}}}}

////////////////////////////////////////////////////////////////////////////

CDataListWrapBase::CDataListWrapBase(void) : m_ctrlId(0)
{
}

CDataListWrapBase::CDataListWrapBase(const HWND ctrl) : m_ctrlId(0)
{
	if (!::IsWindow(ctrl))
	{
		ATLASSERT(FALSE);
	}
	else
	{
		m_list = ctrl;
		m_ctrlId = ::GetDlgCtrlID(m_list);
	}
}

CDataListWrapBase::CDataListWrapBase(::ATL::CWindow& parent_ref, const UINT ctrlId) : m_ctrlId(0)
{
	HRESULT hr__ = this->AttachTo(parent_ref, ctrlId);
	if (S_OK != hr__)
	{
		ATLASSERT(FALSE);
	}
}

CDataListWrapBase::~CDataListWrapBase(void)
{
}

////////////////////////////////////////////////////////////////////////////

HRESULT      CDataListWrapBase::AppendItem(LPCTSTR pszItem, const LONG lData)
{
	if (!m_list.IsWindow())
		return OLE_E_BLANK;
	const INT ndx_ = m_list.AddString(pszItem);
	if (CB_ERR == ndx_)
		return E_OUTOFMEMORY;
	if (lData)
		m_list.SetItemData(ndx_, (DWORD_PTR)lData);
	return S_OK;
}

HRESULT      CDataListWrapBase::AttachTo(::ATL::CWindow& parent_ref, const UINT ctrlId)
{
	m_ctrlId = ctrlId;
	m_list = parent_ref.GetDlgItem((INT)m_ctrlId);
	if (!m_list.IsWindow())
		return E_INVALIDARG;
	else
		return S_OK;
}

HRESULT      CDataListWrapBase::Detach(void)
{
	HRESULT hr__ = (m_list.IsWindow() ? S_OK : S_FALSE);
	if (S_OK == hr__)
	{
		m_list.Detach();
		m_ctrlId = 0;
	}
	return  hr__;
}

HWND         CDataListWrapBase::GetCtrlHandle(void)const
{
	return m_list.m_hWnd;
}

bool         CDataListWrapBase::IsValid(void)const
{
	return !!m_list.IsWindow();
}

HRESULT      CDataListWrapBase::SelectItem(LPCTSTR pText)
{
	if (!m_list.IsWindow())
		return OLE_E_BLANK;
	const INT nIndex = m_list.SelectString(-1, pText);
	return (CB_ERR != nIndex ? S_OK : S_FALSE);
}

HRESULT      CDataListWrapBase::SelectItemOf(const LONG nItemIdentifier)
{
	if (!m_list.IsWindow())
		return OLE_E_BLANK;
	const INT cnt__ = m_list.GetCount();
	for (INT i__ = 0; i__ < cnt__; i__++)
	{
		const LONG nIdentifier = (LONG)m_list.GetItemData(i__);
		if (nIdentifier == nItemIdentifier)
		{
			m_list.SetCurSel(i__);
			return S_OK;
		}
	}
	m_list.SetCurSel(CB_ERR);
	return S_FALSE;
}

HRESULT      CDataListWrapBase::SetFont(const HFONT fnt__)
{
	if (!m_list.IsWindow())
		return OLE_E_BLANK;
	m_list.SetFont(fnt__);
	return S_OK;
}

HRESULT      CDataListWrapBase::SetHeight(const INT nHeight, const bool bDialogUnits)
{
	if (!m_list.IsWindow())
		return OLE_E_BLANK;
	if (bDialogUnits)
		details::DataListWrap_SetHeight_Dlu(m_list.GetParent(), m_list, nHeight);
	else
		details::DataListWrap_SetHeight_Pix(m_list, nHeight);
	return S_OK;
}

////////////////////////////////////////////////////////////////////////////

HRESULT      CDataListWrapBase::SeedData(void)
{
	return E_NOTIMPL;
}