#ifndef _PLATINUMCLIENTEMPLOYEERECSCACHE_H_516EE1CF_84A0_48ff_9382_8CF92A23537A_INCLUDED
#define _PLATINUMCLIENTEMPLOYEERECSCACHE_H_516EE1CF_84A0_48ff_9382_8CF92A23537A_INCLUDED
/*
	Created by Tech_dog (VToropov) on 24-May-2015 at 1:00:00pm, GMT+8, Phuket, Sunday;
	This is Platinum Client Library Employee Record Data Cache class declaration file.
*/
#include "PPS_EmployeeDataRecord.h"

namespace Platinum { namespace client { namespace data
{
	class CEmployeeRecsCache
	{
		typedef ::std::vector<CEmployeeDataRecord>  TRecords;
		typedef ::std::map<::ATL::CAtlString, INT>  TIndex;  // the key is employee code
	private:
		TRecords                     m_cache;
		TIndex                       m_index;
	public:
		CEmployeeRecsCache(void);
		~CEmployeeRecsCache(void);
	public:
		HRESULT                      Append(const CEmployeeDataRecord&);
		HRESULT                      Clear(void);
		INT                          Count(void)const;
		const CEmployeeDataRecord&   Item  (const INT nIndex)const;
		CEmployeeDataRecord&         Item  (const INT nIndex);
		const CEmployeeDataRecord&   ItemOf(const ::ATL::CAtlString& cs_code)const;
		CEmployeeDataRecord&         ItemOf(const ::ATL::CAtlString& cs_code);
		HRESULT                      ItemOf(const CEmployeeDataRecord&);
		HRESULT                      Remove(const ::ATL::CAtlString& cs_code);
	private:
		CEmployeeRecsCache(const CEmployeeRecsCache&);
		CEmployeeRecsCache& operator= (const CEmployeeRecsCache&);
	};
}}}

#endif/*_PLATINUMCLIENTEMPLOYEERECSCACHE_H_516EE1CF_84A0_48ff_9382_8CF92A23537A_INCLUDED*/