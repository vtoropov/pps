#ifndef __PLATINUMCLIENTWORKAREADATAPROVIDER_H_B80137D0_82CD_4a1e_A675_87A84698CE6A_INCLUDED
#define __PLATINUMCLIENTWORKAREADATAPROVIDER_H_B80137D0_82CD_4a1e_A675_87A84698CE6A_INCLUDED
/*
	Created by Tech_dog (VToropov) on 25-Mar-2014 at 2:13:46pm, GMT+4, Saint-Petersburg, Tuesday;
	This is Platinum Client Work Area Data Provider class declaration file.
*/
#include "PPS_CommonSettings.h"
#include "Shared_GenericDataProvider.h"

namespace Platinum { namespace client { namespace data
{
	using Platinum::client::common::CCommonSettings;
	using shared::lite::data::CDataSet;

	class CWorkAreaDataProvider
	{
	private:
		bool                    m_bInitialized;
		const CCommonSettings&  m_settings;
		CDataSet                m_ds;
	public:
		CWorkAreaDataProvider(const CCommonSettings&);
		~CWorkAreaDataProvider(void);
	public:
		HRESULT                 Initialize(void);
		bool                    IsInitialized(void)const;
		const CDataSet&         DataSet(void)const;
	};
}}}

#endif/*__PLATINUMCLIENTWORKAREADATAPROVIDER_H_B80137D0_82CD_4a1e_A675_87A84698CE6A_INCLUDED*/