#ifndef __PLATINUMCLIENTFINGERVEINIMAGEDATAPROVIDER_15BB822F_BCF2_4777_8BB5_82452F1A0889_INCLUDED
#define __PLATINUMCLIENTFINGERVEINIMAGEDATAPROVIDER_15BB822F_BCF2_4777_8BB5_82452F1A0889_INCLUDED
/*
	Created by Tech_dog (VToropov) 3-Apr-2014 at 9:03:47am, GMT+4, Saint-Petersburg, Thursday;
	This is Platinum Client Finger Vein Image Data Provider class declaration file.
*/
#include "PPS_EmployeeFvData.h"
#include "PPS_SharedObjects.h"
#include "PPS_DataProvider_CommonDefs.h"

namespace Platinum { namespace client { namespace data
{
	class CEnrollFvDataSpec : public CRecordSpecBase
	{
		typedef CRecordSpecBase TBase;
	public:
		enum _enum{
			eCode      = 0,  // employee code
			eFvImage   = 1,  // finger vein data
			eFvUid     = 2,  // finger vein unique identifier (actually a timestamp, i.e. INT64)
		};
	public:
		CEnrollFvDataSpec(void);
	public:
		static const INT nFieldCount = CEnrollFvDataSpec::eFvUid + 1;
	};

	using Platinum::client::common::CSharedObjects;

	class CEnrollFvProvider
	{
	private:
		CSharedObjects&  m_objects;
	public:
		CEnrollFvProvider(CSharedObjects&);
		~CEnrollFvProvider(void);
	public:
		HRESULT          Load(const bool bSuppressAccessDeniedMessage);
		HRESULT          Save(const bool bSuppressAccessDeniedMessage);
	};
}}}

#endif/*__PLATINUMCLIENTFINGERVEINIMAGEDATAPROVIDER_15BB822F_BCF2_4777_8BB5_82452F1A0889_INCLUDED*/