#ifndef __PLATINUMCLIENTTIMETRACKDATARECORD_H_8AF0C8EA_1BFB_4d3a_8318_304F3F69DBD6_INCLUDED
#define __PLATINUMCLIENTTIMETRACKDATARECORD_H_8AF0C8EA_1BFB_4d3a_8318_304F3F69DBD6_INCLUDED
/*
	Created by Tech_dog (VToropov) on 27-Mar-2014 at 2:30:28am, GMT+4, Saint-Petersburg, Thursday;
	This is Platinum Client Time Tracking Data Record class declaration file.
*/
#include "PPS_EmployeeDataRecord.h"
#include "Shared_DataFormat.h"

namespace Platinum { namespace client { namespace data
{
	using shared::lite::data::CTimestamp;

	class CTimeTrackRecord
	{
	private:
		bool                 m_bValid;
		::ATL::CAtlString    m_code;
		::ATL::CAtlString    m_name;
		::ATL::CAtlString    m_date;
		::ATL::CAtlString    m_time;
		::ATL::CAtlString    m_area;
		CTimestamp           m_timestamp;   // finger vein identifier
	public:
		CTimeTrackRecord(const bool bValid = true);
		~CTimeTrackRecord(void);
	public:
		LPCTSTR              Code(void)    const;
		HRESULT              Code(LPCTSTR)      ;
		LPCTSTR              Date(void)    const;
		HRESULT              Date(LPCTSTR)      ;
		const CTimestamp&    FvTimestamp(void)const;
		CTimestamp&          FvTimestamp(void);
		bool                 IsValid(void) const;
		LPCTSTR              Name(void)    const;
		HRESULT              Name(LPCTSTR)      ;
		LPCTSTR              Time(void)    const;
		HRESULT              Time(LPCTSTR)      ;
		LPCTSTR              WorkArea(void)const;
		HRESULT              WorkArea(LPCTSTR)  ;
	public:
		bool operator!=(const CTimeTrackRecord&) const;
		CTimeTrackRecord& operator= (const CEmployeeDataRecord&);
		CTimeTrackRecord& operator= (const CEmployeeDataRecordEx&);
	};

	class CTimeTrackRecord_ValidateRule
	{
	private:
		const CTimeTrackRecord&   m_rec_ref;
		mutable ::ATL::CAtlString m_buffer;
	public:
		CTimeTrackRecord_ValidateRule(const CTimeTrackRecord&);
		~CTimeTrackRecord_ValidateRule(void);
	public:
		LPCTSTR              Details(void)const;
		HRESULT              Validate(void)const;
	};
}}}

#endif/*__PLATINUMCLIENTTIMETRACKDATARECORD_H_8AF0C8EA_1BFB_4d3a_8318_304F3F69DBD6_INCLUDED*/