#ifndef _PPSUSBCONNECTOR_H_D052E046_12E6_4a5d_8D6C_CC723CE020A6_INCLUDED
#define _PPSUSBCONNECTOR_H_D052E046_12E6_4a5d_8D6C_CC723CE020A6_INCLUDED
/*
	Created by Tech_dog (VToropov) on 19-May-2015, at 1:00:00pm, GMT+8, Phuket, Tuesday;
	This is PPS shared library USB notifications connector class declaration file.
*/
#include <Wbemidl.h>
#pragma comment(lib, "wbemuuid.lib")

#include "Shared_SystemError.h"

namespace Platinum { namespace client { namespace usb
{
	using shared::lite::common::CSysError;

	class eDeviceNotification
	{
	public:
		enum _e{
			eUndefined     = 0x0,
			eArrival       = 0x1,
			eRemoval       = 0x2,
		};
	};

	interface IDeviceNotification
	{
		virtual  HRESULT     DeviceNotification_OnChange(const eDeviceNotification::_e, const GUID& guidIdentifier) {guidIdentifier; return E_NOTIMPL;}
		virtual  HRESULT     DeviceNotification_OnChange(const eDeviceNotification::_e, const _variant_t& dev_path) {dev_path;       return E_NOTIMPL;}
	};

	class CEventConnector
	{
	private:
		class CNotificationHandler : public IWbemObjectSink
		{
		private:
			LONG                 m_lRef;
			IDeviceNotification& m_notify;
		public:
			CNotificationHandler(IDeviceNotification&);
			~CNotificationHandler(void);
		public:
			virtual HRESULT  STDMETHODCALLTYPE QueryInterface(REFIID riid, void** ppv) override sealed;
			virtual ULONG    STDMETHODCALLTYPE AddRef(void) override sealed;
			virtual ULONG    STDMETHODCALLTYPE Release(void) override sealed;

		private:
			virtual HRESULT  STDMETHODCALLTYPE Indicate( 
				/* [in] */ long lObjectCount,
				/* [size_is][in] */ __RPC__in_ecount_full(lObjectCount) IWbemClassObject** apObjArray) override sealed;
	        
			virtual HRESULT  STDMETHODCALLTYPE SetStatus( 
				/* [in] */ long lFlags,
				/* [in] */ HRESULT hResult,
				/* [unique][in] */ __RPC__in_opt BSTR strParam,
				/* [unique][in] */ __RPC__in_opt IWbemClassObject* pObjParam) override sealed;
		};
	private:
		CNotificationHandler*    m_pHandler;
		CComPtr<IWbemServices>   m_pService;
		CComPtr<IWbemObjectSink> m_pObject;
	private:
		IDeviceNotification&     m_notify;
		CSysError                m_error;
	public:
		CEventConnector(IDeviceNotification&);
		~CEventConnector(void);
	};
}}}

#endif/*_PPSUSBCONNECTOR_H_D052E046_12E6_4a5d_8D6C_CC723CE020A6_INCLUDED*/