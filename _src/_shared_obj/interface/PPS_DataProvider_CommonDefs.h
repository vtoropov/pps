#ifndef __PLATINUMCLIENTDATAPROVIDERCOMMONDEFS_H_B967854D_ACAC_4554_9CF8_5DC74B9D29C7_INCLUDED
#define __PLATINUMCLIENTDATAPROVIDERCOMMONDEFS_H_B967854D_ACAC_4554_9CF8_5DC74B9D29C7_INCLUDED
/*
	Created by Tech_dog (VToropov) on 3-Apr-2014 at 10:05:42am, GMT+4, Saint-Petersburg, Thursday;
	This is Platinum Client Data Provider Common Definitions declaration file.
*/
#include "Shared_PersistentStorage.h"

namespace Platinum { namespace client { namespace data
{
	VOID            DisplayOpenDataFileError(const CAtlString& _file, const HRESULT _error);
	ATL::CAtlString GetAccessDeniedErrorMessage(LPCTSTR pFolder, const HRESULT hError);
	HRESULT         GetBackupFolder(ATL::CAtlString& cs_folder);
	HRESULT         GetStorageFolder(ATL::CAtlString& cs_storage, const bool bSuppressErrMessage = false);
	HRESULT         IsDataFolderExist(LPCTSTR pFolderPath, const bool bWithCreateOption = true);
	HRESULT         ValidateVeinData(const _variant_t&, const bool bEmptyIsAllowed = true);

	using shared::lite::persistent::CCsvFile;

	class CRecordSpecBase
	{
	protected:
		typedef ::std::vector<CAtlString>  TFields;
		typedef ::std::pair<DWORD, DWORD>  TAcceptRange;
	protected:
		TFields           m_fields;
		TAcceptRange      m_range;
	protected:
		CRecordSpecBase(void);
	public:
		CCsvFile::THeader CreateHeader(void)const;
		INT               FieldCount(void)const;
		CAtlString        FieldNameOf(const INT nIndex)const;
		INT               GetHeaderLen(void)const;
		HRESULT           ValidateData(const CCsvFile&, const bool bShowError);
	};
}}}

#endif/*__PLATINUMCLIENTDATAPROVIDERCOMMONDEFS_H_B967854D_ACAC_4554_9CF8_5DC74B9D29C7_INCLUDED*/