#ifndef __PLATINUMCLIENTSHAREDOBJECTS_H_4D49FC3C_A612_4a32_89D2_45830A77882F_INCLUDED
#define __PLATINUMCLIENTSHAREDOBJECTS_H_4D49FC3C_A612_4a32_89D2_45830A77882F_INCLUDED
/*
	Created by Tech_dog (VToropov) on 22-Mar-2014 at 9:52:48pm, GMT+4, Moscow Region,
	Rail Road Train #43, Coatch #6, Place #1, Saturday;
	This is Platinum Client Application Shared Objects class declaratioon file.
*/
#include "PPS_EmployeeDataRecord.h"
#include "PPS_CommonSettings.h"
#include "PPS_EmployeeCache.h"
#include "ImageProcessor.h"

namespace Platinum { namespace client { namespace common
{
	using Platinum::client::common::CCommonSettings;
	using Platinum::client::data::CEmployeeDataRecord;
	using Platinum::client::data::CEmployeeRecsCache;

	using shared::recognition::IImageProcessor;
	using shared::recognition::IInitializer;
	using shared::recognition::IError;
	using shared::recognition::eProcessorType;
	using shared::recognition::CInitializerPtr;
	using shared::recognition::CImageProcessorPtr;

	class CSharedObjects
	{
	protected:
		HRESULT                      m_hResult;
	protected:
		CCommonSettings&             m_settings_ref;
	protected:
		eProcessorType               m_proc_type;
		CInitializerPtr              m_proc_init;
		CImageProcessorPtr           m_proc_obj;
		CEmployeeRecsCache           m_cache;
	public:
		CSharedObjects(CCommonSettings&);
		~CSharedObjects(void);
	public:
		const CEmployeeRecsCache&    Cache(void)         const;
		CEmployeeRecsCache&          Cache(void)              ;
		const IError&                Error(void)         const; // gets image processor error object reference (read-only)
		HRESULT                      Initialize(void)         ; // initializes the image processor
		const IInitializer&          Initializer(void)   const; // gets image processor initializer object (read-only)
		IInitializer&                Initializer(void)        ; // gets image processor initializer object (read-write)
		bool                         IsInitialized(void) const; // returns the initialization state of the image processor
		HRESULT                      LastResult(void)    const; // returns the last operation result
		const IImageProcessor&       Processor(void)     const; // gets image processor object reference (read-only)
		IImageProcessor&             Processor(void)          ; // gets image processor object reference (read-write)
		const CCommonSettings&       Settings(void)      const; // gets common settings reference (read-only)
		CCommonSettings&             Settings(void)           ; // gets common settings reference (read-write)
	private:
		CSharedObjects(const CSharedObjects&);
		CSharedObjects& operator= (const CSharedObjects&);
	};
}}}


#endif/*__PLATINUMCLIENTSHAREDOBJECTS_H_4D49FC3C_A612_4a32_89D2_45830A77882F_INCLUDED*/