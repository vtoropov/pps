#ifndef __PLATINUMCLIENTEMPLOYEEDATARECORD_H_62672490_9B7A_41e1_9673_E6DCAFFEE12E_INCLUDED
#define __PLATINUMCLIENTEMPLOYEEDATARECORD_H_62672490_9B7A_41e1_9673_E6DCAFFEE12E_INCLUDED
/*
	Created by Tech_dog (VToropov) on 24-Mar-2014 at 5:54:16pm, GMT+4, Saint-Petersburg, Monday;
	This is Platinum Client Employee Data Record class declaration file.
*/
#include "PPS_EmployeeFvData.h"

namespace Platinum { namespace client { namespace data
{
	class CEmployeeDataRecord
	{
	private:
		bool              m_bValid;
		CAtlString        m_code;
		CAtlString        m_name;
		CAtlString        m_area;
		CEmployeeFvData   m_fv_data;
	public:
		CEmployeeDataRecord(const bool bValid = true);
		~CEmployeeDataRecord(void);
	public:
		HRESULT           Clear(void);
		LPCTSTR           Code(void) const;
		HRESULT           Code(LPCTSTR);
		const
		CEmployeeFvData&  FvData(void) const;
		CEmployeeFvData&  FvData(void);
		bool              IsValid(void)const;
		LPCTSTR           Name(void) const;
		HRESULT           Name(LPCTSTR);
		LPCTSTR           WorkArea(void)const;
		HRESULT           WorkArea(LPCTSTR);
	public:
		bool operator!=(const CEmployeeDataRecord&) const;
	};

	class CEmployeeDataRecord_ValidateRule
	{
	private:
		const CEmployeeDataRecord&   m_rec_ref;
		mutable ::ATL::CAtlString    m_buffer;
	public:
		CEmployeeDataRecord_ValidateRule(const CEmployeeDataRecord&);
		~CEmployeeDataRecord_ValidateRule(void);
	public:
		LPCTSTR           Details(void)const;
		HRESULT           Validate(void)const;
		HRESULT           ValidateStrict(void)const;
		HRESULT           ValidateVein(void)const;
	};

	class CEmployeeDataRecordEx :  // this extended version is used to save clocking data
		public  CEmployeeDataRecord
	{
		typedef CEmployeeDataRecord TBase;
	private:
		INT               m_selected; // selected vein image index that is used for clocking record
	public:
		CEmployeeDataRecordEx(void);
	public:
		INT               SelectedFvIndex(void)const;
		VOID              SelectedFvIndex(const INT);
	public:
		CEmployeeDataRecordEx& operator= (const CEmployeeDataRecord&);
	};
}}}

#endif/*__PLATINUMCLIENTEMPLOYEEDATARECORD_H_62672490_9B7A_41e1_9673_E6DCAFFEE12E_INCLUDED*/