#ifndef __PLATINUMCLIENTTIMETRACKDATAPROVIDER_H_1421F5FE_E94A_48a6_BBCA_63D959570238_INCLUDED
#define __PLATINUMCLIENTTIMETRACKDATAPROVIDER_H_1421F5FE_E94A_48a6_BBCA_63D959570238_INCLUDED
/*
	Created by Tech_dog (VToropov) on 27-Mar-2014 at 2:44:42am, GMT+4, Saint-Petersburg, Thursday;
	This is Platinum Client Track Time Data Provider class declaration file.
*/
#include "PPS_TimeTrackRecord.h"
#include "PPS_EmployeeDataRecord.h"
#include "PPS_EmployeeCache.h"
#include "PPS_DataProvider_CommonDefs.h"

namespace Platinum { namespace client { namespace data
{
	using Platinum::client::data::CEmployeeDataRecord;
	using Platinum::client::data::CEmployeeDataRecordEx;

	class CTimeTrackDataSpec : public CRecordSpecBase
	{
		typedef CRecordSpecBase TBase;
	public:
		enum _enum{
			eCode     = 0,
			eDate     = 1,
			eTime     = 2,
			eWorkArea = 3,
			eFvId     = 4,  // an identifier of user's finger vein enrollment template that was used for recognition
		};
	public:
		CTimeTrackDataSpec(void);
	public:
		static const INT nFieldCount = CTimeTrackDataSpec::eFvId + 1;
	};

	class CTimeTrackDataProvider
	{
	public:
		typedef ::std::vector<CTimeTrackRecord> TRecords;
	private:
		bool              m_bDirty;
		TRecords          m_records;
	public:
		CTimeTrackDataProvider(void);
		~CTimeTrackDataProvider(void);
	public:
		HRESULT           Initialize(const bool bSuppressAccessDeniedMessage);
		HRESULT           Initialize(const bool bSuppressAccessDeniedMessage, const CEmployeeRecsCache&);
		const
		CTimeTrackRecord& Record(const INT nIndex)const;
		CTimeTrackRecord& Record(const INT nIndex);
		INT               RecordCount(void)const;
		HRESULT           Refresh(void);
		HRESULT           Refresh(const CEmployeeRecsCache&);
		HRESULT           Save(void);
		HRESULT           SaveEntry(const CTimeTrackRecord&);   // appends a new entry record to the data file
	public:
		HRESULT           Append(const CTimeTrackRecord&);
	};
}}}

#endif/*__PLATINUMCLIENTTIMETRACKDATAPROVIDER_H_1421F5FE_E94A_48a6_BBCA_63D959570238_INCLUDED*/