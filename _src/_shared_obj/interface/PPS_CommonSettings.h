#ifndef __PLATINUMCLIENTAPPLICATIONCOMMONSETTINGS_H_3FD03D04_0734_4fe1_B4CC_92F1FADC911C_INCLUDED
#define __PLATINUMCLIENTAPPLICATIONCOMMONSETTINGS_H_3FD03D04_0734_4fe1_B4CC_92F1FADC911C_INCLUDED
/*
	Created by Tech_dog (VToropov) on 22-Mar-2014 at 8:15:26pm, GMT+4, Moscow Region,
	Rail Road Train #43, Coatch #6, Place #1, Saturday;
	This is Platinum Client Application Common Settings class declaration file.
*/
#include "Shared_PersistentStorage.h"

namespace Platinum { namespace client { namespace common
{
	using shared::lite::persistent::CPrivateProfile;
	class CCommonSettings
	{
	public:
		class CDataStorage
		{
		private:
			CPrivateProfile&  m_prf_ref;
		public:
			CDataStorage(CPrivateProfile&);
			~CDataStorage(void);
		public:
			CAtlString        BackupFolder (void)const;   // backup folder for data files
			CAtlString        CustomFolder (void)const;   // gets CSV files' folder that is specified in the application profile
			CAtlString        DefaultFolder(void)const;   // gets CSV files' default folder, i.e. application_full_path\storage
			bool              IsCustomFolderUsed(void)const;
			CAtlString        WorkAreaProfilePath(void)const;
			CAtlString        WorkAreaProfileSection(void)const;
		};
	private:
		CDataStorage          m_stg;
	protected:
		CPrivateProfile       m_profile;
	public:
		CCommonSettings(void);
		~CCommonSettings(void);
	public:
		const CDataStorage&   Storage(void)const;         // gets data storage settings object reference
	};
}}}

typedef Platinum::client::common::CCommonSettings::CDataStorage TStorageSettings;

#endif/*__PLATINUMCLIENTAPPLICATIONCOMMONSETTINGS_H_3FD03D04_0734_4fe1_B4CC_92F1FADC911C_INCLUDED*/