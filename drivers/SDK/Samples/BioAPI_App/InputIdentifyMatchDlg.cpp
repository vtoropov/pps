// InputIdentifyMatchDlg.cpp
//

#include "stdafx.h"
#include "BioAPI_Test.h"
#include "InputIdentifyMatchDlg.h"
#include ".\inputidentifymatchdlg.h"


// CInputIdentifyMatchDlg dialog

IMPLEMENT_DYNAMIC(CInputIdentifyMatchDlg, CDialog)
CInputIdentifyMatchDlg::CInputIdentifyMatchDlg(CWnd* pParent /*=NULL*/)
	: CDialog(CInputIdentifyMatchDlg::IDD, pParent)
	, m_InputBIRKind(0)
	, m_FMR(0)
	, m_Timeout(0)
	, m_MaxNumberOfResults(0)
{
}

CInputIdentifyMatchDlg::~CInputIdentifyMatchDlg()
{
}

void CInputIdentifyMatchDlg::DoDataExchange(CDataExchange* pDX)
{
	CDialog::DoDataExchange(pDX);
	DDX_Control(pDX, IDC_LIST2, m_ProcessedBIRList);
	DDX_Control(pDX, IDC_LIST1, m_TemplateBIRList);
	DDX_Radio(pDX, IDC_RADIO1, m_InputBIRKind);
	DDX_Text(pDX, IDC_EDIT1, m_FMR);
	DDX_Text(pDX, IDC_EDIT8, m_Timeout);
	DDX_Text(pDX, IDC_EDIT9, m_MaxNumberOfResults);
}


BEGIN_MESSAGE_MAP(CInputIdentifyMatchDlg, CDialog)
	ON_BN_CLICKED(IDOK, OnBnClickedOk)
	ON_BN_CLICKED(IDC_RADIO1, OnBnClickedRadio1)
	ON_BN_CLICKED(IDC_RADIO2, OnBnClickedRadio2)
END_MESSAGE_MAP()


// CInputIdentifyMatchDlg message handlers

BOOL CInputIdentifyMatchDlg::OnInitDialog()
{
	CDialog::OnInitDialog();

	

	char buf[256];

	for (BIRHandleArray::iterator e = m_BIRHandleList.begin(); e != m_BIRHandleList.end(); e++) {
		sprintf(buf, "BIRHandle : BIRHandle = %08x, Purpose = %d, Type = %d", e->Handle, e->Purpose, e->Type);
		m_ProcessedBIRList.AddString(buf);
	}
	for (BIRArray::iterator e = m_BIRList.begin(); e != m_BIRList.end(); e++) {
		sprintf(buf, "BIR : Date = %04d/%02d/%02d %02d:%02d:%02d, Purpose = %d, Type = %d", 
			e->Header.CreationDTG.Date.Year, e->Header.CreationDTG.Date.Month, e->Header.CreationDTG.Date.Day, 
			e->Header.CreationDTG.Time.Hour, e->Header.CreationDTG.Time.Minute, e->Header.CreationDTG.Time.Second, 
			e->Header.Purpose, e->Header.Type);
		m_TemplateBIRList.AddString(buf);
	}

	m_InputBIRKind = 0;

	return TRUE;  // return TRUE unless you set the focus to a control
	
}

void CInputIdentifyMatchDlg::OnBnClickedOk()
{
	
	UpdateData(TRUE);
	m_ProcessedIndex = m_ProcessedBIRList.GetCurSel();
	if ( m_ProcessedIndex == LB_ERR ) {
		return ;
	}

	m_TemplateListSize = m_TemplateBIRList.GetSelItems(256, m_TemplateIndexList);

	OnOK();
}

void CInputIdentifyMatchDlg::OnBnClickedRadio1()
{
	
	char buf[256];
	m_ProcessedBIRList.ResetContent();
	for (BIRHandleArray::iterator e = m_BIRHandleList.begin(); e != m_BIRHandleList.end(); e++) {
		sprintf(buf, "BIRHandle : BIRHandle = %08x, Purpose = %d, Type = %d", e->Handle, e->Purpose, e->Type);
		m_ProcessedBIRList.AddString(buf);
	}
	UpdateData(TRUE);
}

void CInputIdentifyMatchDlg::OnBnClickedRadio2()
{
	
	char buf[256];
	m_ProcessedBIRList.ResetContent();
	for (BIRArray::iterator e = m_BIRList.begin(); e != m_BIRList.end(); e++) {
		sprintf(buf, "BIR : Date = %04d/%02d/%02d %02d:%02d:%02d, Purpose = %d, Type = %d", 
			e->Header.CreationDTG.Date.Year, e->Header.CreationDTG.Date.Month, e->Header.CreationDTG.Date.Day, 
			e->Header.CreationDTG.Time.Hour, e->Header.CreationDTG.Time.Minute, e->Header.CreationDTG.Time.Second, 
			e->Header.Purpose, e->Header.Type);
		m_ProcessedBIRList.AddString(buf);
	}
	UpdateData(TRUE);
}
