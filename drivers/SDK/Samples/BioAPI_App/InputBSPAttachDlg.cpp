// InputBSPAttachDlg.cpp
//

#include "stdafx.h"
#include "BioAPI_Test.h"
#include "InputBSPAttachDlg.h"
#include ".\inputbspattachdlg.h"


// CInputBSPAttachDlg dialog

IMPLEMENT_DYNAMIC(CInputBSPAttachDlg, CDialog)
CInputBSPAttachDlg::CInputBSPAttachDlg(CWnd* pParent /*=NULL*/)
	: CDialog(CInputBSPAttachDlg::IDD, pParent)
{
}

CInputBSPAttachDlg::~CInputBSPAttachDlg()
{
}

void CInputBSPAttachDlg::DoDataExchange(CDataExchange* pDX)
{
	CDialog::DoDataExchange(pDX);
	DDX_Control(pDX, IDC_LIST1, m_UnitList);
}


BEGIN_MESSAGE_MAP(CInputBSPAttachDlg, CDialog)
	ON_BN_CLICKED(IDOK, OnBnClickedOk)
END_MESSAGE_MAP()


// CInputBSPAttachDlg message handlers

BOOL CInputBSPAttachDlg::OnInitDialog()
{
	CDialog::OnInitDialog();

	
	for (StrArray::iterator e = m_UnitIDList.begin(); e != m_UnitIDList.end(); e++) {
		m_UnitList.AddString(*e);
	}

	return TRUE;  // return TRUE unless you set the focus to a control
	
}

void CInputBSPAttachDlg::SetUnitIDList(StrArray UnitIDList)
{
	m_UnitIDList = UnitIDList;
}

int CInputBSPAttachDlg::GetSelectedUnitNumList(int SelectedUnitList[])
{
	for (int i = 0; i < m_NumList; i++) {
		SelectedUnitList[i] = m_SelectedUnitNumList[i];
	}

	
	return m_NumList;
}


void CInputBSPAttachDlg::OnBnClickedOk()
{
	
	UpdateData(FALSE);
	m_NumList = m_UnitList.GetSelItems(4, m_SelectedUnitNumList);

	OnOK();
}
