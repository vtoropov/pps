// verifyBSP.cpp
//

#include "stdafx.h"
#include "BioAPI_Test.h"
#include "verifyBSP.h"
#include ".\verifybsp.h"


// verifyBSP dialog

IMPLEMENT_DYNAMIC(verifyBSP, CDialog)
verifyBSP::verifyBSP(CWnd* pParent /*=NULL*/)
	: CDialog(verifyBSP::IDD, pParent)
	, m_bsphash(_T(""))
{
}

verifyBSP::~verifyBSP()
{
}

void verifyBSP::DoDataExchange(CDataExchange* pDX)
{
	CDialog::DoDataExchange(pDX);
	DDX_Text(pDX, IDC_EDIT1, m_bsphash);
}


BEGIN_MESSAGE_MAP(verifyBSP, CDialog)
	ON_BN_CLICKED(IDCANCEL, OnBnClickedCancel)
	ON_BN_CLICKED(IDOK, OnBnClickedOk)
END_MESSAGE_MAP()


// verifyBSP message handlers

void verifyBSP::OnBnClickedCancel()
{
	
	OnCancel();
}

void verifyBSP::OnBnClickedOk()
{
	
	OnOK();
}
