#pragma once
#include "afxwin.h"
#include "Define.h"

// CInputFreeBIRHandleDlg dialog

class CInputFreeBIRHandleDlg : public CDialog
{
	DECLARE_DYNAMIC(CInputFreeBIRHandleDlg)

public:
	BIRHandleArray m_BIRHandleList;
	int m_SelectedIndex;

public:
	CInputFreeBIRHandleDlg(CWnd* pParent = NULL);   // standard constructor
	virtual ~CInputFreeBIRHandleDlg();

// Dialog Data
	enum { IDD = IDD_DIALOG_FREEBIRHANDLE };

protected:
	virtual void DoDataExchange(CDataExchange* pDX);    // DDX/DDV support

	DECLARE_MESSAGE_MAP()
public:
	virtual BOOL OnInitDialog();
	CListBox m_BIRIDList;
	afx_msg void OnBnClickedOk();
};
