// BioAPI_TestDlg.cpp
//

#include "stdafx.h"
#include "BioAPI_Test.h"
#include "BioAPI_TestDlg.h"
#include ".\bioapi_testdlg.h"
#include "EventHandler.h"
#include "Define.h"
#include "InputBSPLoadDlg.h"
#include "InputBSPAttachDlg.h"
#include "InputControlUnitDlg.h"
#include "InputCaptureDlg.h"
#include "InputFreeBIRHandleDlg.h"
#include "InputGetBIRFromHandleDlg.h"
#include "InputGetHeaderFromHandleDlg.h"
#include "InputCreateTemplateDlg.h"
#include "InputSetGUICallbacksDlg.h"
#include "InputVerifyMatchDlg.h"
#include "InputVerifyDlg.h"
#include "InputFreeDlg.h"
#include "InputIdentifyMatchDlg.h"
#include "InputIdentifyDlg.h"
#include "deleteParameter.h"
#include "verifyBSP.h"
#include "Challenge.h"
#include "InitCertificate.h"

#ifdef _DEBUG
#define new DEBUG_NEW
#endif

// CAboutDlg dialog used for App About

class CAboutDlg : public CDialog
{
public:
	CAboutDlg();

// Dialog Data
	enum { IDD = IDD_ABOUTBOX };

	protected:
	virtual void DoDataExchange(CDataExchange* pDX);    // DDX/DDV support

// Implementation
protected:
	DECLARE_MESSAGE_MAP()
};

CAboutDlg::CAboutDlg() : CDialog(CAboutDlg::IDD)
{
}

void CAboutDlg::DoDataExchange(CDataExchange* pDX)
{
	CDialog::DoDataExchange(pDX);
}

BEGIN_MESSAGE_MAP(CAboutDlg, CDialog)
END_MESSAGE_MAP()


// CBioAPI_TestDlg dialog



CBioAPI_TestDlg::CBioAPI_TestDlg(CWnd* pParent /*=NULL*/)
	: CDialog(CBioAPI_TestDlg::IDD, pParent)
{
	m_BSPSchemaList = NULL ;
	m_NumBSP = 0 ;
	m_UnitSchemaList = NULL ;
	m_BioAPIVersion = 0x20 ;
	certBIR.BiometricData.Data = NULL;
	certBIR.BiometricData.Length = 0;
	certBIR.SecurityBlock.Data = NULL;
	certBIR.SecurityBlock.Length = 0;

	TCHAR szFullPath[ _MAX_PATH ] ;
	TCHAR szFileName[_MAX_PATH];
	GetModuleFileName( NULL, szFullPath, sizeof( szFullPath ) ) ;
	_splitpath( szFullPath, NULL, NULL, szFileName, NULL ) ;
	m_appName = szFileName ;
	
	m_FMR = SAMPLE_APP_FMR_DEFAULT ;
	m_FileBIRNum = 0 ;
	m_BSPHandle = 0 ;
	m_hIcon = AfxGetApp()->LoadIcon(IDR_MAINFRAME);

	m_GuiStreamingCallbackCtx = 1 ;
	m_GuiStateCallbackCtx = 2 ;

}

void CBioAPI_TestDlg::DoDataExchange(CDataExchange* pDX)
{
	CDialog::DoDataExchange(pDX);
}

BEGIN_MESSAGE_MAP(CBioAPI_TestDlg, CDialog)
	ON_WM_SYSCOMMAND()
	ON_WM_PAINT()
	ON_WM_QUERYDRAGICON()
	//}}AFX_MSG_MAP
	ON_BN_CLICKED(IDC_BUTTON_INIT, OnBnClickedButtonInit)
	ON_BN_CLICKED(IDC_BUTTON_TERMINATE, OnBnClickedButtonTerminate)
	ON_BN_CLICKED(IDC_BUTTON_GET_FRAMEWORK_INFO, OnBnClickedButtonGetFrameworkInfo)
	ON_BN_CLICKED(IDC_BUTTON_ENUM_BSPS, OnBnClickedButtonEnumBsps)
	ON_BN_CLICKED(IDC_BUTTON_BSP_LOAD, OnBnClickedButtonBspLoad)
	ON_BN_CLICKED(IDC_BUTTON_BSP_UNLOAD, OnBnClickedButtonBspUnload)
	ON_BN_CLICKED(IDC_BUTTON_BSP_ATTACH, OnBnClickedButtonBspAttach)
	ON_BN_CLICKED(IDC_BUTTON_BSP_DETACH, OnBnClickedButtonBspDetach)
	ON_BN_CLICKED(IDC_BUTTON_QUERY_UNITS, OnBnClickedButtonQueryUnits)
	ON_BN_CLICKED(IDC_BUTTON_CONTROL_UNIT, OnBnClickedButtonControlUnit)
	ON_BN_CLICKED(IDC_BUTTON_FREE_BIR_HANDLE, OnBnClickedButtonFreeBirHandle)
	ON_BN_CLICKED(IDC_BUTTON_GET_BIR_FROM_HANDLE, OnBnClickedButtonGetBirFromHandle)
	ON_BN_CLICKED(IDC_BUTTON_GET_HEADER_FROM_HANDLE, OnBnClickedButtonGetHeaderFromHandle)
	ON_BN_CLICKED(IDC_BUTTON_ENABLE_EVENTS, OnBnClickedButtonEnableEvents)
	ON_BN_CLICKED(IDC_BUTTON_SET_GUI_CALLBACKS, OnBnClickedButtonSetGuiCallbacks)
	ON_BN_CLICKED(IDC_BUTTON_CAPTURE, OnBnClickedButtonCapture)
	ON_BN_CLICKED(IDC_BUTTON_CREATE_TEMPLATE, OnBnClickedButtonCreateTemplate)
	ON_BN_CLICKED(IDC_BUTTON_PROCESS, OnBnClickedButtonProcess)
	ON_BN_CLICKED(IDC_BUTTON_VERIFY_MATCH, OnBnClickedButtonVerifyMatch)
	ON_BN_CLICKED(IDC_BUTTON_IDENTIFY_MATCH, OnBnClickedButtonIdentifyMatch)
	ON_BN_CLICKED(IDC_BUTTON_ENROLL, OnBnClickedButtonEnroll)
	ON_BN_CLICKED(IDC_BUTTON_VERIFY, OnBnClickedButtonVerify)
	ON_BN_CLICKED(IDC_BUTTON_IDENTIFY, OnBnClickedButtonIdentify)
	ON_BN_CLICKED(IDC_BUTTON_CANCEL, OnBnClickedButtonCancel)
	ON_BN_CLICKED(IDC_BUTTON_FREE, OnBnClickedButtonFree)
	ON_BN_CLICKED(IDC_BUTTON_ATTACH_PROCESS, OnBnClickedButtonAttachProcess)
	ON_BN_CLICKED(IDC_DIGITALSIGNBIR, OnBnClickedDigitalsignbir)
	ON_BN_CLICKED(IDC_DELETEPARAMETER, OnBnClickedDeleteparameter)
	ON_BN_CLICKED(IDC_VERIFYBSP, OnBnClickedVerifybsp)
	ON_BN_CLICKED(IDC_VERIFYCHALLENGE, OnBnClickedVerifychallenge)
	ON_BN_CLICKED(IDC_IDENTIFYCHALLENGE, OnBnClickedIdentifychallenge)
	ON_BN_CLICKED(IDC_VERIFYBIR, OnBnClickedVerifybir)
	ON_BN_CLICKED(IDC_DIGITALSIGNBIR2, OnBnClickedDigitalsignbir2)
END_MESSAGE_MAP()


// CBioAPI_TestDlg message handlers

BOOL CBioAPI_TestDlg::OnInitDialog()
{
	CDialog::OnInitDialog();

	// Add "About..." menu item to system menu.

	// IDM_ABOUTBOX must be in the system command range.
	ASSERT((IDM_ABOUTBOX & 0xFFF0) == IDM_ABOUTBOX);
	ASSERT(IDM_ABOUTBOX < 0xF000);

	CMenu* pSysMenu = GetSystemMenu(FALSE);
	if (pSysMenu != NULL)
	{
		CString strAboutMenu;
		strAboutMenu.LoadString(IDS_ABOUTBOX);
		if (!strAboutMenu.IsEmpty())
		{
			pSysMenu->AppendMenu(MF_SEPARATOR);
			pSysMenu->AppendMenu(MF_STRING, IDM_ABOUTBOX, strAboutMenu);
		}
	}

	// Set the icon for this dialog.  The framework does this automatically
	//  when the application's main window is not a dialog
	SetIcon(m_hIcon, TRUE);			// Set big icon
	SetIcon(m_hIcon, FALSE);		// Set small icon

	// TODO: Add extra initialization here

	for ( ; ; ) {
		char szFileName[ 128 ] ;
		sprintf( szFileName, "WORK%d.BIR", m_FileBIRNum ) ;

		HANDLE hFile = CreateFile( szFileName, GENERIC_READ, 0, NULL, OPEN_EXISTING, 0, NULL ) ;
		if ( hFile == INVALID_HANDLE_VALUE ) {
			break ;
		}
		BioAPI_BIR BIR ;
		memset( &BIR, 0, sizeof( BIR ) ) ;

		DWORD dwReadSz ;
		ReadFile( hFile, &( BIR.Header ), sizeof( BIR.Header ), &dwReadSz, NULL ) ;

		ReadFile( hFile, &( BIR.BiometricData.Length ), sizeof( BIR.BiometricData.Length ), &dwReadSz, NULL ) ;
		BIR.BiometricData.Data = new BYTE[ BIR.BiometricData.Length ] ;
		ReadFile( hFile, BIR.BiometricData.Data, BIR.BiometricData.Length , &dwReadSz, NULL ) ;

		ReadFile( hFile, &( BIR.SecurityBlock.Length ), sizeof( BIR.SecurityBlock.Length ), &dwReadSz, NULL ) ;
		if ( BIR.SecurityBlock.Length != 0 ) {
			BIR.SecurityBlock.Data = new BYTE[ BIR.SecurityBlock.Length ] ;
			ReadFile( hFile, BIR.SecurityBlock.Data, BIR.SecurityBlock.Length , &dwReadSz, NULL ) ;
		}

		CloseHandle( hFile ) ;
		m_BIRList.push_back(BIR);
		m_FileBIRNum++ ;
		certBIR.Header = BIR.Header ;
		certBIR.BiometricData.Data = BIR.BiometricData.Data ;
		certBIR.BiometricData.Length = BIR.BiometricData.Length ;
	}

	return TRUE;  // return TRUE  unless you set the focus to a control
}
	
CBioAPI_TestDlg::~CBioAPI_TestDlg()
{
	for ( BIRArray::iterator e = m_BIRList.begin() ; e != m_BIRList.end() ; e++ ) {
		freeBIR( &(*e) ) ;
	}

	for (BIRHandleArray::iterator e = m_BIRHandleList.begin(); e != m_BIRHandleList.end(); e++) {
		if ( e->Purpose != 0 ) {
			BioAPI_FreeBIRHandle( m_BSPHandle, e->Handle ) ;
		}
	}

	BioAPI_Free(m_UnitSchemaList);

	freeBSPSchemaList( &m_BSPSchemaList, &m_NumBSP ) ;

	return ;
}

void CBioAPI_TestDlg::OnSysCommand(UINT nID, LPARAM lParam)
{
	if ((nID & 0xFFF0) == IDM_ABOUTBOX)
	{
		CAboutDlg dlgAbout;
		dlgAbout.DoModal();
	}
	else
	{
		CDialog::OnSysCommand(nID, lParam);
	}
}

// If you add a minimize button to your dialog, you will need the code below
//  to draw the icon.  For MFC applications using the document/view model,
//  this is automatically done for you by the framework.

void CBioAPI_TestDlg::OnPaint() 
{
	if (IsIconic())
	{
		CPaintDC dc(this); // device context for painting

		SendMessage(WM_ICONERASEBKGND, reinterpret_cast<WPARAM>(dc.GetSafeHdc()), 0);

		// Center icon in client rectangleZ
		int cxIcon = GetSystemMetrics(SM_CXICON);
		int cyIcon = GetSystemMetrics(SM_CYICON);
		CRect rect;
		GetClientRect(&rect);
		int x = (rect.Width() - cxIcon + 1) / 2;
		int y = (rect.Height() - cyIcon + 1) / 2;

		// Draw the icon
		dc.DrawIcon(x, y, m_hIcon);
	}
	else
	{
		CDialog::OnPaint();
	}
}

// The system calls this function to obtain the cursor to display while the user drags
//  the minimized window.
HCURSOR CBioAPI_TestDlg::OnQueryDragIcon()
{
	return static_cast<HCURSOR>(m_hIcon);
}

void CBioAPI_TestDlg::OnBnClickedButtonInit()
{
	
	char msg[256];

	// BioAPI_Init
	BioAPI_RETURN rc = BioAPI_Init(m_BioAPIVersion);
	
	if (rc != BioAPI_OK) {
		sprintf(msg, "BioAPI_Init failed.(0x%08x)", rc);
		MessageBox(msg);
		return;
	}

	sprintf(msg, "BioAPI_Init successful.");
	MessageBox(msg);

}

void CBioAPI_TestDlg::OnBnClickedButtonTerminate()
{
	
	// BioAPI_Init
	char msg[256];

	BioAPI_RETURN rc = BioAPI_Terminate();
	
	if (rc != BioAPI_OK) {
		sprintf(msg, "BioAPI_Terminate failed.(0x%08x)", rc);
		MessageBox(msg);
		return;
	}

	sprintf(msg, "BioAPI_Terminate successful.");
	MessageBox(msg);
}

void CBioAPI_TestDlg::OnBnClickedButtonGetFrameworkInfo()
{
	
	char msg[256];

	BioAPI_RETURN rc = BioAPI_GetFrameworkInfo(&m_FrameworkSchema);
	if (rc != BioAPI_OK) {
		sprintf(msg, "BioAPI_GetFrameworkInfo failed.(0x%08x)", rc);
		MessageBox(msg);
		return;
	}

	sprintf(msg, "BioAPI_GetFrameworkInfo successful.\n"
				 "FrameworkUuid=%02x%02x%02x%02x-%02x%02x-%02x%02x-%02x%02x-%02x%02x%02x%02x%02x%02x\n"
				 "FwDescription=%s\n"
				 "Path=%s\n"
				 "SpecVersion=0x%08x\n"
				 "ProductVersion=%s\n"
				 "Vendor=%s\n",
			m_FrameworkSchema.FrameworkUuid[ 0 ],
			m_FrameworkSchema.FrameworkUuid[ 1 ],
			m_FrameworkSchema.FrameworkUuid[ 2 ],
			m_FrameworkSchema.FrameworkUuid[ 3 ],
			m_FrameworkSchema.FrameworkUuid[ 4 ],
			m_FrameworkSchema.FrameworkUuid[ 5 ],
			m_FrameworkSchema.FrameworkUuid[ 6 ],
			m_FrameworkSchema.FrameworkUuid[ 7 ],
			m_FrameworkSchema.FrameworkUuid[ 8 ],
			m_FrameworkSchema.FrameworkUuid[ 9 ],
			m_FrameworkSchema.FrameworkUuid[ 10 ],
			m_FrameworkSchema.FrameworkUuid[ 11 ],
			m_FrameworkSchema.FrameworkUuid[ 12 ],
			m_FrameworkSchema.FrameworkUuid[ 13 ],
			m_FrameworkSchema.FrameworkUuid[ 14 ],
			m_FrameworkSchema.FrameworkUuid[ 15 ],
			m_FrameworkSchema.FwDescription,
			m_FrameworkSchema.Path,
			m_FrameworkSchema.SpecVersion,
			m_FrameworkSchema.ProductVersion,
			m_FrameworkSchema.Vendor
			);
	MessageBox(msg);

	
	BioAPI_Free( m_FrameworkSchema.Path ) ;
	m_FrameworkSchema.Path = NULL ;
}

void CBioAPI_TestDlg::OnBnClickedButtonEnumBsps()
{
	
	char msg[256], temp[256];
	char BSPName[256];
	uint32_t i;

	
	m_BSPNameList.clear();
	freeBSPSchemaList( &m_BSPSchemaList, &m_NumBSP ) ;

	BioAPI_RETURN rc = BioAPI_EnumBSPs(&m_BSPSchemaList, &m_NumBSP);
	if (rc != BioAPI_OK) {
		sprintf(msg, "BioAPI_EnumBSPs failed.(0x%08x)\n", rc);
		MessageBox(msg);
		return;
	}

	sprintf(msg, "BioAPI_EnumBSPs successful.\n");

	for (i = 0; i < m_NumBSP; i++) {
		sprintf(BSPName, "%d : %s", i, m_BSPSchemaList[i].BSPDescription);
		m_BSPNameList.push_back(BSPName);
		sprintf(temp, "%s\n", BSPName);
		strcat(msg, temp);
	}

	MessageBox(msg);
}

void CBioAPI_TestDlg::OnBnClickedButtonBspLoad()
{
	
	char msg[256];

	CInputBSPLoadDlg BSPLoadDlg;

	BSPLoadDlg.SetBSPList(m_BSPNameList);

	if (BSPLoadDlg.DoModal() == IDCANCEL) {
		return;
	}

	m_nCrtBSPNum = BSPLoadDlg.GetBSPNum();

	if (BSPLoadDlg.GetEventOnOff()) {
		m_EventHandler = NULL;
		m_EventHandlerCxt = NULL;
	} else {
		m_EventHandler = EventHandler;
		m_EventHandlerCxt = m_hWnd;
	}

	BioAPI_RETURN rc = BioAPI_BSPLoad(&m_BSPSchemaList[m_nCrtBSPNum].BSPUuid, m_EventHandler, m_EventHandlerCxt);
	if (rc != BioAPI_OK) {
		sprintf(msg, "BioAPI_BSPLoad failed.(0x%08x)", rc);
		MessageBox(msg);
		return;
	}

	sprintf(msg, "BioAPI_BSPLoad successful.");
	MessageBox(msg);
}

void CBioAPI_TestDlg::OnBnClickedButtonBspUnload()
{
	
	char msg[256];

	BioAPI_RETURN rc = BioAPI_BSPUnload(&m_BSPSchemaList[m_nCrtBSPNum].BSPUuid, m_EventHandler, m_EventHandlerCxt);
	if (rc != BioAPI_OK) {
		sprintf(msg, "BioAPI_BSPUnload failed.(0x%08x)", rc);
		MessageBox(msg);
		return;
	}

	sprintf(msg, "BioAPI_BSPUnload successful.");
	MessageBox(msg);
}

void CBioAPI_TestDlg::OnBnClickedButtonBspAttach()
{
	
	char msg[256];
	BioAPI_UNIT_LIST_ELEMENT UnitList[4];
	int SelectedUnitList[4], nSelectedUnitNum;
	int i;

	CInputBSPAttachDlg BSPAttachDlg;
	BSPAttachDlg.SetUnitIDList(m_UnitIDList);

	if (BSPAttachDlg.DoModal() == IDCANCEL) {
		return;
	}

	nSelectedUnitNum = BSPAttachDlg.GetSelectedUnitNumList(SelectedUnitList);

	for (i = 0; i < nSelectedUnitNum; i++) {
		UnitList[i].UnitCategory = m_UnitSchemaList[SelectedUnitList[i]].UnitCategory;
		UnitList[i].UnitId = m_UnitSchemaList[SelectedUnitList[i]].UnitId;
	}

	BioAPI_RETURN rc = BioAPI_BSPAttach(&m_BSPSchemaList[m_nCrtBSPNum].BSPUuid,
		m_BioAPIVersion,
		UnitList,
		nSelectedUnitNum,
		&m_BSPHandle);

	if (rc != BioAPI_OK) {
		sprintf(msg, "BioAPI_BSPAttach failed.(0x%08x)", rc);
		MessageBox(msg);
		return;
	}

	sprintf(msg, "BioAPI_BSPAttach successful.\n(BSPHandle = %x)", m_BSPHandle);
	MessageBox(msg);
}

DWORD WINAPI execBnClickedButtonBspDetach(LPVOID lpParameter)
{
	((CBioAPI_TestDlg*)lpParameter)->doBspDetach();
	return 0;
}

void CBioAPI_TestDlg::OnBnClickedButtonBspDetach()
{
	HANDLE hDlgThread = CreateThread(NULL, NULL, execBnClickedButtonBspDetach, (LPVOID)this, NULL, NULL);
	CloseHandle( hDlgThread ) ;
}

void CBioAPI_TestDlg::doBspDetach()
{
	
	char msg[256];

	BioAPI_RETURN rc = BioAPI_BSPDetach(m_BSPHandle);

	if (rc != BioAPI_OK) {
		sprintf(msg, "BioAPI_BSPDetach failed.(0x%08x)", rc);
		MessageBox(msg);
		return;
	}

	sprintf(msg, "BioAPI_BSPDetach successful.");
	MessageBox(msg);
}

void CBioAPI_TestDlg::OnBnClickedButtonQueryUnits()
{
	
	char msg[256], temp[256];
	char UnitID[256];
	uint32_t i, uNumUnits;

	
	m_UnitIDList.clear();
	BioAPI_Free(m_UnitSchemaList);
	m_UnitSchemaList = NULL ;

	BioAPI_RETURN rc = BioAPI_QueryUnits(&m_BSPSchemaList[m_nCrtBSPNum].BSPUuid, &m_UnitSchemaList, &uNumUnits);
	if (rc != BioAPI_OK) {
		sprintf(msg, "BioAPI_QueryUnits failed.(0x%08x)", rc);
		MessageBox(msg);
		return;
	}

	sprintf(msg, "BioAPI_QueryUnits successful.");
	for (i = 0; i < uNumUnits; i++) {
		switch (m_UnitSchemaList[i].UnitCategory) {
			case BioAPI_CATEGORY_SENSOR :
				sprintf(UnitID, "Sensor Unit (ID = %d)", m_UnitSchemaList[i].UnitId);
				break;
			case BioAPI_CATEGORY_ARCHIVE :
				sprintf(UnitID, "Archive Unit (ID = %d)", m_UnitSchemaList[i].UnitId);
				break;
			case BioAPI_CATEGORY_PROCESSING_ALG :
				sprintf(UnitID, "Processing Unit (ID = %d)", m_UnitSchemaList[i].UnitId);
				break;
			case BioAPI_CATEGORY_MATCHING_ALG :
				sprintf(UnitID, "Matching Unit (ID = %d)", m_UnitSchemaList[i].UnitId);
				break;

		}
		m_UnitIDList.push_back(UnitID);
		sprintf(temp, "%s\n", UnitID);
		strcat(msg, temp);
	}

	MessageBox(msg);
}

DWORD WINAPI execBnClickedButtonControlUnit(LPVOID lpParameter)
{
	
	((CBioAPI_TestDlg*)lpParameter)->doControlUnit() ;
	return 0;
}

void CBioAPI_TestDlg::OnBnClickedButtonControlUnit()
{
	HANDLE hDlgThread = CreateThread(NULL, NULL, execBnClickedButtonControlUnit, (LPVOID)this, NULL, NULL);
	CloseHandle( hDlgThread ) ;
}

void CBioAPI_TestDlg::doControlUnit()
{
	
	char msg[256];
	CInputControlUnitDlg ControlUnitDlg;
	BioAPI_UNIT_ID nUnitID;
	uint32_t nControlCode;
	int nInputData[4];
	uint32_t nInputDataSize;
	BioAPI_DATA InputData, OutputData = {0, NULL};

	if ( m_UnitSchemaList != NULL ) {
		ControlUnitDlg.m_nUnitID = m_UnitSchemaList[0].UnitId ;
	}
	if (ControlUnitDlg.DoModal() == IDCANCEL) {
		return;
	}

	nUnitID = ControlUnitDlg.m_nUnitID;
	nControlCode = ControlUnitDlg.m_nControlCode;
	nInputData[0] = ControlUnitDlg.m_nInputData0;
	nInputData[1] = ControlUnitDlg.m_nInputData1;
	nInputData[2] = ControlUnitDlg.m_nInputData2;
	nInputData[3] = ControlUnitDlg.m_nInputData3;
	nInputDataSize = ControlUnitDlg.m_nInputDataSize;

	InputData.Length = nInputDataSize;
	if (nInputDataSize > 0) {
		InputData.Data = nInputData ;
	} else {
		InputData.Data = NULL;
	}

	BioAPI_RETURN rc = BioAPI_ControlUnit(m_BSPHandle, nUnitID, nControlCode, &InputData, &OutputData);

	if (rc != BioAPI_OK) {
		sprintf(msg, "BioAPI_ControlUnit failed.(0x%08x)", rc);
		MessageBox(msg);
		return;
	}

	sprintf(msg, "BioAPI_ControlUnit successful.");
	MessageBox(msg);

	BioAPI_Free( OutputData.Data ) ;
}

DWORD WINAPI execBnClickedButtonFreeBirHandle(LPVOID lpParameter)
{
	((CBioAPI_TestDlg*)lpParameter)->doFreeBirHandle();
	return 0;
}

void CBioAPI_TestDlg::OnBnClickedButtonFreeBirHandle()
{
	HANDLE hDlgThread = CreateThread(NULL, NULL, execBnClickedButtonFreeBirHandle, (LPVOID)this, NULL, NULL);
	CloseHandle( hDlgThread ) ;
}

void CBioAPI_TestDlg::doFreeBirHandle()
{
	
	char msg[256];
	CInputFreeBIRHandleDlg FreeBIRHandleDlg;
	int nIndex;
	BioAPI_BIR_HANDLE TargetBIRHandle;

	FreeBIRHandleDlg.m_BIRHandleList = m_BIRHandleList;
	if (FreeBIRHandleDlg.DoModal() == IDCANCEL) {
		return;
	}
	nIndex = FreeBIRHandleDlg.m_SelectedIndex;
	TargetBIRHandle = m_BIRHandleList[nIndex].Handle;

	BioAPI_RETURN rc = BioAPI_FreeBIRHandle(m_BSPHandle, TargetBIRHandle);

	if (rc != BioAPI_OK) {
		sprintf(msg, "BioAPI_FreeBIRHandle failed.(0x%08x)", rc);
		MessageBox(msg);
		return;
	}

	
	m_BIRHandleList[nIndex].Purpose = 0;
	m_BIRHandleList[nIndex].Type = 0;

	sprintf(msg, "BioAPI_FreeBIRHandle successful.");
	MessageBox(msg);
}

DWORD WINAPI execBnClickedButtonGetBirFromHandle(LPVOID lpParameter)
{
	((CBioAPI_TestDlg*)lpParameter)->doGetBirFromHandle();
	return 0;
}

void CBioAPI_TestDlg::OnBnClickedButtonGetBirFromHandle()
{
	HANDLE hDlgThread = CreateThread(NULL, NULL, execBnClickedButtonGetBirFromHandle, (LPVOID)this, NULL, NULL);
	CloseHandle( hDlgThread ) ;
}

void CBioAPI_TestDlg::doGetBirFromHandle()
{
	
	char msg[256];
	CInputGetBIRFromHandleDlg GetBIRFromHandleDlg;
	int nIndex;
	BioAPI_BIR_HANDLE TargetBIRHandle;
	BioAPI_BIR BIRfromH ;
	BioAPI_BIR BIR;

	GetBIRFromHandleDlg.m_BIRHandleList = m_BIRHandleList;
	if (GetBIRFromHandleDlg.DoModal() == IDCANCEL) {
		return;
	}
	nIndex = GetBIRFromHandleDlg.m_SelectedIndex;
	TargetBIRHandle = m_BIRHandleList[nIndex].Handle;

	BioAPI_RETURN rc = BioAPI_GetBIRFromHandle( m_BSPHandle, TargetBIRHandle, &BIRfromH ) ;

	if (rc != BioAPI_OK) {
		sprintf(msg, "BioAPI_GetBIRFromHandle failed.(0x%08x)", rc);
		MessageBox(msg);
		return;
	}

	duplicateBIR( &BIR, &BIRfromH ) ;
	BioAPI_Free( BIRfromH.BiometricData.Data ) ;
	BioAPI_Free( BIRfromH.SecurityBlock.Data ) ;
	m_BIRList.push_back( BIR ) ;

	
	m_BIRHandleList[nIndex].Purpose = 0;
	m_BIRHandleList[nIndex].Type = 0;

	char szFileName[ 128 ] ;
	sprintf( szFileName, "WORK%d.BIR", m_FileBIRNum ) ;
	HANDLE hFile = CreateFile( szFileName, GENERIC_WRITE, 0, NULL, CREATE_NEW, 0, NULL ) ;
	if ( hFile != INVALID_HANDLE_VALUE ) {
		DWORD dwBytes ;
		WriteFile( hFile, &( BIR.Header ), sizeof( BIR.Header ), &dwBytes, NULL ) ;

		WriteFile( hFile, &( BIR.BiometricData.Length ), sizeof( BIR.BiometricData.Length ), &dwBytes, NULL ) ;
		WriteFile( hFile, BIR.BiometricData.Data, BIR.BiometricData.Length, &dwBytes, NULL ) ;

		WriteFile( hFile, &( BIR.SecurityBlock.Length ), sizeof( BIR.SecurityBlock.Length ), &dwBytes, NULL ) ;
		WriteFile( hFile, BIR.SecurityBlock.Data, BIR.SecurityBlock.Length, &dwBytes, NULL ) ;

		CloseHandle( hFile ) ;
		m_FileBIRNum++ ;
	}

	sprintf(msg, "BioAPI_GetBIRFromHandle successful.");
	MessageBox(msg);
}

DWORD WINAPI execBnClickedButtonGetHeaderFromHandle(LPVOID lpParameter)
{
	((CBioAPI_TestDlg*)lpParameter)->doGetHeaderFromHandle();
	return 0;
}

void CBioAPI_TestDlg::OnBnClickedButtonGetHeaderFromHandle()
{
	HANDLE hDlgThread = CreateThread(NULL, NULL, execBnClickedButtonGetHeaderFromHandle, (LPVOID)this, NULL, NULL);
	CloseHandle( hDlgThread ) ;
}

void CBioAPI_TestDlg::doGetHeaderFromHandle()
{
	
	char msg[256];
	CInputGetHeaderFromHandleDlg GetHeaderFromHandleDlg;
	int nIndex;
	BioAPI_BIR_HANDLE TargetBIRHandle;
	BioAPI_BIR_HEADER BIRHeader;

	GetHeaderFromHandleDlg.m_BIRHandleList = m_BIRHandleList;
	if (GetHeaderFromHandleDlg.DoModal() == IDCANCEL) {
		return;
	}
	nIndex = GetHeaderFromHandleDlg.m_SelectedIndex;
	TargetBIRHandle = m_BIRHandleList[nIndex].Handle;

	BioAPI_RETURN rc = BioAPI_GetHeaderFromHandle(m_BSPHandle, TargetBIRHandle, &BIRHeader);

	if (rc != BioAPI_OK) {
		sprintf(msg, "BioAPI_GetHeaderFromHandle failed.(0x%08x)", rc);
		MessageBox(msg);
		return;
	}

	sprintf(msg, "BioAPI_GetHeaderFromHandle successful.\nPurpose = %d, Type = %d", BIRHeader.Purpose, BIRHeader.Type);
	MessageBox(msg);
}

void CBioAPI_TestDlg::OnBnClickedButtonEnableEvents()
{
	
	char msg[256];
	BioAPI_EVENT_MASK EventMask = 0;

	BioAPI_RETURN rc = BioAPI_EnableEvents(m_BSPHandle, EventMask);

	if (rc != BioAPI_OK) {
		sprintf(msg, "BioAPI_EnableEvents failed.(0x%08x)", rc);
		MessageBox(msg);
		return;
	}

	sprintf(msg, "BioAPI_EnableEvents successful.");
	MessageBox(msg);
}

DWORD WINAPI execBnClickedButtonSetGuiCallbacks(LPVOID lpParameter)
{
	((CBioAPI_TestDlg*)lpParameter)->doSetGuiCallbacks();
	return 0;
}

void CBioAPI_TestDlg::OnBnClickedButtonSetGuiCallbacks()
{
	HANDLE hDlgThread = CreateThread(NULL, NULL, execBnClickedButtonSetGuiCallbacks, (LPVOID)this, NULL, NULL);
	CloseHandle( hDlgThread ) ;
}

void CBioAPI_TestDlg::doSetGuiCallbacks()
{
	
	char msg[256];
	BioAPI_GUI_STATE_CALLBACK pGuiStateCallback = NULL;
	BioAPI_GUI_STREAMING_CALLBACK pGuiStreamingCallback = NULL;

	CInputSetGUICallbacksDlg SetGUICallbacksDlg;

	if (SetGUICallbacksDlg.DoModal() == IDCANCEL) {
		return;
	}

	if (SetGUICallbacksDlg.m_UseStateCallback == 1) {
		pGuiStateCallback = GuiStateCallback;
	}
	if (SetGUICallbacksDlg.m_UseStreamingCallback == 1) {
		pGuiStreamingCallback = GuiStreamingCallback;
	}

	BioAPI_RETURN rc = BioAPI_SetGUICallbacks(m_BSPHandle, pGuiStreamingCallback, &m_GuiStreamingCallbackCtx, 
															pGuiStateCallback, &m_GuiStateCallbackCtx);

	if (rc != BioAPI_OK) {
		sprintf(msg, "BioAPI_SetGuiCallbacks failed.(0x%08x)", rc);
		MessageBox(msg);
		return;
	}

	sprintf(msg, "BioAPI_SetGuiCallbacks successful.");
	MessageBox(msg);

}

DWORD WINAPI execBnClickedButtonCapture(LPVOID lpParameter)
{
	((CBioAPI_TestDlg*)lpParameter)->doCapture();
	return 0;
}

void CBioAPI_TestDlg::OnBnClickedButtonCapture()
{
	HANDLE hDlgThread = CreateThread(NULL, NULL, execBnClickedButtonCapture, (LPVOID)this, NULL, NULL);
	CloseHandle( hDlgThread ) ;
}

void CBioAPI_TestDlg::doCapture()
{
	
	char msg[256];

	BioAPI_BIR_PURPOSE Purpose;
	BioAPI_BIR_HANDLE CapturedBIRHandle;
	int32_t Timeout;
	BIR_HANDLE_INFO BIRInfo;

	CInputCaptureDlg CaptureDlg;

	CaptureDlg.m_nPurpose = BioAPI_PURPOSE_ENROLL ;
	CaptureDlg.m_nTimeout = SAMPLE_APP_TIMEOUT_DEFAULT ;

	if (CaptureDlg.DoModal() == IDCANCEL) {
		return;
	}

	if ( CaptureDlg.m_SelectedIndex == 0 ) {
		Purpose = BioAPI_PURPOSE_VERIFY ;
	} else if ( CaptureDlg.m_SelectedIndex == 1 ) {
		Purpose = BioAPI_PURPOSE_IDENTIFY;
	} else {
		Purpose = BioAPI_PURPOSE_ENROLL;
	}
	Timeout = CaptureDlg.m_nTimeout;

	DWORD callTime = GetTickCount();
	BioAPI_RETURN rc = BioAPI_Capture(m_BSPHandle, Purpose, BioAPI_NO_SUBTYPE_AVAILABLE, 
						NULL, &CapturedBIRHandle, Timeout, NULL ) ;
	callTime = GetTickCount() - callTime;

	if (rc != BioAPI_OK) {
		sprintf(msg, "BioAPI_Capture failed.(0x%08x) [Latency of API:%ld msec]", rc, callTime);
		MessageBox(msg);
		return;
	}

	BioAPI_BIR_HEADER Header;
	rc = BioAPI_GetHeaderFromHandle(m_BSPHandle, CapturedBIRHandle, &Header);
	if (rc != BioAPI_OK) {
		sprintf(msg, "The error occurred.");
		MessageBox(msg);
		return;
	}

	BIRInfo.Handle = CapturedBIRHandle;
	BIRInfo.Purpose = Header.Purpose;
	BIRInfo.Type = Header.Type;
	m_BIRHandleList.push_back(BIRInfo);

	sprintf(msg, "BioAPI_Capture successful. [Latency of API:%ld msec]", callTime);
	MessageBox(msg);
}

DWORD WINAPI execBnClickedButtonCreateTemplate(LPVOID lpParameter)
{
	((CBioAPI_TestDlg*)lpParameter)->doCreateTemplate();
	return 0;
}

void CBioAPI_TestDlg::OnBnClickedButtonCreateTemplate()
{
	HANDLE hDlgThread = CreateThread(NULL, NULL, execBnClickedButtonCreateTemplate, (LPVOID)this, NULL, NULL);
	CloseHandle( hDlgThread ) ;
}

void CBioAPI_TestDlg::doCreateTemplate()
{
	
	char msg[256];
	BioAPI_INPUT_BIR CapturedBIRInputData;
	BioAPI_BIR_HANDLE TemplateBIRHandle;
	BIR_HANDLE_INFO BIRInfo;

	CInputCreateTemplateDlg CreateTemplateDlg;

	CreateTemplateDlg.m_BIRHandleList = m_BIRHandleList;
	CreateTemplateDlg.m_BIRList = m_BIRList;

	if (CreateTemplateDlg.DoModal() == IDCANCEL) {
		return;
	}

	
	if (CreateTemplateDlg.m_InputDataKind == 0) {
		CapturedBIRInputData.Form = BioAPI_BIR_HANDLE_INPUT;
		CapturedBIRInputData.InputBIR.BIRinBSP = &( m_BIRHandleList[CreateTemplateDlg.m_SelectedIndex].Handle ) ;
	} else {
		CapturedBIRInputData.Form = BioAPI_FULLBIR_INPUT;
		CapturedBIRInputData.InputBIR.BIR = &( m_BIRList[ CreateTemplateDlg.m_SelectedIndex ] ) ;
	}

	DWORD callTime = GetTickCount();
	BioAPI_RETURN rc = BioAPI_CreateTemplate( m_BSPHandle, &CapturedBIRInputData, NULL,
		NULL, &TemplateBIRHandle, NULL, NULL ) ;
	callTime = GetTickCount() - callTime;

	if (rc != BioAPI_OK) {
		sprintf(msg, "BioAPI_CreateTemplate failed.(0x%08x) [Latency of API:%ld msec]", rc, callTime);
		MessageBox(msg);
		return;
	}

	BioAPI_BIR_HEADER Header;
	rc = BioAPI_GetHeaderFromHandle(m_BSPHandle, TemplateBIRHandle, &Header);
	if (rc != BioAPI_OK) {
		sprintf(msg, "The error occurred.");
		MessageBox(msg);
		return;
	}

	BIRInfo.Handle = TemplateBIRHandle;
	BIRInfo.Purpose = Header.Purpose;
	BIRInfo.Type = Header.Type;
	m_BIRHandleList.push_back(BIRInfo);

	sprintf(msg, "BioAPI_CreateTemplate successful. [Latency of API:%ld msec]", callTime);
	MessageBox(msg);

	return ;
}

DWORD WINAPI execBnClickedButtonProcess(LPVOID lpParameter)
{
	((CBioAPI_TestDlg*)lpParameter)->doProcess();
	return 0;
}

void CBioAPI_TestDlg::OnBnClickedButtonProcess()
{
	HANDLE hDlgThread = CreateThread(NULL, NULL, execBnClickedButtonProcess, (LPVOID)this, NULL, NULL);
	CloseHandle( hDlgThread ) ;
}

void CBioAPI_TestDlg::doProcess()
{
	
	char msg[256];
	BioAPI_INPUT_BIR CapturedBIRInputData;
	BioAPI_BIR_HANDLE ProcessedBIRHandle;
	BIR_HANDLE_INFO BIRInfo;

	CInputCreateTemplateDlg CreateTemplateDlg;

	CreateTemplateDlg.m_BIRHandleList = m_BIRHandleList;
	CreateTemplateDlg.m_BIRList = m_BIRList;

	if (CreateTemplateDlg.DoModal() == IDCANCEL) {
		return;
	}

	
	if (CreateTemplateDlg.m_InputDataKind == 0) {
		CapturedBIRInputData.Form = BioAPI_BIR_HANDLE_INPUT;
		CapturedBIRInputData.InputBIR.BIRinBSP = &( m_BIRHandleList[ CreateTemplateDlg.m_SelectedIndex ].Handle ) ;
	} else {
		CapturedBIRInputData.Form = BioAPI_FULLBIR_INPUT;
		CapturedBIRInputData.InputBIR.BIR = &( m_BIRList[ CreateTemplateDlg.m_SelectedIndex ] ) ;
	}

	DWORD callTime = GetTickCount();
	BioAPI_RETURN rc = BioAPI_Process( m_BSPHandle, &CapturedBIRInputData, NULL, &ProcessedBIRHandle ) ;
	callTime = GetTickCount() - callTime;

	if (rc != BioAPI_OK) {
		sprintf(msg, "BioAPI_Process failed.(0x%08x) [Latency of API:%ld msec]", rc, callTime);
		MessageBox(msg);
		return;
	}

	BioAPI_BIR_HEADER Header;
	rc = BioAPI_GetHeaderFromHandle(m_BSPHandle, ProcessedBIRHandle, &Header);
	if (rc != BioAPI_OK) {
		sprintf(msg, "The error occurred.");
		MessageBox(msg);
		return;
	}

	BIRInfo.Handle = ProcessedBIRHandle;
	BIRInfo.Purpose = Header.Purpose;
	BIRInfo.Type = Header.Type;
	m_BIRHandleList.push_back(BIRInfo);

	sprintf(msg, "BioAPI_Process successful. [Latency of API:%ld msec]", callTime);
	MessageBox(msg);

	return ;
}

DWORD WINAPI execBnClickedButtonVerifyMatch(LPVOID lpParameter)
{
	((CBioAPI_TestDlg*)lpParameter)->doVerifyMatch();
	return 0;
}

void CBioAPI_TestDlg::OnBnClickedButtonVerifyMatch()
{
	HANDLE hDlgThread = CreateThread(NULL, NULL, execBnClickedButtonVerifyMatch, (LPVOID)this, NULL, NULL);
	CloseHandle( hDlgThread ) ;
}

void CBioAPI_TestDlg::doVerifyMatch()
{
	
	char msg[256];

	BioAPI_FMR MaxFMRRequested;
	BioAPI_INPUT_BIR ProcessedBIRInputData;
	BioAPI_INPUT_BIR ReferenceTemplateInputData;
	BioAPI_BOOL Result;
	BioAPI_FMR FMRAchieved;

	CInputVerifyMatchDlg VerifyMatchDlg;

	VerifyMatchDlg.m_BIRHandleList = m_BIRHandleList;
	VerifyMatchDlg.m_BIRList = m_BIRList;
	VerifyMatchDlg.m_FMRThreshold = SAMPLE_APP_FMR_DEFAULT ;

	if (VerifyMatchDlg.DoModal() == IDCANCEL) {
		return;
	}

	MaxFMRRequested = VerifyMatchDlg.m_FMRThreshold;

	

	
	if (VerifyMatchDlg.m_BaseImageKind == 0) {
		
		ProcessedBIRInputData.Form = BioAPI_BIR_HANDLE_INPUT;
		ProcessedBIRInputData.InputBIR.BIRinBSP = &( m_BIRHandleList[ VerifyMatchDlg.m_SelectedBaseImage ].Handle ) ;
	} else {
		
		ProcessedBIRInputData.Form = BioAPI_FULLBIR_INPUT;
		ProcessedBIRInputData.InputBIR.BIR = &( m_BIRList[ VerifyMatchDlg.m_SelectedBaseImage ] ) ;
	}

	
	if (VerifyMatchDlg.m_ReferenceImageKind == 0) {
		
		ReferenceTemplateInputData.Form = BioAPI_BIR_HANDLE_INPUT;
		ReferenceTemplateInputData.InputBIR.BIRinBSP = &( m_BIRHandleList[ VerifyMatchDlg.m_SelectedReferenceImage ].Handle ) ;
	} else {
		
		ReferenceTemplateInputData.Form = BioAPI_FULLBIR_INPUT;
		ReferenceTemplateInputData.InputBIR.BIR = &( m_BIRList[ VerifyMatchDlg.m_SelectedReferenceImage ] ) ;
	}

	// VerifyMatch
	DWORD callTime = GetTickCount();
	BioAPI_RETURN rc = BioAPI_VerifyMatch(m_BSPHandle, MaxFMRRequested, 
		&ProcessedBIRInputData, &ReferenceTemplateInputData, NULL,
		&Result, &FMRAchieved, NULL ) ;
	callTime = GetTickCount() - callTime;

	if (rc != BioAPI_OK) {
		sprintf(msg, "BioAPI_VerifyMatch failed.(0x%08x) [Latency of API:%ld msec]", rc, callTime);
		MessageBox(msg);
		return;
	}

	sprintf(msg, "BioAPI_VerifyMatch successful. [Latency of API:%ld msec]\nResult of verification = %s, FMR = %d(%1.4f)", 
		callTime,
		(Result == BioAPI_FALSE) ? ("unmatched") : ("matched"), 
		FMRAchieved, FMRAchieved / (double)0x7FFFFFFF);
	MessageBox(msg);

	return ;
}

DWORD WINAPI execBnClickedButtonIdentifyMatch(LPVOID lpParameter)
{
	((CBioAPI_TestDlg*)lpParameter)->doIdentifyMatch();
	return 0;
}

void CBioAPI_TestDlg::OnBnClickedButtonIdentifyMatch()
{
	HANDLE hDlgThread = CreateThread(NULL, NULL, execBnClickedButtonIdentifyMatch, (LPVOID)this, NULL, NULL);
	CloseHandle( hDlgThread ) ;
}

void CBioAPI_TestDlg::doIdentifyMatch()
{
	
	char msg[256];

	BioAPI_FMR MaxFMRRequested;
	BioAPI_INPUT_BIR ProcessedBIRInputData;
	BioAPI_IDENTIFY_POPULATION Population;
	uint32_t TotalNumberOfTemplates;
	uint32_t MaxNumberOfResults;
	uint32_t NumberOfResults;
	BioAPI_CANDIDATE *Candidates;
	int32_t Timeout;
	unsigned int i ;

	CInputIdentifyMatchDlg IdentifyMatchDlg;

	IdentifyMatchDlg.m_FMR = m_FMR ;
	IdentifyMatchDlg.m_Timeout = SAMPLE_APP_TIMEOUT_DEFAULT ;
	IdentifyMatchDlg.m_BIRList = m_BIRList;
	IdentifyMatchDlg.m_BIRHandleList = m_BIRHandleList;
	IdentifyMatchDlg.m_MaxNumberOfResults = 10;

	if (IdentifyMatchDlg.DoModal() == IDCANCEL) {
		return;
	}

	m_FMR = IdentifyMatchDlg.m_FMR ;

	
	if (IdentifyMatchDlg.m_InputBIRKind == 0) {
		ProcessedBIRInputData.Form = BioAPI_BIR_HANDLE_INPUT;
		ProcessedBIRInputData.InputBIR.BIRinBSP = &( m_BIRHandleList[ IdentifyMatchDlg.m_ProcessedIndex ].Handle ) ;
	} else {
		ProcessedBIRInputData.Form = BioAPI_FULLBIR_INPUT;
		ProcessedBIRInputData.InputBIR.BIR = &( m_BIRList[IdentifyMatchDlg.m_ProcessedIndex] ) ;
	}

	TotalNumberOfTemplates = IdentifyMatchDlg.m_TemplateListSize;

	
	Population.Type = BioAPI_ARRAY_TYPE;
	Population.BIRs.BIRArray = new BioAPI_BIR_ARRAY_POPULATION ;
	Population.BIRs.BIRArray->NumberOfMembers = TotalNumberOfTemplates;
	Population.BIRs.BIRArray->Members = new BioAPI_BIR[ TotalNumberOfTemplates ] ;
	for ( i = 0 ; i < TotalNumberOfTemplates ; i++ ) {
		duplicateBIR( &( Population.BIRs.BIRArray->Members[ i ] ), &( m_BIRList[ IdentifyMatchDlg.m_TemplateIndexList[ i ] ] ) ) ;
	}

	MaxFMRRequested = IdentifyMatchDlg.m_FMR;
	Timeout = IdentifyMatchDlg.m_Timeout;
	MaxNumberOfResults = IdentifyMatchDlg.m_MaxNumberOfResults;

	DWORD callTime = GetTickCount();
	BioAPI_RETURN rc = BioAPI_IdentifyMatch(m_BSPHandle, MaxFMRRequested, 
		&ProcessedBIRInputData, &Population, TotalNumberOfTemplates, BioAPI_FALSE, MaxNumberOfResults,
		&NumberOfResults, &Candidates, Timeout);
	callTime = GetTickCount() - callTime;

	if (rc != BioAPI_OK) {
		sprintf(msg, "BioAPI_IdentifyMatch failed.(0x%08x) [Latency of API:%ld msec]", rc, callTime);
		MessageBox(msg);
		return;
	}

	if (Candidates != NULL)
	{
		sprintf(msg, "BioAPI_IdentifyMatch successful. [Latency of API:%ld msec]\nCandidate.Type = %d\n\nNumberOfResult = %d\n", 
			callTime, Candidates[0].Type, NumberOfResults);
	}
	else
	{
		sprintf(msg, "BioAPI_IdentifyMatch successful. [Latency of API:%ld msec]\n\nNumberOfResult = %d\n", 
			callTime, NumberOfResults);
	}

	char temp[100], *msgbuf;
	msgbuf = (char*)malloc(sizeof(char)*100*NumberOfResults + 256);
	strcpy(msgbuf, msg);

	for (uint32_t i = 0; i < NumberOfResults; i++) {
		sprintf(temp, "FMR = %d(%1.4f), Date = %04d/%02d/%02d %02d:%02d:%02d\n", 
			Candidates[i].FMRAchieved, 
			Candidates[i].FMRAchieved / (double)0x7FFFFFFF, 
			Population.BIRs.BIRArray->Members[*Candidates[i].BIR.BIRInArray].Header.CreationDTG.Date.Year, 
			Population.BIRs.BIRArray->Members[*Candidates[i].BIR.BIRInArray].Header.CreationDTG.Date.Month, 
			Population.BIRs.BIRArray->Members[*Candidates[i].BIR.BIRInArray].Header.CreationDTG.Date.Day, 
			Population.BIRs.BIRArray->Members[*Candidates[i].BIR.BIRInArray].Header.CreationDTG.Time.Hour, 
			Population.BIRs.BIRArray->Members[*Candidates[i].BIR.BIRInArray].Header.CreationDTG.Time.Minute, 
			Population.BIRs.BIRArray->Members[*Candidates[i].BIR.BIRInArray].Header.CreationDTG.Time.Second);
		strcat(msgbuf, temp);
	}

	MessageBox(msgbuf);

	free( msgbuf ) ;

	for ( i = 0 ; i < NumberOfResults ; i++ ) {
		BioAPI_Free( Candidates[ i ].BIR.BIRInArray ) ;
	}
	BioAPI_Free( Candidates ) ;

	for ( i = 0 ; i < TotalNumberOfTemplates ; i++ ) {
		freeBIR( &( Population.BIRs.BIRArray->Members[ i ] ) ) ;
	}
	delete[] Population.BIRs.BIRArray->Members ;
	delete Population.BIRs.BIRArray ;

	return ;
}

DWORD WINAPI execBnClickedButtonEnroll(LPVOID lpParameter)
{
	((CBioAPI_TestDlg*)lpParameter)->doEnroll();
	return 0;
}

void CBioAPI_TestDlg::OnBnClickedButtonEnroll()
{
	HANDLE hDlgThread = CreateThread(NULL, NULL, execBnClickedButtonEnroll, (LPVOID)this, NULL, NULL);
	CloseHandle( hDlgThread ) ;
}

void CBioAPI_TestDlg::doEnroll()
{
	
	char msg[256];
	BioAPI_BIR_PURPOSE Purpose;
	BioAPI_BIR_HANDLE TemplateBIRHandle;
	int32_t Timeout;
	BIR_HANDLE_INFO BIRInfo;
	CInputCaptureDlg CaptureDlg;

	CaptureDlg.m_nPurpose = -1;
	CaptureDlg.m_nTimeout = SAMPLE_APP_TIMEOUT_DEFAULT ;
	if (CaptureDlg.DoModal() == IDCANCEL) {
		return;
	}

	if ( CaptureDlg.m_SelectedIndex == 0 ) {
		Purpose = BioAPI_PURPOSE_VERIFY ;
	} else if ( CaptureDlg.m_SelectedIndex == 1 ) {
		Purpose = BioAPI_PURPOSE_IDENTIFY;
	} else {
		Purpose = BioAPI_PURPOSE_ENROLL;
	}
	Timeout = CaptureDlg.m_nTimeout;

	DWORD callTime = GetTickCount();
	BioAPI_RETURN rc = BioAPI_Enroll( m_BSPHandle, Purpose, BioAPI_NO_SUBTYPE_AVAILABLE, NULL, NULL, 
		&TemplateBIRHandle, NULL, Timeout, NULL, NULL ) ;
	callTime = GetTickCount() - callTime;

	if (rc != BioAPI_OK) {
		sprintf(msg, "BioAPI_Enroll failed.(0x%08x) [Latency of API:%ld msec]", rc, callTime);
		MessageBox(msg);
		return;
	}

	BioAPI_BIR_HEADER Header;

	rc = BioAPI_GetHeaderFromHandle(m_BSPHandle, TemplateBIRHandle, &Header);
	if (rc != BioAPI_OK) {
		sprintf(msg, "The error occurred.");
		MessageBox(msg);
		return;
	}

	BIRInfo.Handle = TemplateBIRHandle;
	BIRInfo.Purpose = Header.Purpose;
	BIRInfo.Type = Header.Type;
	m_BIRHandleList.push_back(BIRInfo);

	sprintf(msg, "BioAPI_Enroll successful. [Latency of API:%ld msec]", callTime);
	MessageBox(msg);
}

DWORD WINAPI execBnClickedButtonVerify(LPVOID lpParameter)
{
	((CBioAPI_TestDlg*)lpParameter)->doVerify();
	return 0;
}

void CBioAPI_TestDlg::OnBnClickedButtonVerify()
{
	HANDLE hDlgThread = CreateThread(NULL, NULL, execBnClickedButtonVerify, (LPVOID)this, NULL, NULL);
	CloseHandle( hDlgThread ) ;
}

void CBioAPI_TestDlg::doVerify()
{
	
	char msg[256];

	BioAPI_FMR MaxFMRRequested;
	BioAPI_INPUT_BIR ReferenceTemplateInputData;
	BioAPI_BOOL Result;
	BioAPI_FMR FMRAchieved;
	int32_t Timeout;

	CInputVerifyDlg VerifyDlg;
	VerifyDlg.m_BIRHandleList = m_BIRHandleList;
	VerifyDlg.m_BIRList = m_BIRList;
	VerifyDlg.m_FMRThreshold = SAMPLE_APP_FMR_DEFAULT ;
	VerifyDlg.m_Timeout = SAMPLE_APP_TIMEOUT_DEFAULT ;

	if (VerifyDlg.DoModal() == IDCANCEL) {
		return;
	}

	MaxFMRRequested = VerifyDlg.m_FMRThreshold;
	Timeout = VerifyDlg.m_Timeout;

	
	if (VerifyDlg.m_InputDataKind == 0) {
		
		ReferenceTemplateInputData.Form = BioAPI_BIR_HANDLE_INPUT;
		ReferenceTemplateInputData.InputBIR.BIRinBSP = &( m_BIRHandleList[ VerifyDlg.m_SelectedIndex ].Handle ) ;
	} else {
		
		ReferenceTemplateInputData.Form = BioAPI_FULLBIR_INPUT;
		ReferenceTemplateInputData.InputBIR.BIR = &( m_BIRList[ VerifyDlg.m_SelectedIndex ] ) ;
	}

	// Verify
	DWORD callTime = GetTickCount();
	BioAPI_RETURN rc = BioAPI_Verify(m_BSPHandle, MaxFMRRequested, 
		&ReferenceTemplateInputData, BioAPI_NO_SUBTYPE_AVAILABLE, NULL,
		&Result, &FMRAchieved, NULL, Timeout, NULL ) ;
	callTime = GetTickCount() - callTime;

	if (rc != BioAPI_OK) {
		sprintf(msg, "BioAPI_Verify failed.(0x%08x) [Latency of API:%ld msec]", rc, callTime);
		MessageBox(msg);
		return;
	}

	sprintf(msg, "BioAPI_Verify successful. [Latency of API:%ld msec]\nResult of verification = %s, FMR = %d(%1.4f)", 
		callTime,
		(Result == BioAPI_FALSE) ? ("unmatched") : ("matched"), 
		FMRAchieved, FMRAchieved / (double)0x7FFFFFFF);

	MessageBox(msg);
}

DWORD WINAPI execBnClickedButtonIdentify(LPVOID lpParameter)
{
	((CBioAPI_TestDlg*)lpParameter)->doIdentify();
	return 0;
}

void CBioAPI_TestDlg::OnBnClickedButtonIdentify()
{
	HANDLE hDlgThread = CreateThread(NULL, NULL, execBnClickedButtonIdentify, (LPVOID)this, NULL, NULL);
	CloseHandle( hDlgThread ) ;
}

void CBioAPI_TestDlg::doIdentify()
{
	
	char msg[256];

	BioAPI_FMR MaxFMRRequested;
	BioAPI_IDENTIFY_POPULATION Population;
	uint32_t TotalNumberOfTemplates;
	uint32_t MaxNumberOfResults;
	uint32_t NumberOfResults;
	int32_t Timeout;
	BioAPI_CANDIDATE *Candidates = NULL;
	BioAPI_RETURN rc ;
	unsigned int i ;

	CInputIdentifyDlg IdentifyDlg;

	IdentifyDlg.m_FMR = m_FMR ;
	IdentifyDlg.m_Timeout = SAMPLE_APP_TIMEOUT_DEFAULT ;
	IdentifyDlg.m_BIRList = m_BIRList;
	IdentifyDlg.m_MaxNumberOfResults = 10;

	if (IdentifyDlg.DoModal() == IDCANCEL) {
		return;
	}

	m_FMR = IdentifyDlg.m_FMR ;

	TotalNumberOfTemplates = IdentifyDlg.m_TemplateListSize;

	
	Population.Type = BioAPI_ARRAY_TYPE;
	Population.BIRs.BIRArray = new BioAPI_BIR_ARRAY_POPULATION ;
	Population.BIRs.BIRArray->NumberOfMembers = TotalNumberOfTemplates;
	Population.BIRs.BIRArray->Members = new BioAPI_BIR[ TotalNumberOfTemplates ] ;
	for ( i = 0; i < TotalNumberOfTemplates; i++) {
		duplicateBIR( &( Population.BIRs.BIRArray->Members[i] ), &( m_BIRList[ IdentifyDlg.m_TemplateIndexList[ i ] ] ) ) ;
	}

	MaxFMRRequested = IdentifyDlg.m_FMR;
	Timeout = IdentifyDlg.m_Timeout;
	MaxNumberOfResults = IdentifyDlg.m_MaxNumberOfResults;

	DWORD callTime = GetTickCount();
	rc = BioAPI_Identify(m_BSPHandle, MaxFMRRequested, 
		BioAPI_NO_SUBTYPE_AVAILABLE, &Population, TotalNumberOfTemplates, BioAPI_FALSE, MaxNumberOfResults, 
		&NumberOfResults, &Candidates, Timeout, NULL ) ;
	callTime = GetTickCount() - callTime;

	if (rc != BioAPI_OK) {
		sprintf(msg, "BioAPI_Identify failed.(0x%08x) [Latency of API:%ld msec]", rc, callTime);
		MessageBox(msg);
		return;
	}

	if (Candidates != NULL)
	{
		sprintf(msg, "BioAPI_Identify successful. [Latency of API:%ld msec]\nCandidate.Type = %d\n\nNumberOfResult = %d\n", 
			callTime, Candidates[0].Type, NumberOfResults ) ;
	}
	else
	{
		sprintf(msg, "BioAPI_Identify successful. [Latency of API:%ld msec]\n\nNumberOfResult = %d\n", 
			callTime, NumberOfResults);
	}

	char temp[100], *msgbuf;
	msgbuf = (char*)malloc(sizeof(char)*100*NumberOfResults + 256);
	strcpy(msgbuf, msg);

	for (uint32_t i = 0; i < NumberOfResults; i++) {
		sprintf(temp, "FMR = %d(%1.4f), Date = %04d/%02d/%02d %02d:%02d:%02d\n", 
			Candidates[i].FMRAchieved, 
			Candidates[i].FMRAchieved / (double)0x7FFFFFFF, 
			Population.BIRs.BIRArray->Members[*Candidates[i].BIR.BIRInArray].Header.CreationDTG.Date.Year, 
			Population.BIRs.BIRArray->Members[*Candidates[i].BIR.BIRInArray].Header.CreationDTG.Date.Month, 
			Population.BIRs.BIRArray->Members[*Candidates[i].BIR.BIRInArray].Header.CreationDTG.Date.Day, 
			Population.BIRs.BIRArray->Members[*Candidates[i].BIR.BIRInArray].Header.CreationDTG.Time.Hour, 
			Population.BIRs.BIRArray->Members[*Candidates[i].BIR.BIRInArray].Header.CreationDTG.Time.Minute, 
			Population.BIRs.BIRArray->Members[*Candidates[i].BIR.BIRInArray].Header.CreationDTG.Time.Second);
		strcat(msgbuf, temp);
	}

	MessageBox(msgbuf);

	free( msgbuf ) ;

	for ( i = 0 ; i < NumberOfResults ; i++ ) {
		BioAPI_Free( Candidates[ i ].BIR.BIRInArray ) ;
	}
	BioAPI_Free( Candidates ) ;

	for ( i = 0 ; i < TotalNumberOfTemplates ; i++ ) {
		freeBIR( &( Population.BIRs.BIRArray->Members[ i ] ) ) ;
	}
	delete[] Population.BIRs.BIRArray->Members ;
	delete Population.BIRs.BIRArray ;

	return ;
}

DWORD WINAPI execBnClickedButtonCancel(LPVOID lpParameter)
{
	((CBioAPI_TestDlg*)lpParameter)->doCancel();
	return 0;
}

void CBioAPI_TestDlg::OnBnClickedButtonCancel()
{
	HANDLE hDlgThread = CreateThread(NULL, NULL, execBnClickedButtonCancel, (LPVOID)this, NULL, NULL);
	CloseHandle( hDlgThread ) ;
}

void CBioAPI_TestDlg::doCancel()
{
	
	char msg[256];

	BioAPI_RETURN rc = BioAPI_Cancel(m_BSPHandle);

	if (rc != BioAPI_OK) {
		sprintf(msg, "BioAPI_Cancel failed.(0x%08x)", rc);
		MessageBox(msg);
		return;
	}

	sprintf(msg, "BioAPI_Cancel successful.");
	MessageBox(msg);
}

void CBioAPI_TestDlg::OnBnClickedButtonFree()
{
	
	char msg[256];
	BioAPI_RETURN rc = BioAPI_OK;

	CInputFreeDlg FreeDlg;

	if (FreeDlg.DoModal() == IDCANCEL) {
		return;
	}

	if (FreeDlg.m_SelectedIndex == 0) {
		rc = BioAPI_Free(m_BSPSchemaList);
		m_BSPSchemaList = NULL;
		m_NumBSP = 0 ;
	} else {
		if ( ( m_UnitSchemaList != NULL ) && ( m_UnitSchemaList->UnitProperty.Length > 0 ) ) {
			rc = BioAPI_Free(m_UnitSchemaList->UnitProperty.Data);
		}
		rc = BioAPI_Free(m_UnitSchemaList);
		m_UnitSchemaList = NULL;
	}


	if (rc != BioAPI_OK) {
		sprintf(msg, "BioAPI_Free failed.(0x%08x)", rc);
		MessageBox(msg);
		return;
	}

	sprintf(msg, "BioAPI_Free successful.");
	MessageBox(msg);
}


void CBioAPI_TestDlg::OnBnClickedButtonAttachProcess()
{
	
	char msg[1024];
	BioAPI_RETURN rc = BioAPI_OK;
	uint32_t uIdx;
	char szBSPList[ 4096 ] ;

	// Init
	rc = BioAPI_Init(m_BioAPIVersion);
	if (rc != BioAPI_OK) {
		sprintf(msg, "BioAPI_Init failed.(0x%08x)", rc);
		MessageBox(msg);
		return;
	}

	// EnumBSPs
	char BSPName[512];
	m_BSPNameList.clear();
	freeBSPSchemaList( &m_BSPSchemaList, &m_NumBSP ) ;

	rc = BioAPI_EnumBSPs(&m_BSPSchemaList, &m_NumBSP);
	if (rc != BioAPI_OK) {
		sprintf(msg, "BioAPI_EnumBSPs failed.(0x%08x)", rc);
		MessageBox(msg);
		return;
	}

	szBSPList[ 0 ] = '\0' ;
	for (uIdx = 0; uIdx < m_NumBSP; uIdx++) {
		sprintf(BSPName, "%d : %s", uIdx, m_BSPSchemaList[uIdx].BSPDescription);
		m_BSPNameList.push_back(BSPName);

		sprintf( BSPName,
				"[%d]\r\n"
				"BSPDescription\t%s\r\n"
				"BSPUuid\t\t%02x%02x%02x%02x-%02x%02x-%02x%02x-%02x%02x-%02x%02x%02x%02x%02x%02x\r\n"
				"Path\t\t%s\r\n"
				"SpecVersion\t0x%08x\r\n"
				"ProductVersion\t%s\r\n"
				"Vendor\t\t%s\r\n\r\n",
				uIdx + 1,
				m_BSPSchemaList[uIdx].BSPDescription,
				m_BSPSchemaList[uIdx].BSPUuid[ 0 ],
				m_BSPSchemaList[uIdx].BSPUuid[ 1 ],
				m_BSPSchemaList[uIdx].BSPUuid[ 2 ],
				m_BSPSchemaList[uIdx].BSPUuid[ 3 ],
				m_BSPSchemaList[uIdx].BSPUuid[ 4 ],
				m_BSPSchemaList[uIdx].BSPUuid[ 5 ],
				m_BSPSchemaList[uIdx].BSPUuid[ 6 ],
				m_BSPSchemaList[uIdx].BSPUuid[ 7 ],
				m_BSPSchemaList[uIdx].BSPUuid[ 8 ],
				m_BSPSchemaList[uIdx].BSPUuid[ 9 ],
				m_BSPSchemaList[uIdx].BSPUuid[ 10 ],
				m_BSPSchemaList[uIdx].BSPUuid[ 11 ],
				m_BSPSchemaList[uIdx].BSPUuid[ 12 ],
				m_BSPSchemaList[uIdx].BSPUuid[ 13 ],
				m_BSPSchemaList[uIdx].BSPUuid[ 14 ],
				m_BSPSchemaList[uIdx].BSPUuid[ 15 ],
				m_BSPSchemaList[uIdx].Path,
				m_BSPSchemaList[uIdx].SpecVersion,
				m_BSPSchemaList[uIdx].ProductVersion,
				m_BSPSchemaList[uIdx].Vendor
				);
		strcat( szBSPList, BSPName ) ;
	}

	CInputBSPLoadDlg BSPLoadDlg;

	BSPLoadDlg.SetBSPList(m_BSPNameList);

	if (BSPLoadDlg.DoModal() == IDCANCEL) {

		freeBSPSchemaList( &m_BSPSchemaList, &m_NumBSP ) ;

		rc = BioAPI_Terminate();
	
		if (rc != BioAPI_OK) {
			sprintf(msg, "BioAPI_Terminate failed.(0x%08x)", rc);
			MessageBox(msg);
		}

		return;
	}

	m_nCrtBSPNum = BSPLoadDlg.GetBSPNum();

	if (BSPLoadDlg.GetEventOnOff()) {
		m_EventHandler = NULL;
		m_EventHandlerCxt = NULL;
	} else {
		m_EventHandler = EventHandler;
		m_EventHandlerCxt = m_hWnd;
	}

	// BSPLoad
	rc = BioAPI_BSPLoad(&m_BSPSchemaList[m_nCrtBSPNum].BSPUuid, m_EventHandler, m_EventHandlerCxt);
	if (rc != BioAPI_OK) {
		sprintf(msg, "BioAPI_BSPLoad failed.(0x%08x)", rc);
		MessageBox(msg);
		return;
	}

	// QueryUnits
	char UnitID[256];
	uint32_t uNumUnits;
	m_UnitIDList.clear();
	BioAPI_Free(m_UnitSchemaList);
	m_UnitSchemaList = NULL ;

	rc = BioAPI_QueryUnits(&m_BSPSchemaList[m_nCrtBSPNum].BSPUuid, &m_UnitSchemaList, &uNumUnits);
	if (rc != BioAPI_OK) {
		sprintf(msg, "BioAPI_QueryUnits failed.(0x%08x)", rc);
		MessageBox(msg);
		return;
	}

	if (m_UnitSchemaList == NULL) {
	    sprintf(msg, "Unable to find the unit. The process is terminated.");
		MessageBox(msg);
		return;
	}

    sprintf(msg, "BioAPI_QueryUnits successful.");
	for (uIdx = 0; uIdx < uNumUnits; uIdx++) {
		switch (m_UnitSchemaList[uIdx].UnitCategory) {
			case BioAPI_CATEGORY_SENSOR :
				sprintf(UnitID, "Sensor Unit (ID = %d)", m_UnitSchemaList[uIdx].UnitId);
				break;
			case BioAPI_CATEGORY_ARCHIVE :
				sprintf(UnitID, "Archive Unit (ID = %d)", m_UnitSchemaList[uIdx].UnitId);
				break;
			case BioAPI_CATEGORY_PROCESSING_ALG :
				sprintf(UnitID, "Processing Unit (ID = %d)", m_UnitSchemaList[uIdx].UnitId);
				break;
			case BioAPI_CATEGORY_MATCHING_ALG :
				sprintf(UnitID, "Matching Unit (ID = %d)", m_UnitSchemaList[uIdx].UnitId);
				break;
			}
		m_UnitIDList.push_back(UnitID);
	}

	// BSPAttach
	int nSelectedUnitNum = 1 ;

	BioAPI_UNIT_LIST_ELEMENT UnitList[4] ;
	UnitList[0].UnitCategory = m_UnitSchemaList[0].UnitCategory;
	UnitList[0].UnitId = m_UnitSchemaList[0].UnitId;

	rc = BioAPI_BSPAttach(&m_BSPSchemaList[m_nCrtBSPNum].BSPUuid,
		m_BioAPIVersion,
		UnitList,
		nSelectedUnitNum,
		&m_BSPHandle);

	if (rc != BioAPI_OK) {
		sprintf(msg, "BioAPI_BSPAttach failed.(0x%08x)", rc);
		MessageBox(msg);
		return;
	}

	
	int nInputData[ 1 ] ;
	BioAPI_DATA InputData ;
	InputData.Data = nInputData ;
	nInputData[ 0 ] = 0 ;
	InputData.Length = 1 ;

	rc = BioAPI_ControlUnit( m_BSPHandle, m_UnitSchemaList[ 0 ].UnitId, 5, &InputData, NULL ) ;
	if (rc != BioAPI_OK) {
		sprintf(msg, "BioAPI_ControlUnit failed.(0x%08x)", rc);
		MessageBox(msg);
	}

	sprintf( msg,
			"'Attach Process as a whole' was finished.\r\n"
			"\r\n<BSP to use>\r\n"
			"BSPDescription\t%s\r\n"
			"BSPUuid\t\t%02x%02x%02x%02x-%02x%02x-%02x%02x-%02x%02x-%02x%02x%02x%02x%02x%02x\r\n"
			"Path\t\t%s\r\n"
			"SpecVersion\t0x%08x\r\n"
			"ProductVersion\t%s\r\n"
			"Vendor\t\t%s\r\n"
			"\r\n<The list of BSP which Framework can use>\r\n"
			"%s",
			m_BSPSchemaList[m_nCrtBSPNum].BSPDescription,
			m_BSPSchemaList[m_nCrtBSPNum].BSPUuid[ 0 ],
			m_BSPSchemaList[m_nCrtBSPNum].BSPUuid[ 1 ],
			m_BSPSchemaList[m_nCrtBSPNum].BSPUuid[ 2 ],
			m_BSPSchemaList[m_nCrtBSPNum].BSPUuid[ 3 ],
			m_BSPSchemaList[m_nCrtBSPNum].BSPUuid[ 4 ],
			m_BSPSchemaList[m_nCrtBSPNum].BSPUuid[ 5 ],
			m_BSPSchemaList[m_nCrtBSPNum].BSPUuid[ 6 ],
			m_BSPSchemaList[m_nCrtBSPNum].BSPUuid[ 7 ],
			m_BSPSchemaList[m_nCrtBSPNum].BSPUuid[ 8 ],
			m_BSPSchemaList[m_nCrtBSPNum].BSPUuid[ 9 ],
			m_BSPSchemaList[m_nCrtBSPNum].BSPUuid[ 10 ],
			m_BSPSchemaList[m_nCrtBSPNum].BSPUuid[ 11 ],
			m_BSPSchemaList[m_nCrtBSPNum].BSPUuid[ 12 ],
			m_BSPSchemaList[m_nCrtBSPNum].BSPUuid[ 13 ],
			m_BSPSchemaList[m_nCrtBSPNum].BSPUuid[ 14 ],
			m_BSPSchemaList[m_nCrtBSPNum].BSPUuid[ 15 ],
			m_BSPSchemaList[m_nCrtBSPNum].Path,
			m_BSPSchemaList[m_nCrtBSPNum].SpecVersion,
			m_BSPSchemaList[m_nCrtBSPNum].ProductVersion,
			m_BSPSchemaList[m_nCrtBSPNum].Vendor,
			szBSPList
			);

	MessageBox( msg ) ;
}

void CBioAPI_TestDlg::OnBnClickedDeleteparameter()
{
	char msg[256];
	BioAPI_RETURN rc = BioAPI_OK;

	deleteParameter delPara;
	delPara.m_appName = m_appName ;

	if(delPara.DoModal() ==IDCANCEL)return;

	m_appName = delPara.m_appName ;

	BioAPI_STRING appName;
	memcpy(appName,delPara.m_appName,delPara.m_appName.GetLength()+1);

	rc = HiBioAPI_DeleteParameter(&appName);
	if (rc != BioAPI_OK) {
		sprintf(msg, "HiBioAPI_DeleteParameter failed.(0x%08x)", rc);
		MessageBox(msg);
		return;
	}
	sprintf(msg, "HiBioAPI_DeleteParameter successful.", rc);
	MessageBox(msg);
}

void CBioAPI_TestDlg::OnBnClickedVerifybsp()
{
	char msg[256];
	BioAPI_RETURN rc = BioAPI_OK;
	verifyBSP verify;
	if(verify.DoModal() ==IDCANCEL)return;

	
	BioAPI_STRING bsp;
	memcpy(bsp,verify.m_bsphash,verify.m_bsphash.GetLength()+1);

	// VerifyBSP
	rc = HiBioAPI_VerifyBSP(&m_BSPSchemaList[m_nCrtBSPNum].BSPUuid,&bsp);
	if (rc != BioAPI_OK) {
		sprintf(msg, "HiBioAPI_VerifyBSP failed.(0x%08x)", rc);
		MessageBox(msg);
		return;
	}
	sprintf(msg, "HiBioAPI_VerifyBSP successful.", rc);
	MessageBox(msg);
}

#if 0
#define HI_TEST_VERIFY_MATCH_CHALLENGE
#endif

void CBioAPI_TestDlg::OnBnClickedVerifychallenge()
{
	char msg[256];
	BioAPI_RETURN rc = BioAPI_OK;
	CChallenge challenge;
	if(challenge.DoModal() ==IDCANCEL)return;

	
	BioAPI_STRING challengeCode;
	memcpy(challengeCode,challenge.m_challenge,challenge.m_challenge.GetLength()+1);

	int nIndex = 0 ;

	
#if defined( HI_TEST_VERIFY_MATCH_CHALLENGE )	
	BioAPI_FMR MaxFMRRequested;
	BioAPI_INPUT_BIR ProcessedBIRInputData;
	BioAPI_INPUT_BIR ReferenceTemplateInputData;
	BioAPI_BOOL Result;
	BioAPI_FMR FMRAchieved;

	CInputVerifyMatchDlg VerifyMatchDlg;

	VerifyMatchDlg.m_BIRHandleList = m_BIRHandleList;
	VerifyMatchDlg.m_BIRList = m_BIRList;
	VerifyMatchDlg.m_FMRThreshold = SAMPLE_APP_FMR_DEFAULT ;

	if (VerifyMatchDlg.DoModal() == IDCANCEL) {
		return;
	}

	MaxFMRRequested = VerifyMatchDlg.m_FMRThreshold;

	

	
	if (VerifyMatchDlg.m_BaseImageKind == 0) {
		
		ProcessedBIRInputData.Form = BioAPI_BIR_HANDLE_INPUT;
		ProcessedBIRInputData.InputBIR.BIRinBSP = &( m_BIRHandleList[ VerifyMatchDlg.m_SelectedBaseImage ].Handle ) ;
	} else {
		
		ProcessedBIRInputData.Form = BioAPI_FULLBIR_INPUT;
		ProcessedBIRInputData.InputBIR.BIR = &( m_BIRList[ VerifyMatchDlg.m_SelectedBaseImage ] ) ;
	}

	
	nIndex = VerifyMatchDlg.m_SelectedReferenceImage ;
	if ( VerifyMatchDlg.m_ReferenceImageKind == 0 ) {
		
		ReferenceTemplateInputData.Form = BioAPI_BIR_HANDLE_INPUT;
		ReferenceTemplateInputData.InputBIR.BIRinBSP = &( m_BIRHandleList[ nIndex ].Handle ) ;
	} else {
		ReferenceTemplateInputData.Form = BioAPI_FULLBIR_INPUT ;
		ReferenceTemplateInputData.InputBIR.BIR = &( m_BIRList[ nIndex ] ) ;
	}

	// VerifyMatch
	DWORD callTime = GetTickCount();
	rc = BioAPI_VerifyMatch(m_BSPHandle, MaxFMRRequested, 
		&ProcessedBIRInputData, &ReferenceTemplateInputData, NULL,
		&Result, &FMRAchieved, NULL ) ;
	callTime = GetTickCount() - callTime;

	if (rc != BioAPI_OK) {
		sprintf(msg, "BioAPI_VerifyMatch failed.(0x%08x) [Latency of API:%ld msec]", rc, callTime);
		MessageBox(msg);
		return;
	}
#else	
	BioAPI_FMR MaxFMRRequested;
	BioAPI_INPUT_BIR ReferenceTemplateInputData;
	BioAPI_BOOL Result;
	BioAPI_FMR FMRAchieved;
	int32_t Timeout;

	CInputVerifyDlg VerifyDlg;
	VerifyDlg.m_BIRHandleList = m_BIRHandleList;
	VerifyDlg.m_BIRList = m_BIRList;
	VerifyDlg.m_FMRThreshold = SAMPLE_APP_FMR_DEFAULT ;
	VerifyDlg.m_Timeout = SAMPLE_APP_TIMEOUT_DEFAULT ;

	if (VerifyDlg.DoModal() == IDCANCEL) {
		return;
	}

	MaxFMRRequested = VerifyDlg.m_FMRThreshold;
	Timeout = VerifyDlg.m_Timeout;

	
	nIndex = VerifyDlg.m_SelectedIndex ;
	if ( VerifyDlg.m_InputDataKind == 0 ) {
		
		ReferenceTemplateInputData.Form = BioAPI_BIR_HANDLE_INPUT;
		ReferenceTemplateInputData.InputBIR.BIRinBSP = &( m_BIRHandleList[ nIndex ].Handle ) ;
	} else {
		ReferenceTemplateInputData.Form = BioAPI_FULLBIR_INPUT ;
		ReferenceTemplateInputData.InputBIR.BIR = &( m_BIRList[ nIndex ] ) ;
	}

	// Verify
	DWORD callTime = GetTickCount();
	rc = BioAPI_Verify(m_BSPHandle, MaxFMRRequested, 
		&ReferenceTemplateInputData, BioAPI_NO_SUBTYPE_AVAILABLE, NULL,
		&Result, &FMRAchieved, NULL, Timeout, NULL ) ;
	callTime = GetTickCount() - callTime;

	if (rc != BioAPI_OK) {
		sprintf(msg, "BioAPI_Verify failed.(0x%08x) [Latency of API:%ld msec]", rc, callTime);
		MessageBox(msg);
		return;
	}
#endif

	

	
	hi_bioapi_verifystatus verify;
	verify.Securitycode.Data = NULL;

	rc = HiBioAPI_VerifyChallenge(&challengeCode,&verify);
	if (rc != BioAPI_OK) {
		sprintf(msg, "HiBioAPI_VerifyChallenge failed.(0x%08x)", rc);
		MessageBox(msg);
		return;
	}

	
	verify.Securitycode.Data = new BYTE[verify.Securitycode.Length];

	
	rc = HiBioAPI_VerifyChallenge(&challengeCode,&verify);
	if (rc != BioAPI_OK) {
		sprintf(msg, "HiBioAPI_VerifyChallenge failed.(0x%08x)", rc);
		MessageBox(msg);
		return;
	}

	sprintf(msg, "HiBioAPI_VerifyChallenge successful.\nFMR:%i\nChallenge:%s\nHash:%s\nResult:%i\nQuality:%i\n"
		,verify.achievedFMR,verify.challengecode,verify.HashReferenceBIR,verify.Result,
		verify.samplaQuality);
	MessageBox(msg);

	
	BioAPI_DATA securityCode ;
	BioAPI_BIR *pRefBIR = NULL ;

	if ( ReferenceTemplateInputData.Form == BioAPI_BIR_HANDLE_INPUT ) {
		BioAPI_BIR BIRfromH ;
		BioAPI_BIR refBIR ;

		rc = BioAPI_GetBIRFromHandle( m_BSPHandle, *( ReferenceTemplateInputData.InputBIR.BIRinBSP ), &BIRfromH ) ;
		if ( rc != BioAPI_OK ) {
			sprintf( msg, "BioAPI_GetBIRFromHandle failed.(0x%08x)", rc ) ;
			MessageBox( msg ) ;
			return ;
		}

		duplicateBIR( &refBIR, &BIRfromH ) ;
		BioAPI_Free( BIRfromH.BiometricData.Data ) ;
		BioAPI_Free( BIRfromH.SecurityBlock.Data ) ;
		m_BIRList.push_back( refBIR ) ;

		
		m_BIRHandleList[ nIndex ].Purpose = 0;
		m_BIRHandleList[ nIndex ].Type = 0;

		pRefBIR = &refBIR ;
	} else {
		pRefBIR = ReferenceTemplateInputData.InputBIR.BIR ;
	}

	rc = getSecurityCode( pRefBIR, Result, FMRAchieved, &challenge, &securityCode ) ;
	if ( rc == FALSE ) {
		sprintf(msg, "failed to generate the security code." ) ;
		MessageBox(msg);
		return;
	}

	if ( verify.Securitycode.Length != securityCode.Length ) {
		sprintf(msg, "The security codes did not match." ) ;
		MessageBox(msg);
		return;
	}

	if ( memcmp( verify.Securitycode.Data, securityCode.Data, securityCode.Length ) != 0 ) {
		sprintf(msg, "The security codes did not match." ) ;
		MessageBox(msg);
		return;
	}

	sprintf( msg, "The security codes matched." ) ;
	MessageBox( msg ) ;

	delete[] verify.Securitycode.Data ;
	delete[] securityCode.Data ;

	return ;
}

#if 0
#define HI_TEST_IDENTIFY_MATCH_CHALLENGE
#endif

void CBioAPI_TestDlg::OnBnClickedIdentifychallenge()
{
	char msg[256];
	BioAPI_RETURN rc = BioAPI_OK;
	CChallenge challenge;
	if(challenge.DoModal() ==IDCANCEL)return;

	
	BioAPI_STRING challengeCode;
	memcpy(challengeCode,challenge.m_challenge,challenge.m_challenge.GetLength()+1);

	
#if defined( HI_TEST_IDENTIFY_MATCH_CHALLENGE )	
	// Capture
	BioAPI_BIR_HANDLE CapturedBIRHandle;

	rc = BioAPI_Capture(m_BSPHandle, BioAPI_PURPOSE_ENROLL, BioAPI_NO_SUBTYPE_AVAILABLE, 
						NULL, &CapturedBIRHandle, SAMPLE_APP_TIMEOUT_DEFAULT, NULL ) ;
	if (rc != BioAPI_OK) {
		sprintf(msg, "BioAPI_Capture(CreateTemplate) failed.(0x%08x)", rc );
		MessageBox(msg);
		return;
	}

	BioAPI_INPUT_BIR CapturedBIRInputData;
	BioAPI_BIR_HANDLE TemplateBIRHandle;

	CapturedBIRInputData.Form = BioAPI_BIR_HANDLE_INPUT;
	CapturedBIRInputData.InputBIR.BIRinBSP = &CapturedBIRHandle;
	rc = BioAPI_CreateTemplate( m_BSPHandle, &CapturedBIRInputData, NULL,
		NULL, &TemplateBIRHandle, NULL, NULL ) ;
	if (rc != BioAPI_OK) {
		sprintf(msg, "BioAPI_CreateTemplate failed.(0x%08x)", rc );
		MessageBox(msg);
		return;
	}

	BioAPI_FreeBIRHandle(m_BSPHandle, CapturedBIRHandle);
	CapturedBIRHandle = NULL ;

	rc = BioAPI_Capture(m_BSPHandle, BioAPI_PURPOSE_IDENTIFY, BioAPI_NO_SUBTYPE_AVAILABLE, 
						NULL, &CapturedBIRHandle, SAMPLE_APP_TIMEOUT_DEFAULT, NULL );
	if (rc != BioAPI_OK) {
		sprintf(msg, "BioAPI_Capture(Process) failed.(0x%08x)", rc );
		MessageBox(msg);
		return;
	}

	BioAPI_BIR_HANDLE ProcessedBIRHandle;
	rc = BioAPI_Process( m_BSPHandle, &CapturedBIRInputData, NULL, &ProcessedBIRHandle ) ;
	if (rc != BioAPI_OK) {
		sprintf(msg, "BioAPI_Process failed.(0x%08x)", rc );
		MessageBox(msg);
		return;
	}

	BioAPI_FreeBIRHandle(m_BSPHandle, CapturedBIRHandle);
	CapturedBIRHandle = NULL ;

	BioAPI_FMR MaxFMRRequested = SAMPLE_APP_FMR_DEFAULT ;
	BioAPI_INPUT_BIR ProcessedBIRInputData;
	BioAPI_IDENTIFY_POPULATION Population;
	uint32_t TotalNumberOfTemplates = 1;
	uint32_t MaxNumberOfResults = 0;
	uint32_t NumberOfResults;
	BioAPI_CANDIDATE *Candidates;

	ProcessedBIRInputData.Form = BioAPI_BIR_HANDLE_INPUT;
	ProcessedBIRInputData.InputBIR.BIRinBSP = &ProcessedBIRHandle;

	BioAPI_BIR BIR ;
	rc = BioAPI_GetBIRFromHandle( m_BSPHandle, TemplateBIRHandle, &BIR ) ;
	if (rc != BioAPI_OK) {
		sprintf(msg, "BioAPI_GetBIRFromHandle failed.(0x%08x)", rc );
		MessageBox(msg);
		return;
	}

	Population.Type = BioAPI_ARRAY_TYPE;
	Population.BIRs.BIRArray = new BioAPI_BIR_ARRAY_POPULATION ;
	Population.BIRs.BIRArray->NumberOfMembers = TotalNumberOfTemplates;
	Population.BIRs.BIRArray->Members = new BioAPI_BIR[ TotalNumberOfTemplates ] ;
	duplicateBIR( &( Population.BIRs.BIRArray->Members[ 0 ] ), &BIR ) ;
	BioAPI_Free( BIR.BiometricData.Data ) ;
	BioAPI_Free( BIR.SecurityBlock.Data ) ;

	rc = BioAPI_IdentifyMatch(m_BSPHandle, MaxFMRRequested, 
		&ProcessedBIRInputData, &Population, TotalNumberOfTemplates, BioAPI_FALSE, MaxNumberOfResults,
		&NumberOfResults, &Candidates, Timeout);
	if (rc != BioAPI_OK) {
		sprintf(msg, "BioAPI_IdentifyMatch failed.(0x%08x)", rc );
		MessageBox(msg);
		return;
	}
#else	
	BioAPI_FMR MaxFMRRequested;
	BioAPI_IDENTIFY_POPULATION Population;
	uint32_t TotalNumberOfTemplates;
	uint32_t MaxNumberOfResults;
	uint32_t NumberOfResults;
	int32_t Timeout;
	BioAPI_CANDIDATE *Candidates = NULL;

	CInputIdentifyDlg IdentifyDlg;

	IdentifyDlg.m_FMR = m_FMR ;
	IdentifyDlg.m_Timeout = SAMPLE_APP_TIMEOUT_DEFAULT ;
	IdentifyDlg.m_BIRList = m_BIRList;
	IdentifyDlg.m_MaxNumberOfResults = 10;

	if (IdentifyDlg.DoModal() == IDCANCEL) {
		return;
	}

	m_FMR = IdentifyDlg.m_FMR ;

	TotalNumberOfTemplates = IdentifyDlg.m_TemplateListSize;

	
	Population.Type = BioAPI_ARRAY_TYPE;
	Population.BIRs.BIRArray = new BioAPI_BIR_ARRAY_POPULATION ;
	Population.BIRs.BIRArray->NumberOfMembers = TotalNumberOfTemplates;
	Population.BIRs.BIRArray->Members = new BioAPI_BIR[ TotalNumberOfTemplates ] ;
	for (unsigned int i = 0; i < TotalNumberOfTemplates; i++) {
		duplicateBIR( &( Population.BIRs.BIRArray->Members[i] ), &( m_BIRList[ IdentifyDlg.m_TemplateIndexList[ i ] ] ) ) ;
	}

	MaxFMRRequested = IdentifyDlg.m_FMR;
	Timeout = IdentifyDlg.m_Timeout;
	MaxNumberOfResults = IdentifyDlg.m_MaxNumberOfResults;

	rc = BioAPI_Identify(m_BSPHandle, MaxFMRRequested, 
		BioAPI_NO_SUBTYPE_AVAILABLE, &Population, TotalNumberOfTemplates, BioAPI_FALSE, MaxNumberOfResults, 
		&NumberOfResults, &Candidates, Timeout, NULL ) ;
	if (rc != BioAPI_OK) {
		sprintf(msg, "BioAPI_Identify failed.(0x%08x)", rc);
		MessageBox(msg);
		return;
	}
#endif

	

	
	hi_bioapi_identifystatus identifystatus;
	identifystatus.Securitycode.Data = NULL;

	rc = HiBioAPI_IdentifyChallenge( &challengeCode, &identifystatus ) ;
	if (rc != BioAPI_OK) {
		sprintf(msg, "HiBioAPI_IdentifyChallenge failed.(0x%08x)", rc);
		MessageBox(msg);
		return;
	}

	
	identifystatus.Securitycode.Data = new BYTE[identifystatus.Securitycode.Length];

	
	rc = HiBioAPI_IdentifyChallenge( &challengeCode, &identifystatus ) ;
	if (rc != BioAPI_OK) {
		sprintf(msg, "HiBioAPI_IdentifyChallenge failed.(0x%08x)", rc);
		MessageBox(msg);
		return;
	}

	sprintf(msg, "HiBioAPI_IdentifyChallenge successful.\r\nChallenge:%s\nResult:%i\nQuality:%i"
		,identifystatus.challengecode,identifystatus.Result,identifystatus.samplaQuality);
	MessageBox(msg);

	
	BioAPI_DATA securityCode ;
	rc = getSecurityCode( NumberOfResults, 100, Candidates, &challenge, &securityCode ) ;
	if ( rc == FALSE ) {
		sprintf(msg, "failed to generate the security code." ) ;
		MessageBox(msg);
		return;
	}

	if ( identifystatus.Securitycode.Length != securityCode.Length ) {
		sprintf(msg, "The security codes did not match." ) ;
		MessageBox(msg);
		return;
	}

	if ( memcmp( identifystatus.Securitycode.Data, securityCode.Data, securityCode.Length ) != 0 ) {
		sprintf(msg, "The security codes did not match." ) ;
		MessageBox(msg);
		return;
	}

	sprintf( msg, "The security codes matched." ) ;
	MessageBox( msg ) ;

	delete[] identifystatus.Securitycode.Data ;
	delete[] securityCode.Data ;

	for (unsigned int j = 0; j < NumberOfResults; ++j)
	{
		BioAPI_Free(Candidates[j].BIR.BIRInArray);
	}
	BioAPI_Free(Candidates);

	for ( unsigned int i = 0 ; i < TotalNumberOfTemplates ; i++ ) {
		freeBIR( &( Population.BIRs.BIRArray->Members[ i ] ) ) ;
	}
	delete[] Population.BIRs.BIRArray->Members ;
	delete Population.BIRs.BIRArray ;

#if defined(HI_DEBUG_IDENTIFY_MATCH_CHALLENGE)
	BioAPI_FreeBIRHandle(m_BSPHandle, ProcessedBIRHandle);
#endif

	return ;
}

void CBioAPI_TestDlg::OnBnClickedVerifybir()
{
	char msg[256];

	
	BioAPI_RETURN rc = HiBioAPI_VerifyReferenceBIR(&certBIR);
	if (rc != BioAPI_OK) {
		sprintf(msg, "HiBioAPI_VerifyReferenceBIR failed.(0x%08x)", rc);
		MessageBox(msg);
		return;
	}
	sprintf(msg, "HiBioAPI_VerifyReferenceBIR successful.", rc);
	MessageBox(msg);

}


void CBioAPI_TestDlg::OnBnClickedDigitalsignbir()
{
	char msg[256];
	CInputGetBIRFromHandleDlg GetBIRFromHandleDlg;
	int nIndex;
	BioAPI_BIR_HANDLE TargetBIRHandle;
	BioAPI_BIR BIR,BIR2,BIRfromH ;

	CInputCreateTemplateDlg CreateTemplateDlg;

	CreateTemplateDlg.m_BIRHandleList = m_BIRHandleList;
	CreateTemplateDlg.m_BIRList = m_BIRList;

	if (CreateTemplateDlg.DoModal() == IDCANCEL) {
		return;
	}

	if ( CreateTemplateDlg.m_InputDataKind == 0 ) {
		
		nIndex = CreateTemplateDlg.m_SelectedIndex ;
		TargetBIRHandle = m_BIRHandleList[ nIndex ].Handle ;

		BioAPI_RETURN rc = BioAPI_GetBIRFromHandle( m_BSPHandle, TargetBIRHandle, &BIRfromH ) ;
		if (rc != BioAPI_OK) {
			sprintf(msg, "BioAPI_GetBIRFromHandle failed.(0x%08x)", rc);
			MessageBox(msg);
			return;
		}

		duplicateBIR( &BIR, &BIRfromH ) ;
		BioAPI_Free( BIRfromH.BiometricData.Data ) ;
		BioAPI_Free( BIRfromH.SecurityBlock.Data ) ;
		m_BIRList.push_back( BIR ) ;

		
		m_BIRHandleList[nIndex].Purpose = 0;
		m_BIRHandleList[nIndex].Type = 0;
	} else {
		
		BIR = m_BIRList[ CreateTemplateDlg.m_SelectedIndex ] ;
	}

	
	BIR2.BiometricData.Data = new BYTE[BIR.BiometricData.Length];
	BIR2.BiometricData.Length = BIR.BiometricData.Length;
	memcpy(BIR2.BiometricData.Data,BIR.BiometricData.Data,BIR.BiometricData.Length);
	BIR2.Header = BIR.Header;	
	BIR2.SecurityBlock.Data = NULL ;
	BIR2.SecurityBlock.Length = 0 ;

	
	certBIR.BiometricData.Data = NULL;
	certBIR.BiometricData.Length = 0;
	certBIR.SecurityBlock.Data = NULL;
	certBIR.SecurityBlock.Length = 0;

	BioAPI_RETURN rc = HiBioAPI_DigitalSignBIR(&BIR2, &certBIR);
	if (rc != BioAPI_OK) {
		sprintf(msg, "HiBioAPI_DigitalSignBIR failed.(0x%08x)", rc);
		delete[] BIR2.BiometricData.Data;BIR2.BiometricData.Data = NULL ;
		MessageBox(msg);
		return;
	}

	
	certBIR.SecurityBlock.Data = new BYTE[certBIR.SecurityBlock.Length];	
	certBIR.BiometricData.Length = BIR.BiometricData.Length;	
	certBIR.BiometricData.Data = new BYTE[BIR.BiometricData.Length];	
	memcpy(certBIR.BiometricData.Data,BIR.BiometricData.Data,BIR.BiometricData.Length);	
	certBIR.Header = BIR.Header;

	rc = HiBioAPI_DigitalSignBIR(&BIR2, &certBIR);
	if (rc != BioAPI_OK) {
		sprintf(msg, "HiBioAPI_DigitalSignBIR failed.(0x%08x)", rc);
		delete []BIR2.BiometricData.Data;BIR2.BiometricData.Data = NULL;
		delete []certBIR.BiometricData.Data;certBIR.BiometricData.Data = NULL;
		delete []certBIR.SecurityBlock.Data;certBIR.SecurityBlock.Data = NULL;
		MessageBox(msg);
		return;
	}

	m_BIRList.push_back(certBIR);
	char szFileName[ 128 ] ;
	sprintf( szFileName, "WORK%d.BIR", m_FileBIRNum ) ;
	HANDLE hFile = CreateFile( szFileName, GENERIC_WRITE, 0, NULL, CREATE_NEW, 0, NULL ) ;
	if ( hFile != INVALID_HANDLE_VALUE ) {
		DWORD dwBytes ;
		WriteFile( hFile, &( certBIR.Header ), sizeof( certBIR.Header ), &dwBytes, NULL ) ;

		WriteFile( hFile, &( certBIR.BiometricData.Length ), sizeof( certBIR.BiometricData.Length ), &dwBytes, NULL ) ;
		WriteFile( hFile, certBIR.BiometricData.Data, certBIR.BiometricData.Length, &dwBytes, NULL ) ;

		WriteFile( hFile, &( certBIR.SecurityBlock.Length ), sizeof( certBIR.SecurityBlock.Length ), &dwBytes, NULL ) ;
		WriteFile( hFile, certBIR.SecurityBlock.Data, certBIR.SecurityBlock.Length, &dwBytes, NULL ) ;

		CloseHandle( hFile ) ;
		m_FileBIRNum++ ;
	}

	delete []BIR2.BiometricData.Data;BIR2.BiometricData.Data = NULL;

	sprintf(msg, "HiBioAPI_DigitalSignBIR successful.\n Type:(0x%08x)", certBIR.Header.Type);
	MessageBox(msg);

}
void CBioAPI_TestDlg::OnBnClickedDigitalsignbir2()
{
	char msg[256];
	BioAPI_RETURN rc = BioAPI_OK;

	InitCertificate initCert;
	initCert.m_appName = m_appName ;

	if(initCert.DoModal() ==IDCANCEL)return;

	m_appName = initCert.m_appName ;

	BioAPI_STRING appName ;
	memcpy(appName,initCert.m_appName,initCert.m_appName.GetLength()+1);

	rc = HiBioAPI_SetEndUserParameter( &appName, &initCert.m_certid );
	if (rc != BioAPI_OK) {
		sprintf(msg, "HiBioAPI_SetEndUserParameter failed.(0x%08x)", rc);
		MessageBox(msg);
		return;
	}
	sprintf(msg, "HiBioAPI_SetEndUserParameter successful.", rc);
	MessageBox(msg);

}

void CBioAPI_TestDlg::freeBSPSchemaList( BioAPI_BSP_SCHEMA **ppBSPSchemaList, uint32_t *pNumBSP )
{
	BioAPI_BSP_SCHEMA *pBSPSchemaList = *ppBSPSchemaList ;
	uint32_t nNumBSP = *pNumBSP ;

	if ( pBSPSchemaList == NULL ) {
		return ;
	}

	for ( uint32_t i = 0 ; i < nNumBSP ; i++ )
	{
		BioAPI_Free( pBSPSchemaList[ i ].Path ) ;
		BioAPI_Free( pBSPSchemaList[ i ].BSPSupportedFormats ) ;
	}

	BioAPI_Free( pBSPSchemaList ) ;
	*ppBSPSchemaList = NULL ;

	*pNumBSP = 0 ;
}

void CBioAPI_TestDlg::initBIR( BioAPI_BIR *pBIR )
{
	pBIR->BiometricData.Length = 0 ;
	pBIR->BiometricData.Data = NULL ;

	pBIR->SecurityBlock.Length = 0 ;
	pBIR->SecurityBlock.Data = NULL ;
}

void CBioAPI_TestDlg::freeBIR( BioAPI_BIR *pBIR )
{
	delete[] pBIR->BiometricData.Data ;
	delete[] pBIR->SecurityBlock.Data ;

	pBIR->BiometricData.Data = NULL ;
	pBIR->BiometricData.Length = 0 ;
	pBIR->SecurityBlock.Data = NULL ;
	pBIR->SecurityBlock.Length = 0 ;

	return ;
}

BOOL CBioAPI_TestDlg::getSecurityCode( const BioAPI_BIR *pReferenceBIR, BioAPI_BOOL Result, BioAPI_FMR FMRAchieved, const CChallenge *pChallenge, BioAPI_DATA *pSecurityCode )
{
	BOOL bRc = FALSE ;

	
	BYTE bHashCode[ 20 ] ;
	DWORD dwHashCodeSize = sizeof( bHashCode ) ;
	DWORD dwResult = getHashCode(	( BYTE * )pReferenceBIR->BiometricData.Data,
									pReferenceBIR->BiometricData.Length,
									bHashCode, &dwHashCodeSize ) ;
	if ( dwResult != ERROR_SUCCESS ) {
		return( FALSE ) ;
	}

	
	char szEncodeHash[ 50 ] ;
	ZeroMemory( szEncodeHash, sizeof( szEncodeHash ) ) ;
	size_t uEncodeHashSize = sizeof( szEncodeHash ) ;
	base64Encode( bHashCode, dwHashCodeSize, ( unsigned char * )szEncodeHash, &uEncodeHashSize ) ;

	char szSecurityData[ 512 ] ;
	ZeroMemory( szSecurityData, sizeof( szSecurityData ) ) ;
	char szWork[ 100 ] ;

	
	itoa( Result, szWork, 10 ) ;
	strcpy( szSecurityData, szWork ) ;

	
	itoa( pReferenceBIR->Header.Quality, szWork, 10 ) ;
	strcat( szSecurityData, szWork ) ;

	
	itoa( FMRAchieved, szWork, 10 ) ;
	strcat( szSecurityData, szWork ) ;

	
	strcat( szSecurityData, szEncodeHash ) ;

	
	strcat( szSecurityData, pChallenge->m_challenge ) ;
	DWORD dwSecurityDataSize = ( DWORD )strlen( szSecurityData ) ;

	
	if ( pChallenge->m_SelectedAlg == 0 ) {
		
		dwResult = signMessage( pChallenge->m_pCertContext, ( BYTE * )szSecurityData, dwSecurityDataSize, NULL, ( DWORD * )&( pSecurityCode->Length ) ) ;
		if ( dwResult != ERROR_SUCCESS ) {
			return( FALSE ) ;
		}
		pSecurityCode->Data = new BYTE[ pSecurityCode->Length ] ;
		dwResult = signMessage( pChallenge->m_pCertContext, ( BYTE * )szSecurityData, dwSecurityDataSize, ( BYTE * )pSecurityCode->Data, ( DWORD * )&( pSecurityCode->Length ) ) ;
		if ( dwResult != ERROR_SUCCESS ) {
			return( FALSE ) ;
		}
	} else {
		// HMAC
		pSecurityCode->Data = new BYTE[ 20 ] ;
		dwResult = createHMAC(	( BYTE * )szSecurityData, dwSecurityDataSize,
								( BYTE * )pSecurityCode->Data, ( DWORD * )&( pSecurityCode->Length ),
								( const BYTE * )( const char * )( pChallenge->m_MacSeed ), pChallenge->m_MacSeed.GetLength() ) ;
		if ( dwResult != ERROR_SUCCESS ) {
			return( FALSE ) ;
		}
	}

	return( TRUE ) ;
}

BOOL CBioAPI_TestDlg::getSecurityCode(
	uint32_t NumberOfResults,
	BioAPI_QUALITY Quality,
	const BioAPI_CANDIDATE *aCandidates,
	const CChallenge *pChallenge,
	BioAPI_DATA *pSecurityCode )
{
	DWORD dwResult ;

	DWORD dwSecurityDataBuffSize = 512 + 22 * NumberOfResults ;
	char *szSecurityData = new char[ dwSecurityDataBuffSize ] ;
	ZeroMemory( szSecurityData, dwSecurityDataBuffSize ) ;
	char szWork[ 100 ] ;

	
	itoa( NumberOfResults, szWork, 10 ) ;
	strcpy( szSecurityData, szWork ) ;

	
	itoa( Quality, szWork, 10 ) ;
	strcat( szSecurityData, szWork ) ;

	if ( NumberOfResults != 0 ) {
		for ( unsigned int i = 0 ; i < NumberOfResults ; i++ ) {
			
			itoa( *( aCandidates[ i ].BIR.BIRInArray ), szWork, 10 ) ;
			strcat( szSecurityData, szWork ) ;

			
			itoa( aCandidates[ i ].FMRAchieved, szWork, 10 ) ;
			strcat( szSecurityData, szWork ) ;
		}
	}

	
	strcat( szSecurityData, pChallenge->m_challenge ) ;
	DWORD dwSecurityDataSize = ( DWORD )strlen( szSecurityData ) ;

	
	if ( pChallenge->m_SelectedAlg == 0 ) {
		
		dwResult = signMessage( pChallenge->m_pCertContext, ( BYTE * )szSecurityData, dwSecurityDataSize, NULL, ( DWORD * )&( pSecurityCode->Length ) ) ;
		if ( dwResult != ERROR_SUCCESS ) {
			return( FALSE ) ;
		}
		pSecurityCode->Data = new BYTE[ pSecurityCode->Length ] ;
		dwResult = signMessage( pChallenge->m_pCertContext, ( BYTE * )szSecurityData, dwSecurityDataSize, ( BYTE * )pSecurityCode->Data, ( DWORD * )&( pSecurityCode->Length ) ) ;
		if ( dwResult != ERROR_SUCCESS ) {
			return( FALSE ) ;
		}
	} else {
		
		pSecurityCode->Data = new BYTE[ 20 ] ;
		dwResult = createHMAC(	( BYTE * )szSecurityData, dwSecurityDataSize,
								( BYTE * )pSecurityCode->Data, ( DWORD * )&( pSecurityCode->Length ),
								( const BYTE * )( const char * )( pChallenge->m_MacSeed ), pChallenge->m_MacSeed.GetLength() ) ;
		if ( dwResult != ERROR_SUCCESS ) {
			return( FALSE ) ;
		}
	}

	delete[] szSecurityData ;

	return( TRUE ) ;
}


DWORD CBioAPI_TestDlg::getHashCode( BYTE *pbHashData, DWORD dwHashDataLen, BYTE *pbHashCode, DWORD *pdwHashCodeSize )
{
	HCRYPTPROV	hProv ;
	HCRYPTHASH	hHash ;

	
	BOOL bResult = CryptAcquireContext( &hProv, NULL, NULL, PROV_RSA_FULL, NULL ) ;
	DWORD dwErrorNo = GetLastError() ;
	
	if ( !bResult && ( dwErrorNo == NTE_BAD_KEYSET ) ) {
		
		if ( !CryptAcquireContext( &hProv, NULL, NULL, PROV_RSA_FULL, CRYPT_NEWKEYSET ) ) {
			return GetLastError() ;
		}
	}

	
	if ( !CryptCreateHash( hProv, CALG_SHA1, 0, 0, &hHash ) ) {
		return GetLastError() ;
	}

	
	if ( !CryptHashData( hHash, pbHashData, dwHashDataLen, 0 ) ) {
		return GetLastError() ;
	}

	
	if ( !CryptGetHashParam( hHash, HP_HASHVAL, pbHashCode, pdwHashCodeSize, 0 ) ) {
		return GetLastError();
	}

	CryptDestroyHash( hHash ) ;
	CryptReleaseContext( hProv, 0 ) ;

	return ERROR_SUCCESS ;
}

static const unsigned char base64_table[65] ="ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz0123456789+/";

int CBioAPI_TestDlg::base64Encode(const unsigned char *src, size_t len,unsigned char *out,size_t *out_len)
{
         unsigned char *pos;
         const unsigned char *end, *in;
         size_t olen;
         int line_len;

         olen = len * 4 / 3 + 4; 
         olen += olen / 72; 
         olen++; 
//		 out = new unsigned char [olen];
         if (out == NULL)
                 return NULL;
 
         end = src + len;
         in = src;
         pos = out;
         line_len = 0;
         while (end - in >= 3) {
                 *pos++ = base64_table[in[0] >> 2];
                 *pos++ = base64_table[((in[0] & 0x03) << 4) | (in[1] >> 4)];
                 *pos++ = base64_table[((in[1] & 0x0f) << 2) | (in[2] >> 6)];
                 *pos++ = base64_table[in[2] & 0x3f];
                 in += 3;
                 line_len += 4;
                 if (line_len >= 72) {
                         *pos++ = '\n';
                         line_len = 0;
                 }
         }
 
         if (end - in) {
                 *pos++ = base64_table[in[0] >> 2];
                 if (end - in == 1) {
                         *pos++ = base64_table[(in[0] & 0x03) << 4];
                         *pos++ = '=';
                 } else {
                         *pos++ = base64_table[((in[0] & 0x03) << 4) |
                                               (in[1] >> 4)];
                         *pos++ = base64_table[(in[1] & 0x0f) << 2];
                 }
                 *pos++ = '=';
                 line_len += 4;
         }
 
         if (line_len)
                 *pos++ = '\n';
 
         *pos = '\0';
         if (out_len)
                 *out_len = pos - out;
         return true;
 }

DWORD CBioAPI_TestDlg::signMessage(
	PCCERT_CONTEXT pCertContext,
	BYTE *pbMessage,
	DWORD pbMessagelen,
	BYTE *pbSignedMsgBlob,
	DWORD *pbSignedMsgBloblen )
{
	
	CRYPT_SIGN_MESSAGE_PARA	sigPara;
	ZeroMemory( &sigPara, sizeof( sigPara ) ) ;
	sigPara.cbSize = sizeof( sigPara ) ;
	sigPara.dwMsgEncodingType = X509_ASN_ENCODING | PKCS_7_ASN_ENCODING ;
	sigPara.pSigningCert = pCertContext ;
	sigPara.HashAlgorithm.pszObjId = szOID_RSA_SHA1RSA ;
	sigPara.HashAlgorithm.Parameters.cbData = NULL ;
	sigPara.cMsgCert = 1 ;
	sigPara.rgpMsgCert = &pCertContext ;

	const BYTE	*messageArray[] = {pbMessage};
	DWORD		msgSizeArray[1];
	msgSizeArray[0] = pbMessagelen ;

	
	if ( pbSignedMsgBlob == NULL )
	{
		if( !CryptSignMessage( &sigPara, FALSE, 1, messageArray, msgSizeArray,
								NULL, pbSignedMsgBloblen ) )
		{
			return GetLastError();
		}else{
			return ERROR_SUCCESS;
		}
	}

	
	if ( !CryptSignMessage( &sigPara, FALSE, 1, messageArray,
				msgSizeArray, pbSignedMsgBlob, pbSignedMsgBloblen ) )
	{
		return GetLastError() ;
	}

	return ERROR_SUCCESS ;
}

DWORD CBioAPI_TestDlg::createHMAC(
	BYTE *pbData, DWORD dwDataLen,
	BYTE *pbHash, DWORD *pdwHashLen,
	const BYTE *pbSeed, const DWORD dwSeedLen )
{
	DWORD dwResult ;
	HCRYPTPROV hProv ;
	HCRYPTHASH hHash ;
	HCRYPTKEY hKey ;

	
	BOOL bResult = CryptAcquireContext( &hProv, NULL, NULL, PROV_RSA_FULL, NULL ) ;
	DWORD dwErrorNo = GetLastError() ;
	
	if ( !bResult && ( dwErrorNo == NTE_BAD_KEYSET ) ) {
		
		if ( !CryptAcquireContext( &hProv, NULL, NULL, PROV_RSA_FULL, CRYPT_NEWKEYSET ) ) {
			return GetLastError() ;
		}
	}

	
	if ( !CryptCreateHash( hProv, CALG_SHA1, 0, 0, &hHash ) ) {
		return GetLastError() ;
	}

	
	if ( !CryptHashData( hHash, pbSeed, dwSeedLen, 0 ) ) {
		return GetLastError() ;
	}

	
	if ( !CryptDeriveKey( hProv, CALG_3DES, hHash, CRYPT_CREATE_SALT | CRYPT_EXPORTABLE, &hKey ) ) {
		return GetLastError() ;
	}
	CryptDestroyHash( hHash ) ;

	
	if ( !CryptCreateHash( hProv, CALG_HMAC, hKey, 0, &hHash ) ) {
		return GetLastError() ;
	}

	HMAC_INFO hmacInfo ;
	ZeroMemory( &hmacInfo, sizeof( hmacInfo ) ) ;
	hmacInfo.HashAlgid = CALG_SHA1 ;

	
	dwResult = CryptSetHashParam( hHash, HP_HMAC_INFO, ( BYTE * )&hmacInfo, 0 ) ;
	if ( dwResult == FALSE ) {
		return GetLastError() ;
	}

	
	if ( !CryptHashData( hHash, pbData, dwDataLen, 0 ) ) {
		return GetLastError() ;
	}

	
	if ( !CryptGetHashParam( hHash, HP_HASHVAL, pbHash, pdwHashLen, 0 ) ) {
		return GetLastError() ;
	}

	CryptDestroyKey( hKey ) ;
	CryptDestroyHash( hHash ) ;
	CryptReleaseContext( hProv, 0 ) ;

	return( ERROR_SUCCESS ) ;
}

DWORD CBioAPI_TestDlg::duplicateBIR( BioAPI_BIR *pDest, const BioAPI_BIR *pSrc )
{
	DWORD dwRc = 0 ;

	
	pDest->Header = pSrc->Header ;

	
	pDest->BiometricData.Length = pSrc->BiometricData.Length ;
	if ( pDest->BiometricData.Length > 0 ) {
		pDest->BiometricData.Data = new BYTE[ pDest->BiometricData.Length ] ;
		memcpy( pDest->BiometricData.Data, pSrc->BiometricData.Data, pDest->BiometricData.Length ) ;
	} else {
		pDest->BiometricData.Data = NULL ;
	}

	
	pDest->SecurityBlock.Length = pSrc->SecurityBlock.Length ;
	if ( pDest->SecurityBlock.Length > 0 ) {
		pDest->SecurityBlock.Data = new BYTE[ pDest->SecurityBlock.Length ] ;
		memcpy( pDest->SecurityBlock.Data, pSrc->SecurityBlock.Data, pDest->SecurityBlock.Length ) ;
	} else {
		pDest->SecurityBlock.Data = NULL ;
	}

	return dwRc ;
}
