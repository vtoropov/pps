#pragma once
#include "afxwin.h"
#include "Define.h"

// CInputVerifyMatchDlg Dialog

class CInputVerifyMatchDlg : public CDialog
{
	DECLARE_DYNAMIC(CInputVerifyMatchDlg)

public:
	BIRArray m_BIRList;
	BIRHandleArray m_BIRHandleList;
	int m_SelectedBaseImage;
	int m_SelectedReferenceImage;

public:
	CInputVerifyMatchDlg(CWnd* pParent = NULL);   // standard constructor
	virtual ~CInputVerifyMatchDlg();

// Dialog Data
	enum { IDD = IDD_DIALOG_VERIFYMATCH };

protected:
	virtual void DoDataExchange(CDataExchange* pDX);    // DDX/DDV support

	DECLARE_MESSAGE_MAP()
public:
	CListBox m_BaseImageList;
	CListBox m_ReferenceImageList;
	BOOL m_BaseImageKind;
	BOOL m_ReferenceImageKind;
	int m_FMRThreshold;
	virtual BOOL OnInitDialog();
	afx_msg void OnBnClickedRadio1();
	afx_msg void OnBnClickedRadio2();
	afx_msg void OnBnClickedRadio3();
	afx_msg void OnBnClickedRadio5();
	afx_msg void OnBnClickedOk();
};
