// Challenge.cpp
//

#include "stdafx.h"
#include "BioAPI_Test.h"
#include "Challenge.h"


// CChallenge dialog

IMPLEMENT_DYNAMIC(CChallenge, CDialog)
CChallenge::CChallenge(CWnd* pParent /*=NULL*/)
	: CDialog(CChallenge::IDD, pParent)
	, m_challenge(_T(""))
	, m_SelectedAlg(0)
	, m_MacSeed(_T(""))
{
	m_pCertContext = NULL ;
}

CChallenge::~CChallenge()
{
	if ( m_pCertContext != NULL ) {
		CertFreeCertificateContext( m_pCertContext ) ;
		m_pCertContext = NULL ;
	}
}

void CChallenge::DoDataExchange(CDataExchange* pDX)
{
	CDialog::DoDataExchange(pDX);
	DDX_Text(pDX, IDC_EDIT1, m_challenge);
	DDX_Control(pDX, IDC_ALG_LIST, m_AlgList);
	DDX_Text(pDX, IDC_EDIT_MACSEED, m_MacSeed);
	DDX_Control(pDX, IDC_CERT_LIST, m_CertList);
}


BEGIN_MESSAGE_MAP(CChallenge, CDialog)
	ON_BN_CLICKED(IDOK, OnBnClickedOk)
END_MESSAGE_MAP()


// CChallenge message handlers

#define HI_TEST_APP_CERT_STORE_NAME "ROOT"

BOOL CChallenge::OnInitDialog()
{
	CDialog::OnInitDialog();

	
	m_AlgList.AddString("HI_BIOAPI_ALG_SIGNATURE");
	m_AlgList.AddString("HI_BIOAPI_ALG_HMAC");
	m_AlgList.SetCurSel( 1 );

	HCERTSTORE hCertStore = NULL ;
	PCCERT_CONTEXT pCertContext = NULL ;
	char szNameString[256];

	hCertStore = ::CertOpenSystemStore( NULL, HI_TEST_APP_CERT_STORE_NAME ) ;

	for (;;) {	
		pCertContext = ::CertEnumCertificatesInStore(hCertStore, pCertContext);
		if (pCertContext == NULL) {	
			break;
		}

		
		::CertGetNameString(
			pCertContext,
			CERT_NAME_SIMPLE_DISPLAY_TYPE,
			0,
			NULL,
			szNameString,
			256);

		
		m_CertList.AddString( szNameString );
	}

	m_CertList.SetCurSel( 0 );

	if (pCertContext)
		::CertFreeCertificateContext(pCertContext);
	if (hCertStore) 
		::CertCloseStore(hCertStore, CERT_CLOSE_STORE_CHECK_FLAG);

	return TRUE;  // return TRUE unless you set the focus to a control
	
}

void CChallenge::OnBnClickedOk()
{
	
	m_SelectedAlg = m_AlgList.GetCurSel();

	if ( m_SelectedAlg == 0 ) {
		HCERTSTORE hCertStore = NULL ;
		PCCERT_CONTEXT pCertContext = NULL ;
		char szNameString[256];
		wchar_t wszNameString[256];

		int selectedIndex = m_CertList.GetCurSel();
		m_CertList.GetText( selectedIndex, szNameString ) ;
		mbstowcs( wszNameString, szNameString, 256 ) ;

		hCertStore = ::CertOpenSystemStore( NULL, HI_TEST_APP_CERT_STORE_NAME ) ;
		pCertContext = CertFindCertificateInStore(
				hCertStore,
				PKCS_7_ASN_ENCODING | X509_ASN_ENCODING,
				0,
				CERT_FIND_SUBJECT_STR,
				wszNameString,
				NULL ) ;

		BOOL bResult = CryptFindCertificateKeyProvInfo( pCertContext, CRYPT_FIND_USER_KEYSET_FLAG, NULL ) ;
		if ( bResult == FALSE ) {
			MessageBox( "no private key found for this cert." ) ;
			return ;
		}

		m_pCertContext = CertDuplicateCertificateContext( pCertContext ) ;

		if (pCertContext)
			::CertFreeCertificateContext(pCertContext);
		if (hCertStore) 
			::CertCloseStore(hCertStore, CERT_CLOSE_STORE_CHECK_FLAG);
	}

	UpdateData(TRUE);

	OnOK();
}
