#pragma once
#include "afxwin.h"
#include "Define.h"

// CInputFreeDlg dialog

class CInputFreeDlg : public CDialog
{
	DECLARE_DYNAMIC(CInputFreeDlg)

public:
	int m_SelectedIndex;

public:
	CInputFreeDlg(CWnd* pParent = NULL);   // standard constructor
	virtual ~CInputFreeDlg();

// Dialog Data
	enum { IDD = IDD_DIALOG_FREE };

protected:
	virtual void DoDataExchange(CDataExchange* pDX);    // DDX/DDV support

	DECLARE_MESSAGE_MAP()
public:
	CListBox m_BIRIDList;
	virtual BOOL OnInitDialog();
	afx_msg void OnBnClickedOk();
};
