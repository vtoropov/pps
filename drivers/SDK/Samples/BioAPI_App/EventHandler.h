#include "bioapi.h"

BioAPI_RETURN BioAPI EventHandler(
					const BioAPI_UUID		*BSPUuid,
					BioAPI_UNIT_ID			UnitId,		
					void *                          AppNotifyCallbackCtx, // the main windows handle
					const BioAPI_UNIT_SCHEMA	*UnitSchema,
					BioAPI_EVENT			 eventType);

BioAPI_RETURN BioAPI GuiStateCallback(
	void *GuiStateCallbackCtx,
	BioAPI_GUI_STATE GuiState,
	BioAPI_GUI_RESPONSE *Response,
	BioAPI_GUI_MESSAGE Message,
	BioAPI_GUI_PROGRESS Progress,
	const BioAPI_GUI_BITMAP *SampleBuffer);

BioAPI_RETURN BioAPI GuiStreamingCallback(
	void *GuiStreamingCallbackCtx,
	const BioAPI_GUI_BITMAP *Bitmap);
