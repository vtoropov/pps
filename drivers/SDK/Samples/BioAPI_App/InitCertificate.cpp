// InitCertificate.cpp
//

#include "stdafx.h"
#include "BioAPI_Test.h"
#include "InitCertificate.h"


// InitCertificate dialog

IMPLEMENT_DYNAMIC(InitCertificate, CDialog)
InitCertificate::InitCertificate(CWnd* pParent /*=NULL*/)
	: CDialog(InitCertificate::IDD, pParent)
	, m_appName(_T(""))
{
	m_certid.Issuer.cbData = 0 ;
	m_certid.Issuer.pbData = NULL ;

	m_certid.SerialNumber.cbData = 0 ;
	m_certid.SerialNumber.pbData = NULL ;
}

InitCertificate::~InitCertificate()
{
	if ( m_certid.Issuer.pbData != NULL ) {
		delete[] m_certid.Issuer.pbData ;
		m_certid.Issuer.pbData = NULL ;
	}

	if ( m_certid.SerialNumber.pbData != NULL ) {
		delete[] m_certid.SerialNumber.pbData ;
		m_certid.SerialNumber.pbData = NULL ;
	}
}

void InitCertificate::DoDataExchange(CDataExchange* pDX)
{
	CDialog::DoDataExchange(pDX);
	DDX_Text(pDX, IDC_EDIT1, m_appName);
	DDX_Control(pDX, IDC_CERT_LIST, m_CertList);
}


BEGIN_MESSAGE_MAP(InitCertificate, CDialog)
	ON_BN_CLICKED(IDOK, OnBnClickedOk)
END_MESSAGE_MAP()


// InitCertificate message handlers

#define HI_TEST_APP_CERT_STORE_NAME "ROOT"
//#define HI_TEST_APP_CERT_STORE_NAME "MY"

BOOL InitCertificate::OnInitDialog()
{
	CDialog::OnInitDialog();

	HCERTSTORE hCertStore = NULL ;
	PCCERT_CONTEXT pCertContext = NULL ;
	char szNameString[256];

	hCertStore = ::CertOpenSystemStore( NULL, HI_TEST_APP_CERT_STORE_NAME ) ;

	for (;;) {	
		pCertContext = ::CertEnumCertificatesInStore(hCertStore, pCertContext);
		if (pCertContext == NULL) {	
			break;
		}

		
		::CertGetNameString(
			pCertContext,
			CERT_NAME_SIMPLE_DISPLAY_TYPE,
			0,
			NULL,
			szNameString,
			256);

		
		m_CertList.AddString( szNameString );
	}

	m_CertList.SetCurSel( 0 );

	if (pCertContext)
		::CertFreeCertificateContext(pCertContext);
	if (hCertStore) 
		::CertCloseStore(hCertStore, CERT_CLOSE_STORE_CHECK_FLAG);

	return TRUE;  // return TRUE unless you set the focus to a control
	
}

void InitCertificate::OnBnClickedOk()
{

	HCERTSTORE hCertStore = NULL ;
	PCCERT_CONTEXT pCertContext = NULL ;
	char szNameString[256];
	wchar_t wszNameString[256];

	int selectedIndex = m_CertList.GetCurSel();
	m_CertList.GetText( selectedIndex, szNameString ) ;
	mbstowcs( wszNameString, szNameString, 256 ) ;

	hCertStore = ::CertOpenSystemStore( NULL, HI_TEST_APP_CERT_STORE_NAME ) ;
	pCertContext = CertFindCertificateInStore(
			hCertStore,
			PKCS_7_ASN_ENCODING | X509_ASN_ENCODING,
			0,
			CERT_FIND_SUBJECT_STR,
			wszNameString,
			NULL ) ;

	BOOL bResult = CryptFindCertificateKeyProvInfo( pCertContext, CRYPT_FIND_USER_KEYSET_FLAG, NULL ) ;
	if ( bResult == FALSE ) {
		MessageBox( "no private key found for this cert." ) ;
		return ;
	}

	m_certid.Issuer.cbData = pCertContext->pCertInfo->Issuer.cbData ;
	m_certid.Issuer.pbData = new BYTE[ m_certid.Issuer.cbData ] ;
	memcpy( m_certid.Issuer.pbData, pCertContext->pCertInfo->Issuer.pbData, m_certid.Issuer.cbData ) ;

	m_certid.SerialNumber.cbData = pCertContext->pCertInfo->SerialNumber.cbData ;
	m_certid.SerialNumber.pbData = new BYTE[ m_certid.SerialNumber.cbData ] ;
	memcpy( m_certid.SerialNumber.pbData, pCertContext->pCertInfo->SerialNumber.pbData, m_certid.SerialNumber.cbData ) ;

	UpdateData(TRUE);

	if (pCertContext)
		::CertFreeCertificateContext(pCertContext);
	if (hCertStore) 
		::CertCloseStore(hCertStore, CERT_CLOSE_STORE_CHECK_FLAG);

	OnOK();
}
