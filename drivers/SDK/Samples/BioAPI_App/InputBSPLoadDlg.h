#pragma once
#include "afxwin.h"
#include "Define.h"


// CInputBSPLoadDlg dialog

class CInputBSPLoadDlg : public CDialog
{
	DECLARE_DYNAMIC(CInputBSPLoadDlg)

public:
	CInputBSPLoadDlg(CWnd* pParent = NULL);   // standard constructor
	virtual ~CInputBSPLoadDlg();

// Dialog Data
	enum { IDD = IDD_DIALOG_BSPLOAD };

protected:
	StrArray m_strBSPNameList;
	int m_nBSPNum;
	int m_radioEventOnOff;

protected:
	virtual void DoDataExchange(CDataExchange* pDX);    // DDX/DDV support

	DECLARE_MESSAGE_MAP()
public:
	CComboBox m_BSPList;
	void SetBSPList(StrArray strBSPNameList);
	int GetBSPNum();
	int GetEventOnOff();
	afx_msg void OnCbnSelchangeComboBsplist();
	afx_msg void OnBnClickedOk();
	virtual BOOL OnInitDialog();
	CButton m_btnOK;
};
