#pragma once
#include "afxwin.h"
#include "Define.h"


// CInputGetHeaderFromHandle Dialog

class CInputGetHeaderFromHandleDlg : public CDialog
{
	DECLARE_DYNAMIC(CInputGetHeaderFromHandleDlg)

public:
	BIRHandleArray m_BIRHandleList;
	int m_SelectedIndex;

public:
	CInputGetHeaderFromHandleDlg(CWnd* pParent = NULL);   // standard constructor
	virtual ~CInputGetHeaderFromHandleDlg();

// Dialog Data
	enum { IDD = IDD_DIALOG_GETHEADERFROMHANDLE };

protected:
	virtual void DoDataExchange(CDataExchange* pDX);    // DDX/DDV support

	DECLARE_MESSAGE_MAP()
public:
	CListBox m_BIRIDList;
	virtual BOOL OnInitDialog();
	afx_msg void OnBnClickedOk();
};
