// InputSetGUICallbacks.cpp
//

#include "stdafx.h"
#include "BioAPI_Test.h"
#include "InputSetGUICallbacksDlg.h"
#include ".\inputsetguicallbacksDlg.h"


// CInputSetGUICallbacksDlg dialog

IMPLEMENT_DYNAMIC(CInputSetGUICallbacksDlg, CDialog)
CInputSetGUICallbacksDlg::CInputSetGUICallbacksDlg(CWnd* pParent /*=NULL*/)
	: CDialog(CInputSetGUICallbacksDlg::IDD, pParent)
	, m_UseStateCallback(FALSE)
	, m_UseStreamingCallback(FALSE)
{
}

CInputSetGUICallbacksDlg::~CInputSetGUICallbacksDlg()
{
}

void CInputSetGUICallbacksDlg::DoDataExchange(CDataExchange* pDX)
{
	CDialog::DoDataExchange(pDX);
	DDX_Radio(pDX, IDC_RADIO1, m_UseStateCallback);
	DDX_Radio(pDX, IDC_RADIO3, m_UseStreamingCallback);
}


BEGIN_MESSAGE_MAP(CInputSetGUICallbacksDlg, CDialog)
	ON_BN_CLICKED(IDOK, OnBnClickedOk)
END_MESSAGE_MAP()


// CInputSetGUICallbacksDlg message handlers

BOOL CInputSetGUICallbacksDlg::OnInitDialog()
{
	CDialog::OnInitDialog();

	
	m_UseStateCallback = 0;
	m_UseStreamingCallback = 0;

	return TRUE;  // return TRUE unless you set the focus to a control
	
}

void CInputSetGUICallbacksDlg::OnBnClickedOk()
{
	
	UpdateData(TRUE);

	OnOK();
}
