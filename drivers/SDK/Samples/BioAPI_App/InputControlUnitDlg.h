#pragma once


// CInputControlUnitDlg dialog

class CInputControlUnitDlg : public CDialog
{
	DECLARE_DYNAMIC(CInputControlUnitDlg)

public:
	CInputControlUnitDlg(CWnd* pParent = NULL);   // standard constructor
	virtual ~CInputControlUnitDlg();

// Dialog Data
	enum { IDD = IDD_DIALOG_CONTROLUNIT };

protected:
	virtual void DoDataExchange(CDataExchange* pDX);    // DDX/DDV support

	DECLARE_MESSAGE_MAP()
public:
	int m_nControlCode;
	int m_nInputData0;
	int m_nInputData1;
	int m_nInputData2;
	int m_nInputData3;
	int m_nUnitID;
	int m_nInputDataSize;
};
