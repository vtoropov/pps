#pragma once
#include "afxwin.h"
#include "Define.h"


// CInputIdentifyDlg Dialog

class CInputIdentifyDlg : public CDialog
{
	DECLARE_DYNAMIC(CInputIdentifyDlg)

public:
	BIRArray m_BIRList;
	int m_TemplateIndexList[256];
	int m_TemplateListSize;

public:
	CInputIdentifyDlg(CWnd* pParent = NULL);   // standard constructor
	virtual ~CInputIdentifyDlg();

// Dialog Data
	enum { IDD = IDD_DIALOG_IDENTIFY };

protected:
	virtual void DoDataExchange(CDataExchange* pDX);    // DDX/DDV support

	DECLARE_MESSAGE_MAP()
public:
	CListBox m_TemplateBIRList;
	int m_FMR;
	int m_Timeout;
	int m_MaxNumberOfResults;
	virtual BOOL OnInitDialog();
	afx_msg void OnBnClickedOk();
};
