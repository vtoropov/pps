// BioAPI_TestDlg.h
//

#pragma once


#include "bioapi.h"
#include "hi_bioapi_api.h"
#include "hi_bioapi.h"
#include "Define.h"

class CChallenge ;

// CBioAPI_TestDlg dialog
class CBioAPI_TestDlg : public CDialog
{
// Construction
public:
	CBioAPI_TestDlg(CWnd* pParent = NULL);	// standard constructor
	~CBioAPI_TestDlg();
	
// Dialog Data
	enum { IDD = IDD_BIOAPI_TEST_DIALOG };

	protected:
	virtual void DoDataExchange(CDataExchange* pDX);	// DDX/DDV support


private:
	BIRHandleArray m_BIRHandleList;	
	BIRArray m_BIRList;	

	BioAPI_HANDLE m_BSPHandle;	

	StrArray m_BSPNameList;	
	int m_nCrtBSPNum;	
	StrArray m_UnitIDList;	
	BioAPI_VERSION m_BioAPIVersion;	
	BioAPI_FRAMEWORK_SCHEMA m_FrameworkSchema;	
	BioAPI_BSP_SCHEMA *m_BSPSchemaList;	
	BioAPI_UNIT_SCHEMA *m_UnitSchemaList;	
	uint32_t m_NumBSP;	
	BioAPI_EventHandler m_EventHandler;
	HWND m_EventHandlerCxt;
	int m_FileBIRNum ;

	void freeBSPSchemaList( BioAPI_BSP_SCHEMA **, uint32_t * ) ;
	void initBIR( BioAPI_BIR * ) ;
	void freeBIR( BioAPI_BIR * ) ;
	BOOL getSecurityCode( const BioAPI_BIR *, BioAPI_BOOL, BioAPI_FMR, const CChallenge *, BioAPI_DATA * ) ;
	BOOL getSecurityCode( uint32_t, BioAPI_QUALITY, const BioAPI_CANDIDATE *,const CChallenge *, BioAPI_DATA * ) ;
	DWORD getHashCode( BYTE *, DWORD, BYTE *, DWORD * ) ;
	int base64Encode( const unsigned char *, size_t, unsigned char *, size_t * ) ;
	DWORD signMessage( PCCERT_CONTEXT, BYTE *, DWORD, BYTE *, DWORD * ) ;
	DWORD createHMAC( BYTE *, DWORD, BYTE *, DWORD *, const BYTE *, const DWORD ) ;
	DWORD duplicateBIR( BioAPI_BIR *, const BioAPI_BIR * ) ;
	CString m_appName ;
	BioAPI_FMR m_FMR ;

	
	int m_GuiStreamingCallbackCtx ;
	int m_GuiStateCallbackCtx ;

// Implementation
protected:
	HICON m_hIcon;
	BioAPI_BIR certBIR;

	// Generated message map functions
	virtual BOOL OnInitDialog();
	afx_msg void OnSysCommand(UINT nID, LPARAM lParam);
	afx_msg void OnPaint();
	afx_msg HCURSOR OnQueryDragIcon();
	DECLARE_MESSAGE_MAP()
public:
	afx_msg void OnBnClickedButtonInit();
	afx_msg void OnBnClickedButtonTerminate();
	afx_msg void OnBnClickedButtonGetFrameworkInfo();
	afx_msg void OnBnClickedButtonEnumBsps();
	afx_msg void OnBnClickedButtonBspLoad();
	afx_msg void OnBnClickedButtonBspUnload();
	afx_msg void OnBnClickedButtonBspAttach();
	afx_msg void OnBnClickedButtonBspDetach();
	afx_msg void OnBnClickedButtonQueryUnits();
	afx_msg void OnBnClickedButtonControlUnit();
	afx_msg void OnBnClickedButtonFreeBirHandle();
	afx_msg void OnBnClickedButtonGetBirFromHandle();
	afx_msg void OnBnClickedButtonGetHeaderFromHandle();
	afx_msg void OnBnClickedButtonEnableEvents();
	afx_msg void OnBnClickedButtonSetGuiCallbacks();
	afx_msg void OnBnClickedButtonCapture();
	afx_msg void OnBnClickedButtonCreateTemplate();
	afx_msg void OnBnClickedButtonProcess();
	afx_msg void OnBnClickedButtonVerifyMatch();
	afx_msg void OnBnClickedButtonIdentifyMatch();
	afx_msg void OnBnClickedButtonEnroll();
	afx_msg void OnBnClickedButtonVerify();
	afx_msg void OnBnClickedButtonIdentify();
	afx_msg void OnBnClickedButtonCancel();
	afx_msg void OnBnClickedButtonFree();
	afx_msg void OnBnClickedButtonAttachProcess();
	afx_msg void OnBnClickedDigitalsignbir();
	afx_msg void OnBnClickedDeleteparameter();
	afx_msg void OnBnClickedVerifybsp();
	afx_msg void OnBnClickedVerifychallenge();
	afx_msg void OnBnClickedIdentifychallenge();
	afx_msg void OnBnClickedVerifybir();
	afx_msg void OnBnClickedDigitalsignbir2();

	void doBspDetach();
	void doControlUnit();
	void doFreeBirHandle();
	void doGetBirFromHandle();
	void doGetHeaderFromHandle();
	void doSetGuiCallbacks();
	void doCapture();
	void doCreateTemplate();
	void doProcess();
	void doVerifyMatch();
	void doIdentifyMatch();
	void doEnroll();
	void doVerify();
	void doIdentify();
	void doCancel();

};
