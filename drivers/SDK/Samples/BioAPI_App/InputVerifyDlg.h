#pragma once
#include "afxwin.h"
#include "Define.h"


// CInputVerifyDlg Dialog

class CInputVerifyDlg : public CDialog
{
	DECLARE_DYNAMIC(CInputVerifyDlg)

public:
	BIRArray m_BIRList;
	BIRHandleArray m_BIRHandleList;
	int m_SelectedIndex;

public:
	CInputVerifyDlg(CWnd* pParent = NULL);   // standard constructor
	virtual ~CInputVerifyDlg();

// Dialog Data
	enum { IDD = IDD_DIALOG_VERIFY };

protected:
	virtual void DoDataExchange(CDataExchange* pDX);    // DDX/DDV support

	DECLARE_MESSAGE_MAP()
public:
	CListBox m_BIRIDList;
	int m_InputDataKind;
	int m_Timeout;
	int m_FMRThreshold;
	virtual BOOL OnInitDialog();
	afx_msg void OnBnClickedRadio1();
	afx_msg void OnBnClickedRadio2();
	afx_msg void OnBnClickedOk();
};
