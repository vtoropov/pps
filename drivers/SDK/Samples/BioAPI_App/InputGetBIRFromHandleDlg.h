#pragma once
#include "afxwin.h"
#include "Define.h"

// CInputGetBIRFromHandleDlg Dialog

class CInputGetBIRFromHandleDlg : public CDialog
{
	DECLARE_DYNAMIC(CInputGetBIRFromHandleDlg)

public:
	BIRHandleArray m_BIRHandleList;
	int m_SelectedIndex;

public:
	CInputGetBIRFromHandleDlg(CWnd* pParent = NULL);   // standard constructor
	virtual ~CInputGetBIRFromHandleDlg();

// Dialog Data
	enum { IDD = IDD_DIALOG_GETBIRFROMHANDLE };

protected:
	virtual void DoDataExchange(CDataExchange* pDX);    // DDX/DDV support

	DECLARE_MESSAGE_MAP()
public:
	CListBox m_BIRIDList;
	virtual BOOL OnInitDialog();
	afx_msg void OnBnClickedOk();
};
