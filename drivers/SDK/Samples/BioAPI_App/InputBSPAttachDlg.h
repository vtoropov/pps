#pragma once
#include "afxwin.h"
#include "Define.h"

// CInputBSPAttachDlg dialog

class CInputBSPAttachDlg : public CDialog
{
	DECLARE_DYNAMIC(CInputBSPAttachDlg)

public:
	StrArray m_UnitIDList;
	int m_SelectedUnitNumList[4];
	int m_NumList;

	void SetUnitIDList(StrArray UnitIDList);
	int GetSelectedUnitNumList(int SelectedUnitNumList[]);

public:
	CInputBSPAttachDlg(CWnd* pParent = NULL);   // standard constructor
	virtual ~CInputBSPAttachDlg();

// Dialog Data
	enum { IDD = IDD_DIALOG_BSPATTACH };

protected:
	virtual void DoDataExchange(CDataExchange* pDX);    // DDX/DDV support

	DECLARE_MESSAGE_MAP()
public:
	CListBox m_UnitList;
	virtual BOOL OnInitDialog();
	afx_msg void OnBnClickedOk();
};
