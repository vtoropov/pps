#pragma once
#include "afxwin.h"


// CInputSetGUICallbacks Dialog

class CInputSetGUICallbacksDlg : public CDialog
{
	DECLARE_DYNAMIC(CInputSetGUICallbacksDlg)

public:
	CInputSetGUICallbacksDlg(CWnd* pParent = NULL);   // standard constructor
	virtual ~CInputSetGUICallbacksDlg();

// Dialog Data
	enum { IDD = IDD_DIALOG_SETGUICALLBACKS };

protected:
	virtual void DoDataExchange(CDataExchange* pDX);    // DDX/DDV support

	DECLARE_MESSAGE_MAP()
public:
	virtual BOOL OnInitDialog();
	int m_UseStateCallback;
	int m_UseStreamingCallback;
	afx_msg void OnBnClickedOk();
};
