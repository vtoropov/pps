#pragma once

#include <wincrypt.h>

// InitCertificate dialog

class InitCertificate : public CDialog
{
	DECLARE_DYNAMIC(InitCertificate)

public:
	InitCertificate(CWnd* pParent = NULL);   // standard constructor
	virtual ~InitCertificate();

// Dialog Data
	enum { IDD = IDD_DIALOG_INITCERTIFICATE };

protected:
	virtual void DoDataExchange(CDataExchange* pDX);    // DDX/DDV support

	DECLARE_MESSAGE_MAP()
public:
	CString m_appName;
	CERT_ISSUER_SERIAL_NUMBER m_certid ;
	CListBox m_CertList;

	virtual BOOL OnInitDialog();
	afx_msg void OnBnClickedOk();
};
