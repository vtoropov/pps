// InputCreateTemplateDlg.cpp
//

#include "stdafx.h"
#include "BioAPI_Test.h"
#include "InputCreateTemplateDlg.h"
#include ".\inputcreatetemplatedlg.h"


// CInputCreateTemplateDlg dialog

IMPLEMENT_DYNAMIC(CInputCreateTemplateDlg, CDialog)
CInputCreateTemplateDlg::CInputCreateTemplateDlg(CWnd* pParent /*=NULL*/)
	: CDialog(CInputCreateTemplateDlg::IDD, pParent)
	, m_InputDataKind(FALSE)
{
}

CInputCreateTemplateDlg::~CInputCreateTemplateDlg()
{
}

void CInputCreateTemplateDlg::DoDataExchange(CDataExchange* pDX)
{
	CDialog::DoDataExchange(pDX);
	DDX_Control(pDX, IDC_LIST1, m_BIRIDList);
	DDX_Radio(pDX, IDC_RADIO1, m_InputDataKind);
}


BEGIN_MESSAGE_MAP(CInputCreateTemplateDlg, CDialog)
	ON_BN_CLICKED(IDC_RADIO1, OnBnClickedRadio1)
	ON_BN_CLICKED(IDC_RADIO2, OnBnClickedRadio2)
	ON_BN_CLICKED(IDOK, OnBnClickedOk)
END_MESSAGE_MAP()


// CInputCreateTemplateDlg message handlers

BOOL CInputCreateTemplateDlg::OnInitDialog()
{
	char buf[256];

	CDialog::OnInitDialog();
	
	

	m_InputDataKind = 0;

	for (BIRHandleArray::iterator e = m_BIRHandleList.begin(); e != m_BIRHandleList.end(); e++) {
		sprintf(buf, "BIRHandle : BIRHandle = %08x, Purpose = %d, Type = %d", e->Handle, e->Purpose, e->Type);
		m_BIRIDList.AddString(buf);
	}

	return TRUE;  // return TRUE unless you set the focus to a control
	
}

void CInputCreateTemplateDlg::OnBnClickedRadio1()
{
	
	char buf[256];
	m_BIRIDList.ResetContent();
	for (BIRHandleArray::iterator e = m_BIRHandleList.begin(); e != m_BIRHandleList.end(); e++) {
		sprintf(buf, "BIRHandle : BIRHandle = %08x, Purpose = %d, Type = %d", e->Handle, e->Purpose, e->Type);
		m_BIRIDList.AddString(buf);
	}
	UpdateData(TRUE);

}

void CInputCreateTemplateDlg::OnBnClickedRadio2()
{
	
	char buf[256];
	m_BIRIDList.ResetContent();
	for (BIRArray::iterator e = m_BIRList.begin(); e != m_BIRList.end(); e++) {
		sprintf(buf, "BIR : Date = %04d/%02d/%02d %02d:%02d:%02d, Purpose = %d, Type = %d", 
			e->Header.CreationDTG.Date.Year, e->Header.CreationDTG.Date.Month, e->Header.CreationDTG.Date.Day, 
			e->Header.CreationDTG.Time.Hour, e->Header.CreationDTG.Time.Minute, e->Header.CreationDTG.Time.Second, 
			e->Header.Purpose, e->Header.Type);
		m_BIRIDList.AddString(buf);
	}
	UpdateData(TRUE);
}

void CInputCreateTemplateDlg::OnBnClickedOk()
{
	
	UpdateData(FALSE);
	m_SelectedIndex = m_BIRIDList.GetCurSel();
	if ( m_SelectedIndex == LB_ERR ) {
		return ;
	}

	OnOK();
}
