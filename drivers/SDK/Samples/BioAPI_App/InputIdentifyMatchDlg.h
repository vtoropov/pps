#pragma once
#include "afxwin.h"
#include "Define.h"

// CInputIdentifyMatchDlg Dialog

class CInputIdentifyMatchDlg : public CDialog
{
	DECLARE_DYNAMIC(CInputIdentifyMatchDlg)

public:
	BIRArray m_BIRList;
	BIRHandleArray m_BIRHandleList;
	int m_ProcessedIndex;
	int m_TemplateIndexList[256];
	int m_TemplateListSize;

public:
	CInputIdentifyMatchDlg(CWnd* pParent = NULL);   // standard constructor
	virtual ~CInputIdentifyMatchDlg();

// Dialog Data
	enum { IDD = IDD_DIALOG_IDENTIFYMATCH };

protected:
	virtual void DoDataExchange(CDataExchange* pDX);    // DDX/DDV support

	DECLARE_MESSAGE_MAP()
public:
	CListBox m_ProcessedBIRList;
	CListBox m_TemplateBIRList;
	virtual BOOL OnInitDialog();
	afx_msg void OnBnClickedOk();
	int m_InputBIRKind;
	afx_msg void OnBnClickedRadio1();
	afx_msg void OnBnClickedRadio2();
	int m_FMR;
	int m_Timeout;
	int m_MaxNumberOfResults;
};
