// InputFreeDlg.cpp
//

#include "stdafx.h"
#include "BioAPI_Test.h"
#include "InputFreeDlg.h"
#include ".\inputfreedlg.h"


// CInputFreeDlg dialog

IMPLEMENT_DYNAMIC(CInputFreeDlg, CDialog)
CInputFreeDlg::CInputFreeDlg(CWnd* pParent /*=NULL*/)
	: CDialog(CInputFreeDlg::IDD, pParent)
{
}

CInputFreeDlg::~CInputFreeDlg()
{
}

void CInputFreeDlg::DoDataExchange(CDataExchange* pDX)
{
	CDialog::DoDataExchange(pDX);
	DDX_Control(pDX, IDC_LIST1, m_BIRIDList);
}


BEGIN_MESSAGE_MAP(CInputFreeDlg, CDialog)
	ON_BN_CLICKED(IDOK, OnBnClickedOk)
END_MESSAGE_MAP()


// CInputFreeDlg message handlers

BOOL CInputFreeDlg::OnInitDialog()
{
	CDialog::OnInitDialog();

	
	m_BIRIDList.AddString("BSPSchemaList");
	m_BIRIDList.AddString("UnitSchemaList");

	return TRUE;  // return TRUE unless you set the focus to a control
	
}

void CInputFreeDlg::OnBnClickedOk()
{
	
	UpdateData(FALSE);
	m_SelectedIndex = m_BIRIDList.GetCurSel();
	if ( m_SelectedIndex == LB_ERR ) {
		return ;
	}

	OnOK();
}
