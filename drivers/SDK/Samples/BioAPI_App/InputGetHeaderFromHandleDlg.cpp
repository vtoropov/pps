// InputGetHeaderFromHandle.cpp
//

#include "stdafx.h"
#include "BioAPI_Test.h"
#include "InputGetHeaderFromHandleDlg.h"
#include ".\inputgetheaderfromhandleDlg.h"


// CInputGetHeaderFromHandle dialog

IMPLEMENT_DYNAMIC(CInputGetHeaderFromHandleDlg, CDialog)
CInputGetHeaderFromHandleDlg::CInputGetHeaderFromHandleDlg(CWnd* pParent /*=NULL*/)
	: CDialog(CInputGetHeaderFromHandleDlg::IDD, pParent)
{
}

CInputGetHeaderFromHandleDlg::~CInputGetHeaderFromHandleDlg()
{
}

void CInputGetHeaderFromHandleDlg::DoDataExchange(CDataExchange* pDX)
{
	CDialog::DoDataExchange(pDX);
	DDX_Control(pDX, IDC_LIST1, m_BIRIDList);
}


BEGIN_MESSAGE_MAP(CInputGetHeaderFromHandleDlg, CDialog)
	ON_BN_CLICKED(IDOK, OnBnClickedOk)
END_MESSAGE_MAP()


// CInputGetHeaderFromHandleDlg message handlers

BOOL CInputGetHeaderFromHandleDlg::OnInitDialog()
{
	CDialog::OnInitDialog();
	char buf[256];

	
	for (BIRHandleArray::iterator e = m_BIRHandleList.begin(); e != m_BIRHandleList.end(); e++) {
		sprintf(buf, "BIRHandle = %08x, Purpose = %d, Type = %d", e->Handle, e->Purpose, e->Type);
		m_BIRIDList.AddString(buf);
	}


	return TRUE;  // return TRUE unless you set the focus to a control
	
}

void CInputGetHeaderFromHandleDlg::OnBnClickedOk()
{
	
	UpdateData(FALSE);

	m_SelectedIndex = m_BIRIDList.GetCurSel();
	if ( m_SelectedIndex == LB_ERR ) {
		return ;
	}

	OnOK();
}
