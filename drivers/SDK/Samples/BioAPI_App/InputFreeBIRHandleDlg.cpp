// InputFreeBIRHandleDlg.cpp
//

#include "stdafx.h"
#include "BioAPI_Test.h"
#include "InputFreeBIRHandleDlg.h"
#include ".\inputfreebirhandledlg.h"


// CInputFreeBIRHandleDlg dialog

IMPLEMENT_DYNAMIC(CInputFreeBIRHandleDlg, CDialog)
CInputFreeBIRHandleDlg::CInputFreeBIRHandleDlg(CWnd* pParent /*=NULL*/)
	: CDialog(CInputFreeBIRHandleDlg::IDD, pParent)
{
}

CInputFreeBIRHandleDlg::~CInputFreeBIRHandleDlg()
{
}

void CInputFreeBIRHandleDlg::DoDataExchange(CDataExchange* pDX)
{
	CDialog::DoDataExchange(pDX);
	DDX_Control(pDX, IDC_LIST1, m_BIRIDList);
}


BEGIN_MESSAGE_MAP(CInputFreeBIRHandleDlg, CDialog)
	ON_BN_CLICKED(IDOK, OnBnClickedOk)
END_MESSAGE_MAP()


// CInputFreeBIRHandleDlg message handlers

BOOL CInputFreeBIRHandleDlg::OnInitDialog()
{
	CDialog::OnInitDialog();
	char buf[256];

	
	for (BIRHandleArray::iterator e = m_BIRHandleList.begin(); e != m_BIRHandleList.end(); e++) {
		sprintf(buf, "BIRHandle = %08x, Purpose = %d, Type = %d", e->Handle, e->Purpose, e->Type);
		m_BIRIDList.AddString(buf);
	}


	return TRUE;  // return TRUE unless you set the focus to a control
	
}

void CInputFreeBIRHandleDlg::OnBnClickedOk()
{
	
	UpdateData(FALSE);

	m_SelectedIndex = m_BIRIDList.GetCurSel();
	if ( m_SelectedIndex == LB_ERR ) {
		return ;
	}

	OnOK();
}
