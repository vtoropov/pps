// InputGetBIRFromHandleDlg.cpp
//

#include "stdafx.h"
#include "BioAPI_Test.h"
#include "InputGetBIRFromHandleDlg.h"
#include ".\inputgetbirfromhandledlg.h"


// CInputGetBIRFromHandleDlg dialog

IMPLEMENT_DYNAMIC(CInputGetBIRFromHandleDlg, CDialog)
CInputGetBIRFromHandleDlg::CInputGetBIRFromHandleDlg(CWnd* pParent /*=NULL*/)
	: CDialog(CInputGetBIRFromHandleDlg::IDD, pParent)
{
}

CInputGetBIRFromHandleDlg::~CInputGetBIRFromHandleDlg()
{
}

void CInputGetBIRFromHandleDlg::DoDataExchange(CDataExchange* pDX)
{
	CDialog::DoDataExchange(pDX);
	DDX_Control(pDX, IDC_LIST1, m_BIRIDList);
}


BEGIN_MESSAGE_MAP(CInputGetBIRFromHandleDlg, CDialog)
	ON_BN_CLICKED(IDOK, OnBnClickedOk)
END_MESSAGE_MAP()


// CInputGetBIRFromHandleDlg message handlers

BOOL CInputGetBIRFromHandleDlg::OnInitDialog()
{
	CDialog::OnInitDialog();
	char buf[256];

	
	for (BIRHandleArray::iterator e = m_BIRHandleList.begin(); e != m_BIRHandleList.end(); e++) {
		sprintf(buf, "BIRHandle = %08x, Purpose = %d, Type = %d", e->Handle, e->Purpose, e->Type);
		m_BIRIDList.AddString(buf);
	}


	return TRUE;  // return TRUE unless you set the focus to a control
	
}

void CInputGetBIRFromHandleDlg::OnBnClickedOk()
{
	
	UpdateData(FALSE);

	m_SelectedIndex = m_BIRIDList.GetCurSel();
	if ( m_SelectedIndex == LB_ERR ) {
		return ;
	}

	OnOK();
}
