#pragma once


// deleteParameter dialog

class deleteParameter : public CDialog
{
	DECLARE_DYNAMIC(deleteParameter)

public:
	deleteParameter(CWnd* pParent = NULL);   // standard constructor
	virtual ~deleteParameter();

// Dialog Data
	enum { IDD = IDD_DIALOG_DELETEPARAMETER };

protected:
	virtual void DoDataExchange(CDataExchange* pDX);    // DDX/DDV support

	DECLARE_MESSAGE_MAP()
public:
	CString m_appName;
};
