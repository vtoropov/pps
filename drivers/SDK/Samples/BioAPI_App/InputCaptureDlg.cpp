// InputCaptureDlg.cpp
//

#include "stdafx.h"
#include "BioAPI_Test.h"
#include "InputCaptureDlg.h"


// CInputCaptureDlg dialog

IMPLEMENT_DYNAMIC(CInputCaptureDlg, CDialog)
CInputCaptureDlg::CInputCaptureDlg(CWnd* pParent /*=NULL*/)
	: CDialog(CInputCaptureDlg::IDD, pParent)
	, m_nPurpose(0)
	, m_nTimeout(0)
	, m_SelectedIndex( 0 )
{
}

CInputCaptureDlg::~CInputCaptureDlg()
{
}

void CInputCaptureDlg::DoDataExchange(CDataExchange* pDX)
{
	CDialog::DoDataExchange(pDX);
	DDX_Text(pDX, IDC_EDIT8, m_nTimeout);
	DDX_Control(pDX, IDC_PUR_COMBO, m_PurBox);
}


BEGIN_MESSAGE_MAP(CInputCaptureDlg, CDialog)
	ON_BN_CLICKED(IDOK, OnBnClickedOk)
END_MESSAGE_MAP()


// CInputCaptureDlg message handlers

BOOL CInputCaptureDlg::OnInitDialog()
{
	CDialog::OnInitDialog();

	
	m_PurBox.AddString("BioAPI_PURPOSE_VERIFY");
	m_PurBox.AddString("BioAPI_PURPOSE_IDENTIFY");
	m_PurBox.AddString("BioAPI_PURPOSE_ENROLL");

	switch ( m_nPurpose ) {
	case 1 :
	case 2 :
	case 3 :
		m_PurBox.SetCurSel( m_nPurpose - 1 );
		break ;
	default :
		m_PurBox.SetCurSel( 2 );
		m_PurBox.EnableWindow( FALSE );
		break ;
	}

	return TRUE;  // return TRUE unless you set the focus to a control
	
}

void CInputCaptureDlg::OnBnClickedOk()
{
	
	m_SelectedIndex = m_PurBox.GetCurSel();
	UpdateData(TRUE);

	OnOK();
}
