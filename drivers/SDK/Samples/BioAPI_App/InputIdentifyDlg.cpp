// InputIdentifyDlg.cpp
//

#include "stdafx.h"
#include "BioAPI_Test.h"
#include "InputIdentifyDlg.h"
#include ".\inputidentifydlg.h"


// CInputIdentifyDlg dialog

IMPLEMENT_DYNAMIC(CInputIdentifyDlg, CDialog)
CInputIdentifyDlg::CInputIdentifyDlg(CWnd* pParent /*=NULL*/)
	: CDialog(CInputIdentifyDlg::IDD, pParent)
	, m_FMR(0)
	, m_Timeout(0)
	, m_MaxNumberOfResults(0)
{
}

CInputIdentifyDlg::~CInputIdentifyDlg()
{
}

void CInputIdentifyDlg::DoDataExchange(CDataExchange* pDX)
{
	CDialog::DoDataExchange(pDX);
	DDX_Control(pDX, IDC_LIST1, m_TemplateBIRList);
	DDX_Text(pDX, IDC_EDIT1, m_FMR);
	DDX_Text(pDX, IDC_EDIT8, m_Timeout);
	DDX_Text(pDX, IDC_EDIT9, m_MaxNumberOfResults);
}


BEGIN_MESSAGE_MAP(CInputIdentifyDlg, CDialog)
	ON_BN_CLICKED(IDOK, OnBnClickedOk)
END_MESSAGE_MAP()


// CInputIdentifyDlg message handlers

BOOL CInputIdentifyDlg::OnInitDialog()
{
	CDialog::OnInitDialog();

	
	char buf[256];
	for (BIRArray::iterator e = m_BIRList.begin(); e != m_BIRList.end(); e++) {
		sprintf(buf, "BIR : Date = %04d/%02d/%02d %02d:%02d:%02d, Purpose = %d, Type = %d", 
			e->Header.CreationDTG.Date.Year, e->Header.CreationDTG.Date.Month, e->Header.CreationDTG.Date.Day, 
			e->Header.CreationDTG.Time.Hour, e->Header.CreationDTG.Time.Minute, e->Header.CreationDTG.Time.Second, 
			e->Header.Purpose, e->Header.Type);
		m_TemplateBIRList.AddString(buf);
	}


	return TRUE;  // return TRUE unless you set the focus to a control
	
}

void CInputIdentifyDlg::OnBnClickedOk()
{
	
	UpdateData(TRUE);
	m_TemplateListSize = m_TemplateBIRList.GetSelItems(256, m_TemplateIndexList);

	OnOK();
}
