// InputBSPLoadDlg.cpp
//

#include "stdafx.h"
#include "BioAPI_Test.h"
#include "InputBSPLoadDlg.h"
#include ".\inputbsploaddlg.h"

// CInputBSPLoadDlg dialog

IMPLEMENT_DYNAMIC(CInputBSPLoadDlg, CDialog)
CInputBSPLoadDlg::CInputBSPLoadDlg(CWnd* pParent /*=NULL*/)
	: CDialog(CInputBSPLoadDlg::IDD, pParent)
	, m_radioEventOnOff(FALSE)
{
}

CInputBSPLoadDlg::~CInputBSPLoadDlg()
{
}

void CInputBSPLoadDlg::DoDataExchange(CDataExchange* pDX)
{
	CDialog::DoDataExchange(pDX);
	DDX_Control(pDX, IDC_COMBO_BSPLIST, m_BSPList);
	DDX_Radio(pDX, IDC_RADIO1, m_radioEventOnOff);
	DDX_Control(pDX, IDOK, m_btnOK);
}


BEGIN_MESSAGE_MAP(CInputBSPLoadDlg, CDialog)
	ON_CBN_SELCHANGE(IDC_COMBO_BSPLIST, OnCbnSelchangeComboBsplist)
	ON_BN_CLICKED(IDOK, OnBnClickedOk)
END_MESSAGE_MAP()


// CInputBSPLoadDlg message handlers

void CInputBSPLoadDlg::SetBSPList(StrArray strBSPNameList)
{
	m_strBSPNameList = strBSPNameList;
}

void CInputBSPLoadDlg::OnCbnSelchangeComboBsplist()
{
	

	int sel = m_BSPList.GetCurSel();
	if (sel >= 0) {
		m_btnOK.EnableWindow(TRUE);
	} else {
		m_btnOK.EnableWindow(FALSE);
	}
}

void CInputBSPLoadDlg::OnBnClickedOk()
{
	

    m_nBSPNum = m_BSPList.GetCurSel();
	if ( m_nBSPNum == LB_ERR ) {
		return ;
	}

	OnOK();
}

BOOL CInputBSPLoadDlg::OnInitDialog()
{
	CDialog::OnInitDialog();



	m_btnOK.EnableWindow(FALSE);

	for (StrArray::iterator e = m_strBSPNameList.begin(); e != m_strBSPNameList.end(); e++) {
		m_BSPList.AddString(*e);
		m_btnOK.EnableWindow(TRUE);
	}
	m_BSPList.SetCurSel( 0 ) ;

	m_radioEventOnOff = 0;

	return TRUE;  // return TRUE unless you set the focus to a control
	
}

int CInputBSPLoadDlg::GetBSPNum()
{
	return m_nBSPNum;
}

int CInputBSPLoadDlg::GetEventOnOff()
{
	return m_radioEventOnOff;
}
