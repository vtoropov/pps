#pragma once

#include <wincrypt.h>	// for CERT_ISSUER_SERIAL_NUMBER

// CChallenge dialog

class CChallenge : public CDialog
{
	DECLARE_DYNAMIC(CChallenge)

public:
	CChallenge(CWnd* pParent = NULL);   // standard constructor
	virtual ~CChallenge();

// Dialog Data
	enum { IDD = IDD_DIALOG_CHALLENGE };

protected:
	virtual void DoDataExchange(CDataExchange* pDX);    // DDX/DDV support

	DECLARE_MESSAGE_MAP()
public:
	virtual BOOL OnInitDialog();
	afx_msg void OnBnClickedOk();

	CString m_challenge;
	CListBox m_AlgList;
	int m_SelectedAlg;
	CString m_MacSeed ;
	PCCERT_CONTEXT m_pCertContext ;
	CListBox m_CertList;
};
