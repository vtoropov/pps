// InputControlUnitDlg.cpp
//

#include "stdafx.h"
#include "BioAPI_Test.h"
#include "InputControlUnitDlg.h"


// CInputControlUnitDlg dialog

IMPLEMENT_DYNAMIC(CInputControlUnitDlg, CDialog)
CInputControlUnitDlg::CInputControlUnitDlg(CWnd* pParent /*=NULL*/)
	: CDialog(CInputControlUnitDlg::IDD, pParent)
	, m_nControlCode(0)
	, m_nInputData0(0)
	, m_nInputData1(0)
	, m_nInputData2(0)
	, m_nInputData3(0)
	, m_nUnitID(0)
	, m_nInputDataSize(0)
{
}

CInputControlUnitDlg::~CInputControlUnitDlg()
{
}

void CInputControlUnitDlg::DoDataExchange(CDataExchange* pDX)
{
	CDialog::DoDataExchange(pDX);
	DDX_Text(pDX, IDC_EDIT1, m_nControlCode);
	DDX_Text(pDX, IDC_EDIT2, m_nInputData0);
	DDX_Text(pDX, IDC_EDIT3, m_nInputData1);
	DDX_Text(pDX, IDC_EDIT4, m_nInputData2);
	DDX_Text(pDX, IDC_EDIT5, m_nInputData3);
	DDX_Text(pDX, IDC_EDIT6, m_nUnitID);
	DDX_Text(pDX, IDC_EDIT7, m_nInputDataSize);
}


BEGIN_MESSAGE_MAP(CInputControlUnitDlg, CDialog)
END_MESSAGE_MAP()


// CInputControlUnitDlg message handlers
