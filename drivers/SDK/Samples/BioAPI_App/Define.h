#ifndef _DEFINE_H_INCLUDED
#define _DEFINE_H_INCLUDED

#include <vector>
#include "bioapi_type.h"

typedef struct bir_handle_info {
	BioAPI_BIR_HANDLE Handle;
	BioAPI_BIR_PURPOSE Purpose;
	BioAPI_BIR_DATA_TYPE Type;
} BIR_HANDLE_INFO;

typedef std::vector<CString> StrArray;
typedef std::vector<BIR_HANDLE_INFO> BIRHandleArray;
typedef std::vector<BioAPI_BIR> BIRArray;

#define SAMPLE_APP_FMR_DEFAULT 2072
#define SAMPLE_APP_TIMEOUT_DEFAULT 10000	

#endif // _DEFINE_H_INCLUDED
