#pragma once


// CInputCaptureDlg dialog

class CInputCaptureDlg : public CDialog
{
	DECLARE_DYNAMIC(CInputCaptureDlg)

public:
	CInputCaptureDlg(CWnd* pParent = NULL);   // standard constructor
	virtual ~CInputCaptureDlg();
	int m_SelectedIndex;

// Dialog Data
	enum { IDD = IDD_DIALOG_CAPTURE };

protected:
	virtual void DoDataExchange(CDataExchange* pDX);    // DDX/DDV support

	DECLARE_MESSAGE_MAP()
public:
	virtual BOOL OnInitDialog();
	afx_msg void OnBnClickedOk();

	CComboBox m_PurBox;
	int m_nPurpose;
	int m_nTimeout;
};
