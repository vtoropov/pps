#pragma once
#include "Define.h"
#include "afxwin.h"

// CInputCreateTemplateDlg dialog

class CInputCreateTemplateDlg : public CDialog
{
	DECLARE_DYNAMIC(CInputCreateTemplateDlg)

public:
	BIRArray m_BIRList;
	BIRHandleArray m_BIRHandleList;
	int m_SelectedIndex;

public:
	CInputCreateTemplateDlg(CWnd* pParent = NULL);   // standard constructor
	virtual ~CInputCreateTemplateDlg();

// Dialog Data
	enum { IDD = IDD_DIALOG_CREATETEMPLATE };

protected:
	virtual void DoDataExchange(CDataExchange* pDX);    // DDX/DDV support

	DECLARE_MESSAGE_MAP()
public:
	virtual BOOL OnInitDialog();
	CListBox m_BIRIDList;
	int m_InputDataKind;
	afx_msg void OnBnClickedRadio1();
	afx_msg void OnBnClickedRadio2();
	afx_msg void OnBnClickedOk();
};
