#pragma once


// verifyBSP Dialog

class verifyBSP : public CDialog
{
	DECLARE_DYNAMIC(verifyBSP)

public:
	verifyBSP(CWnd* pParent = NULL);   // standard constructor
	virtual ~verifyBSP();

// Dialog Data
	enum { IDD = IDD_DIALOG_VERIFYBSP };

protected:
	virtual void DoDataExchange(CDataExchange* pDX);    // DDX/DDV support

	DECLARE_MESSAGE_MAP()
public:
	CString m_bsphash;
	afx_msg void OnBnClickedCancel();
	afx_msg void OnBnClickedOk();
};
