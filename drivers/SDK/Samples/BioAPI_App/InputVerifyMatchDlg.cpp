// InputVerifyMatchDlg.cpp
//

#include "stdafx.h"
#include "BioAPI_Test.h"
#include "InputVerifyMatchDlg.h"
#include ".\inputverifymatchdlg.h"


// CInputVerifyMatchDlg dialog

IMPLEMENT_DYNAMIC(CInputVerifyMatchDlg, CDialog)
CInputVerifyMatchDlg::CInputVerifyMatchDlg(CWnd* pParent /*=NULL*/)
	: CDialog(CInputVerifyMatchDlg::IDD, pParent)
	, m_BaseImageKind(FALSE)
	, m_ReferenceImageKind(FALSE)
	, m_FMRThreshold(0)
{
}

CInputVerifyMatchDlg::~CInputVerifyMatchDlg()
{
}

void CInputVerifyMatchDlg::DoDataExchange(CDataExchange* pDX)
{
	CDialog::DoDataExchange(pDX);
	DDX_Control(pDX, IDC_LIST1, m_BaseImageList);
	DDX_Control(pDX, IDC_LIST2, m_ReferenceImageList);
	DDX_Radio(pDX, IDC_RADIO1, m_BaseImageKind);
	DDX_Radio(pDX, IDC_RADIO3, m_ReferenceImageKind);
	DDX_Text(pDX, IDC_EDIT1, m_FMRThreshold);
}


BEGIN_MESSAGE_MAP(CInputVerifyMatchDlg, CDialog)
	ON_BN_CLICKED(IDC_RADIO1, OnBnClickedRadio1)
	ON_BN_CLICKED(IDC_RADIO2, OnBnClickedRadio2)
	ON_BN_CLICKED(IDC_RADIO3, OnBnClickedRadio3)
	ON_BN_CLICKED(IDC_RADIO5, OnBnClickedRadio5)
	ON_BN_CLICKED(IDOK, OnBnClickedOk)
END_MESSAGE_MAP()


// CInputVerifyMatchDlg message handlers

BOOL CInputVerifyMatchDlg::OnInitDialog()
{
	CDialog::OnInitDialog();

	
	char buf[256];

	m_BaseImageKind = 0;
	m_ReferenceImageKind = 0;

	for (BIRHandleArray::iterator e = m_BIRHandleList.begin(); e != m_BIRHandleList.end(); e++) {
		sprintf(buf, "BIRHandle : BIRHandle = %08x, Purpose = %d, Type = %d", e->Handle, e->Purpose, e->Type);
		m_BaseImageList.AddString(buf);
	}
	for (BIRHandleArray::iterator e = m_BIRHandleList.begin(); e != m_BIRHandleList.end(); e++) {
		sprintf(buf, "BIRHandle : BIRHandle = %08x, Purpose = %d, Type = %d", e->Handle, e->Purpose, e->Type);
		m_ReferenceImageList.AddString(buf);
	}

	return TRUE;  // return TRUE unless you set the focus to a control
	
}

void CInputVerifyMatchDlg::OnBnClickedRadio1()
{
	
	char buf[256];
	m_BaseImageList.ResetContent();
	for (BIRHandleArray::iterator e = m_BIRHandleList.begin(); e != m_BIRHandleList.end(); e++) {
		sprintf(buf, "BIRHandle : BIRHandle = %08x, Purpose = %d, Type = %d", e->Handle, e->Purpose, e->Type);
		m_BaseImageList.AddString(buf);
	}
	UpdateData(TRUE);
}

void CInputVerifyMatchDlg::OnBnClickedRadio2()
{
	
	char buf[256];
	m_BaseImageList.ResetContent();
	for (BIRArray::iterator e = m_BIRList.begin(); e != m_BIRList.end(); e++) {
		sprintf(buf, "BIR : Date = %04d/%02d/%02d %02d:%02d:%02d, Purpose = %d, Type = %d", 
			e->Header.CreationDTG.Date.Year, e->Header.CreationDTG.Date.Month, e->Header.CreationDTG.Date.Day, 
			e->Header.CreationDTG.Time.Hour, e->Header.CreationDTG.Time.Minute, e->Header.CreationDTG.Time.Second, 
			e->Header.Purpose, e->Header.Type);
		m_BaseImageList.AddString(buf);
	}
	UpdateData(TRUE);
}

void CInputVerifyMatchDlg::OnBnClickedRadio3()
{
	
	char buf[256];
	m_ReferenceImageList.ResetContent();
	for (BIRHandleArray::iterator e = m_BIRHandleList.begin(); e != m_BIRHandleList.end(); e++) {
		sprintf(buf, "BIRHandle : BIRHandle = %08x, Purpose = %d, Type = %d", e->Handle, e->Purpose, e->Type);
		m_ReferenceImageList.AddString(buf);
	}
	UpdateData(TRUE);
}


void CInputVerifyMatchDlg::OnBnClickedRadio5()
{
	
	char buf[256];
	m_ReferenceImageList.ResetContent();
	for (BIRArray::iterator e = m_BIRList.begin(); e != m_BIRList.end(); e++) {
		sprintf(buf, "BIR : Date = %04d/%02d/%02d %02d:%02d:%02d, Purpose = %d, Type = %d", 
			e->Header.CreationDTG.Date.Year, e->Header.CreationDTG.Date.Month, e->Header.CreationDTG.Date.Day, 
			e->Header.CreationDTG.Time.Hour, e->Header.CreationDTG.Time.Minute, e->Header.CreationDTG.Time.Second, 
			e->Header.Purpose, e->Header.Type);
		m_ReferenceImageList.AddString(buf);
	}
	UpdateData(TRUE);
}

void CInputVerifyMatchDlg::OnBnClickedOk()
{
	

	UpdateData(TRUE);
	m_SelectedBaseImage = m_BaseImageList.GetCurSel();
	if ( m_SelectedBaseImage == LB_ERR ) {
		return ;
	}

	m_SelectedReferenceImage = m_ReferenceImageList.GetCurSel();
	if ( m_SelectedReferenceImage == LB_ERR ) {
		return ;
	}

	OnOK();
}
