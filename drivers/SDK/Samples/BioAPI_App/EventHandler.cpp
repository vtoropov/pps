#include "stdafx.h"
#include "EventHandler.h"

BioAPI_RETURN BioAPI EventHandler(
					const BioAPI_UUID		*BSPUuid,
					BioAPI_UNIT_ID			UnitId,		
					void *                          AppNotifyCallbackCtx, // the main windows handle
					const BioAPI_UNIT_SCHEMA	*UnitSchema,
					BioAPI_EVENT			 eventType)
{
	char msg[256];

	// Do not call MFC CWin object created by another thread, use m_hWnd instead 
	//HWND hWndMain = AfxGetApp()->m_pMainWnd->m_hWnd;
	HWND hWndMain = (HWND) AppNotifyCallbackCtx;

 	// add unit to the list if units
	switch (eventType)
	{
		case BioAPI_NOTIFY_INSERT:
			sprintf(msg, "A BioAPI_NOTIFY_INSERT event occurred on the UNIT %d.", UnitSchema->UnitId);
			MessageBox(hWndMain, msg, "Event detection", 0);

			break;

		case BioAPI_NOTIFY_REMOVE:

			sprintf(msg, "A BioAPI_NOTIFY_REMOVE event occurred.");
			MessageBox(hWndMain, msg, "Event detection", 0);

			break;

		case BioAPI_NOTIFY_FAULT:

			sprintf(msg, "A BioAPI_NOTIFY_FAULT event occurred.");
			MessageBox(hWndMain, msg, "Event detection", 0);

			break;

		case BioAPI_NOTIFY_SOURCE_PRESENT:

			sprintf(msg, "A BioAPI_NOTIFY_SOURCE_PRESENT event occurred.");
			MessageBox(hWndMain, msg, "Event detection", 0);

			break;

		case BioAPI_NOTIFY_SOURCE_REMOVED:

			sprintf(msg, "A BioAPI_NOTIFY_SOURCE_REMOVED event occurred.");
			MessageBox(hWndMain, msg, "Event detection", 0);

			break;

		default:
			sprintf( msg, "An unknown event occurred. EventType: 0x%08x", eventType ) ;
			MessageBox(hWndMain, msg, "Event detection", 0);

			break;
	}

	return BioAPI_OK;
}

BioAPI_RETURN BioAPI GuiStateCallback(
	void *GuiStateCallbackCtx,
	BioAPI_GUI_STATE GuiState,
	BioAPI_GUI_RESPONSE *Response,
	BioAPI_GUI_MESSAGE Message,
	BioAPI_GUI_PROGRESS Progress,
	const BioAPI_GUI_BITMAP *SampleBuffer)
{
	char msg[256];
	int *pCtx = NULL;
	*Response = BioAPI_CONTINUE ;

	if ((GuiState & BioAPI_MESSAGE_PROVIDED) == 0) {
		return BioAPI_OK;
	}

	if (GuiStateCallbackCtx != NULL) {
		pCtx = (int*)GuiStateCallbackCtx;
	}

	switch (Message) {
		case 0:	// PROCESS
			break;
		case 1:	// SUCCESS
			if (pCtx == NULL) {
				sprintf(msg, "Received a capture success message. (Cxt = NULL)");
			} else {
				sprintf(msg, "Received a capture success message. (Cxt = %d)", *pCtx);
			}
			MessageBox(NULL, msg, "GUI State Callback", 0);
			break;
		case 2:	// FAIL
			if (pCtx == NULL) {
				sprintf(msg, "Received a capture error message. (Cxt = NULL)");
			} else {
				sprintf(msg, "Received a capture error message. (Cxt = %d)", *pCtx);
			}
			MessageBox(NULL, msg, "GUI State Callback", 0);
			break;
		case 3:	// CANCEL
			if (pCtx == NULL) {
				sprintf(msg, "Received a capture cancel message. (Cxt = NULL)");
			} else {
				sprintf(msg, "Received a capture cancel message. (Cxt = %d)", *pCtx);
			}
			MessageBox(NULL, msg, "GUI State Callback", 0);
			break;
		case 4:	// TIMEOUT
			if (pCtx == NULL) {
				sprintf(msg, "Received a capture timeout message. (Cxt = NULL)");
			} else {
				sprintf(msg, "Received a capture timeout message. (Cxt = %d)", *pCtx);
			}
			MessageBox(NULL, msg, "GUI State Callback", 0);
			break;
		case 5:	// INSERT
			if (pCtx == NULL) {
				sprintf(msg, "Received a finger placement message. (Cxt = NULL)");
			} else {
				sprintf(msg, "Received a finger placement message. (Cxt = %d)", *pCtx);
			}
			MessageBox(NULL, msg, "GUI State Callback", 0);
			break;
		default:	// UNKOWN
			if (pCtx == NULL) {
				sprintf(msg, "Received an unknown message (0x%08x). (Cxt = NULL)", Message ) ;
			} else {
				sprintf(msg, "Received an unknown message (0x%08x). (Cxt = %d)", Message, *pCtx ) ;
			}
			MessageBox(NULL, msg, "GUI State Callback", 0);
			break;
	}

	return BioAPI_OK;
}

BioAPI_RETURN BioAPI GuiStreamingCallback(
	void *GuiStreamingCallbackCtx,
	const BioAPI_GUI_BITMAP *Bitmap)
{

	// nop

	return BioAPI_OK;
}

