// deleteParameter.cpp
//

#include "stdafx.h"
#include "BioAPI_Test.h"
#include "deleteParameter.h"


// deleteParameter dialog

IMPLEMENT_DYNAMIC(deleteParameter, CDialog)
deleteParameter::deleteParameter(CWnd* pParent /*=NULL*/)
	: CDialog(deleteParameter::IDD, pParent)
	, m_appName(_T(""))
{
}

deleteParameter::~deleteParameter()
{
}

void deleteParameter::DoDataExchange(CDataExchange* pDX)
{
	CDialog::DoDataExchange(pDX);
	DDX_Text(pDX, IDC_EDIT1, m_appName);
}


BEGIN_MESSAGE_MAP(deleteParameter, CDialog)
END_MESSAGE_MAP()


// deleteParameter message handlers
