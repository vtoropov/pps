// BioAPI_TestDlg.cpp : implementation file
//

#include "stdafx.h"
#include "BioAPI_Test.h"
#include "BioAPI_TestDlg.h"
#include ".\bioapi_testdlg.h"
#include "InitSecretKey.h"
#include "RestrictBSP.h"
#include "InitParameter.h"
#include "deleteParameter.h"
#include "InitCertificate.h"

#ifdef _DEBUG
#define new DEBUG_NEW
#endif

// CAboutDlg dialog used for App About

class CAboutDlg : public CDialog
{
public:
	CAboutDlg();

// Dialog Data
	enum { IDD = IDD_ABOUTBOX };

	protected:
	virtual void DoDataExchange(CDataExchange* pDX);    // DDX/DDV support

// Implementation
protected:
	DECLARE_MESSAGE_MAP()
};

CAboutDlg::CAboutDlg() : CDialog(CAboutDlg::IDD)
{
}

void CAboutDlg::DoDataExchange(CDataExchange* pDX)
{
	CDialog::DoDataExchange(pDX);
}

BEGIN_MESSAGE_MAP(CAboutDlg, CDialog)
END_MESSAGE_MAP()


// CBioAPI_TestDlg dialog



CBioAPI_TestDlg::CBioAPI_TestDlg(CWnd* pParent /*=NULL*/)
	: CDialog(CBioAPI_TestDlg::IDD, pParent)
	, m_appName( _T( "" ) )
	, m_PIN( _T( "" ) ) 
{
	m_hIcon = AfxGetApp()->LoadIcon(IDR_MAINFRAME);
}

void CBioAPI_TestDlg::DoDataExchange(CDataExchange* pDX)
{
	CDialog::DoDataExchange(pDX);
	DDX_Control(pDX, IDC_STATIC_TITLE, m_title);
}

BEGIN_MESSAGE_MAP(CBioAPI_TestDlg, CDialog)
	ON_WM_SYSCOMMAND()
	ON_WM_PAINT()
	ON_WM_QUERYDRAGICON()
	//}}AFX_MSG_MAP
	ON_BN_CLICKED(IDC_INITSECRETKEY, OnBnClickedInitsecretkey)
	ON_BN_CLICKED(IDC_RESTRICTBSP, OnBnClickedRestrictbsp)
	ON_BN_CLICKED(IDC_INITVERIFYPARAMETER, OnBnClickedInitverifyparameter)
	ON_BN_CLICKED(IDC_INITIDENTIFYPARAMETER2, OnBnClickedInitidentifyparameter)
	ON_BN_CLICKED(IDC_DELETEPARAMETER, OnBnClickedDeleteparameter)
	ON_BN_CLICKED(IDC_INITCERTIFICATE, OnBnClickedInitcertificate)
END_MESSAGE_MAP()


// CBioAPI_TestDlg message handlers

BOOL CBioAPI_TestDlg::OnInitDialog()
{
	CDialog::OnInitDialog();

	// Add "About..." menu item to system menu.

	// IDM_ABOUTBOX must be in the system command range.
	ASSERT((IDM_ABOUTBOX & 0xFFF0) == IDM_ABOUTBOX);
	ASSERT(IDM_ABOUTBOX < 0xF000);

	CMenu* pSysMenu = GetSystemMenu(FALSE);
	if (pSysMenu != NULL)
	{
		CString strAboutMenu;
		strAboutMenu.LoadString(IDS_ABOUTBOX);
		if (!strAboutMenu.IsEmpty())
		{
			pSysMenu->AppendMenu(MF_SEPARATOR);
			pSysMenu->AppendMenu(MF_STRING, IDM_ABOUTBOX, strAboutMenu);
		}
	}

	// Set the icon for this dialog.  The framework does this automatically
	//  when the application's main window is not a dialog
	SetIcon(m_hIcon, TRUE);			// Set big icon
	SetIcon(m_hIcon, FALSE);		// Set small icon

	// TODO: Add extra initialization here

    m_titleFont.CreateFont(16,0,0,0,FW_BOLD,FALSE,TRUE,0, 
          SHIFTJIS_CHARSET,OUT_DEFAULT_PRECIS,
      CLIP_DEFAULT_PRECIS,DEFAULT_QUALITY,
          DEFAULT_PITCH,"MS Shell Dlg");

	m_title.SetFont( &m_titleFont ) ; 

	return TRUE;  // return TRUE  unless you set the focus to a control
}

CBioAPI_TestDlg::~CBioAPI_TestDlg()
{
}

void CBioAPI_TestDlg::OnSysCommand(UINT nID, LPARAM lParam)
{
	if ((nID & 0xFFF0) == IDM_ABOUTBOX)
	{
		CAboutDlg dlgAbout;
		dlgAbout.DoModal();
	}
	else
	{
		CDialog::OnSysCommand(nID, lParam);
	}
}

// If you add a minimize button to your dialog, you will need the code below
//  to draw the icon.  For MFC applications using the document/view model,
//  this is automatically done for you by the framework.

void CBioAPI_TestDlg::OnPaint() 
{
	if (IsIconic())
	{
		CPaintDC dc(this); // device context for painting

		SendMessage(WM_ICONERASEBKGND, reinterpret_cast<WPARAM>(dc.GetSafeHdc()), 0);

		// Center icon in client rectangle
		int cxIcon = GetSystemMetrics(SM_CXICON);
		int cyIcon = GetSystemMetrics(SM_CYICON);
		CRect rect;
		GetClientRect(&rect);
		int x = (rect.Width() - cxIcon + 1) / 2;
		int y = (rect.Height() - cyIcon + 1) / 2;

		// Draw the icon
		dc.DrawIcon(x, y, m_hIcon);
	}
	else
	{
		CDialog::OnPaint();
	}
}

// The system calls this function to obtain the cursor to display while the user drags
//  the minimized window.
HCURSOR CBioAPI_TestDlg::OnQueryDragIcon()
{
	return static_cast<HCURSOR>(m_hIcon);
}

void CBioAPI_TestDlg::OnBnClickedInitsecretkey()
{
	char msg[256];
	BioAPI_RETURN rc = BioAPI_OK;

	InitSecretKey secret;
	secret.m_appName = m_appName ;
	secret.m_PIN = m_PIN ;

	if(secret.DoModal() ==IDCANCEL)return;

	m_appName = secret.m_appName ;

	BioAPI_STRING appName,seed,pin;
	ALG_ID AlgName ;
	memcpy(appName,secret.m_appName,secret.m_appName.GetLength()+1);
	if (secret.m_SelectedIndex == 0)
	{
		AlgName = CALG_3DES;
	}
	else
	{
		AlgName = CALG_AES_256;
	}
	memcpy(seed,secret.m_Seed,secret.m_Seed.GetLength()+1);
	memcpy( pin, secret.m_PIN, secret.m_PIN.GetLength() + 1 ) ;

	rc = HiBioAPI_InitSecretKey( &appName, AlgName, &seed, &pin ) ;
	if (rc != BioAPI_OK) {
		sprintf(msg, "HiBioAPI_InitSecretKey failed.(0x%08x)\n", rc);
		MessageBox(msg);
		return;
	}

	m_PIN = secret.m_PIN ;

	MessageBox("HiBioAPI_InitSecretKey successful.\n");
}

void CBioAPI_TestDlg::OnBnClickedRestrictbsp()
{
	char msg[256];
	BioAPI_RETURN rc = BioAPI_OK;

	RestrictBSP restrict;
	restrict.m_appName = m_appName ;
	restrict.m_usage = 4 ;
	restrict.m_operation_mask = 0 ;
	restrict.m_PIN = m_PIN ;

	if(restrict.DoModal() ==IDCANCEL)return;

	m_appName = restrict.m_appName ;

	BioAPI_STRING appName,pin;
	memcpy(appName,restrict.m_appName,restrict.m_appName.GetLength()+1);
	memcpy( pin, restrict.m_PIN, restrict.m_PIN.GetLength() + 1 ) ;

	rc = HiBioAPI_RestrictBSPFunction(&appName,restrict.m_usage,restrict.m_operation_mask,&pin);
	if (rc != BioAPI_OK) {
		sprintf(msg, "HiBioAPI_RestrictBSPFunction failed.(0x%08x)\n", rc);
		MessageBox(msg);
		return;
	}

	m_PIN = restrict.m_PIN ;

	sprintf(msg, "HiBioAPI_RestrictBSPFunction successful.\n", rc);
	MessageBox(msg);
}

void CBioAPI_TestDlg::OnBnClickedInitverifyparameter()
{
	char msg[256];
	BioAPI_RETURN rc = BioAPI_OK;

	InitParameter initPara(1) ;
	initPara.m_appName = m_appName ;
	initPara.m_PIN = m_PIN ;

	if(initPara.DoModal() ==IDCANCEL)return;

	m_appName = initPara.m_appName ;

	BioAPI_STRING appName,pin,cert;
	memcpy(appName,initPara.m_appName,initPara.m_appName.GetLength()+1);
	memcpy( pin, initPara.m_PIN, initPara.m_PIN.GetLength() + 1 ) ;
	memset(cert,0,initPara.m_certID.GetLength()+1);
	memcpy(cert,initPara.m_certID,initPara.m_certID.GetLength()+1);
	hi_bioapi_securitycodeID secID;
	if ( initPara.m_SelectedAlg == 0 ) {
		secID.securitycodeType = HI_BIOAPI_ALG_SIGNATURE ;
	} else {
		secID.securitycodeType = HI_BIOAPI_ALG_HMAC ;
		memset((char*)secID.macSeed,0,strlen((char*)cert));
		memcpy((char*)secID.macSeed,(char*)cert,initPara.m_certID.GetLength()+1);
	}

	rc = HiBioAPI_InitVerifyParameter(&appName,initPara.m_maxFMRRequest,initPara.m_trialNumber
		,initPara.m_hour,initPara.m_setFMRAchieved,secID,&pin);
	if (rc != BioAPI_OK) {
		sprintf(msg, "HiBioAPI_InitVerifyParameter failed.(0x%08x)\n", rc);
		MessageBox(msg);
		return;
	}

	m_PIN = initPara.m_PIN ;

	sprintf(msg, "HiBioAPI_InitVerifyParameter successful.\n", rc);
	MessageBox(msg);
}

void CBioAPI_TestDlg::OnBnClickedInitidentifyparameter()
{
	char msg[256];
	BioAPI_RETURN rc = BioAPI_OK;

	InitParameter initPara(2);
	initPara.m_appName = m_appName ;
	initPara.m_PIN = m_PIN ;

	if(initPara.DoModal() ==IDCANCEL)return;

	m_appName = initPara.m_appName ;

	BioAPI_STRING appName,pin,cert;
	memcpy(appName,initPara.m_appName,initPara.m_appName.GetLength()+1);
	memcpy( pin, initPara.m_PIN, initPara.m_PIN.GetLength() + 1 ) ;
	memcpy(cert,initPara.m_certID,initPara.m_certID.GetLength()+1);
	hi_bioapi_securitycodeID secID;
	if ( initPara.m_SelectedAlg == 0 ) {
		secID.securitycodeType = HI_BIOAPI_ALG_SIGNATURE ;
	} else {
		secID.securitycodeType = HI_BIOAPI_ALG_HMAC ;
		memset((char*)secID.macSeed,0,strlen((char*)cert));
		memcpy((char*)secID.macSeed,(char*)cert,initPara.m_certID.GetLength()+1);
	}

	rc = HiBioAPI_InitIdentifyParameter(&appName,initPara.m_maxFMRRequest,initPara.m_trialNumber
		,initPara.m_hour,initPara.m_setFMRAchieved,secID,&pin);

	if (rc != BioAPI_OK) {
		sprintf(msg, "HiBioAPI_InitIdentifyParameter failed.(0x%08x)\n", rc);
		MessageBox(msg);
		return;
	}

	m_PIN = initPara.m_PIN ;

	sprintf(msg, "HiBioAPI_InitIdentifyParameter successful.\n", rc);
	MessageBox(msg);
}

void CBioAPI_TestDlg::OnBnClickedDeleteparameter()
{
	char msg[256];
	BioAPI_RETURN rc = BioAPI_OK;

	deleteParameter delPara;
	delPara.m_appName = m_appName ;

	if(delPara.DoModal() ==IDCANCEL)return;

	m_appName = delPara.m_appName ;

	BioAPI_STRING appName;
	memcpy(appName,delPara.m_appName,delPara.m_appName.GetLength()+1);

	rc = HiBioAPI_DeleteParameter(&appName);
	if (rc != BioAPI_OK) {
		sprintf(msg, "HiBioAPI_DeleteParameter failed.(0x%08x)\n", rc);
		MessageBox(msg);
		return;
	}

	sprintf(msg, "HiBioAPI_DeleteParameter successful.\n", rc);
	MessageBox(msg);
}

void CBioAPI_TestDlg::OnBnClickedInitcertificate()
{
	char msg[256];
	BioAPI_RETURN rc = BioAPI_OK;

	InitCertificate initCert;
	initCert.m_appName = m_appName ;
	initCert.m_PIN = m_PIN ;

	if(initCert.DoModal() ==IDCANCEL)return;

	m_appName = initCert.m_appName ;

	BioAPI_STRING appName,pin;
	memcpy(appName,initCert.m_appName,initCert.m_appName.GetLength()+1);
	memcpy( pin, initCert.m_PIN, initCert.m_PIN.GetLength() + 1 ) ;

	rc = HiBioAPI_InitCertificate( &appName, &initCert.m_certid, &pin ) ;
	if (rc != BioAPI_OK) {
		sprintf(msg, "HiBioAPI_InitCertificate failed.(0x%08x)\n", rc);
		MessageBox(msg);
		return;
	}

	m_PIN = initCert.m_PIN ;

	sprintf(msg, "HiBioAPI_InitCertificate successful.\n", rc);
	MessageBox(msg);
}
