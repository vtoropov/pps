#pragma once


// InitParameter dialog

class InitParameter : public CDialog
{
	DECLARE_DYNAMIC(InitParameter)

public:
	InitParameter(int para = 0, CWnd* pParent = NULL);   // standard constructor
	virtual ~InitParameter();

// Dialog Data
	enum { IDD = IDD_DIALOG_INITPARAMETER };

protected:
	virtual void DoDataExchange(CDataExchange* pDX);    // DDX/DDV support

	DECLARE_MESSAGE_MAP()
public:
	CString m_appName;
	int m_maxFMRRequest;
	int m_trialNumber;
	int m_hour;
	int m_setFMRAchieved;
	CListBox m_AlgList;
	int m_SelectedAlg;
	CString m_certID;
	CString m_PIN ;
	int m_para ;

	virtual BOOL OnInitDialog();
	afx_msg void OnBnClickedOk();

};
