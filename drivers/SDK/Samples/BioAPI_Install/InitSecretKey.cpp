// InitSecretKey.cpp
//

#include "stdafx.h"
#include "BioAPI_Test.h"
#include "InitSecretKey.h"


// InitSecretKey dialog

IMPLEMENT_DYNAMIC(InitSecretKey, CDialog)
InitSecretKey::InitSecretKey(CWnd* pParent /*=NULL*/)
	: CDialog(InitSecretKey::IDD, pParent)
	, m_appName(_T(""))
	, m_Seed(_T(""))
	, m_PIN( _T( "" ) )
{
}

InitSecretKey::~InitSecretKey()
{
}

void InitSecretKey::DoDataExchange(CDataExchange* pDX)
{
	CDialog::DoDataExchange(pDX);
	DDX_Text(pDX, IDC_EDIT1, m_appName);
	DDX_Control(pDX, IDC_ALG_ID, m_AlgName);
	DDX_Text(pDX, IDC_EDIT9, m_Seed);
	DDX_Text(pDX, IDC_EDIT10, m_PIN ) ;
}


BEGIN_MESSAGE_MAP(InitSecretKey, CDialog)
	ON_BN_CLICKED(IDOK, OnBnClickedOk)
END_MESSAGE_MAP()


// InitSecretKey message handlers

BOOL InitSecretKey::OnInitDialog()
{
	CDialog::OnInitDialog();

	
	m_AlgName.AddString("CALG_3DES");
	m_AlgName.AddString("CALG_AES_256");
	m_AlgName.SetCurSel( 1 );

	return TRUE;  // return TRUE unless you set the focus to a control
	
}

void InitSecretKey::OnBnClickedOk()
{
	
	m_SelectedIndex = m_AlgName.GetCurSel();
	UpdateData(TRUE);

	OnOK();
}
