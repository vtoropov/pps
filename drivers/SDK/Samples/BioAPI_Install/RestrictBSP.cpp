// RestrictBSP.cpp
//

#include "stdafx.h"
#include "BioAPI_Test.h"
#include "RestrictBSP.h"


// RestrictBSP dialog

IMPLEMENT_DYNAMIC(RestrictBSP, CDialog)
RestrictBSP::RestrictBSP(CWnd* pParent /*=NULL*/)
	: CDialog(RestrictBSP::IDD, pParent)
	, m_appName(_T(""))
	, m_usage(0)
	, m_operation_mask(0)
	, m_PIN( _T( "" ) )
{
}

RestrictBSP::~RestrictBSP()
{
}

void RestrictBSP::DoDataExchange(CDataExchange* pDX)
{
	CDialog::DoDataExchange(pDX);
	DDX_Text(pDX, IDC_EDIT1, m_appName);
	DDX_Text(pDX, IDC_EDIT2, m_usage);
	DDX_Text(pDX, IDC_EDIT4, m_operation_mask);
	DDX_Text(pDX, IDC_EDIT3, m_PIN ) ;
}


BEGIN_MESSAGE_MAP(RestrictBSP, CDialog)
END_MESSAGE_MAP()


// RestrictBSP message handlers
