#pragma once


// RestrictBSP dialog

class RestrictBSP : public CDialog
{
	DECLARE_DYNAMIC(RestrictBSP)

public:
	RestrictBSP(CWnd* pParent = NULL);   // standard constructor
	virtual ~RestrictBSP();

// Dialog Data
	enum { IDD = IDD_DIALOG_RESTRICTBSP };

protected:
	virtual void DoDataExchange(CDataExchange* pDX);    // DDX/DDV support

	DECLARE_MESSAGE_MAP()
public:
	CString m_appName;
	int m_usage;
	int m_operation_mask;
	CString m_PIN ;
};
