//{{NO_DEPENDENCIES}}
// Microsoft Visual C++ generated include file.
// Used by BioAPI_Test.rc
//
#define IDM_ABOUTBOX                    0x0010
#define IDD_ABOUTBOX                    100
#define IDS_ABOUTBOX                    101
#define IDD_BIOAPI_TEST_DIALOG          102
#define IDR_MAINFRAME                   128
#define IDD_DIALOG_IDENTIFYMATCH1       144
#define IDD_DIALOG_INITSECRETKEY        145
#define IDD_DIALOG_RESTRICTBSP          146
#define IDD_DIALOG_INITPARAMETER        147
#define IDD_DIALOG_DELETEPARAMETER      148
#define IDD_DIALOG_                     149
#define IDD_DIALOG_INITCERTIFICATE      155
#define IDD_DIALOG1                     156
#define IDC_BUTTON_INIT                 1000
#define IDC_BUTTON_TERMINATE            1001
#define IDC_BUTTON_GET_FRAMEWORK_INFO   1002
#define IDC_BUTTON_ENUM_BSPS            1003
#define IDC_BUTTON_BSP_LOAD             1004
#define IDC_BUTTON_BSP_UNLOAD           1005
#define IDC_BUTTON_BSP_ATTACH           1006
#define IDC_BUTTON_BSP_DETACH           1007
#define IDC_BUTTON_QUERY_UNITS          1008
#define IDC_BUTTON_ENUM_BFPS            1009
#define IDC_BUTTON_QUERY_BFPS           1010
#define IDC_BUTTON_CONTROL_UNIT         1011
#define IDC_BUTTON_FREE_BIR_HANDLE      1012
#define IDC_BUTTON_GET_BIR_FROM_HANDLE  1013
#define IDC_BUTTON_GET_HEADER_FROM_HANDLE 1014
#define IDC_BUTTON_ENABLE_EVENTS        1015
#define IDC_BUTTON_SET_GUI_CALLBACKS    1016
#define IDC_BUTTON_CAPTURE              1017
#define IDC_BUTTON_CREATE_TEMPLATE      1018
#define IDC_BUTTON_PROCESS              1019
#define IDC_BUTTON_PROCESS_WITH_AUX_BIR 1020
#define IDC_BUTTON_VERIFY_MATCH         1021
#define IDC_BUTTON_IDENTIFY_MATCH       1022
#define IDC_BUTTON_ENROLL               1023
#define IDC_BUTTON_VERIFY               1024
#define IDC_BUTTON_IDENTIFY             1025
#define IDC_BUTTON_IMPORT               1026
#define IDC_BUTTON_PRESET_IDENTIFY_POPULATION 1027
#define IDC_BUTTON_DB_OPEN              1028
#define IDC_BUTTON_DB_CLOSE             1029
#define IDC_BUTTON_DB_CREATE            1030
#define IDC_BUTTON_DB_DELETE            1031
#define IDC_BUTTON_DB_SET_MARKER        1032
#define IDC_BUTTON_DB_FREE_MARKER       1033
#define IDC_BUTTON_DB_STORE_BIR         1034
#define IDC_BUTTON_DB_GET_BIR           1035
#define IDC_BUTTON_DB_GET_NEXT_BIR      1036
#define IDC_BUTTON_DB_DELETE_BIR        1037
#define IDC_BUTTON_SET_POWER_MODE       1038
#define IDC_BUTTON_SET_INDICATOR_STATUS 1039
#define IDC_BUTTON_GET_INDICATOR_STATUS 1040
#define IDC_BUTTON_CALIBRATE_SENSOR     1041
#define IDC_BUTTON_CANCEL               1042
#define IDC_BUTTON_FREE                 1043
#define IDC_BUTTON_UTIL_INSTALL_BSP     1044
#define IDC_BUTTON_UTIL_INSTALL_BFP     1045
#define IDC_INITSECRETKEY               1046
#define IDC_RESTRICTBSP                 1047
#define IDC_INITVERIFYPARAMETER         1048
#define IDC_INITIDENTIFYPARAMETER2      1049
#define IDC_DELETEPARAMETER             1050
#define IDC_VERIFYBSP                   1051
#define IDC_INITSECRETKEY2              1051
#define IDC_VERIFYCHALLENGE             1052
#define IDC_IDENTIFYCHALLENGE           1053
#define IDC_INITCERTIFICATE             1054
#define IDC_VERIFYBIR                   1055
#define IDC_VERIFYBIR2                  1056
#define IDC_DIGITALSIGNBIR              1056
#define IDC_DIGITALSIGNBIR2             1057
#define IDC_ENROLLMENT_PRIMITIVE        1058
#define IDC_ENROLLMENT_ABSTRACTION      1059
#define IDC_EDIT1                       1060
#define IDC_VERIFICATION_PRIMITIVE      1060
#define IDC_EDIT8                       1061
#define IDC_BUTTON_ATTACH_PROCESS       1061
#define IDC_EDIT9                       1062
#define IDC_VERIFICATION_ABSTRACTION    1062
#define IDC_EDIT10                      1063
#define IDC_IDENTIFICATION_PRIMITIVE    1063
#define IDC_EDIT2                       1064
#define IDC_IDENTIFICATION_ABSTRACTION  1064
#define IDC_ALG_ID                      1064
#define IDC_EDIT3                       1065
#define IDC_CERT_LIST                   1065
#define IDC_EDIT4                       1066
#define IDC_REPEAT_CAPTURE              1066
#define IDC_EDIT5                       1067
#define IDC_REPEAT_ENROLL               1067
#define IDC_REPEAT_VERIFY               1068
#define IDC_LIST1                       1068
#define IDC_ALG_LIST                    1068
#define IDC_REPEAT_CONTROLUNIT          1069
#define IDC_STATIC_TITLE                1069
#define IDC_REPEAT_IDENTIFY             1070
#define IDC_REPEAT_LOAD_UNLOAD          1071
#define IDC_REPEAT_ENROLL2              1072
#define IDC_REPEAT_ATTACH_DETACH        1072

// Next default values for new objects
// 
#ifdef APSTUDIO_INVOKED
#ifndef APSTUDIO_READONLY_SYMBOLS
#define _APS_NEXT_RESOURCE_VALUE        164
#define _APS_NEXT_COMMAND_VALUE         32771
#define _APS_NEXT_CONTROL_VALUE         1071
#define _APS_NEXT_SYMED_VALUE           106
#endif
#endif
