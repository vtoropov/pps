// BioAPI_TestDlg.h : header file
//

#pragma once


#include "bioapi.h"
#include "hi_bioapi.h"

// CBioAPI_TestDlg dialog
class CBioAPI_TestDlg : public CDialog
{
// Construction
public:
	CBioAPI_TestDlg(CWnd* pParent = NULL);	// standard constructor
	~CBioAPI_TestDlg();
// Dialog Data
	enum { IDD = IDD_BIOAPI_TEST_DIALOG };

	protected:
	virtual void DoDataExchange(CDataExchange* pDX);	// DDX/DDV support

// Implementation
protected:
	HICON m_hIcon;

	// Generated message map functions
	virtual BOOL OnInitDialog();
	afx_msg void OnSysCommand(UINT nID, LPARAM lParam);
	afx_msg void OnPaint();
	afx_msg HCURSOR OnQueryDragIcon();
	DECLARE_MESSAGE_MAP()
public:
	afx_msg void OnBnClickedInitsecretkey();
	afx_msg void OnBnClickedRestrictbsp();
	afx_msg void OnBnClickedInitverifyparameter();
	afx_msg void OnBnClickedInitidentifyparameter();
	afx_msg void OnBnClickedDeleteparameter();
	afx_msg void OnBnClickedInitcertificate();

	CString m_appName;
	CString m_PIN ;
private:
	CStatic m_title;	
	CFont m_titleFont;	
};
