#pragma once
#include "afxwin.h"


// InitSecretKey dialog

class InitSecretKey : public CDialog
{
	DECLARE_DYNAMIC(InitSecretKey)

public:
	InitSecretKey(CWnd* pParent = NULL);   // standard constructor
	virtual ~InitSecretKey();
	int m_SelectedIndex;

// Dialog Data
	enum { IDD = IDD_DIALOG_INITSECRETKEY };

protected:
	virtual void DoDataExchange(CDataExchange* pDX);    // DDX/DDV support

	DECLARE_MESSAGE_MAP()
public:
	CString m_appName;
	CComboBox m_AlgName;
	CString m_Seed;
	CString m_PIN ;

	virtual BOOL OnInitDialog();
	afx_msg void OnBnClickedOk();
};
