// InitParameter.cpp
//

#include "stdafx.h"
#include "BioAPI_Test.h"
#include "InitParameter.h"

#define SAMPLE_APP_FMR_MAX_DEFAULT 208597
#define SAMPLE_APP_TRIAL_NUM_DEFAULT 0
#define SAMPLE_APP_MINUTE_DEFAULT 0

#define TITLE_DEFAULT "HiBioAPI_InitParameter"
#define TITLE_VERIFY_PARAMETER "HiBioAPI_InitVerifyParameter"
#define TITLE_IDENTIFY_PARAMETER "HiBioAPI_InitIdentifyParameter"

// InitParameter dialog

IMPLEMENT_DYNAMIC(InitParameter, CDialog)
InitParameter::InitParameter(int para, CWnd* pParent /*=NULL*/)
	: CDialog(InitParameter::IDD, pParent)
	, m_appName(_T(""))
	, m_maxFMRRequest( SAMPLE_APP_FMR_MAX_DEFAULT )
	, m_trialNumber( SAMPLE_APP_TRIAL_NUM_DEFAULT )
	, m_hour( SAMPLE_APP_MINUTE_DEFAULT )
	, m_setFMRAchieved(0)
	, m_SelectedAlg(0)
	, m_certID(_T(""))
	, m_PIN( _T( "" ) )
	, m_para(para)

{
}

InitParameter::~InitParameter()
{
}

void InitParameter::DoDataExchange(CDataExchange* pDX)
{
	CDialog::DoDataExchange(pDX);
	DDX_Text(pDX, IDC_EDIT1, m_appName);
	DDX_Text(pDX, IDC_EDIT8, m_maxFMRRequest);
	DDX_Text(pDX, IDC_EDIT9, m_trialNumber);
	DDX_Text(pDX, IDC_EDIT10, m_hour);
	DDX_Text(pDX, IDC_EDIT2, m_setFMRAchieved);
	DDX_Control(pDX, IDC_ALG_LIST, m_AlgList);
	DDX_Text(pDX, IDC_EDIT4, m_certID);
	DDX_Text(pDX, IDC_EDIT5, m_PIN ) ;
}


BEGIN_MESSAGE_MAP(InitParameter, CDialog)
	ON_BN_CLICKED(IDOK, OnBnClickedOk)
END_MESSAGE_MAP()


// InitParameter message handlers

BOOL InitParameter::OnInitDialog()
{
	CDialog::OnInitDialog();

	
	m_AlgList.AddString("HI_BIOAPI_ALG_SIGNATURE");
	m_AlgList.AddString("HI_BIOAPI_ALG_HMAC");
	m_AlgList.SetCurSel( 1 );

	
	switch (m_para) {
		
		case 1 :
			CDialog::SetWindowTextA(TITLE_VERIFY_PARAMETER) ;
			break ;
		case 2 :
			CDialog::SetWindowTextA(TITLE_IDENTIFY_PARAMETER) ;
			break ;
		default :
			CDialog::SetWindowTextA(TITLE_DEFAULT) ;
			break ;
	}

	return TRUE;  // return TRUE unless you set the focus to a control
	
}

void InitParameter::OnBnClickedOk()
{
	
	m_SelectedAlg = m_AlgList.GetCurSel();
	UpdateData(TRUE);

	OnOK();
}

