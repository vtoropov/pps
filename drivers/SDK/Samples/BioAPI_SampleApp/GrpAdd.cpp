/**
*@All Rights Reserved.    
*@Copyright (C) 2007,2011, Hitachi Ltd.
*/

// GrpAdd.cpp
//

#include "stdafx.h"
#include "BioAPI_SampleApp.h"
#include "GrpAdd.h"


// CGrpAdd dialog

IMPLEMENT_DYNAMIC(CGrpAdd, CDialog)

CGrpAdd::CGrpAdd(CWnd* pParent /*=NULL*/)
	: CDialog(CGrpAdd::IDD, pParent)
	, m_xvGrpName(_T(""))
{

}

CGrpAdd::~CGrpAdd()
{
}

void CGrpAdd::DoDataExchange(CDataExchange* pDX)
{
	CDialog::DoDataExchange(pDX);
	DDX_Text(pDX, IDC_EDIT_GRP, m_xvGrpName);
}


BEGIN_MESSAGE_MAP(CGrpAdd, CDialog)
END_MESSAGE_MAP()


// CGrpAdd message handlers


void CGrpAdd::OnBnClickedOk()
{
	
	OnOK();
}


CString CGrpAdd::getGrpName()
{
	return m_xvGrpName;	
}
