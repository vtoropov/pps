/**
*@All Rights Reserved.    
*@Copyright (C) 2007,2011, Hitachi Ltd.
*/

#pragma once


// CGrpAdd dialog


class CGrpAdd : public CDialog
{
	DECLARE_DYNAMIC(CGrpAdd)

public:
	CGrpAdd(CWnd* pParent = NULL);   // standard constructor
	virtual ~CGrpAdd();

// Dialog Data
	enum { IDD = IDD_DIALOG_ADD_GRP };

protected:
	virtual void DoDataExchange(CDataExchange* pDX);    // DDX/DDV support

	DECLARE_MESSAGE_MAP()

public:
	
	CString getGrpName();	

private:
	
	CString m_xvGrpName;	
	afx_msg void OnBnClickedOk();	

};
