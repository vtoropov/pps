/**
*@All Rights Reserved.    
*@Copyright (C) 2007,2011, Hitachi Ltd.
*/

// BioAPI_SampleAppDlg.cpp
//

#include "stdafx.h"
#include "BioAPI_SampleApp.h"
#include "BioAPI_SampleAppDlg.h"

#ifdef _DEBUG
#define new DEBUG_NEW
#endif

//#define BSP_DEFAULT_UUID	( "85b448bf-36a6-4843-b34b-bcdfef45d1d7" ) 
#define BSP_DEFAULT_UUID	( "e41a9357-67df-44d5-80e3-64935d21b642" ) 

// CAboutDlg dialog used for App About

class CAboutDlg : public CDialog
{
public:
	CAboutDlg();

// Dialog Data
	enum { IDD = IDD_ABOUTBOX };

	protected:
	virtual void DoDataExchange(CDataExchange* pDX);    // DDX/DDV support

// Implementation
protected:
	DECLARE_MESSAGE_MAP()
};

CAboutDlg::CAboutDlg() : CDialog(CAboutDlg::IDD)
{
}

void CAboutDlg::DoDataExchange(CDataExchange* pDX)
{
	CDialog::DoDataExchange(pDX);
}

BEGIN_MESSAGE_MAP(CAboutDlg, CDialog)
END_MESSAGE_MAP()


// CBioAPI_SampleAppDlg dialog




CBioAPI_SampleAppDlg::CBioAPI_SampleAppDlg(CWnd* pParent /*=NULL*/)
	: CDialog(CBioAPI_SampleAppDlg::IDD, pParent)
	, m_xvBeep(FALSE)
	, m_xvCmbBSPUuid(0)
{

	// BioAPI_Init
	m_BioAPIVersion = 0x20 ;

	// BioAPI_EnumBSPs
	m_BSPSchemaList = NULL ;
	m_NumBSP = 0 ;

	// BioAPI_BSPLoad
	m_EventHandler = NULL;
	m_EventHandlerCxt = NULL;

	// BioAPI_QueryUnits
	m_UnitSchemaList = NULL;
	m_NumUnits = 0;

	m_hIcon = AfxGetApp()->LoadIcon(IDR_MAINFRAME);
}

CBioAPI_SampleAppDlg::~CBioAPI_SampleAppDlg()
{

	
	for (BIRArray::iterator e = m_BIRList.begin(); e != m_BIRList.end(); e++) {
		BioAPI_Free(e->BiometricData.Data);
		e->BiometricData.Data = NULL;

		BioAPI_Free(e->SecurityBlock.Data);
		e->SecurityBlock.Data = NULL;		
	}

	BioAPI_RETURN rc = terminateFrameWork() ;

}

void CBioAPI_SampleAppDlg::DoDataExchange(CDataExchange* pDX)
{
	CDialog::DoDataExchange(pDX);
	DDX_Control(pDX, IDC_TAB, m_xcTab);
	DDX_Check(pDX, IDC_CHK_BEEP, m_xvBeep);
	DDX_Control(pDX, IDC_STATIC_TITLE, m_xcTitle);
	DDX_Control(pDX, IDC_COMBO_BSPUuid, m_xcCmbBSPUuid);
	DDX_CBIndex(pDX, IDC_COMBO_BSPUuid, m_xvCmbBSPUuid);
}

BEGIN_MESSAGE_MAP(CBioAPI_SampleAppDlg, CDialog)
	ON_WM_SYSCOMMAND()
	ON_WM_PAINT()
	ON_WM_QUERYDRAGICON()
	//}}AFX_MSG_MAP
	ON_BN_CLICKED(IDC_BTN_FIN, &CBioAPI_SampleAppDlg::OnBnClickedBtnFin)
	ON_NOTIFY(TCN_SELCHANGE, IDC_TAB, &CBioAPI_SampleAppDlg::OnTcnSelchangeTab)
	ON_BN_CLICKED(IDC_BTN_CHANGE_BSP, &CBioAPI_SampleAppDlg::OnBnClickedBtnChangeBsp)
END_MESSAGE_MAP()


// CBioAPI_SampleAppDlg message handlers

BOOL CBioAPI_SampleAppDlg::OnInitDialog()
{
	CDialog::OnInitDialog();

	// Add "About..." menu item to system menu.

	// IDM_ABOUTBOX must be in the system command range.
	ASSERT((IDM_ABOUTBOX & 0xFFF0) == IDM_ABOUTBOX);
	ASSERT(IDM_ABOUTBOX < 0xF000);

	CMenu* pSysMenu = GetSystemMenu(FALSE);
	if (pSysMenu != NULL)
	{
		CString strAboutMenu;
		strAboutMenu.LoadString(IDS_ABOUTBOX);
		if (!strAboutMenu.IsEmpty())
		{
			pSysMenu->AppendMenu(MF_SEPARATOR);
			pSysMenu->AppendMenu(MF_STRING, IDM_ABOUTBOX, strAboutMenu);
		}
	}

	// Set the icon for this dialog.  The framework does this automatically
	//  when the application's main window is not a dialog
	SetIcon(m_hIcon, TRUE);			// Set big icon
	SetIcon(m_hIcon, FALSE);		// Set small icon

	// TODO: Add extra initialization here
	char msg[256];
	CRect rect ;

	BioAPI_RETURN rc = CBioAPI_SampleAppDlg::initFrameWork( BSP_DEFAULT_UUID );
	if (rc != BioAPI_OK) {
		sprintf(msg, "An error occurred during preprocessing.");
		MessageBox(msg,TITLE,MB_ICONSTOP);
		exit(EXIT_FAILURE);	
	}

	
	m_dlg_CldTab1.Create(CDlgCldTab1::IDD, this) ;
	m_dlg_CldTab2.Create(CDlgCldTab2::IDD, this) ;
	m_dlg_CldTab3.Create(CDlgCldTab3::IDD, this) ;
	m_dlg_CldTab4.Create(CDlgCldTab4::IDD, this) ;

	m_dlg_CldTab1.GetClientRect(&rect) ;

	
	rect.SetRect(70,110,535,390);	// upper-x, upper-y, lower-x, lower-y

	
	m_dlg_CldTab1.MoveWindow(&rect, FALSE);
    m_dlg_CldTab2.MoveWindow(&rect, FALSE);
    m_dlg_CldTab3.MoveWindow(&rect, FALSE);
	m_dlg_CldTab4.MoveWindow(&rect, FALSE);

	
	m_xcTab.InsertItem(0, _T("About"));
	m_xcTab.InsertItem(0, _T("1:Many Identification"));
	m_xcTab.InsertItem(0, _T("1:1 Authentication"));
	m_xcTab.InsertItem(0, _T("Registration"));

	
	m_xcTab.SetCurSel(-1);
	m_xcTab.SetCurFocus(0);

	
    m_Font.CreateFont(20,0,0,0,FW_BOLD,FALSE,TRUE,0, 
          SHIFTJIS_CHARSET,OUT_DEFAULT_PRECIS,
      CLIP_DEFAULT_PRECIS,DEFAULT_QUALITY,
          DEFAULT_PITCH,"MS Shell Dlg");

	m_xcTitle.SetFont(&m_Font); 

	return TRUE;  // return TRUE  unless you set the focus to a control
}

void CBioAPI_SampleAppDlg::OnSysCommand(UINT nID, LPARAM lParam)
{
	if ((nID & 0xFFF0) == IDM_ABOUTBOX)
	{
		CAboutDlg dlgAbout;
		dlgAbout.DoModal();
	}
	else
	{
		CDialog::OnSysCommand(nID, lParam);
	}
}

// If you add a minimize button to your dialog, you will need the code below
//  to draw the icon.  For MFC applications using the document/view model,
//  this is automatically done for you by the framework.

void CBioAPI_SampleAppDlg::OnPaint()
{
	if (IsIconic())
	{
		CPaintDC dc(this); // device context for painting

		SendMessage(WM_ICONERASEBKGND, reinterpret_cast<WPARAM>(dc.GetSafeHdc()), 0);

		// Center icon in client rectangle
		int cxIcon = GetSystemMetrics(SM_CXICON);
		int cyIcon = GetSystemMetrics(SM_CYICON);
		CRect rect;
		GetClientRect(&rect);
		int x = (rect.Width() - cxIcon + 1) / 2;
		int y = (rect.Height() - cyIcon + 1) / 2;

		// Draw the icon
		dc.DrawIcon(x, y, m_hIcon);
	}
	else
	{
		CDialog::OnPaint();
	}
}

// The system calls this function to obtain the cursor to display while the user drags
//  the minimized window.
HCURSOR CBioAPI_SampleAppDlg::OnQueryDragIcon()
{
	return static_cast<HCURSOR>(m_hIcon);
}


void CBioAPI_SampleAppDlg::OnTcnSelchangeTab(NMHDR *pNMHDR, LRESULT *pResult)
{
	
    int cel;	
	
    cel = m_xcTab.GetCurSel();
	
    m_dlg_CldTab1.ShowWindow(cel == 0 ? SW_SHOW : SW_HIDE);
    m_dlg_CldTab2.ShowWindow(cel == 1 ? SW_SHOW : SW_HIDE);
    m_dlg_CldTab3.ShowWindow(cel == 2 ? SW_SHOW : SW_HIDE);
	m_dlg_CldTab4.ShowWindow(cel == 3 ? SW_SHOW : SW_HIDE);
	
	
	switch (cel) {
		
		case 0:
			
			m_dlg_CldTab1.Init_Tab1();
			break;
		
		case 1:
			
			m_dlg_CldTab2.Init_Tab2();
			break;
		
		case 2:
			
			m_dlg_CldTab3.Init_Tab3();
			break;
		
		case 3:
			
			break;

		default:
			break;
	}

	if (pResult) {
		*pResult = 0;
	}
}


BioAPI_RETURN BioAPI
CBioAPI_SampleAppDlg::initFrameWork( const char * sBSPUuid )
{
	
	BioAPI_RETURN ret = BioAPI_OK;

	char msg[256];
	BioAPI_RETURN rc;
	
	char sBSPStr[50] = { 0 } ;		
	char sLoadedBSP[50] = { 0 } ;	

	//-------- BioAPI_Init --------//	
	rc = BioAPI_Init(m_BioAPIVersion);
	
	if (rc != BioAPI_OK) {
		sprintf(msg, "BioAPI_Init encountered an error. (0x%08x)\r\n", rc);
		MessageBox(msg,TITLE,MB_ICONSTOP);
		return -99 ;
	}

	//-------- BioAPI_GetFrameworkInfo --------//
	rc = BioAPI_GetFrameworkInfo(&m_FrameworkSchema);

	if (rc != BioAPI_OK) {
		sprintf(msg, "BioAPI_GetFrameworkInfo encountered an error. (0x%08x)\r\n", rc);
		MessageBox(msg,TITLE,MB_ICONSTOP);
		return -99;
	}

	//-------- BioAPI_EnumBSPs --------//
	rc = BioAPI_EnumBSPs(&m_BSPSchemaList, &m_NumBSP);
	if (rc != BioAPI_OK) {
		sprintf(msg, "BioAPI_EnumBSPs encountered an error. (0x%08x)\r\n", rc);
		MessageBox(msg,TITLE,MB_ICONSTOP);
		return -99;
	}
	
	
	int size = sizeof( sLoadedBSP ) ;
	strncpy( sLoadedBSP, sBSPUuid, size ) ;
	sLoadedBSP[size-1] = '\0' ;
	
	UpdateData( TRUE ) ;

	m_xcCmbBSPUuid.ResetContent() ;	

	for ( uint32_t i = 0; i < m_NumBSP; i++ ) {
		
		sprintf( sBSPStr, "%02x%02x%02x%02x-%02x%02x-%02x%02x-%02x%02x-%02x%02x%02x%02x%02x%02x",
			m_BSPSchemaList[i].BSPUuid[ 0 ],
			m_BSPSchemaList[i].BSPUuid[ 1 ],
			m_BSPSchemaList[i].BSPUuid[ 2 ],
			m_BSPSchemaList[i].BSPUuid[ 3 ],
			m_BSPSchemaList[i].BSPUuid[ 4 ],
			m_BSPSchemaList[i].BSPUuid[ 5 ],
			m_BSPSchemaList[i].BSPUuid[ 6 ],
			m_BSPSchemaList[i].BSPUuid[ 7 ],
			m_BSPSchemaList[i].BSPUuid[ 8 ],
			m_BSPSchemaList[i].BSPUuid[ 9 ],
			m_BSPSchemaList[i].BSPUuid[ 10 ],
			m_BSPSchemaList[i].BSPUuid[ 11 ],
			m_BSPSchemaList[i].BSPUuid[ 12 ],
			m_BSPSchemaList[i].BSPUuid[ 13 ],
			m_BSPSchemaList[i].BSPUuid[ 14 ],
			m_BSPSchemaList[i].BSPUuid[ 15 ]
		) ;

		
		if ( strcmp( sBSPStr, sLoadedBSP ) == 0 ) {

			m_xvCmbBSPUuid = i ; 
			m_BSP_Uuid = &m_BSPSchemaList[i].BSPUuid ;	

		}

		m_xcCmbBSPUuid.InsertString(-1, sBSPStr) ; 
	}

	UpdateData( FALSE ) ;

	//-------- BioAPI_BSPLoad --------//
	rc = BioAPI_BSPLoad(m_BSP_Uuid, m_EventHandler, m_EventHandlerCxt);
	if (rc != BioAPI_OK) {
		sprintf(msg, "BioAPI_BSPLoad encountered an error. (0x%08x)\r\n", rc);
		MessageBox(msg,TITLE,MB_ICONSTOP);
		return -99;
	}

	//-------- BioAPI_QueryUnits --------//
	rc = BioAPI_QueryUnits(m_BSP_Uuid, &m_UnitSchemaList, &m_NumUnits);
	if (rc != BioAPI_OK) {
		sprintf(msg, "BioAPI_QueryUnits encountered an error. (0x%08x)\r\n", rc);
		MessageBox(msg,TITLE,MB_ICONSTOP);
		return -99;
	}

	
	if (m_UnitSchemaList == NULL) {
		sprintf(msg, "Unable to find the unit. The process is terminated.\r\n");
		MessageBox(msg,TITLE,MB_ICONEXCLAMATION);
		return -99;
	}

	//-------- BioAPI_BSPAttach --------//
	BioAPI_UNIT_LIST_ELEMENT UnitList[1];
	UnitList[0].UnitCategory = m_UnitSchemaList[0].UnitCategory;
	UnitList[0].UnitId = m_UnitSchemaList[0].UnitId;

	rc = BioAPI_BSPAttach(m_BSP_Uuid,
		m_BioAPIVersion,
		UnitList,
		1,
		&m_BSPHandle);
	if (rc != BioAPI_OK) {
		sprintf(msg, "BioAPI_BSPAttach encountered an error. (0x%08x)\r\n", rc);
		MessageBox(msg,TITLE,MB_ICONSTOP);
		return -99;
	}

	return ret;
}



BioAPI_RETURN CBioAPI_SampleAppDlg::terminateFrameWork( void )
{
	
	freeUnitSchemaList( &m_UnitSchemaList, &m_NumUnits );

	//-------- BioAPI_BSPDetach --------//
	BioAPI_RETURN rc = BioAPI_BSPDetach(m_BSPHandle);
	if (rc != BioAPI_OK) {
		// nop
	}

	//-------- BioAPI_BSPUnload --------//
	rc = BioAPI_BSPUnload(m_BSP_Uuid, m_EventHandler, m_EventHandlerCxt);
	if (rc != BioAPI_OK) {
		// nop
	}

	
	freeBSPSchemaList( &m_BSPSchemaList, &m_NumBSP );

	//-------- BioAPI_Terminate --------//
	rc = BioAPI_Terminate();
	if (rc != BioAPI_OK) {
		// nop
	}

	return rc ;
}


void CBioAPI_SampleAppDlg::getFWInfo(char* chr)
{

	sprintf(chr, "FrameworkUuid=%02x%02x%02x%02x-%02x%02x-%02x%02x-%02x%02x-%02x%02x%02x%02x%02x%02x\r\n"
				 "FwDescription=%s\r\n"
				 "Path=%s\r\n"
				 "SpecVersion=0x%08x\r\n"
				 "ProductVersion=%s\r\n"
				 "Vendor=%s\r\n",
			m_FrameworkSchema.FrameworkUuid[ 0 ],
			m_FrameworkSchema.FrameworkUuid[ 1 ],
			m_FrameworkSchema.FrameworkUuid[ 2 ],
			m_FrameworkSchema.FrameworkUuid[ 3 ],
			m_FrameworkSchema.FrameworkUuid[ 4 ],
			m_FrameworkSchema.FrameworkUuid[ 5 ],
			m_FrameworkSchema.FrameworkUuid[ 6 ],
			m_FrameworkSchema.FrameworkUuid[ 7 ],
			m_FrameworkSchema.FrameworkUuid[ 8 ],
			m_FrameworkSchema.FrameworkUuid[ 9 ],
			m_FrameworkSchema.FrameworkUuid[ 10 ],
			m_FrameworkSchema.FrameworkUuid[ 11 ],
			m_FrameworkSchema.FrameworkUuid[ 12 ],
			m_FrameworkSchema.FrameworkUuid[ 13 ],
			m_FrameworkSchema.FrameworkUuid[ 14 ],
			m_FrameworkSchema.FrameworkUuid[ 15 ],
			m_FrameworkSchema.FwDescription,
			m_FrameworkSchema.Path,
			m_FrameworkSchema.SpecVersion,
			m_FrameworkSchema.ProductVersion,
			m_FrameworkSchema.Vendor
			);
			
}



void CBioAPI_SampleAppDlg::getBSPInfo(char* chr)
{

	sprintf(chr, "BSPUuid=%02x%02x%02x%02x-%02x%02x-%02x%02x-%02x%02x-%02x%02x%02x%02x%02x%02x\r\n"
				 "BSPDescription=%s\r\n"
				 "Path=%s\r\n"
				 "SpecVersion=0x%08x\r\n"
				 "ProductVersion=%s\r\n"
				 "Vendor=%s\r\n",
			m_BSPSchemaList[m_xvCmbBSPUuid].BSPUuid[ 0 ],
			m_BSPSchemaList[m_xvCmbBSPUuid].BSPUuid[ 1 ],
			m_BSPSchemaList[m_xvCmbBSPUuid].BSPUuid[ 2 ],
			m_BSPSchemaList[m_xvCmbBSPUuid].BSPUuid[ 3 ],
			m_BSPSchemaList[m_xvCmbBSPUuid].BSPUuid[ 4 ],
			m_BSPSchemaList[m_xvCmbBSPUuid].BSPUuid[ 5 ],
			m_BSPSchemaList[m_xvCmbBSPUuid].BSPUuid[ 6 ],
			m_BSPSchemaList[m_xvCmbBSPUuid].BSPUuid[ 7 ],
			m_BSPSchemaList[m_xvCmbBSPUuid].BSPUuid[ 8 ],
			m_BSPSchemaList[m_xvCmbBSPUuid].BSPUuid[ 9 ],
			m_BSPSchemaList[m_xvCmbBSPUuid].BSPUuid[ 10 ],
			m_BSPSchemaList[m_xvCmbBSPUuid].BSPUuid[ 11 ],
			m_BSPSchemaList[m_xvCmbBSPUuid].BSPUuid[ 12 ],
			m_BSPSchemaList[m_xvCmbBSPUuid].BSPUuid[ 13 ],
			m_BSPSchemaList[m_xvCmbBSPUuid].BSPUuid[ 14 ],
			m_BSPSchemaList[m_xvCmbBSPUuid].BSPUuid[ 15 ],
			m_BSPSchemaList[m_xvCmbBSPUuid].BSPDescription,
			m_BSPSchemaList[m_xvCmbBSPUuid].Path,
			m_BSPSchemaList[m_xvCmbBSPUuid].SpecVersion,
			m_BSPSchemaList[m_xvCmbBSPUuid].ProductVersion,
			m_BSPSchemaList[m_xvCmbBSPUuid].Vendor
			);
			
}


void CBioAPI_SampleAppDlg::freeBSPSchemaList( BioAPI_BSP_SCHEMA **ppBSPSchemaList, uint32_t *pNumBSP )
{
	BioAPI_BSP_SCHEMA *pBSPSchemaList = *ppBSPSchemaList ;
	uint32_t nNumBSP = *pNumBSP ;

	if ( pBSPSchemaList == NULL ) {
		return ;
	}

	for ( uint32_t i = 0 ; i < nNumBSP ; i++ )
	{
		BioAPI_Free( pBSPSchemaList[ i ].Path ) ;
		BioAPI_Free( pBSPSchemaList[ i ].BSPSupportedFormats ) ;
	}

	BioAPI_Free( pBSPSchemaList ) ;
	*ppBSPSchemaList = NULL ;

	*pNumBSP = 0 ;
}


void CBioAPI_SampleAppDlg::freeUnitSchemaList( BioAPI_UNIT_SCHEMA **ppUnitSchemaList, uint32_t *pNumUnits )
{
	BioAPI_UNIT_SCHEMA *pUnitSchemaList = *ppUnitSchemaList ;
	uint32_t nNumUnits = *pNumUnits ;

	if ( pUnitSchemaList == NULL ) {
		return ;
	}

	for ( uint32_t i = 0 ; i < nNumUnits ; i++ )
	{
		BioAPI_Free( pUnitSchemaList[ i ].UnitProperty.Data ) ;
	}

	BioAPI_Free( pUnitSchemaList ) ;
	*ppUnitSchemaList = NULL ;

	*pNumUnits = 0 ;
}


BioAPI_HANDLE CBioAPI_SampleAppDlg::getBSPHandle(void)
{
	return m_BSPHandle;
}


void CBioAPI_SampleAppDlg::setBIRList(BioAPI_BIR BIR)
{
	
	m_BIRList.push_back(BIR);
}


void CBioAPI_SampleAppDlg::setDataInfoList(DATA_INFO DataInfo)
{
	
	m_DataInfoList.push_back(DataInfo);
}


BIRArray CBioAPI_SampleAppDlg::getBIRList(void)
{
	return m_BIRList;
}


Data_InfoArray CBioAPI_SampleAppDlg::getDataInfoList(void)
{
	return m_DataInfoList;
}


bool CBioAPI_SampleAppDlg::getChkBeepState(void)
{
	UpdateData(TRUE);
	return (m_xvBeep ? true : false) ;
}


BioAPI_UNIT_ID CBioAPI_SampleAppDlg::getUnitID(void)
{
	return m_UnitSchemaList[0].UnitId;
}


BioAPI_RETURN BioAPI 
CBioAPI_SampleAppDlg::setBeepOff(bool c_flg)
{
	char msg[256];
	
	BioAPI_DATA InputData, OutputData = {0, NULL};	

	BioAPI_UNIT_ID unit_id = getUnitID();

	int nInputData[1];
	
	if (c_flg) {
		nInputData[0] = 0;	
	}
	
	else {
		nInputData[0] = 1;	
	}
	InputData.Length = 1;	
	InputData.Data = nInputData ;

	//-------- BioAPI_ControlUnit--------//
	BioAPI_RETURN rc = BioAPI_ControlUnit(m_BSPHandle, unit_id, 5, &InputData, &OutputData);
	if (rc != BioAPI_OK) {
		sprintf(msg, "BioAPI_ControlUnit encountered an error. (0x%08x)\r\n", rc);
		MessageBox(msg,TITLE,MB_ICONSTOP);
		return -99;
	}
	else {
		return BioAPI_OK;
	}
}


void CBioAPI_SampleAppDlg::freeFW_Mem(void)
{

	
	BioAPI_Free( m_FrameworkSchema.Path ) ;
	m_FrameworkSchema.Path = NULL ;

}


void CBioAPI_SampleAppDlg::OnBnClickedBtnFin()
{
	
	OnCancel();
}


void CBioAPI_SampleAppDlg::OnBnClickedBtnChangeBsp()
{
	
	char sBSPUuid[50] = { 0 } ;
	char msg[256] = { 0 } ;
	BioAPI_RETURN rc;

	UpdateData(TRUE) ;

	sprintf( sBSPUuid, "%02x%02x%02x%02x-%02x%02x-%02x%02x-%02x%02x-%02x%02x%02x%02x%02x%02x",
		m_BSPSchemaList[m_xvCmbBSPUuid].BSPUuid[ 0 ],
		m_BSPSchemaList[m_xvCmbBSPUuid].BSPUuid[ 1 ],
		m_BSPSchemaList[m_xvCmbBSPUuid].BSPUuid[ 2 ],
		m_BSPSchemaList[m_xvCmbBSPUuid].BSPUuid[ 3 ],
		m_BSPSchemaList[m_xvCmbBSPUuid].BSPUuid[ 4 ],
		m_BSPSchemaList[m_xvCmbBSPUuid].BSPUuid[ 5 ],
		m_BSPSchemaList[m_xvCmbBSPUuid].BSPUuid[ 6 ],
		m_BSPSchemaList[m_xvCmbBSPUuid].BSPUuid[ 7 ],
		m_BSPSchemaList[m_xvCmbBSPUuid].BSPUuid[ 8 ],
		m_BSPSchemaList[m_xvCmbBSPUuid].BSPUuid[ 9 ],
		m_BSPSchemaList[m_xvCmbBSPUuid].BSPUuid[ 10 ],
		m_BSPSchemaList[m_xvCmbBSPUuid].BSPUuid[ 11 ],
		m_BSPSchemaList[m_xvCmbBSPUuid].BSPUuid[ 12 ],
		m_BSPSchemaList[m_xvCmbBSPUuid].BSPUuid[ 13 ],
		m_BSPSchemaList[m_xvCmbBSPUuid].BSPUuid[ 14 ],
		m_BSPSchemaList[m_xvCmbBSPUuid].BSPUuid[ 15 ]
	) ;

	
	rc = terminateFrameWork() ;

	rc = CBioAPI_SampleAppDlg::initFrameWork( sBSPUuid ) ;

	if (rc != BioAPI_OK) {
		sprintf( msg, "An error occurred during changing BSP.") ;
		MessageBox( msg, TITLE, MB_ICONSTOP ) ;
		exit( EXIT_FAILURE ) ;	
	}
	else {
		sprintf( msg, "Changing BSP successful.\r\n(Current BSPUuid: %s)", sBSPUuid ) ;
		MessageBox( msg, TITLE, MB_ICONINFORMATION ) ;
	}
	
	m_dlg_CldTab1.UpdateBSPHandle() ;
	m_dlg_CldTab2.UpdateBSPHandle() ;
	m_dlg_CldTab3.UpdateBSPHandle() ;
	
	m_dlg_CldTab4.OnInitDialog() ;

	UpdateData( FALSE ) ;
}
