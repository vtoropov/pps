/**
*@All Rights Reserved.    
*@Copyright (C) 2007,2011, Hitachi Ltd.
*/

#pragma once

#include "Define.h"
#include "afxwin.h"

// CDlgCldTab3 dialog


class CDlgCldTab3 : public CDialog
{
	DECLARE_DYNAMIC(CDlgCldTab3)

public:
	CDlgCldTab3(CWnd* pParent = NULL);   // standard constructor
	virtual ~CDlgCldTab3();

// Dialog Data
	enum { IDD = IDD_DIALOG_TAB3 };

protected:
	virtual void DoDataExchange(CDataExchange* pDX);    // DDX/DDV support

	DECLARE_MESSAGE_MAP()

public:
	
	virtual BOOL OnInitDialog(void);
	void Init_Tab3();
	void UpdateBSPHandle(void) ;	

private:
	
	int m_xvCmbGrp;	
	CComboBox m_xcCmbGrp;	
	BIRArray m_BIRList;	
	Data_InfoArray m_DataInfoList;	
	unsigned int m_All_BirCnt;	
	BioAPI_HANDLE m_BSPHandle;	

	
	afx_msg void OnBnClickedBtnIdentify();	
	void OnOK();	
	void OnCancel();	

};
