/**
*@All Rights Reserved.    
*@Copyright (C) 2007,2011, Hitachi Ltd.
*/

// BioAPI_SampleAppDlg.h
//

#pragma once
#include "afxcmn.h"
#include "dlgcldtab1.h"
#include "dlgcldtab2.h"
#include "dlgcldtab3.h"
#include "dlgcldtab4.h"

#include "bioapi.h"
#include "hi_bioapi.h"
#include "afxwin.h"

// CBioAPI_SampleAppDlg dialog
class CBioAPI_SampleAppDlg : public CDialog
{
// standard constructor
public:
	CBioAPI_SampleAppDlg(CWnd* pParent = NULL);	// standard constructor
	~CBioAPI_SampleAppDlg();

// Dialog Data
	enum { IDD = IDD_BIOAPI_SAMPLEAPP_DIALOG };

	protected:
	virtual void DoDataExchange(CDataExchange* pDX);	// DDX/DDV support


// Implementation
protected:
	HICON m_hIcon;

	// Generated message map functions
	virtual BOOL OnInitDialog();
	afx_msg void OnSysCommand(UINT nID, LPARAM lParam);
	afx_msg void OnPaint();
	afx_msg HCURSOR OnQueryDragIcon();
	DECLARE_MESSAGE_MAP()

public:

	void getFWInfo(char* chr);	
	void getBSPInfo(char* chr);	
	void setBIRList(BioAPI_BIR BIR);	
	void setDataInfoList(DATA_INFO DataInfo);	
	BioAPI_RETURN BioAPI setBeepOff(bool c_flg);	
	BioAPI_HANDLE getBSPHandle(void);	
	BIRArray getBIRList(void);	
	Data_InfoArray getDataInfoList(void);	
	bool getChkBeepState(void); 
	void freeFW_Mem(void) ;	

private:
	
	CTabCtrl m_xcTab;	
	CDlgCldTab1 m_dlg_CldTab1;	
	CDlgCldTab2 m_dlg_CldTab2;	
	CDlgCldTab3 m_dlg_CldTab3;	
	CDlgCldTab4 m_dlg_CldTab4;	
	BioAPI_VERSION m_BioAPIVersion;	
	BioAPI_FRAMEWORK_SCHEMA m_FrameworkSchema;	
	BioAPI_BSP_SCHEMA *m_BSPSchemaList;	
	uint32_t m_NumBSP;	
	uint32_t m_NumUnits;	
	BioAPI_EventHandler m_EventHandler;	
	HWND m_EventHandlerCxt;	
	BioAPI_UNIT_SCHEMA *m_UnitSchemaList;	
	BioAPI_HANDLE m_BSPHandle;	
	BOOL m_xvBeep;	
	BIRArray m_BIRList;	
	Data_InfoArray m_DataInfoList;	
	BioAPI_UUID* m_BSP_Uuid;	
	CStatic m_xcTitle;	
	CFont m_Font;	
	CComboBox m_xcCmbBSPUuid;	
	int m_xvCmbBSPUuid;	
	
	void freeBSPSchemaList( BioAPI_BSP_SCHEMA **, uint32_t * ) ;	
	void freeUnitSchemaList( BioAPI_UNIT_SCHEMA **ppUnitSchemaList, uint32_t *pNumUnits );	
	afx_msg void OnTcnSelchangeTab(NMHDR *pNMHDR, LRESULT *pResult);	
	afx_msg void OnBnClickedBtnFin();	
	afx_msg void OnBnClickedBtnChangeBsp();	
	BioAPI_RETURN BioAPI initFrameWork( const char * ) ;	
	BioAPI_UNIT_ID getUnitID(void);	
	BioAPI_RETURN terminateFrameWork( void ) ;	

};
