/**
*@All Rights Reserved.    
*@Copyright (C) 2007,2011, Hitachi Ltd.
*/

#pragma once

#include "Define.h"
#include "afxwin.h"

// CDlgCldTab2 dialog


class CDlgCldTab2 : public CDialog
{
	DECLARE_DYNAMIC(CDlgCldTab2)

public:
	CDlgCldTab2(CWnd* pParent = NULL);   // standard constructor
	virtual ~CDlgCldTab2();

// Dialog Data
	enum { IDD = IDD_DIALOG_TAB2 };

protected:
	virtual void DoDataExchange(CDataExchange* pDX);    // DDX/DDV support

	DECLARE_MESSAGE_MAP()

public:
	
	virtual BOOL OnInitDialog(void);
	void Init_Tab2();	
	void UpdateBSPHandle(void) ;	

private:
	
	CListBox m_xcListBIR;	
	int m_xvSelectedIndex;	
	BIRArray m_BIRList;	
	Data_InfoArray m_DataInfoList;	
	BioAPI_HANDLE m_BSPHandle;	

	
	afx_msg void OnBnClickedBtnVerify();	
	void OnOK();	
	void OnCancel();	


};
