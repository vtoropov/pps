/**
*@All Rights Reserved.    
*@Copyright (C) 2007,2011, Hitachi Ltd.
*/

// BioAPI_SampleApp.h : main header file for the PROJECT_NAME application
//

#pragma once

#ifndef __AFXWIN_H__
	#error "include 'stdafx.h' before including this file for PCH"
#endif

#include "resource.h"		// main symbols


// CBioAPI_SampleAppApp:
// See BioAPI_SampleApp.cpp for the implementation of this class
//

class CBioAPI_SampleAppApp : public CWinApp
{
public:
	CBioAPI_SampleAppApp();

// Overrides
	public:
	virtual BOOL InitInstance();

// Implementation

	DECLARE_MESSAGE_MAP()
};

extern CBioAPI_SampleAppApp theApp;