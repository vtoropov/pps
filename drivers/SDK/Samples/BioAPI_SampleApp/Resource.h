//{{NO_DEPENDENCIES}}
// Microsoft Visual C++ generated include file.
// Used by BioAPI_SampleApp.rc
//
#define IDM_ABOUTBOX                    0x0010
#define IDD_ABOUTBOX                    100
#define IDS_ABOUTBOX                    101
#define IDD_BIOAPI_SAMPLEAPP_DIALOG     102
#define IDR_MAINFRAME                   128
#define IDD_DIALOG_TAB1                 129
#define IDD_DIALOG_TAB2                 130
#define IDD_DIALOG_TAB3                 131
#define IDD_DIALOG_TAB4                 132
#define IDD_DIALOG_GRPADD               133
#define IDD_DIALOG_ADD_GRP              133
#define IDC_TAB                         1000
#define IDC_CHK_BEEP                    1001
#define IDC_BTN_FIN                     1002
#define IDC_STATIC_TITLE                1003
#define IDC_STATIC_TAB1_1               1004
#define IDC_STATIC_TAB2_1               1005
#define IDC_STATIC_TAB2_2               1006
#define IDC_STATIC_TAB3_1               1007
#define IDC_STATIC_TAB3_2               1008
#define IDC_STATIC_TAB4_1               1009
#define IDC_STATIC_TAB4_2               1010
#define IDC_STATIC_TAB4_3               1011
#define IDC_STATIC_TAB1_2               1012
#define IDC_STATIC_TAB1_3               1013
#define IDC_CMB_GRP                     1014
#define IDC_EDIT_DATA                   1015
#define IDC_BTN_ADD_GRP                 1016
#define IDC_BTN_DEL_GRP                 1017
#define IDC_BTN_ENROLL                  1018
#define IDC_LIST_BIR                    1019
#define IDC_BTN_VERIFY                  1020
#define IDC_COMBO1                      1021
#define IDC_BTN_IDENTIFY                1022
#define IDC_EDIT_FW                     1023
#define IDC_EDIT_BSP                    1024
#define IDC_STATIC_ADD_GRP_1            1024
#define IDC_STATIC_ADD_GRP_2            1025
#define IDC_EDIT1                       1026
#define IDC_EDIT_GRP                    1026
#define IDC_STATIC_BSPUuid              1027
#define IDC_COMBO_BSPUuid               1028
#define IDC_BUTTON1                     1029
#define IDC_BTN_CHANGE_BSP              1029

// Next default values for new objects
// 
#ifdef APSTUDIO_INVOKED
#ifndef APSTUDIO_READONLY_SYMBOLS
#define _APS_NEXT_RESOURCE_VALUE        134
#define _APS_NEXT_COMMAND_VALUE         32771
#define _APS_NEXT_CONTROL_VALUE         1030
#define _APS_NEXT_SYMED_VALUE           101
#endif
#endif
