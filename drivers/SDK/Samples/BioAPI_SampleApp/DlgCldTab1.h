/**
*@All Rights Reserved.    
*@Copyright (C) 2007,2011, Hitachi Ltd.
*/

#pragma once

#include "Define.h"
#include "GrpAdd.h"
#include "afxwin.h"

// CDlgCldTab1 dialog


class CDlgCldTab1 : public CDialog
{
	DECLARE_DYNAMIC(CDlgCldTab1)

public:
	CDlgCldTab1(CWnd* pParent = NULL);   // standard constructor
	virtual ~CDlgCldTab1();

// Dialog Data
	enum { IDD = IDD_DIALOG_TAB1 };

protected:
	virtual void DoDataExchange(CDataExchange* pDX);    // DDX/DDV support

	DECLARE_MESSAGE_MAP()

public:
	
	virtual BOOL OnInitDialog(void);
	void Init_Tab1();	
	void UpdateBSPHandle(void) ;	

private:
	
	CComboBox m_xcCmbGrp;	
	int m_xvCmbGrp;	
	CString m_xvDataName;	
	BioAPI_HANDLE m_BSPHandle;	
	Data_InfoArray m_DataInfoList;	

	
	afx_msg void OnBnClickedBtnAddGrp();	
	afx_msg void OnBnClickedBtnDelGrp();	
	afx_msg void OnBnClickedBtnEnroll();	
	void OnOK();	
	void OnCancel();	

};
