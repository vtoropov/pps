/**
*@All Rights Reserved.    
*@Copyright (C) 2007,2011, Hitachi Ltd.
*/

// DlgCldTab4.cpp
//

#include "stdafx.h"
#include "BioAPI_SampleApp.h"
#include "DlgCldTab4.h"

#include "BioAPI_SampleAppDlg.h"

// CDlgCldTab4 dialog

IMPLEMENT_DYNAMIC(CDlgCldTab4, CDialog)

CDlgCldTab4::CDlgCldTab4(CWnd* pParent /*=NULL*/)
	: CDialog(CDlgCldTab4::IDD, pParent)
	, m_xvFWInfo(_T(""))
	, m_xvBSPInfo(_T(""))
{

}

CDlgCldTab4::~CDlgCldTab4()
{
}

void CDlgCldTab4::DoDataExchange(CDataExchange* pDX)
{
	CDialog::DoDataExchange(pDX);
	DDX_Text(pDX, IDC_EDIT_FW, m_xvFWInfo);
	DDX_Text(pDX, IDC_EDIT_BSP, m_xvBSPInfo);
}


BEGIN_MESSAGE_MAP(CDlgCldTab4, CDialog)
END_MESSAGE_MAP()


// CDlgCldTab4 message handlers

void CDlgCldTab4::OnOK()
{
	// nop
}

void CDlgCldTab4::OnCancel()
{
	// nop
}


BOOL CDlgCldTab4::OnInitDialog(void)
{
	char msg[256];
	
	CBioAPI_SampleAppDlg * pr = (CBioAPI_SampleAppDlg*)GetParent();
	
	pr->getFWInfo(msg);
	setFWInfo(msg);
	
	pr->getBSPInfo(msg);
	setBSPInfo(msg);
	
	UpdateData(FALSE);
	
	pr->freeFW_Mem();

	return TRUE;
}


void CDlgCldTab4::setFWInfo(char* p)
{
	m_xvFWInfo = p;
	UpdateData(FALSE);
}


void CDlgCldTab4::setBSPInfo(char* p)
{
	m_xvBSPInfo = p;
	UpdateData(FALSE);
}