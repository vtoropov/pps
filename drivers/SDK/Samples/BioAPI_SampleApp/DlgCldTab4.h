/**
*@All Rights Reserved.    
*@Copyright (C) 2007,2011, Hitachi Ltd.
*/

#pragma once

// CDlgCldTab4 dialog


class CDlgCldTab4 : public CDialog
{
	DECLARE_DYNAMIC(CDlgCldTab4)

public:
	CDlgCldTab4(CWnd* pParent = NULL);   // standard constructor
	virtual ~CDlgCldTab4();

// Dialog Data
	enum { IDD = IDD_DIALOG_TAB4 };

protected:
	virtual void DoDataExchange(CDataExchange* pDX);    // DDX/DDV support
	DECLARE_MESSAGE_MAP()

public:
	
	virtual BOOL OnInitDialog(void);

private:
	
	CString m_xvFWInfo;	
	CString m_xvBSPInfo;	
	
	void setFWInfo(char* p);	
	void setBSPInfo(char* p);	
	void OnOK();	
	void OnCancel();	
};
