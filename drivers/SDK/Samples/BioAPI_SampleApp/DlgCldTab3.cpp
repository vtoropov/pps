/**
*@All Rights Reserved.    
*@Copyright (C) 2007,2011, Hitachi Ltd.
*/

// DlgCldTab3.cpp
//

#include "stdafx.h"
#include "BioAPI_SampleApp.h"
#include "DlgCldTab3.h"

#include "BioAPI_SampleAppDlg.h"

// CDlgCldTab3 dialog

IMPLEMENT_DYNAMIC(CDlgCldTab3, CDialog)

CDlgCldTab3::CDlgCldTab3(CWnd* pParent /*=NULL*/)
	: CDialog(CDlgCldTab3::IDD, pParent)
	, m_xvCmbGrp(0)
{

}

CDlgCldTab3::~CDlgCldTab3()
{
}

void CDlgCldTab3::DoDataExchange(CDataExchange* pDX)
{
	CDialog::DoDataExchange(pDX);
	DDX_CBIndex(pDX, IDC_COMBO1, m_xvCmbGrp);
	DDX_Control(pDX, IDC_COMBO1, m_xcCmbGrp);
}


BEGIN_MESSAGE_MAP(CDlgCldTab3, CDialog)
	ON_BN_CLICKED(IDC_BTN_IDENTIFY, &CDlgCldTab3::OnBnClickedBtnIdentify)
END_MESSAGE_MAP()


// CDlgCldTab3 message handlers

void CDlgCldTab3::OnOK()
{
	// nop
}

void CDlgCldTab3::OnCancel()
{
	// nop
}


void CDlgCldTab3::Init_Tab3()
{
	
	int itemCnt, i;	
	CString str_GrpName;	
	bool flg_Repeat;	
	
	CBioAPI_SampleAppDlg * pr = (CBioAPI_SampleAppDlg*)GetParent();
	m_DataInfoList = pr->getDataInfoList();
	m_BIRList = pr->getBIRList();
	
	UpdateData(TRUE);
	
	m_xcCmbGrp.ResetContent();
	m_All_BirCnt = 0;	
	
	for (Data_InfoArray::iterator e = m_DataInfoList.begin(); e != m_DataInfoList.end(); e++) {
		
		m_All_BirCnt++;
		
		itemCnt = m_xcCmbGrp.GetCount();
		
		flg_Repeat = false ;	
		
		for (i=0; i<itemCnt; i++) {	
			
			m_xcCmbGrp.GetLBText(i, str_GrpName);
			
			if (str_GrpName == e->GName) {

				
				flg_Repeat = true ;	
				break;
			}
		}
		
		if (flg_Repeat) continue ;

		m_xcCmbGrp.InsertString(-1, _T(e->GName));
		UpdateData(FALSE);

	}	// end for

}


BOOL CDlgCldTab3::OnInitDialog(void)
{
	
	this->UpdateBSPHandle() ;
	
	UpdateData(FALSE);

	return TRUE;
}

void CDlgCldTab3::UpdateBSPHandle(void)
{
	
	CBioAPI_SampleAppDlg * pr = (CBioAPI_SampleAppDlg*)GetParent();
	
	
	m_BSPHandle = pr->getBSPHandle();
}

void CDlgCldTab3::OnBnClickedBtnIdentify()
{
	
	CString GName;	
	char msg[256];
	BioAPI_RETURN rc;
	UpdateData(TRUE);
	
	if (m_xcCmbGrp.GetCount() == 0) {
		sprintf(msg, "There is no group registered to match.");
		MessageBox(msg,TITLE,MB_ICONEXCLAMATION);
		return;
	}
	
	if (m_xvCmbGrp == -1) {
		sprintf(msg, "Select a group to match.");
		MessageBox(msg,TITLE,MB_ICONEXCLAMATION);
		return;
	}
	
	CBioAPI_SampleAppDlg * pr = (CBioAPI_SampleAppDlg*)GetParent();

	if (pr->getChkBeepState()) {
		
		rc = pr->setBeepOff(true);
	}
	else {
		
		rc = pr->setBeepOff(false);
	}
	
	if (rc != BioAPI_OK) {
		sprintf(msg, "An error occurred in sound alert control.");
		MessageBox(msg,TITLE,MB_ICONSTOP);
		return;
	}
	
	m_xcCmbGrp.GetLBText(m_xvCmbGrp,GName);
	int p_cnt = 0;	
	
	for (unsigned int j=0; j<m_All_BirCnt; j++) {	
		
		if (m_DataInfoList[j].GName == GName) {
			p_cnt++;
		}
	}
	
	uint32_t NumberOfResults;
	BioAPI_CANDIDATE *Candidates = NULL;
	
	BioAPI_FMR MaxFMRRequested = 2072;	
	BioAPI_IDENTIFY_POPULATION Population;
	
	Population.Type = BioAPI_ARRAY_TYPE;	
	Population.BIRs.BIRArray = (BioAPI_BIR_ARRAY_POPULATION*)malloc(sizeof(BioAPI_BIR_ARRAY_POPULATION));
	Population.BIRs.BIRArray->NumberOfMembers = p_cnt;
	Population.BIRs.BIRArray->Members = (BioAPI_BIR*)malloc(sizeof(BioAPI_BIR) * p_cnt);
	int t = 0;	
	for (unsigned int i = 0; i < m_All_BirCnt; i++) {
		
		if (m_DataInfoList[i].GName != GName) {
			continue;
		}
		Population.BIRs.BIRArray->Members[t].Header = m_BIRList[i].Header;
		Population.BIRs.BIRArray->Members[t].BiometricData.Length = m_BIRList[i].BiometricData.Length;
		if (Population.BIRs.BIRArray->Members[t].BiometricData.Length > 0) {
			Population.BIRs.BIRArray->Members[t].BiometricData.Data = malloc(Population.BIRs.BIRArray->Members[t].BiometricData.Length);
			memcpy(Population.BIRs.BIRArray->Members[t].BiometricData.Data, m_BIRList[i].BiometricData.Data, m_BIRList[i].BiometricData.Length);
		}
		Population.BIRs.BIRArray->Members[t].SecurityBlock.Length = 0 ;
		Population.BIRs.BIRArray->Members[t].SecurityBlock.Data = NULL ;
		
		t++;
	}

	//-------- BioAPI_Identify --------//
	rc = BioAPI_Identify(m_BSPHandle, MaxFMRRequested, 
		BioAPI_NO_SUBTYPE_AVAILABLE, &Population, p_cnt, BioAPI_FALSE, 0, 
		&NumberOfResults, &Candidates, 10000, NULL);

	if (rc != BioAPI_OK) {
		
		if (rc == 0x01000118) {
			sprintf(msg, "Processing cancelled by the user.");
			MessageBox(msg,TITLE,MB_ICONINFORMATION);
		}
		else {
			sprintf(msg, "BioAPI_Identify encountered an error. (0x%08x)\r\n", rc);
			MessageBox(msg,TITLE,MB_ICONSTOP);
		}
		return;
	}

	sprintf(msg, "Identification %s.\r\n\r\nMatched BIRs: %d", (Candidates != NULL) ? "successful" : "failed", NumberOfResults);
	MessageBox(msg,TITLE,(Candidates != NULL) ? MB_ICONINFORMATION : MB_ICONEXCLAMATION);

	
	for (uint32_t i = 0; i < NumberOfResults; i++)
	{
		BioAPI_Free(Candidates[i].BIR.BIRInArray);
	}

	BioAPI_Free(Candidates);
	Candidates = NULL;
	
	for ( int i = 0 ; i < p_cnt ; i++ )
	{
		free( Population.BIRs.BIRArray->Members[ i ].BiometricData.Data ) ;
	}
	free(Population.BIRs.BIRArray->Members);
	free(Population.BIRs.BIRArray);

}
