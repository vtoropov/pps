/**
*@All Rights Reserved.    
*@Copyright (C) 2007,2011, Hitachi Ltd.
*/

// DlgCldTab1.cpp
//

#include "stdafx.h"
#include "BioAPI_SampleApp.h"
#include "DlgCldTab1.h"

#include "BioAPI_SampleAppDlg.h"

// CDlgCldTab1 dialog

IMPLEMENT_DYNAMIC(CDlgCldTab1, CDialog)

CDlgCldTab1::CDlgCldTab1(CWnd* pParent /*=NULL*/)
	: CDialog(CDlgCldTab1::IDD, pParent)
	, m_xvCmbGrp(0)
	, m_xvDataName(_T(""))
{

}

CDlgCldTab1::~CDlgCldTab1()
{
}

void CDlgCldTab1::DoDataExchange(CDataExchange* pDX)
{
	CDialog::DoDataExchange(pDX);
	DDX_Control(pDX, IDC_CMB_GRP, m_xcCmbGrp);
	DDX_CBIndex(pDX, IDC_CMB_GRP, m_xvCmbGrp);
	DDX_Text(pDX, IDC_EDIT_DATA, m_xvDataName);
}


BEGIN_MESSAGE_MAP(CDlgCldTab1, CDialog)
	ON_BN_CLICKED(IDC_BTN_ADD_GRP, &CDlgCldTab1::OnBnClickedBtnAddGrp)
	ON_BN_CLICKED(IDC_BTN_DEL_GRP, &CDlgCldTab1::OnBnClickedBtnDelGrp)
	ON_BN_CLICKED(IDC_BTN_ENROLL, &CDlgCldTab1::OnBnClickedBtnEnroll)
END_MESSAGE_MAP()


// CDlgCldTab1 message handlers

void CDlgCldTab1::OnOK()
{
	// nop
}

void CDlgCldTab1::OnCancel()
{
	// nop
}


void CDlgCldTab1::Init_Tab1()
{
	
	CBioAPI_SampleAppDlg * pr = (CBioAPI_SampleAppDlg*)GetParent();
	
	
	m_DataInfoList = pr->getDataInfoList();

}

BOOL CDlgCldTab1::OnInitDialog(void)
{
	
	this->UpdateBSPHandle() ;
	UpdateData(FALSE);

	return TRUE;
}

void CDlgCldTab1::UpdateBSPHandle(void)
{
	
	CBioAPI_SampleAppDlg * pr = (CBioAPI_SampleAppDlg*)GetParent();
	
	
	m_BSPHandle = pr->getBSPHandle();
}

void CDlgCldTab1::OnBnClickedBtnAddGrp()
{
	
	CString str, str_GrpName;	
	int grp_len;	
	int i, ct;	
	char msg[256];
	
	CGrpAdd GrpAdd;
	
	if (GrpAdd.DoModal() == IDOK) {
		
		str_GrpName = GrpAdd.getGrpName();
		str_GrpName.Replace("�@", " ");
		
		str_GrpName = str_GrpName.Trim();
		
		
		ct = m_xcCmbGrp.GetCount();	
		
		for (i=0; i<ct; i++) {

			UpdateData(TRUE);
			
			m_xcCmbGrp.GetLBText(i,str);

			
			if (str == str_GrpName) return;
		}
		
		grp_len = str_GrpName.GetLength();
		
		if (grp_len > 30) {
			sprintf(msg, "A group name must be in up to 30 bytes in size.");
			MessageBox(msg,TITLE,MB_ICONEXCLAMATION);
			return;
		}
		
		if (grp_len != 0) {
			m_xcCmbGrp.InsertString(-1, _T(str_GrpName));
			UpdateData(FALSE);
		}
	}
}


void CDlgCldTab1::OnBnClickedBtnDelGrp()
{
	
	CString str_GrpName;	
	bool flg_NgDel = false;	
	char msg[256];

	UpdateData(TRUE);
	
	if (m_xcCmbGrp.GetCount() == 0) {
		sprintf(msg, "Register a group.");
		MessageBox(msg,TITLE,MB_ICONEXCLAMATION);
		return;
	}
	
	if (m_xvCmbGrp == -1 ) {
		sprintf(msg, "Select a group.");
		MessageBox(msg,TITLE,MB_ICONEXCLAMATION);
		return;
	}
	
	m_xcCmbGrp.GetLBText(m_xvCmbGrp, str_GrpName);
	
	Init_Tab1();

	for (Data_InfoArray::iterator e = m_DataInfoList.begin(); e != m_DataInfoList.end(); e++) {
		
		
		if (e->GName == str_GrpName) {
			
			flg_NgDel = true;
			break;
		}
	}
	
	if (flg_NgDel) {
		sprintf(msg, "You cannot remove a group specified for capture.");
		MessageBox(msg,TITLE,MB_ICONEXCLAMATION);
		return;
	}
	
	m_xcCmbGrp.DeleteString(m_xvCmbGrp);
	UpdateData(FALSE);
}


void CDlgCldTab1::OnBnClickedBtnEnroll()
{
	
	char msg[256];
	BioAPI_RETURN rc;
	CString str_DataName;	
	
	UpdateData(TRUE);
	
	if (m_xcCmbGrp.GetCount() == 0) {
		sprintf(msg, "Register a group name.");
		MessageBox(msg,TITLE,MB_ICONEXCLAMATION);
		return;
	}
	
	if (m_xvCmbGrp == -1) {
		sprintf(msg, "Select a group name.");
		MessageBox(msg,TITLE,MB_ICONEXCLAMATION);
		return;
	}
	
	str_DataName = m_xvDataName.GetString() ;	
	if (str_DataName.GetLength() == 0) {
		sprintf(msg, "Specify the data name.");
		MessageBox(msg,TITLE,MB_ICONEXCLAMATION);
		return;
	}
	
	if (str_DataName.GetLength() > 30) {
		sprintf(msg, "A data name must be in up to 30 bytes in size.");
		MessageBox(msg,TITLE,MB_ICONEXCLAMATION);
		return;
	}
	
	CBioAPI_SampleAppDlg * pr = (CBioAPI_SampleAppDlg*)GetParent();
	
	if (pr->getChkBeepState()) {
		
		rc = pr->setBeepOff(true);
	}
	else {
		
		rc = pr->setBeepOff(false);
	}

	if (rc != BioAPI_OK) {
		sprintf(msg, "An error occurred in sound alert control.");
		MessageBox(msg,TITLE,MB_ICONSTOP);
		return;
	}
	
	
	BioAPI_BIR_HANDLE TemplateBIRHandle;	
	
	BioAPI_BIR_PURPOSE Purpose = BioAPI_PURPOSE_ENROLL;	
	BioAPI_BIR_SUBTYPE Subtype = BioAPI_NO_SUBTYPE_AVAILABLE;	
	int32_t Timeout = 10000;	

	//-------- BioAPI_Enroll --------//
	rc = BioAPI_Enroll(m_BSPHandle, Purpose, Subtype, NULL, NULL, &TemplateBIRHandle, NULL, Timeout, NULL, NULL);

	if (rc != BioAPI_OK) {
		
		if (rc == 0x01000118) {
			sprintf(msg, "Processing cancelled by the user.");
			MessageBox(msg,TITLE,MB_ICONINFORMATION);
		}
		else {
			sprintf(msg, "BioAPI_Enroll encountered an error. (0x%08x)\r\n", rc);
			MessageBox(msg,TITLE,MB_ICONSTOP);
		}
		return;
	}
	
	
	BioAPI_BIR BIR;
	BIR.BiometricData.Data = NULL;
	BIR.SecurityBlock.Data = NULL;
	
	//-------- BioAPI_GetBIRFromHandle --------//
	rc = BioAPI_GetBIRFromHandle(m_BSPHandle, TemplateBIRHandle, &BIR);

	if (rc != BioAPI_OK) {
		sprintf(msg, "BioAPI_GetBIRFromHandle encountered an error. (0x%08x)\r\n", rc);
		MessageBox(msg,TITLE,MB_ICONSTOP);
		return;
	}
	
	
	DATA_INFO DataInfo;

	m_xcCmbGrp.GetLBText(m_xvCmbGrp, DataInfo.GName);	
	DataInfo.DName = m_xvDataName;	
	
	pr->setDataInfoList(DataInfo);
	pr->setBIRList(BIR);

	sprintf(msg, "Registration successful.");
	MessageBox(msg, TITLE, MB_ICONINFORMATION);

}

