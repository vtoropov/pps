/**
*@All Rights Reserved.    
*@Copyright (C) 2007,2011, Hitachi Ltd.
*/

// DlgCldTab2.cpp
//

#include "stdafx.h"
#include "BioAPI_SampleApp.h"
#include "DlgCldTab2.h"

#include "BioAPI_SampleAppDlg.h"

// CDlgCldTab2 dialog

IMPLEMENT_DYNAMIC(CDlgCldTab2, CDialog)

CDlgCldTab2::CDlgCldTab2(CWnd* pParent /*=NULL*/)
	: CDialog(CDlgCldTab2::IDD, pParent)
	, m_xvSelectedIndex(0)
{

}

CDlgCldTab2::~CDlgCldTab2()
{
}

void CDlgCldTab2::DoDataExchange(CDataExchange* pDX)
{
	CDialog::DoDataExchange(pDX);
	DDX_Control(pDX, IDC_LIST_BIR, m_xcListBIR);
	DDX_LBIndex(pDX, IDC_LIST_BIR, m_xvSelectedIndex);
}


BEGIN_MESSAGE_MAP(CDlgCldTab2, CDialog)
	ON_BN_CLICKED(IDC_BTN_VERIFY, &CDlgCldTab2::OnBnClickedBtnVerify)
END_MESSAGE_MAP()


// CDlgCldTab2 message handlers

void CDlgCldTab2::OnOK()
{
	// nop
}

void CDlgCldTab2::OnCancel()
{
	// nop
}


void CDlgCldTab2::Init_Tab2()
{
	
	char buf[256];
	
	CBioAPI_SampleAppDlg * pr = (CBioAPI_SampleAppDlg*)GetParent();
	
	m_DataInfoList = pr->getDataInfoList();
	m_BIRList = pr->getBIRList();

	m_xcListBIR.ResetContent();
	
	for (Data_InfoArray::iterator e = m_DataInfoList.begin(); e != m_DataInfoList.end(); e++) {
		sprintf(buf, "Group name:%s, Data name:%s", e->GName, e->DName);
		m_xcListBIR.InsertString(-1, _T(buf));
	}

	UpdateData(FALSE);
	
	Invalidate( FALSE ) ;

}


BOOL CDlgCldTab2::OnInitDialog(void)
{
	
	this->UpdateBSPHandle() ;
	UpdateData(FALSE);

	return TRUE;
}


void CDlgCldTab2::UpdateBSPHandle(void)
{
	
	CBioAPI_SampleAppDlg * pr = (CBioAPI_SampleAppDlg*)GetParent();
	
	m_BSPHandle = pr->getBSPHandle();
}


void CDlgCldTab2::OnBnClickedBtnVerify()
{
	
	char msg[256];
	BioAPI_RETURN rc;
	UpdateData(TRUE);
	
	if (m_xcListBIR.GetCount() == 0) {
		sprintf(msg, "There is no template registered to match.");
		MessageBox(msg,TITLE,MB_ICONEXCLAMATION);
		return;
	}
	
	m_xvSelectedIndex = m_xcListBIR.GetCurSel();
	
	if (m_xvSelectedIndex == -1) {
		sprintf(msg, "Select a template to match.");
		MessageBox(msg,TITLE,MB_ICONEXCLAMATION);
		return;
	}
	
	CBioAPI_SampleAppDlg * pr = (CBioAPI_SampleAppDlg*)GetParent();
	
	if (pr->getChkBeepState()) {
		
		rc = pr->setBeepOff(true);
	}
	else {
		
		rc = pr->setBeepOff(false);
	}

	if (rc != BioAPI_OK) {
		sprintf(msg, "An error occurred in sound alert control.");
		MessageBox(msg,TITLE,MB_ICONSTOP);
		return;
	}
	
	
	BioAPI_BOOL Result;
	BioAPI_FMR FMRAchieved;
	
	BioAPI_FMR MaxFMRRequested = 2072;	
	BioAPI_INPUT_BIR ReferenceTemplateInputData;
	ReferenceTemplateInputData.Form = BioAPI_FULLBIR_INPUT;	
	ReferenceTemplateInputData.InputBIR.BIR = &( m_BIRList[ m_xvSelectedIndex ] ) ;

	//-------- BioAPI_Verify --------//
	rc = BioAPI_Verify(m_BSPHandle, MaxFMRRequested, 
		&ReferenceTemplateInputData, BioAPI_NO_SUBTYPE_AVAILABLE, NULL,
		&Result, &FMRAchieved, NULL, 10000, NULL);

	if (rc != BioAPI_OK) {
		
		if (rc == 0x01000118) {
			sprintf(msg, "Processing cancelled by the user.");
			MessageBox(msg,TITLE,MB_ICONINFORMATION);
		}
		else {
			sprintf(msg, "BioAPI_Verify encountered an error. (0x%08x)\r\n", rc);
			MessageBox(msg,TITLE,MB_ICONSTOP);
		}
		return;
	}
	
	sprintf(msg, "Authentication %s.",(Result == BioAPI_FALSE) ? "failed" : "successful");
	MessageBox(msg,TITLE,(Result == BioAPI_FALSE) ? MB_ICONEXCLAMATION : MB_ICONINFORMATION);

}

